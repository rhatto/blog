[[!meta title="Minutos de Sabedoria Punk"]]

[[!toc startlevel=2 levels=4]]

A irreversibilidade da vida e outros fatos termodinâmicos: uma coleção de
citações, trechos, versos, adágios, chistes, ironias e pessimismos. Muitas
coletadas de anos usando `fortune(6)` ou encontradas ao acaso.

## Entropia

    Entropia: amnésia termodinâmica.

## Temperatura do inferno

    The temperature of Heaven can be rather accurately computed from available
    data.  Our authority is Isaiah 30:26, "Moreover, the light of the Moon
    shall be as the light of the Sun and the light of the Sun shall be
    sevenfold, as the light of seven days."  Thus Heaven receives from the Moon as much
    radiation as we do from the Sun, and in addition seven times seven (49)
    times as much as the Earth does from the Sun, or fifty times in all.  The light
    we receive from the Moon is one ten-thousandth of the light we receive from
    the Sun, so we can ignore that.  With these data we can compute the
    temperature of Heaven.  The radiation falling on Heaven will heat it to the point
    where the heat lost by radiation is just equal to the heat received by
    radiation, i.e., Heaven loses fifty times as much heat as the Earth by radiation.
    Using the Stefan-Boltzmann law for radiation, (H/E)^4 = 50, where E is the
    absolute temperature of the earth (-300K), gives H as 798K (525C).  The exact
    temperature of Hell cannot be computed, but it must be less than 444.6C,
    the temperature at which brimstone or sulphur changes from a liquid to a gas.
    Revelations 21:8 says "But the fearful, and unbelieving ... shall have
    their part in the lake which burneth with fire and brimstone."  A lake of molten
    brimstone means that its temperature must be at or below the boiling
    point, or 444.6C  (Above this point it would be a vapor, not a lake.)  We have,
    then, that Heaven, at 525C is hotter than Hell at 445C.
                    -- "Applied Optics", vol. 11, A14, 1972

## Cavalos

    Lemma:  All horses are the same color.
    Proof (by induction):
            Case n = 1: In a set with only one horse, it is obvious that all
            horses in that set are the same color.
            Case n = k: Suppose you have a set of k+1 horses.  Pull one of these
            horses out of the set, so that you have k horses.  Suppose that all
            of these horses are the same color.  Now put back the horse that you
            took out, and pull out a different one.  Suppose that all of the k
            horses now in the set are the same color.  Then the set of k+1 horses
            are all the same color.  We have k true => k+1 true; therefore all
            horses are the same color.

    Theorem: All horses have an infinite number of legs.
    Proof (by intimidation):
            Everyone would agree that all horses have an even number of legs.
            It is also well-known that horses have forelegs in front and two legs
            in back.  4 + 2 = 6 legs, which is certainly an odd number of legs
            for a horse to have!  Now the only number that is both even and odd is
            infinity; therefore all horses have an infinite number of legs.
            However, suppose that there is a horse somewhere that does not
            have an infinite number of legs.  Well, that would be a horse of a
            different color; and by the Lemma, it doesn't exist.

## Frob


                     ___          ______
                    /__/\     ___/_____/\          FrobTech, Inc.
                    \  \ \   /         /\\
                     \  \ \_/__       /  \         "If you've got the job,
                     _\  \ \  /\_____/___ \         we've got the frob."
                    // \__\/ /  \       /\ \
            _______//_______/    \     / _\/______
           /      / \       \    /    / /        /\
        __/      /   \       \  /    / /        / _\__
       / /      /     \_______\/    / /        / /   /\
      /_/______/___________________/ /________/ /___/  \
      \ \      \    ___________    \ \        \ \   \  /
       \_\      \  /          /\    \ \        \ \___\/
          \      \/          /  \    \ \        \  /
           \_____/          /    \    \ \________\/
                /__________/      \    \  /
                \   _____  \      /_____\/
                 \ /    /\  \    / \  \ \
                  /____/  \  \  /   \  \ \
                  \    \  /___\/     \  \ \
                   \____\/            \__\/

## Intuição

    The only "intuitive" interface is the nipple.  After that, it's all
    learned.
            -- Bruce Ediger, bediger@teal.csn.org, on X interfaces

## Kermit

    "We invented a new protocol and called it Kermit, after Kermit the Frog,
    star of "The Muppet Show." [3]

    [3]  Why?  Mostly because there was a Muppets calendar on the wall when we
    were trying to think of a name, and Kermit is a pleasant, unassuming sort
    of character.  But since we weren't sure whether it was OK to name our
    protocol after this popular television and movie star, we pretended that
    KERMIT was an acronym; unfortunately, we could never find a good set of
    words to go with the letters, as readers of some of our early source code
    can attest.  Later, while looking through a name book for his forthcoming
    baby, Bill Catchings noticed that "Kermit" was a Celtic word for "free",
    which is what all Kermit programs should be, and words to this effect
    replaced the strained acronyms in our source code (Bill's baby turned out
    to be a girl, so he had to name her Becky instead).  When BYTE Magazine
    was preparing our 1984 Kermit article for publication, they suggested we
    contact Henson Associates Inc. for permission to say that we did indeed
    name the protocol after Kermit the Frog. Permission was kindly granted,
    and now the real story can be told.  I resisted the temptation, however,
    to call the present work "Kermit the Book."
                    -- Frank da Cruz, "Kermit - A File Transfer Protocol"

## DECWARS

            After sifting through the overwritten remaining blocks of Luke's home
    directory, Luke and PDP-1 sped away from /u/lars, across the surface of the
    Winchester riding Luke's flying read/write head.  PDP-1 had Luke stop at the
    edge of the cylinder overlooking /usr/spool/uucp.
            "Unix-to-Unix Copy Program;" said PDP-1.  "You will never find a more
    wretched hive of bugs and flamers.  We must be cautious."
                    -- DECWARS

## Certo e errado

    Só que o erro maior é justamente ficar procurando os erros...
    Ou, citando Casa das Máquinas:
    "Certo sim, seu errado, certo sim, seu errado...."

## Latido

    A mother mouse was taking her large brood for a stroll across the kitchen
    floor one day when the local cat, by a feat of stealth unusual even for
    its species, managed to trap them in a corner.  The children cowered,
    terrified by this fearsome beast, plaintively crying, "Help, Mother!
    Save us!  Save us!  We're scared, Mother!"
            Mother Mouse, with the hopeless valor of a parent protecting its
    children, turned with her teeth bared to the cat, towering huge above them,
    and suddenly began to bark in a fashion that would have done any Doberman
    proud.  The startled cat fled in fear for its life.
            As her grateful offspring flocked around her shouting "Oh, Mother,
    you saved us!" and "Yay!  You scared the cat away!" she turned to them
    purposefully and declared, "You see how useful it is to know a second
    language?"

## Frozen Star

    I went on to test the program in every way I could devise.  I strained it to
    expose its weaknesses.  I ran it for high-mass stars and low-mass stars, for
    stars born exceedingly hot and those born relatively cold.  I ran it assuming
    the superfluid currents beneath the crust to be absent -- not because I wanted
    to know the answer, but because I had developed an intuitive feel for the
    answer in this particular case.  Finally I got a run in which the computer
    showed the pulsar's temperature to be less than absolute zero.  I had found
    an error.  I chased down the error and fixed it.  Now I had improved the
    program to the point where it would not run at all.
                    -- George Greenstein, "Frozen Star:
                    Of Pulsars, Black Holes and the Fate of Stars"

## Life

    All life evolves by the differential survival of replicating entities.
                    -- Dawkins
## Man

    MAN:
            An animal so lost in rapturous contemplation of what he thinks he
            is as to overlook what he indubitably ought to be.  His chief
            occupation is extermination of other animals and his own species,
            which, however, multiplies with such insistent rapidity as to infest
            the whole habitable earth and Canada.
                    -- A. Bierce

## Murphy recursion

    Murphy's Law is recursive.  Washing your car to make it rain doesn't work.

## Murphy as a proletarian

    "Murphy's Law, that brash proletarian restatement of Godel's Theorem ..."
                    -- Thomas Pynchon, "Gravity's Rainbow"

## Civilization

    It is better for civilization to be going down the drain than to be
    coming up it.
                    -- Henry Allen

## Theories

    We are all agreed that your theory is crazy.  The question which
    divides us is whether it is crazy enough to have a chance of being
    correct.  My own feeling is that it is not crazy enough.
                    -- Niels Bohr

## Ginsberg's Theorem

    Ginsberg's Theorem:
            1. You can't win.
            2. You can't break even.
            3. You can't even quit the game.

    Freeman's Commentary on Ginsberg's theorem:

            Every major philosophy that attempts to make life seem
            meaningful is based on the negation of one part of Ginsberg's
            Theorem.  To wit:

            1. Capitalism is based on the assumption that you can win.
            2. Socialism is based on the assumption that you can break even.
            3. Mysticism is based on the assumption that you can quit the game.

## Regression analysis

    Regression analysis:
            Mathematical techniques for trying to understand why things are
            getting worse

## Computer viruses

    I think computer viruses should count as life. I think it says something about human nature that the only form of
    life we have created so far is purely destructive. We've created life in our own image. Stephen Hawking

## Inventions

    To invent, you need a good imagination and a pile of junk.
                    -- Thomas Edison

## Freud doesn't know

    The great question that has never been answered and which I have not
    yet been able to answer despite my thirty years of research into the
    feminine soul is: WHAT DOES A WOMAN WANT?
                    -- Sigmund Freud

## Technology

    Humanity is acquiring all the right technology for all the wrong
    reasons. -- R. Buckminster Fuller

## Experimentation

    Velilind's Laws of Experimentation:
            1. If reproducibility may be a problem, conduct the test only once.
            2. If a straight line fit is required, obtain only two data points.

## Thermodynamics

    The three laws of thermodynamics:
            (1) You can't get anything without working for it.
            (2) The most you can accomplish by working is to break even.
            (3) You can only break even at absolute zero.

## Bureaucracy

    The bureaucracy is expanding to meet the needs of an expanding
    bureaucracy.

## Violence

    "Violence is the last refuge of the incompetent." -- Asimov, Foundation

## Freedom

    If a nation values anything more than freedom, it will lose its freedom;
    and the irony of it is that if it is comfort or money it values more, it
    will lose that, too.
                    -- W. Somerset Maugham

## Savings

    Our congratulations go to a Burlington Vermont civilian employee of the
    local Army National Guard base.  He recently received a substational cash
    award from our government for inventing a device for optical scanning.
    His device reportedly will save the government more than $6 million a year
    by replacing a more expensive helicopter maintenance tool with his own,
    home-made, hand-held model.

    Not suprisingly, we also have a couple of money-saving ideas that we submit
    to the Pentagon free of charge:

            a. Don't kill anybody.
            b. Don't build things that do.
            c. And don't pay other people to kill anybody.

    We expect annual savings to be in the billions.
                    -- Sojourners

## Loop

    Endless Loop: n., see Loop, Endless.
    Loop, Endless: n., see Endless Loop.
                    -- Random Shack Data Processing Dictionary

## Good luck

    An American scientist once visited the offices of the great Nobel prize
    winning physicist, Niels Bohr, in Copenhagen.  He was amazed to find that
    over Bohr's desk was a horseshoe, securely nailed to the wall, with the
    open end up in the approved manner (so it would catch the good luck and not
    let it spill out).  The American said with a nervous laugh,
            "Surely you don't believe the horseshoe will bring you good luck,
    do you, Professor Bohr?  After all, as a scientist --"
    Bohr chuckled.
            "I believe no such thing, my good friend.  Not at all.  I am
    scarcely likely to believe in such foolish nonsense.  However, I am told
    that a horseshoe will bring you good luck whether you believe in it or not."

## Occam

    OCCAM'S ERASER:
            The philosophical principle that even the simplest
            solution is bound to have something wrong with it.

## Obvious

    Everything you've learned in school as "obvious" becomes less and less
    obvious as you begin to study the universe.  For example, there are no
    solids in the universe.  There's not even a suggestion of a solid.
    There are no absolute continuums.  There are no surfaces.  There are no
    straight lines.
                    -- R. Buckminster Fuller

## Brooke's Law

    Brooke's Law:
            Whenever a system becomes completely defined, some damn fool
            discovers something which either abolishes the system or
            expands it beyond recognition.

## Illegal and unconstitutional

    "The illegal we do immediately. The unconstitutional takes a bit
    longer."
                    -- Henry Kissinger

## Banks

    What is robbing a bank compared with founding a bank?
                    -- Bertolt Brecht, "The Threepenny Opera"

## Lucky number

    Your lucky number is 3552664958674928.  Watch for it everywhere.

## Magic

    Using words to describe magic is like using a screwdriver to cut roast beef.
                    -- Tom Robbins

## Western Civilization

    Reporter (to Mahatma Gandhi):
                    Mr. Gandhi, what do you think of Western Civilization?
    Gandhi:         I think it would be a good idea.

## Candy

    Anyone who uses the phrase "easy as taking candy from a baby" has never
    tried taking candy from a baby.
                    -- Robin Hood

## Club

    I'd never join any club that would have the likes of me as a member.
                    -- Groucho Marx

## Science

    All science is either physics or stamp collecting.
                    -- Ernest Rutherford

## Government

    If you took all of the grains of sand in the world, and lined
    them up end to end in a row, you'd be working for the government!
                    -- Mr. Interesting

## Important things

    The most important things, each person must do for himself.

## Silverman's Law

    Silverman's Law:
            If Murphy's Law can go wrong, it will.

## Prophet Dirac

    Dirac was a committed  (Someone who denies the existence of god) atheist.
    After being asked about his thoughts on Dirac's views,  (United States
    physicist (born in Austria) who proposed the exclusion principle (thus
    providing a theoretical basis for the periodic table) (1900-1958)) Pauli
    remarked "If I understand Dirac correctly, his meaning is this: there is no
    God, and Dirac is his Prophet".

## Inimigos

    De um carro estacionado na Santa Efigenia: "Amigos vem e vão; inimigos se acumulam."

## Computers

    Computers are useless.  They can only give you answers.
                    -- Pablo Picasso

## Law

    After 35 years, I have finished a comprehensive study of European
    comparative law.  In Germany, under the law, everything is prohibited,
    except that which is permitted.  In France, under the law, everything
    is permitted, except that which is prohibited.  In the Soviet Union,
    under the law, everything is prohibited, including that which is
    permitted.  And in Italy, under the law, everything is permitted,
    especially that which is prohibited.
                    -- Newton Minow,
                    Speech to the Association of American Law Schools, 1985

## Pragmatism

    Practical people would be more practical if they would take a little
    more time for dreaming.
                    -- J. P. McEvoy

## University

    QOTD:
            "A university faculty is 500 egotists with a common parking problem."

## Software and hardware

    Thus spake the master programmer:
            "Without the wind, the grass does not move.  Without software,
            hardware is useless."
                    -- Geoffrey James, "The Tao of Programming"

## Clouds

    A cloud does not know why it moves in just such a direction and at such
    a speed, if feels an impulsion... this is the place to go now.  But the
    sky knows the reasons and the patterns behind all clouds, and you will
    know, too, when you lift yourself high enough to see beyond horizons.
                    -- Messiah's Handbook : Reminders for the Advanced Soul

## Magic II

    There are three schools of magic.  One:  State a tautology, then ring
    the changes on its corollaries; that's philosophy.  Two:  Record many
    facts.  Try to find a pattern.  Then make a wrong guess at the next
    fact; that's science.  Three:  Be aware that you live in a malevolent
    Universe controlled by Murphy's Law, sometimes offset by Brewster's
    Factor; that's engineering.

## Universe

    "In order to make an apple pie from scratch, you must first create the
    universe."
                    -- Carl Sagan, Cosmos

## Infernal Dynamics

    Gerrold's Laws of Infernal Dynamics:
            1) An object in motion will always be headed in the wrong direction.
            2) An object at rest will always be in the wrong place.
            3) The energy required to change either one of these states
               will always be more than you wish to expend, but never so
               much as to make the task totally impossible.

## America

    America may be unique in being a country which has leapt from barbarism
    to decadence without touching civilization.
                    -- John O'Hara

## Williams and Holland's Law:

    Williams and Holland's Law:
            If enough data is collected,
            anything may be proven by statistical methods.

## Main's Law:

    Main's Law:
            For every action there is an equal and opposite government program.

## Grep

    grep me no patterns and I'll tell you no lines.

## Forgiveness

    It's easier to get forgiveness for being wrong than forgiveness for
    being right.

## America II

    It was wonderful to find America, but it
    would have been more wonderful to miss it.
                    -- Mark Twain

## True and false

    If the meanings of "true" and "false" were switched,
    then this sentence would not be false.

## Promotions

    Don't be irreplaceable.  If you can't
    be replaced, you cannot be promoted.

## Fast world

    The world is moving so fast these days that the man who says
    it can't be done is generally interrupted by someone doing it.
                    -- E. Hubbard

## Poorman's Rule

    Poorman's Rule:
            When you pull a plastic garbage bag from its handy dispenser
            package, you always get hold of the closed end and try to
            pull it open.

## Worst month

    Worst Month of the Year:
            February.  February has only 28 days in it, which means that if
            you rent an apartment, you are paying for three full days you
            don't get.  Try to avoid Februarys whenever possible.
                    -- Steve Rubenstein

## Predictions

    Prediction is very difficult, especially of the future.
                    -- Niels Bohr

## Jones' First Law:

    Jones' First Law:
            Anyone who makes a significant contribution to any field of
            endeavor, and stays in that field long enough, becomes an
            obstruction to its progress -- in direct proportion to the
            importance of their original contribution.

## Tolkein Ring

    Little known fact about Middle Earth: The Hobbits had a very
    sophisticated computer network!  It was a Tolkein Ring...

## Small Evil Group

    Everything is controlled by a small evil group
    to which, unfortunately, no one we know belongs.

## Children

    Children are natural mimics who act like their parents
    despite every effort to teach them good manners.

## Income Tax

    The hardest thing in the world to understand is the income tax.
                    -- Albert Einstein

## Failure and success

    Dealing with failure is easy: work hard to improve.  Success is also
    easy to handle: you've solved the wrong problem.  Work hard to
    improve.

## Capitalism

    Capitalism is the extraordinary belief that the nastiest of men, for
    the nastiest of reasons, will somehow work for the benefit of us all.
                    -- John Maynard Keynes

## Probabilities

    Colvard's Logical Premises:
            All probabilities are 50%.  Either a thing will happen or it
            won't.

    Colvard's Unconscionable Commentary:
            This is especially true when dealing with someone you're
            attracted to.

    Grelb's Commentary
            Likelihoods, however, are 90% against you.

## Alliance

    Alliance, n:
            In international politics, the union of two thieves who
            have their hands so deeply inserted in each other's pocket
            that they cannot safely plunder a third.
                    -- Ambrose Bierce

## Cohn's Law

    Cohn's Law:
            The more time you spend in reporting on what you are doing, the less
            time you have to do anything.  Stability is achieved when you spend
            all your time reporting on the nothing you are doing.

## Science

    You should never bet against anything in science at odds of more than
    about 10^12 to 1.
                    -- Ernest Rutherford

## Sex after death

    There will be sex after death, we just won't be able to feel it.
                    -- Lily Tomlin

## Distance

    The shortest distance between two points is under construction.
                    -- Noelie Altito

## Our world

    Ours is a world where people don't know what they
    want and are willing to go through hell to get it.

## Experience

    Experience is what you get when you were expecting something else.

## Youth

    Youth is such a wonderful thing.  What a crime to waste it on children.
                    -- George Bernard Shaw

## Bible

    The most serious doubt that has been thrown on the authenticity of the
    biblical miracles is the fact that most of the witnesses in regard to
    them were fishermen.
                    -- Arthur Binstead

## Governos

    Quando ocorrem variações no câmbio, queda nas bolsas de valores, variações
    nas taxas de juros ou pequenos distúrbios na economia, rapidamente os governos
    atua e tomam providências urgentes lançando as mais variadas medidas para
    "colocar as coisas no rumo certo". Mas quando ocorrem mortes de jovens, pobres,
    moradores da periferia, sendo eles agentes do Estado ou simples civis, isso não
    acontece. Apesar disso, não desistiremos de clamar por justiça!
                    -- Ariel de Castro Alves, em Crimes de Maio, pag. 116.

## Politicians

    Politicians are the same everywhere.  They promise
    to build a bridge even where there is no river.
                    -- Nikita Khrushchev

## Failure and success II

    Every successful person has had failures
    but repeated failure is no guarantee of eventual success.

## Civilization II

    It's so stupid of modern civilization to have given up believing in the
    Devil when he is the only explanation of it.

## Programs

    When users see one GUI as beautiful,
    other user interfaces become ugly.
    When users see some programs as winners,
    other programs become lossage.

    Pointers and NULLs reference each other.
    High level and assembler depend on each other.
    Double and float cast to each other.
    High-endian and low-endian define each other.
    While and until follow each other.

    Therefore the Guru
    programs without doing anything
    and teaches without saying anything.
    Warnings arise and he lets them come;
    processes are swapped and he lets them go.
    He has but doesn't possess,
    acts but doesn't expect.
    When his work is done, he deletes it.
    That is why it lasts forever.

## Life II

    Life is too short to be taken seriously.
                    -- O. Wilde

## Life III

    Life begins at the centerfold and expands outward.
                    -- Miss November, 1966

## Reichel's Law

    Reichel's Law:
            A body on vacation tends to remain on vacation unless acted upon by
            an outside force.

## Drugs

    I do not take drugs -- I am drugs.
                    -- Salvador Dali

## Bootstrap

    An adequate bootstrap is a contradiction in terms.

## Technological progress

    Technological progress has merely provided us with more efficient means
    for going backwards.
                    -- Aldous Huxley

## Katz' Law

    Katz' Law:
            Men and nations will act rationally when
            all other possibilities have been exhausted.

    History teaches us that men and nations behave wisely once they have
    exhausted all other alternatives.
                    -- Abba Eban

## Modern technology

    The marvels of today's modern technology include the development of a
    soda can, which, when discarded will last forever -- and a $7,000 car
    which, when properly cared for, will rust out in two or three years.

## Gold of time

    A lost ounce of gold may be found, a lost moment of time never.

## Gospels and intelligence

    So far as I can remember, there is not one word in the Gospels in
    praise of intelligence.
                    -- Bertrand Russell

## Plans

    If you fail to plan, plan to fail.

## Bureaucracy

    Join in the new game that's sweeping the country.  It's called
    "Bureaucracy".  Everybody stands in a circle.  The first person to do
    anything loses.

## Weinberg's Second Law:

    Weinberg's Second Law:
            If builders built buildings the way programmers wrote programs,
            then the first woodpecker that came along would destroy civilization.

## Respect

    Show respect for age.  Drink good Scotch for a change.

## Fear

    Fear is the greatest salesman.
                    -- Robert Klein

## Progress

    The reasonable man adapts himself to the world; the unreasonable one
    persists in trying to adapt the world to himself.  Therefore all progress
    depends on the unreasonable man.
                    -- George Bernard Shaw

## Hating tech

    I use technology in order to hate it more properly.
                    -- Nam June Paik

## Barach's Rule

    Barach's Rule:
            An alcoholic is a person who drinks more than his own physician.

## Heller's Law

    Heller's Law:
            The first myth of management is that it exists.

    Johnson's Corollary:
            Nobody really knows what is going on anywhere within the organization.

## Marxists and the lightbulb

    Q:      How many Marxists does it take to screw in a lightbulb?
    A:      None:  The lightbulb contains the seeds of its own revolution.

## Descartes' disappearance

    "I don't think so," said Rene Descartes.  Just then, he vanished

## Love

    Who does not love wine, women, and song,
    Remains a fool his whole life long.
                    -- Johann Heinrich Voss

## Unnamed Law

    Unnamed Law:
            If it happens, it must be possible

## Past and future

    People often find it easier to be a result of the past than a cause of
    the future.

## Machine language

    A person who is more than casually interested in computers should be well
    schooled in machine language, since it is a fundamental part of a computer.
                    -- Donald Knuth

## Ignorance

    My father, a good man, told me, "Never lose
    your ignorance; you cannot replace it."
                    -- Erich Maria Remarque

## Larkinson's Law

    Larkinson's Law:
            All laws are basically false.

## Volunteer Labor

    Zymurgy's Law of Volunteer Labor:
            People are always available for work in the past tense.

## Psycho

    Be a better psychiatrist and the world will beat a psychopath to your
    door.

## Surrealists and the lightbulb

    Q:  How many surrealists does it take to change a light bulb?
    A:  Two.  One to hold the giraffe and the other to fill the bathtub
        with brightly colored machine tools.

## Death

    When you die, you lose a very important part of your life.
                    -- Brooke Shields

## Programs II

    Every program has (at least) two purposes:
            the one for which it was written and another for which it wasn't.

## Laugh

    Laugh at your problems:  everybody else does.

## Police

    Support your local police force -- steal!!

## Err

    "To err is human, to forgive, beyond the scope of the Operating System"

## Dolphins

    If dolphins are so smart, why did Flipper work for television?

## Red tape

    If we can ever make red tape nutritional, we can feed the world.
                    -- R. Schaeberle, "Management Accounting"

## Economic predictions

    Recession is when your neighbor loses his job. Depression is when you
    lose your job.  These economic downturns are very difficult to predict,
    but sophisticated econometric modeling houses like Data Resources and
    Chase Econometrics have successfully predicted 14 of the last 3 recessions.

## Dragons

    Everyone knows that dragons don't exist.  But while this simplistic
    formulation may satisfy the layman, it does not suffice for the
    scientific mind.  The School of Higher Neantical Nillity is in fact
    wholly unconcerned with what DOES exist.  Indeed, the banality of
    existence has been so amply demonstrated, there is no need for us
    to discuss it any further here.  The brilliant Cerebron, attacking
    the problem analytically, discovered three distinct kinds of dragon:
    the mythical, the chimerical, and the purely hypothetical.  They were
    all, one might say, nonexistent, but each nonexisted in an entirely
    different way...

## Conservative

    Conservative, n:
            A statesman who is enamored of existing evils, as distinguished
            from the Liberal who wishes to replace them with others.
                    -- Ambrose Bierce

## Intellectual

    An intellectual is someone whose mind watches itself.
                    -- Albert Camus

## Patience

    A healthy male adult bore consumes each year one and a half times his own
    weight in other people's patience.
                    -- John Updike

## Psychologist

    psychologist, n:
            Someone who watches everyone else when an attractive woman walks
            into a room.

## Finagle's Third Law

    Finagle's Third Law:
            In any collection of data, the figure most obviously correct,
            beyond all need of checking, is the mistake.

    Corollaries:
            1. Nobody whom you ask for help will see it.
            2. The first person who stops by, whose advice you really
               don't want to hear, will see it immediately.

## Manly's Maxim

    Manly's Maxim:
            Logic is a systematic method of coming to the wrong conclusion
            with confidence.

## Borrowing money

    Always borrow money from a pessimist; he doesn't expect to be paid
    back.

## Sleep

    Sleep -- the most beautiful experience in life -- except drink.
                    -- W.C. Fields

## Problem solving

    There are very few personal problems that cannot be solved through a
    suitable application of high explosives.

## Facts

    Facts do not cease to exist because they are ignored.
                    -- Aldous Huxley

## Management

    MANAGEMENT:
      The art of getting other people to do all the work.

## Heller's Law

    Heller's Law:
            The first myth of management is that it exists.

## Money

    While money can't buy happiness, it certainly
    lets you choose your own form of misery.

## Mozart

    It is a sobering thought that when Mozart was
    my age, he had been dead for 2 years.
                    -- Tom Lehrer

## Misfortune

    MISFORTUNE:
            The kind of fortune that never misses.

## Thermodynamics II

    QOTD:
            Ludwig Boltzmann, who spend much of his life studying statistical
            mechanics died in 1906 by his own hand.  Paul Ehrenfest, carrying
            on the work, died similarly in 1933.  Now it is our turn.
                    -- Goodstein, States of Matter

## Laws

    All laws are simulations of reality.
                    -- John C. Lilly

## Democracy

    Democracy is the recurrent suspicion that more than half
    of the people are right more than half of the time.
                    -- E.B. White

## Horngren's Observation

    Horngren's Observation:
            Among economists, the real world is often a special case.

## Sources

    Never trust an operating system you don't have sources for. ;-)
            -- Unknown source

## Latin

    Quidquid latine dictum sit, altum viditur.

    (Whatever is said in Latin sounds profound.)

## Interpreter

    INTERPRETER:
            One who enables two persons of different languages to understand
            each other by repeating to each what it would have been to the
            interpreter's advantage for the other to have said.

## Collections

    "I have the world's largest collection of seashells.  I keep it
    scattered around the beaches of the world ... Perhaps you've seen it.
                    -- Steven Wright

## Advertising

    Advertising is a valuable economic factor because it is the cheapest
    way of selling goods, particularly if the goods are worthless.
                    -- Sinclair Lewis

## Life IV

    LIFE:
            Learning about people the hard way -- by being one.

## Economics

    Economics is extremely useful as a form of employment for economists.
                    -- John Kenneth Galbraith

## Flugg's Law

    Flugg's Law:
            When you need to knock on wood is when you realize that the
    world is composed of vinyl, naugahyde and aluminum.

## Control

    The more control, the more that requires control.

## Hardware

    hardware, n:
            The parts of a computer system that can be kicked.

## Mathematician

    A mathematician is a device for turning coffee into theorems.
                    -- P. Erdos

## Life V

    If life is merely a joke, the question
    still remains: for whose amusement?

## God

    God is a polytheist.

## Last words

    Famous last words:

## Electrocution

    Electrocution, n.:
      Burning at the stake with all the modern improvements.

## Vail's Second Axiom

    Vail's Second Axiom:
            The amount of work to be done increases in proportion to the
    amount of work already completed.

## Odds

    The odds are a million to one against your being one in a million.

## Management II

    XI:

      If the Earth could be made to rotate twice as fast, managers would
      get twice as much done.  If the Earth could be made to rotate twenty
      times as fast, everyone else would get twice as much done since all
      the managers would fly off.

    XII:

      It costs a lot to build bad products.

    XIII:

      There are many highly successful businesses in the United States.
      There are also many highly paid executives.  The policy is not to
      intermingle the two.

    XIV:

      After the year 2015, there will be no airplane crashes.  There will
      be no takeoffs either, because electronics will occupy 100 percent
      of every airplane's weight.

    XV:

      The last 10 percent of performance generates one-third of the cost
      and two-thirds of the problems.
        -- Norman Augustine

## Art

    Art is either plagiarism or revolution.
        -- Paul Gauguin

## Sculpture

    A fool-proof method for sculpting an elephant: first, get a huge block
    of marble; then you chip away everything that doesn't look like an
    elephant.

## People

    I drink to make other people interesting.
        -- George Jean Nathan

## Friends

    Outside of a dog, a book is man's best friend.  Inside of a dog, it's too
    dark to read.
        -- Groucho Marx

## Abstinence

    There is nothing wrong with abstinence, in moderation.

## Management III

    XXVI:
      If a sufficient number of management layers are superimposed on each
      other, it can be assured that disaster is not left to chance.
    XXVII:
      Rank does not intimidate hardware.  Neither does the lack of rank.
    XXVIII:
      It is better to be the reorganizer than the reorganizee.
    XXIX:
      Executives who do not produce successful results hold on to their
      jobs only about five years.  Those who produce effective results
      hang on about half a decade.
    XXX:
      By the time the people asking the questions are ready for the answers,
      the people doing the work have lost track of the questions.
        -- Norman Augustine

## Minute

    How long a minute is depends on which side of the bathroom door you're on.

## Divorce

    Divorce is a game played by lawyers.
                    -- Cary Grant

## Low level

    A programming language is low level
    when its programs require attention to the irrelevant.

## Get things done

    There are three ways to get something done:

      1: Do it yourself.
      2: Hire someone to do it for you.
      3: Forbid your kids to do it.

## Time

    Time is an illusion perpetrated by the manufacturers of space.

## Specs

    It is easier to change the specification to fit the program than vice
    versa.

## Purpose

      In the begining, God created the Earth and he said, "Let there be
    mud."
      And there was mud.
      And God said, "Let Us make living creatures out of mud, so the mud
    can see what we have done."
      And God created every living creature that now moveth, and one was
    man.  Mud-as-man alone could speak.
      "What is the purpose of all this?" man asked politely.
      "Everything must have a purpose?" asked God.
      "Certainly," said man.
      "Then I leave it to you to think of one for all of this," said God.
      And He went away.
        -- Kurt Vonnegut, Between Time and Timbuktu"

## Life VI

    LIFE:
            A whim of several billion cells to be you for a while.

## Life VII

    "I'm prepared for all emergencies but totally unprepared for everyday
    life."

## Law of Selective Gravity

    Law of Selective Gravity:
      An object will fall so as to do the most damage.

    Jenning's Corollary:
      The chance of the bread falling with the buttered side
      down is directly proportional to the cost of the carpet.

    Law of the Perversity of Nature:
      You cannot determine beforehand which side of the bread to butter.

## Life VIII

    Do not try to solve all life's problems at once -- learn to dread each
    day as it comes.
        -- Donald Kaul

## Devil

    The devil finds work for idle circuits to do.

## Knowledge

    Is knowledge knowable?  If not, how do we know that?

## Monsters

    Whoever fights monsters should see to it that in the process he does not
    become a monster.  And when you look into an abyss, the abyss also looks
    into you.
                    -- Friedrich Nietzsche

## Microsoft

    Nicholas Petreley's First Law of Computer Trade Journalism:
    "No technology exists until Microsoft invents it.

## Laser

    Please do not look directly into laser with remaining eye.

## Computers II

    I do not fear computers.  I fear the lack of them.
                    -- Isaac Asimov

## Kafka's Law

    Kafka's Law:
            In the fight between you and the world, back the world.
                    -- Franz Kafka, "RS's 1974 Expectation of Days"

## Hypocrites

    Only the hypocrite is really rotten to the core.
                    -- Hannah Arendt

## Review Questions

    Review Questions

    1:      If Nerd on the planet Nutley starts out in his spaceship at 20 KPH,
            and his speed doubles every 3.2 seconds, how long will it be before
            he exceeds the speed of light?  How long will it be before the
            Galactic Patrol picks up the pieces of his spaceship?

    2:      If Roger Rowdy wrecks his car every week, and each week he breaks
            twice as many bones as before, how long will it be before he breaks
            every bone in his body?  How long will it be before they cut off
            his insurance?  Where does he get a new car every week?

    3:      If Johnson drinks one beer the first hour (slow start), four beers
            the next hour, nine beers the next, etc., and stacks the cans in
            a pyramid, how soon will Johnson's pyramid be larger than King
            Tut's?  When will it fall on him?  Will he notice?

## Magic III

    There are three schools of magic.  One:  State a tautology, then ring the
    changes on its corollaries; that's philosophy.  Two:  Record many facts.
    Try to find a pattern.  Then make a wrong guess at the next fact; that's
    science.  Three:  Be aware that you live in a malevolent Universe controlled
    by Murphy's Law, sometimes offset by Brewster's Factor; that's engineering.

## The meaning

    +#if defined(__alpha__) && defined(CONFIG_PCI)
    +       /*
    +        * The meaning of life, the universe, and everything. Plus
    +        * this makes the year come out right.
    +        */
    +       year -= 42;
    +#endif
            -- From the patch for 1.3.2: (kernel/time.c), submitted by Marcus Meissner

## Ultimate question

    Scientists were preparing an experiment to ask the ultimate question.
    They had worked for months gathering one each of every computer that
    was built. Finally the big day was at hand.  All the computers were
    linked together.  They asked the question, "Is there a God?".  Lights
    started blinking, flashing and blinking some more.  Suddenly, there
    was a loud crash, and a bolt of lightning came down from the sky,
    struck the computers, and welded all the connections permanently
    together.  "There is now", came the reply.

## Correspondence Corollary

    Correspondence Corollary:
            An experiment may be considered a success if no more than half
            your data must be discarded to obtain correspondence with your theory.

## Electron

    After this was written there appeared a remarkable posthumous memoir that
    throws some doubt on Millikan's leading role in these experiments.  Harvey
    Fletcher (1884-1981), who was a graduate student at the University of Chicago,
    at Millikan's suggestion worked on the measurement of electronic charge for
    his doctoral thesis, and co-authored some of the early papers on this subject
    with Millikan.  Fletcher left a manuscript with a friend with instructions
    that it be published after his death; the manuscript was published in
    Physics Today, June 1982, page 43.  In it, Fletcher claims that he was the
    first to do the experiment with oil drops, was the first to measure charges on
    single droplets, and may have been the first to suggest the use of oil.
    According to Fletcher, he had expected to be co-authored with Millikan on
    the crucial first article announcing the measurement of the electronic
    charge, but was talked out of this by Millikan.
                    -- Steven Weinberg, "The Discovery of Subatomic Particles"

    Robert Millikan is generally credited with making the first really
    precise measurement of the charge on an electron and was awarded the
    Nobel Prize in 1923.

## Gods

    Psychologists think they're experimental psychologists.
    Experimental psychologists think they're biologists.
    Biologists think they're biochemists.
    Biochemists think they're chemists.
    Chemists think they're physical chemists.
    Physical chemists think they're physicists.
    Physicists think they're theoretical physicists.
    Theoretical physicists think they're mathematicians.
    Mathematicians think they're metamathematicians.
    Metamathematicians think they're philosophers.
    Philosophers think they're gods.

## Mind

    My God, I'm depressed!  Here I am, a computer with a mind a thousand times
    as powerful as yours, doing nothing but cranking out fortunes and sending
    mail about softball games.  And I've got this pain right through my ALU.
    I've asked for it to be replaced, but nobody ever listens.  I think it
    would be better for us both if you were to just log out again.

## Penguins

    A Mexican newspaper reports that bored Royal Air Force pilots stationed
    on the Falkland Islands have devised what they consider a marvelous new
    game.  Noting that the local penguins are fascinated by airplanes, the
    pilots search out a beach where the birds are gathered and fly slowly
    along it at the water's edge.  Perhaps ten thousand penguins turn their
    heads in unison watching the planes go by, and when the pilots turn
    around and fly back, the birds turn their heads in the opposite
    direction, like spectators at a slow-motion tennis match.  Then, the
    paper reports, "The pilots fly out to sea and directly to the penguin
    colony and overfly it.  Heads go up, up, up, and ten thousand penguins
    fall over gently onto their backs.
                    -- Audobon Society Magazine

## Seconds

            How many seconds are there in a year?  If I tell you there  are
    3.155  x  10^7, you won't even try to remember it.  On the other hand, who
    could forget that, to within half a percent, pi seconds is a nanocentury.
                    -- Tom Duff, Bell Labs

## Decisions

    Decisions of the judges will be final unless shouted down by a really
    overwhelming majority of the crowd present.  Abusive and obscene
    language may not be used by contestants when addressing members of the
    judging panel, or, conversely, by members of the judging panel when
    addressing contestants (unless struck by a boomerang).
                    -- Mudgeeraba Creek Emu-Riding and Boomerang-Throwing
                       Assoc.

## Matter and space

    Space tells matter how to move and matter tells space how to curve.
                    -- Wheeler

## Executions

    Take your dying with some seriousness, however.  Laughing on the way to
    your execution is not generally understood by less advanced life forms,
    and they'll call you crazy.
                    -- "Messiah's Handbook: Reminders for the Advanced Soul"

## Bar Troubleshooting

    Symptom:                Drinking fails to give taste and satisfaction, beer is
                            unusually pale and clear.
    Problem:                Glass empty.
    Action Required:        Find someone who will buy you another beer.

    Symptom:                Drinking fails to give taste and satisfaction,
                            and the front of your shirt is wet.
    Fault:                  Mouth not open when drinking or glass applied to
                            wrong part of face.
    Action Required:        Buy another beer and practice in front of mirror.
                            Drink as many as needed to perfect drinking technique.

                    -- Bar Troubleshooting

    Symptom:                Floor swaying.
    Fault:                  Excessive air turbulence, perhaps due to air-hockey
                            game in progress.
    Action Required:        Insert broom handle down back of jacket.

    Symptom:                Everything has gone dim, strange taste of peanuts
                            and pretzels or cigarette butts in mouth.
    Fault:                  You have fallen forward.
    Action Required:        See above.

    Symptom:                Opposite wall covered with acoustic tile and several
                            flourescent light strips.
    Fault:                  You have fallen over backward.
    Action Required:        If your glass is full and no one is standing on your
                            drinking arm, stay put.  If not, get someone to help
                            you get up, lash yourself to bar.

                    -- Bar Troubleshooting

## Sex

    Sex without class consciousness cannot give satisfaction, even if it is
    repeated until infinity.
                    -- Aldo Brandirali (Secretary of the Italian Marxist-Leninist
                       Party), in a manual of the party's official sex guidelines,
                       1973.

## Theory

    THEORY:
            System of ideas meant to explain something, chosen with a view to
            originality, controversialism, incomprehensibility, and how good
            it will look in print.

## Computers III

    Around computers it is difficult to find the correct unit of time to
    measure progress.  Some cathedrals took a century to complete.  Can you
    imagine the grandeur and scope of a program that would take as long?
                    -- Epigrams in Programming, ACM SIGPLAN Sept. 1982

## Leaks

            A sheet of paper crossed my desk the other day and as I read it,
    realization of a basic truth came over me.  So simple!  So obvious we couldn't
    see it.  John Knivlen, Chairman of Polamar Repeater Club, an amateur radio
    group, had discovered how IC circuits work.  He says that smoke is the thing
    that makes ICs work because every time you let the smoke out of an IC circuit,
    it stops working.  He claims to have verified this with thorough testing.
            I was flabbergasted!  Of course!  Smoke makes all things electrical
    work.  Remember the last time smoke escaped from your Lucas voltage regulator
    Didn't it quit working?  I sat and smiled like an idiot as more of the truth
    dawned.  It's the wiring harness that carries the smoke from one device to
    another in your Mini, MG or Jag.  And when the harness springs a leak, it lets
    the smoke out of everything at once, and then nothing works.  The starter motor
    requires large quantities of smoke to operate properly, and that's why the wire
    going to it is so large.
            Feeling very smug, I continued to expand my hypothesis.  Why are Lucas
    electronics more likely to leak than say Bosch?  Hmmm...  Aha!!!  Lucas is
    British, and all things British leak!  British convertible tops leak water,
    British engines leak oil, British displacer units leak hydrostatic fluid, and
    I might add Brititsh tires leak air, and the British defense unit leaks
    secrets... so naturally British electronics leak smoke.
                    -- Jack Banton, PCC Automotive Electrical School

## Dopewars

    Indo para Brooklyn
    A moça próxima a você no metrô lhe diz,
     "Drogas podem ser suas amigas!"
    Indo para Bronx
    Você escuta alguém tocando `Legalize Já` por Planet Hemp
    Viciados estão comprando Ópio a preços ridículos!

    -- Dopewars

## Cérebro

    Um médico britânico diz:

    "A medicina, em meu país, está tão avançada que nós podemos retirar o cérebro de um homem,
     colocá-lo em outro homem, e fazer com que, em seis semanas, ele já esteja procurando
     emprego."

    Um médico alemão diz:

    "Isto não é nada. Nós podemos retirar o cérebro de uma pessoa, colocá-lo em outra, e fazer com
     que, em quatro semanas, ela esteja se preparando para a guerra."

    O médico americano, para não ser superado, diz:

    "Vocês, meus caros, estão muito atrás. Nós, recentemente, retiramos um homem sem cérebro, do
     Texas, conseguimos colocá-lo na Casa Branca, e, agora, temos a metade do país procurando
     emprego e a outra metade se preparando para a guerra."

## Chocolate

    Chocolate de menta: escove os dentes e em seguida mastigue uma barra
    daquelas que são vendidas no trem.

## Randomly Generated Tagline

    Randomly Generated Tagline:
    "Any sufficiently perverted technology is indistinguishable from Perl."
                          - Unknown

## Brasil

    O Brasil é sério, mas é surrealista

    -- Jorge Amado

## Ovo e a galinha

    A galinha e apenas o meio que o ovo encontrou para produzir outro ovo.
      -- Samuel Butler

## Crimes

    A sociedade prepara os crimes e os indivíduos se limitam a executá-los.

    -- Queteler apud Bakunin, A Instrução Integral, p. 86.

## História

    "Às vezes você está vivendo um momento que entra para a história, mas está do lado errado."
     -- Mario "Macora" Castillo
        http://www1.folha.uol.com.br/esporte/folhanacopa/2014/07/1483578-selecao-que-levou-a-maior-goleada-das-copas-diz-que-brasil-foi-pior.shtml

## Machines

    Human beings can't keep track of the world any more, we have to leave it up to the machines.

    -- The Shockware Rider

## Provos

    a verdade é que os piores inimigos desta época são:
    os sujeitos que usam imagens programadas para chupar nossos
    olhos como se fossem ovos

    -- Lucebert, em Provos, da Coleção Bardena pág. 132

## Computer Science

    There are only two hard things in Computer Science: cache invalidation and naming things.

    -- Phil Karlton

## Caos

    Existe um grande caos abaixo do céu - a situação é excelente.

    --- Mao Tsé-Tung:

## Destruição

    O operário fez tudo, e o operário pode destruir tudo, porque pode fazer tudo de novo.

    -- Marx

## Desespero

    A situação desesperada da época em que vivo me enche de esperança.

    -- Marx em carta a Ruge

## Fracasso

    Fracassei em tudo o que tentei na vida.
    Tentei alfabetizar as crianças brasileiras, não consegui.
    Tente salvar os índios, não consegui.
    Tentei fazer uma universidade séria e fracassei.
    Tentei fazer o Brasil desenvolver-se autonomamente e fracassei.
    Mas os fracassos são minhas vitórias.
    Eu detestaria estar no lugar de quem me venceu.

    -- Darcy Ribeiro

## Meta

    Sempre permaneça no metanível. Sempre há um metanível acima do qual você se
    encontra. Nunca se coloque numa situação na qual você não possa se suicidar.
    Ande sempre com sua pílula de cicuta.

    --- logoutman

## Jogo da Forca

    Give me six lines written by the most honest man in the world, and I will find enough in them to hang him.

    -- https://en.wikiquote.org/wiki/Cardinal_Richelieu

    Corolário do Araponga:

    Talvez menos linhas sejam necessárias para condenar alguém. Talvez apenas com a
    citação acima já seria possível condenar o pobre Cardeal Richelieu.

    O acúmulo de dados pela vigilância de massa compromete qualquer pessoa em
    crimes previstos num entulho jurídico acumulado ao longo de centenas de anos.

## Somos agentes duplos, títeres de qual jogo doentio?

    It's the oldest question of all, George. Who can spy on the spies?

    -- Tinker, Tailor, Soldier, Spy

## Liberdade

    "My Brain is the key that sets me free."

    -- Houdini

## Razão e progresso

    The reasonable man adapts himself to the world; the unreasonable one
    persists in trying to adapt the world to himself.  Therefore all progress
    depends on the unreasonable man.
                    -- George Bernard Shaw

### Desatualização

    Tudo se desatualiza à velocidade da luz. Inclusive a luz.

### Tautologia da técnica

    Toda a tecnologia deve ser substituível por materiais disponíveis no presídio
    (fazemos o que podemos com o que temos).

### Confiança

    Um computador confiável é uma região do espaço-tempo à qual foi atribuído
    um voto de confiança.

### Poesia

    O que é bom para o lixo é bom para a poesia

    -- Manoel de Barros, Matéria de Poesia, in Poesias Completas, pág. 137
