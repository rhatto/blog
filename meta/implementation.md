[[!meta title="Implementation"]]
[[!meta date="2010-01-01 12:00:00-0300"]]

### Technology

* This is a [statically-generated website](/research/suckless/sites).
* That tries to implement [IndieWebCamp principles](http://indiewebcamp.com/principles).
<!--
* Using the same tags from [Fluxo de Links](https://links.fluxo.info) so
  content can be archived and referenced.
-->

### Workflow

* Branch `develop` receives commits and is pushed regularly in the repository.
* Only `master` branch is compiled and published in the website.
