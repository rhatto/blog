[[!meta title="Language"]]
[[!meta date="2010-01-01 12:00:00-0300"]]

# Note to english speakers

This blog is bi-lingual (brazilian portuguese and international english), but
without an attempt to isolate each language.

# Nota ao público lusófono

Este blog é bilíngue (português brasileiro e inglês internacional), porém sem
tentativa de isolamento entre idiomas:

1. Conteúdo em português é voltado especificamente ao público brasileiro e
   lusófono, em geral ligado a temas de interesse específico ou à atuação
   local.
2. Conteúdo em ingês é voltado ao público geral e ligado a temas da computação,
   desenvolvimento de sistemas, etc.
3. Nomenclatura de pastas/seções em inglês, por convenção.
