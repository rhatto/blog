[[!meta title="Tarefas"]]
[[!meta date="2010-01-01 12:00:00-0300"]]

# A fazer

* Meta: dividir conteúdos em subpáginas.
* Renomear ramo principal do repositório para `main`.
* Arrumar datas de criação (`date`) e atualização (`updated`) das páginas,
  usando a diretiva [meta][].
* Uniformizar estrutura dos documentos.
* Corrigir Markdown.
* Limpeza geral, retirando/movendo conteúdos como fichamentos e coisas
  inacabadas/pela metade.
* Adicionar tag(s) para idioma(s) em cada página; e opcionalmente índice de
  páginas por idioma.
* Descomentar o logotipo da página inicial (pode necessitar de uma fonte
  especial e/ou de largura fixa para exibição).
* Tema noturno.

[meta]: https://ikiwiki.info/ikiwiki/directive/meta/
