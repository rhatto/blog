[[!meta title="Keys"]]
[[!meta date="2016-04-24 19:09:03-0300"]]
[[!meta updated="2016-04-24 19:09:03-0300"]]

* My main contacts are defined by my current [OpenPGP][] public key.
* Please bring your fingerprint when meeting me :)

[OpenPGP]: https://plano.autodefesa.org/specs/chaves.html

## Current contact key

* [0x4FA73DE89ADE75998AC24E97B8C1D523FE7AAA84](4FA73DE89ADE75998AC24E97B8C1D523FE7AAA84).

## Previous contact keys

* [0x66CA01CE2BF2C9B7E8D64E340546823964E39FCA](66CA01CE2BF2C9B7E8D64E340546823964E39FCA).
* [0x2FA6AB9C2B7C48554C0BDB5BBD2CB59C6B566777](https://pgp.mit.edu/pks/lookup?search=0x2FA6AB9C2B7C48554C0BDB5BBD2CB59C6B566777&op=index).

## Contact keys and statements

[[!map pages="page(keys*)" show="title"]]
