[[!meta title="Nerven"]]

[[!img 2017-05-01-23:28:46_1280x800.png link="no"]]

Nevralgia, alucinação e paranóia num colapso psíquico e convulsão social numa civilização se
arruinando. Todos os personagens se enrolam.

Quando o avanço da própria civilização engenda desordem tal que os mecanismos de controle
cerebrais e sociais entram em falha.

[[!img 2017-05-01-23:22:30_1280x800.png link="no"]]

Filmado durante a República de Weimar, "Nervos" é um dos filmes citados no documentário 
[From Caligari to Hitler: German Cinema in the Age of the Masses](http://www.imdb.com/title/tt3908344/).

[Versão com cartelas em alemão](https://www.youtube.com/watch?v=R3I5X19r1xs).

## Cartelas com timecode

Industrial:

    53
    00:08:38,640 --> 00:08:44,112
    "Our invention is the greatest
    since time immemorial
    
    54
    00:08:44,400 --> 00:08:48,552
    and will make us masters of the world!"
    
    55
    00:08:52,720 --> 00:08:55,837
    "We will conquer the world
    
    56
    00:08:56,120 --> 00:09:03,549
    with our machines and tools,
    designed to break all resistance!
    
    57
    00:09:03,840 --> 00:09:07,799
    Do you hear me? The whole world!"
    
    58
    00:09:12,720 --> 00:09:23,073
    "The flag will be the symbol
    of our domination over the world."
    
    59
    00:09:27,000 --> 00:09:32,791
    "Start the machine! Raise the flag!"

Narrador:
    
    60
    00:09:39,320 --> 00:09:43,996
    The exploding machine
    destroys the newly opened factory
    
    [...]

Psiquiatra:
    
    231
    01:06:10,640 --> 01:06:21,198
    "These people here all appear healthy.
    They are, however, seriously ill."
    
    232
    01:06:41,400 --> 01:06:45,473
    "And the cause?"
    
    233
    01:06:48,240 --> 01:06:55,874
    "The progression of civilization;
    the struggle for existence;
    
    234
    01:06:56,160 --> 01:07:03,555
    anxiety and the terrors of war;
    the sins of the parents..."
    
    [...]

Industrial:
    
    265
    01:15:33,520 --> 01:15:46,479
    "My own nerves
    mirror the nerves of the world."
    
    266
    01:15:51,640 --> 01:15:55,599
    "And the world's nerves are ill!"

## Spoiler

... e a saída pós-apocalíptica é o amor e o trabalho duro num paraíso idílico.
