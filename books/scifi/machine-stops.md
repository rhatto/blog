[[!meta title="The Machine Stops"]]

[THE MACHINE STOPS ... E.M. Forster](http://archive.ncsa.illinois.edu/prajlich/forster.html):

    And of course she had studied the civilization that had immediately preceded
    her own - the civilization that had mistaken the functions of the system, and
    had used it for bringing people to things, instead of for bringing things to
    people. Those funny old days, when men went for change of air instead of
    changing the air in their rooms!

    [...]

    Few travelled in these days, for, thanks to the advance of science, the earth
    was exactly alike all over.

    [...]

    What was the good of going to Peking when it was just like Shrewsbury? Why
    return to Shrewsbury when it would all be like Peking? Men seldom moved their
    bodies; all unrest was concentrated in the soul.

    [...]

    Beneath those corridors of shining tiles were rooms, tier below tier, reaching
    far into the earth, and in each room there sat a human being, eating, or
    sleeping, or producing ideas. And buried deep in the hive was her own room.

    [...]

    But the Committee of the Mending Apparatus now came forward, and allayed the
    panic with well-chosen words. It confessed that the Mending Apparatus was
    itself in need of repair.

    [...]

    There were gradations of terror- at times came rumours of hope-the Mending
    Apparatus was almost mended-the enemies of the Machine had been got under- new
    "nerve-centres" were evolving which would do the work even more magnificently
    than before.

    [...]

    "I have seen them, spoken to them, loved them. They are hiding in the midst and
    the ferns until our civilization stops. Today they are the Homeless - tomorrow
    ------ "
