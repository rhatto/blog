[[!meta title="Four Futures: Life After Capitalism"]]

* Author: Peter Frase
* Year: 2016
* Publisher: Verso / Jacobin

## Trechos

    Fictional futures are, in my view, preferable to those works of
    “futurism” that attempt to directly predict the future, obscuring
    its inherent uncertainty and contingency and thereby stultifying
    the reader. Within the areas discussed in this book, a
    paradigmatic futurist would be someone like Ray Kurzweil, who
    confidently predicts that by 2049, computers will have achieved
    humanlike intelligence, with all manner of world-changing consequences.
    24  Such prognostications generally end up unconvincing as prophecy
    and unsatisfying as fiction. Science fiction is to futurism what
    social theory is to conspiracy theory: an altogether richer, more
    honest, and more humble enterprise. Or to put it another way, it
    is always more interesting to read an account that derives the general
    from the particular (social theory) or the particular from the general
    (science fiction), rather than attempting to go from the general
    to the general (futurism) or the particular to the particular
    (conspiracism).

    -- 16

               Abundance   Scarcity
    Equality   communism   socialism
    Hierarchy  rentism     exterminism

    Exercises like this aren’t unprecedented. A similar typology can be
    found in a 1999 article by Robert Costanza in The Futurist. 26
    There are four scenarios: Star Trek, Big Government, Ecotopia,
    and Mad Max. For Costanza, however, the two axes are “world view
    and policies” and “the real state of the world.” Thus the four
    boxes are filled in according to whether human ideological
    predilections match reality: in the “Big Government” scenario, for
    example, progress is restrained by safety standards because the
    “technological skeptics” deny the reality of unlimited resources. My
    contribution to this debate is to emphasize the significance of
    capitalism and politics.

    [...]

    So for me, sketching out multiple futures is an attempt to
    leave a place for the political and the contingent. My
    intention is not to claim that one future will automatically
    appear through the magical working out of technical and ecological
    factors that appear from outside. Instead, it is to insist that where
    we end up will be a result of political struggle. The intersection of
    science fiction and politics is these days often associated with the
    libertarian right and its deterministic techno-utopian fantasies; I
    hope to reclaim the long left-wing tradition of mixing imaginative
    speculation with political economy. The starting point of the entire
    analysis is that capitalism is going to end, and that, as Luxemburg
    said,

    -- 17

    Kurt Vonnegut’s first novel, Player Piano, describes a society that
    seems, on the surface, like a postlabor utopia, where machines have
    liberated humans from toil. For Vonnegut, however, this isn’t a utopia at
    all. He describes a future where production is almost entirely carried
    out by machines, overseen by a small technocratic elite. Everyone else
    is essentially superfluous from an economic perspective, but the society
    is rich enough to provide a comfortable life for all of them. Vonnegut
    refers to this condition as a “second childhood” at one point,
    and he views it not as an achievement but as a horror. For him, and
    for the main protagonists in the novel, the main danger of an automated
    society is that it deprives life of all meaning and dignity. If
    most people are not engaged directly in producing the necessities
    of life, he seems to think, they will inevitably fall into torpor
    and despair.

    -- 19

    The French sociologist Bruno Latour has made the same observation through his
    reading of Mary Shelley’s seminal science fiction tale, Frankenstein. This
    story is not, he observes, the warning against technology and humanity’s hubris
    that it is so often made out to be. 13 The real sin of Frankenstein (which is
    the name of the scientist and not the monster) was not in making his creation
    but in abandoning it to the wilderness rather than loving and caring for it.
    This, for Latour, is a parable about our relationship to technology and
    ecology. When the technologies that we have created end up having unforeseen
    and terrifying consequences—global warming, pollution, extinctions—we recoil in
    horror from them. Yet we cannot, nor should we, abandon nature now. We have no
    choice but to become ever more involved in consciously changing nature. We have
    no choice but to love the monster we have made, lest it turn on us and destroy
    us. This, says Latour, “demands more of us than simply embracing technology and
    innovation”; it requires a perspective that “sees the process of human
    development as neither liberation from Nature nor as a fall from it, but rather
    as a process of becoming ever-more attached to, and intimate with, a panoply of
    nonhuman natures.” 14

    -- 43-44

    But short of that, there are ways to turn some of the predatory “sharing
    economy” businesses into something a bit more egalitarian. Economics writer
    Mike Konczal, for instance, has suggested a plan to “socialize Uber.” 26  He
    notes that since the company’s workers already own most of the capital—their
    cars—it would be relatively easy for a worker cooperative to set up an online
    platform that works like the Uber app but is controlled by the workers
    themselves rather than a handful of Silicon Valley capitalists.

    -- 48

    The sociologist Bryan Turner has argued that we live in an “enclave society.” 8
    Despite the myth of increasing mobility under globalization, we in fact inhabit
    an order in which “governments and other agencies seek to regulate spaces and,
    where necessary, to immobilize flows of people, goods and services” by means of
    “enclosure, bureaucratic barriers, legal exclusions and registrations.” 9 Of
    course, it is the movements of the masses whose movements are restricted, while
    the elite remains cosmopolitan and mobile. Some of the examples Turner adduces
    are relatively trivial, like frequent-flyer lounges and private rooms in public
    hospitals. Others are more serious, like gated communities (or, in the more
    extreme case, private islands) for the rich, and ghettos for the poor—where
    police are responsible for keeping poor people out of the “wrong”
    neighborhoods. Biological quarantines and immigration restrictions take the
    enclave concept to the level of the nation-state. In all cases, the prison
    looms as the ultimate dystopian enclave for those who do not comply, whether it
    is the federal penitentiary or the detention camp at Guantanamo Bay. Gated
    communities, private islands, ghettos, prisons, terrorism paranoia, biological
    quarantines—these amount to an inverted global gulag, where the rich live in
    tiny islands of wealth strewn around an ocean of misery.

    [...]

    Silicon Valley is a hotbed of such sentiments, plutocrats talking openly about
    “secession.” In one widely disseminated speech, Balaji Srinivasan, the
    cofounder of a San Francisco genetics company, told an audience of start-up
    entrepreneurs that “we need to build opt-in society, outside the US, run by
    technology.” 12  For now, that reflects hubris and ignorance of the myriad ways
    someone like him is supported by the workers who make his life possible.

    -- 53

    Remember exterminism’s central problematic: abundance and freedom from work are
    possible for a minority, but material limits make it impossible to extend that
    same way of life to everyone. At the same time, automation has rendered masses
    of workers superfluous. The result is a society of surveillance, repression,
    and incarceration, always threatening to tip over into one of outright
    genocide.

    But suppose we stare into that abyss? What’s left when the “excess” bodies have
    been disposed of repression, and incarceration, always threatening to tip over
    into one of outright genocide.  But suppose we stare into that abyss? What’s
    left when the “excess” bodies have been disposed of and the rich are finally
    left alone with their robots and their walled compounds? The combat drones and
    robot executioners could be decommissioned, the apparatus of surveillance
    gradually dismantled, and the remaining population could evolve past its brutal
    and dehumanizing war morality and settle into a life of equality and
    abundance—in other words, into communism.

    As a descendant of Europeans in the United States, I have an idea of what that
    might be like. After all, I’m the beneficiary of a genocide.

    My society was founded on the systematic extermination of the North American
    continent’s original inhabitants. Today, the surviving descendants of those
    earliest Americans are sufficiently impoverished, small in number, and
    geographically isolated that most Americans can easily ignore them as they go
    about their lives. Occasionally the survivors force themselves onto our
    attention. But mostly, while we may lament the brutality of our ancestors, we
    don’t contemplate giving up our prosperous lives or our land.  Just as Marcuse
    said, nobody ever gave a damn about the victims of history.  Zooming out a bit
    farther, then, the point is that we don’t necessarily pick one of the four
    futures: we could get them all, and there are paths that lead from each one to
    all of the others.

    We have seen how exterminism becomes communism. Communism, in turn, is always
    subject to counterrevolution, if someone can find a way to reintroduce
    artificial scarcity and create a new rentist elite. Socialism is subject to
    this pressure even more severely, since the greater level of shared material
    hardship increases the impetus for some group to set itself up as the
    privileged elite and turn the system into an exterminist one.

    But short of a civilizational collapse so complete that it cuts us off from our
    accumulated knowledge and plunges us into a new dark ages, it’s hard to see a
    road that leads back to industrial capitalism as we have known it. That is the
    other important point of this book. We can’t go back to the past, and we can’t
    even hold on to what we have now. Something new is coming—and indeed, in some
    way, all four futures are already here, “unevenly distributed,” in William
    Gibson’s phrase. It’s up to us to build the collective power to fight for the
    futures we want.

    -- 63-64
