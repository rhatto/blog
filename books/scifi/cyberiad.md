[[!meta title="The Cyberiad"]]

* Author: Stanislaw Lem.
* Info: [The Cyberiad - Wikipedia](https://en.wikipedia.org/wiki/The_Cyberiad).

## Trechos

    Come, let us hasten to a higher plane,
    Where dyads tread the fairy fields of Venn,
    Their indices bedecked from one to n,
    Commingled in an endless Markov chain!

    Come, every frustum longs to be a cone,
    And every vector dreams of matrices.
    Hark to the gentle gradient of the breeze:
    It whispers of a more ergodic zone.

    In Riemann, Hilbert or in Banach space
    Let superscripts and subscripts go their ways.
    Our asymptotes no longer out of phase,
    We shall encounter, counting, face to face.
                    -- The Cyberiad
