[[!meta title="Livros - Tecnologia"]]

    Os manifestos <aceleracionistas> insitem sobre as virtudes da aceleração
    tecnológica sem dizer palavra sobre as condições materiais- energéticas,
    ambientais, geopolíticas, etc- de tal processo, que conduzira
    "automaticamente" pensam os autores, à redução da jornada de trabalho(
    Também em Bangladesh?quando?), ao aumento das horas de lazer (a sociedade
    do espetáculo sai do armário!), à renda universal etc: ...

    A outra aceleração, a saber, aquela que diz respeito aos processod de
    ultrapassagem dos valores criticos dos parâmentros ambientais- quando
    chegaremos a +4C, que talves sejam + 6C ou + 8C? quando se dara o fim dos
    estoques de pesca? .... essa outra recebe no máximo uma menção que beira a
    frivolidade, para não dizermos um puro e simples negacionismo...

    -- O mundo em suspenso, de Viveiros e Debora Danowski, pág 150

[[!inline pages="page(books/technology*)" archive="yes"]]
