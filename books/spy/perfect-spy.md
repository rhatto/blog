[[!meta title="A Perfect Spy"]]

* Author: John Le Carré

## Trechos

---

    A society that admires its shock troops had batter be bloody careful about where it's going.
      -- A Perfect Spy

---

    Intelligence is nothing if not an institutionalised black market in perishable commodities.
      -- A Perfect Spy

---

    Military intelligence has about as much to do with intelligence as military music has to do
    with music.
      -- A Perfect Spy

---

    When two people have decided to go to bed with each other, what passes between them before
    the event is a matter of form rather than of content.
      -- A Perfect Spy

---

    Hell, Jack, we're licensed crooks, that's all I'm saying. What's our racket? Know what our racket is?
    It is to place our larcenous natures at the service of the state. So I mean why should I feel different
    about Magnus just because maybe he go the mix a little wrong? I can't.
      -- A Perfect Spy

---

    "Stop this now. There's not a man or woman in this room who won't look like a traitor once you start
    to pull our life stories inside out. A man can't remember where he was on the night of the tenth?
    Then he's lying. He can remember? Then he's too damn flip with his alibi. You go one more yard with this
    and everyone who tells the truth will become a barefaced liar, everyone who does a decent job will
    be working fir the other side. You carry on like this and you'll sink our service better than the
    Russians ever could. Or is that what you want?

    -- A Perfect Spy

---

    If bogies ask, it's because they don't know. So don't tell them. If they ask and they do know,
    they're trying to catch you out. So don't tell'em either.
    
    -- A Perfect Spy
