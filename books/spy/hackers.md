[[!meta title="Os aspectos políticos do hacking"]]

Coletânea de textos sobre hacking.

    Lines in the Sand: Which Side Are You On in the Hacker Class War
    http://www.phrack.org/issues.html?issue=68&id=16#article
  
    What's wrong with the kids these days? | PUSCII blog
    http://www.puscii.nl/blog/content/whats-wrong-kids-these-days
  
    Kid's tomorrow | PUSCII blog
    http://www.puscii.nl/blog/content/kids-tomorrow
  
    Hacklabs and hackerspaces – tracing two genealogies » Journal of Peer Production
    http://peerproduction.net/issues/issue-2/peer-reviewed-papers/hacklabs-and-hackerspaces/
  
    CMI Brasil - Precisamos falar sobre o Facebook
    http://www.midiaindependente.org/pt/blue/2012/11/514157.shtml
  
    Saudi Surveillance
    http://thoughtcrime.org/blog/saudi-surveillance/
  
    The Banality of ‘Don’t Be Evil’ by Julian Assange - NYTimes.com
    https://www.nytimes.com/2013/06/02/opinion/sunday/the-banality-of-googles-dont-be-evil.html?_r=0
  
    Hacking at the crossroad: US military funding of hackerspaces » Journal of Peer Production
    http://peerproduction.net/issues/issue-2/invited-comments/hacking-at-the-crossroad/

Análise
-------

A distinção entre black hats, gray hats e white hats é artificial e foi criada pela indústria.

- Os hackers originais estavam atrás era de recursos! Era o movimento dos sem-computador.
- Hoje, há uma cortina de fumaça infosec. Precisamos tomar consciência que ainda não conquistamos
  nossos recursos.
