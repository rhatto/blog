[[!meta title="The Hacker Crackdown"]]

Seleção de trechos [deste livro](http://www.gutenberg.org/ebooks/101):

    4154 When rumor about LoD's mastery of Georgia's switching network got
    4155 around to BellSouth through Bellcore and telco security scuttlebutt,
    4156 they at first refused to believe it.  If you paid serious attention to
    4157 every rumor out and about these hacker kids, you would hear all kinds
    4158 of wacko saucer-nut nonsense:  that the National Security Agency
    4159 monitored all American phone calls, that the CIA and DEA tracked
    4160 traffic on bulletin-boards with word-analysis programs, that the Condor
    4161 could start World War III from a payphone.

Jocoso, mas premonitório! Mais:

    11658 Kapor is a man with a vision.  It's a very novel vision which he and
    11659 his allies are working out in considerable detail and with great
    11660 energy.  Dark, cynical, morbid cyberpunk that I am, I cannot avoid
    11661 considering some of the darker implications of "decentralized,
    11662 nonhierarchical, locally empowered" networking.
    11663 
    11664 I remark that some pundits have suggested that electronic
    11665 networking--faxes, phones, small-scale photocopiers--played a strong
    11666 role in dissolving the power of centralized communism and causing the
    11667 collapse of the Warsaw Pact.
    11668 
    11669 Socialism is totally discredited, says Kapor, fresh back from the
    11670 Eastern Bloc.  The idea that faxes did it, all by themselves, is rather
    11671 wishful thinking.
    11672 
    11673 Has it occurred to him that electronic networking might corrode
    11674 America's industrial and political infrastructure to the point where
    11675 the whole thing becomes untenable, unworkable--and the old order just
    11676 collapses headlong, like in Eastern Europe?
    11677 
    11678 "No," Kapor says flatly.  "I think that's extraordinarily unlikely.  In
    11679 part, because ten or fifteen years ago, I had similar hopes about
    11680 personal computers--which utterly failed to materialize." He grins
    11681 wryly, then his eyes narrow. "I'm VERY opposed to techno-utopias.
    11682 Every time I see one, I either run away, or try to kill it."
    11683 
    11684 It dawns on me then that Mitch Kapor is not trying to make the world
    11685 safe for democracy.  He certainly is not trying to make it safe for
    11686 anarchists or utopians--least of all for computer intruders or
    11687 electronic rip-off artists.  What he really hopes to do is make the
    11688 world safe for future Mitch Kapors.  This world of decentralized,
    11689 small-scale nodes, with instant global access for the best and
    11690 brightest, would be a perfect milieu for the shoestring attic
    11691 capitalism that made Mitch Kapor what he is today.
