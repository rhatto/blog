[[!meta title="Puzzle Palace"]]

* Author: James Bamford

## Excerpts

    For reasons of security, as well as the fact that the State De-
    partment's portion of the budget could not by law be spent within
    the District of Columbia, Yardley set up shop in New York City.
    After first considering a building at 17 East 36 Street, he finally
    settled on a stately four-story brownstone at 3 East 38 Street,
    owned by an old friend.

    --- page 13 (pdf) e 25 (book)

    The key to the legislation could have been dreamed up by Franz Kafka: the
    establishment of a supersecret federal court.  Sealed away behind a
    cipher-locked door in a windowless room on the top floor of the Justice
    Department building, the Foreign Intelligence Surveillance Court is most
    certainly the strangest creation in the history of the federal Judiciary. Its
    establishment was the product of compromises between legislators who wanted the
    NSA and FBI, the only agencies affected by the FISA, to follow the standard
    procedure of obtaining a court order re- quired in criminal investigations, and
    legislators who felt the agencies should have no regulation whatsoever in their
    foreign intelligence surveillances.
    
    [...]
    
    Almost unheard of outside the inner sanctum of the intelligence
    establishment, the court is like no other. It sits in secret session, holds no
    adversary hearings, and issues almost no public opinions or reports. It is
    listed in neither the Government Organization Manual nor the United States
    Court Directory and has even tried to keep its precise location a secret. "On
    its face," said one legal authority familiar with the court, "it is an affront
    to the traditional American concept of justice."

    -- page 453

    [...]

    Then there is the last, and possibly most intriguing, part of the definition,
    which stipulates that NSA has not "acquired" anything until the communication
    has been processed "into an intelligible form intended for human inspection."
    NSA is there- fore free to intercept all communications, domestic as well as
    foreign, without ever coming under the law. Only when it selects the "contents"
    of a particular communication for further "proc- essing" does the FISA take
    effect.

    -- page 458

    Like most things in Britain, the practice of eavesdropping is
    deeply rooted in tradition and probably dates back at least to
    1653. In that year Lord Thurloe created what was known as
    "The Secret Office," which specialized in clandestinely opening
    and copying international correspondence. That custom later
    carried over to telegrams and finally the telephone shortly after
    it was first introduced in England in 1879.

    -- page 487

