[[!meta title="Sobre a brevidade da vida"]]

* [Seneca On The Shortness Of Life](https://archive.org/details/SenecaOnTheShortnessOfLife).
* [Sobre a Brevidade da Vida - Sêneca](http://lelivros.love/book/baixar-livro-sobre-a-brevidade-da-vida-seneca-em-pdf-epub-e-mobi-ou-ler-online/).

## Trechos

    A vida é suficientemente longa e com generosidade nos foi dada, para a
    realização das maiores coisas, se a empregamos bem. Mas, quando ela se esvai no
    luxo e na indiferença, quando não a empregamos em nada de bom, então,
    finalmente constrangidos pela fatalidade, sentimos que ela já passou (4) por
    nós sem que tivéssemos percebido. O fato é o seguinte: não recebemos uma vida
    breve, mas a fazemos, nem somos dela carentes, mas esbanjadores. Tal como
    abundantes e régios recursos, quando caem nas mãos de um mau senhor,
    dissipam-se num momento, enquanto que, por pequenos que sejam, se são confiados
    a um bom guarda, crescem pelo uso, assim também nossa vida se estende por muito
    tempo, para aquele que sabe dela bem dispor.

    [...]

    Por que nos queixamos da Natureza? Ela mostrou-se benevolente: a vida, se
    souberes utilizá-la, é longa. Mas uma avareza insaciável apossa-se de, um de
    outro, uma laboriosa dedicação a atividades inúteis, um embriaga-se de vinho,
    outro entorpece-se na inatividade; a este, uma ambição sempre dependente das
    opiniões alheias o esgota, um incontido desejo de comerciar leva aquele a
    percorrer todas as terras e todos os mares, na esperança de lucro; a paixão
    pelos assuntos militares atormenta alguns, sempre preocupados com perigos
    alheios ou inquietos com seus próprios; há os que, por uma servidão voluntária,
    se desgastam numa ingrata solicitude a seus superiores; (2) a busca da beleza
    de um outro ou o cuidado com sua própria ocupa a muitos; a maioria, que não
    persegue nenhum objetivo fixo, é atirada a novos desígnios por uma vaga e
    inconstante leviandade, desgostando-se com isso; alguns não definiram para onde
    dirigir sua vida, e o destino surpreende-os esgotados e bocejantes, de tal
    forma que não duvido ser verdadeiro o que disse, à maneira de oráculo, o maior
    dos poetas: “Pequena é a parte da vida que (3) vivemos.” Pois todo o restante
    não é vida, mas tempo. 

    [...]

    Finalmente, todos concordam que um homem ocupado não pode fazer nada bem: não
    pode se dedicar à eloqüência, nem aos estudos liberais, uma vez que seu
    espírito, ocupado em coisas diversas, não se aprofunda em nada, mas, pelo
    contrário, tudo rejeita, pensando que tudo lhe é imposto. Nada é menos próprio
    do homem ocupado do que viver, pois não há outra coisa que seja mais difícil de
    aprender. Professores das outras artes, há vários e por toda parte, dentre
    algumas dessas, vemos crianças terem atingido tanta maestria, que chegam até a
    ensiná-las. Deve-se aprender a viver por toda a vida, e, por mais que tu talvez
    te espantes, a vida (4) toda é um aprender a morrer. Muitos dos maiores homens,
    tendo afastado todos os obstáculos e renunciado às riquezas, a seus negócios e
    aos prazeres, empregaram até o último de seus dias para aprender a viver,
    contudo muitos deles deixaram a vida tendo confessado ainda não sabê-lo – e
    muito menos ainda (5) o sabem os que mencionei acima.

    [...]

    Cada um faz precipitar sua vida e (9) padece da ânsia do futuro e de tédio
    do presente.

    [...]

    Portanto não há por que pensar que alguém tenha vivido muito, por causa de suas
    rugas ou cabelos brancos: ele não viveu por muito tempo, simplesmente foi por
    muito tempo. Pensarias ter navegado muito, aquele que, tendo se afastado do
    porto, foi apanhado por violenta tempestade, errou para cá e para lá e ficou a
    dar voltas, conforme a mudança dos ventos e o capricho dos furacões, sem
    contudo sair do lugar? Ele não navegou muito, mas foi muito acossado.

    [...]

    Os homens recebem pensões e aluguéis com muito prazer e concentram neles suas
    preocupações, esforços e cuidados, mas ninguém dá valor ao tempo; usa-se dele a
    rédeas soltas, como se nada custasse. Porém, quando doentes, se estão próximos
    do perigo de morte, prostram-se aos joelhos dos médicos; ou, se temem a pena
    capital, estão prontos a gastar todos os seus bens para viver.
    Tamanha é a discórdia de seus (3) sentimentos! Se fosse possível apresentar a
    cada um a conta dos anos futuros, da mesma forma que podemos fazer com os
    passados, como tremeriam aqueles que vissem restar-lhes poucos anos e como os
    poupariam! Pois, se é fácil administrar o que, embora curto, é certo, deve-se
    conservar com muito cuidado o que não se pode saber (4) quando há de acabar.
    Contudo não há por que pensar que eles ignoram que coisa preciosa é o tempo:
    costumam dizer aos que amam muitíssimo que estão dispostos a lhes dar parte de
    seus dias. E realmente dão, sem se aperceberem disto, mas dão de forma a
    subtraírem vários anos a si, sem aumentar os daqueles. Mas ignoram o fato mesmo
    de estarem perdendo seus anos, por isso lhes é tolerável a perda de um bem que
    não é (5) notado. Ninguém devolverá teus anos, ninguém te fará voltar a ti
    mesmo. Uma vez principiada, a vida segue seu curso e não reverterá nem o
    interromperá, não se elevará, não te avisará de sua velocidade. Transcorrerá
    silenciosamente, não se prolongará por ordem de um rei, nem pelo apoio do povo.
    Correrá tal como foi impulsionada no primeiro dia, nunca desviará seu curso,
    nem o retardará. Que sucederá? Tu estás ocupado, e a vida se apressa; por sua
    vez virá a morte, à qual deverás te entregar, queiras ou não.

    9 – 1: Pode haver algo mais estúpido que o modo de ver de alguns – falo
    daqueles que deixam de lado a prudência. Ocupam-se para poder viver melhor:
    armazenam a vida, gastando-a! Fazem seus planos a longo prazo; no entanto
    protelar é do maior prejuízo para a vida: arrebata-nos cada dia que se oferece
    a nós, rouba-nos o presente ao prometer o futuro. O maior impedimento para
    viver é a expectativa, a qual tende para o amanhã e faz perder o momento
    presente. Do que está nas mãos da Fortuna, dispões; o que está nas tuas,
    despedes. Para onde ficas a olhar? Para que tendes? Tudo que está por vir se
    assenta na incerteza: desde (2) já, vive!

    [...]

    O tempo presente é brevíssimo, tanto que a alguns parece não existir, pois está
    sempre em movimento; flui e precipita-se; deixa de ser antes de vir a ser; é
    tão incapaz de deter-se, quanto o mundo ou as estrelas, cujo infatigável
    movimento não lhes permite permanecer no mesmo lugar. Pertence, pois, aos
    ocupados, apenas o tempo presente, que é tão breve que não pode ser abarcado; e
    este mesmo escapa-lhes, ocupados que estão em muitas coisas.

    [...]

    Dentre todos os homens, somente são ociosos os que estão disponíveis para a
    sabedoria; eles são os únicos a viver, pois, não apenas administram bem sua
    vida, mas acrescentam-lhe toda a eternidade. Todos os anos que se passaram
    antes deles são somados aos seus.

    [...]

    Nenhum destes [filósofos] forçará tua morte, todos te ensinarão a morrer,
    nenhum dissipará teus anos, mas te oferecerá os seus. Nunca a conversação com
    eles será perigosa, fatal a amizade ou onerosa a deferência. Conseguirás deles
    tudo o que quiseres: não será deles a culpa (2) se não tiveres exaurido tudo o
    que desejas. Que felicidade, que bela velhice não aguarda o que se dispôs a ser
    seu cliente! Ele terá com quem discutir sobre as menores, bem como sobre as
    maiores, questões, a quem consultar diariamente sobre si mesmo, de quem ouvir a
    verdade sem ofensa e ser louvado sem adulação, a cuja (3) semelhança se possa
    moldar. Costumamos dizer que não está em nosso poder escolher os pais que a
    sorte nos destinou, mas que nos foram dados ao acaso; contudo é nos permitido
    ter um nascimento segundo a nossa escolha. Existem famílias dos mais nobres
    espíritos: escolhe a qual delas queres pertencer, e receberás não apenas seu
    nome, mas também seus próprios bens, que não terás de vigiar miserável e
    mesquinhamente, pois, quanto mais forem partilhados pelos homens, maiores (4)
    se tornarão. Estes te darão o acesso à eternidade, te elevarão àquelas alturas
    de onde ninguém se precipita. Esta é a única maneira de prolongara existência
    mortal e, até mais, de convertê-la em imortalidade. As dignidades, os
    monumentos, tudo o que a ambição impôs por decretos, ou construiu com o suor,
    depressa há de cair em ruínas: não há nada que a longa passagem dos anos não
    destrua ou desordene. Mas ela não pode tocar nos conhecimentos que a sabedoria
    consagrou, nenhuma idade os destruirá ou diminuirá, a seguinte e as sucessivas
    sempre hão de aumentá-los ainda mais: pois a inveja tem olhos apenas para o que
    está próximo de si, e admiramos com menos malícia o que está (5) distante.
    Portanto a vida do filósofo estende-se por muito tempo, e ele não está
    confinado nos mesmos limites que os outros. É o único a não depender das leis
    do gênero humano: todos os séculos servem-no como a um deus.

    [...]

    É extremamente breve e agitada a vida dos que esquecem o passado, negligenciam
    o presente e receiam o futuro; quando chegam ao termo de suas existências, os
    pobres coitados compreendem tardiamente que (2) estiveram por longo tempo
    ocupados em nada fazer.  [...]
    Não há ainda razão para se pensar que isto também seja uma prova de uma vida
    longa: – o fato de muitas vezes os dias lhes parecerem longos, ou porque se
    queixam de as horas custarem a passar até que chegue o momento do jantar; pois,
    se porventura as ocupações os abandonam, sentem-se desertados e inquietam-se
    mesmo no lazer, nem sabem como dispor dele ou matá-lo. Portanto anseiam por uma
    ocupação qualquer, e todo intervalo de tempo entre duas ocupações lhes é um
    fardo. E – por Hércules – tal é o que acontece quando se fixa a data dos
    combates de gladiadores, ou quando se aguarda o dia de um outro gênero qualquer
    de espetáculo ou divertimento: (4) desejam saltar os dias intermediários!

    [...]

    Em meio a grandes labutas, conseguem o que desejam e ansiosos conservam o que
    conseguiram; entretanto não têm consciência de que o tempo nunca mais há de
    voltar. Novas ocupações seguem-se às antigas; a esperança suscita esperança; a
    ambição, ambição.

    [...]

    Portanto, meu caro Paulino, aparta-te da multidão e, já bastante acossado pela
    duração de tua existência, não te afastes de um porto mais tranqüilo. Pensa
    quantas vagas já te acometeram, quantas tempestades, de uma parte, já
    suportaste na vida particular, quantas, de outra, suscitaste contra ti na vida
    pública. Teu valor já foi suficientemente testado, em fatigantes e atormentadas
    provas, o teu valor: tenta ver o que pode realizar no ócio. A maior parte de
    tua vida, e certamente a melhor, foi dada à República, toma (2) também para ti
    um pouco de teu tempo. Não te convoco a um retiro indolente e inativo, nem
    a afogar todo o teu vigoroso caráter no sono ou nos prazeres caros à multidão:
    isso não é estar em sossego. Encontrarás tarefas maiores que todas as que
    cumpriste devotadamente até aqui, as quais executarás no retiro e livre de (3)
    preocupações.

    [...]

    Recolhe-te a estas coisas mais tranqüilas, mais seguras, melhores! Acaso tu
    pensas serem o mesmo estas duas coisas: cuidar que o trigo seja transportado ao
    celeiro, intacto e a salvo da fraude ou negligência dos carregadores, que não
    se estrague pela fermentação, que esteja bem seco, que seu peso e medida
    confiram, e elevar-se às coisas sagradas e sublimes para conhecer qual é a
    substância de deus, seu prazer, sua condição, sua forma, que destino aguarda
    tua alma, que lugar a Natureza nos destina após nos separarmos do corpo, qual a
    razão por que ela mantém os corpos mais pesados no centro do universo, suspende
    os altos às regiões altas, eleva o fogo à mais alta, impele as estrelas às suas
    trajetórias e ainda outras coisas cheias de notáveis (2) maravilhas? Abandona o
    solo e volta-te a esses estudos! Agora, enquanto o sangue ferve, deve-se ir,
    com determinação, para o melhor.

    [...]

    Portanto, quando vires freqüentemente uma toga pretexta ou um nome célebre no
    fórum, não o invejes: essas coisas são adquiridas ao custo da vida. Para ligar
    seu nome a um único ano, consumirão todos os seus anos. A uns, a vida abandonou
    logo nas primeiras etapas, antes que tivessem atingido as alturas ambicionadas;
    a outros, após terem galgado o cume das honras através de mil desonestidades,
    sobrevém o triste pensamento: “ter trabalhado tanto por uma inscrição num
    túmulo!”
