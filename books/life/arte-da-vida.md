[[!meta title="Arte da vida"]]

* Autor: Zygmunt Bauman
* Ano: 2009
* Editora: Zahar

## Índice

* Classe média, ansiedade, obsessão, 64, 65.
* Utopia, distopia, novo homem, acidente, 131.
* Terceiro homem, 157.
* Reciprocidade, responsabilidade, 160.
* Organização, 163.
