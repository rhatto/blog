[[!meta title="A Arte de Viver para as Novas Gerações"]]

Sobre
-----

A Arte de Viver para as Novas Gerações, Raoul Vaneigem.

Versões
-------

* http://library.nothingness.org/articles/SI/en/pub_contents/5
* http://arikel.free.fr/aides/vaneigem/

Trechos
-------

    The path toward simplicity is the most complex of all, and here in particular
    it seemed best not to tear away from the commonplace the tangle of roots which
    enable us to transplant it into another region, where we can cultivate it to
    our own profit.

    -- http://library.nothingness.org/articles/SI/en/display/34

    Gostaria que um livro como este fosse acessível às cabeças menos acostumadas com a
    linguagem das idéias. Espero não ter fracassado totalmente. Desse caos, algum dia sairão
    fórmulas capazes de atingir à queima-roupa os nosso inimigos. Até lá, que as frases
    relidas aqui e ali tenham seus efeitos. A via para a simplicidade é a mais complexa de
    todas e, especialmente nesse caso, era conveniente não arrancar ao lugar-comum as
    múltiplas raízes que poderemos transplantar a outro terreno e cultivá-las em nosso
    benefício.

    -- 8

    A opção de viver é uma opção política. Não queremos um mundo no qual a garantia
    de não morrer de fome se troca pelo risco de morrer de tédio.

    -- 9

    O bom comportamento do prisioneiro depende da esperança de fugir que a prisão
    alimenta. Por outro lado, impelido contra uma parede sem saída, um homem apenas
    conhece a raiva de destruí-la ou de quebrar nela a cabeça, o que não deixa de
    ser lamentável para uma boa organização social (mesmo se o suicida não tiver a
    feliz idéia de se matar no estilo dos príncipes orientais, levando com ele
    todos os seus servos: juízes, bispos, generais, policiais, psiquiatras, filósofos,
    managers, especialistas e cibernéticos).

    -- 36

    Assim que o líder do jogo se torna um chefe, o princípio hierárquico se salva, e a revolução
    se detém para presidir o massacre dos revolucionários. É preciso lembrar sempre: o projeto
    insurrecional só pertence às massas, o líder reforça-o, o chefe o trai. É entre o líder e o
    chefe que inicialmente se desenrola a luta autêntica.

    -- 37

    Resumindo, a descompressão nada mais é do que o controle dos antagonismos pelo poder.
    A oposição de dois termos toma sentido pela introdução de um terceiro. Se só existem dois
    pólos, eles se neutralizam, uma vez que cada um se define pelos valores do outro. É
    impossível escolher entre eles, entra-se no domínio da tolerância e do relativismo, tão
    querido à burguesia. Como é compreensível o interesse da hierarquia apostólica romana na
    querela entre o maniqueísmo e o trinitarismo!

    -- 38

    O poder aquisitivo é a licença de aquisição do poder. O velho proletariado
    vendia a força de trabalho para subsistir; o seu escasso tempo livre era gasto
    mais ou menos de maneira agradável em discussões, conversas, nos bares, fazendo
    amor, caminhando em festas e motins. O novo proletariado vende a força de
    trabalho para consumir. Quando não busca no trabalho forçado uma promoção
    hierárquica, o trabalhador é convidado a comprar objetos (carro, gravata,
    cultura...) que lhe atribuirão o seu lugar na escala social.
    Esta é a era em que a ideologia do consumo se torna o consumo da ideologia.
    A expansão cultural leste-oeste não é um acidente. De um lado, o homo
    consumidor compra um litro de uísque e recebe como prêmio a mentira que o
    acompanha. Do outro, o homem comunista compra ideologia e recebe como prêmio um
    litro de vodca. Paradoxalmente, os regimes soviéticos e capitalistas seguem um
    caminho comum, os primeiros graças à sua economia de produção, os segundos pela
    sua economia de consumo.

    -- 46

    Com as diferenças crescendo em número e se tornando menores, a distância
    entre ricos e pobres diminui de fato, e a humanidade é nivelada, com as
    variações de pobreza sendo as únicas variações. O ponto culminante seria a
    sociedade cibernética composta de especialistas hierarquizados segundo a sua
    aptidão de consumir e de fazer consumir as doses de poder necessárias ao
    funcionamento de uma gigantesca máquina social da qual eles seriam ao mesmo
    tempo a entrada e a saída de dados. Uma sociedade de exploradores-explorados
    onde alguns escravos são mais iguais do que outros.

    -- 47

    A história é a transformação contínua da alienação natural em alienação social,
    e também, paradoxalmente, o contínuo reforço de um movimento de contestação que
    irá dissolvê-la, “desalienando-a”. A luta histórica contra a alienação natural
    transforma a alienação natural em alienaçao social, mas o movimento de
    desalienação histórica atinge por sua vez a alienação social e denuncia a sua
    magia fundamental.

    [...]

    O apodrecimento das relações humanas pela troca e pela contrapartida está
    evidentemente ligado à existência da burguesia. Que a troca persista em uma
    parte do mundo em que se diz que a sociedade sem classes se realizou atesta que
    a sombra da burguesia continua reinar debaixo da bandeira vermelha. Enquanto
    isso, entre as pessoas que vivem nos países industrializados, o prazer de dar
    delimita muito claramente a fronteira entre o mundo do cálculo e o mundo da
    exuberância, da festa.

    -- 49

    Dom, troca, contrapartida e doação.

    -- 49

    Técnica.

    -- 50

    Desse ponto de vista, a história não passa da transformação da alienação
    natural em alienação social: um processo de desalienação transformado em um
    processo de alienação social, um movimento de libertação que produza novos
    grilhões. Embora, no final, a vontade de emancipação humana ataque diretamente
    o conjunto dos mecanismos paralisantes, ou seja, a organização social baseada
    na apropriação privada. Esse é o movimento de desalienação que vai desfazer a
    história e realizá-la em novos modos de vida.

    A ascensão da burguesia ao poder anuncia a vitória do homem sobre as forças
    naturais.  Mas, na mesma hora, a organização social hierárquica, nascida da
    necessidade de luta contra a fome, a doença, o desconforto etc., perde sua
    justificativa e é obrigada a endossar a responsabilidade pelo mal-estar nas
    civilizações industriais. Hoje os homens já não atribuem a sua miséria à
    hostilidade da natureza, mas sim, à tirania de uma forma social totalmente
    inadequada, totalmente anacrônica. Destruindo o poder mágico dos senhores
    feudais, a burguesia condenou a magia do poder hierárquico. O proletariado
    executará a sentença.

    -- 50

    O princípio hierárquico é o princípio mágico que resitiu à emancipação dos homens e as
    suas lutas históricas pela liberdade. De agora em diante nenhuma revolução será digna
    desse nome se não implicar pelo menos a eliminação radical de toda hierarquia.

    -- 51

    Rigidamente quantificado (pelo dinheiro e depois pela quantidade de poder, por aquilo a
    que poderíamos chamar “unidades sociométricas de poder”), a troca polui todas as relações
    humanas, todos os sentimentos, todos os pensamentos. Onde quer que a troca domine, só
    sobram coisas, um mundo de homens-objetos congelados nos organogramas do poder
    cibernético: o mundo da reificação. Mas é também, paradoxalmente, a oportunidade de uma
    reestruturação radical dos nossos modelos de vida e de pensamento. Um ponto zero em que
    tudo pode verdadeiramente começar.

    [...]

    Ao sacrifício do senhor sucede o último estágio do sacrifício, o sacrifício do especialista.
    Para consumir, o especialista faz outros consumirem de acordo com um programa
    cibernético no qual a hiper-racionalidade das trocas suprimirá o sacrifício – e o homem ao
    mesmo tempo! Se a troca pura regular um dia as modalidades de existência dos cidadãos-
    robôs da democracia cibernética, o sacrifício deixará de existir. Para obedecer, os objetos
    não têm necessidade de justificativa. O sacrifício não faz parte do programa das máquinas
    assim como do seu oposto, o projeto do homem total.

    -- 52

    Contrariamente aos interesses daqueles que controlam seu uso, a técnica tende a
    desmistificar o mundo.

    [...]

    As mediações alienadas enfraquecem o homem ao tornarem-se indispensáveis. Uma
    máscara social cobre os seres e objetos. No estado atual de apropriação primitiva, essa
    máscara transforma aquilo que ela cobre em coisas mortas, em mercadorias. Não existe
    mais natureza. Reencontrar a natureza é reinventá-la como adversário vantajoso
    construindo novas relações sociais. A excrescência do equipamento material arrebenta o
    casulo da velha sociedade hierárquica.

    -- 55

    O quantitativo e o linear confundem-se. O qualitativo é plurivalente, o quantitativo,
    unívoco. A vida quantificada se torna uma linha uniforme que é seguida em direção à
    morte.

    -- 61

    que trilha é essa na qual, ao me procurar, acabo me perdendo?
    Que cortina é essa que me separa de mim mesmo sob pretexto de me proteger? E como me
    reencontrar nesses fragmentos desintegrados que me compõem? Avanço a uma terrível
    incerteza de que um dia eu consiga me apoderar de mim. Tudo se passa como se os meus
    passos me precedessem, como se pensamentos e afetos seguissem os contornos de uma
    paisagem mental que eles pensam criar, e que na realidade os modela. Uma força absurda –
    tanto mais absurda quanto se inscreve na racionalidade do mundo e parece incontestável –
    coage a saltar sem parar para atingir um solo que os meus pés nunca abandonaram. E com
    esse salto inútil em direção a mim, só o que consigo é que o meu presente seja tirado de
    mim: a maior parte do tempo eu vivo afastado daquilo que sou, ao ritmo do tempo morto...
    A meu ver, é muito grande a indiferença das pessoas quando em certas épocas se vê o

    -- 63

    Ao saciar a sobrevivência por meio de uma alimentação artificial, a sociedade de consumo
    suscita um novo apetite de viver. Onde quer que a sobrevivência esteja tão garantida quanto
    o trabalho, as antigas salvaguardas transformam-se em obstáculos. Não só a luta para
    sobreviver impede de viver: uma vez que se torna uma luta sem objetivos reais, ela corrói
    iaté a própria sobrevivência, tornando precário o que era irrisório. A sobrevivência cresceu
    tanto que, se não trocar de pele, ela nos sufocará na sua pele à medida que morre.
    A proteção fornecida pelos senhores perdeu razão de ser desde que a solicitude mecânica

    [...]

    O poder já não protege, ele protege a si próprio contra todos.

    -- 65-66

    Quais são os métodos de sedução do poder? A coação interiorizada
    que assegura uma consciência tranquila baseada na mentira: o masoquismo do cidadão
    honesto. Foi de fato necessário chamar de desprendimento ao que não passava de
    castração, pintar com as cores da liberdade a escolha entre várias formas de servidão.

    -- 71

    Sedução para domesticar, ordem, morte em pequenas ou grandes doses,
    bloqueio da força criativa.

    -- 72

    Quando o rebelde começa a acreditar que luta por um bem superior, o princípio
    autoritário ganha impulso. Nunca faltaram razões à humanidade para renunciar ao
    humano. De fato algumas pessoas possuem um verdadeiro reflexo de submissão, um
    medo irracional da liberdade, um masoquismo visível em toda parte da vida
    cotidiana. Com que amarga facilidade se abandona um desejo, uma paixão, a parte
    essencial de si. Com que passividade, com que inércia se aceita viver por uma
    coisa qualquer, agir por qualquer coisa, com a palavra “coisa” arrastando por
    toda parte o seu peso morto. Uma vez que é difícil ser si mesmo, abdica-se o
    mais rápido possível, ao primeiro pretexto: o amor pelos filhos, pela leitura,
    pela alcachofra. Nosso desejo de cura apaga-se sob tal generalidade abstrata da
    doença.

    [...]

    A revolução se faz todos os dias, apesar dos especialistas da revolução e em
    oposição a eles: uma revolução sem nome, como tudo aquilo que pertence à
    experiência vivida. Ela prepara, na clandestinidade cotidiana dos gestos e dos
    sonhos, a sua coerência explosiva.

    Nenhum problema é tão importante para mim quanto aquele que é colocado todo dia
    pela dificuldade de inventar uma paixão, de realizar um desejo, de construir um
    sonho da forma espontânea como durante a noite ele é construído na minha mente
    enquanto durmo.

    [...]

    Escrevo por impaciência e com impaciência. Para viver sem tempo morto. O que as
    outras pessoas dizem só me interessa na medida em que me diga respeito. Elas
    precisam de mim para que se salvem assim como eu preciso delas para que eu me
    salve. O nosso projeto é comum. Mas está fora de questão que o projeto do homem
    total esteja ligado à redução da individualidade. Não existe castração maior ou
    menor.

    -- 74

    Arte, vida, criatividade.

    -- 75-77

    Do mesmo modo que a classe dominante tem os melhores motivos do mundo para
    negar a existência da luta de classes, assim a história da separação não pode
    deixar de se confundir com a história da dissimulação.

    -- 78

    Excetuando as máquinas de guerra, as máquinas antigas têm origem no teatro:
    gruas, roldanas, mecanismos hidráulicos eram usados como acessórios teatrais
    bem antes de transformarem as relações de produção. Este fato merece ser
    salientado: por mais longe que se recue, a dominação da terra e dos homens
    depende sempre de técnicas invariavelmente consagradas ao serviço do trabalho e
    da ilusão.

    -- 84

    Por meio ainda da técnica rudimentar da imagem, o indivíduo aprende a modelar
    as suas atitudes existenciais segundo os retratos-robôs que dele traça a
    psicologia moderna. Os seus tiques e manias pessoais se tornam os meios pelos
    quais o poder o integra nos seus esquemas. A miséria da vida cotidiana atinge o
    ápice ao pôr-se em cena na tela. Do mesmo modo que a passividade do consumidor
    é uma passividade ativa, a passividade do espectador reside na sua capacidade
    de assimilar papéis para depois desempenhá-los de acordo com as normas
    oficiais. A repetição de imagens, os estereótipos oferecem uma série de modelos
    na qual cada um deve escolher um papel. O espetáculo é um museu de imagens, um
    armazém de sombras chinesas. É também um teatro experimental. O homem-
    consumidor se deixa condicionar pelos estereótipos (lado passivo) segundo os
    quais modela os seus diferentes comportamentos (lado ativo). Dissimular a
    passividade, renovando as formas de participação espetacular e a variedade de
    estereótipos, é aquilo a que hoje se

    -- 85

    Os papéis são desmanchados pela força da resistência da experiência vivida, e
    assim a espontaneidade arrebenta o abscesso da inautencidade e da
    pseudo-atividade.

    -- 86

    A habilidade em desempenhar e lidar com os papéis determina o lugar ocupado na
    hierarquia do espetáculo. A decomposição do espetáculo prolifera os
    estereótipos e os papéis, os quais justamente por isso caem no ridículo, e
    roçam demasiado perto a sua negação, isto é, o gesto espontâneo (1,2)

    A identificação é o caminho de entrada no papel. A necessidade de se
    identificar com ele é mais importante para a estabilidade do poder que a
    escolha dos modelos de identifiicação.  A identificação é um estado doentio,
    mas só as identificações acidentais caem na categoria oficial chamada ”doença
    mental”.O papel tem por função vampirizar a vontade de viver (3) O papel
    representa a experiência vivida, porém ao mesmo tempo a reifica. Ele também
    oferece consolo pela vida que ele empobrece, tornando-se assim um prazer
    substituto e neurótico. É importante se libertar dos papéis recolocando-os no
    domínio do lúdico(4)

    [...]

    O peso do inautêntico suscita uma reação violenta, quase biológica, do querer-viver.

    -- 87

    Os nossos esforços, aborrecimentos, fracassos, o absurdo dos nossos atos provêm
    na maioria das vezes da imperiosa necessidade em que nos encontramos de
    desempenhar papéis híbridos, papéis que parecem responder aos nossos
    verdadeiros desejos, mas que na verdade são antagônicos a eles. “Queremos
    viver”, dizia Pascal, “de acordo com a idéia dos outros, numa vida imaginária.
    E por isso cultivamos aparências.

    -- 88

    Aonde a sociedade do espetáculo vai buscar os seus novos estereótipos? Ela os
    encontra graças à injeção de criatividade que impede que alguns papéis se
    conformem ao estereótipo decadente ( da mesma forma que a linguagem se renova
    em contato com as formas populares). Graças, em outras palavras, ao elemento de
    jogo que transforma os papéis.

    -- 89

    a identificação – o princípio do teste de Szondi (psiquiatra que representou
    uma oposição à linha dura stalinista dentro da URSS) é bem conhecido. O
    paciente é convidado a escolher, no meio de 48 fotos de doentes em estado de
    crise paroxística, os rostos que lhe inspiram simpatia ou aversão.
    Invariavelmente são escolhidos os indivíduos que apresentam uma pulsão que o
    paciente aceita, ao passo que são rejeitados aqueles que expressam pulsões que
    ele rejeita. A partir dos resultados o psquiatra constrói um perfil pusional do
    qual se serve para liberar o paciente ou para dirigi-lo ao crematório
    climatizado dos hispitais psiquiátricos.

    Consideremos agora os imperativos da sociedade do consumo, uma sociedade na
    qual a essência do homem é consumir: consumir Coca-cola, literatura, idéias,
    sexo, arquitetura, TV, poder. Os bens de consumo, as ideologias, os
    estereótipos, são as fotos de um formidável teste de Szondi no qual cada um de
    nós é convidado a tomar parte, não por meio de uma simples escolha, mas por um
    compromisso, por uma atividade prática.

    -- 90

    Pode-se considerar que as pesquisas de mercado, as técnicas de motivação, as
    sondagens de opinião, os inquéritos sociológicos, o estruturalismo são parte
    desse projeto, não importa o quão anárquicas e débeis possam ser ainda suas
    contribuições. Faltam a coordenação e a racionalização? Os cibernéticos
    tratarão disso, se lhes dermos a chance.

    [...]

    A doença mental não existe. É uma categoria cômoda para agrupar e afastar os
    casos em que a identificação não ocorreu de forma apropriada. Aqueles que o
    poder não pode governar nem matar, são rotulados de loucos. Aí se encontram os
    extremistas e os megalomaníacos do papel. Encontram-se também os que riem dos
    papéis ou os recusam.

    -- 91

    Papel, Reich, couraça.

    -- 92

    Quanto mais nos desligamos do papel, melhor manipulamos contra o adversário. Quanto
    mais evitamos o peso das coisas, mais conquistamos leveza de movimentos.
    Os amigos não ligam muito para as formas...Discutem abertamente, certos de que
    não podem machucar um ao outro. Onde a comunicação real é buscada, os equívocos
    não são um crime.

    -- 94

    Quanto mais se esgota aquilo que tem por função estiolar a vida cotidiana, mais
    o poderio da vida vence o poder dos papéis. Esse é o início da inversão de
    perspectiva. É nesse nível que a nova teoria revolucionária deve se concentrar
    a fim de abrir a brecha que leva à superação. Dentro da era do cálculo e da
    suspeita inaugurada pelo capitalismo e pelo stalinismo, opõe-se e constrói-se
    uma fase clandestina de tática, a era do jogo.

    O estado de degradação do espetáculo, as experiência individuais, as
    manifestações coletivas de recusa fornecem o contexto para o desenvolvimento de
    táticas práticas para lidar com os papéis. Coletivamente é possível suprimir os
    papéis. A criatividade espontânea e o ambiente festivo que fluem livremente nos
    momentos revolucionários oferecem exemplos numerosos disso. Quando a alegria
    ocupa o coração do povo não existe líder ou encenação que dele se possa
    apoderar.

    -- 99

    Segundo Hans Selye, o teórico do estresse, a síndrome geral da adaptação possui
    três fases: a reação de alarme, a fase de resistência e a fase de esgotamento.
    No plano do parecer, o homem soube lutar pela eternidade, mas , no plano da
    vida autêntica, ainda se encontra na fase da adaptação animal: reação
    espontânea na infância, consolidação na maturidade, esgotamento na velhice. E,
    hoje em dia, quanto mais as pessoas buscam o plano do parecer, mais o cadáver
    do caráter efêmero e incoerente do espetáculo demonstra que elas vivem como um
    cão e morrem como um tufo de erva seca. Não pode estar longe o dia em que se
    reconhecerá que a organização social criada pelo homem para transformar o mundo
    segundo os seus desejos não serve mais a esse objetivo. E que ela não passa de
    um sistema de proibição que impede a criação de uma forma superior de
    organização e o uso de técnicas de libertação e realização individuais que o
    homem forjou por meio da história da apropriação privada, da exploração do
    homem pelo homem e do poder hierárquico.

    -- 102

    Colocar a serviço do imutável a ideologia do progresso e da mudança cria um
    paradoxo que nada, de agora em diante, pode esconder à consciência , nem
    justificar diante dela. Neste universo em que a técnica e o conforto se
    expandem, vemos que os seres se fecham em si mesmos, endurecem, vivem
    mesquinhamente, morrem por coisas sem importância. É um pesadelo no qual nos
    prometeram uma liberdade absoluta e nos deram um metro cúbico de autonomia
    individual, rigorosamente controlada pelos vizinhos. Um espaço-tempo da
    mesquinhez e do pensamento pequeno.

    -- 105

    Ninguém tem o direito de ignorar que a força do condicionamento o habitua a
    sobreviver com um centésimo do seu potencial de viver.

    -- 107

    O revoltado sem outro horizonte além do muro das coações corre o risco de
    quebrar a cabeça nele ou de defendê-lo um dia com uma teimosa estupidez. Já que
    se apreender na perspectiva das coações é sempre olhar no sentido desejado pelo
    poder, quer para recusá-lo, quer para aceitá-lo. Assim o homem se encontra no
    fim da linha, coberto de podridão como diz Rosanov. Limitado por todos os
    lados, ele resite a qualquer intrusão, e monta guarda sobre si mesmo,
    zelosamente, sem perceber que se tornou estéril: que mantém vigílila sobre um
    cemitério.

    [...]

    Como as pessoas mais inclinadas aos acordos comprometedores sempre consideram
    uma incomensurável glória permanecerem íntegras em um ou dois pontos
    específicos!

    Nenhum laço é mais difícil de romper que aquele no qual o indivíduo se prende a
    si próprio quando sua revolta se perde dessa forma. Quando ele coloca a sua
    liberdade a serviço da não-liberdade, o aumento da força da não-liberdade que
    resulta disso o escraviza. Ora, pode acontecer que nada se assemelhe tanto à
    não-liberdade quanto o esforço em direção à liberdade, mas a não- liberdade tem
    como particularidade que uma vez comprada ela perde todo o seu valor, mesmo que
    seu preço seja tão alto quanto a liberdade.

    -- 114-115

    Não existe ninguém, por mais alienado que seja, que não possua e não reconheça
    a si próprio uma parte irredutível de criatividade, um quarto escuro protegido
    contra qualquer intrusão de mentira e de coações. No dia em que a organização
    social estender o seu controle sobre essa parte do homem, ela reinará apenas
    sobre robôs e cadáveres.

    [...]

    Agora que a alienação do consumidor é esclarecida pela própria dialética do
    consumo, que prisão eles preparam para a subversivíssima criatividade
    individual? Eu já disse que a última saída dos dirigentes era transformar as
    pessoas em organizadoras da própria passividade.

    -- 125

    A espontaneidade - a espontaneidade é o modo de ser da criatividade individual.
    Ela é o seu primeiro jorro, ainda imaculado, não poluído na fonte e ainda não
    ameaçado de recuperação. Se nada é mais bem repartido no mundo do que a
    criatividade, a espontaneidade, pelo contrário, parece ser um privilégio. Só a
    possuem aqueles que por meio de uma longa resitência ao poder ganharam a
    consciência de seu próprio valor como indivíduos. Nos momentos revolucionários
    isso significa a maioria das pessoas. Em outros períodos, quando a revolução é
    construída dia a dia sem ser vista, são mais pessoas do que pensamos. Onde quer
    que subsista um raio de criatividade, a espontaneidade conserva as sua
    possibilidades.

    -- 126

    Só é espontâneo aquilo que não emana de uma coação interiorizada, mesmo
    subconscientemente, e que além disso escapa ao domínio da abstração alienante,
    à recuperação espetacular. Ela é mais uma conquista do que algo dado. A
    reestruturação do indivíduo deve passar por uma reestruturação do inconsciente
    (compare com a construção dos sonhos).

    [...]

    Para mim, a espontaneidade constitui uma experiência imediata, uma consciência
    da experiência vivida, dessa experiência vivida cercada por todos os lados,
    ameaçada por proibições e contudo, ainda não alienada, ainda não reduzida ao
    inautêntico. No centro da experiência vivida, cada um se encontra mais perto de
    si mesmo.

    [...]

    A consciência do presente harmoniza-se à experiência vivida como uma espécie de
    improvisação. Esse prazer, pobre porque ainda isolado, rico porque já orientado
    para o prazer idêntico dos oturos, carrega uma grande semelhança com o prazer
    do jazz. O estilo de improvisação da vida cotidiana em seus melhores momentos
    cabe no que Alfons Dauer escreve a respeito do Jazz : a concepção africana do
    ritmo difere da nossa porque o apreendemos auditivamente ao passo que os
    africanos o apreendem por meio do movimento corporal. A sua técnica consiste
    essencialmente em introduzir a descontinuidade no seio do equilíbrio estático
    imposto ao longo do tempo pelo ritmo e pela métrica. Essa descontinuidade
    resultante da presença de centros de gravidade extáticos fora do tempo da
    própria métrica e ritmo, cria constantemente tensões entre as batidas estática
    e as batidas extáticas que lhes são sobrepostas”

    -- 127

    A comunicação tão imperativamente desejada pelo artista é impedida e proibida
    mesmo nas relações mais simples da vida cotidiana. De tal modo que a busca de
    novos modos de comunicação, longe de estar reservada aos pintores ou aos
    poetas, é parte hoje de um esforço coletivo. Assim acaba a velha especialização
    da arte. Já não existem artistas uma vez que todos os são. A futura obra de
    arte é a construção de uma vida apaixonante.

    -- 131

    Aqui se encontram as três fases históricas que caracterizam a evolução do senhor:

    1 o princípio de dominação, ligado à sociedade feudal;
    2 o princípio de exploração ligado à sociedade burguesa;
    3 o princípio de organização, ligado à sociedade cibernética

    Na verdade, os três elementos são indissociáveis – não se domina sem explorar
    nem organizar simultaneamente – mas o peso de cada um varia conforme as épocas.
    À medida que se passa de uma fase a outra, a autonomia e o âmbito da
    responsabilidade do senhor são reduzidos. A humanidade do senhor tende para
    zero enquanto a desumanidade do poder desencarnado tende ao infinito.

    Conforme o princípio de dominação, o senhor recusa aos escravos uma existência
    que limitaria a sua. No princípio de exploração, o patrão concede aos
    trabalhadores uma existência que alimenta e amplia a sua. O princípio de
    organização separa as existências individuais em frações, segundo o grau de
    capacidade de liderança ou execução que comportam (um chefe de oficina seria
    por exemplo definido no final de longos cálculos de sua produtividade,
    representatividade, etc, por 56% de dirigente, 40% de executor e 4% ambíguo,
    como diria Fourier).

    [...]

    Os massacres de Auschwitz ainda possuem um lirismo quando comparados às mãos
    geladas do condicionamento generalizado que a organização tecnocrática dos
    cibernéticos prepara para a sociedade, futura e tão próxima.

    [...]

    A parte do poder que restava aos possuidores dos instrumentos de produção
    desaparece a partir do instante em que as máquinas, escapando aos
    proprietários, passam para o controle dos técnicos que organizam o seu emprego.
    Entretanto, os próprios organizadores são lentamente digeridos pelos esquemas e
    programas que elaboram. A máquina simples foi talvez a última justificativa da
    existência dos chefes, o último suporte do seu último vestígio de humanidade. A
    organização cibernética da produção e do consumo passa obrigatoriamente pelo
    controle, planejamento, racionalização da vida cotidiana.

    Os especialistas são esses senhores em migalhas, esses senhores-escravos que
    proliferam no território da vida cotidiana. As susas possibilidades são nulas,
    podemos garantir. Já em 1867 no congresso de Laussane da I Internacional,
    Francau declarou: durante muito tempo estivemos a reboque dos marqueses dos
    diplomas e dos princípes da ciência. Tratemos nós próprios de nossos assuntos
    e, por mais inábeis que sejamos, nunca os faremos pior do que como foram feitos
    em nosso nome”. Palavras cheias de sabedoria, e cujo sentido se reforça com a
    proliferação dos especialistas e sua incrustação em todos os aspectos da vida
    pessoal.  Uma divisão opera-se claramente entre aqueles que obedecem à atração
    magnética que exerce a grande kafkiana da cibernética e aqueles que, obedecendo
    a seus próprios impulsos, se esforçam por lhe escapar.

    -- 136-137

    O senhor sem escravos ou a superação aristocrática da aristocracia – o senhor
    perdeu-se pelos mesmos caminhos que Deus. Desaba como um Golem logo que deixa
    de amar os homens, logo que deixa portanto de amar o prazer que pode ter em
    oprimi-los, logo que abandona o princípio hedonista. Há pouco prazer em
    deslocar coisas, em manipular seres passivos e insensíveis como tijolos. No seu
    requinte, deus busca criaturas vivas, de boa carne pulsante, almas arrepiadas
    de terror e respeito. Necessita, para experimentar a própria grandeza, sentir a
    presença de súditos ardentes na oração, na contestação, no subterfúgio, e até
    no insulto. O deus católico dispõe-se a conceder liberdade verdadeira, mas à
    maneira dos penhoristas, só como empréstimo. Ele brinca de gato e rato com os
    homens até o juízo final, quando os devora. Pelo fim da idade média, com a
    entrada em cena da burguesia, esse deus é lentamente humanizado. Humanizado de
    forma paradoxal, uma vez que se torna objeto, da mesma forma que os homens.
    Condenando os homens à predestinação, o deus de Calvino perde o prazer do
    julgamento arbitrário, não é mais livre para esmagar quem ele quiser e quando
    quiser. Deus das transações comerciais, sem fantasia, comedido e frio como uma
    taxa de câmbio, envergonha-se, esconde-se. O diálogo é rompido.

    [...]

    Por que razão é o senhor obrigado a abandonar a exigência hedonista? O que o
    impede de alcançar o gozo total a não ser a sua própria condição de senhor, o
    seu comprometimento com o princípio de superioridade hierárquica? E esse
    abandono aumenta à medida que a hierarquia se fragmenta, que os senhores se
    multiplicam diminuindo de tamanho, que a história democratiza o poder. O gozo
    imperfeito dos senhores tornou-se gozo dos senhores imperfeitos. Viu-se como os
    senhores burgueses, plebeus, ubuescos, coroaram a sua ravolta de cervejaria com
    a festa fúnebre do fascismo. Mas logo nem sequer festa existirá para os
    senhores-escravos, para os últimos homens hierárquicos, somente a tristeza das
    coisas, uma serenidade soturna, o mal-estar do papel, a consciência do “nada
    ser” O que acontecerá a essas coisas que nos governam? Será necessário
    destruí-las?

    Certamente, e os mais bem preparados para liquidar esses escravos-no-poder são
    aqueles que lutam desde sempre contra a escravidão. A criatividade popular, que
    nem a autoridade dos senhores e nem a dos patrões destruiu, jamais se ajoelhará
    diante de necessidades programáticas e de planejamentos tecnocráticos. Alguém
    objetará que menos paixão e entusiasmo pode ser mobilizado para a liquidação de
    uma forma abstrata, um sistema, do que para a execução de senhores odiados. Mas
    isso seria encarar o problema do ponto de vista errado, do ponto de vista do
    poder. Contrariamente à burguesia, o proletariado não se define pelo seu
    adversário de classe, ele traz em si o fim da distinção em classes e o fim da
    hierarquia. O papel da burguesia foi unicamente negativo. Saint-just o lembra
    magnificamente: aquilo que constitui uma república é a destruição total daquilo
    que lhe é oposto.”

    Se a burguesia se contenta em forjar armas contra a feudalidade, e portanto
    contra si mesma, o proletariado pelo contrário contém em si a sua superação
    possível. Ele é a poesia momentaneamente alienada pela classe dominante ou pela
    organização tecnocrática, mas sempre a ponto de emergir. Único depositário da
    vontade de viver, porque só ele conheceu até o paroxismo o caráter insuportável
    da sobrevivência, o proletariado quebrará a muralha das coações pelo sopro do
    seu prazer e pela violência espontânea da sua criatividade. Toda alegria e riso
    a serem liberados, ele já possui. É dele mesmo que tira a força e a paixão.
    Aquilo que ele se prepara para construir destruirá por acréscimo tudo aquilo
    que a ele se opõe do mesmo modo que em uma fita magnética, uma gravação apaga a
    outra. O poder das coisas será abolido pelo proletariado no ato da sua própria
    abolição. Será um gesto de luxo, uma espécie de indolência, uma graça
    demonstrada por aqueles que provam a sua superioridade. Do novo proletariado
    sairão os senhores sem escravos, não os autômatos do humanismo com que sonham
    os masturbadores da esquerda pretensamente revolucionária.  A violência
    insurrecional das massas é apenas um aspecto da criatividade do proletariado: a
    sua impaciência em negar-se do mesmo modo que é impaciente em executar a
    sentença que a sobrevivência pronuncia contra si mesma.

    -- 138-139

    A superação do grande senhor e do homem cruel aplicará ao pé da letra o
    adimirável princípio de Keats : tudo aquilo que pode ser destruído deve ser
    destruído para que as crianças possam ser salvas da escravidão.  Essa superação
    deve ser operada simultaneamente em três esferas : a) a superação da
    organização patriarcal; b) a superação do poder hierárquico; c) a superação da
    arbitrariedade subjetiva, do capricho autoritário.

    [...]

    A criança adquire uma experiência subjetiva da liberdade, desconhecida de
    qualquer espécie animal, mas permanece por outro lado na dependência objetiva
    dos pais; necessita de seus cuidados e solicitude. O que distingue a criança de
    um animal jovem é que a criança possui o sentido da transformação do mundo, ou
    seja, poesia, mesmo que em grau limitado. Ao mesmo tempo, é proibido a ela o
    acesso a técnicas que os adultos empregam na maior parte do tempo contra essa
    poesia, por exemplo, técnicas de condicionamento das próprias crianças. E
    quando as crianças finalmente chegam à idade de ter acesso às técnicas, já
    perderam sob o peso das coações, na sua maturidade, aquilo que dava
    superioridade à infância. O universo dos senhores antigos carrega o mesmo
    estigma do universo das crianças: as técnicas de libertação estão fora do seu
    alcance. Desde então está condenado a sonhar com uma transformação do mundo e a
    viver segundo as leis da adaptação ao mundo.

    -- 140

    O jogo da criança, como o jogo dos nobres tem necesssidade de ser libertado, de
    ser posto novamente em um lugar de honra. Hoje o momento é historicamente
    favorável. Trata-se de salvar a criança realizando o projeto dos senhores
    antigos: a infância e a sua subjetividade soberana, a infância com seu riso que
    é um murmúrio de espontaneidade, a infância e seu modo de se ligar em si mesma
    para iluminar o mundo, e seu modo de iluminar os objetos com uma luz
    estranhamente familiar.

    Perdemos a beleza das coisas, o seu modo de existir deixando-as morrer nas mãos
    do poder e dos deuses. Em vão, a magnífica fantasia do surrealismo tentou
    reanimá-las por meio de uma irradiação poética: o poder do imaginário não basta
    para romper a casaca da alienação social que aprisiona as coisas. Ele não
    consegue devolvê-las ao livre jogo da subjetividade. Visto do ângulo do poder,
    uma pedra, uma árvore um mixer um ciclotron são objetos mortos , cruzes
    fincadas na vontade de vê-las diferentes e de mudá-las. E contudo, para além do
    significado atribuído a eles, sei que poderiam ser excitantes para mim. Sei que
    uma máquina pode suscitar paixão desde que posta a serviço do jogo, da
    fantasia, da liberdade. Em um mundo em que tudo é vivo, incluindo as árvores e
    as pedras, já não existem signos contemplados passivamente. Tudo fala da
    alegria. O triunfo da subjetividade dará vida às coisas. E o insuportável
    domínio atual das coisas mortas sobre a subjetividade não é, no fundo, a melhor
    oportunidade histórica de chegar a um estado de vida superior?

    [...]

    É necessário descobrir novas fronteiras. As limitações impostas pela alienação
    deixaram, se não de nos aprisionar, pelo menos de nos iludir. Durante séculos,
    os homens permaneceram diante de uma porta carcomida, abrindo nela buraquinhos
    com um alfinete com uma facilidade crescente. Basta um empurrão hoje para
    derrubá-la, e é somente depois disso, do outro lado, que tudo começa. A questão
    para o proletariado não consiste mais em tomar o poder, mas em pôr-lhe fim
    definitivamnete. Do lado de fora do mundo hierarquizado, as possibilidades vêm
    ao nosso encontro. O primado da vida sobre a sobrevivência será o movimento
    histórico que desfará a história. Os nossos verdadeiros adversários ainda estão
    para ser inventados, e cabe a nós buscar o contato com eles, entrar em combate
    com eles sob o pueril – infantil – avesso das coisas.

    -- 142

    Poderá a vontade individual enfim libertada pela vontdade coletiva ultrapassar
    em proezas o controle sinistramente soberbo já alcançado sobre os seres humanos
    pelas técnicas de condicionamento do estado policial? De um homem faz-se um cão
    um tijolo um militar torturador, e não se poderia fazer dele um homem?

    -- 143

    O espaço é um ponto na linha do tempo, na máquina que transforma o futuro em
    passado.  O tempo controla o espaço vivido, mas controla-o do exterior,
    fazendo-o passar, tornando-o transitório. Contudo, o espaço da vida individual
    não é um espaço puro, e o tempo que o arrasta não é também uma pura
    temporalidade. Vale a pena examinar a questão com mais cuidado.

    Cada ponto terminal na linha do tempo é único e particular, e entretanto logo
    que se acrescenta o ponto seguinte, o seu predecessor desaparece na
    uniformidade da linha, digerido por um passado que já conhece outros pontos.
    Impossível distingui-lo. Cada ponto portanto faz progredir a linha que o faz
    desaparecer.

    [...]

    Por mais que o espaço vivido seja um universo de sonhos, desejos, de
    criatividade prodigiosa, ele não passa em termos de duração de um ponto que
    sucede a outro ponto correndo segundo um único princípio, o da destruição. Ele
    aparece, se desenvolve e desaparece na linha anônima do passado na qual o seu
    cadáver se torna matéria-prima aos lampejos da memória e aos historiadores.

    [...]

    O espaço cristalino da vida cotidiana rouba uma parcela de tempo exterior
    graças à qual se cria uma pequena área de espaço-tempo unitário: é o
    espaço-tempo dos momentos da criatividade, do prazer, do orgasmo. O lugar dessa
    alquimia é minúsculo, mas a intensidade vivida é tal que exerce na maioria das
    pessoas um fascínio sem igual. Visto pelos olhos do poder, observando do
    exterior, esses momentos de paixão não passam de um ponto irrisório, um
    instante drenado do futuro pelo passado. A linha do tempo objetivo nada sabe –
    e nada quer saber – do presente como presença subjetiva imediata. E por sua
    vez, a vida subjetiva apertada no espaço de um ponto – a minha alegria, o meu
    prazer, as minhas fantasias – não gostaria de saber nada sobre o
    tempo-que-escoa, o tempo linear, o tempo das coisas. Ela deseja, pelo
    contrário, aprender tudo do seu presente já que afinal ela nada mais é que um
    presente.

    -- 148

    O projeto de enriquecimento do espaço-tempo da experiência vivida passa pela
    análise daquilo que o empobrece. O tempo linear só domina os homens na medida
    em que lhes impede de transformar o mundo, na medida em que os coage portanto a
    se adptarem.  Para o poder, o inimigo número um é a criatividade individual
    irradiando livremente. E a força da criatividade está no unitário. Como se
    esforça o poder para quebrar a unidade do espaço-tempo vivido? Transformando a
    experiência vivida em mercadoria, lançando-a no mercado do espetáculo, ao sabor
    da oferta e da procura por papéis e estereótipos (foi isso que discuti nas
    páginas dedicadas aos papéis, no capítulo XV).

    -- 150

    Como distrair os homens de seu presente a não ser atraindo-os à esfera na qual o tempo
    escoa? Essa tarefa cabe ao historiador. O historiador organiza o passado, fragmentado-o
    conforme a linha oficial do tempo, depois arruma os acontecimentos em categorias ad hoc.
    Essas categorias, de fácil uso, põem os acontecimentos passados em quarentena. Sólidos
    parênteses os isolam, os contêm, os impedem de tomar vida, de ressuscitar, de rebentar de
    novo nas ruas da nossa vida cotidiana. O acontecimento está, por assim dizer, congelado. É
    proibido juntar-se a ele, refazê-lo completá-lo, tentar a sua superação. Aí está ele,
    conservado para sempre e suspenso para a contemplação dos estetas. Uma leve mudança de
    ênfase e hei-lo transposto do passado ao futuro. O futuro não é mais que historiadores se
    repetindo. O futuro que eles anunciam é uma colagem de recordações, das suas
    recordações. Vulgarizada pelos pensadores stalinistas, a famosa noção do sentido da
    história acabou esvaziando de toda humanidade tanto o futuro quanto o passado.

    -- 151

    Construir uma arte de viver é hoje uma reivindicação popular. É necessário
    concretizar em um espaço-tempo apaixonadamente vivido as pesquisas de todo um
    passado artístico que na verdade foram postas de lado de modo descuidado.
    
    Neste caso as recordações as quais me refiro, são recordações de feridas
    mortais. Aquilo que não é terminado apodrece. O passado é erroneamente tratado
    como irremediável.  Ironicamente, aqueles que falam dele com um dado definitivo
    não param de triturá-lo, de falsificá-lo, de arranjá-lo ao gosto do dia. Eles
    agem como o pobre Winston, em 1984 de George Orwell, reescrevendo artigos de
    jornais antigos que foram contraditos pela evolução dos acontecimentos.

    [...]

    Existe apenas uma forma valorosa de esquecer : aquela que apaga o passado
    realizando-o.  Aquela que salva da decomposição pela superação. Os fatos, por
    mais longe que se situem, nunca disseram sua última palavra. Basta uma mudança
    radical no presente para que desçam das estantes do museu e ganhem vida aos
    nossos pés.

    -- 152

    Construir o presente é corrigir o passado, mudar a psicogeografia do nosso
    ambiente, libertar de sua ganga os sonhos e os desejos insatisfeitos, deixar as
    paixões individuais harmonizarem-se no coletivo. O intervalo de tempo que
    separa os revoltados de 1525 dos rebeldes muletistas, Spartakus de Pancho Villa
    só pode ser transposto pela minha vontade de viver.

    Esperar por amanhãs festivos é o que impossibilita as nossas festas de hoje. O
    futuro é pior que o oceano: ele nada contém. Planejamento, prospecção, plano a
    longo prazo: é o mesmo que especular sobre o teto da casa quando o primeiro
    andar não existe mais. E contudo se construíres bem o presente o resto virá por
    consequência.

    [...]

    Na zona da criação verdadeira o tempo se dilata. No inautêntico, o tempo se
    acelera.

    -- 153

    A tarefa é sempre resolver as contradições do presente, não parar no meio do
    caminho, não se deixar distrair, tomar o caminho da superação. Essa tarefa é
    coletiva, de paixão, de jogo (a eternidade é o mundo do jogo, diz Boehme). Por
    mais pobre que seja o presente sempre contém a verdadeira riqueza, a da
    construção possível. Esse é o poema interrompido que me enche de alegria.

    -- 153-154

    A paixão de criar é a base do projeto de realização (2), a paixão do amor é a
    base do projeto de comunicação (4), a paixão do jogo é a base do projeto de
    participação (6). Dissociados, esses três projetos reforçam a unidade
    repressiva do poder. A subjetividade radical é a presença atualmente observável
    na maioria das pessoas de uma mesma vontade de construir uma vida apaixonante
    (3). O erotismo é a coerência espontânea que dá unidade prática à tentativa de
    enriquecer a experiência vivida. ( 5)

    [...]

    A construção da vida cotidiana realiza no mais alto grau a unidade do racional
    e do passional. O mistério deliberadamente tecido desde sempre a respeito da
    vida tem como principal objetivo dissimular a trivialidade da sobrevivência. De
    fato, a vontade de viver é inseparável de uma certa vontade de organização. O
    fascínio que a promessa de uma vida rica e multidimensional exerce sobre cada
    indíviduo adquire, necessariamente, o aspecto de um projeto submetido no todo
    ou em parte ao poder social encarregado de impedi-lo.  Assim como o governo dos
    homens recorre essencialmente a um tríplice modo de opressão – a coação, a
    mediação alienante e a sedução mágica – também a vontade de viver encontra
    força e coerência na unidade de três projetos indissociáveis : a realização, a
    comunicação, a participação.
    
    Em uma história dos homens que não se reduzisse à história da sobrevivência,
    sem por outro lado se dissociar dela, a dialética desse triplo projeto, aliada
    à dialética das forças produtivas explica a maioria dos comportamentos. Não há
    motim ou revolução que não revele uma busca apaixonada por uma vida exuberante,
    por uma transparência das relações humanas e por um modo coletivo de
    transformação do mundo. De fato, três paixões fundamentais parecem animar a
    evolução histórica, paixões que são para a vida aquilo que a necessidade de
    alimento e proteção são para a sobrevivência. A paixão da criação, a paixão do
    amor e a paixão do jogo interagem com a necessidade de alimento e de proteção,
    tal como a vontade de viver interfere continuamente na necessidade de
    sobreviver.

    -- 155

    A mania cartesiana de fragmentar, e de progredir de forma gradual, produz
    necessariamente uma realidade coxa e incompleta. Os exércitos da Ordem só
    recrutam mutilados.

    -- 156

    Mostrei de que maneira a organização social hierárquica constrói o mundo
    destruindo os homens; de que modo o aperfeiçoamento do seu mecanismo e das suas
    redes a fez funcionar como um computador gigante cujos programadores são também
    programados; de que modo, enfim, o mais frio dos monstros frios encontra a sua
    realização no projeto do estado cibernético.

    Nessas condições a luta pelo pão de cada dia, o combate contra o desconforto, a
    busca de uma estabilidade de emprego e de uma segurança material são, na frente
    social, igualmente expedições ofensivas que tomam lenta mas seguramente o
    aspecto de ações de retaguarda (mas não se deve subestimar a importância
    delas).

    Apesar de falsos compromissos e de atividades ilusórias, uma energia criadora
    continuamente estimulada não é absorvida mais depressa suficientemente sob a
    ditadura do consumo. [...] O que acontecerá a essa exuberância repentinamente
    disponível, a esse excesso de robustez e de virilidade que nem as coações nem a
    mentira conseguiram verdadeiramente desgastar?

    Não recuperada pelo consumo artístico e cultural – pelo espetáculo ideológico –
    a criatividade volta-se espontaneamente contra as condições e as garantias de
    sobrevivência.  Os rebeldes não têm nada a perder a não ser sua sobrevivência.
    Contudo, podem perdê-la de dois modos : perdendo a vida ou construindo-a. Já
    que a sobrevivência é uma espécie de morte lenta, existe uma tentação, não
    desprovida de sentimentos genuínos, de acelerar o movimento e morrer mais
    depressa como pisar fundo no acelerador de um carro de corrida.  “Vive-se”
    então negativamente a negação da sobrevivência. Ou pelo contrário, as pessoas
    podem se esforçar por sobreviver como anti-sobreviventes concentrando sua
    energia no enriquecimento da vida cotidiana. Negam a sobrevivência
    incorporando-a em uma atividade lúdica construtiva. Essas duas soluções
    promovem a tendência unitária e contraditória da dialética da decomposição e da
    superação.

    -- 157

    Nietzsche.

    -- 158

    O fervor dos soldados rasos faz a disciplina dos exércitos: a única coisa que a
    cachorrada policial aprende é a hora de morder e a hora de rastejar.

    [...]

    Se a violência inerente aos grupos de jovens delinquentes deixasse de se
    dissipar em ações espetaculares e tornar-se insurrecional, provocaria sem
    dúvida uma reação em cadeia, uma onda de choque qualitativa. [...] Se os
    blouson noirs atingiram uma consciência revolucionária pela simples compreensão
    daquilo que já são e pela simples exigência de querer ser mais, é provável que
    determinem o epicentro da inversão de perspectiva. Federar os seus grupos seria
    o ato que simultaneamente manifestaria e permitiria essa consciência.

    -- 159

    Ninguém se desenvolve livremente sem espalhar a liberdade no mundo.

    -- 163

    Os especialistas da comunicação organizam a mentira em proveito dos senhores de
    cadáveres.

    [...]

    - quanto mais me desligo do objeto do meu desejo, e quanto mais força objetiva
      dou ao objeto do meu desejo, mais o meu desejo se torna despreocupado em
      relação ao seu objeto;

    - quanto mais me desligo do meu desejo como objeto e mais força objetiva dou ao
      objeto do meu desejo, mais o meu desejo encontra sua razão de ser no ser
      amado.

    -- 165

    A verdadeira escolha é entre a sedução espetacular – a conversa fiada – e a
    sedução pelo qualitativo – a pessoa que é sedutora porque não se preocupa em
    seduzir.

    -- 166

    O prazer é o princípio de unificação. O amor é a paixão pela unidade em um
    momento comum. A amizade é a paixão pela unidade em um projeto comum.

    -- 167

    Sem predizer os detalhes de uma sociedade em que a organização das relações
    humanas esteja aberta sem reservas à paixão do jogo, podemos no entanto prever
    que ela apresentará as seguintes características:

    - recusa de chefes e de qualquer hierarquia
    - recusa de sacrifício
    - recusa de papéis
    - liberdade de realização autêntica
    - transparência das relações sociais

    -- 170

    São, evidentemente, grupos numericamente pequenos, as micrissociedades, que
    apresentam as melhores condições de experimentação. Nelas, o jogo regulará
    soberanamente os mecanismos da vida em comum, a harmonização dos caprichos, dos
    desejos das paixões.

    [...]

    O que aconteceria então se as pessoas começassem a brincar com os papéis da
    vida real?

    -- 171

    Se alguém entra no jogo com um papel fixo, um papel sério, ou essa pessoa se
    arruína ou arruína o jogo. É o caso do provocador. O provocador é um
    especialista em jogo coletivo.

    [...]

    Qual é o melhor provocador? O líder do jogo que se torna dirigente.

    [...]

    Diferentemente do provocador, o traidor aparece espontaneamente em um grupo
    revolucionário. Ele surge sempre que a paixão do jogo desaparece e junto com
    ela o projeto de participação real. O traidor é um homem que não encontrando
    como se realizar autenticamente por meio do modo de participação que lhe é
    proposto, decide jogar contra essa participação não para corrigi-la mas para
    destrui-la.  traidor é a doença senil dos grupos revolucinarios.

    [...]

    Um exército eficientemente hierarquizado pode ganhar uma guerra, mas não uma
    revolução. Uma horda indisciplinada não consegue a vitória nem na guerra, nem
    na revolução. O problema é organizar sem hierarquizar, ou em outras palavras,
    procurar que o líder do jogo não se torne um chefe. O espírito lúdico é a
    melhor garantia contra a esclerose autoritária. Nada resiste a criatividade
    armada. Sabemos que as tropas de Villa e de Makhno derrotaram os mais
    aguerridos batalhões de exército.

    -- 172

    A organização hierárquica e a completa falta de disciplina são ambas ineficientes.

    [...]

    Como manter a disciplina necessária ao combate numa tropa que se recusa
    obedecer servilmente a um chefe? Como evitar a falta de coesão? Na maioria das
    vezes, os exércitos revolucionários sucumbem ao mal da submissão a uma causa ou
    a busca inconsequente do prazer.

    [...]

    A melhor tática forma uma só unidade com o cálculo hedonista. A vontade de
    viver, brutal, desenfreada é para o combatente a mais mortífera arma secreta.
    Essa arma volta-se contra aqueles que a ameaçam: para defender a pele, o
    soldado tem todo o interesse em atirar nos superiores.

    -- 173

    Quando uma canalização de água arrebentou no laboratório de Pavlov, nenhum dos
    cães que sobreviveram à inundação conservou o menor traço do seu longo
    condicionamento. O maremoto das grandes transformações sociais teria menos
    efeito sobre os homens do que uma inundação sobre cães?

    -- 178

    Aqueles que se aproximam da revolução afastando-se de si mesmos – como todos os
    militantes fazem – se aproximam de costas para trás às avessas.

    -- 180
