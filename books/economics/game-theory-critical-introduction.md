[[!meta title="Game Theory: a Critical Introduction"]]

## Index

* Difference between meteorological and traffic-jam-style predictions: 18.
* Frequent assumption (but not always) on game theory that individuals knows
  the rules of the game; that they even known their inner motives: 28.
* Individualism, separation of structure and choice, 31.
* MAD, 86; 88.
* Elimination of non-credible threats, 88.

# Excerpts

## Intro

What is game theory:

    In many respects this enthusiasm is not difficult to understand. Game theory
    was probably born with the publication of The Theory of Games and Economic
    Behaviour by John von Neumann and Oskar Morgenstern (first published in
    1944 with second and third editions in 1947 and 1953). They defined a game
    as any interaction between agents that is governed by a set of rules
    specifying the possible moves for each participant and a set of outcomes for
    each possible combination of moves.

How it can help:

    If game theory does make a further substantial contribution, then we
    believe that it is a negative one. The contribution comes through
    demonstrating the limits of a particular form of individualism in social
    science: one based exclusively on the model of persons as preference satisfiers.
    This model is often regarded as the direct heir of David Hume’s (the 18th
    century philosopher) conceptualisation of human reasoning and motivation. It
    is principally associated with what is known today as rational choice theory, or
    with the (neoclassical) economic approach to social life (see Downs, 1957, and
    Becker, 1976). Our main conclusion on this theme (which we will develop
    through the book) can be rephrased accordingly: we believe that game theory
    reveals the limits of ‘rational choice’ and of the (neoclassical) economic
    approach to life. In other words, game theory does not actually deliver Jon
    Elster’s ‘solid microfoundations’ for all social science; and this tells us
    something about the inadequacy of its chosen ‘microfoundations’.

Assumptions:

    three key assumptions: agents are instrumentally
    rational (section 1.2.1); they have common knowledge of this rationality
    (section 1.2.2); and they know the rules of the game (section 1.2.3).
    These assumptions set out where game theory stands on the big questions of
    the sort ‘who am I, what am I doing here and how can I know about either?’.
    The first and third are ontological. 1 They establish what game theory takes as
    the material of social science: in particular, what it takes to be the essence of
    individuals and their relation in society. The second raises epistemological
    issues 2 (and in some games it is not essential for the analysis). It is concerned
    with what can be inferred about the beliefs which people will hold about how
    games will be played when they have common knowledge of their rationality.

Instrumental rationality (_Homo economicus_):

    We spend more time discussing these assumptions than is perhaps usual in
    texts on game theory because we believe that the assumptions are both
    controversial and problematic, in their own terms, when cast as general
    propositions concerning interactions between individuals. This is one respect
    in which this is a critical introduction. The discussions of instrumental
    rationality and common knowledge of instrumental rationality (sections 1.2.1
    and 1.2.2), in particular, are indispensable for anyone interested in game
    theory. In comparison section 1.2.3 will appeal more to those who are
    concerned with where game theory fits in to the wider debates within social

    [...]

    Individuals who are instrumentally rational have preferences over various
    ‘things’, e.g. bread over toast, toast and honey over bread and butter, rock
    over classical music, etc., and they are deemed rational because they select
    actions which will best satisfy those preferences. One of the virtues of this
    model is that very little needs to be assumed about a person’s preferences.
    Rationality is cast in a means-end framework with the task of selecting the
    most appropriate means for achieving certain ends (i.e. preference
    satisfaction); and for this purpose, preferences (or ‘ends’) must be coherent
    in only a weak sense that we must be able to talk about satisfying them more
    or less. Technically we must have a ‘preference ordering’ because it is only
    when preferences are ordered that we will be able to begin to make
    judgements about how different actions satisfy our preferences in different
    degrees.

    [...]

    Thus it appears a promisingly general model of action. For instance, it could
    apply to any type of player of games and not just individuals. So long as the
    State or the working class or the police have a consistent set of objectives/
    preferences, then we could assume that it (or they) too act instrumentally so
    as to achieve those ends. Likewise it does not matter what ends a person
    pursues: they can be selfish, weird, altruistic or whatever; so long as they
    consistently motivate then people can still act so as to satisfy them best.

An agent is "rational" in this conext when they have preference ordering" and
if "they select the action that maximizes those preferences:

    Readers familiar with neoclassical Homo economicus will need no further
    introduction. This is the model found in standard introductory texts, where
    preferences are represented by indifference curves (or utility functions) and
    agents are assumed rational because they select the action which attains the
    highest feasible indifference curve (maximises utility). For readers who have
    not come across these standard texts or who have forgotten them, it is worth
    explaining that preferences are sometimes represented mathematically by a
    utility function. As a result, acting instrumentally to satisfy best one’s
    preferences becomes the equivalent of utility maximising behaviour.

Reason and slavery:

    Even when we accept the Kantian argument, it is plain that reason’s
    guidance is liable to depend on characteristics of time and place. For
    example, consider the objective of ‘owning another person’. This obviously
    does not pass the test of the categorical imperative since all persons could
    not all own a person. Does this mean then we should reject slave-holding? At
    first glance, the answer seems to be obvious: of course, it does! But notice it
    will only do this if slaves are considered people. Of course we consider
    slaves people and this is in part why we abhor slavery, but ancient Greece
    did not consider slaves as people and so ancient Greeks would not have been
    disturbed in their practice of slavery by an application of the categorical
    imperative.

Reason dependent on culture:

    Wittgenstein suggests that if you want to know why people act in the way that
    they do, then ultimately you are often forced in a somewhat circular fashion to
    say that such actions are part of the practices of the society in which those
    persons find themselves. In other words, it is the fact that people behave in a
    particular way in society which supplies the reason for the individual person to
    act: or, if you like, actions often supply their own reasons. This is shorthand
    description rather than explanation of Wittgenstein’s argument, but it serves to
    make the connection to an influential body of psychological theory which
    makes a rather similar point.

Cognitive dissonance and free market proponents:

    Festinger’s (1957) cognitive dissonance theory proposes a model where
    reason works to ‘rationalise’ action rather than guide it. The point is that we
    often seem to have no reason for acting the way that we do. For instance, we
    may recognise one reason for acting in a particular way, but we can equally
    recognise the pull of a reason for acting in a contrary fashion. Alternatively,
    we may simply see no reason for acting one way rather than another. In such
    circumstances, Festinger suggests that we experience psychological distress. It
    comes from the dissonance between our self-image as individuals who are
    authors of our own action and our manifest lack of reason for acting. It is like
    a crisis of self-respect and we seek to remove it by creating reasons. In short
    we often rationalise our actions ex post rather than reason ex ante to take them
    as the instrumental model suggests.

    [...]

    Research has shown that people seek out and read advertisements for
    the brand of car they have just bought. Indeed, to return us to economics, it is
    precisely this insight which has been at the heart of one of the Austrian and
    other critiques of the central planning system when it is argued that planning
    can never substitute for the market because it presupposes information
    regarding preferences which is in part created in markets when consumers
    choose.

Infinite regress of the economics of information acquisiton (i.e learning, eg.
from a secret service):

    Actually most game theorists seem to agree on one aspect of the problem
    of belief formation in the social world: how to update beliefs in the presence
    of new information. They assume agents will use Bayes’s rule. This is explained
    in Box 1.6. We note there some difficulties with transplanting a technique from
    the natural sciences to the social world which are related to the observation we
    have just made. We focus here on a slightly different problem. Bayes provides
    a rule for updating, but where do the original (prior) expectations come from?
    Or to put the question in a different way: in the absence of evidence, how do
    agents form probability assessments governing events like the behaviour of
    others?

    There are two approaches in the economics literature. One responds by
    suggesting that people do not just passively have expectations. They do not
    just wait for information to fall from trees. Instead they make a conscious
    decision over how much information to look for. Of course, one must have
    started from somewhere, but this is less important than the fact that the
    acquisition of information will have transformed these original ‘prejudices’.
    The crucial question, on this account, then becomes: what determines the
    amount of effort agents put into looking for information? This is deceptively
    easy to answer in a manner consistent with instrumental rationality. The
    instrumentally rational agent will keep on acquiring information to the point
    where the last bit of search effort costs her or him in utility terms the same
    amount as the amount of utility he or she expects to get from the
    information gained by this last bit of effort. The reason is simple. As long as
    a little bit more effort is likely to give the agent more utility than it costs,
    then it will be adding to the sum of utilities which the agent is seeking to
    maximise.

    [...]

    This looks promising and entirely consistent with the definition of
    instrumentally rational behaviour. But it begs the question of how the agent
    knows how to evaluate the potential utility gains from a bit more information
    _prior to gaining that information_. Perhaps he or she has formulated expectations of
    the value of a little bit more information and can act on that. But then the
    problem has been elevated to a higher level rather than solved. How did he or
    she acquire that expectation about the value of information? ‘By acquiring
    information about the value of information up to the point where the
    marginal benefits of this (second-order) information were equal to the costs’,
    is the obvious answer. But the moment it is offered, we have the beginnings of
    an infinite regress as we ask the same question of how the agent knows the
    value of this second-order information. To prevent this infinite regress, we
    must be guided by something _in addition_ to instrumental calculation. But this
    means that the paradigm of instrumentally rational choices is incomplete. The
    only alternative would be to assume that the individual _knows_ the benefits that
    he or she can expect on average from a little more search (i.e. the expected
    marginal benefits) because he or she knows the full information set. But then
    there is no problem of how much information to acquire because the person
    knows everything!

Infinite recursion of the common knowledge (CKR):

    If you want to form an expectation about what somebody does, what
    could be more natural than to model what determines their behaviour and
    then use the model to predict what they will do in the circumstances that
    interest you? You could assume the person is an idiot or a robot or whatever,
    but most of the time you will be playing games with people who are
    instrumentally rational like yourself and so it will make sense to model your
    opponent as instrumentally rational. This is the idea that is built into the
    analysis of games to cover how players form expectations. We assume that
    there is common knowledge of rationality held by the players. It is at once
    both a simple and complex approach to the problem of expectation
    formation. The complication arises because with common knowledge of
    rationality I know that you are instrumentally rational and since you are
    rational and know that I am rational you will also know that I know that you
    are rational and since I know that you are rational and that you know that I
    am rational I will also know that you know that I know that you are rational
    and so on…. This is what common knowledge of rationality means.

    [...]

    It is difficult to pin down because common knowledge of X
    (whatever X may be) cannot be converted into a finite phrase beginning with ‘I
    know…’. The best one can do is to say that if Jack and Jill have common
    knowledge of X then ‘Jack knows that Jill knows that Jack knows …that Jill
    knows that Jack knows…X’—an infinite sentence. The idea reminds one of
    what happens when a camera is pointing to a television screen that conveys the
    image recorded by the very same camera: an infinite self-reflection. Put in this
    way, what looked a promising assumption suddenly actually seems capable of
    leading you anywhere.

    [...]

    The problem of expectation formation spins hopelessly out of control.

    Nevertheless game theorists typically assume CKR and many of them, and
    certainly most people who apply game theory in economics and other
    disciplines

Uniformity: Consistent Alignment of Beliefs (CAB), another weak assumption
based on Harsanyi doctrine requiring equal information; followed by a
comparison with Socract dialectics:

    Put informally, the notion of _consistent alignment of beliefs_ (CAB) means that
    no instrumentally rational person can expect another similarly rational
    person who has the same infor mation to develop different thought
    processes. Or, alternatively, that no rational person expects to be surprised
    by another rational person. The point is that if the other person’s thought is
    genuinely moving along rational lines, then since you know the person is
    rational and you are also rational then your thoughts about what your
    rational opponent might be doing will take you on the same lines as his or
    her own thoughts. The same thing applies to others provided they respect
    _your_ thoughts. So your beliefs about what your opponents will do are
    consistently aligned in the sense that if you actually knew their plans, you
    would not want to change your beliefs; and if they knew your plans they
    would not want to change the beliefs they hold about you and which support
    their own planned actions.

    Note that this does not mean that everything can be deterministically
    predicted.

Reason reflecting on itself:

    These observations are only designed to signal possible trouble ahead
    and we shall examine this issue in greater detail in Chapters 2 and 3. We
    conclude the discussion now with a pointer to wider philosophical currents.
    Many decades before the appearance of game theor y, the Ger man
    philosophers G.F.W.Hegel and Immanuel Kant had already considered the
    notion of the self-conscious reflection of human reasoning on itself. Their
    main question was: can our reasoning faculty turn on itself and, if it can,
    what can it infer? Reason can certainly help persons develop ways of
    cultivating the land and, therefore, escape the tyranny of hunger. But can it
    understand how it, itself, works? In game theory we are not exactly
    concerned with this issue but the question of what follows from common
    knowledge of rationality has a similar sort of reflexive structure. When
    reason knowingly encounters itself in a game, does this tell us anything
    about what reason should expect of itself?

    What is revealing about the comparison between game theory and
    thinkers like Kant and Hegel is that, unlike them, game theory offers
    something settled in the form of CAB. What is a source of delight,
    puzzlement and uncertainty for the German philosophers is treated as a
    problem solved by game theory. For instance, Hegel sees reason reflecting
    on reason as it reflects on itself as part of the restlessness which drives
    human history. This means that for him there are no answers to the
    question of what reason demands of reason in other people outside of
    human history. Instead history offers a changing set of answers. Likewise
    Kant supplies a weak answer to the question. Rather than giving substantial
    advice, reason supplies a negative constraint which any principle of
    knowledge must satisfy if it is to be shared by a community of rational
    people: any rational principle of thought must be capable of being followed
    by all. O’Neill (1989) puts the point in the following way:

        [Kant] denies not only that we have access to transcendent meta-
        physical truths, such as the claims of rational theology, but also that
        reason has intrinsic or transcendent vindication, or is given in
        consciousness. He does not deify reason. The only route by which we
        can vindicate certain ways of thinking and acting, and claim that those
        ways have authority, is by considering how we must discipline our
        thinking if we are to think or act at all. This disciplining leads us not to
        algorithms of reason, but to certain constraints on all thinking,
        communication and interaction among any plurality. In particular we are
        led to the principle of rejecting thought, act or communication that is
        guided by principles that others cannot adopt.
        (O’Neill p. 27)

Summary:

    To summarise, game theory is avowedly Humean in orientation.  [...]
    The second [aspect] is that game theorists seem to assume _too much_ on behalf
    of reason [even more than Hume did].

Giddens, Wittgenstein language games and the "organic or holistic view of the relation between action and structure" (pages 30-31):

    The question is ontological and it connects directly with the earlier
    discussion of instrumental rationality. Just as instrumental rationality is not
    the only ontological view of what is the essence of human rationality, there is
    more than one ontological view regarding the essence of social interaction.
    Game theory works with one view of social interaction, which meshes well
    with the instrumental account of human rationality; but equally there are
    other views (inspired by Kant, Hegel, Marx, Wittgenstein) which in turn
    require different models of (rational) action.

State (pages 32-33):

    Perhaps the most famous example of this type of
    institutional creation comes from the early English philosopher Thomas
    Hobbes who suggested in Leviathan that, out of fear of each other,
    individuals would contract with each other to form a State. In short, they
    would accept the absolute power of a sovereign because the sovereign’s
    ability to enforce contracts enables each individual to transcend the dog-
    eat-dog world of the state of nature, where no one could trust anyone and
    life was ‘short, nasty and brutish’.

    Thus, the key individualist move is to draw attention to the way that
    structures not only constrain; they also enable (at least those who are in a
    position to create them). It is the fact that they enable which persuades
    individuals consciously (as in State formation) or unconsciously (in the case
    of those which are generated spontaneously) to build them. To bring out
    this point and see how it connects with the earlier discussion of the
    relation between action and structure it may be helpful to contrast Hobbes
    with Rousseau. Hobbes has the State emerging from a contract between
    individuals because it serves the interests of those individuals. Rousseau
    also talked of a social contract between individuals, but he did not speak
    this individualist language. For him, the political (democratic) process was
    not a mere means of ser ving persons’ interests by satisfying their
    preferences. It was also a process which changed people’s preferences. People
    were socialised, if you like, and democracy helped to create a new human
    being, more tolerant, less selfish, better educated and capable of cherishing
    the new values of the era of Enlightenment. By contrast, Hobbes’ men and
    women were the same people before and after the contract which created
    the State. 4

Game theory as justification of individualism (pages 32-33), which reminds the
discussion made by Dany-Robert Dufour in La Cite Perverse; it is also noted
that the State is considered a "collective action agency":

    Where do structures come from when they are separate from actions?  An
    ambitious response which distinguishes methodological individualists of all
    types is that the structures are merely the deposits of previous interactions
    (potentially understood, of course, as games). This answer may seem to threaten
    an infinite regress in the sense that the structures of the previous
    interaction must also be explained and so on. But, the individualist will want
    to claim that ultimately all social str uctures spring from interactions
    between some set of asocial individuals; this is why it is ‘individualist’.

    [...]

    Returning to game theory’s potential contribution, we can see that, in so
    far as individuals are modelled as Humean agents, game theory is well placed
    to help assess the claims of methodological individualists. After all, game
    theory purports to analyse social interaction between individuals who, as
    Hume argued, have passions and a reason to serve them. Thus game theory
    should enable us to examine the claim that, beginning from a situation with
    no institutions (or structures), the self-interested behaviour of these
    instrumentally rational agents will either bring about institutions or fuel their
    evolution. An examination of the explanatory power of game theory in such
    settings is one way of testing the individualist claims.
    In fact, as we shall see in subsequent chapters, the recurring difficulty

    [...]

    Suppose we take the methodological individualist route and see
    institutions as the deposits of previous interactions between individuals.
    Individualists are not bound to find that the institutions which emerge in
    this way are fair or just. Indeed, in practice, many institutions reflect the
    fact that they were created by one group of people and then imposed on
    other groups. All that any methodological individualist is committed to is
    being able to find the origin of institutions in the acts of individuals qua
    individuals. The political theory of liberal individualism goes a stage
    further and tries to pass judgement on the legitimacy of particular
    institutions. Institutions in this view are to be regarded as legitimate in so
    far as all individuals who are governed by them would have broadly
    ‘agreed’ to their creation.

    Naturally, much will turn on how ‘agreement’ is to be judged because
    people in desperate situations will often ‘agree’ to the most desperate of
    outcomes. Thus there are disputes over what constitutes the appropriate
    reference point (the equivalent to Hobbes’s state of nature) for judging
    whether people would have agreed to such and such an arrangement. We set
    aside a host of further problems which emerge the moment one steps outside
    liberal individualist premises and casts doubt over whether people’s
    preferences have been autonomously chosen. Game theory has little to
    contribute to this aspect of the dispute. However, it does make two
    significant contributions to the discussions in liberal individualism with
    respect to how we might judge ‘agreement’.

Prisioner's dilemma and the hobbesian argument for the creation of a State (pages 36-37):
resolution would require a higher State in the next upper level of recursion:

    Finally there is the prisoners’ dilemma game (to which we have dedicated the
    whole of Chapter 5 and much of Chapter 6). Recall the time when there were
    still two superpowers each of which would like to dominate the other, if
    possible. They each faced a choice between arming and disarming. When both
    arm or both disarm, neither is able to dominate the other. Since arming is
    costly, when both decide to arm this is plainly worse than when both decide to
    disarm. However, since we have assumed each would like to dominate the
    other, it is possible that the best outcome for each party is when that party
    arms and the other disarms since although this is costly it allows the arming
    side to dominate the other. These preferences are reflected in the ‘arbitrary’
    utility pay-offs depicted in Figure 1.4.
    
    Game theory makes a rather stark prediction in this game: both players will
    arm (the reasons will be given later). It is a paradoxical result because each
    does what is in their own interest and yet their actions are collectively self-
    defeating in the sense that mutual armament is plainly worse than the
    alternative of mutual disarmament which was available to them (pay-off 1 for
    utility pay-offs depicted in Figure 1.4.
    
    Game theory makes a rather stark prediction in this game: both players will
    arm (the reasons will be given later). It is a paradoxical result because each
    does what is in their own interest and yet their actions are collectively self-
    defeating in the sense that mutual armament is plainly worse than the
    alternative of mutual disarmament which was available to them (pay-off 1 for
    each rather than 2). The existence of this type of interaction together with the
    inference that both will arm has provided one of the strongest arguments for
    the creation of a State. This is, in effect, Thomas Hobbes’s argument in
    Leviathan. And since our players here are themselves States, both countries
    should agree to submit to the authority of a higher State which will enforce an
    agreement to disar m (an argument for a strong, independent, United
    Nations?).

Too much trust in that type of instrumental rationality might lead to lower
outcomes in some games:

    The term rationalisable has been used to describe such strategies because a
    player can defend his or her choice (i.e. rationalise it) on the basis of beliefs
    about the beliefs of the opponent which are not inconsistent with the game’s
    data. However, to pull this off, we need ‘more’ commonly known rationality
    than in the simpler games in Figures 2.1 and 2.3. Looking at Figure 2.4 we see
    that outcome (100, 90) is much more inviting than the rationalisable outcome
    (1, 1). It is the deepening confidence in each other’s instrumental rationality
    (fifth-order CKR, to be precise) which leads our players to (1, 1). In summary
    notation, the rationalisable strategies R2, C2 are supported by the following
    train of thinking (which reflects the six steps described earlier):

    -- 48

Nash-equilibrium: self-confirming strategy:

    A set of rationalisable strategies (one for each player) are in a Nash
    equilibrium if their implementation confirms the expectations of each player
    about the other’s choice.  Put differently, Nash strategies are the only
    rationalisable ones which, if implemented, confirm the expectations on which
    they were based. This is why they are often referred to as self-confirming
    strategies or why it can be said that this equilibrium concept requires that
    players’ beliefs are consistently aligned (CAB).

    -- 53

Arguments against CAB:

    In the same spirit, it is sometimes argued (borrowing a line from John von
    Neumann and Oskar Morgenstern) that the objective of any analysis of games is
    the equivalent of writing a book on how to play games; and the minimum
    condition which any piece of advice on how to play a game must satisfy is
    simple: the advice must remain good advice once the book has been published.
    In other words, it could not really be good advice if people would not want to
    follow it once the advice was widely known. On this test, only (R2, C2) pass,
    since when the R player follows the book’s advice, the C player would want to
    follow it as well, and vice versa. The same cannot be said of the other
    rationalisable strategies. For instance, suppose (R1, C1) was recommended: then
    R would not want to follow the advice when C is expected to follow it by
    selecting C1 and likewise, if R was expected to follow the advice, C would not
    want to.
    
    Both versions of the argument with respect to what mutual rationality entails
    seem plausible. Yet, there is something odd here. Does respect for each other’s
    rationality lead each person to believe that neither will make a mistake in a
    game? Anyone who has talked to good chess players (perhaps the masters of
    strategic thinking) will testify that rational persons pitted against equally
    rational opponents (whose rationality they respect) do not immediately assume
    that their opposition will never make errors. On the contrary, the point in
    chess is to engender such errors! Are chess players irrational then?  One is
    inclined to answer no, but why? And what is the difference as

    -- 57

Limits conceptualizing reason as an algorithm ("Humean approach to reason
is algorithmic"):

    Harsanyi doctrine seems to depend on a powerfully algorithmic and controversial
    view of reason. Reason on this account (at least in an important part) is akin
    to a set of rules of inference which can be used in moving from evidence to
    expectations. That is why people using reason (because they are using the same
    algorithms) should come to the same conclusion. However, there is genuine
    puzzlement over whether such an algorithmic view of reason can apply to all
    circumstances. Can any finite set of rules contain rules for their own
    application to all possible circumstances? The answer seems to be no, since
    under some sufficiently detailed level of description there will be a question of
    whether the rule applies to this event and so we shall need rules for applying
    the rules for applying the rules. And as there is no limit to the detail of the
    description of events, we shall need rules for applying the rules for applying
    the rules, and so on to infinity. In other words, every set of rules will require
    creative interpretation in some circumstances and so in these cases it is
    perfectly possible for two individuals who share the same rules to hold
    divergent expectations.

    This puts a familiar observation from John Maynard Keynes and Frank
    Knight regarding genuine uncertainty in a slightly different way, but
    nevertheless it yields the same conclusion. There will be circumstances under
    which individuals are unable to decide rationally what probability assessment
    to attach to events because the events are uncertain and so it should not be
    surprising to find that they disagree. Likewise, the admiration for
    entrepreneurship found among economists of the Austrian school depends on
    the existence of uncertainty. Entrepreneurship is highly valued precisely
    because, as a result of uncertainty, people can hold different expectations
    regarding the future. In this context, the entrepreneurs are those who back
    their judgement against that of others and succeed. In other words, there
    would be no job for entrepreneurs if we all held common expectations in a
    world ruled by CAB!
    
    A similar conclusion regarding ineliminable uncertainty is shared by social
    theorists who have been influenced by the philosophy of Kant. They deny that
    reason should be understood algorithmically or that it always supplies answers
    as to what to do. For Kantians reason supplies a critique of itself which is the
    source of negative restraints on what we can believe rather than positive
    instructions as to what we should believe. Thus the categorical imperative (see
    section 1.2.1), which according to Kant ought to determine many of our
    significant choices, is a sieve for beliefs and it rarely singles out one belief.
    Instead, there are often many which pass the test and so there is plenty of
    room for disagreement over what beliefs to hold.

    Perhaps somewhat surprisingly though, a part of Kant’s argument might
    lend support to the Nash equilibrium concept. In particular Kant thought that
    rational agents should only hold beliefs which are capable of being
    universalised. This idea, taken by itself, might prove a powerful ally of Nash.
    [...] Of course, a full Kantian perspective is
    likely to demand rather more than this and it is not typically adopted by game
    theorists. Indeed such a defence of Nash would undo much of the
    foundations of game theory: for the categorical imperative would even
    recommend choosing dominated strategies if this is the type of behaviour that
    each wished everyone adopted. Such thoughts sit uncomfortably with the
    Humean foundations of game theory and we will not dwell on them for now.
    Instead, since the spirit of the Humean approach to reason is algorithmic, we
    shall continue discussing the difficulties with the Harsanyi—Aumann defence
    of Nash.

    -- 58-60

"Irrational" plays which might intend to send a message to other players:

    Indeed why should one assume in this way that players cannot (or
    should not) try to make statements about themselves through patterning
    their ‘trembles? The question becomes particularly sharp once it is recalled
    that, on the conventional account, players must expect that there is always
    some chance of a tremble. Trembles in this sense are part of normal
    behaviour, and the critics argue that agents may well attempt to use them
    as a medium for signalling something to each other. Of course, players will
    not do so if they believe that their chosen pattern is going to be ignored
    by others. But that is the point: why assume that this is what they will
    believe from the beginning, especially when agents can see that the
    generally accepted use of trembles as signals might secure a better
    outcome for both players [...]?

    Note that this is not an argument against backward induction per se: it is an
    argument against assuming CKR while working out beliefs via backward
    induction (i.e. a criticism of Nash backward induction). When agents consider
    patterning their ‘trembles’, they project forward about future behaviour given
    that there are trembles now or in the past. What makes it ambiguous whether
    they should do this, or stick to Nash backward induction instead, is that there
    is no uniquely rational way of playing games like Figures 3.5 or 3.6 (unlike the
    race to 20 game in which there is). In this light, the subgame perfect Nash
    equilibrium offers one of many possible scenarios of how rational agents will
    behave.

    -- 93

Why not expand this affirmation so _any_ move to signal some intention?

## Misc

Page 101:

    Hence these refinements (e.g. proper equilibria), likethe Nash
    equilibrium project itself, seem to have to appeal to somethingother
    than the traditional assumptions of game theory regarding
    rationalaction in a social context.

Page 102:

    regarding the relation betweenconvention following and instrumental
    rationality. The worry here takes usback to the discussion of section
    1.2.3 where for instance it was suggested thatconventions might best be
    understood in the way suggested by Wittgenstein orHegel. In short, the
    acceptance of convention may actually require a radicalreassessment of
    the ontological foundations of game theory.

Page 102:

    actually require a radicalreassessment of the ontological foundations
    of game theory.

Page 103:

    Why not give up on the Nash concept altogether? This ‘giving up’ might
    takeon one of two forms. Firstly, game theory could appeal to the
    concept ofrationalisable strategies (recall section 2.4 of Chapter 2)
    which seemuncontentiously to flow from the assumptions of instrumental
    rationalityand CKR. The difficulty with such a move is that it concedes
    that gametheory is unable to say much about many games (e.g. Figures
    2.6, 2.12, etc.).Naturally, modesty of this sort might be entirely
    appropriate for gametheory, although it will diminish its claims as a
    solid foundation for socialscience.

Page 104:

    Unlike the instrumentally rational model, for Hegelians and Marxists
    actionbased on preferences feeds back to affect preferences, and so on,
    in an everunfolding chain. (See Box 3.1 for a rather feeble attempt to
    blend desires andbeliefs.) Likewise some social psychologists might
    argue that the key to actionlies less with preferences and more with
    the cognitive processes used bypeople; and consequently we should
    address ourselves to understanding theseprocesses.

Page 105:

    105

Page 106:

    Quite simply, the significant social processes which write history
    cannot beunderstood through the lens of instrumental rationality. This
    destines gametheory to a footnote in some future text on the history of
    social theory. Welet the reader decide.3

Page 108:

    Thirdly, the sociology of the discipline may provide further clues.
    Twoconditions would seem to be essential for the modern development of
    adiscipline within the academy. Firstly the discipline must be
    intellectuallydistinguishable from other disciplines. Secondly, there
    must be some barriersto the amateur pursuit of the discipline. (A third
    condition which goes withoutsaying is that the discipline must be able
    to claim that what it does ispotentially worth while.) The first
    condition reduces the competition fromwithin the academy which might
    come from other disciplines (to do thisworthwhile thing) and the second
    ensures that there is no effectivecompetition from outside the academy.
    In this context, the rational choicemodel has served economics very
    well. It is the distinguishing intellectualfeature of economics as a
    discipline and it is amenable to such formalisationthat it keeps most
    amateurs well at bay. Thus it is plausible to argue that thesuccess of
    economics as a discipline within the social sciences has been
    closelyrelated to its championing of the rational choice model.

Page 108:

    kind of amnesia or lobotomy which thediscipline seems to have suffered
    regarding most things philosophical duringthe postwar period.

Page 108:

    It isoften more plausible to think of the academy as a battleground
    betweendisciplines rather than between ideas and the disciplines which
    have goodsurvival features (like the barriers to entry identified
    above)

Page 109:

    explanations willonly prosper in so far as they are both superior and
    they are not institutionallyundermined by the rise of neoclassical
    economics and the demise ofsociology. It is not necessary to see these
    things conspiratorially to see thepoint of this argument. All academics
    have fought their corner in battles overresources and they always use
    the special qualities of their discipline asammunition in one way or
    another. Thus one might explain in functionalist termsthe mystifying
    attachment of economics and game theory to Nash.

Page 110:

    We have no special reason to prioritise one strand of our
    proposedexplanation. Yet, there is more than a hint of irony in the
    last suggestionbecause Jon Elster has often championed game theory and
    its use of the Nashequilibrium concept as an alternative to functional
    arguments in social science.Well, if the use of Nash by game theorists
    is itself to be explainedfunctionally, then…

Page 111:

    Liberal theorists often explain the State with reference to state of
    nature. Forinstance, within the Hobbesian tradition there is a stark
    choice between astate of nature in which a war of all against all
    prevails and a peacefulsociety where the peace is enforced by a State
    which acts in the interest ofall. The legitimacy of the State derives
    from the fact that people who wouldotherwise live in Hobbes’s state of
    nature (in which life is ‘brutish, nasty andshort’) can clearly see the
    advantages of creating a State. Even if a State had

Page 111:

    not surfaced historically for all sorts of other reasons, it would have
    to beinvented.Such a hypothesised ‘invention’ would require a
    cooperative act of comingtogether to create a State whose purpose will
    be to secure rights over life andproperty. Nevertheless, even if all
    this were common knowledge, it wouldnot guarantee that the State will
    be created. There is a tricky further issuewhich must be resolved. The
    people must agree to the precise property rightswhich the State will
    defend and this is tricky because there are typically avariety of
    possible property rights and the manner in which the benefits ofpeace
    will be distributed depends on the precise property rights which
    areselected (see Box 4.1).In other words, the common interest in peace
    cannot be the onlyelement in the liberal explanation of the State, as
    any well-defined andpoliced property rights will secure the peace. The
    missing element is anaccount of how a particular set of property rights
    are selected and thiswould seem to require an analysis of how people
    resolve conflicts ofinterest. This is where bargaining theory promises
    to make an importantcontribution to the liberal theory of the State
    because it is concernedprecisely with interactions of this sort.

Page 112:

    State creation in Hobbes’s world provides one example (which
    especiallyinterests us because it suggests that bargaining theory may
    throw light onsome of the claims of liberal political theory with
    respect to the State), butthere are many others.

Page 113:

    The creation of the institutions for enforcing agreements (like the
    State)which are presumed by cooperative game theory requires as we have
    seenthat agents first solve the bargaining problem non-cooperatively.

Page 113:

    Indeed for this reason, and following thepractice of most game
    theorists, we have so far discussed the non-cooperative play of games
    ‘as if ’ there was no communication, therebyimplicitly treating any
    communication which does take place in the absenceof an enforcement
    agency as so much ‘cheap talk’

Page 113:

    In cooperative games agents cantalk to each other and make agreements
    which are binding on later play. Innon-cooperative games, no agreements
    are binding. Players can say whateverthey like, but there is no
    external agency which will enforce that they dowhat they have said they
    will do.

Page 114:

    Thus it will have shown not just what sort of State rational agents
    mightagree to create, but also how rational agents might solve a host
    of bargainingproblems in social life. Unfortunately we have reasons to
    doubt therobustness of this analysis and it is not difficult to see our
    grounds forscepticism. If bargaining games resemble the hawk-dove game
    and thediscussion in Chapter 2 is right to point to the existence of
    multipleequilibria in this game under the standard assumptions of game
    theory, thenhow does bargaining theory suddenly manage to generate a
    uniqueequilibrium?

Page 114:

    114face value, the striking result of the non-cooperative analysis of
    thebargaining problem is that it yields the same solution to the
    bargainingproblem as the axiomatic approach. If this result is robust,
    then it seems thatgame theory will have done an extraordinary service
    by showing thatbargaining problems have unique solutions (whichever
    route is preferred).Thus it will have shown not just what sort of State
    rational agents mightagree to create, but also how rational agents
    might solve a host of bargainingproblems in social life. Unfortunately
    we have reasons to doubt therobustness of this analysis and it is not
    difficult to see our grounds forscepticism. If bargaining games
    resemble the hawk-dove game and thediscussion in Chapter 2 is right to
    point to the existence of multipleequilibria in this game under the
    standard assumptions of game theory, thenhow does bargaining theory
    suddenly manage to generate a uniqueequilibrium?

Page 116:

    A threat or promise which, if carried out, costs more tothe agent who
    issued it than if it is not carried out, iscalled an incredible threat
    or promise.

Page 138:

    However, this failure topredict should be welcomed by John Rawls and
    Robert Nozick as it providesan opening to their contrasting views of
    what counts as justice betweenrational agents.

Page 138:

    If the Nash solution were unique, then game theory would have
    answeredan important question at the heart of liberal theory over the
    type of Statewhich rational agents might agree to create. In addition,
    it would have solveda question in moral philosophy over what justice
    might demand in this and avariety of social interactions. After all,
    how to divide the benefits from socialcooperation seems at first sight
    to involve a tricky question in moralphilosophy concerning what is
    just, but if rational agents will only ever agreeon the Nash division
    then there is only one outcome for rational agents.Whether we want to
    think of this as just seems optional. But if we do or ifwe think that
    justice is involved, then we will know, and for onceunambiguously, what
    justice apparently demands between instrumentallyrational
    agents.Unfortunately, though, it seems we cannot draw these inferences
    becausethe Nash solution is not the unique outcome. Accepting this
    conclusion, weare concerned in this section with what bargaining theory
    then contributes tothe liberal project of examining the State as if it
    were the result of rational

Page 140:

    behind the veil of ignorance.

Page 142:

    Torture: Another example in moral philosophy is revealed by the problem
    oftorture for utilitarians. For instance, a utilitarian calculation
    focuses onoutcomes by summing the individual utilities found in
    society. In so doing itdoes not enquire about the fairness or otherwise
    of the processes responsiblefor generating those utilities with the
    result that it could sanction torture whenthe utility gain of the
    torturer exceeds the loss of the person being tortured.Yet most people
    would feel uncomfortable with a society which sanctionedtorture on
    these grounds because it unfairly transgresses the ‘rights’ of
    thetortured.

Page 143:

    Granted that society (andthe State) are not the result of some
    living-room negotiation, what kind of“axioms” would have generated the
    social outcomes which we observe in agiven society?’ That is, even if
    we reject the preceding fictions (i.e. of the Stateas a massive
    resolution of an n-person bargaining game, or of the veil ofignorance)
    as theoretically and politically misleading, we may still
    pinpointcertain axioms which would have generated the observed income
    distributions(or distributions of opportunities, social roles, property
    rights, etc.) as a resultof an (utterly) hypothetical bargaining game.

Page 143:

    Roemer (1988) considers a problem faced by an international
    agencycharged with distributing some resources with the aim of
    improving health(say lowering infant mortality rates). How should the
    authority distributethose resources? This is a particularly tricky
    issue because different countriesin the world doubtless subscribe to
    some very different principles which theywould regard as relevant to
    this problem; and so agreement on a particularrule seems unlikely.
    Nevertheless, he suggests that we approach the problemby considering
    the following constraints (axioms) which we might want toapply to the
    decision rule because they might be the object of significantagreement.

Page 144:

    rule which allocates resources in such a way as to raise the country
    with thelowest infant survival rate to that of the second lowest, and
    then if the budgethas not been exhausted, it allocates resources to
    these two countries until theyreach the survival rate of the third
    lowest country, and so on until the budgetis exhausted.

Page 147:

    It is tempting to think that the problem only arises here because
    theprisoners cannot communicate with one another. If they could get
    togetherthey would quickly see that the best for both comes from ‘not
    confessing’.But as we saw in the previous chapter, communication is not
    all that isneeded. Each still faces the choice of whether to hold to an
    agreement that

Page 148:

    The recognition ofthis predicament helps explain why individuals might
    rationally submit to theauthority of a State, which can enforce an
    agreement for ‘peace’. Theyvoluntarily relinquish some of their freedom
    that they enjoy in the(hypothesised) state of nature to the State
    because it unlocks the prisoners’dilemma. (It should be added perhaps
    that this is not to be taken as a literalaccount of how all States or
    enforcement agencies arise. The point of theargument is to demonstrate
    the conditions under which a State or enforcementagency would enjoy
    legitimacy among a population even though it restrictedindividual
    freedoms.)

Page 148:

    their normal business with the result that they prosper and enjoy a
    more‘commodious’ living (as Hobbes phrased it), choosing strategy
    ‘peace’ is like‘not confessing’ above; when everyone behaves in this
    manner it is much betterthan when they all choose ‘war’ (’confess’).
    However, and in spite of wideranging recognition that peace is better
    than war, the same prisoners’ dilemmaproblem surfaces and leads to war.

Page 148:

    While Hobbes thought that the authority of the State should be absolute
    soas to discourage any cheating on ‘peace’, he also thought the scope
    of itsinterventions in this regard would be quite minimal. In contrast
    much of themodern fascination with the prisoners’ dilemma stems from
    the fact that theprisoners’ dilemma seems to be a ubiquitous feature of
    social life. Forinstance, it plausibly lies at the heart of many
    problems which groups

Page 148:

    they have struck over ‘not confessing’. Is it in the interest of either
    party tokeep to such an agreement? No, a quick inspection reveals that
    the bestaction in terms of pay-offs is still to ‘confess’. As Thomas
    Hobbes remarkedin Leviathan when studying a similar problem, ‘covenants
    struck without thesword are but words’. The prisoners may trumpet the
    virtue of ‘notconfessing’ but if they are only motivated instrumentally
    by the pay-offs,then it is only so much hot air because each will
    ‘confess’ when the timecomes for a decision.

Page 148:

    What seems to be required to avoid this outcome is a mechanism
    whichallows for joint or collective decision making, thus ensuring that
    both actuallydo ‘not confess’. In other words, there is a need for a
    mechanism for enforcingan agreement—Hobbes’s ‘sword’, if you like. And
    it is this recognition whichlies at the heart of a traditional liberal
    argument dating back to Hobbes for thecreation of the State which is
    seen as the ultimate enforcement agency.(Notice, however, that such an
    argument applies equally to some otherinstitutions which have the
    capacity to enforce agreements, for example theMafia.) In Hobbes’s
    story, each individual in the state of nature can behavepeacefully or
    in a war-like fashion. Since peace allows everyone to go about

Page 149:

    it is notuncommon to find the dilemma treated as the essential model of
    social life

Page 149:

    The following four sectionsand the next chapter, on repeated games,
    discuss some of the developmentsin the social science literature which
    have been concerned with how thedilemma might be unlocked without the
    services of the State. In otherwords, the later sections focus on the
    question of whether the widespreadnature of this type of interaction
    necessarily points to the (legitimate inliberal terms) creation of an
    activist State. Are there other solutions whichcan be implemented
    without involving the State or any public institution?Since the scope
    of the State’s activities has become one of the mostcontested issues in
    contemporary politics, it will come as no surprise todiscover that the
    discussions around alternative solutions to the dilemmahave assumed a
    central importance in recent political (and especially inliberal and
    neoliberal) theory.

Page 149:

    It arises as a problem of trust in every elemental economic
    exchangebecause it is rare for the delivery of a good to be perfectly
    synchronised withthe payment for it and this affords the opportunity to
    cheat on the

Page 150:

    These are two-person examples of the dilemma, but it is probably the
    ‘n-person’ version of the dilemma (usually called the free rider
    problem) which hasattracted most attention. It creates a collective
    action problem among groupsof individuals. Again the examples are
    legion.

Page 151:

    The instrumentally rational individual will recognise that the best
    action is‘do not attach’ (i.e. defection) whatever the others do. This
    means that in apopulation of like-minded individuals, all will decide
    similarly with the resultthat each individual gains 2 utils. This is
    plainly an inferior outcome for allbecause everyone could have attached
    the device and if they all had done soeach would have enjoyed 3
    utils.In these circumstances the individuals in this economy might
    agree to theState enforcing attachment of the device. Alternatively, it
    is easy to see howanother popular intervention by the State would also
    do the trick. The Statecould tax each individual who did not attach the
    device a sum equivalent to 2utils and this would turn ‘attach’ (C) into
    the dominant strategy.

Page 151:

    There is nothinglike the State which can enforce contracts within the
    household to keep akitchen clean, but interestingly within a family
    household one oftenobserves the exercise of patriarchal or paternal
    power instead. Of course,the potential difficulty with such an
    arrangement is that the patriarch mayrule in a partial manner with the
    result that the kitchen is clean but with nohelp from the hands of the
    patriarch! The role of the State has in suchcases been captured, so to
    speak, by an interested party determined bygender. Then gender becomes
    the determinant of who bears the burdenand who has the more privileged
    role. Social power which ‘solves’prisoners’ dilemmas can be thus
    exercised without the direct involvementof the State (even though the
    State often enshrines such power in its owninstitutions).

Page 152:

    Hence the prisoners’ dilemma/free rider might plausibly lie atthe
    distinction which is widely attributed to Marx in the discussion of
    classconsciousness between a class ‘of itself’ and ‘for itself’ (see
    Elster, 1986b). Onsuch a view a class transforms itself into a ‘class
    for itself’, or a society avoidsdeficient demand, by unlocking the
    dilemma.

Page 153:

    Adam Smith’s account of how the self-interest of sellers combines with
    thepresence of many sellers to frustrate their designs and to keep
    prices lowmight also fit this model of interaction. If you are the
    seller choosing from thetwo row strategies C and D, then imagine that C
    and D translate into ‘charge ahigh price’ and ‘charge a low price’
    respectively. Figure 5.2 could reflect yourpreference ordering as high
    prices for all might be better than low prices forall and charging a
    low price when all others charge a high might be the bestoption because
    you scoop market share. Presumably the same applies to yourcompetitors.
    Thus even though all sellers would be happier with a high level
    ofprices, their joint interest is subverted because each acting
    individually quiterationally charges a low price. It is as if an
    invisible hand was at work onbehalf of the consumers.

Page 155:

    This is perhaps the most radical departure from the
    conventionalinstrumental understanding of what is entailed by
    rationality because, whileaccepting the pay-offs, it suggests that
    agents should act in a different wayupon them. The notion of
    rationality is no longer understood in the means—end framework as the
    selection of the means most likely to satisfy given ends.

Page 155:

    thus enabling‘rationality’ to solve the problem when there are
    sufficient numbers ofKantian agents.

Page 155:

    For instance, we mighthave wrongly assumed earlier that there is no
    honour among thieves becauseacting honourably could be connected to
    acting rationally in some fullaccount of rationality in which case the
    dilemma might be unlocked withoutthe intervention of the State (or some
    such agency). This general idea oflinking a richer notion of rational
    agency with the spontaneous solution ofthe dilemma has been variously
    pursued in the social science literature andthis section and the
    following three consider four of the more prominentsuggestions.

Page 155:

    The first connects rationality with morality and Kant provides a
    readyreference. His practical reason demands that we should undertake
    thoseactions which when generalised yield the best outcomes. It does
    not matterwhether others perform the same calculation and actually
    undertake thesame action as you. The morality is deontological and it
    is rational for theagent to be guided by a categorical imperative (see
    Chapter 1). Consequently,in the free rider problem, the application of
    the categorical imperative willinstruct Kantian agents to follow the
    cooperative action

Page 156:

    Similarly partisans in occupied Europe during the Second World War
    riskedtheir lives even when it was not clear that it was instrumentally
    rational toconfront the Nazis. In such cases, it seems people act on a
    sense of what isright.

Page 156:

    Likewise, Hardin (1982) suggests thatthe existence of environmental and
    other voluntary organisations usuallyentails overcoming a free rider
    problem and in the USA this may beexplained in part by an American
    commitment to a form ofcontractarianism whereby ‘people play fair if
    enough others do’

Page 156:

    Instead, rationality is conceived more as an expression of what is
    possible: ithas become an end in its own right. This is not only
    radical, it is alsocontroversial. Deontological moral philosophy is
    controversial for the obviousreason that it is not concerned with the
    actual consequences of an action, aswell as for the move to connect it
    with rationality. (Nevertheless, O’Neill(1989) presents a recent
    argument and provides an extended discussion of thismoral psychology
    and how it might be applied.)Kant’s morality may seem rather demanding
    for these reasons, but thereare weaker or vaguer types of moral
    motivation which also seem capableof unlocking the prisoners’ dilemma.
    For example, a general altruisticconcern for the welfare of others may
    provide a sufficient reason forpeople not to defect on the cooperative
    arrangement.

Page 157:

    Another departure from the strict instrumental model of rational action
    comeswhen individuals make decisions in a context of norms and these
    norms arecapable of overriding considerations of what is instrumentally
    rational.

Page 157:

    On the other hand, given the well-known difficultiesassociated with any
    coherent system of ethics (like utilitarianism), it seemsquite likely
    that a person’s ethical concerns will not be captured by a well-behaved
    set of preferences (see for instance Sen (1970) on the problems ofbeing
    a Paretian Liberal). Indeed rational agents may well base their actions
    onreasons which are external to their preferences.

Page 157:

    Of course, there is a tricky issue concerning whether these rather
    weaker orvaguer moral motivations (like altruism, acting on what is
    fair or what is right)mark a deep breach with the instrumental model of
    action. It might be arguedthat such ethical concerns can be represented
    in this model by introducing theconcept of ethical preferences. Thus
    the influence of ethical preferencestransforms the pay-offs in the
    game.

Page 158:

    Disputeswithin Aboriginal society are neither perceived as simply
    between twoindividuals nor subject to some established community
    tribunal. It is for thisreason that the resolution of a major conflict
    will involve a significant amountof negotiation between the parties.
    Yet the informal laws which govern thecontents of the negotiations are
    well entrenched in the tribal culture. Forexample, it is not uncommon
    for family members of the perpetrator to beasked to accept ‘punishment’
    if the individual offender is in prison andtherefore unavailable.

Page 159:

    First World War. This was a war of unprecedentedcarnage both at the
    beginning and the end. Yet during a middle period, non-aggression
    between the two opposing trenches emerged spontaneously in theform of a
    ‘live and let live’ norm. Christmas fraternisation is one
    well-knownexample, but the ‘live and let live’ norm was applied much
    more widely.Snipers would not shoot during meal times and so both sides
    could go abouttheir business ‘talking and laughing’ at these hours.
    Artillery was predictablyused both at certain times and at certain
    locations. So both sides couldappear to demonstrate aggression by
    venturing out at certain times and tocertain locations, knowing that
    the shells would fall predictably close to, butnot on, their chosen
    route. Likewise, it was not considered ‘etiquette’ to fireon working
    parties who had been sent out to repair a position or collect thedead
    and so on.

Page 159:

    For instance, it is sometimes argued that thenorms of Confucian
    societies enable those economies to solve the prisoners’dilemma/free
    rider problems within companies without costly contracting
    andmonitoring activity and that this explains, in part, the economic
    success ofthose economies (see Hargreaves Heap, 1991, Casson, 1991,
    North, 1991).Akerlof ’s (1983) discussion of loyalty filters, where he
    explains the relativesuccess of Quaker groups in North America by their
    respect for the norm ofhonesty, is another example—

Page 160:

    Wittgenstein of Philosophical Investigations1 is an obvious source for
    this viewbecause he would deny that the meaning of something like a
    person’sinterests or desires can be divorced from a social setting; and
    this is a usefulopportunity to take that argument further. The
    attribution of meaningrequires language rules and it is impossible to
    have a private language. Thereis a long argument around the possibility
    or otherwise of private languagesand it may be worth pursuing the point
    in a slightly different way by askinghow agents have knowledge of what
    action will satisfy the condition ofbeing instrumentally rational. Any
    claim to knowledge involves a firstunquestioned premise: I know this
    because I accept x. Otherwise an infiniteregress is inevitable: I
    accept x because I accept y and I accept ybecause…and so on.
    Accordingly, if each person’s knowledge of what isrational is to be
    accessible to one another, then they must share the samefirst premises.
    It was Wittgenstein’s point that people must share somepractices if
    they are to attach meaning to words and so avoid the problem ofinfinite
    redescription which comes with any attempt to specify the rules
    forapplying the rules of a language.

Page 161:

    There is another similarity and difference which might also be
    usefullymarked. To make it very crudely one might draw an analogy
    between thedifficulty which Wittgenstein encounters over knowledge
    claims and a similardifficulty which Simon (1982) addresses. (Herbert
    Simon is well known ineconomics for his claim that agents are
    procedurally rational, or boundedlyrational, because they do not have
    the computing capacity to work out whatis the best to do in complex
    settings.) To be sure, Wittgenstein finds theproblem in an infinite
    regress of first principles while Simon finds thedifficulty in the
    finite computing capacity of the brain. Nevertheless, both

Page 161:

    discussion of the Harsanyi doctrine because a similar claim seems to
    underpinthat doctrine. Namely that all rational individuals must come
    to the sameconclusion when faced by the same evidence. Wittgenstein
    would agree to theextent that some such shared basis of interpretation
    must be present ifcommunication is to be possible. But he would deny
    that all societies andpeoples will share the same basis for
    interpretations. The source of the sharingfor Wittgenstein is not some
    universal ‘rationality’, as it is for Harsanyi; ratherit is the
    practices of the community in which the people live, and these willvary
    considerably across time and space.

Page 162:

    let us make the view inspired by Wittgensteinvery concrete. The
    suggestion is that what is instrumentally rational is notwell defined
    unless one appeals to the prevailing norms of behaviour. Thismay seem a
    little strange in the context of a prisoners’ dilemma where thedemands
    of instrumental rationality seem plain for all to see: defect! But,in
    reply, those radically inspired by Wittgenstein would complain that
    thenorms have already been at work in the definition of the matrix and
    itspay-offs because it is rare for any social setting to throw up
    unvarnishedpay-offs. A social setting requires interpretation before
    the pay-offs can beassigned and norms are implicated in those
    interpretations. (See forexample Polanyi (1945) who argues, in his
    celebrated discussion of the riseof industrial society, that the
    incentives of the market system are onlyeffective when the norms of
    society place value on private materialadvance.)

Page 162:

    The last reflection on rationality comes from David Gauthier. He
    remainsfirmly in the instrumental camp and ambitiously argues that its
    dictates havebeen wrongly understood in the prisoners’ dilemma game.
    Instrumental rationalitydemands cooperation and not defection! To make
    his argument he distinguishesbetween two sorts of maximisers: a
    straightforward maximiser (SM) and aconstrained maximiser (CM). A
    straightforward maximiser defects (D)following the same logic that we
    have used so far. The constrained maximiseruses a conditional strategy
    of cooperating (C) with fellow constrainedmaximisers and defecting with
    straightforward maximisers. He then asks:which disposition
    (straightforward or constrained) should an instrumentallyrational
    person choose to have? (The decision can be usefully compared with
    asimilar one confronting Ulysses in connection with listening to the
    Sirens,

Page 164:

    The point is that if instrumental rationality is what motivates the CM
    inthe prisoners’ dilemma, then a CM must want to defect

Page 164:

    In other words, being a CM may be better than beingan SM, but the best
    strategy of all is to label yourself a CM and then cheaton the deal.
    And, of course, when people do this, we are back in a worldwhere
    everyone defects.

Page 164:

    Surely, this line of argument goes,it pays not to ‘zap’ a fellow CM
    because your reputation as a CM is therebypreserved and this enables
    you to interact more fruitfully with fellow CMs inthe future. Should
    you zap a fellow CM now, then everyone will know that youare a rogue
    and so in your future interactions, you will be treated as an SM.
    Inshort, in a repeated setting, it pays to forgo the short run gain
    from defectingbecause this ensures the benefits of cooperation over the
    long run. Thusinstrumental calculation can make true CM behaviour the
    best course ofaction.

Page 165:

    Moreover, it achieved aremarkable degree of cooperation.

Page 165:

    each program.Tit-for-Tat, submitted by Anatol Rapoport, won the
    tournament. Theprogram starts with a cooperative move and then does
    whatever theopponent did on the previous move. It was, as Axelrod
    points out, not onlythe simplest program, it was also the best!

Page 165:

    dilemma can be defeated without the intervention of a collective agency
    likethe State—that is, provided the interaction is repeated
    sufficiently often tomake the long term benefits outweigh the short
    gains.

Page 166:

    ‘Is the Prisoners’ dilemma all of sociology?’Of course, it is not, he
    answers. Nevertheless, it has fascinated social scientistsand proved
    extremely difficult to unlock in one-shot plays of the game—atleast,
    without the creation of a coercive agency like the State which is
    capableof enforcing a collective action or without the introduction of
    norms or somesuitable form of moral motivation on the part of the
    individuals playing thegame. Of course, many interactions are repeated
    and so this stark conclusionmay be modified by the discussion of the
    next chapter.

Page 167:

    Perhaps somewhat surprisingly, mutualdefection remains the only Nash
    equilibrium. The following two sectionsdiscuss, respectively,
    indefinitely repeated prisoners’ dilemma and therelated free rider
    games. We show (section 6.4) that mutual cooperation isa possible Nash
    equilibrium outcome in these games provided there is a‘sufficient’
    degree of uncertainty over when the repetition will cease.There are
    some significant implications here both for liberal politicaltheory and
    for the explanatory power of game theory. We notice that thisresult
    means that mutual cooperation might be achieved without theintervention
    of a collective agency like the State and/or withoutappealing to some
    expanded notion of rational agency

Page 168:

    the absence of a theory of equilibriumselection.

Page 170:

    Firstly, it provides a theoretical warrant for the belief that
    cooperation in theprisoners’ dilemma can be rationally sustained
    without the intervention ofsome collective agency like the State,
    provided there is sufficient (to be definedlater) doubt over when the
    repeated game will end. Thus the presence of aprisoners’ dilemma
    interaction does not necessarily entail either a poor socialoutcome or
    the institutions of formal collective decision making. The
    thirdalternative is for players to adopt a tit-for-tat strategy
    rationally.1 If they adoptthis third alternative the socially inferior
    outcome of mutual defection will beavoided without the interfering
    presence of the State or some other formal(coercive) institution.

Page 171:

    Equally, it is probable that both prisonersin the original example may
    think twice about ‘confessing’ because each knowsthat they are likely
    to encounter one another again (if not in prison, at leastoutside) and
    so there are likely to be opportunities for exacting ‘punishment’ ata
    later date.

Page 171:

    Folk theorem

Page 172:

    This is an extremely important result for the social sciences because
    itmeans that there are always multiple Nash equilibria in such
    indefinitelyrepeated games. Hence, even if Nash is accepted as the
    appropriateequilibrium concept for games with individuals who are
    instrumentally rationaland who have common knowledge of that
    rationality, it will not explain howindividuals select their strategies
    because there are many strategy pairs whichform Nash equilibria in
    these repeated games. Of course, we have encounteredthis problem in
    some one-shot games before, but the importance of this resultis that it
    means the problem is always there in indefinitely repeated games.Even
    worse, it is amplified by repetition. In other words, game theory needs
    tobe supplemented by a theory of equilibrium selection if it is to
    explain actionin these indefinitely repeated games, especially if it is
    to explain howcooperation actually arises spontaneously in indefinitely
    repeated prisoners’dilemma games.

Page 175:

    Now consider a tit-for-tat strategy in this group which works in
    thefollowing way. The strategy partitions the group into those who are
    in ‘goodstanding’ and those who are in ‘no standing’ based on whether
    the individualcontributed to the collective fund in the last time
    period. Those in ‘goodstanding’ are eligible for the receipt of help
    from the group if they fall ‘ill’ thistime period, whereas those who
    are in ‘no standing’ are not eligible for help.Thus tit-for-tat
    specifies cooperation and puts you in ‘good standing’ for thereceipt of
    a benefit if you fall ‘ill’ (alternatively, to connect with the
    earlierdiscussion, one might think of cooperating as securing a
    ‘reputation’ whichputs one in ‘good standing’

Page 175:

    Notice your decision now will determine whether you are in ‘good
    standing’from now until the next opportunity that you get to make this
    decision (whichwill be the next period if you do not fall ‘ill’ or the
    period after that if you fall‘ill’). So we focus on the returns from
    your choice now until you next get theopportunity to choose.

Page 176:

    Who needs the State?

Page 176:

    Here we pick up threads of the Hobbesianargument for the State and see
    what the result holds for this argument. At firstglance, the argument
    for the State seems to be weakened because it appearsthat a group can
    overcome the free rider problem without recourse to theState for
    contract enforcement. So long as the group can punish free riders
    byexcluding them from the benefits of cooperation (as for instance the
    Pygmiespunished Cephu—see Chapter 5), then there is the possibility of
    ‘spontaneous’public good provision through the generalisation of the
    tit-for-tat strategy.Having noted this, nevertheless, the point seems
    almost immediately to beblunted since the difference between a
    Hobbesian State which enforcescollective agreements and the generalised
    tit-for-tat arrangement is notaltogether clear and so in proving one we
    are hardly undermining the other.After all, the State merely codifies
    and implements the policies of‘punishment’ on behalf of others in a
    very public way (with the rituals ofpolice stations, courts and the
    like). But, is this any different from the golfclub which excludes a
    member from the greens when the dues have not beenpaid or the Pygmies’
    behaviour towards Cephu? Or the gang which excludespeople who have not
    contributed ‘booty’ to the common fund?

Page 176:

    Box 6.2

Page 178:

    contract—that the creation of the State by the individual also helps
    shape asuperior individual.) Hayek, however, prefers the ‘English
    tradition’ because hedoubts (a) that the formation of the State is part
    of a process which liberates(and moulds) the social agent and (b) that
    there is the knowledge to informsome central design so that it can
    perform the task of resolving free ridingbetter than spontaneously
    generated solutions (like tit-for-tat). In other words,reason should
    know its limits and this is what informs Hayek’s support forEnglish
    pragmatism and its suspicion of the State.Of course there is a big ‘if
    in Hayek’s argument. Although Beirut stillmanaged to function without a
    grand design, most of its citizens prayed forone. In short, the
    spontaneous solution is not always the best. Indeed, as wehave seen,
    the cooperative solution is just one among many Nash equilibria
    inrepeated games, so in the absence of some machinery of collective
    decisionmaking, there seems no guarantee it will be selected. Against
    this, however, itis sometimes argued that evolution will favour
    practices which generate thecooperative outcome since societies that
    achieve cooperation in these gameswill prosper as compared with those
    which are locked in mutual defection.This is the cue for a discussion
    of evolutionary game theory and we shall leavefurther discussion of the
    State until we turn to evolutionary game theory

Page 178:

    Instead the result seems important because it demythologises the
    State.Firstly the State qua State (that is, the State with its police
    force, its courts andthe like) is not required to intrude into every
    social interaction which suffersfrom a free rider problem. There are
    many practices and institutions which aresurrogates for the State in
    this regard. Indeed, the Mafia has plausiblydisplaced the State in
    certain areas precisely because it provides the services ofa State.
    Likewise, during the long civil war years inhabitants of Beirutsomehow
    still managed to maintain services which required the overcoming offree
    rider problems.Secondly since something like the State as contract
    enforcer might well arise‘spontaneously’ through the playing of free
    rider games repeatedly, it need notrequire any grand design. There need
    be no constitutional conventions. In thisway the result counts strongly
    for what Hayek (1962) refers to as the Englishas opposed to the
    European continental Enlightenment tradition. The latterstresses the
    power of reason to construct institutions that overcome problemslike
    those of the free rider. (It also often presupposes—recall Rousseau’s
    social

Page 178:

    different if you pay the State in the form of taxes or the Mafia in the
    form oftribute?

Page 185:

    example comes from strategicdecisions by the legislature when the
    Executive is trying to push throughParliament a series of bills that
    the latter is unsympathetic towards.

Page 185:

    President proposes legislation. The Congress is notin sympathy with the
    proposal and must decide whether to make amendments.If it decides to
    make an amendment, then the President must decide whetherto fight the
    amendment or acquiesce. Looking at the President’s pay-offs it
    isobvious that, even though he or she prefers that the Congress does
    not amendthe legislation, if it does, he or she would not want to fight

Page 186:

    the Folk theorem ensures that aninfinity of war/acquiescence patterns
    are compatible with instrumentalrationality. Nevertheless, the duration
    of such games is usually finite andsometimes their length is
    definite—e.g. US Presidents have a fixed term andincumbents have only a
    fixed number of local markets that they wish todefend. What happens
    then? Would it make sense for the President or theincumbent to put on a
    show of strength early on (e.g. by fighting the Congressor unleashing a
    price war) in order to create a reputation for belligerence thatwould
    make the Congress and the entrant think that, in future rounds,
    theywill end up with pay-off -1/2 if they dare them?

Page 186:

    In the finitely repeated version of the game Nash backward
    inductionargues against this conclusion. Just as in the case of the
    prisoners’ dilemmain the previous subsection, it suggests that, since
    there will be no fighting atthe last play of the game, the reputation
    of the President/incumbent willunravel to the first stage and no
    fighting will occur (rationally). Theconclusion changes again once we
    drop CKR (or allow for different types ofplayers).

Page 190:

    Of course, there may be actions that can be takenoutside the game and
    which have a similar effect on the beliefs of others. Such‘signalling’
    behaviour is considered briefly in this section to round out
    thediscussion of reputations. It is of potential relevance not only to
    repeated, butalso to one-shot games.

Page 192:

    when the game isrepeated and there is a unique Nash equilibrium things
    change. The Nashequilibrium is attractive because as time goes by and
    agents adjust theirexpectations of what others will do in the light of
    experience, then they willseem naturally drawn to the Nash equilibrium
    because it is the only restingplace for beliefs. Any other set of
    beliefs will upset itself.

Page 192:

    Nevertheless, there is still no guarantee that a Nash equilibrium
    willsurface even if it exists and it is unique.

Page 193:

    The strength of the Nash equilibrium is that forward looking agents
    mayrealise that (R2, C2) is the only outcome that does not engender
    such thoughts.We just saw that adaptive (or backward looking)
    expectations will not do thetrick. If, however, after having been
    around the pay-off matrix a few timesplayers ask themselves the
    question ‘How can we reach a stable outcome?’,they may very well
    conclude that the only such outcome is the Nashequilibrium (R2, C2).But
    why would they want to ask such a question? What is so wrong
    withinstability (and disequilibrium) after all? Indeed in the case of
    Figure 2.6 ourplayers have an incentive to avoid a stable outcome
    (observe that on averagethe cycle which takes them from one extremity
    of the pay-off matrix toanother yields a much higher pay-off than the
    Nash equilibrium result). If, onthe other hand, pay-offs were as in
    Figure 6.4 below, they would be stronglymotivated to reach the Nash
    equilibrium.

Page 193:

    It is easy to see that this type of adaptivelearning will never lead
    the players to the Nash equilibrium outcome (R2, C2).Instead, they will
    be oscillating between outcomes (R1, C1), (R1, C3), (R3, C1)and (R3,
    C3).Can they break away from this never ending cycle and hit the
    Nashequilibrium? They can provided they converge onto a common
    forwardlooking train of thought. For

Page 194:

    Thus we conclude that whether repetition makes the Nashequilibrium more
    or less likely when it is unique must depend on thecontingencies of how
    people learn and the precise pay-offs from non-Nashbehaviour.

Page 194:

    Broadly put, this is one and the same problem. It is a problem
    withspecifying how agents come to hold beliefs which are extraneous to
    the game(in the sense that they cannot be generated endogenously
    through theapplication of the assumptions of instrumental rationality
    and commonknowledge of instrumental rationality)

Page 195:

    the insights of evolutionary game theory arecrucial material for many
    political and philosophical debates, especially thosearound the State.

Page 195:

    The argument for suchan agency turns on the general problem of
    equilibrium selection and on theparticular difficulty of overcoming the
    prisoners’ dilemma. When there aremultiple equilibria, the State can,
    through suitable action on its own part, guidethe outcomes towards one
    equilibrium rather than another. Thus the problemof equilibrium
    selection is solved by bringing it within the ambit of
    consciouspolitical decision making. Likewise, with the prisoners’
    dilemma/ free riderproblem, the State can provide the services of
    enforcement. Alternativelywhen the game is repeated sufficiently and
    the issue again becomes one ofequilibrium selection, then the State can
    guide the outcomes towards thecooperative Nash equilibrium.

Page 195:

    intransigent Right’

Page 196:

    —that is, the idea that you can turn social outcomes intomatters of
    social choice through the intervention of a collective action
    agencylike the State. The positive argument against ‘political
    rationalism’, as the quoteabove suggests, turns on the idea that these
    interventions are not evennecessary. The failure to intervene does not
    spell chaos, chronic indecision,fluctuations and outcomes in which
    everyone is worse off than they couldhave been. Instead, a ‘spontaneous
    order’ will be thrown up as a result ofevolutionary processes.

Page 196:

    Likewise, there are problems of ‘political failure’ that subvert the
    ideal ofdemocratic decision making and which can match the market
    failures that theState is attempting to rectify. For example, Buchanan
    and Wagner (1977) andTullock (1965) argue that special interests are
    bound to skew ‘democraticdecisions’ towards excessively large
    bureaucracies and high governmentexpenditures. Furthermore there are
    difficulties, especially after the Arrowimpossibility theorem, with
    making sense of the very idea of something likethe ‘will of the people’
    in whose name the State might be acting (see Arrow,1951, Riker, 1982,
    Hayek, 1962, and Buchanan, 1954).1These, so to speak, are a shorthand
    list of the negative arguments comingfrom the political right against
    ‘political rationalism’ or ‘socialconstructivism’

Page 196:

    Forinstance, there are problems of inadequate knowledge which can mean
    thateven the best intentioned and executed political decision generates
    unintendedand undesirable consequences. Indeed this has always been an
    importanttheme in Austrian economics, featuring strongly in the 1920s
    debate over thepossibility of socialist planning as well as
    contemporary doubts over thewisdom of more minor forms of State
    intervention.

Page 196:

    Hayek (1962) himself tracesthe battlelines in the dispute back to the
    beginning of Enlightenmentthinking:Hayek distinguished two intellectual
    lines of thought about freedom, ofradically opposite upshot. The first
    was an empiricist, essentially Britishtradition descending from Hume,
    Smith and Ferguson, and seconded byBurke and Tucker, which understood
    political development as aninvoluntary process of gradual institutional
    improvement, comparable tothe workings of a market economy or the
    evolution of common law. Thesecond was a rationalist, typically French
    lineage descending fromDescartes through Condorcet to Comte, with a
    horde of modernsuccessors, which saw social institutions as fit for
    premeditatedconstruction, in the spirit of polytechnic engineering. The
    former lineled to real liberty; the latter inevitably destroyed it.

Page 197:

    evolutionary stable strategies

Page 197:

    In particular, wesuggest that the evolutionary approach can help
    elucidate the idea that poweris mobilised through institutions and
    conventions. We conclude the chapterwith a summing-up of where the
    issue of equilibrium selection and the debateover the State stands
    after the contribution of the evolutionary approach.

Page 197:

    The basic idea behind this equilibrium concept is that an ESS is a
    strategywhich when used among some population cannot be ‘invaded’ by
    anotherstrategy because it cannot be bested. So when a population uses
    a strategy I,‘mutants’ using any other strategy J cannot get a toehold
    and expand amongthat population.

Page 197:

    This is why evolutionary game theory assumes significance in the
    debateover an active State. It should help assess the claims of
    ‘spontaneous order’made by those in the British corner and so advance
    one of the central debatesin Enlightenment political thinking.

Page 198:

    This is, if youlike, a version of Hobbes’s nightmare where there are no
    property rightsand everyone you come across will potentially claim your
    goods.

Page 202:

    Secondly, and more specifically, there is the result that although the
    symmetricalplay of this game yields a unique equilibrium, it becomes
    unstable the momentrole playing begins and some players start to
    recognise asymmetry. Sincecreative agents seem likely to experiment
    with different ways of playing thegame, it would be surprising if there
    was never some deviation based on anasymmetry. Indeed it would be more
    than surprising because there is muchevidence to support the idea that
    people look for ‘extraneous’ reasons whichmight explain what are in
    fact purely random types of behaviour (see theadjacent box on winning
    streaks).Formally, this leaves us with the old problem of how the
    solution to thegame comes about. However, evolutionary game theory does
    at least point usin the direction of an answer. The phase diagram in
    Figure 7.2 reveals that theselection of an equilibrium depends
    critically on the initial set of beliefs

Page 202:

    once animperfect form of rationality is posited. In other words, it is
    not beingdeduced as an implication of the common knowledge of
    rationalityassumption which has been the traditional approach of
    mainstream gametheory.

Page 203:

    Thirdly, it can be noted that the selection of one ESS rather than
    anotherembodies a convention

Page 203:

    To put these observationsrather less blandly, since rationality on this
    account is only responsible for thegeneral impulse towards mimicking
    profitable behaviour, the history of thegame depends in part on what
    are the idiosyncratic and unpredictable (non-rational, one might say,
    as opposed to irrational) features of individual beliefsand learning.

Page 204:

    Fourthly, the selection of one equilibrium rather than another
    potentiallymatters rather deeply. In effect in the hawk—dove game over
    contestedproperty, what happens in the course of moving to one of the
    ESSs is theestablishment of a form of property rights. Either those
    playing role R get theproperty and role C players concede this right,
    or those playing role C get theproperty and role R players concede this
    right. This is interesting not onlybecause it contains the kernel of a
    possible explanation of property rights (onwhich we shall say more
    later) but also because the probability of playing roleR or role C is
    unlikely to be distributed uniformly over the population. Indeed,this
    distribution will depend on whatever is the source of the distinction
    usedto assign people to roles.

Page 204:

    The question, then, of how a source of differentiation gets
    establishedbecomes rather important.

Page 204:

    Thus the behaviour at one of theseESSs is conventionally determined
    and, to repeat the earlier point, we can plotthe emergence of a
    particular convention with the use of this phase diagram.It will depend
    both on the presumption that agents learn from experience(the rational
    component of the explanation) and on the particularidiosyncratic (and
    non-rational) features of initial beliefs and precise learningrules.

Page 206:

    After all, perhaps the presence of these conventions can only
    beaccounted for by a move towards a Wittgensteinian ontology, in which
    casemainstream game theory’s foundations look decidedly wobbly. To
    prevent thisdrift a more robust response is required.The alternative
    response is to deny that the appeal to shared prominenceor salience
    involves either an infinite regress or an acknowledgement
    thatindividuals are necessarily ontologically social

Page 206:

    There is a further and deeper problem with the concept of salience
    basedon analogy because the attribution of terms like ‘possession’
    plainly begs thequestion by presupposing the existence of some sort of
    property rights in thepast. In other words, people already share a
    convention in the past and this isbeing used to explain a closely
    related convention in the present. Thus we havenot got to the bottom of
    the question concerning how people come to holdconventions in the first
    place.3

Page 206:

    So, of course, we cannot hope to explainhow they actually achieve a new
    coordination without appealing to thosebackground conventions. In this
    sense, it would be foolish for socialscientists (and game theorists, in
    particular) to ignore the social context inwhich individuals play new
    games.

Page 208:

    This conclusion reinforces the earlier result that the course of
    historydepends in part on what seem from the instrumental account of
    rationalbehaviour to be non-rational (and perhaps idiosyncratic) and
    thereforefeatures of human beliefs and action which are difficult to
    predict

Page 209:

    mechanically. One can interpret this in the spirit of
    methodologicalindividualism at the expense of conceding that
    individuals are, in this regard,importantly unpredictable. On the one
    hand, this does not look good for theexplanatory claims of the theory.
    On the other hand, to render theindividuals predictable, it seems that
    they must be given a shared history andthis will only raise the
    methodological concern again of whether we canaccount for this sharing
    satisfactorily without a changed ontology. Insummary, if individuals
    are afforded a shared history, then social context is‘behind’ no one
    and ‘in’ everyone and then the question is whether it is agood idea to
    analyse behaviour by assuming (as methodological individualistsdo) the
    separability of context and action.4

Page 213:

    The underlying point here is that discrimination may be
    evolutionarystable if the dominated cannot find ways of challenging the
    social conventionthat supports their subjugation. This conclusion is
    not necessarily rightbecause there are other potential sources of
    change. The insight that we preferto draw is that individual attempts
    to buck an established convention areunlikely to succeed, whereas the
    same is not true when individuals takecollective action.

Page 213:

    Stasis, status quo: Thus the introduction of a convention will benefit
    the average person, butif you happen to be so placed with respect to
    the convention that you onlyplay the dominant role with a probability
    of less than 1/3, then you would bebetter off without the convention.
    This result may seem puzzling at first: whydo the people who play a
    dominant role less than 1/3 of the time not revert tothe symmetric play
    of the game and so undermine the convention? The answeris that even
    though the individual would be better off if everyone quit
    theconvention, it does not make sense to do so individually. After all,
    aconvention will tell your opponent to play either H or D, and then
    instruct youto play D or H respectively; and you can do no better than
    follow thisconvention since the best reply to H remains D and likewise
    the best reply toD is H. It is just tough luck if you happen to get the
    D instruction all thetime!We take the force of this individual
    calculation to be a powerful contributorto the status quo and it might
    seem to reveal that evolutionary processes yieldto stasis.

Page 213:

    Conventions, inequality and revolt

Page 214:

    To summarise, we should expect a convention to emerge even though itmay
    not suit everyone, or indeed even if it short-changes the majority. It
    maybe discriminatory, inequitable, non-rational, indeed thoroughly
    disagreeable, yetsome such convention is likely to arise whenever a
    social interaction like hawk-dove is repeated. Which convention emerges
    will depend on the sharedsalience of extraneous features of the
    interaction, initial beliefs and the waythat people learn.

Page 214:

    Standstill: A potential weakness of evolutionary game theory has just
    becomeapparent. Once the bandwagon has come to a standstill, and one
    conventionhas been selected, the theory cannot account for a potential
    subversion of theestablished convention. Such an account would require,
    as we argued in theprevious paragraph, an understanding of political
    (that is, collective) actionbased on a more active form of human agency
    than the one provided byinstrumental rationality. Can evolutionary game
    theory go as far?

Page 219:

    Recall the idea of a trembling hand in section 2.7.1 and suppose
    thatplayers make mistakes sometimes. In particular, when they intend
    tocooperate they occasionally execute the decision wrongly and they
    defect. Inthese circumstances, playing t punishes you for the mistake
    endlessly becauseit means that your opponent defects next round in
    response to your mistakendefection. If in the next period you
    cooperate, you are bound to get zapped.If you follow your t-strategy
    next time, then you will be defecting while youropponent will be
    cooperating and a frustrating sequence of alternatingdefections and
    cooperations will ensue. One way out of this bind is toamend t to t’
    whereby, if you defect by mistake, then you cooperate twiceafterwards:
    the first time as a gesture of acknowledging your mistake and thesecond
    in order to coordinate your cooperative behaviour with that of
    youropponent. In other words, the amended tit-for-tat instructs you to
    cooperatein response to a defection which has been provoked by an
    earlier mistakendefection on your part.

Page 219:

    Eventhough strategy C would do equally well as a reply to t’, if your
    opponentmade the mistake (last period) then you know that your opponent
    willcooperate in the next two rounds no matter what you do this period.
    Thusyour best response in this round is to defect

Page 221:

    Conventions as covert social power

Page 221:

    even more covert power that comes from being able to mould the
    preferencesand the beliefs of others so that a conflict of interest is
    not even latentlypresent.

Page 221:

    with the interests of another.It is common in discussions of power to
    distinguish between the overt andthe covert exercise of power. Thus,
    for instance, Lukes (1974) distinguishesthree dimensions of power.
    There is the power that is exercised in the politicalor the economic
    arena where individuals, or firms, institutions, etc., are able
    tosecure decisions which favour their interests over others quite
    overtly. This isthe overt exercise of power along the first dimension.
    In addition, there is themore covert power that comes from keeping
    certain items off the politicalagenda. Some things simply do not get
    discussed in the political arena and inthis way the status quo
    persists. Yet the status quo advantages some rather thanothers and so
    this privileging of the status quo by keeping certain issues offthe
    political agenda is the second dimension of power. Finally, there is
    the

Page 222:

    The figure of Spartacus captured imaginations over theages, not so much
    because of his military antics, but because he personifiedthe
    possibility of liberating the slaves from the beliefs which sustained
    theirsubjugation.

Page 222:

    this is the power which works through the mind and which dependsfor its
    influence on the involvement or agreement of large numbers of
    thepopulation (again connecting with the earlier observation about the
    force ofcollective action).

Page 222:

    State were consciously to select a convention in these circumstances
    thenwe might observe the kind of political haggling associated with the
    overtexercise of power. Naturally when a convention emerges
    spontaneously, we donot observe this because there is no arena for the
    haggling to occur, yet theemergence of a convention is no less decisive
    than a conscious politicalresolution in resolving the conflict of
    interest.6Evolutionary game theory also helps reveal the part played by
    beliefs,especially the beliefs of the subordinate group, in securing
    the power of thedominant group (a point, for example, which is central
    to Gramsci’s notion ofhegemony and Hart’s contention that the power of
    the law requires voluntarycooperation).

Page 224:

    Theannexing of virtue can happen as a result of well-recognised
    patterns ofcognition.

Page 224:

    Of course, like all theories of cognitive dissonance removal,this story
    begs the question of whether the adjustment of beliefs can do thetrick
    once one knows that the beliefs have been adjusted for the
    purpose.Nevertheless, there seem to be plenty of examples of dissonance
    removal

Page 225:

    Our final illustration of how evolutionary game theory might help
    sharpenour understanding of debates around power in the social sciences
    relates tothe question of how gender and race power distributions are
    constitutedand persist. The persistence of these power imbalances is a
    puzzle to some.

Page 227:

    Once a convention isestablished in this game, a set of property
    relations are also established.Hence the convention could encode a set
    of class relations for this gamebecause it will, in effect, indicate
    who owns what and some may end upowning rather a lot when others own
    scarcely anything. However, as wehave seen a convention of this sort
    will only emerge once the game isplayed asymmetrically and this
    requires an appeal to some piece ofextraneous information like sex or
    age or race, etc. In short, the creationof private property relations
    from the repeated play of these gamesdepends on the use of some other
    asymmetry and so it is actuallyimpossible to imagine a situation of
    pure class relations, as they couldnever emerge from an evolutionary
    historical process. Or to put thisslightly differently: asymmetries
    always go in twos!This understanding of the relation has further
    interesting implications.For instance, an attack on gender
    stratification is in part an attack on classstratification and vice
    versa.

Page 227:

    Likewise, however, it would be wrong toimagine that the attack on
    either if successful would spell the end of theother.

Page 227:

    On this account of powerthrough the working of convention, the
    ideological battle aimed atpersuading people not to think of themselves
    as subordinate is half thebattle because these beliefs are part of the
    way that power is mobilised.

Page 228:

    . The feedback mechanism, however, ispresent in this analysis and it
    arises because there is ‘learning’. It is theassumption that people
    shift towards practices which secure better outcomes(without knowing
    quite why the practice works for the best) which is thefeedback
    mechanism responsible for selecting the practices. Thus in the
    debateover functional explanation, the analysis of evolutionary games
    lends supportto van Parijs’s (1982) argument that ‘learning’ might
    supply the generalfeedback mechanism for the social sciences which will
    license functionalexplanations in exactly the same way as natural
    selection does in the biologicalsciences.

Page 228:

    effect, the explanation of gender and racial inequalities using
    thisevolutionary model is an example of functional argument.

Page 228:

    The differencebetween men and women or between whites and blacks has no
    merit inthe sense that it does not explain why the differentiation
    persists. Thedifferentiation has the unintended consequence of helping
    the populationto coordinate its decision making in settings where there
    are benefitsfrom coordination. It is this function of helping the
    population to selectan equilibrium in a situation which would otherwise
    suffer from theconfusion of multiple equilibria which explains the
    persistence of thedifferentiation.

Page 229:

    So far, however, the difference between the two camps (H&EVGT andMarx)
    is purely based on value judgements: one argues that illusory moralsare
    good for all, the other that they are not. In this sense, both
    canprofitably make use of the analysis in evolutionary game theory.
    Indeed, aswe have already implied in section 7.3.4, a radical political
    project grounded

Page 229:

    On the side of H&EVGT, Hume thinks that suchillusions play a positive
    role (in providing the ‘cement’ which keeps societytogether) in
    relation to the common good. So do neo-Humeans (like Sugden)who are, of
    course, less confident that invocation of the ‘common good’ is agood
    idea (as we mentioned in section 7.6.2) but who are still happy to
    seeconventions (because of the order they bring) become entrenched in
    sociallife even if this is achieved with the help of a few moral
    ‘illusions’. On theother side, however, Marx insists that moral
    illusions are never a good idea(indeed he dislikes all illusions).
    Especially since, as he sees it, their socialfunction is to help some
    dreadful conventions survive (recall how in section7.3.4 we showed that
    disagreeable conventions may become stable even ifthey are detrimental
    to the majority). Marx believed that we can

Page 229:

    which sound quite like observations that Marxists might make:
    theimportance of taking collective action if one wants to change a
    convention;how power can be covertly exercised; how beliefs
    (particularly moral beliefs)may become endogenous to the conventions we
    follow; how propertyrelations might develop functionally; and so on.

Page 229:

    Indeedmost of the ideas developed on the basis of H&EVGT in the
    precedingpages would find Marx in agreement.

Page 229:

    People may think that their beliefson such matters go beyond material
    values (i.e. self-interest, which in ourcontext means pay-offs); that
    they respond to certain universal ideals aboutwhat is ‘good’ and
    ‘right’, when all along their moral beliefs are a direct(even if
    unpredictable) repercussion of material conditions and interests.

Page 230:

    An analysis of hawk—dove games, along the lines of H&EVGT, helpsexplain
    the evolution of property rights in primitive societies. Once
    theserights are in place and social production is under way, each group
    in society(e.g. the owners of productive means, or those who do not own
    tools, land,machines, etc.) develops its own interest. And since (as
    H&EVGT concurs)conventions evolve in response to such interests, it is
    not surprising thatdifferent conventions are generated within different
    social groups in responseto the different interests. The result is
    conflicting sets of conventions which

Page 230:

    Finally, the established (stable) conventions acquire moral weight and
    even leadpeople to believe in something called the common good—which is
    most likelyanother illusion

Page 230:

    In summary, H&EVGTbegins with a behavioural theory based on the
    individual interest and eventuallylands on its agreeable by-product:
    the species interest. There is nothing inbetween the two types of
    interest. By contrast, Marx posits another type ofinterest in between:
    class interest.Marx’s argument is that humans are very different from
    other speciesbecause we produce commodities in an organised way before
    distributingthem. Whereas other species share the fruits of nature
    (hawk—dove games aretherefore ‘naturally’ pertinent in their state of
    nature), humans have developedcomplex social mechanisms for producing
    goods. Naturally, the norms ofdistribution come to depend on the
    structure of these productive mechanisms.They involve a division of
    labour and lead to social divisions (classes). Whichclass a person
    belongs to depends on his or her location (relative to others)within
    the process of production. The moment collective production (as in
    thecase of Cephu and his tribe in Chapter 5) gave its place to a
    separationbetween those who owned the tools of production and those who
    workedthose tools, then groups with significantly different (and often
    contradictory)interests developed.

Page 230:

    in collective action is as compatible with evolutionary game theory as
    is theneo-Humeanism of Sugden (1986, 1989). But is there something more
    inMarx than a left wing interpretation of evolutionary game theory? We
    thinkthere is.

Page 231:

    lead to conflicting morals. Each set of morals becomes an ideology.9
    Which setof morals (or ideology) prevails at any given time? Marx
    thinks that, inevitably,the social class which is dominant in the
    sphere of production and distributionwill also be the one whose set of
    conventions and morals (i.e. whose ideology)will come to dominate over
    society as a whole.To sum up Marx’s argument so far, prevailing moral
    beliefs are illusoryproducts of a social selection process where the
    driving force is not somesubjective individual interest but objective
    class interest rooted in thetechnology and relations of production.
    Although there are many conflictingnorms and morals, at any particular
    time the morality of the ruling class isuniquely evolutionary stable.
    The mélange of legislation, moral codes, norms,etc., reflects this
    dominant ideology.But is there a fundamental difference between the
    method of H&EVGTand Marx? Or is it just a matter of introducing classes
    in the analysiswithout changing the method?

Page 231:

    So, how would Marx respond to evolutionary game theory if he werearound
    today? He would, we think, be very interested in some of the
    radicalconclusions in this chapter. However, he would also speak
    derisively of thematerialism of H&EVGT Marx habitually poured scorn on
    those (e.g.Spinoza and Feuerbach) who transplanted models from the
    natural sciencesto the social sciences with little or no modification
    to allow for the fact thathuman beings are very different to atoms,
    planets and molecules.12 Wemention this because at the heart of H&EVGT
    lies a simple Darwinianmechanism (witness that there is no analytical
    difference between the modelsin the biology of John Maynard Smith and
    the models in this chapter). Marxwould probably claim that the theory
    is not sufficiently evolutionary because(a) its mechanism comes to a
    standstill once a stable convention has evolved,and (b) of its reliance
    on instrumental rationality which reduces humanactions to passive
    reflex responses to some (meta-physical) self-interest.

Page 232:

    Especially in hisphilosophical (as opposed to economic) works, Marx
    argued strongly for anevolutionary (or more precisely historical)
    theory of society with a modelof human agency which retains human
    activity as a positive (creative) forceat its core. In addition, Marx
    often spoke out against mechanism; againstmodels borrowed directly from
    the natural sciences (astronomy andbiology are two examples that he
    warned against). It is helpful to preservesuch an aversion since humans
    are ontologically different to atoms andgenes. Of course Marx himself
    has been accused of mechanism and,indeed, in the modern (primarily
    Anglo-Saxon) social theory literature he istaken to be an exemplar of
    19th century mechanism. Nevertheless hewould deny this, pointing to the
    dialectical method he borrowed fromHegel and which (he would claim)
    allowed him to have a scientific, yetnon-mechanistic, outlook. Do we
    believe him? As authors we disagree here.SHH does not, while YV does.

Page 232:

    Of course there is always the answer that self-interest feeds into
    moral beliefsand then moral beliefs feed back into self-interest and
    alter people’s desires.And so on. But that would be too circular for
    Marx. It would not explainwhere the process started and where it is
    going. By contrast, his version ofmaterialism (which he labelled
    historical materialism) starts from thetechnology of production and the
    corresponding social organisation. Thelatter entails social classes
    which in turn imbue people with interests; peopleact on those interests
    and, mostly without knowing it, they shape theconventions of social
    life which then give rise to morals. The process,however, is grounded
    on the technology of production at the beginning of thechain. And as
    this changes (through technological innovations) it provides theimpetus
    for the destabilisation of the (temporarily) evolutionary
    stableconventions at the other end of the chain.

Page 232:

    Ifmorals are socially manufactured, then so is self-interest.

Page 233:

    Perhaps our disagreement needs to be understood in terms of thelack of
    a shared history in relation to these debates—one of us embarkingfrom
    an Anglo-Saxon, the other from a (south) European, tradition. It
    was,after all, one of our important points in earlier chapters that
    game theoristsshould not expect a convergence of beliefs unless agents
    have a sharedhistory!

Page 234:

    most of the population. This would seem to provide ammunition for the
    socialconstructivists, but of course it depends on them believing that
    collectiveaction agencies like the State will have sufficient
    information to distinguish thesuperior outcomes. Perhaps all that can
    be said on this matter is that, if youreally believe that evolutionary
    forces will do the best that is possible, then it isbeyond dispute that
    these forces have thrown up people who are predisposedto take
    collective action. Thus it might be argued that our
    evolutionarysuperiority as a species derives in part precisely from the
    fact that we are pro-active through collective action agencies rather
    than reactive as we would beunder a simple evolutionary scheme.

Page 234:

    Turning to another dispute, that between social constructivism and
    spontaneousorder within liberal political theory, two clarifications
    have occurred. The first isthat there can be no presumption that a
    spontaneous order will deliveroutcomes which make everyone better off,
    or even outcomes which favour

Page 234:

    Thesetheoretical moves will threaten to dissolve the distinction
    between action andstructure which lies at the heart of the game
    theoretical depiction of social lifebecause it will mean that the
    structure begins to supply reasons for action andnot just constraints
    upon action. On the optimistic side, this might be seen asjust another
    example of how discussions around game theory help to dissolvesome of
    the binary oppositions which have plagued some debates in
    socialscience—just as it helped dissolve the opposition between gender
    and classearlier in this chapter. However, our concern here is not to
    point to requiredchanges in ontology of a particular sort. The point is
    that some change isnecessary, and that it is likely to threaten the
    basic approach of game theory tosocial life.

Page 234:

    Secondly, on the difficult cases where equilibrium selection
    involveschoices over whose interests are to be favoured (i.e. it is not
    a matter ofselecting the equilibrium which is better for everyone),
    then it is notobvious that a collective action agency like the State is
    any better placed tomake this decision than a process of spontaneous
    order. This may come asa surprise, since we have spent most of our time
    here focusing on theindeterminacy of evolutionary games when agents are
    only weaklyinstrumentally rational.

Page 235:

    In other words the very debate within liberal political theory over
    socialconstructivism versus spontaneous order is itself unable to come
    to aresolution precisely because its shared ontological foundations are
    inadequatefor the task of social explanation. In short, we conclude
    that not only willgame theory have to embrace some expanded form of
    individual agency, if itis to be capable of explaining many social
    interactions, but also that this isnecessary if it is to be useful to
    the liberal debate over the scope of theState.

Page 237:

    sabotage

Page 238:

    What it does mean is thatour interpretation of results must be cautious
    and that, ultimately,laboratory experiments may only be telling us how
    people behave inlaboratories.

Page 241:

    becausethere are some players who are unconditionally cooperative or
    ‘altruistic’ in theway that they play this game and, secondly, because
    whether someone iscooperative or not seems to be determined by one’s
    background, rather thanby how clever (or rational) he or she is (see
    adjacent box on the curse ofeconomics). In this sense, the evidence
    seems to point to a falsification of theassumption of instrumentally
    rational action based on the pay-offs

Page 242:

    divisions of an army are stationed on two hill-tops overlooking a
    valley inwhich an enemy division can be clearly seen. It is known that
    if both divisionsattack simultaneously they will capture the enemy with
    none, or very little, lossof life. However, there were no prior plans
    to launch such an attack, as it wasnot anticipated that the enemy would
    be spotted in that location. How will thetwo divisions coordinate their
    attack (we assume that they must maintain visualand radio silence)?
    Neither commanding officer will launch an attack unless heis sure that
    the other will attack at the same time. Thus a classic
    coordinationproblem emerges.Imagine now that a messenger can be sent
    but that it will take him about anhour to convey the message. However,
    it is also possible that he will be caughtby the enemy in the meantime.
    If everything goes smoothly and the messengergets safely from one
    hill-top to another, is this enough for a coordinated attackto be
    launched? Suppose the message sent by the first commanding officer
    tothe second read: ‘Let’s attack at dawn!’ Will the second officer
    attack at dawn?No, unless he is confident that the first commanding
    officer (who sent the

Page 242:

    message) knows that the message has been received. So, the
    secondcommanding officer sends the messenger back to the first with the
    message:‘Message received. Dawn it is!’ Will the second officer attack
    now? Not untilhe knows that the messenger has delivered his message.
    Paradoxically, noamount of messages will do the trick since
    confirmation of receipt of the lastmessage will be necessary regardless
    of how many messages have been alreadyreceived.

Page 242:

    We see that in a coordination game like the above, even a very
    highdegree of common knowledge of the plan to attack at dawn is not
    enough toguarantee coordination (see Box 8.3 for an example of how
    different degreesof common knowledge can be engendered in the
    laboratory). What is needed(at least in theory) is a consistent
    alignment of beliefs (CAB) about the plan.1And yet this does not
    exclude the possibility that the two commandingofficers will both
    attack at dawn with very high probability. How successfullythey
    coordinate will, however, depend on more than a high degree ofcommon
    knowledge. Indeed the latter may even be un-necessary providedthe time
    of the attack is carefully chosen. The classic early experiments
    byThomas Schelling on behaviour in coordination games have confirmed
    this—

Page 246:

    Thus in experiments, Pareto superiority does not seem to be a
    generalcriterion which players use to select between Nash equilibria
    (see also Chapter7). In conclusion, so far it seems that the way people
    actually play these gamesis neither directly controlled by the
    strategic aspects of the game (i.e. thelocation of the best response
    marks (+) and (-) in the matrix) nor by the size ofthe return from
    coordinating on non-Nash outcomes such as (R3, C3): it is
    aso-far-unexplained mixture of the two factors that decides.

Page 251:

    To phrase this conclusion slightly differently, but in a way which
    connectswith the results in the next section, bargaining is a ‘complex
    socialphenomenon’ where people take cues from aspects of their social
    life whichgame theory typically overlooks. Thus players seem to base
    their behaviouron aspects of the social interaction which game theory
    typically treats asextraneous; and when players share these extraneous
    reference points such

Page 258:

    What we have here is an evolution ofsocial roles. Players with the R
    label develop a different attitude towardsreflective cooperation to
    those players with the C role in spite of the fact that theRs and the
    Cs are the same people. In other words, the signal which causes
    theobserved pattern of cooperation seems to be emitted by the label R
    or C. Thisreminds us of the discussion in Chapter 7 about the capacity
    of sex, race andother extraneous features to pin down a convention on
    which the structure ofdiscrimination is grounded.

Page 258:

    Experimentation with game theory is good, clean fun. Can it be more
    thanthat? Can it offer a way out of the obtuse debates on CKR, CAB,
    NEMS,Nash backward induction, out-of-equilibrium behaviour, etc.? The
    answerdepends on how we interpret the results. And as interpretation
    leaves plentyof room for controversy, we should not expect the data
    from the laboratoryunequivocally to settle any disputes. Our suspicion
    is that experiments are togame theory what the latter is to liberal
    individualism: a brilliant means ofcodifying its problems and of
    creating a taxonomy of time-honoureddebates.There are, however,
    important benefits from experimenting. Watchingpeople play games
    reminds us of their inherent unpredictability, their sense offairness,
    their complex motivation—of all those things that we tend to forgetwhen
    we model humans as bundles of preferences moving around some pay-

Page 258:

    radical breakwith the exclusive reliance of instrumental rationality is
    also necessary.

Page 260:

    At root we suspect that the major problem is the one that the
    experimentsin the last chapter isolate: namely, that people appear to
    be more complexlymotivated than game theory’s instrumental model allows
    and that a part ofthat greater complexity comes from their social
    location.We do not regard this as a negative conclusion. Quite the
    contrary, it standsas a challenge to the type of methodological
    individualism which has had afree rein in the development of game
    theory.

Page 260:

    Along the way to this conclusion, we hope also that you have had
    fun.Prisoners’ dilemmas and centipedes are great party tricks. They are
    easy todemonstrate and they are amenable to solutions which are
    paradoxical enoughto stimulate controversy and, with one leap of the
    liberal imagination, theaudience can be astounded by the thought that
    the fabric of society (even theexistence of the State) reduces to these
    seemingly trivial games—Fun andGames, as the title of Binmore’s (1992)
    text on game theory neatly puts it. Butthere is a serious side to all
    this. Game theory is, indeed, well placed toexamine the arguments in
    liberal political theory over the origin and the scopeof agencies for
    social choice like the State. In this context, the problems whichwe
    have identified with game theory resurface as timely warnings of
    thedifficulties any society is liable to face if it thinks of itself
    only in terms ofliberal individualism.

Page 260:

    The ambitious claim that game theory will provide a unified foundation
    for allsocial science seems misplaced to
