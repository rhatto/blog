[[!meta title="Entre o cristal e a fumaça"]]

## Geral

* Gráfico de Bourgeois, pág. 58.
* Delírio, aprendizagem, memória e novidade, 124.
* Metafísica, 146.
* Limites da teoria de Shannon para explicar a hipercomplexidade: ausência de significado, 171.
* Cérebro volumoso, juvenilização, aprendizagem, individuação, 172.
* Erros fecundos, erros fatais em Morin, diversificação, juventude, velhice, 180.

## Natural ou artificial

    Em particular, será um sistema humano - social, por exemplo -
    natural ou artificial? Pelo fato de ser fabricado por seres humanos, ele
    parece ser uma organização artificial, como todas as que resultam de
    planos e programas saídos de cérebros humanos. Nessa medida, a lógica
    dos sistemas naturais bem poderia afigurar-se inadequada, ou até deslo-
    cada e perigosa. Entretanto, pelo fato de uma organização social ser
    também o resultado da composição de efeitos de um grande número de
    indivíduos, trata-se igualmente, sob certos aspectos, de um sistema auto-
    organizador natural. Nele, forçosamente, o papel dos planos e programas
    é relativamente limitado pelo papel das finalidades e desejos dos indiví-
    duos e dos grupos. Mesmo nas sociedades totalitárias, a questão da origem
    da autoridade planificadora remete às motivações individuais que fazem
    com que a aceitemos ou nos adaptemos a ela. Essas motivações, conscien-
    tes e inconscientes, apesar de humanas, não provêm do cérebro de um
    engenheiro superdotado. O que equivale a dizer que, numa grande medida,
    também elas se oferecem a nossa observação sob a forma de sistemas
    naturais imperfeitamente conhecidos, constituídos por suas interações.

    -- 10

## Finalismo: teleonomia versus teleologia

    Na verdade, quer o admitamos ou não, há um finalismo implícito na
    maioria dos discursos biológicos. Ofa, essa situação é incômoda, do ponto
    de vista do método científico, por negar o princípio · de causalidade,
    segundo o qual as causas de um fenômeno devem ser descobertas antes,
    e não depois de sua ocorrência. Sendo esse princípio um fundamento do
    método científico, a impossibilidade de prescindir do finalismo na biolo-
    gia era uma deficiência dessa ciência que J. Monod analisou brilhante-
    mente na primeira parte de seu livro.

    [...]

    Resumida em termos muito sucintos, sua tese é a seguinte: um processo
    teleonômico não funciona em virtude das causas finais, apesar de ter essa
    aparência e embora pareça orientado para a realização de formas que só se
    evidenciarão no final do processo.  O que o determina, de fato, não são essas
    formas como causas finais, e sim a realização' de um programa, como numa
    máquina programada cujo funcionamento parece orientado para a realização de um
    estado futuro, quando, na verdade, é cau.s almente determinado pela seqüência
    de estados pela qual o programa preestabelecido a faz passar. O programa em si,
    contido no genoma característico da espécie, é o resultado da longa

    -- 18

## Origem da vida

    O problema da origem da vida, hoje em dia, é o do aparecimento do primeiro
    programa. De fato, a admitirmos a metáfora da programação genética contida nos
    ADNs - e veremos, mais adiante, que ela não está a salvo de sérias críticas - ,
    o programa do desenvolvimento de Um indivíduo lhe é fornecido no nascimento,
    por ocasião da fecundação do óvulo, a partir da replicação dos ADNs de seus
    pais. Assim, coloca-se a questão da origem do primeiro programa, isto é, do
    primeiro ADN capaz de se reproduzir e de codificar a síntese das enzimas.

    Ante essa questão, várias linhas de resposta são possíveis. Uma delas extrapola
    a reprodução laboratorial de condições físico-químicas que, supostamente,
    teriam sido as da atmosfera primitiva e da •·sopa•• primitiva. Ela se baseia
    nos resultados de experiências que demonstraram a possibilidade, nessas
    condições, de sínteses de aminoácidos e de nucleo- tídios, tijolos iniciais
    indispensáveis à fabricação do já complicadíssimo edifício desse primeiro
    programa. Evidentemente, devemos sublinhar o caráter hipotético dessas teorias,
    às quais J. Monod, por sua vez, não pareceu dar muita importância. Para ele, a
    questão da origem da vida e do primeiro programa era uma questão
    não-científica, pois concernia à ocor- rência de um evento de baixíssima
    probabilidade, mas que mesmo assim ocorreu, e de uma vez só. Para ele, já que
    nada além de encontros moleculares ao acaso poderia explicar a constituição do
    primeiro organis- mo vivo, e já que esta, em tais circunstâncias, só poderia
    ser imaginada com uma probabilidade praticamente nula, a questão de sua
    ocorrência não mais podia ser colocada em termos de probabilidade, a
    posteriori, agora que sabemos que isso aconteceu. Tratar-se-ia, portanto,
    tipicamente de um evçnto único, não-reprodutível, e que escaparia por definição
    ao campo de aplicação da pesquisa científica.

    Outros, ao contrário, como A. Katzir-Katchalsky, 10 M. Eigen 11 e 1.
    Prigogine, 12 não desistiram e partiram em busca de leis de organização -
    físico-químicas, é claro - que permitissem compreender, desta vez, não apenas
    que o primeiro programa não tivera uma probabilidade quase nula, mas que, ao
    contrário, sua ocorrência fora obrigatória e inelutável. Dentro dessa
    perspectiva, a origem da vida não teria sido um evento único de baixíssima
    probabilidade, mas um evento que se reproduziria todas as ve- zes que as
    condições físico-químicas da terra primitiva se materializassem.  A eventual
    descoberta de formas de vida em outros planetas seria, eviden- temente, um
    argumento a favor dessa segunda linha de pensamento.

    -- 21

## Ordem dos documentos

    É conhecida a história da escrivaninha e das prateleiras entulhadas de
    livros e documentos.• Estes, aparentemente, acham-se empilhados de qualquer
    maneira. No. entanto, seu dono sabe perfeitamente encontrar, se for preciso, o
    documento que procura. Ao contrário, quando, por infelid- dade, alguém ousa pôr
    ordem neles .. , é possível que o dono se tome incapaz de encontrar o que quer
    que seja. É evidente, neste caso, que a aparente desordem era uma ordem, e
    vice-versa. Aqui, trata-se de docu- mentos em sua relação com seu usuário. A
    desordem aparente oculta uma ordem determinada pelo conhecimento individual de
    cada um dos docu- mentos e de sua possível significação utilitária. Mas, em que
    aspecto essa ordem tem a aparência de desordem? É que, para o segundo
    observador, aquele que quer ºpôr em ordem .. , os documentos já não têm,
    individual- mente, a mesma significação. Em casos extremos, não têm
    significação alguma, a não ser a que se liga a sua forma geométrica e ao lugar
    que eles podem ocupar na escrivaninha e nas prateleiras, de maneira a que
    coinci- dam, em seu conjunto, com uma certa idéia a priori, com um padrão
    consiederado globalmente ordenado. Vemos, portanto, que a oposição entre ordem
    e aparência de ordem provém de os doc-umentos serem considerados, quer
    individualmente, com . sua significação, quer global- mente~ com uma
    significação individual diferente (determinada, por exemplo, por seu tamanho ou
    sua cor, ou por qualquer outro princípio de alinhamento importado de fora e sem
    a opinião de seu usuário), quer ainda sem significação alguma.

    -- 27

## Confiabilidade dos organismos

    Daí todo um campo de pesquisas, inaugurado por von Neumann [4] e seguido por
    muitos outros, especialmente Winograd e Cowan [3, 6], com a finalidade de
    descobrir princípios de construção de autômatos cuja confiabilidade fosse maior
    que a de seus componentes!  Essas pesquisas resultaram na definição de
    condições necessárias (e suficientes) para a realização desses autômatos. A
    maioria dessas condi- ções (redundância dos componentes, redundância das
    funções, complexi- dade dos componentes, deslocalização das funções) [6, 7]
    resultou numa espécie de compromisso entre determinismo e indeterminismo na
    cons- trução dos autômatos, como se uma certa quantidade de indeterminação
    fosse necessária, a partir de certo grau de complexidade, para pennitir ao
    sistema adaptar-se a um certo nível de ruído. Isso, evidentemente, não tleixa
    de lembrar um resultado análogo obtido na teoria dos jogos pelo mesmo Neumann [8].

    [...]

    Quando um sistema se fixa num estado particular, ele fica inadaptável, e esse
    estado final pode ser igualmente ruim. Ele será incapaz de se ajustar a alguma
    coisa que constitua uma situação inadequada" [9].

    -- 38

## Ordem pelo ruído

    Isso é apenas uma conseqüência de que, na ausência de erros de replicação,
    nenhuma novidade pode aparecer.

    -- 49

    Assim, ao menos em princípio, vemos como uma produção de informação
    sob o efeito de fatores aleatórios nada tem de misterioso: ela não passa da
    co.nseqüência de produções de erros num sistema repetitivo, constituído
    de maneira a não se: destruído quase que de imediato por um número
    relativamente pequeno de erros.

    Na verdade, no que concerne à evolução das espécies, nenhum
    mecanismo é concebível, à parte os que foram sugeridos por determinadas
    teorias, nas quais eventos aleatórios (mutações ao acaso) são responsáveis
    por uma evolução orientada para uma complexidade e uma riqueza ma-io-
    res da organização. No que concerne ao desenvolvimento e à maturação
    dos indivíduos, é muito possível que esses mecanismos também desem-
    penhem um papel nada desprezível, especialmente se incluirmos aí os
    fenômenos de aprendizagem adaptativa não dirigida, na qual o indivíduo
    se adapta a uma situação radicalmente nova, em que é difícil recorrer a
    um programa preestabelecido. De qualquer modo, essa noção de programa
    preestabelecido, aplicada aos organismos, é muito discutível, na medida
    em que se trata de programas de ••origem interna .. , fabricados pelos
    próprios organismos e modificados no curso de seu desenvolvimento. Na
    medida em que o genoma é fornecido de fora (pelos pais), é freqüente ele
    ser assemelhado a um programa de computador, mas essa semelhança nos
    parece inteiramente abusiva. Se há uma metáfora cibernética apta a ser
    utilizada para descrever o papel do genoma, a da memória nos parece
    muito mais adequada que a do programa, pois esta última implica todos
    os mecanismos de regulação que não se acham presentes no próprio
    genoma. Sem isso, não evitamos o paradoxo do programa que precisa dos
    produtos de sua execução para ser lido e executado. Ao contrário, as
    teorias da auto-organização permitem compreender a natureza lógica de
    sistemas onde o que desempenha a função do programa se modifica sem
    parar, de maneira não preestabelecida, sob o efeito de fatores .. aleató-
    rios" do ambiente, produtores de .. erros" no sistema.
    
    Mas, que são esses erros? Segundo o que acabamos de ver, até por
    causa de seus efeitos positivos, eles já não parecem ser erros em absoluto.
    O ruído provocado no sistema pelos fatores aleatórios do ambiente já não
    seria um verdadeiro ruído, a partir do momento em que fosse utilizado
    pelo sistema como fator de organização. Isso significaria que os fatores
    do ambiente não são aleatórios. Mas eles são. Ou, mais exatamente,
    depende da reação posterior do sistema em relação a eles o fato de, a
    posteriori, esses · fatores serem reconhecidos como aleatórios ou como
    parte de uma organização. A priori, eles são efetivamente aleatórios, se
    definirmos o acaso como a intersecção de duas cadeias de causalidade
    independentes: as causas de sua ocorrência nada têm a ver com o enca-
    deamento dos fenômenos que constituiu a história anterior do sistema até
    então. É nesse sentido que sua ocorrência e seu encontro com essa história
    constituem ruído, do ponto de vista das trocas de informação no sistema,
    e só são passíveis de produzir erros nele. Mas, a partir do momento em
    que o sistema é capaz de reagir a esses erros, de modo não apenas a não
    desaparecer, mas também a modificar a si mesmo num sentido que lhe
    seja benéfico, ou que, no mínimo, preserve sua sobrevivência posterior;
    em outras palavras, a partir do momento em que o sistema é capaz de
    integrar esses erros em sua própria organização, .eles então perdem um
    pouco, a posteriori, seu caráter de erros. Preservam-no apenas de um
    ponto de vista externo ao sistema; no sentido de que., como efeitos do
    ambiente sobre este, eles mesmos não correspondem a nenhum programa
    preestabelecido, contido no ambiente e destinado a organizar ou desorga-
    nizar o sistema. 11 Ao contrário, de um ponto de vista interno, na medida
    em que a organização consiste precisamente numa seqüência de desorga-
    nizações resgatadas, eles só aparecem como erros no instante exato de sua
    ocorrência e em relação a uma manutenção, que seria tão nefasta quanto
    imaginária, de um statu quo do sistema organizado, que imaginamos tão
    logo uma descrição estática dele nos possa ser dada. Caso contrário, e
    depois desse instante, eles são integrados e recuperados como fatores de
    organização. Os efeitos do ruído tomam-se, então, eventos da história do
    sistema e de seu processo de organização. Contudo, permanecem como
    efeitos de um ruído, visto que sua ocorrência era imprevisível.

    -- 50-51

## Ruído organizacional

    Uma das questões mais difíceis a propósito desse problema capital
    das organizações hierárquicas, que encontramos por toda parte na ·biolo-
    gia, é a seguinte: como passamos de um nível para outro, ou, mais
    precisamente, quais são as determinações causais que dirigem a passagem
    de um nível de integração para outro?

    Num sistema dinâmico, descrito por um sistema de equações dife-
    renciais, às funções (soluções do sistema) caracterizam o nível em que
    estamos interessados; as condições limites caracterizam o nível superior.
    Compreendemos perfeitamente como as condições limites, que impõem
    as constantes de integração, determinam as funções de soluções do siste-
    ma. Mas, inversamente, como podem as funções influenciar as condições
    limites? Em outras palavras, como pode um nível inferior - menos
    integrado - , na matemática, influenciar o nível superior? Como repre-
    sentar o efeito do nível molecular sobre as células, o das células nos órgãos
    e o dos órgãos no organismo, embora esse seja o pão de cada dia da
    observação biológica?

    -- 60

    Isso significa que a introdução da posição do observador não cons-
    titui apenas uma etapa lógica do raciocínio: esse observador, externo ao
    sistema, é, de fato, num sistema hierarquizado, o nível de organização
    superior (englobante), comparado aos sistemas-elementos que o consti-_
    tuem; é o órgão em relação à célula, o organismo em relação ao órgão etc.
    É em relação a ele que os efeitos do ruído sobre uma via no interior do
    sistema, em certas condições, podem ser positivos.

    -- 61

## Auto-organização e individuação

    A teoria da auto-organização fornece um princípio geral de diferenciação pela
    destruição, eventualmen- te aleatória, de uma redundância que caracteriza o
    estado inicial de indiferenciação. Assim, a quantidade de informação contida
    num eventual programa genético pode ser consideravelmente reduzida em
    comparação com a que seria necessária no caso de uma determinação rigorosa dos
    detalhes da diferenciação. Isso parece particularmente pertinente no que
    concerne ao desenvolvimento do sistema nervoso, onde uma parcela de
    aleatoriedade permite uma considerável economia de informação genéti- ca I 5
    que, de outra maneira, seria insuficiente, caso tivesse que especificar em
    todos os seus detalhes um sistema constituído de mais de dez bilhões de
    neurônios interligados. Também aí podemos observar, pelo menos em alguns casos,
    conexões inicialmente redundantes, que se especificam no curso do
    desenvolvimento, perdendo essa redundância. 16

    [...]

    Esses processos são empregados não apenas nos "reconhecimentos
    de formas" que caracterizam nosso sistema cognitivo, mas também na
    constituição e no funcionamento do sistema imunológico, verdadeira
    máquina de aprendizagem e de integração do novo, desta vez no nível de
    formas celulares e moleculares. De fato, o sistema imunológico realiza
    uma rede celular em que as células - os linfócitos - são ligadas, entre
    si e com os antígenos que constituem seus estímulos externos, por meca-
    nismos de reconhecimento molecular ao nível de suas membranas. Tam-
    bém aí estamos diante de um sistema de aprendizagem não-dirigida cujo
    desenvolvimento é condicionado pela história dos contatos com diferen-
    tes andgenos, uma história, evidentemente, pelo menos em parte, não-pro-
    gramada e aleatória. Ora, o reconhecimento dos antígenos pelos linfócitos
    é o resultado, no nível molecular e celular, de uma seleção de linfócitos
    preexistentes, com suas estruturas membranosas adequadas, cuja multi-
    plicação é desencadeada pelo contato com determinado antígeno (seleção
    clonai). Por isso, a possibilidade de uma variedade praticamente infinita
    e imprevisível de reações imunológicas, a partir de um número finito de
    linfócitos determinados, implica a cooperação de diversos níveis diferen-
    tes de reconhecimento. Uma combinação de células diferentes, pertencen-
    tes a níveis diferentes, multiplica consideravelmente a variedade das
    respostas possíveis (Jerne 18) . Por fim, também nesse caso, uma redundân-
    cia inicial nessa cooperação - transmissão de informações entre diferen-
    tes níveis da rede celular que constitui o sistema imunológico - talvez
    permita explicar o desenvolvimento com aumento da diversidade e da
    especificidade. 19 Este, no final das contas, leva à constituição da indivi-
    dualidade molecular de cada organismo, que, no homem, sabemos ser
    praticamente absoluta. Na verdade, ela é condicionada pelos encontros
    parcialmente aleatórios com estruturas moleculares e celulares trazidas
    por um ambiente sempre renovado, pelo menos em parte.

    -- 62-63

### Ruído e significação

    Como vimos anteriormente a propósito da história da escrivaninha desar-
    rumada, a idéia do sentido e da significação está sempre presente na noção
    de ordem, bem como na de informação. Contudo, vimos também que a
    teoria de Shannon só permitiu quantificar a informação ao preço da
    colocação de sua significação entre parênteses. O princípio da ordem a
    partir do ruído, em suas sucessivas formulações quantitativas (H. von
    Foerster, 1960; H. Atlan, 1968, 1972, 1975 2 º), utilizou igualmente a teoria
    de Shannon, da qual estão ausentes as preocupações com a significação.
    Na verdade, o problema do sentido e da significação. continua presente,
    muito embora o suponhamos eliminado. Está presente, é claro, nas noções
    de codificação e decodificação. Mas também está presente, de maneira
    implícita-negativa e como uma espécie de sombra, em todas as utilizações
    das noções de quantidade de informação ou de entropia para avaliar o
    estado de complexidade, de ordem ou desordem de um sistema. Finalmen-
    te, veremos que o princípio de ordem a partir do ruído, apesar de expresso
    num formalismo puramente probabilístico do qual o sentido se acha
    ausente, repousa implicitamente na existência da significação, e até de
    diversas significações da informação. Em outras palavras, trata-se de uma
    possível via .de abordagem para a solução do último dos problemas que a
    teoria de Shannon negligenciou: o da significação da informação. 2 1

    Para isso, é conveniente apreendermos, logo de saída, a inversão
    que efetuamos em relação à formulação inicial de von Foetster, quando
    exprimimos o princípio da ordem através do ruído como um aumento da
    variedade, da informação de Shannon e da complexidade, ligado a uma
    diminuição da redundância.

    -- 63-64

### Complexidade

    Em outras palavras, complexidade é uma desordem aparente onde temos razões para
    presumir uma ordem oculta; ou ainda, a complexidade é uma ordem cujo código
    não conhecemos.

    -- 67

    É pelo fato de a informação ser medida (por nós) por uma fórmula
    da qual o sentido está ausente, que seu oposto, o ruído, pode ser gerador
    de informação. Isso nos permite continuar a exprimi-lo pela mesma
    função H, embora sua significação seja diferente, por ser recebida em dois
    ·níveis diferentes de organização. A informação, num nível elementar, tem
    um sentido que desprezamos quando a medimos pelas fórmulas de Shan-
    non, mas que se traduz por seus efeitos em seu destinatário, a saber a
    estrutura e as funções desse nível, tal como as percebemos.

    -- 74-75

### Delírio

    Qualquer hipótese científica realmente nova é, de fato, da ordem do
    delírio, do ponto de vista de seu conteúdo, por se tratar de uma projéção
    do imaginário no real. É tão-somente por aceitar, a priori, a possibilidade
    de ser transformada ou mesmo abandonada, sob o efeito de confrontações
    com novas observações e experiências, qu~ ela fmalmente se separa disso.
    Em particular, poqemos compreender como a própria interpretação psica-
    nalítica pode desempenhar o papel de um delírio organizado, ou, ao
    contrário, o de uma criação libertária, conforme seja vivida de maneira
    fechada, como o modelo central - o padrão imutável-, o pólo organi-
    zador, ou de maneira aberta, como uma etapa fugaz no processo auto-or-
    ganizador. Entretanto, seja qual for o caso, o conteúdo da interpretação
    consiste sempre no que costumamos chamar "uma projeção do imaginá-
    rio no real".

    [...]

    Dentro dessa pers- pectiva, podemos compreender que esse desvelamento do
    delírio no Homo sapiens, latente, por ser inconsciente em seus predecessores,
    tenha sido concomitante a'o desenvolvimento da linguagem simbólica, na medi- da
    em que este implicou e permitiu, justamente, um considerável aumento das
    capacidades de memória, em comparação com as que lhe eram preexistentes.

    -- 124-125

### Humanismo

    Num artigo publicado há alguns anos, A. David constatou que cada
    um dos progressos da cibernética fazia o homem desaparecer um pouco
    mais [6]. Mas um último sobressalto de humanismo o fez localizar em nós
    o derradeiro recôndito de onde seria impossível desalojar o homem: seria
    o desejo (nosso programa, em outras palavras?). Mediante isso, ele nos
    sugeriu uma descrição futurista de homens telegrafados no espaço sob a
    forma de "programas puros ... Mas, que acontece com isso quando se
    constata que, nos sistemas cibernéticos auto-organizadores dotados da
    complexidade dos organismos vivos, o programa não pode ser localizado,
    porque se reconstitui sem parar? Pois bem, isso significa que o homem é
    finalmente desalojado até mesmo daí, e que para nós é melhor que seja
    assim, porque, dessa maneira, a unidade e a autonomia de nossa pessoa,
    na medida em que se produzirem, não mais poderão ser telegrafadas no
    espaço, separadas do resto, que a superfície que limita um volume e define
    sua unidade não pode ser separada desse volume. Alguns programas de
    organizações talvez possam ser telegrafados: os sistemas assim realizados
    talvez possam assemelhar-se a nós e dialogar conosco. Não há nada de
    inquietante nisso, 9 muito pelo contrário, porque eles não serão nós; como
    tampouco o são as máquinas, inclusive as mais poderosas, que nos
    prolongam.

    [6. A. David, "Nouvelles définitions de l'humanisme", in Wiener e Schadc,
    (orgs.), Progress in Biocybernetics, Nova York, Elsevier Publications Co.,
    1966.]

    -- 122

### Tempo e irreversibilidade

    Mas existe um outro tipo de situação, muito diferente, que aparece
    ao observarmos fenômenos naturais - não artificialmente criados por
    outro seres humanos -, e quando estes nos parecem orientados de tal
    maneira que as coisas acontecem como se fossem determinadas por um
    projeto, ou seja, também por uma vontade, um desejo ou uma intenção.
    Naturalmente, esse tipo de situação é encontrado, em especial, quando
    observamos os sistemas biológicos em todos os seus níveis de organiza-
    ção, exceto, talvez, ;io nível molecular. Isso explica que a biologia tenha
    freqüentemente dado margem a toda sorte de especulações místicas ou
    religiosas, e nem sempre no melhor sentido: se observamos fenômenos
    em que as coisas se produzem de maneira aparentemente finalista, como
    se resultassem de uma vontade (mesmo que não haja ninguém para nos
    dar informações sobre essa vontade), torna-se tentador, é claro, assimilar
    a existência dessa suposta vontade à vontade de Deus ou do Criador. O
    que vimos até o momento nos mostra em que sentido essa hipótese não é
    necessária, pois começamos a compreender como a matéria pode ser um
    locus de fenômenos de àuto-organização: em razão de diversos tipos de
    interações entre a ordem e o acaso, amostras de matéria podem evoluir de
    tal maneira que, aos olhos do observador externo, parecem determinadas
    por seu futuro, embora, na verdade, isso não aconteça.

    A verdade é que, nessas situações - e embora não sejamos obriga-
    dos a presumir a existência de uma vontade consciente -, estamos
    lidando com uma inversão local do tempo, na medida em que se produz
    uma diminuição local da entropia. Essa inversão não resulta, é claro, de
    uma vontade humana que dite sua orientação, e as vontades humanas são
    as únicas que conhecemos, porque a vontade de Deus é apenas uma
    abstração da vontade humana.

    -- 143

    A biologia físico-química nos indica - sem por isso nos dar
    nenhuma receita, é claro - como tudo isso é teoricamente possível, pek·
    menos em princípio, e como funciona nos sistemas biológicos em desen -
    volvimento. Exatamente, embora de maneira abstrata, isso pode se resu-
    mir assim: a habitual direção irreversível do tempo se inverte nos proces-
    sos em que a entropia de um sistema aberto decresce e em que a
    informação e a organização são criadas através da utilização de interações
    aleatórias do sistema com seu ambiente. Isso é apenas uma conseqüência
    direta do fato de que o habitual caráter irreversível do tempo, na física, é
    - determinado pela lei do aumento da entropia. De fato, daí decorre que,
    quando se pode produzir uma diminuição da entropia em algum lugar, é
    como se a direção do tempo, localmente, fosse invertida nesse ponto; o
    que equivale a dizer que a passagem do tempo, de destrutiva, toma-se
    criadora.

    -- 149

### Novas ciência e epistemologia

    Assim, a ciência do homem, visando a uma ciência do político, desembocaria
    inevitavelmente numa ciência do homem conhecedor e sábio, e portanto, numa
    ciência sobre a ciência, numa nova epistemologia, e portanto, num novo
    paradigma, numa nova prática científica. A reforma da ciência aqui conclamada
    implica uma superação da atitude operacional que se impôs e continua a se impor
    cada vez mais na prática científica: o objetivo da ciência já não é compreender
    - pois, afinal, que é compreender, se só nos colocamos problemas que podemos
      resolver e eliminamos todas as questões consideradas "não-científicas"? - , e
    sim resolver problemas de laboratório graças aos quais se molda um novo
    universo técnico e lógico, que tendemos a considerar -- em virtude de sua
    eficácia operacional - coincidente com a realidade física inteira. O fato de
    isso não acontecer, de esse universo ser cada vez mais artificial - para ser
    repetitivo e reproduzível, para que a antiga ciência possa aplicar-se a ele
    eficazmente-, constitui, evidentemente, a razão do abismo que reconhecemos,
    sempre com um certo espanto ingênuo, entre as ciências laboratoriais e a
    ciência do real vivido . Há nisso uma maquinação da epistemologia ocidental,
    que H. Marcuse, ao que saibamos, foi o primeiro a denunciar. Julgou-se que,
    para escapar aos engodos da metafísica, a ciência deveria ser apenas
    operacional, e eis que nos encerramos no universo alienante e unidimensional do
    operacional sem negatividade, onde o estrangeiro e o estranho são simplesmente
    rechaçados, afastados, quando não podem ser recuperados.

    -- 181-182
