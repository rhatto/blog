[[!meta title="Contra o Método"]]

* Autor: Paul Feyerabend
* Editora: Unesp
* Edição: 1a
* Ano: 2003

## Geral

* Viagens distintas: Odisseus e Sólon: descoberta e pesquisa, 252 (nota de rodapé).
* Lógica e ilógica, 259-262, 265.
* Participante/observador, 292.
* Filosofia pragmática, 293.
* Naturalismo versus idealismo, 299-300.
* Razão versus prática, 301.

## Conhecimento do conhecimento

* Cap. 11: importante! Mas também temos que levar em conta que Galileu era Galileu e não um Zé Ninguém, que poderia ser prontamente chamado de lunático.
* Descrição dinâmica de uma "seleção científica" envolvendo múltiplos fatores, por exemplo:

    Em suma: o que é necessário para submeter a teste a concepção de Copérnico é uma
    visão de mundo inteiramente nova contendo uma nova visão do homem e de suas capacidades
    de conhecer.

    É óbvio que tal nova visão de mundo levará um longo tempo a aparecer e talvez
    jamais tenhamos êxito em formulá-la em sua totalidade. É extremamente improvável
    que a idéia do movimento da Terra seja de modo imediato seguida pelo aparecimento,
    em pleno esplendor formal, de todas as ciências que, diz-se agora, constituírem
    o corpo da "física clássica". Ou, para ser um pouco mais realista, tal sequência
    de eventos não apenas é extremamente improvável, mas é impossível em princípio,
    dada a natureza dos humanos e das complexidades do mundo que habitam. Hoje
    Copérnico, amanhã Helmholtz -- isso não passa de um sonho utópico. Contudo,
    é somente depois que tenham surgido essas ciências que se pode dizer que um
    teste faz sentido.

    Essa necessidade de esperar e de ignorar grande massa de observações e medições
    críticas quase nunca é discutida em nossas metodologias.

    -- 164

## Regras e princípios

Regular, irregular, regra: 213.

    Mas nem as regras, nem os princípios nem tempouco os fatos são sacrossantos.
    O defeito pode encontrar-se nelees e não na idéia de que a Terra se move.

    -- 177

## Miséria do racionalismo

    A invenção de teorias depende de nossos talentos e de outras circunstâncias fortuitas,
    como uma vida sexual satisfatória. Contudo, enquanto subsistirem esses talentos,
    o esquema apresentado é uma explicação correta do desenvolvimento de um conhecimento
    que satisfaz as regras do racionalismo crítico.

    Ora, a essa altura, pode-se levantar duas questões:

    1. É desejável viver de acordo com as regras de um racionalismo crítico?
    2. É possível ter ambas as coisas, a ciência como a conhecemos e essas regras?

    No que me diz respeito, a primeira questão é bem mais importante que a segunda.
    De fato, a ciência e as instituições relacionadas desempenham um papel importante
    em nossa cultura e ocupam o centro de interesse pra muitos filósofos (a maioria
    dos filósofos é oportunista). Assim, as idéias da escola popperiana foram obtidas
    generalizando-se soluções para problemas metodológicos e epistemológicos. O racionalismo
    crítico surgiu da tentativa de entender a revolução einsteniana e foi depois
    estendido à política e mesmo à vida privada. Tal procedimento talvez satisfaça a
    um filósofo de escola, que olha a vida através dos óculos de seus próprios problemas
    técnicos e reconhece ódio, amor, felicidade somente conforme ocorrem nesses problemas.
    Mas, se considerarmos interesses humanos e, acima de tudo, a questão da liberdade
    humana (liberdade da fome, do desespero, da tirania de sistemas de pensamento
    emperrados e não a "liberdade da vontade" acadêmica), então estamos procedento
    da pior maneira possível.

    Com efeito, não é possível que a ciência tal como atualmente a conhecemos, ou uma
    "busca pela verdade", no estilo da filosofia tradicional, venha a criar um monstro?
    Não é possível que uma abordagem objetiva, que desaprova ligações pessoais entre
    as entidades examinadas, venha a causar danos às pessoas, transformando-as em mecanismos
    miseráveis, inamistosos e hipócritas, sem charme nem humor?

    -- 215.

## Racional e irracional

    Meu diagnóstico e minhas sugestões coincidem com os de Lakatos - até certo ponto.
    Lakatos identificou princípios de racionalidade excessivamente rígidos como a fonte
    de algumas versões de irracionalismo e insistiu conosco para que adotemos padrões novos
    e mais liberais. Identifiquei padrões de racionalidade excessivamente rígidos, bem
    como um respeito geral pela "razão", como a fonte de algumas formas de misticismo
    e irracionalismo, e também insisto na adoção de padrões mais liberais. Porém, ao passo
    que o grande "respeito pela ciência" que tem Lakatos leva-o a buscar esses padrões
    nos limites da ciência moderna "dos últimos dois séculos", recomendo colocar a ciência
    em seu lugar como uma forma de conhecimento interessante, mas de modo algum exclusiva,
    que tem muitas vantagens mas também muitos inconvenientes.

    -- 225

[[!tag epistemology philosophy]]
