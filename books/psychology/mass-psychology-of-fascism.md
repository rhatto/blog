[[!meta title="The Mass Psychology of Fascism"]]
[[!meta date="2018-10-07 15:00:00-0300"]]

* Author: Wilhelm Reich.
* Status: reading!

### Excerpts

    Revolutionary activity in every area of human existence will come about by itself
    when the contradictions in every new process are comprehended; it will consist of
    identification with those forces that are moving in the direction of genuine progress. To
    be radical, according to Karl Marx, means’ getting to the root of things’. If one gets to the
    root of things, if one grasps their contradictory operations, then the overcoming of
    political reaction is assured. If one does not get to the root of things, one ends, whether
    one wants to or not, in mechanism, in economism or even in metaphysics, and inevitably
    loses one’s footing. Hence, a critique can only be significant and have a practical value if
    it can show where the contradictions of social reality were overlooked. What was
    revolutionary about Marx was not that he wrote this or that proclamation or pointed out
    revolutionary goals; his major revolutionary contribution is that he recognized the
    industrial productive forces as the progressive force of society and that he depicted the
    contradictions of capitalist economy as they relate to real life. The failure of the workers’
    movement must mean that our knowledge of those forces that retard social progress is
    very limited, indeed, that some major factors are still altogether unknown.

    [...]

    It was this very vulgar Marxism that maintained that the economic crisis of 1929-33
    was of such a magnitude that it would of necessity lead to an ideological Leftist
    orientation among the stricken masses. While there was still talk of a ‘revolutionary
    revival’ in Germany, even after the defeat of January 1933, the reality of the situation
    showed that the economic crisis, which, according to expectations, was supposed to entail
    a development to the Left in the ideology of the masses, had led to an extreme
    development to the Right in the ideology of the proletarian strata of the population. The
    result was a cleavage between the economic basis, which developed to the Left, and the
    ideology of broad layers of society, which developed to the Right. This cleavage was
    overlooked; consequently, no one gave a thought to asking how broad masses living in
    utter poverty could become nationalistic. Explanations such as ‘chauvinism’, ‘psychosis’,
    ‘the consequences of Versailles’, are not of much use, for they do not enable us to cope
    with the tendency of a distressed middle class to become radical Rightist; such
    explanations do not really comprehend the processes at work in this tendency. In fact, it
    was not only the middle class that turned to the Right, but broad and not always the worst
    elements of the proletariat. One failed to see that the middle classes, put on their guard by
    the success of the Russian Revolution, resorted to new and seemingly strange
    preventative measures (such as Roosevelt’s ‘New Deal’), which were not understood at
    that time and which the workers’ movement neglected to analyse. One also failed to see
    that, at the outset and during the initial stages of its development to a mass movement,
    fascism was directed against the upper middle class and hence could not be disposed of
    ‘merely as a bulwark of big finance’, if only because it was a mass movement. Where
    was the problem?

    [...]

    The Marxist thesis to the effect that originally ‘that which is materialistic’ (existence)
    is converted into ‘that which is ideological’ (in consciousness), and not vice versa, leaves
    two questions open: (i) how this takes place, what happens in man’s brain in this process;
    and (2) how the ‘consciousness’ (we will refer to it as psychic structure from now on)
    that is formed in this way reacts upon the economic process. Character-analytic
    psychology fills this gap by revealing the process in man’s psychic life, which is
    determined by the conditions of existence. By so doing, it puts its finger on the
    ‘subjective factor’, which the vulgar Marxist had failed to comprehend. Hence, political
    psychology has a sharply delineated task. It cannot, for instance, explain the genesis of
    class society or the capitalist mode of production (whenever it attempts this, the result is
    always reactionary nonsense - for instance, that capitalism is a symptom of man’s greed).
    Nonetheless, it is political psychology - and not social economy -that is in a position to
    investigate the structure of man’s character in a given epoch, to investigate how he thinks
    and acts, how the contradictions of his existence work themselves out, how he tries to
    cope with this existence, etc. To be sure, it examines individual men and women only. If,
    however, it specializes in the investigation of typical psychic processes common to one
    category, class, professional group, etc., and excludes individual differences, then it

    [...]

    Hence, we are not saying anything new, and we are not revising Marx, as is so often
    maintained: ‘All human conditions ‘, that is, not only the conditions that are a part of the
    work process, but also the most private and most personal and highest accomplishments
    of human instinct and thought; also, in other words, the sexual life of women and
    adolescents and children, the level of the sociological investigation of these conditions
    and its application to new social questions. With a certain kind of these ‘human
    conditions’, Hitler was able to bring about a historical situation that is not to be ridiculed
    out of existence. Marx was not able to develop sociology of sex, because at that time
    sexology did not exist. Hence, it now becomes a question of incorporating both the purely
    economic and sex-economic conditions into the framework of sociology, of destroying
    the hegemony of the mystics and metaphysicians in this domain.


    [...]

    The ideology of every social formation has the function not only of reflecting the
    economic process of this society, but also and more significantly of embedding this
    economic process in the psychic structures of the people who make up the society. Man is
    subject to the conditions of his existence in a twofold way: directly through the
    immediate influence of his economic and social position, and indirectly by means of the
    ideological structure of the society. His psychic structure, in other words, is forced to
    develop a contradiction corresponding to the contradiction between the influence
    exercised by his material position and the influence exercised by the ideological structure
    of society. The worker, for instance, is subject to the situation of his work as well as to
    the general ideology of the society. Since man, however, regardless of class, is not only
    the object of these influences but also reproduces them in his activities, his thinking and
    acting must be just as contradictory as the society from which they derive. But, inasmuch
    as a social ideology changes man’s psychic structure, it has not only reproduced itself in
    man but, what is more significant, has become an active force, a material power in man,
    who in turn has become concretely changed, and, as a consequence thereof, acts in a
    different and contradictory fashion. It is in this way and only in this way that the


    [...]

    Thus, the statement that the ‘ideology’ changes at a slower pace than the economic basis
    is invested with a definite cogency. The basic traits of the character structures
    corresponding to a definite historical situation are formed in early childhood, and are far
    more conservative than the forces of technical production. It results from this that, as
    time goes on, the psychic structures lag behind the rapid changes of the social conditions
    from which they derived, and later tome into conflict with new forms of life. This is the
    basic trait of the nature of so-called tradition, i.e., of the contradiction between the old


    [...]

    result. Social psychology sees the problem in an entirely different light: what has to be
    explained is not the fact that the man who is hungry steals or the fact that the man who is
    exploited strikes, but why the majority of those who are hungry don’t steal and why the
    majority of those who are exploited don’t strike. Thus, social economy can give a
    complete explanation of a social fact that serves a rational end, i.e., when it satisfies an
    immediate need and reflects and magnifies the economic situation. The social economic
    explanation does not hold up, on the other hand, when a man’s thought and action are
    inconsistent with the economic situation, are irrational, in other words. The vulgar
    Marxist and the narrow-minded economist, who do not acknowledge psychology, are

    [...]

    thinking. Both assertions, because they failed to see the complexities of the issue, were
    rigidly mechanistic. A realistic appraisal would have had to point out that the average
    worker bears a contradiction in himself; that he, in other words, is neither a clear-cut
    revolutionary nor a clear-cut conservative, but stands divided. His psychic structure
    derives on the one hand from the social situation (which prepares the ground for
    revolutionary attitudes) and on the other hand from the entire atmosphere of authoritarian
    society - the two being at odds with one another.

    [...]

    concrete results solely through the activities of the masses subjected to them.
    To be sure, the freedom movements of Germany knew of the so-called ‘subjective
    factor of history’ (contrary to mechanistic materialism, Marx conceived of man as the
    subject of history, and it was precisely this side of Marxism that Lenin built upon); what
    was lacking was a comprehension of irrational, seemingly purposeless actions or, to put
    it another way, of the cleavage between economy and ideology. We have to be able to
    explain how it was possible for mysticism to have triumphed over scientific sociology.
    This task can be accomplished only if our line of questioning is such that a new mode of
    action results spontaneously from our explanation. If the working man is neither a clear-
    cut reactionary nor a clear-cut revolutionary, but is caught in a contradiction between
    reactionary and revolutionary tendencies, then if we succeed in putting our finger on this

    [...]

    contradiction, the result must be a mode of action that offsets the conservative psychic
    forces with revolutionary forces. Every form of mysticism is reactionary, and the
    reactionary man is mystical. To ridicule mysticism, to try to pass it off as ‘befogging’ or
    as ‘psychosis’, does not lead to a programme against mysticism. If mysticism is correctly
    comprehended, however, an antidote must of necessity result. But to accomplish this task,
    the relations between social situation and structural formation, especially the irrational
    ideas that are not to be explained on a purely socio-economic basis, have to be
    comprehended as completely as our means of cognition allow.

    [...]

    a number of new insights. It proceeds from the following presuppositions:
    Marx found social life to be governed by the conditions of economic production and
    by the class conflict that resulted from these conditions at a definite point of history. It is
    only seldom that brute force is resorted to in the domination of the oppressed classes by
    the owners of the social means of production; its main weapon is its ideological power
    over the oppressed, for it is this ideology that is the mainstay of the state apparatus. We
    have already mentioned that for Marx it is the living, productive man, with his psychic
    and physical disposition, who is the first presupposition of history and of politics. The
    character structure of active man, the so-called ‘subjective factor of history’ in Marx’s
    sense, remained uninvestigated because Marx was a sociologist and not a psychologist,
    and because at that time scientific psychology did not exist. Why man had allowed

    [...]

    himself to be exploited and morally humiliated, why, in short, he had submitted to
    slavery for thousands of years, remained unanswered; what had been ascertained were
    only the economic process of society and the mechanism of economic exploitation.
    Just about half a century later, using a special method he called psychoanalysis, Freud
    discovered the process that governs psychic life. His most important discoveries, which
    had a devastating and revolutionary effect upon a large number of existing ideas (a fact
    that garnered him the hate of the world in the beginning), are as follows:
    Consciousness is only a small part of the psychic life; it itself is governed by psychic
    processes that take place unconsciously and are therefore not accessible to conscious
    control. Every psychic experience (no matter how meaningless it appears to be), such as a
    dream, a useless performance, the absurd utterances of the psychically sick and mentally
    deranged, etc., has a function and a ‘meaning’ and can be completely understood if one
    can succeed in tracing its etiology. Thus psychology, which had been steadily
    deteriorating into a kind of physics of the brain (‘brain mythology’) or into a theory of a
    mysterious objective Geist, entered the domain of natural science.
    Freud’s second great discovery was that even the small child develops a lively

    [...]

    given in the way of progressive and revolutionary impetus. This is not the place to prove
    this. Psychoanalytic sociology tried to analyse society as it would analyse an individual,
    set up an absolute antithesis between the process of civilization and sexual gratification,
    conceived of destructive instincts as primary biological facts governing human destiny
    immutably, denied the existence of a matriarchal primeval period, and ended in a
    crippling scepticism, because it recoiled from the consequences of its own discoveries. Its
    hostility towards efforts proceeding on the basis of these discoveries goes back many
    years, and its representatives are unswerving in their opposition to such efforts. All of this
    has not the slightest effect on our determination to defend Freud’s great discoveries
    against every attack, regardless of origin or source.
