[[!meta title="Eros and Civilization"]]

* Author: Hebert Marcuse
* Some subjects covered (keywords): productivity, efficiency, labor, repression, domination, alienation, automation.

## Snippets

### From Pleasure Principle to Reality Principle

The becoming of an organized ego:

    The vicissitudes of the instincts are the vicissitudes of the mental apparatus
    in civilization. The animal drives become human instincts under the influence
    of the external reality. Their original "location" in the organism and their
    basic direction remain the same, but their objectives and their manifestations
    are subject to change. All psychoanalytic concepts (sublimation ,
    identification, projection, repression, introjection) connote the mutability of
    the instincts. But the reality which shapes the instincts as well as their
    needs and satisfaction is a socio-historical world. The animal man becomes a
    human being only through a fundamental transformation of his nature, affecting
    not only the instinctual aims but also the instinctual "values" -- that is, the
    principles that govern the attainment of the aims. The change in the governing
    value system may be tentatively defined as follows:

    from:                     to:
    immediate satisfaction    delayed satisfaction
    pleasure                  restraint of pleasure
    joy (play)                toil (work)
    receptiveness             productiveness
    absence of repression     security

    Freud described this change as the transformation of the pleasure principle
    into the reality principle. The interpretation of the "mental apparatus" in
    terms of these two principles is basic to Freud' s theory and remains so in
    spite of all modifications of the dualistic conception. It corresponds largely
    (but not entirely) to the distinction between unconscious and conscious
    processes. The individual exists, as it were, in two different dimensions,
    characterized by different mental processes and principles.

    The difference between these two dimensions is a genetic-historical as well as
    a structural one: the unconscious, ruled by the pleasure principle, comprises
    "the older, primary processes, the residues of a phase of development in which
    they were the only kind of mental processes." They strive for nothing but for
    "gaining pleasure; from any operation which might arouse unpleasantness (`
    pain') mental activity draws back." 1 But the unrestrained pleasure principle
    comes into conflict with the natural and human environment . The individual
    comes to the traumatic realization that full and painless gratification of his
    needs is impossible. And after this experience of disappointment, a new
    principle of mental functioning gains ascendancy. The reality principle
    supersedes the pleasure principle: man learns to give up momentary, uncertain,
    and destructive pleasure for delayed, restrained, but "assured" pleasure. 2
    Because of this lasting gain through renunciation and restraint, according to
    Freud, the reality principle "safeguards " rather than "dethrones," "modifies "
    rather than denies, the pleasure principle.

### Civilized Introjection: the self-repression

    The effective subjugation of the instincts to repressive controls is imposed
    not by nature but by man. The primal father, as the archetype of domination,
    initiates the chain reaction of enslavement, rebellion, and reinforced
    domination which marks the history of civilization. But ever since the first ,
    prehistoric restoration of domination following the first rebellion, repression
    from without has been supported by repression from within: the unfree
    individual introjects his masters and their commands into his own mental
    apparatus. The struggle against freedom reproduces itself in the psyche of man
    , as the self- repression of the repressed individual, and his self-repression
    in turn sustains his masters and their institutions. It is this mental dynamic
    which Freud unfolds as the dynamic of civilization.

    [...]

    Scarcity ( Lebensnot, Ananke) teaches men that they cannot freely gratify their
    instinctual impulses, that they cannot live under the pleasure principle.
    Society's motive in enforcing the decisive modification of the instinctual
    structure is thus "economic; since it has not means enough to support life for
    its members without work on their part, it must see to it that the number of
    these members is restricted and their energies directed away from sexual
    activities on to their work." 4

    [...]

    According to Freud's conception the equation of freedom and happiness tabooed
    by the conscious is upheld by the unconscious. Its truth, although repelled by
    consciousness, continues to haunt the mind; it preserves the memory of past
    stages of individual development at which integral gratification is obtained.
    And the past continues to claim the future: it generates the wish that the
    paradise be re-created on the basis of the achievements of civilization.

### Eros and Thanatos

At first it sounds like The Force from Star Wars...

    The pleasure principle, then., is a tendency operating in the service of a
    function whose business it is to free the mental apparatus entirely from
    excitation or to keep the amount of excitation in it constant or to keep it as
    low as possible. We cannot yet decide with certainty in favour of any of these
    ways of putting it. 5

    But more and more the inner logic of the conception asserts itself. Constant
    freedom from excitation has been finally abandoned at the birth of life; the
    instinctual tendency toward equilibrium thus is ultimately regression behind
    life itself. The primary processes of the mental apparatus, in their striving
    for integral gratification, seem to be fatally bound to the "most universal
    endeavour of all living substance -- namely to return to the quiescence of the
    inorganic world." 6 The instincts are drawn into the orbit of death. "If it is
    true that life is governed by Fechner's principle of constant equilibrium, it
    consists of a continuous descent toward death." 7 The Nirvana principle  now
    emerges as the "dominating tendency of mental life, and perhaps of nervous life
    in general." And the pleasure principle appears in the light of the Nirvana
    principle -- as an "expression" of the Nirvana principle: . . the effort to
    reduce, to keep constant or to remove internal tension due to stimuli (the
    "Nirvana Principle".. )... finds expression in the pleasure principle; and our
    recognition of this fact is one of our strongest reasons for believing in the
    existence of death instincts. 8

    However, the primacy of the Nirvana principle, the terrifying convergence of
    pleasure and death, is dissolved as soon as it is established. No matter how
    universal the regressive inertia of organic life, the instincts strive to
    attain their objective in fundamentally different modes. The difference is
    tantamount to that of sustaining and destroying life. Out of the common nature
    of instinctual life develop two antagonistic instincts. The life instincts
    (Eros) gain ascendency over the death instincts. They continuously counteract
    and delay the "descent toward death": "fresh tensions are introduced by the
    claims of Eros, of the sexual instincts, as expressed in instinctual needs." 9
    They begin their life-reproducing function with the separation of the germ
    cells from the organism and the coalescence of two such cell bodies, 10
    proceeding to the establishment and preservation of "ever greater unities" of
    life. 11

    They thus win, against death, the "potential immortality" of the living
    substance. 12 The dynamic dualism of instinctual life seems assured. However,
    Freud at once harks back to the original common nature of the instincts. The
    life instincts "are conservative in the same sense as the other instincts in
    that they bring back earlier states of the living substance" -- although they
    are conservative "to a higher degree." 13 Sexuality would thus ultimately obey
    the same principle as the death instinct. Later, Freud, in order to illustrate
    the regressive character of sexuality, recalls Plato's "fantastic hypothesis"
    that "living substance at the time of its coming to life was torn apart into
    small particles, which have ever since endeavoured to reunite through the
    sexual instincts." 14 Does Eros, in spite of all the evidence, in the last
    analysis work in the service of the death instinct, and is life really only one
    long "detour to death"? 15 But the evidence is strong enough, and the detour is
    long enough to warrant the opposite assumption. Eros is defined as the great
    unifying force that preserves all life. 16 The ultimate relation between Eros
    and Thanatos remains obscure.

    If Eros and Thanatos thus emerge as the two basic instincts whose ubiquitous
    presence and continuous fusion (and de-fusion) characterize the life process,
    then this theory of instincts is far more than a reformulation of the preceding
    Freudian concepts.

    [...]

    However, the discovery of the common "conservative nature" of the instincts
    militates against the dualistic conception and keeps Freud's late
    metapsychology in that state of suspense and depth which makes it one of the
    great intellectual ventures in the science of man. The quest for the common
    origin  of the two basic instincts can no longer be silenced.  Fenichel pointed
    out 20 that Freud himself made a decisive step in this direction by assuming a
    "displaceable energy, which is in itself neutral, but is able to join forces
    either with an erotic or with a destructive impulse" -- with the life or the
    death instinct. Never before has death been so consistently taken into the
    essence of life; but never before also has death come so close to Eros.
    Fenichel raises the decisive question whether the antithesis of Eros and death
    instinct is not the "differentiation of an originally common root." He suggests
    that the phenomena grouped together as the death instinct may be taken as
    expression of a principle "valid for all instincts," a principle which, in the
    course of development, "might have been modified.. by external influences ."
    Moreover, if the "regression-compulsion " in all organic life is striving for
    integral quiescence, if the Nirvana principle is the ground of the pleasure
    principle, then the necessity of death appears in an entirely new light. The
    death instinct is destructiveness not for its own sake, but for the relief of
    tension. The descent toward death is an unconscious flight from pain and want.
    It is an expression of the eternal struggle against suffering and repression.
    And the death instinct itself seems to be affected by the historical changes
    which affect this struggle. Further explanation of the historical character of
    the instincts requires placing them in the new concept of the person  which
    corresponds to the last version of Freud's theory of instincts.

### A person

* The main "layers" of the mental structure are now designated as id, ego, and superego.
* The id is free from the forms.
* Ego: the "mediator" between the id and the external world.

Superego:

    This development, by which originally conscious struggles with the demands of
    reality (the parents and their successors in the formation of the superego) are
    transformed into unconscious automatic reactions, is of the utmost importance
    for the course of civilization. The reality principle asserts itself through a
    shrinking of the conscious ego in a significant direction: the autonomous
    development of the instincts is frozen, and their pattern is fixed at the
    childhood level. Adherence to a status quo ante  is implanted in the
    instinctual structure. The individual becomes instinctually re-actionary -- in
    the literal as well as the figurative sense.

### Biological and historical processes

    (a) Surplus-repression:  the restrictions necessitated by social domination.
    This is distinguished from (basic) repression:  the "modifications " of the
    instincts necessary for the perpetuation of the human race in civilization.

    (b) Performance principle:  the prevailing historical form of the reality principle.

    Behind the reality principle lies the fundamental fact of Ananke or scarcity (
    Lebensnot), which means that the struggle for existence takes place in a world
    too poor for the satisfaction of human needs without constant restraint,
    renunciation, delay. In other words, whatever satisfaction is possible
    necessitates work, more or less painful arrangements and undertakings for the
    procurement of the means for satisfying needs. For the duration of work, which
    occupies practically the entire existence of the mature individual, pleasure is
    "suspended" and pain prevails.

    However, this argument, which looms large in Freud' s metapsychology, is
    fallacious in so far as it applies to the brute fact  of scarcity what actually
    is the consequence of a specific organization  of scarcity, and of a specific
    existential attitude enforced by this organization.
    The prevalent scarcity has, throughout civilization (although in very different
    modes), been organized in such a way that it has not been distributed
    collectively in accordance with individual needs, nor has the procurement of
    goods for the satisfaction of needs been organized with the objective of best
    satisfying the developing needs of the individuals.
    Instead, the distribution  of scarcity as well as the effort of overcoming it,
    the mode of work, have been imposed  upon individuals -- first by mere
    violence, subsequently by a more rational utilization of power.
    Domination differs from rational exercise of authority. The latter, which is
    inherent in any societal division of labor, is derived from knowledge and
    confined to the administration of functions and arrangements necessary for the
    advancement of the whole. In contrast, domination is exercised by a particular
    group or individual in order to sustain and enhance itself in a privileged
    position.

    [...]

    Moreover, while any form of the reality principle demands a considerable degree
    and scope of repressive control over the instincts, the specific historical
    institutions of the reality principle and the specific interests of domination
    introduce additional  controls over and above those indispensable for civilized
    human association. These additional controls arising from the specific
    institutions of domination are what we denote as surplus-repression.

### Primeval revolutions and counter-revolutions: the return of the repressed

    The role of the women gains increasing importance . "A good part of the power
    which had become vacant through the father' s death passed to the women; the
    time of the matriarchate followed." 11 It seems essential for Freud' s
    hypothesis that in the sequence of the development toward civilization the
    matriarchal period is preceded  by primal patriarchal despotism: the low degree
    of repressive domination, the extent of erotic freedom, which are traditionally
    associated with matriarchy appear, in Freud's hypothesis, as consequences of
    the overthrow of patriarchal despotism rather than as primary "natural"
    conditions. In the development of civilization, freedom becomes possible only
    as liberation. Liberty follows  domination -- and leads to the reaffirmation of
    domination. Matriarchy is replaced by a patriarchal counter-revolution, and the
    latter is stabilized by the institutionalization of religion.

    Male gods at first appear as sons by the side of the great mother-deities, but
    gradually they assume the features of the father; polytheism cedes to
    monotheism, and then returns the "one and only father deity whose power is
    unlimited." 13 Sublime and sublimated, original domination becomes eternal,
    cosmic, and good, and in this form guards the process of civilization. The
    "historical rights" of the primal father are restored.

    [...]

    Must not their sense of guilt include guilt about the betrayal and denial of
    their deed? Are they not guilty of restoring the repressive father, guilty of
    self-imposed perpetuation of domination? The question suggests itself if
    Freud's phylogenetic hypothesis is confronted with his notion of the
    instinctual dynamic. As the reality principle takes root, even in its most
    primitive and most brutally enforced form, the pleasure principle becomes
    something frightful and terrifying; the impulses for free gratification meet
    with anxiety, and this anxiety calls for protection against them. The
    individuals have to defend themselves against the specter of their integral
    liberation from want and pain, against integral gratification. And the latter
    is represented by the woman who, as mother, has once, for the first and last
    time, provided such gratification. These are the instinctual factors which
    reproduce the rhythm of liberation and domination.

    [...]

    If we follow this train of thought beyond Freud, and connect it with the
    twofold origin of the sense of guilt, the life and death of Christ would appear
    as a struggle against the father -- and as a triumph over the father. 21 The
    message of the Son was the message of liberation: the overthrow of the Law
    (which is domination) by Agape (which is Eros). This would fit in with the
    heretical image of Jesus as the Redeemer in the flesh, the Messiah who came to
    save man here on earth. Then the subsequent transubstantiation of the Messiah,
    the deification of the Son beside the Father, would be a betrayal of his
    message by his own disciples -- the denial of the liberation in the flesh, the
    revenge on the redeemer. Christianity would then have surrendered the gospel of
    Agape-Eros again to the Law; the father-rule would be restored and
    strengthened. In Freudian terms, the primal crime could have been expiated,
    according to the message of the Son, in an order of peace and love on earth. It
    was not; it was rather superseded by another crime -- that against the Son.
    With his transubstantiation, his gospel too was transubstantiated; his
    deification removed his message from this world. Suffering and repression were
    perpetuated.

    [...]

    We have seen that Freud's theory is focused on the recurrent cycle
    "domination-rebellion-domination." But the second domination is not simply a
    repetition of the first one; the cyclical movement is progress  in domination.
    From the primal father via the brother clan to the system of institutional
    authority characteristic of mature civilization, domination becomes
    increasingly impersonal, objective, universal, and also increasingly rational,
    effective, productive. At the end, under the rule of the fully developed
    performance principle, subordination appears as implemented through the social
    division of labor itself (although physical and personal force remains an
    indispensable instrumentality).

    [...]

    The development of a hierarchical system of social labor not only rationalizes
    domination but also "contains" the rebellion against domination. At the
    individual level, the primal revolt is contained within the framework of the
    normal Oedipus conflict. At the societal level, recurrent rebellions and
    revolutions have been followed by counterrevolutions and restorations. From the
    slave revolts in the ancient world to the socialist revolution, the struggle of
    the oppressed has ended in establishing a new, "better" system of domination;
    progress has taken place through an improving chain of control. Each revolution
    has been the conscious effort to replace one ruling group by another; but each
    revolution has also released forces that have "overshot the goal," that have
    striven for the abolition of domination and exploitation. The ease with which
    they have been defeated demands explanations. The ease with which they have
    been defeated demands explanations. Neither the prevailing constellation of
    power, nor immaturity of the productive forces, nor absence of class
    consciousness provides an adequate answer. In every revolution, there seems to
    have been a historical moment when the struggle against domination might have
    been victorious -- but the moment passed. An element of self-defeat  seems to
    be involved in this dynamic (regardless of the validity of such reasons as the
    prematurity and inequality of forces ). In this sense, every revolution has
    also been a betrayed revolution.

### Technics

    Technics provide the very basis for progress; technological rationality sets
    the mental and behaviorist pattern for productive performance, and "power over
    nature" has become practically identical with civilization. Is the
    destructiveness sublimated in these activities sufficiently subdued and
    diverted to assure the work of Eros? It seems that socially useful
    destructiveness is less sublimated than socially useful libido. To be sure, the
    diversion of destructiveness from the ego to the external world secured the
    growth of civilization. However, extroverted destruction remains destruction:
    its objects are in most cases actually and violently assailed, deprived of
    their form, and reconstructed only after partial destruction; units are
    forcibly divided, and the component parts forcibly rearranged. Nature is
    literally "violated." Only in certain categories of sublimated aggressiveness
    (as in surgical practice) does such violation directly strengthen the life of
    its object. Destructiveness, in extent and intent, seems to be more directly
    satisfied in civilization than the libido.

    [...]

    Then, through constructive technological destruction, through the constructive
    violation of nature, the instincts would still operate toward the annihilation
    of life. The radical hypothesis of Beyond the Pleasure Principle  would stand:
    the instincts of self-preservation, self-assertion, and mastery, in so far as
    they have absorbed this destructiveness, would have the function of assuring
    the organism' s "own path to death."

    [...]

    The growing mastery of nature then would, with the growing productivity of
    labor, develop and fulfill the human needs only as a by-product:  increasing
    cultural wealth and knowledge would provide the material for progressive
    destruction and the need for increasing instinctual repression.

    [...]

    However, the very progress of civilization tends to make this rationality a
    spurious one. The existing liberties and the existing gratifications are tied
    to the requirements of domination; they themselves become instruments of
    repression. The excuse of scarcity, which has justified institutionalized
    repression since its inception, weakens as man 's knowledge and control over
    nature enhances the means for fulfilling human needs with a minimum of toil.
    The still prevailing impoverishment of vast areas of the world is no longer due
    chiefly to the poverty of human and natural resources but to the manner in
    which they are distributed and utilized.

    This difference may be irrelevant to politics and to politicians but it is of
    decisive importance to a theory of civilization which derives the need for
    repression from the "natural" and perpetual disproportion between human desires
    and the environment in which they must be satisfied. If such a "natural"
    condition, and not certain political and social institutions, provides the
    rationale for repression, then it has become irrational. The culture of
    industrial civilization has turned the human organism into an ever more
    sensitive, differentiated, exchangeable instrument, and has created a social
    wealth sufficiently great to transform this instrument into an end in itself.
    The available resources make for a qualitative  change in the human needs.
    Rationalization and mechanization of labor tend to reduce the quantum of
    instinctual energy channeled into toil (alienated labor), thus freeing energy
    for the attainment of objectives set by the free play of individual faculties.

    Technology operates against the repressive utilization of energy in so far as
    it minimizes the time necessary for the production of the necessities of life,
    thus saving time for the development of needs beyond  the realm of necessity
    and of necessary waste.

    But the closer the real possibility of liberating the individual from the
    constraints once justified by scarcity and immaturity, the greater the need for
    maintaining and streamlining these constraints lest the established order of
    domination dissolve. Civilization has to defend itself against the specter of a
    world which could be free. If society cannot use its growing productivity for
    reducing repression (because such usage would upset the hierarchy of the status
    quo), productivity must be turned against  the individuals; it becomes itself
    an instrument of universal control. Totalitarianism spreads over late
    industrial civilization wherever the interests of domination prevail upon
    productivity, arresting and diverting its potentialities. The people have to be
    kept in a state of permanent mobilization, internal and external. The
    rationality of domination has progressed to the point where it threatens to
    invalidate its foundations; therefore it must be reaffirmed more effectively
    than ever before. This time there shall be no killing of the father, not even a
    "symbolic" killing -- because he may not find a successor.

    [...]

    Note: 20 In his paper on "The Delay of the Machine Age," Hanns Sachs made an
    interesting attempt to demonstrate narcissism as a constitutive element of the
    reality principle in Greek civilization. He discussed the problem of why the
    Greeks did not develop a machine technology although they possessed the skill
    and knowledge which would have enabled them to do so. He was not satisfied with
    the usual explanations on economic and sociological grounds. Instead, he
    proposed that the predominant narcissistic element in Greek culture prevented
    technological progress: the libidinal cathexis of the body was so strong that
    it militated against mechanization and automatization. Sachs' paper appeared in
    the Psychoanalytic Quarterly, II (1933) , 42off.

### Repression due to exogenous factors: the central argument

    Therefore, if the historical process tended to make obsolete the institutions
    of the performance principle, it would also tend to make obsolete the
    organization of the instincts -- that is to say, to release the instincts from
    the constraints and diversions required by the performance principle. This
    would imply the real possibility of a gradual elimination of
    surplus-repression, whereby an expanding area of destructiveness could be
    absorbed or neutralized by strengthened libido. Evidently, Freud' s theory
    precludes the construction of any psychoanalytical utopia. If we accept his
    theory and still maintain that there is historical substance in the idea of a
    non-repressive civilization, then it must be derivable from Freud's instinct
    theory itself. His concepts must be examined to discover whether or not they
    contain elements that require reinterpretation. This approach would parallel
    the one used in the preceding sociological discussion.

    [...]

    Freud maintains that an essential conflict between the two principles is
    inevitable; however, in the elaboration of his theory, this inevitability seems
    to be opened to question. The conflict, in the form it assumes in civilization,
    is said to be caused and perpetuated by the prevalence of Ananke, Lebensnot,
    the struggle for existence. (The later stage of the instinct theory, with the
    concepts of Eros and death instinct, does not cancel this thesis: Lebensnot
    now appears as the want and deficiency inherent in organic life itself.) The
    struggle for existence necessitates the repressive modification of the
    instincts chiefly because of the lack of sufficient means and resources for
    integral, painless and toilless gratification of instinctual needs. If this is
    true, the repressive organization of the instincts in the struggle for
    existence would be due to exogenous  factors -- exogenous in the sense that
    they are not inherent in the "nature" of the instincts but emerge from the
    specific historical conditions under which the instincts develop.

    [...]

    According to Freud, this distinction is meaningless, for the instincts
    themselves are "historical"; 1 there is no instinctual structure "outside" the
    historical structure. However, this does not dispense with the necessity of
    making the distinction -- except that it must be made within  the historical
    structure itself. The latter appears as stratified on two levels: (a) the
    phylogenetic-biological level, the development of the animal man in the
    struggle with nature; and (b) the sociological level, the development of
    civilized individuals and groups in the struggle among themselves and with
    their environment .

    The two levels are in constant and inseparable interaction, but factors
    generated at the second level are exogenous to the first and have therefore a
    different weight and validity (although, in the course of the development, they
    can "sink down" to the first level): they are more relative; they can change
    faster and without endangering or reversing the development of the genus. This
    difference in the origin of instinctual modification underlies the distinction
    we have introduced between repression and surplus-repression; 2 the latter
    originates and is sustained at the sociological level.

    [...]

    For his metapsychology, it is not decisive whether the inhibitions are imposed
    by scarcity or by the hierarchical distribution  of scarcity, by the struggle
    for existence or by the interest in domination. And indeed the two factors --
    the phylogenetic-biological and the sociological -- have grown together in the
    recorded history of civilization. But their union has long since become
    "unnatural" -and so has the oppressive "modification" of the pleasure principle
    by the reality principle. Freud' s consistent denial of the possibility of an
    essential liberation of the former implies the assumption that scarcity is as
    permanent as domination -- an assumption that seems to beg the question. By
    virtue of this assumption, an extraneous fact obtains the theoretical dignity
    of an inherent element of mental life, inherent even in the primary instincts.
    In the light of the long-range trend of civilization, and in the light of
    Freud' s own interpretation of the instinctual development, the assumption must
    be questioned. The historical piossibility of a gradual decontrolling of the
    instinctual development must be taken seriously, perhaps even the historical
    necessity -- if civilization is to progress to a higher stage of freedom.

    [...]

    The diagram sketches a historical sequence from the beginning of organic life
    (stages 2 and 3), through the formative stage of the two primary instincts (5),
    to their "modified " development as human instincts in civilization (6-7). The
    turning points are at stages 3 and 6. They are both caused by exogenous factors
    by virtue of which the definite formation as well as the subsequent dynamic of
    the instincts become "historically acquired." At stage 3, the exogenous factor
    is the " unrelieved tension " created by the birth of organic life; the
    "experience" that life is less "satisfactory," more painful, than the preceding
    stage generates the death instinct as the drive for relieving this tension
    through regression. The working of the death instinct thus appears as the
    result of the trauma of primary frustration: want and pain, here caused by a
    geological-biological event.

    The other turning point, however, is no longer a geological-biological one: it
    occurs at the threshold of civilization. The exogenous factor here is Ananke,
    the conscious struggle for existence. It enforces the repressive controls of
    the sex instincts (first through the brute violence of the primal father, then
    through institutionalization and internalization), as well as the
    transformation of the death instinct into socially useful aggression and
    morality. This organization of the instincts (actually a long process) creates
    the civilized division of labor, progress, and law and order"; but it also
    starts the chain of events that leads to the progressive weakening of Eros and
    thereby to the growth of aggressiveness and guilt feeling. We have seen that
    this development is not "inherent" in the struggle for existence but only in
    its oppressive organization, and that at the present stage the possible
    conquest of want makes this struggle ever more irrational.

    [...]

    In the biological-geological conditions which Freud assumed for the living
    substance as such, no such change can be envisaged; the birth of life continues
    to be a trauma, and thus the reign of the Nirvana principle seems to be
    unshakable. However, the derivatives of the death instinct operate only in
    fusion with the sex instincts; as long as life grows, the former remain
    subordinate to the latter; the fate of the destrudo (the "energy" of the
    destruction instincts) depends on that of the libido. Consequently, a
    qualitative change in the development of sexuality must necessarily alter the
    manifestations of the death instinct.

    Thus, the hypothesis of a non-repressive civilization must be theoretically
    validated first by demonstrating the possibility of a nonrepressive development
    of the libido under the conditions of mature civilization. The direction of
    such a development is indicated by those mental forces which, according to
    Freud, remain essentially free from the reality principle and carry over this
    freedom into the world of mature consciousness. Their re-examination must be
    the next step.

### Detours to death: death instinct and negentropy

    Our re-examination must therefore begin with Freud's analysis of the death
    instinct.  We have seen that, in Freud's late theory of the instincts, the
    "compulsion inherent in organic life to restore an earlier state of things
    which the living entity has been obliged to abandon under the pressure of
    external disturbing forces" 4 is common to both primary instincts: Eros and
    death instinct. Freud regards this retrogressive tendency as an expression of
    the "inertia" in organic life, and ventures the following hypothetical
    explanation: at the time when life originated in inanimate matter, a strong
    "tension" developed which the young organism strove to relieve by returning to
    the inanimate condition. 5 At the early stage of organic life, the road to the
    previous state of inorganic existence was probably very short, and dying very
    easy; but gradually "external influences " lengthened this road and compelled
    the organism to take ever longer and more complicated "detours to death."

[[!img detours-to-death.png link="no"]]

### Phantasy

    Phantasy plays a most decisive function in the total mental structure: it links
    the deepest layers of the unconscious with the highest products of
    consciousness (art), the dream with the reality; it preserves the archetypes of
    the genus, the perpetual but repressed ideas of the collective and individual
    memory, the tabooed images of freedom.

    [...]

    The recognition of phantasy (imagination) as a thought process with its own
    laws and truth values was not new in psychology and philosophy; Freud' s
    original contribution lay in the attempt to show the genesis of this mode of
    thought and its essential connection with the pleasure principle. The
    establishment of the reality principle causes a division and mutilation of the
    mind which fatefully determines its entire development. The mental process
    formerly unified in the pleasure ego is now split: its main stream is channeled
    into the domain of the reality principle and brought into line with its
    requirements. Thus conditioned, this part of the mind obtains the monopoly of
    interpreting, manipulating, and altering reality -- of governing remembrance
    and oblivion, even of defining what reality is and how it should be used and
    altered. The other part of the mental apparatus remains free from the control
    of the reality principle -- at the price of becoming powerless,
    inconsequential, unrealistic.
    Whereas the ego was formerly guided and driven by the whole  of its mental
    energy, it is now to be guided only by that part of it which conforms to the
    reality principle. This part and this part alone is to set the objectives,
    norms, and values of the ego; as reason  it becomes the sole repository of
    judgment, truth, rationality; it decides what is useful and useless, good and
    evil. 2 Phantasy  as a separate mental process is born and at the same time
    left behind by the organization of the pleasure ego into the reality ego.
    Reason prevails: it becomes unpleasant but useful and correct; phantasy remains
    pleasant but becomes useless, untrue -- a mere play, daydreaming. As such, it
    continues to speak the language of the pleasure principle, of freedom from
    repression, of uninhibited desire and gratification -- but reality proceeds
    according to the laws of reason, no longer committed to the dream language.

    [...]

    The danger of abusing the discovery of the truth value of imagination for
    retrogressive tendencies is exemplified by the work of Carl Jung.

## Unsublimated pleasure

    Smell and taste give, as it were, unsublimated pleasure per se (and unrepressed
    disgust). They relate (and separate) individuals immediately, without the
    generalized and conventionalized forms of consciousness, morality, aesthetics.
    Such immediacy is incompatible with the effectiveness of organized domination,
    with a society which "tends to isolate people, to put distance between them,
    and to prevent spontaneous relationships and thènatural' animal -like
    expressions of such relations."

### Art

    Still, within the limits of the aesthetic form, art expressed, although in an
    ambivalent manner , the return of the repressed image of liberation; art was
    opposition. At the present stage, in the period of total mobilization, even
    this highly ambivalent opposition seems no longer viable. Art survives only
    where it cancels itself , where it saves its substance by denying its
    traditional form and thereby denying reconciliation: where it becomes
    surrealistic and atonal. 6 Otherwise, art shares the fate of all genuine human
    communication : it dies off.

    [...]

    In a less sublimated form, the opposition of phantasy to the reality principle
    is more at home in such sub-real and surreal processes as dreaming,
    daydreaming, play, the "stream of consciousness."

    [...]

    The surrealists recognized the revolutionary implications of Freud' s
    discoveries: "Imagination is perhaps about to reclaim its rights."
    13 But when they asked, "Cannot the dream also be applied to the solution of
    the fundamental problems of life?" 14 they went beyond psychoanalysis in
    demanding that the dream be made into reality without compromising its content.
    Art allied itself with the revolution. Uncompromising adherence to the strict
    truth value of imagination comprehends reality more fully. That the
    propositions of the artistic imagination are untrue in terms of the actual
    organization of the facts belongs to the essence of their truth: The truth that
    some proposition respecting an actual occasion is untrue may express the vital
    truth as to the aesthetic achievement. It expresses the "great refusal" which
    is its primary characteristic. 15 This Great Refusal is the protest against
    unnecessary repression, the struggle for the ultimate form of freedom -- "to
    live without anxiety." 16 But this idea could be formulated without punishment
    only in the language of art. In the more realistic context of political theory
    and even philosophy, it was almost universally defamed as utopia.

### Utopia

    The relegation of real possibilities to the no-man's land of utopia is itself
    an essential element of the ideology of the performance principle. If the
    construction of a nonrepressive instinctual development is oriented, not on the
    subhistorical past, but on the historical present and mature civilization, the
    very notion of utopia loses its meaning. The negation of the performance
    principle emerges not against but with  the progress of conscious rationality;
    it presupposes the highest maturity of civilization. The very achievements of
    the performance principle have intensified the discrepancy between the archaic
    unconscious and conscious processes of man, on the one hand, and his actual
    potentialities, on the other. The history of mankind seems to tend toward
    another turning point in the vicissitudes of the instincts. And, just as at the
    preceding turning points, the adaptation of the archaic mental structure to the
    new environment would mean another "castrophe" -- an explosive change in the
    environment itself. However, while the first turning point was, according to
    the Freudian hypothesis, an event in geological history, and while the second
    occurred at the beginning of civilization, the third turning point would be
    located at the highest attained level of civilization. The actor in this event
    would be no longer the historical animal man but the conscious, rational
    subject that has mastered and appropriated the objective world as the arena of
    his realization. The historical factor contained in Freud' s theory of
    instincts has come to fruition in history when the basis of Ananke ( Lebensnot)
    -- which, for Freud, provided the rationale for the repressive reality
    principle -- is undermined by the progress of civilization.

    Still, there is some validity in the argument that, despite all progress,
    scarcity and immaturity remain great enough to prevent the realization of the
    principle "to each according to his needs." The material as well as mental
    resources of civilization are still so limited that there must be a vastly
    lower standard of living if social productivity were redirected toward the
    universal gratification of individual needs: many would have to give up
    manipulated comforts if all were to live a human life. Moreover, the prevailing
    international structure of industrial civilization seems to condemn such an
    idea to ridicule. This does not invalidate the theoretical insistence that the
    performance principle has become obsolescent. The reconciliation between
    pleasure and reality principle does not depend on the existence of abundance
    for all. The only pertinent question is whether a state of civilization can be
    reasonably envisaged in which human needs are fulfilled in such a manner and to
    such an extent that surplus-repression can be eliminated.

    Such a hypothetical state could be reasonably assumed at two points, which lie
    at the opposite poles of the vicissitudes of the instincts: one would be
    located at the primitive beginnings of history, the other at its most mature
    stage. The first would refer to a non-oppressive distribution of scarcity (as
    may, for example, have existed in matriarchal phases of ancient society). The
    second would pertain to a rational organization of fully developed industrial
    society after the conquest of scarcity. The vicissitudes of the instincts would
    of course be very different under these two conditions, but one decisive
    feature must be common to both: the instinctual development would be
    non-repressive in the sense that at least the surplus-repression necessitated
    by the interests of domination would not be imposed upon the instincts. This
    quality would reflect the prevalent satisfaction of the basic human needs (most
    primitive at the first, vastly extended and refined at the second stage),
    sexual as well as social: food, housing, clothing, leisure. This satisfaction
    would be (and this is the important point) without toil -- that is, without the
    rule of alienated labor over the human existence. Under primitive conditions,
    alienation has not yet  arisen because of the primitive character of the needs
    themselves, the rudimentary (personal or sexual) character of the division of
    labor, and the absence of an institutionalized hierarchical specialization of
    functions. Under the "ideal" conditions of mature industrial civilization,
    alienation would be completed by general automatization of labor, reduction of
    labor time to a minimum , and exchangeability of functions.  Since the length
    of the working day is itself one of the principal repressive factors imposed
    upon the pleasure principle by the reality principle, the reduction of the
    working day to a point where the mere quantum of labor time no longer arrests
    human development is the first prerequisite for freedom. Such reduction by
    itself would almost certainly mean a considerable decrease in the standard of
    living prevalent today in the most advanced industrial countries. But the
    regression to a lower standard of living, which the collapse of the performance
    principle would bring about, does not militate against progress in freedom.

    The argument that makes liberation conditional upon an ever higher standard of
    living all too easily serves to justify the perpetuation of repression. The
    definition of the standard of living in terms of automobiles , television sets,
    airplanes, and tractors is that of the performance principle itself. Beyond the
    rule of this principle, the level of living would be measured by other
    criteria: the universal gratification of the basic human needs, and the freedom
    from guilt and fear -- internalized as well as external, instinctual as well as
    rrational." "La vraie civilization. . n' est pas dans le gaz, ni dans la
    vapeur, ni dans les tables tournantes. Elle est dans la diminution des traces
    du pêché originel" 17 -- this is the definition of progress beyond the rule of
    the performance principle.

    Under optimum conditions, the prevalence, in mature civilization, of material
    and intellectual wealth would be such as to allow painless gratification of
    needs, while domination would no longer systematically forestall such
    gratification. In this case, the quantum of instinctual energy still to be
    diverted into necessary labor (in turn completely mechanized and rationalized)
    would be so small that a large area of repressive constraints and
    modifications, no longer sustained by external forces , would collapse.

### The Aesthetic Dimension

    Schiller's Letters on the Aesthetic Education of Man (1795), written largely
    under the impact of the Critique of Judgment, aim at a remaking of civilization
    by virtue of the liberating force of the aesthetic function: it is envisaged as
    containing the possibility of a new reality principle.

    [...]

    Since it was civilization itself which "dealt modern man this wound," only a
    new mode of civilization can heal it. The wound is caused by the antagonistic
    relation between the two polar dimensions of the human existence. Schiller
    describes this antagonism in a series of paired concepts: sensuousness and
    reason, matter and form (spirit), nature and freedom, the particular and the
    universal.

    Each of the two dimensions is governed by a basic impulse:  the "sensuous
    impulse " and the "form-impulse." 20 The former is essentially passive,
    receptive, the latter active, mastering, domineering . Culture is built by the
    combination and interaction of these two impulses. But in the established
    civilization, their relation has been an antagonistic one: instead of
    reconciling both impulses by making sensuousness rational and reason sensuous,
    civilization has subjugated sensuousness to reason in such a manner that the
    former, if it reasserts itself , does so in destructive and "savage" forms
    while the tyranny of reason impoverishes and barbarizes sensuousness. The
    conflict must be resolved if human potentialities are to realize themselves
    freely. Since only the impulses have the lasting force that fundamentally
    affects the human existence, such reconciliation between the two impulses must
    be the work of a third impulse. Schiller defines this third mediating impulse
    as the play impulse, its objective as beauty, and its goal as freedom.

    [...]

    The quest is for the solution of a "political" problem : the liberation of man
    from inhuman existential conditions. Schiller states that, in order to solve
    the political problem, "one must pass through the aesthetic, since it is beauty
    that leads to freedom." The play impulse is the vehicle of this liberation. The
    impulse does not aim at playing "with" something ; rather it is the play of
    life itself, beyond want and external compulsion -- the manifestation of an
    existence without fear and anxiety, and thus the manifestation of freedom
    itself.

    Man is free only where he is free from constraint, external and internal,
    physical and moral -- when he is constrained neither by law nor by need. 21 But
    such constraint is  the reality. Freedom is thus, in a strict sense, freedom
    from the established reality: man is free when the "reality loses its
    seriousness" and when its necessity "becomes light" ( leicht). 22 "The greatest
    stupidity and the greatest intelligence have a certain affinity with each other
    in that they both seek only the real"; however, such need for and attachment to
    the real are "merely the results of want."

    In contrast, "indifference to reality" and interest in "show" (dis-play,
    Schein) are the tokens of freedom from want and a "true enlargement of
    humanity." 23 In a genuinely humane civilization, the human existence will be
    play rather than toil, and man will live in display rather than need.

    These ideas represent one of the most advanced positions of thought. It must be
    understood that the liberation from the reality which is here envisaged is not
    transcendental, "inner," or merely intellectual freedom (as Schiller explicitly
    emphasizes 24 ) but freedom in the reality. The reality that "loses its
    seriousness" is the inhumane reality of want and need, and it loses its
    seriousness when wants and needs can be satisfied without alienated labor.
    Then, man is free to "play" with his faculties and potentialities and with
    those of nature, and only by "playing" with them is he free. His world is then
    display ( Schein), and its order is that of beauty.

    Because it is the realization of freedom, play is more  than the constraining
    physical and moral reality: ". . man is only serious  with the agreeable, the
    good, the perfect; but with beauty he plays." 25 Such formulations would be
    irresponsible "aestheticism" if the realm of play were one of ornament, luxury,
    holiday, in an otherwise repressive world. But here the aesthetic function is
    conceived as a principle governing the entire human existence, and it can do so
    only if it becomes "universal."

    [...]

    If we reassemble its main elements, we find:

    (1) The transformation of toil (labor) into play, and of repressive
    productivity into "display" -- a transformation that must be preceded by the
    conquest of want (scarcity) as the determining factor of civilization. 43

    (2) The self-sublimation of sensuousness (of the sensuous impulse) and the
    de-sublimation of reason (of the form-impulse) in order to reconcile the two
    basic antagonistic impulses.

    (3) The conquest of time in so far as time is destructive of lasting
    gratification.

    These elements are practically identical with those of a reconciliation between
    pleasure principle and reality principle. We recall the constitutive role
    attributed to imagination (phantasy) in play and display: Imagination preserves
    the objectives of those mental processes which have remained free from the
    repressive reality principle; in their aesthetic function, they can be
    incorporated into the conscious rationality of mature civilization. The play
    impulse stands for the common denominator of the two opposed mental processes
    and principles.

    [...]

    Non-repressive order is essentially an order of abundance:  the necessary
    constraint is brought about by "superfluity" rather than need. Only an order of
    abundance is compatible with freedom. At this point, the idealistic and the
    materialistic critiques of culture meet. Both agree that nonrepressive order
    becomes possible only at the highest maturity of civilization, when all basic
    needs can be satisfied with a minimum expenditure of physical and mental energy
    in a minimum of time.

    [...]

    Possession and procurement of the necessities of life are the prerequisite,
    rather than the content, of a free society. The realm of necessity, of labor,
    is one of unfreedom because the human existence in this realm is determined by
    objectives and functions that are not its own and that do not allow the free
    play of human faculties and desires.
    The optimum in this realm is therefore to be defined by standards of
    rationality rather than freedom -- namely, to organize production and
    distribution in such a manner that the least time is spent for making all
    necessities available to all members of society. Necessary labor is a system of
    essentially inhuman, mechanical, and routine activities; in such a system,
    individuality cannot be a value and end in itself. Reasonably, the system of
    societal labor would be organized rather with a view to saving time and space
    for the development of individuality outside  the inevitably repressive
    work-world. Play and display, as principles of civilization, imply not the
    transformation of labor but its complete subordination to the freely evolving
    potentialities of man and nature.

## Regression into progress

    The processes that create the ego and superego also shape and perpetuate
    specific societal institutions and relations. Such psychoanalytical concepts as
    sublimation, identification, and introjection have not only a psychical but
    also a social content: they terminate in a system of institutions, laws,
    agencies, things, and customs that confront the individual as objective
    entities. Within this antagonistic system, the mental conflict between ego and
    superego, between ego and id, is at one and the same time a conflict between
    the individual and his society.

    [...]

    Therefore, the emergence of a non-repressive reality principle involving
    instinctual liberation would regress  behind the attained level of civilized
    rationality. This regression would be psychical as well as social: it would
    reactivate early stages of the libido which were surpassed in the development
    of the reality ego, and it would dissolve the institutions of society in which
    the reality ego exists. In terms of these institutions, instinctual liberation
    is relapse into barbarism. However, occurring at the height of civilization, as
    a consequence not of defeat but of victory in the struggle for existence, and
    supported by a free society, such liberation might have very different results.
    It would still be a reversal of the process of civilization, a subversion of
    culture -- but after  culture had done its work and created the mankind and the
    world that could be free.

### Work, toil and play

    Freud's suggestions in Group Psychology and the Analysis of the Ego do more
    than reformulate his thesis of Eros as the builder of culture; culture here
    rather appears as the builder of Eros -- that is to say, as the "natural"
    fulfillment of the innermost trend of Eros. Freud's psychology of civilization
    was based on the inexorable conflict between Ananke and free instinctual
    development. But if Ananke itself becomes the primary field of libidinal
    development, the contradiction evaporates. Not only would the struggle for
    existence not necessarily cancel the possibility of instinctual freedom (as we
    suggested in Chapter 6); but it would even constitute a "prop" for instinctual
    gratificaiton. The work relations which form the base of civilization, and thus
    civilization itself, would be "propped" by non-desexualized instinctual energy.
    The whole concept of sublimation is at stake .

    The problem of work, of socially useful activity, without (repressive)
    sublimation can now be restated. It emerged as the problem of a change in the
    character of work by virtue of which the latter would be assimilated to play --
    the free play of human faculties. What are the instinctual preconditions for
    such a transformation? The most far -reaching attempt to answer this question
    is made by Barbara Lantos in her article "Work and the Instincts." 26 She
    defines work and play in terms of the instinctual stages involved in these
    activities. Play is entirely subject to the pleasure principle: pleasure is in
    the movement itself in so far as it activates erotogenic zones. "The
    fundamental feature of play is, that it is gratifying in itself, without
    serving any other purpose than that of instinctual gratification."

    [...]

    The genital organization of the sexual instincts has a parallel in the
    work-organization of the ego-instincts. 27

    Thus it is the purpose and not the content which marks an activity as play or
    work. 28 A transformation in the instinctual structure (such as that from the
    pregenital to the genital stage) would entail a change in the instinctual value
    of the human activity regardless of its content. For example, if work were
    accompanied by a reactivation of pregenital polymorphous eroticism, it would
    tend to become gratifying in itself without losing its work  content. Now it is
    precisely such a reactivation of polymorphous eroticism which appeared as the
    consequence of the conquest of scarcity and alienation. The altered societal
    conditions would therefore create an instinctual basis for the transformation
    of work into play. In Freud's terms , the less the efforts to obtain
    satisfaction are impeded and directed by the interest in domination, the more
    freely the libido could prop itself upon the satisfaction of the great vital
    needs.

    [...]

    But while the psychoanalytical and anthropological concepts of such an order
    have been oriented on the prehistorical and precivilized past, our discussion
    of the concept is oriented on the future, on the conditions of fully mature
    civilization. The transformation of sexuality into Eros, and its extension to
    lasting libidinal work relations, here presuppose the rational reorganization
    of a huge industrial apparatus, a highly specialized societal division of
    labor, the use of fantastically destructive energies, and the co-operation of
    vast masses.

    The idea of libidinal work relations in a developed industrial society finds
    little support in the tradition of thought, and where such support is
    forthcoming it seems of a dangerous nature. The transformation of labor into
    pleasure is the central idea in Fourier's giant socialist utopia.

    [...]

    Fourier insists that this transformation requires a complete change in the
    social institutions: distribution of the social product according to need,
    assignment of functions according to individual faculties and inclinations,
    constant mutation of functions, short work periods, and so on. But the
    possibility of "attractive labor" ( travail attrayant) derives above all from
    the release of libidinal forces . Fourier assumes the existence of an
    attraction indnstrielle  which makes for pleasurable co-operation. It is based
    on the attraction passionnée  in the nature of man , which persists despite the
    opposition of reason, duty, prejudice.

    [...]

    Fourier comes closer than any other utopian socialist to elucidating the
    dependence of freedom on non-repressive sublimation. However, in his detailed
    blueprint for the realization of this idea, he hands it over to a giant
    organization and administration and thus retains the repressive elements . The
    working communities of the phalanstère  anticipate "strength through joy"
    rather than freedom, the beautification of mass culture rather than its
    abolition. Work as free play cannot be subject to administration; only
    alienated labor can be organized and administered by rational routine. It is
    beyond this sphere, but on its basis, that non-repressive sublimation creates
    its own cultural order.

    [...]

    The necessity to work is a neurotic symptom. It is a crutch. It is an attempt
    to make oneself feel valuable even though there is no particular need for one'
    s working. 37

### Superid

    It has been pointed out that the superego, as the mental representative of
    morality, is not unambiguously the representative of the reality principle,
    especially of the forbidding and punishing father. In many cases, the superego
    seems to be in secret alliance with the id, defending the claims of the id
    against the ego and the external world. Charles Odier therefore proposed that a
    part of the superego is "in the last analysis the representative of a primitive
    phase, during which morality had not yet freed itself from the pleasure
    principle." [superid]

    [...]

    The psychical phenomenon which, in the individual, suggests such a pregenital
    morality is an identification with the mother, expressing itself in a
    castration-wish rather than castration-threat. It might be the survival of a
    regressive tendency: remembrance of the primal Mother-Right, and at the same
    time a "symbolic means against losing the then prevailing privileges of the
    woman." According to Odier, the pregenital and prehistorical morality of the
    superid is incompatible with the reality principle and therefore a neurotic
    factor .

### Time, memory and death

    The flux of time is society' s most natural ally in maintaining law and order,
    conformity, and the institutions that relegate freedom to a perpetual utopia;
    the flux of time helps men to forget what was and what can be: it makes them
    oblivious to the better past and the better future.

    This ability to forget -- itself the result of a long and terrible education by
    experience -- is an indispensable requirement of mental and physical hygiene
    without which civilized life would be unbearable; but it is also the mental
    faculty which sustains submissiveness and renunciation. To forget is also to
    forgive what should not be forgiven if justice and freedom are to prevail. Such
    forgiveness reproduces the conditions which reproduce injustice and
    enslavement: to forget past suffering is to forgive the forces that caused it
    --without defeating these forces . The wounds that heal in time are also the
    wounds that contain the poison. Against this surrender to time, the restoration
    of remembrance to its rights, as a vehicle of liberation, is one of the noblest
    tasks of thought.

    [...]

    Nietzsche saw in the training of memory the beginning of civilized morality --
    especially the memory of obligations, contracts, dues. 10 This context reveals
    the one-sidedness of memory-training in civilization: the faculty was chiefly
    directed toward remembering duties rather than pleasures; memory was linked
    with bad conscience, guilt, and sin. Unhappiness and the threat of punishment ,
    not happiness and the promise of freedom, linger in memory.

    [...]

    Still, this defeat of time is artistic and spurious; remembrance is no real
    weapon unless it is translated into historical action. Then, the struggle
    against time becomes a decisive moment in the struggle against domination: The
    conscious wish to break the continuum of history belongs to the revolutionary
    classes in the moment of action. This consciousness asserted itself during the
    July Revolution. In the evening of the first day of the struggle,
    simultaneously but independently at several places, shots were fired at the
    time pieces on the towers of Paris. 11

    It is the alliance between time and the order of repression that motivates the
    efforts to halt the flux of time, and it is this alliance that makes time the
    deadly enemy of Eros.

    [...]

    Every sound reason is on the side of law and order in their insistence that the
    eternity of joy be reserved for the hereafter, and in their endeavor to
    subordinate the struggle against death and disease to the never-ceasing
    requirements of national and international security.

    The striving for the preservation of time in time, for the arrest of time, for
    conquest of death, seems unreasonable by any standard, and outright impossible
    under the hypothesis of the death instinct that we have accepted. Or does this
    very hypothesis make it more reasonable? The death instinct operates under the
    Nirvana principle: it tends toward that state of "constant gratification" where
    no tension is felt -- a state without want. This trend of the instinct implies
    that its destructive  manifestations would be minimized as it approached such a
    state. If the instinct's basic objective is not the termination of life but
    of pain -- the absence of tension -- then paradoxically, in terms of the
    instinct, the conflict between life and death is the more reduced, the closer
    life approximates the state of gratification. Pleasure principle and Nirvana
    principle then converge.

    [...]

    Death would cease to be an instinctual goal. It remains a fact, perhaps even an
    ultimate necessity -- but a necessity against which the unrepressed energy of
    mankind will protest, against which it will wage its greatest struggle.  In
    this struggle, reason and instinct could unite. Under conditions of a truly
    human existence, the difference between succumbing to disease at the age of
    ten, thirty, fifty, or seventy, and dying a "natural" death after a fulfilled
    life, may well be a difference worth fighting for with all instinctual energy.
    Not those who die, but those who die before they must and want to die, those
    who die in agony and pain, are the great indictment against civilization.

    They also testify to the unredeemable guilt of mankind. Their death arouses the
    painful awareness that it was unnecessary, that it could be otherwise. It takes
    all the institutions and values of a repressive order to pacify the bad
    conscience of this guilt. Once again, the deep connection between the death
    instinct and the sense of guilt becomes apparent. The silent "professional
    agreement" with the fact of death and disease is perhaps one of the most
    widespread expressions of the death instinct -- or, rather, of its social
    usefulness. In a repressive civilization, death itself becomes an instrument of
    repression. Whether death is feared as constant threat, or glorified as supreme
    sacrifice, or accepted as fate, the education for consent to death introduces
    an element of surrender into life from the beginning -- surrender and
    submission.

### Psychoanalytic Therapy and Theory

    Fromm has devoted an admirable paper to "The Social Conditions of
    Psychoanalytic Therapy," in which he shows that the psychoanalytic situation
    (between analyst and patient) is a specific expression of liberalist toleration
    and as such dependent on the existence of such toleration in the society. But
    behind the tolerant attitude of the "neutral" analyst is concealed "respect for
    the social taboos of the bourgeoisie."
    7 Fromm traces the effectiveness of these taboos at the very core of Freudian
    theory, in Freud' s position toward sexual morality. With this attitude, Fromm
    contrasts another conception of therapy, first perhaps formulated by Ferenczi,
    according to which the analyst rejects patricentric-authoritarian taboos and
    enters into a positive rather than neutral relation with the patient. The new
    conception is characterized chiefly by an "unconditional affirmation of the
    patient' s claim for happiness" and the "liberation of morality from its
    tabooistic features ." 8

    [...]

    in a repressive society, individual happiness and productive development are in
    contradiction to society; if they are defined as values to be realized within
    this society, they become themselves repressive.

    [...]

    while psychoanalytic theory recognizes that the sickness of the individual is
    ultimately caused and sustained by the sickness of his civilization,
    psychoanalytic therapy aims at curing the individual so that he can continue to
    function as part of a sick civilization without surrendering to it altogether.

    [...]

    Theoretically, the difference between mental health and neurosis lies only in
    the degree and effectiveness of resignation: mental health is successful,
    efficient resignation -- normally so efficient that it shows forth as
    moderately happy satisfaction. Normality is a precarious condition. "Neurosis
    and psychosis are both of them an expression of the rebellion of the id against
    the outer world, of its ` pain,' unwillingness to adapt itself to necessity --
    to ananke, or, if one prefers, of its incapacity to do so." 9

    [...]

    In the long run, the question is only how much resignation the individual can
    bear without breaking up. In this sense, therapy is a course in resignation: a
    great deal will be gained if we succeed in "transforming your hysterical misery
    into everyday unhappiness," which is the usual lot of mankind. 11

    [...]

    The autonomous personality, in the sense of creative "uniqueness" and fullness
    of its existence, has always been the privilege of a very few. At the present
    stage, the personality tends toward a standardized reaction pattern established
    by the hierarchy of power and functions and by its technical, intellectual, and
    cultural apparatus. 

    The analyst and his patient share this alienation, and since it does not
    usually manifest itself in any neurotic symptom but rather as the hallmark of
    "mental health," it does not appear in the revisionist consciousness.

    [...]

    Fromm writes: Genuine love is rooted in productiveness and may properly be
    called, therefore, "productive love." Its essence is the same whether it is the
    mother's love for the child, our love for man , or the erotic love between two
    individuals. . certain basic elements may be said to be characteristic of all
    forms of productive love. These are care, responsibility, respect, and
    knowledge. 35

    Compare with this ideological formulation Freud' s analysis of the instinctual
    ground and underground of love, of the long and painful process in which
    sexuality with all its polymorphous perversity is tamed and inhibited until it
    ultimately becomes susceptible to fusion with tenderness and affection -- a
    fusion which remains precarious and never quite overcomes its destructive
    elements .

    [...]

    According to Freud, love, in our culture, can and must be practiced as
    "aim-inhibited sexuality," with all the taboos and constraints placed upon it
    by a monogamic-patriarchal society. Beyond its legitimate manifestations, love
    is destruetive and by no means conducive to productiveness and constructive
    work. Love, taken seriously, is outlawed: "There is no longer any place in
    present-day civilized life for a simple natural love between two human beings,"
    37 But to the revisionists, productiveness, love, happiness, and health merge
    in grand hannony; civilization has not caused any conflicts between them which
    the mature person could not solve without serious damage .

    [...]

    Freud had established a substantive link between human freedom and happiness on
    the one hand and sexuality on the other: the latter provided the primary source
    for the former and at the same time the ground for their necessary restriction
    in civilization. The revisionist solution of the conflict through the
    spiritualization of freedom and happiness demanded the weakening of this link .

    [...]

    Fromm 's ideological interpretation of the Oedipus complex implies acceptance
    of the unhappiness of freedom, of its separation from satisfaction; Freud' s
    theory implies that the Oedipus wish is the eternal infantile protest  against
    this separation -- protest not against freedom but against painful , repressive
    freedom. Conversely, the Oedipus wish is the eternal infantile desire for the
    archetype of freedom: freedom from want. And since the (unrepressed) sex
    instinct is the biological carrier of this archetype of freedom, the Oedipus
    wish is essentially "sexual craving." Its natural object is, not simply the
    mother qua  mother, but the mother qua  woman -- female principle of
    gratification. Here the Eros of receptivity, rest, painless and integral
    satisfaction is nearest to the death instinct (return to the womb), the
    pleasure principle nearest to the Nirvana principle. Eros here fights its first
    battle against everything the reality principle stands for: against the father,
    against domination, sublimation, resignation. Gradually then, freedom and
    fulfillment are being associated with these paternal principles; freedom from
    want is sacrificed to moral and spiritual independence. It is first the "sexual
    craving" for the mother-woman that threatens the psychical basis of
    civilization; it is the "sexual craving" that makes the Oedipus conflict the
    prototype of the instinctual conflicts between the individual and his society.
    If the Oedipus wish were in essence nothing more than the wish for protection
    and security ("escape from freedom"), if the child desired only impermissible
    security and not impermissible pleasure, then the Oedipus complex would indeed
    present an essentially educational problem. As such, it can be treated without
    exposing the instinctual danger zones of society.

### Misc

    But, again, Freud shows that this repressive system does not really solve the
    conflict. Civilization plunges into a destructive dialectic: the perpetual
    restrictions on Eros ultimately weaken the life instincts and thus strengthen
    and release the very forces against which they were "called up" -- those of
    destruction.

    [...]

    For the vast majority of the population, the scope and mode of satisfaction are
    determined by their own labor; but their labor is work for an apparatus which
    they do not control, which operates as an independent power to which
    individuals must submit if they want to live. And it becomes the more alien the
    more specialized the division of labor becomes. Men do not live their own lives
    but perform pre-established functions. While they work, they do not fulfill
    their own needs and faculties but work in alienation. Work has now become
    general, and so have the restrictions placed upon the libido: labor time, which
    is the largest part of the individual' s life time, is painful time, for
    alienated labor is absence of gratification, negation of the pleasure
    principle. Libido is diverted for socially useful performances in which the
    individual works for himself only in so far as he works for the apparatus,
    engaged in activities that mostly do not coincide with his own faculties and
    desires.

    [...]

    The work of repression pertains to the death instinct as well as the life
    instinct. Normally, their fusion is a healthy one, but the sustained severity
    of the superego constantly threatens this healthy balance. "The more a man
    checks his aggressive tendencies toward others the more tyrannical, that is
    aggressive, he becomes in his ego-ideal.. the more intense become the
    aggressive tendencies of his ego-ideal against his ego." 57 Driven to the
    extreme, in melancholia, "a pure culture of the death instinct" may hold sway
    in the superego

    [...]

    It is in this context that Freud's metapsychology comes face to face with the
    fatal dialectic of civilization: the very progress of civilization leads to the
    release of increasingly destructive forces. In order to elucidate the
    connection between Freud's individual psychology and the theory of
    civilization, it will be necessary to resume the interpretation of the
    instinctual dynamic at a different level -- namely, the phylogenetic one.

    [...]

    Note: 45 To be sure, every form of society, every civilization has to exact
    labor time for the procurement of the necessities and luxuries of life. But not
    every kind and mode of labor is essentially irreconcilable with the pleasure
    principle. The human relations connected with work may "provide for a very
    considerable discharge of libidinal component impulses, narcissistic,
    aggressive, and even erotic." ( Civilization and Its Discontents, p. 34 note.)
    The irreconcilable conflict is not between work (reality principle) and Eros
    (pleasure principle), but between alienated  labor (performance principle) and
    Eros. The notion of non-alienated, libidinal work will be discussed below.

    [...]

    It is the end result of long historical processes which are congealed in the
    network of human and institutional entities making up society, and these
    processes define the personality and its relationships. Consequently, to
    understand them for what they really are, psychology must unfreeze  them by
    tracing their hidden origins. In doing so, psychology discovers that the
    determining childhood experiences are linked with the experiences of the
    species -- that the individual lives the universal fate of mankind. The past
    defines the present because mankind has not yet mastered its own history.

    [...]

    The basic work in civilization is non-libidinal, is labor; labor is
    "unpleasantness," and such unpleasantness has to be enforced.

    [...]

    To be sure, there is a mode of work which offers a high degree of libidinal
    satisfaction, which is pleasurable in its execution. And artistic work, where
    it is genuine, seems to grow out of a non-repressive instinctual constellation
    and to envisage non-repressive aims -- so much so that the term sublimation
    seems to require considerable modification if applied to this kind of work.

    [...]

    The "automatization" of the superego 25 indicates the defense mechanisms by
    which society meets the threat. The defense consists chiefly in a strengthening
    of controls not so much over the instincts as over consciousness, which, if
    left free, might recognize the work of repression in the bigger and better
    satisfaction of needs. The manipulation of consciousness which has occurred
    throughout the orbit of contemporary industrial civilization has been described
    in the various interpretations of totalitarian and "popular cultures":
    co-ordination of the private and public existence, of spontaneous and required
    reactions. The promotion of thoughtless leisure activities, the triumph of
    anti- intellectual ideologies, exemplify the trend.

    [...]

    But these personal father-images have gradually disappeared behind the
    institutions. With the rationalization of the productive apparatus, with the
    multiplication of functions, all domination assumes the form of administration.
    At its peak, the concentration of economic power seems to turn into anonymity:
    everyone, even at the very top, appears to be powerless before the movements
    and laws of the apparatus itself. Control is normally administered by offices
    in which the controlled are the employers and the employed.

    [...]

    Most of the clichés with which sociology describes the process of
    dehumanization in presentday mass culture are correct; but they seem to be
    slanted in the wrong direction. What is retrogressive is not mechanization and
    standardization but their containment, not the universal co-ordination but its
    concealment under spurious liberties, choices, and individualities. The high
    standard of living in the domain of the great corporations is restrictive  in a
    concrete sociological sense: the goods and services that the individuals buy
    control their needs and petrify their faculties. In exchange for the
    commodities that enrich their life, the individuals sell not only their labor
    but also their free time. The better living is offset by the all-pervasive
    control over living. People dwell in apartment concentrations -- and have
    private automobiles with which they can no longer escape into a different
    world. They have huge refrigerators filled with frozen foods. They have dozens
    of newspapers and magazines that espouse the same ideals. They have innumerable
    choices, innumerable gadgets which are all of the same sort and keep them
    occupied and divert their attention from the real issue -- which is the
    awareness that they could both work less and determine their own needs and
    satisfactions.

    The ideology of today lies in that production and consumption reproduce and
    justify domination. But their ideological character does not change the fact
    that their benefits are real. The repressiveness of the whole lies to a high
    degree in its efficacy: it enhances the scope of material culture, facilitates
    the procurement of the necessities of life, makes comfort and luxury cheaper,
    draws ever-larger areas into the orbit of industry -- while at the same time
    sustaining toil and destruction. The individual pays by sacrificing his time,
    his consciousness, his dreams; civilization pays by sacrificing its own
    promises of liberty, justice, and peace for all.

    The discrepancy between potential liberation and actual repression has come to
    maturity: it permeates all spheres of life the world over. The rationality of
    progress heightens the irrationality of its organization and direction.
    Social cohesion and administrative power are sufficiently strong to protect the
    whole from direct aggression, but not strong enough to eliminate the
    accumulated aggressiveness. It turns against those who do not belong to the
    whole, whose existence is its denial. This foe appears as the archenemy and
    Antichrist himself : he is everywhere at all times ; he represents hidden and
    sinister forces, and his omnipresence requires total mobilization.

    [...]

    Being is essentially the striving for pleasure. This striving becomes an "aim"
    in the human existence: the erotic impulse to combine living substance into
    ever larger and more durable units is the instinctual source of civilization.
    The sex instincts are life  instincts: the impulse to preserve and enrich life
    by mastering nature in accordance with the developing vital needs is originally
    an erotic impulse.
    Ananke is experienced as the barrier against the satisfaction of the life
    instincts, which seek pleasure, not security. And the "struggle for existence"
    is originally a struggle for pleasure: culture begins with the collective
    implementation of this aim. Later, however, the struggle for existence is
    organized in the interest of domination: the erotic basis of culture is
    transformed. When philosophy conceives the essence of being as Logos, it is
    already the Logos of domination -- commanding, mastering, directing reason, to
    which man and nature are to be subjected Freud' s interpretation of being in
    terms of Eros recaptures the early stage of Plato's philosophy, which conceived
    of culture not as the repressive sublimation but as the free
    self-development of Eros. As early as Plato, this conception appears as an
    archaic-mythical residue. Eros is being absorbed into Logos, and Logos is
    reason which subdues the instincts.
    The history of ontology reflects the reality principle which governs the world
    ever more exclusively: The insights contained in the metaphysical notion of
    Eros were driven underground. They survived, in eschatological distortion, in
    many heretic movements, in the hedonistic philosophy. Their history has still
    to be written -- as has the history of the transformation of Eros in Agape. 29
    Freud's own theory follows the general trend: in his work, the rationality of
    the predominant reality principle supersedes the metaphysical speculations on
    Eros.

    [...]

    As an isolated individual phenomenon , the reactivation of narcissistic libido
    is not culture-building but neurotic: The difference between a neurosis and a
    sublimation is evidently the social aspect of the phenomenon . A neurosis
    isolates; a sublimation unites.
