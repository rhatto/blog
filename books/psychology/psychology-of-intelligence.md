[[!meta title="The Psychology of Intelligence"]]

* Author: Jean Piaget.
* Publisher: Routledge Classics.
* Year: 1950. 

## References

* [Piaget's theory of cognitive development - Wikipedia](https://en.wikipedia.org/wiki/Piaget's_theory_of_cognitive_development).

## Overview

This overview is a mixed of both ideas from the book altogether with other
considerations I've got by reading other, related material:

### Intelligence is reversible!

As what's really wonderful about this reversibility is that it's built atop of
lower, fundamental levels of irreversible dynamical systems.

That revesibility is the capacity to the adaptive system do turn away from
configurations that doesn't lead to a defined goal and replace by other
pathways, mixing introspection and empirism.

Reading this book along with The Tree of Live from Maturana and Varella
and Morin's Method I get the feeling that intelligence in life arises from
the sensori-motor system and gets deeper in a process where the nervous
system inflates to give way to impulses/stimuli that originates from itself.

Consequential to this reversibility is that intelligence might experimentation
freely without risking itself producing damages or permanent harm to itself,
which is different to say that somebody can't harm him/herself by the consequence
of his/her acts.

Also, while what happens with intelligence looks entirely reversible, mind is
not composed of intelligence alone. Other instances exist that might put the
whole apparatus on restricted modes of operation, such when in a neurosis which
is a state of constant looping in a given theme.

## Misc

* Perception (imediate contact with the world) (127).

* Habit: beyond short and rapidly automatised connections between per-
  ceptions and responses (habit) (127).

* How the whole body is seem according to his theory? There's a movement (sic)
  where intelligence raises from the sensori-motor to the mind, but can we
  consider the other way as well, about what's conceived by abstract thought
  be then used as a source of sensori-motor intelligence? I guess so, but wonder
  how that could be articulated in Piaget's theory.

## Intelligence and equilibrium

    Then, if intelligence is thus conceived as the form of equilibrium towards
    which all cognitive processes tend, there arises the problem of its relations
    with perception (Chap. 3), and with habit (Chap. 4).

    -- Preface

    Every response, whether it be an act directed towards the outside world or an
    act internalized as thought, takes the form of an adaptation or, better, of a
    re-adaptation. The individual acts only if he experiences a need, i.e., if the
    equilibrium between the environment and the organism is momentarily upset, and
    action tends to re-establish the equilibrium, i.e., to re-adapt the organ- ism
    (Claparède). A response is thus a particular case of inter- action between the
    external world and the subject, but unlike physiological interactions, which
    are of a material nature and involve an internal change in the bodies which are
    present, the responses studied by psychology are of a functional nature and are
    achieved at greater and greater distances in space (percep- tion, etc.) and in
    time (memory, etc.) besides following more and more complex paths (reversals,
    detours, etc.). Behaviour, thus conceived in terms of functional interaction,
    presupposes two essential and closely interdependent aspects: an aﬀective
    aspect and a cognitive aspect.

    -- 5

    Furthermore, intelligence itself does not consist of an isolated and sharply
    diﬀerentiated class of cognitive processes. It is not, properly speaking, one
    form of structuring among others; it is the form of equilibrium towards which
    all the structures arising out of perception, habit and elementary
    sensori-motor mechan- isms tend. It must be understood that if intelligence is
    not a faculty this denial involves a radical functional continuity between the
    higher forms of thought and the whole mass of lower types of cognitive and
    motor adaptation; so intelligence can only be the form of equilibrium towards
    which these tend.

    This does not mean, of course, that a judgment consists of a co- ordination of
    perceptual structures, or that perceiving means unconscious inference (although
    both these theories have been held), for functional continuity in no way
    excludes diversity or even heterogeneity among structures. Every structure is
    to be thought of as a particular form of equilibrium, more or less stable
    within its restricted ﬁeld and losing its stability on reach- ing the limits of
    the ﬁeld. But these structures, forming diﬀerent levels, are to be regarded as
    succeeding one another according to a law of development, such that each one
    brings about a more inclusive and stable equilibrium for the processes that
    emerge from the preceding level. Intelligence is thus only a generic term to
    indicate the superior forms of organization or equilibrium of cognitive
    structurings.

    -- 7

    In general, we may thus conclude that there is an essential unity between the
    sensori-motor processes that engender per- ceptual activity, the formation of
    habits, and pre-verbal or pre- representative intelligence itself. The latter
    does not therefore arise as a new power, superimposed all of a sudden on com-
    pletely prepared previous mechanisms, but is only the expres- sion of these
    same mechanisms when they go beyond present and immediate contact with the
    world (perception), as well as beyond short and rapidly automatised connections
    between per- ceptions and responses (habit), and operate at progressively
    greater distances and by more complex routes, in the direction of mobility and
    reversibility. Early intelligence, therefore, is simply the form of mobile
    equilibrium towards which the mechanisms adapted to perception and habit tend;
    but the latter attain this only by leaving their respective ﬁelds of
    application.  Moreover, intelligence, from this ﬁrst sensori-motor stage
    onwards, has already succeeded in constructing, in the special case of space,
    the equilibrated structure that we call the group of displacements—in an
    entirely empirical or practical form, it is true, and of course remaining on
    the very restricted plane of immediate space. But it goes without saying that
    this organiza- tion, circumscribed as it is by the limitations of action, still
    does not constitute a form of thought. On the contrary, the whole development
    of thought, from the advent of language to the end of childhood, is necessary
    in order that the completed sensori- motor structures, which may even be
    co-ordinated in the form of empirical groups, may be extended into genuine
    operations, which will constitute or reconstruct these groupings and groups at
    the level of symbolic behaviour and reﬂective reasoning.

    -- 127-128

## Logic and psychology

    An axiomatics is an exclusively hypothetico-deductive sci-
    ence, i.e., it reduces to a minimum appeals to experience (it even
    aims to eliminate them entirely) in order freely to reconstruct its
    object by means of undemonstrable propositions (axioms),
    which are to be combined as rigorously as possible and in every
    possible way. In this way geometry has made great progress,
    seeking to liberate itself from all intuition and constructing the
    most diverse spaces simply by deﬁning the primary elements to
    be admitted by hypothesis and the operations to which they are
    subject. The axiomatic method is thus the mathematical method
    par excellence and it has had numerous applications, not only in
    pure mathematics, but in various ﬁelds of applied mathematics
    (from theoretical physics to mathematical economics). The use-
    fulness of an axiomatics, in fact, goes beyond that of demonstra-
    tion (although in this ﬁeld it constitutes the only rigorous
    method); in the face of complex realities, resisting exhaustive
    analysis, it permits us to construct simpliﬁed models of reality
    and thus provides the study of the latter with irreplaceable dis-
    secting instruments. To sum up, an axiomatics constitutes a “pat-
    tern” for reality, as F. Gonseth has clearly shown, and, since all
    abstraction leads to a schematization, the axiomatic method in
    the long run extends the scope of intelligence itself.

    But precisely because of its “schematic” character, an axiomat-
    ics cannot claim to be the basis of, and still less to replace, its
    corresponding experimental science, i.e. the science relating to
    that sector of reality for which the axiomatics forms the pattern.
    Thus, axiomatic geometry is incapable of teaching us what the
    space of the real world is like (and “pure economics” in no way
    exhausts the complexity of concrete economic facts). No axi-
    omatics could replace the inductive science which corresponds
    to it, for the essential reason that its own purity is merely a limit
    which is never completely attained. As Gonseth also says, there
    always remains an intuitive residue in the most puriﬁed pattern
    (just as there is already an element of schematization in all intu-
    ition). This reason alone is enough to show why an axiomatics
    will never be the basis of an experimental science and why there
    is an experimental science corresponding to every axiomatics
    (and, no doubt, vice versa).

    -- page 30

    It is true that in addition to the individual consistency of
    actions there enter into thought interactions of a collective order
    and consequently “norms” imposed by this collaboration. But
    co-operation is only a system of actions, or of operations, car-
    ried out in concert, and we may repeat the preceding argument
    for collective symbolic behaviour, which likewise remains at a
    level containing real structures, unlike axiomatizations of a
    formal nature.

    For psychology, therefore, there remains unaltered the prob-
    lem of understanding the mechanism with which intelligence
    comes to construct coherent structures capable of operational
    combination; and it is no use invoking “principles” which this
    intelligence is supposed to apply spontaneously, since logical
    principles concern the theoretical pattern formulated after
    thought has been constructed and not this living process of con-
    struction itself. Brunschvicg has made the profound observation
    that intelligence wins battles or indulges, like poetry, in a con-
    tinuous work of creation, while logico-mathematical deduction
    is comparable only to treatises on strategy and to manuals of
    “poetic art”, which codify the past victories of action or mind
    but do not ensure their future conquests. 1

    -- page 34

## Habit and sensori-motor intelligence

Circular reaction:

    Let us imagine an infant in a cradle with a raised cover from which
    hang a whole series of rattles and a loose string. The child grasps
    this and so shakes the whole arrangement without expecting to do
    so or understanding any of the detailed spatial or causal rela-
    tions. Surprised by the result, he reaches for the string and
    carries out the whole sequence several times over. J. M. Baldwin
    called this active reproduction of a result at ﬁrst obtained by
    chance a “circular reaction”. The circular reaction is thus a typ-
    ical example of reproductive assimilation. The ﬁrst movement
    executed and followed by its result constitutes a complete action,
    which creates a new need once the objects to which it relates
    have returned to their initial stage; these are then assimilated to
    the previous action (thereby promoted to the status of a schema)
    which stimulates its reproduction, and so on. Now this mechan-
    ism is identical with that which is already present at the source
    of elementary habits except that, in their case, the circular reac-
    tion aﬀects the body itself (so we will give the name “primary
    circular reaction” to that of the early level, such as the schema of
    thumb-sucking), whereas thenceforward, thanks to prehension,
    it is applied to external objects (we will call this behaviour aﬀect-
    ing objects the “secondary circular reaction,” although we must
    remember that these are not yet by any means conceived as
    substances by the child).

    -- 110-112

Early intelligence:

    The routes between the subject and the object fol-
    lowed by action, and also by sensori-motor reconstitutions and
    anticipations, are no longer direct and simple pathways as at the
    previous stages: rectilinear as in perception, or stereotyped and
    uni-directional as in circular reactions. The routes begin to vary
    and the utilisation of earlier schemata begins to extend further in
    time. This is characteristic of the connection between means and
    ends, which henceforth are diﬀerentiated, and this is why we
    may begin to speak of true intelligence. But, apart from the
    continuity that links it with earlier behaviour, we should note the
    limitations of this early intelligence: there are no inventions or
    discoveries of new means, but simply application of known
    means to unforeseen circumstances.

    -- 114

Innovation:

    Two acquisitions characterise the next stage, both relating to
    the utilisation of past experience. The assimilatory schemata so
    far described are of course continually accommodated to
    external data. But this accommodation is, so to speak, suﬀered
    rather than sought; the subject acts according to his needs and
    this action either harmonizes with reality or encounters resist-
    ances which it tries to overcome. Innovations which arise for-
    tuitously are either neglected or else assimilated to previous
    schemata and reproduced by circular reaction. However, a time
    comes when the innovation has an interest of its own, and this
    certainly implies a suﬃcient stock of schemata for comparisons
    to be possible and for the new fact to be suﬃciently like the
    known one to be interesting and suﬃciently diﬀerent to avoid
    satiation. Circular reaction, then, will consist of a reproduction
    of the new phenomenon, but with variations and active
    experimentation that are intended precisely to extract from it its
    new possibilities.

    -- 114

Topology:

    But there now arises a problem whose discussion leads to the study of space.
    Perceptual constancy is the product of simple regulations and we saw (Chap. 3)
    that the absence at all ages of absolute constancy and the existence of adult
    “superconstancy” provide evidence for the regulative rather than operational
    char- acter of the system. There is, therefore, all the more reason why it
    should be true of the ﬁrst two years. Does not the construction of space, on
    the other hand, lead quite rapidly to a grouping structure and even a group
    structure in accordance with
    
    Poincaré’s famous hypothesis concerning the psychologically primary inﬂuence of
    the “group of displacements?” The genesis of space in sensori-motor
    intelligence is com- pletely dominated by the progressive organisation of
    responses, and this in eﬀect leads to a “group” structure. But, contrary to
    Poincaré’s belief in the a priori nature of the group of dis- placements, this
    is developed gradually as the ultimate form of equilibrium reached by this
    motor organisation. Successive co-ordinations (combinativity), reversals
    (reversibility), detours (associativity) and conservations of position
    (identity) gradually give rise to the group, which serves as a necessary
    equilibrium for actions.

    At the ﬁrst two stages (reﬂexes and elementary habits), we could not even speak
    of a space common to the various per- ceptual modalities, since there are as
    many spaces, all mutually heterogeneous, as there are qualitatively distinct
    ﬁelds (mouth, visual, tactile, etc.). It is only in the course of the third
    stage that the mutual assimilation of these various spaces becomes system- atic
    owing to the co-ordination of vision with prehension. Now, step by step with
    these co-ordinations, we see growing up elementary spatial systems which
    already presage the form of composition characteristic of the group. Thus, in
    the case of interrupted circular reaction, the subject returns to the starting-
    point to begin again; when his eyes are following a moving object that is
    travelling too fast for continuous vision (falling etc.), the subject
    occasionally catches up with the object by dis- placements of his own body to
    correct for those of the external moving object.

    But it is as well to realise that, if we take the point of view of the subject
    and not merely that of a mathematical observer, the construction of a group
    structure implies at least two conditions: the concept of an object and the
    decentralisation of movements by correcting for, and even reversing, their
    initial egocentricity.  In fact, it is clear that the reversibility
    characteristic of the group presupposes the concept of an object, and also vice
    versa, since to retrieve an object is to make it possible for oneself to return
    (by displacing either the object itself or one’s own body). The object is
    simply the constant due to the reversible composition of the group.
    Furthermore, as Poincaré himself has clearly shown, the idea of displacement as
    such implies the possibility of diﬀerentiating between irreversible changes of
    state and those changes of position that are characterized precisely by their
    reversibility (or by their possible correction through movements of one’s own
    body). It is obvious, therefore, that without con- servation of objects there
    could not be any “group”, since then everything would appear as a “change of
    state”. The object and the group of displacements are thus indissociable, the
    one con- stituting the static aspect and the other the dynamic aspect of the
    same reality. But this is not all: a world with no objects is a universe with
    no systematic diﬀerentiation between subjective and external realities, a world
    that is consequently “adualistic” (J. M. Baldwin). By this very fact, such a
    universe would be centred on one’s own actions, the subject being all the more
    dominated by this egocentric point of view because he remains
    un-self-conscious. But the group implies just the opposite attitude: a complete
    decentralisation, such that one’s own body is located as one element among
    others in a system of displacements enabling one to distinguish between one’s
    own movements and those of objects.

    -- 123-125
