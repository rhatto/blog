[[!meta title="IBM and the Holocaust"]]

"See everything with Hollerith punchcards":

[[!img dehomag.png link="no"]]

## Contents

[[!toc startlevel=2 levels=4]]

## About

* [IBM and the Holocaust](http://www.ibmandtheholocaust.com/).
* Author: [Edwin Black](http://edwinblack.com/).

It's worth read on it's entirety.

## Impressum

Impressions not to be held in punch cards.

So we have this huge corporation, an empire built around monopolist practices
and information technology. It's pure capitalistic in the sense that it's not
bound any specific foreign government political affiliations providing that
it buys that information machinery.

Watson's micromanagment style of "the most infinitesimal details" (page 241)
is symmetric with IBM's own technologies of control. Shape and being shapen
by a technology, as a mutual reflection with infinesimal consequences
as multiple mirror-images.

Was Watson before NCR -- and hence before IBM -- a mere seller? An the experience
with Patterson's salles manual what changed everything in Watson's mind?

    Patterson had created a sales manual designed to rigidly standardize all
    pitches and practices, and even mold the thought processes of selling. No
    deviation was allowed.

    -- 39

Watson sounds like the Steve Jobs equivalent at that era of
techno-totalitarianism.

Similarly to that inclination to control and domination, a government like nazi
Germany was an _automatic_  customer/partner that exponentiated all
potentialities for _efficiency_ -- in the limited, rationalized as an
unidimensional sense of efficiency.  Note that I'm not using _natural_ to
denote, as nature is just the automatic qualities of something.

Total control freaks meet at the dawn of large-scale information technology --
as we cannot say that informational practices did not exist before.

A technology that was designed to operate no matter whats the nature of the
"business". Be it commodities, manufacturing, people or war-making management.
War-time or logistic-time. Does not matter.

The joint venture of IBM and the Nazis created International Business-As-Usual
with Machines of hateful domination.

Even with the noise in the relation as when Watson broke with Hitler, some
"unstoppable force" of automation was there to stay and groe -- in the sense
that it was already being summoned and the force to stop it would be
tremendous.

The unusual of war was converted to the usual of business. No matter is war is
being waged, the corporate-form now was immune to it using a complex set-up os
nominated trustees, plausible deniability and levels of indirection. It can
"dissolve" itself in parts split inside beligerant nations and regroup after
the war -- keeping activities mostly unaffected and the profit guaranteed.
That is a even higher level of transnationality. It survives beyond localized
humours of mankind.

THINK must be put in perspective. Not only in the ink in the printed punch card.
Not only as a corporation as a Think Tank and efectivelly an acting tank.

A technology based on the operation of counting and sorting limits thinking to
only those two operations. In fact counting enables arithmetic and sorting
stablish the decision-making needed by proper computing, putting the whole
thought inside a box. Further restriction of thought is installed by allowing
it just for the purpose of profit: how to better exploit resources? By selling
that junk massivelly, this type of machinic "phylum" spread like cancer and
gangraned many brains. Copy is memory; punch card destruction is amnesia.
War is peace. Freedom is slavery. The Big Brother, or Big Blue, was an
information/disinformation machine.

Punch cards: holes punched in holes distributed in a plane-section. How that
confines or enables thought?

The nazi war machine was also an information machine, with an important
vulnerability of being too dependent in foreign technology. Hollerith himself
was a German descendent. Was that machinery only possible with this combination
of "traits" (page 31)? Germanic war-and-blood ideology with american capitalist
pragmatism?

Nazism was not only land and blood, but had also a strict and extreme dose of
ratiolaism. Not only megalomania, but also extreme obsession.

Impressum ironically punched on a ThinkPad.

## An image comparison

At [IBM Schoolhouse and Engineering Laboratory Building](https://www-03.ibm.com/ibm/history/history/year_1933.html)
entrance one could read the "Five Steps to Knowledge" [carved _at the
footsteps_](http://www-03.ibm.com/ibm/history/ibm100/us/en/icons/think_culture/transform/)
(THINK / OBSERVE / DISCUSS / LISTEN / READ):

![5 steps](https://www-03.ibm.com/ibm/history/history/images/5steps_to_knowledge.jpg)

![THINK](http://www-03.ibm.com/ibm/history/ibm100/images/icp/Y812281R29443C55/us__en_us__ibm100__cult_think__five_steps__620x350.jpg)

While, at Auschwitz, it was written "Work sets you free" in the entrance gate, above people's head:

![Auschwitz Gate](auschwitz.jpg)

One-dimensional Rationalization as a monotonic misconception of the thought process used for mass extermination.
Slave work, death by starvation which would set extermination camp inmates free from work and from data processing.

A strange opposition of what is written in the ground -- for the head look something from above and at the same time
leaving the head low while the THINK-good stays above -- and what's written above to be seeing from below, diminished.

That Auschwitz photo also has an iconic "HALT" sign at the entry blockade, which is evocative about the
last destination of an information processing in the extermination complex.

## Workflow

The International Holocaust Machines operated through the following stages:

* Census/identification: initial data aquisition on population, assets and commodities, even livestock.
* Confiscation: seized goods, assets, etc.
* Ghettoization and Deportation, through:
  * Sorting punch-card data to pinpoint residency location of undesirables to
    subsequenttly kidnap them.
  * Efficient management of railway using Holleriths to dispatch undesirables.
* Concentration and Extermination, by using punch-card technology to manage how each person would
  die and where it will take place, as well as management of slave work.
* Internal management of the punch card business, which would include inventory
  tracking and spoil recovering after the war.

Besides the well known relation between death and money-making during wars,
that was a Death Factory: if life could be stated as a long "detour to death", a
Death Factory is exactly it's opposite: and acceleration instead of a delay,
the acumen of the industrial process at the massive scale.

## Ideas

Somebody ought to sort out the data -- not using punch cards! -- presenting in
the book: production inputs, outputs and what's known about profits, royalties
and tax avoidance; how money was transfered and invested. Or maybe somebody
already did that? Lot's interesting stuff might be discovered by doing a
quantitative analysis.

It also might be important to search through patent offices for Hollerith
applications.

And creation of organograms and relational charts/maps.

## Questions

How Holleriths were made? Which were manual and with were automaded procedures?
Was an assembly lines and time-controlled manufacturing processes involved?
Does Holleriths were involved in management of it's own production?

## Excerpts

### Hollerith

Machine characteristics:

* Closed, pattented design.
* Commercialized only through leasing.
* Compatible cards between Hollerith machines, "no other machine that might ever be produced" (how?).

Hollerith characteristics:

    Just nineteen years old, Hollerith moved to Washington, D.C., to join
    the Census bureau. Over dinner one night at the posh Potomac Boat Club,
    Director of Vital Statistics, John Billings, quipped to Hollerith, "There ought
    to be a machine for doing the purely mechanical work of tabulating popula-
    tion and similar statistics." Inventive Hollerith began to think about a solu-
    tion. French looms, simple music boxes, and player pianos used punched
    holes on rolls or cards to automate rote activity. About a year later, Hollerith
    was struck with his idea. He saw a train conductor punch tickets in a special
    pattern to record physical characteristics such as height, hair color, size of
    nose, and clothing—a sort of "punched photograph." Other conductors
    could read the code and then catch anyone re-using the ticket of the original
    passenger. 5

    Hollerith's idea was a card with standardized holes, each representing a
    different trait: gender, nationality, occupation, and so forth. The card would
    then be fed into a "reader." By virtue of easily adjustable spring mechanisms
    and brief electrical brush contacts sensing for the holes, the cards could be
    "read" as they raced through a mechanical feeder. The processed cards could
    then be sorted into stacks based on a specified series of punched holes. 6

    Millions of cards could be sorted and resorted. Any desired trait
    could be isolated—general or specific—by simply sorting and resorting for
    data-specific holes. The machines could render the portrait of an entire
    population—or could pick out any group within that population. Indeed, one
    man could be identified from among millions if enough holes could be
    punched into a card and sorted enough times. Every punch card would
    become an informational storehouse limited only by the number of holes. It
    was nothing less than a nineteenth-century bar code for human beings. 7

    -- 31

    Since the Census Bureau only needed most of the tabulators once every
    decade, and because the defensive inventor always suspected some electri-
    cian or mechanic would steal his design, Hollerith decided that the systems
    would be leased by the government, not purchased. This important decision
    to lease machines, not sell them, would dominate all major IBM business
    transactions for the next century. Washington paid Hollerith about $750,000
    to rent his machines for the project. Now the inventor's challenge was to find

    -- 32

    Italy, England, France, Austria, and Germany all submitted orders. Hollerith's
    new technology was vi r t ual l y unrivaled. His machines made advanced census
    taking possible everywhere in the world. He and he alone would control the
    technology because the punchers, sorters, and tabulators were all designed
    to be compatible with each other—and with no other machine that might
    ever be produced. 12

    [...]

    Other than his inventions, Hollerith was said to cherish three things: his
    German heritage, his privacy, and his cat Bismarck. His link to everything
    German was obvious to all around him.

    [...]

    For privacy, Hollerith built a tall fence around his home to keep out
    neighbors and their pets. When too many cats scaled the top to jump into the
    yard, the ever-inventive Hollerith strung electrical wire along the fence, con-
    nected it to a battery, and then perched at his window puffing on a cigar.
    When a neighbor cat would appear threatening Bismarck's privacy, Hollerith
    would depress a switch, sending an electrical jolt into the animal. 16
    Hollerith's first major overseas census was organized for the brutal
    regime of Czar Nicholas II to launch the first-ever census of an estimated
    120 million Russians. Nicholas was anxious to import Hollerith technology.

    -- 34

### IBM merger

The Computing-Tabulating-Recording Company, or CTR:

    The four lackluster firms Flint selected defied any apparent rationale
    for merger. International Time Recording Company manufactured time
    clocks to record worker hours. Computing Scale Company sold simple retail
    scales with pricing charts attached as well as a line of meat and cheese slicers.
    Bundy Manufacturing produced small key-actuated time clocks, but, more
    importantly, it owned prime real estate in Endicott, New York. Of the four,
    Hollerith's Tabulating Machine Company was simply the largest and most
    dominant member of the group. 32

    Moreover, Flint wanted CTR's helm to be captained by a businessman, not a
    technocrat. For that, he chose one of America's up and coming business
    scoundrels, Thomas J. Watson.

    -- 37

### Watson, "Paternalistic and authoritarian"

    Watson was a conqueror. From simple merchandise inauspiciously sold
    to farmers and townsfolk in rural west-central New York, Watson would go
    on to command a global company consumed not with mere customers, but
    with territories, nations, and entire populations. He would identify corporate
    enemies to overcome and strategies to deploy. Like any conqueror, he would
    vanquish all in his way, and then demand the spoils. Salesmanship under
    Watson would elevate from one man's personal elixir to a veritable cult of
    commercial conquest. By virtue of his extraordinary skills, Watson would be
    delivered from his humble beginnings as a late-nineteenth-century horse-
    and-buggy back road peddler, to corporate scoundrel, to legendary tycoon,
    to international statesman, and finally to regal American icon—all in less
    than four decades.

    -- 38

    Watson began the systematic annihilation of Hallwood, its sales, and its
    customer base. Tactics included lurking near the Hallwood office to spy on its
    salesmen and customers. Watson would report the prospective clients so
    "intimidation squads" could pounce. The squads would threaten the prospect with
    tall tales of patent infringement suits by NCR against Hallwood, falsely
    claiming such suits would eventually include anyone who purchased Hallwood
    machines. The frightened customer would then be offered an NCR machine at a
    discount. 43

    -- 40

    Patterson planted him in New York City, handed him a million-dollar budget,
    and asked him to create a fake business called Watson's Cash Register and
    Second Hand Exchange. His mission was to join the community of second-
    hand dealers, learn their business, set up shop nearby, dramatically undersell,
    quietly steal their accounts, intimidate their customers, and otherwise disrupt
    their viability. Watson's fake company never needed to make a profit—only
    spend money to decimate unsuspecting dealers of used registers. Eventually,
    they would either be driven out of business or sell out to Watson with a dra-
    conian non-compete clause. Funneled money from NCR was used for opera-
    tions since Watson had no capital of his own. 46

    -- 41

    NCR salesmen wore dark suits, the corporation innovated a One Hun-
    dred Point Club for agents who met their quota, and The Cash stressed "clean
    living" as a virtue for commercial success. One day during a pep rally to the
    troops, Watson scrawled the word THINK on a piece of paper. Patterson saw
    the note and ordered THINK signs distributed throughout the company.
    Watson embraced many of Patterson's regimenting techniques as indispens-
    able doctrine for good sales. What he learned at NCR would stay with him
    forever. 53

    [...]

    Patterson, Watson, and several dozen other Cash executives were indicted for
    criminal conspiracy to restrain trade and construct a monopoly.

    [...]

    A year later, in 1913, all defendants were found guilty by an Ohio jury.
    Damning evidence, supplied by Watson colleagues and even Watson's own
    signed letters of instructions, were irrefutable. Most of the men, including
    Watson, received a one-year jail sentence. Many of the convicted wept and
    asked for leniency. But not Watson. He declared that he was proud of what
    he had accomplished. 55

    -- 42

    Then came the floods.

    [...]

    The Cash pounced. NCR organized an immense emergency relief effort.

    [...]

    Patterson, Watson, and the other NCR men became national heroes over-

    [...]

    Petitions were sent to President Woodrow Wilson asking for a pardon.
    Considering public sentiment, prosecutors offered consent decrees in lieu of
    jail time. Most of the defendants eagerly signed. Watson, however, refused,
    maintaining he saw nothing wrong in his conduct. Eventually, Watson's attorneys
    successfully overturned the conviction on a technicality. The government
    declined to re-prosecute. 58 But then the unpredictable and maniacal Patterson
    rewarded Watson's

    -- 42-43

    Patterson had demanded starched white shirts and dark suits at NCR. Watson
    insisted CTR employees dress in an identical uniform. And Watson borrowed his
    own NCR innovation, the term THINK, which at CTR was impressed onto as many
    surfaces as could be found, from the wall above Watson's desk to the bottom of
    company stationery. These Patterson cum Watson touches were easy to implement
    since several key Watson aides were old cronies from the NCR scandal days. 66

    -- 45

A "father image":

    Watson embodied more than the boss. He was the Leader. He even had a song.
    Clad in their uniforms of dark blue suits and glistening white shirts, the
    inspirited sales warriors of CTR would sing:

        Mister Watson is the man we're working for,
        He's the Leader of the C-T-R,
        He's the fairest, squarest man we know;
        Sincere and true.
        He has shown us how to play the game.
        And how to make the dough. 70

    -- 46

    "IBM is more than a business—it is a great worldwide institution that is going
    on forever." 74 More than ever. Watson f us e d himself into every facet of IBM's opera-
    tions, injecting his style into every decision, and mesmerizing the psyche of
    every employee. "IBM Spirit"—this was the term Watson ascribed to the all-
    encompassing, almost tribal devotion to company that he demanded.

    [...]

    Children began their indoctrination early, becoming eligible at age three for
    the kiddy rolls of the IBM Club, graduating to junior ranks at age eight. 76

    [...]

    Watson's own son, Tom, who inherited his father's throne at IBM,
    admitted, "The more I worked at IBM, the more I resented Dad for the cult-
    like atmosphere that surrounded him." 78

    [...]

    The ever- present equating of his name with the word THINK was more than an
    Orwellian exercise, it was a true-life indoctrination. The Watson mystique was
    never confined to the four walls of IBM. His aura was only magnified by

    -- 47

    Fortune referred to Watson as "the Leader," with a capital "L." So completely
    con- scious was Watson of his mythic quality that he eyed even the porters on
    trains and waiters in restaurants as potential legend busters. He tossed them
    big tips, often as much as $10, which was largesse for the day.

    [...]

    By giving liberally to charities and universities, by towering as a patron
    of the arts, by arranging scores of organizational memberships, honorary de-
    grees and awards, he further cultivated the man-myth for himself and IBM. 81
    Slogans were endlessly drilled into the extended IBM Family. We For-
    give Thoughtful Mistakes. There Is No Such Thing As Standing Still. Pack Up
    Your Troubles, Mr. Watson Is Here. 82
    And the songs. They began the very first day a man entered the IBM
    culture. They never ended during one's entire tenure. More than 100 songs
    were sung at various company functions. There were several for Watson,
    including the "IBM Anthem"

    [...]

    Revival-style meetings enthralled the men of IBM. Swaying as they
    chanted harmonies of adulation for the Leader, their palms brought together
    in fervent applause in hero worship, fully accepting that their families and
    destinies were intertwined with the family and destiny of the corporation,
    legions of company men incessandy re-dedicated themselves to the "Ever
    Onward" glory of IBM. All of it swirled around the irresistible magnetism,
    t h e i nt oxi cat i n g command, the charismatic cultic control of one man,
    Thomas J. Watson, the Leader. 84

    -- 48-49

### IBM and the Third Reich

    The question confronting all businessmen in 1933 was whether trading
    with Germany was worth either the economic risk or moral descent. This
    question faced Watson at IBM as well. But IBM was in a unique commercial
    position. While Watson and IBM were famous on the American business
    scene, the company's overseas operations were fundamentally below the
    public radar screen. IBM did not import German merchandise, it merely
    exported American technology. The IBM name did not even appear on any
    of thousands of index cards in the address files of leading New York boycott
    organizations. Moreover, the power of punch cards as an automation tool
    had not yet been commonly identified. So the risk that highly visible trading
    might provoke economic retaliation seemed low, especially since Dehomag
    did not even possess a name suggestive of IBM or Watson. 101
    On the other hand, the anticipated reward in Germany was great.

    Watson had learned early on that a government in reorganization, and
    indeed a government tighdy monitoring its society, was good news for IBM.
    During the Depression years, when the Franklin D. Roosevelt Administration
    created a massive bureaucracy to assist the public and control business, IBM
    doubled its size. The National Recovery Act of 1933, for example, meant
    "businesses all of a sudden had to supply the federal government with infor-
    mation in huge and unprecedented amounts," recalled an IBM official. Extra
    forms, export reports, more registrations, more statistics—IBM thrived on
    red tape. 102

    Nazi Germany offered Watson the opportunity to cater to government
    control, supervision, surveillance, and regimentation on a plane never before
    known in human history. The fact that Hitler planned to extend his Reich to
    other nations only magnified the prospective profits. In business terms, that
    was account growth. The technology was almost exclusively IBM's to purvey
    because the firm controlled about 90 percent of the world market in punch
    cards and sorters.

    -- 52

### Dehomag

    To be sure, Dehomag managers were as fervently devoted to the Nazi
    movement as any of Hitler's scientific soldiers. IBM NY understood this from
    the outset. Heidinger, a rabid Nazi, saw Dehomag's unique ability to imbue
    the Reich with population information as a virtual calling from God. His
    enraptured passion for Dehomag's sudden new role was typically expressed
    while opening a new IBM facility in Berlin. "I feel it almost a sacred action,"
    declared Heidinger emotionally, "I pray the blessing of heaven may rest
    upon this place." 118

    That day, while standing next to the personal representative of Watson
    and IBM, with numerous Nazi Party officials in attendance, Heidinger pub-
    licly announced how in tune he and Dehomag were with the Nazi race scien-
    tists who saw population statistics as the key to eradicating the unhealthy,
    inferior segments of society.

    "The physician examines the human body and determines whether . . .
    all organs are working to the benefit of the entire organism," asserted Hei-
    dinger to a crowd of Nazi officials. "We [Dehomag] are very much like the
    physician, in that we dissect, cell by cell, the German cultural body. We
    report every individual characteristic . . . on a little card. These are not dead
    cards, quite to the contrary, they prove later on that they come to life when
    the cards are sorted at a rate of 25,000 per hour according to certain charac-
    teristics. These characteristics are grouped like the organs of our cultural
    body, and they will be calculated and determined with the help of our tabu-
    lating machine. 119

    It was right about this time that Watson decided to engrave the five
    steps leading up to the door of the IBM School in Endicott, New York, with
    five of his favorite words. This school was the place where Watson would
    train his valued disciples in the art of sales, engineering, and technical sup-
    port. Those five uppermost steps, steps that each man ascended before enter-
    ing the front door, were engraved with the following words:

    READ
    LISTEN
    DISCUSS
    OBSERVE

    The fifth and uppermost step was chiseled with the heralded theme of
    the company. It said THINK. 122
    The word THINK was everywhere.

    -- 56-57

### The Census

The datacenter:

    IN MID - SEPTEMBER , 1933, 6,000 brown cardboard boxes began unceremo- niously
    arriving at the cavernous Alexanderplatz census complex in Berlin.  Each box
    was stuffed with questionnaires manually filled out by pen and pencil, but soon
    to be processed by an unprecedented automated praxis. As supervisors emptied
    their precious cargo at the Prussian Statistical Office, each questionnaire—one
    per household—was initialed by an intake clerk, stacked, and then transferred
    downstairs. "Downstairs" led to Dehomag's massive 22,000-square-foot hall, just
    one floor below, specifically rented for the project. 18

    Messengers shuttling stacks of questionnaires from the Statistical Office to
    Dehomag bounded down the right-hand side of an enclosed stairwell. As they
    descended the short flight, the sound of clicking became louder and louder. At
    the landing, they turned left and pushed through the doors. As the doors swung
    open, they encountered an immense high-ceilinged, hangar-like facility
    reverberating with the metallic music of Hollerith technology. Some 450 data
    punchers deployed in narrow rows of punching stations labored behind tall
    upright secretarial displays perfectly matched to the oversized
    census questionnaires. 19

    Turning left again, and then another right brought the messengers to a
    long windowed wall lined with narrow tables. The forms were piled there.
    From these first tables, the forms were methodically distributed to central-
    ized desks scattered throughout the work areas. The census forms were then
    loaded onto small trolleys and shutded again, this time to individual work
    stations, each equipped with a device that resembled a disjointed typewriter
    - actually an input engine. 20

    A continuous "Speed Punching" operation ran two shifts, and three
    when needed. Each shift spanned 7.5 hours with 60 minutes allotted for
    "fresh air breaks" and a company-provided meal. Day and night, Dehomag
    staffers entered the details on 41 million Prussians at a rate of 150 cards per
    hour. Allowing for holidays and a statistical prediction of absenteeism, yet
    ever obsessed with its four-month deadline, Dehomag decreed a quota of
    450,000 cards per day for its workforce. Free coffee was provided to keep
    people awake. A gymnast was brought in to demonstrate graceful aerobics
    and other techniques to relieve fatigue. Company officials bragged that the
    41 million processed cards, if stacked, would tower two and a half times
    higher than the Zugspitze, Germany's 10,000-foot mountain peak. Dehomag
    intended to reach the summit on time. 21

    As company officials looked down upon a floor plan of the layout, the
    linear rows and intersecting columns of work stations must have surely
    resembled a grandiose punch card itself animated into a three-dimensional
    bricks and mortar reality. Indeed, a company poster produced for the project
    showed throngs of miniscule people scrambling over a punch card sketch. 22
    The surreal artwork was more than symbolic.

    -- 63-64

And the description follows which show how was explicity the wish to target Jews.

Note for error-checking procedure and the "statistical prediction of
absenteeism" which imply on the [informate](/books/sociedade/age-of-the-smart-machine)
aspect of the procedure.

### Discretion and secrecy

    Watson developed an extraordinary ability to write reserved and clev-
    erly cautious letters. More commonly, he remained silent and let subordi-
    nates and managers do the writing for him. But they too respected an IBM
    code—unwritten, of course—to observe as much discretion as possible in
    memos and correspondence. This was especially so in the case of corre-
    sponding with or about Nazi Germany, the most controversial business part-
    ner of the day.

    -- 68

    Few understood the far-reaching ramifications of punch card technology and even
    fewer had a foreground understanding that the com- pany Dehomag was in fact
    essentially a wholly-owned subsidiary of Interna- tional Business Machines.

    Boycott and protest movements were ardently trying to crush Hitlerism by
    stopping Germany's exports. Although a network of Jewish and non- sectarian
    anti-Nazi leagues and bodies struggled to organize comprehensive lists of
    companies doing business with Germany, from importers of German toys and shoes
    to sellers of German porcelain and pharmaceuticals, yet IBM and Watson were not
    identified. Neither the company nor its president even appeared in any of
    thousands of hectic phone book entries or handwritten index card files of the
    leading national and regional boycott bodies. Anti- Nazi agitators just didn't
    understand the dynamics of corporate multi- nationalism. 64

    Moreover, IBM was not importing German merchandise, it was export-
    ing machinery. In fact, even exports dwindled as soon as the new plant in
    Berlin was erected, leaving less of a paper trail. So a measure of invisibility
    was assured in 1933.

    -- 75

### Fascism

    But to a certain extent all the worries about granting Hitler the techno-
    logic tools he needed were all subordinated to one irrepressible, ideological
    imperative. Hitler's plans for a new Fascist order with a "Greater Germany"
    dominating all Europe were not unacceptable to Watson. In fact, Watson
    admired the whole concept of Fascism. He hoped he could participate as the
    American capitalistic counterpart of the great Fascist wave sweeping the Con-
    tinent. Most of all, Fascism was good for business.

    THOMAS WATSOON and IBM had separately and jointly spent decades making
    money any way they could. Rules were broken. Conspiracies were hatched.
    Bloody wars became mere market opportunities. To a supranational, making
    money is equal parts commercial Darwinism, corporate ecclesiastics, dynastic
    chauvinism, and solipsistic greed.

    Watson was no Fascist. He was a pure capitalist. But the horseshoe of
    political economics finds little distance between extremities.

    [...]

    After all, his followers wore uniforms, sang songs, and were expected to
    display unquestioned loy- alty to the company he led.

    Fascism, the dictatorial state-controlled political system, was invented
    by Italian Dictator Benito Mussolini. The term symbolically derived from the
    Roman fasces, that is, the bundle of rods surrounding a ceremonial axe used
    during Roman times. Indeed, Nazi symbols and ritual were in large part
    adopted from Mussolini, including the palm-lifting Roman salute. Ironically,
    Italian Fascism was non-racial and not anti-Semitic. National Socialism added
    those defining elements.

    Mussolini fascinated Watson. Once, at a 1937 sales convention, Watson
    spoke out in Il Duce's defense. "I want to pay tribute ... [to the] great leader,
    Benito Mussolini," declared Watson. "I have followed the details of his work
    very carefully since he assumed leadership [in 1922]. Evidence of his leader-
    ship can be seen on all sides. . . . Mussolini is a pioneer . . . Italy is going to
    benefit gready." 65

    Watson explained his personal attraction to the dictator's style and even
    observed similarities with his own corporate, capitalistic model. "One thing
    which has greatly impressed me in connection with his leadership," con-
    ceded Watson, "is the loyalty displayed by the people. To have the loyalty and
    cooperation of everyone means progress—and ultimate success for a nation
    or an individual business ... we should pay tribute to Mussolini for estab-
    lishing this spirit of loyal support and cooperation." 66

    For years, an autographed picture of Mussolini graced the grand piano
    in Watson's living room. 67

    In defense of Fascism, Watson made clear, "Different countries require
    different forms of government and we should be careful not to let people in
    other countries feel that we are trying to standardize principles of govern-
    ment throughout the world." 68

    -- 75-76

What an irony: Watson defending non-standardization of goverments around the world...

    His access to Secretary of State Cordell Hull, and more importantly to
    President Franklin D. Roosevelt, was unparalleled. While the Hoover Justice
    Department was at the height of its anti-trust investigation of IBM in 1932,
    Watson donated large sums to the Roosevelt campaign. Roosevelt's election
    over Hoover was a landslide. Watson now had entree to the White House
    itself. 71

    -- 77

A statesman.

    So a happy medium was found between Watson's desire to maintain
    deniability in IBM's lucrative relations with Germany and his personal desire
    to hobnob with Third Reich VIPs. But, the demands of the growing business
    in Germany would not be free of Watson's famous micro-management. Too

    -- 79

### Technology for the "Final Sollution"

    IBM did not invent Germany's anti-Semitism, but when it volunteered solutions,
    the company virtually braided with Nazism. Like any technologic evolution, each
    new solution powered a new level of sinister expectation and cruel capability.

    When Germany wanted to identify the Jews by name, IBM showed them how. When
    Germany wanted to use that information to launch pro- grams of social expulsion
    and expropriation, IBM provided the technologic wherewithal. When the trains
    needed to run on time, from city to city or between concentration camps, IBM
    offered that solution as well. Ultimately, there was no solution IBM would not
    devise for a Reich willing to pay for services rendered. One solution led to
    another. No solution was out of the question.

    As the clock ticked, as the punch cards clicked, as Jews in Germany saw
    their existence vaporizing, others saw their corporate fortunes rise. Even as
    German Jewry hid in their homes and wept in despair, even as the world
    quietly trembled in fear, there was singing. Exhilarated, mesmerized, the
    faithful would sing, and sing loudly to their Leaders—on both sides of the
    Atlantic.

    Some uniforms were brown. Some were blue.

    -- 79-80

### Corporate schizophrenia

    To achieve his goals, each man had to cooperate in an international
    campaign of corporate schizophrenia designed to achieve maximum deniability
    for both Dehomag and IBM. The storyline depended upon the circumstance
    and the listener. Dehomag could be portrayed as the American-controlled, al-
    most wholly-owned subsidiary of IBM with token German shareholders and
    on-site German managers. Or Dehomag could be a loyal German, staunchly
    Aryan company baptized in the blood of Nazi ideology wielding the power
    of its American investment for the greater glory of Hitler's Reich.

    -- 83

### The rhetoric

    "The physician examines the human body and determines whether ...
    all organs are working to the benefit of the entire organism," asserted Hei-
    dinger to a crowd of company employees and Nazi officials. "We [Dehomag]
    are very much like the physician, in that we dissect, cell by cell, the German
    cultural body. We report every individual characteristic ... on a little card.
    These are not dead cards, quite to the contrary, they prove later on that they
    come to life when the cards are sorted at a rate of 25,000 per hour according
    to certain characteristics. These characteristics are grouped like the organs of
    our cultural body, and they will be calculated and determined with the help
    of our tabulating machine. 27

    "We are proud that we may assist in such a task, a task that provides our
    nation's Physician [Adolf Hitler] with the material he needs for his examina-
    tions. Our Physician can then determine whether the calculated values are in
    harmony with the health of our people. It also means that if such is not the
    case, our Physician can take corrective procedures to correct the sick circum-
    stances. . . . Our characteristics are deeply rooted in our race. Therefore, we
    must cherish them like a holy shrine, which we will—and must—keep pure.
    We have the deepest trust in our Physician and will follow his instructions in
    blind faith, because we know that he will lead our people to a great future.

    -- 88

### Automation and efficiency

    While Hitler's rhetoric was burning the parade grounds and airwaves,
    while Storm Troopers were marching Jews through the streets in ritual
    humiliations, while Reich legislative decrees and a miasma of regional and
    private policies were ousting Jews from their professions and residences,
    while noisy, outrageous acts of persecution were appalling the world, a qui-
    eter process was also underway. Germany was automating.
    Hollerith systems could do more than count. They could schedule, ana-
    lyze, and compute. They could manage.

    -- 92

    [...]

    Hitler's Germany began achieving undreamed of efficiencies.

    -- 94

### Now or then?

    People seated in a doctor's office or a welfare line never comprehended the
    destiny of routine information about their personal traits and conditions.
    Question 11 required a handwritten checkmark if the individual was a for-
    eigner. Later, this information was punched into the correlating punch card in
    columns 29-30 under nationality. 83

    -- 101

### Information as money, on paper

The discourse on purity was also present on technology itself, in the form
of punch cards produced according rigid specificiations using a paper devoid
of "impurities":

    WHEN HERMAN HOLLERITH designed his first punch card, he made it the
    size of a dollar bill. 94 For IBM, information was money. The more Germany
    calculated, tabulated, sorted, and analyzed, the greater the demand for
    machines. Equally important, once a machine was leased, it required vast
    quantities of punch cards. In many cases, a single tabulation required
    thousands of cards. Each card was designed to be used only once, and in a
    single operation. When Dehomag devised more in-depth data processing, the
    improvements only bolstered card demand. How many punch cards were needed?
    Millions - per week. 95

    Punch cards sped through the huffing machines of the Third Reich like tiny
    high-speed mechanized breaths rapidly inhaled and exhaled one time and one time
    only. But Hollerith systems were delicate, precision-engineering instruments
    that depended on a precision-engineered punch card manufac- tured to exacting
    specifications under ideal conditions. Because electrical current in the
    machines sensed the rectangular holes, even a microscopic imperfection would
    make the card inoperable and could foul up the en-

    So IBM production specifications were rigorous. Coniferous chemical
    pulp was milled, treated, and cured to create paper stock containing no
    more than 5 percent ash, and devoid of ground wood, calk fibers, process-
    ing chemicals, slime carbon, or other impurities that might conduct electric-
    ity and "therefore cause incorrect machine sensing." Residues, even in trace
    amounts, would accumulate on gears and other mechanisms, eventually
    causing jams and system shutdowns. Electrical testing to isolate defective
    sheets was mandatory. Paper, when cut, had to lie flat without curl or wrin-
    kle, and feature a hard, smooth finish on either side that yielded a "good
    snap or rattle." 96

    -- 103

There seems to be an equivalent discourse on purity and eugenics during the
development of the transistor. Something to check out.

    Only IBM could make and sell the unique punch cards for its machines.
    Indeed, punch cards were the precious currency of data processing. Depend-
    ing upon the market, IBM derived as much as a third of its profit from card
    sales. Overseas sales were even more of a profit center. Punch card profits
    were enough to justify years of federal anti-trust litigation designed to break
    the company's virtual monopoly on their sale and manufacture."
    When Herman Hollerith invented his technology at the close of the
    previous century, he understood the enduring commercial tactic of prolifer-
    ating a single universal system of hardware and ensuring that he alone pro-
    duced the sole compatible soft goods. Hollerith was right to size his card like
    the dollar. IBM's punch card monopoly was nothing less than a license to
    print money.

    -- 104

    Never before had so many people been identified so precisely, so silently, so
    quickly, and with such far-reaching consequences.  The dawn of the Information
    Age began at the sunset of human decency.

    -- 110

### 1933 census was just a rehearsal

    Top racial experts of the Interior Ministry flew in for the assignment. Working
    with drafts shuttled between Hitler's abode and police headquarters, twin
    decrees of disenfranchisement were finally patched together. The Law for the
    Protec- tion of German Blood and a companion decree entitled the Reich
    Citizenship Law deprived Jews of their German citizenship and now used the term
    explicitly—Jew, not non-Aryan. Moreover, Jews were proscribed from marry- ing
    or having sexual relations with any Aryan.

    [...]

    Laborious and protracted paper searches of individual genealogical
    records were possible. But each case could take months of intensive research.
    That wasn't fast enough for the Nazis. Hitler wanted the Jews identified en
    masse.

    [...]

    Once drafted, the Nuremberg regulations would be completely
    dependent upon Hollerith technology for the fast, wholesale tracing of Jew-
    ish family trees that the Reich demanded. Hollerith systems offered the
    Reich the speed and scope that only an automated system could to identify
    not only half and quarter Jews, but even eighth and sixteenth Jews. 14

    [...]

    Earlier in 1935, the Party's Race Political Office had estimated the total
    number of "race Jews." Thanks to Dehomag's people-counting methods, the
    Nazis believed that the 1933 census, which recorded a half million observant
    Jews, was now obsolete. Moreover, Nazis were convinced that the often-
    quoted total of some 600,000 Jews, which was closer to Germany's 1925
    census, was a mere irrelevance. In mid-June 1935, Dr. Leonardo Conti, a key
    Interior Ministry raceologist, declared 600,000 represented just the "practic-
    ing Jews." The true number of racial Jews in the Reich, he insisted, exceeded
    1.5 million. Conti, who would soon become the Ministry's State Secretary for
    Health overseeing most race questions, was a key assistant to the officials
    rishing to compose the Nuremberg Jewish laws for Hitler. 16

    -- 114-115

"Final sollution":

    Gesturing fanatically, he [Hitler] concluded with this warning: The new law "is
    an attempt at the legal regulation of a problem, which, if it fails, must be
    turned over to the Nazi Party for final solution." 22

    -- 116

### Mechanics

    Ironically, while all understood the evil anti-Jewish process underway,
    virtually none comprehended the technology that was making it possible,
    The mechanics were less than a mystery, they were transparent.
    In 1935, while the world shook at a rearmed Germany speeding toward

    [...]

    NAZI GERMANY was IBM's second most important customer after the U.S.

    [...]

    Business was good. Hitler needed Holleriths. Rigid dictatorial control
    over all aspects of commerce and social life mandated endless reporting and
    oversight.

    [...]

    IBM was guided by one precept: know your customer, anticipate their needs.

    -- 117

    [...]

    Dehomag could do the sorting in-house for a fee. The company bragged that
    it possessed the ability to cross-reference account numbers on bank deposits

    -- 119

    None of Germany's statistical programs came easy. All of them required
    on-going technical innovation. Every project required specific customized
    applications with Dehomag engineers carefully devising a column and corre-
    sponding hole to carry the intended information. Dummy cards were first
    carefully mocked-up in pen and pencil to make sure all categories and their
    placement were acceptable to both Dehomag and the reporting agency. [...]
    Dehomag was Germany's data maestro.

    -- 121

    New devices never stopped appearing. [...] Many of these devices were of course
    dual-purpose. They as routinely helped build Germany's general commercial,
    social, and military infrastructure as they helped a heightening tower of Nazi
    statistical offensives.  In Germany, some of the devices, such as the IBM
    Fingerprint Selecting Sorter, were only usable by Nazi security forces. 46

    -- 123

### What the alliance meant

    Rottke openly conceded the contract between IBM and Heidinger had
    "been made under an unlucky star, [and] appears to be the source of all
    evil." But he nonetheless warned Watson again that if Heidinger's shares
    were transferred to a foreign source Dehomag would probably not be per-
    mitted "the use of the word Deutsche (German) as an enterprise recognized
    in Germany as German." 126 That disaster had to be avoided at all costs. To
    IBM's doctrinaire German managers, including Heidinger, Dehomag repre-
    sented far more than just a profit-making enterprise. To them, Dehomag had
    the technologic ability to keep Germany's war machine automated, facilitate
    her highly efficient seizure of neighboring countries, and achieve the Reich's
    swiftly moving racial agenda. If IBM's subsidiary were deemed non-Aryan,
    the company would be barred from all the sensitive projects awaiting it.
    Hitler's Germany—in spite of itself—would be deprived of the Holleriths it
    so desperately required.

    From Watson's point of view, Germany was on the brink of unleash-
    ing its total conquest of Europe. IBM subsidiaries could be coordinated by

    Dehomag into one efficient continental enterprise, moving parts, cards, and
    machines as the Reich needed them. The new order that Hitler promised was
    made to order for IBM.

    In July 1939, Watson arrived in Berlin to personally mediate with Hei-
    dinger. A compromise would be necessary. The stakes were too high for the
    Nazis. The stakes were too high for capitalism. But it was the Germans who
    gave in, deferring on Heidinger's demands for a few months under term
    Watson dictated. "Watson now controlled something the Third Reich needed
    to launch the next decisive step in the solution of the Jewish question, not
    just in Germany—but all of Europe. Until now, the fastest punchers, tabula-
    tors, and sorters could organize only by numbers. The results could then be
    sorted by sequentially numbered profession, geographic locale, or popula-
    tion category. But now Watson had something new and powerful. 127
    He had the alphabetizes.

    -- 172-173

    In Copenhagen, at the ICC [International Chamber of Commerce] Congress,
    Watson's pro-Axis proposal exceeded anything the State Department could have
    expected. He champi- oned a resolution whereby private businessmen from the
    three Axis and three Allied nations would actually supercede their governments
    and negoti- ate a radical new international trade policy designed to satisfy
    Axis demands for raw materials coveted from other nations. The businessmen
    would then lobby their respective governments' official economic advisors to
    adopt their appeasement proposals for the sake of averting war. Ironically, the
    raw mate- rials were needed by Axis powers solely for the sake of waging war.

    On June 28, under Watson's leadership, the ICC passed a resolution again
    calling for "a fair distribution of raw materials, food stuffs and other
    products . . . [to] render unnecessary the movements of armies across fron-
    tiers." To this end, the ICC asked "the governments of France, Germany, Italy,
    Japan, the United Kingdom and the United States . . . each collaborate with
    their own leading businessmen . , . with respect to national needs . . . [and
    therefore] give all countries of the world a fair opportunity to share in the
    resources of the world." 27

    Even as Watson angled for Germany to be ceded more raw materials,
    Germany was openly raping invaded territories.

    [...]

    No wonder the German delegate to the ICC enthusiastically lauded
    Watson's proposal, which only sought to legitimize by private consultation
    what the Third Reich was undertaking by force. In his final speech of the
    Congress, Watson himself summed up the misery and devastation in the
    world as a mere "difference of opinion." His solution of businessmen confer-
    ring to divvy up other nations' resources to avoid further aggression was
    offered with these words: "We regret that there are unsatisfactory economic
    and political conditions in the world today, with a great difference of opinion
    existing among many countries. But differences of opinion, freely discussed
    and fairly disposed of, result in mutual benefit and increased happiness to all
    concerned." 31

    [...]

    One State Department assistant secretary could not help but comment on the
    similarity of Watson's suggestion to the Axis' own warlike demands. "This is,
    of course, a political question of major world importance," wrote the assistant
    secretary, and one upon which we have been hearing much from Germany, Italy and
    Japan. It occurs to me that it is most unfortunate that Mr. Thomas J. Watson,
    as an American serving as the president of the International Chamber of
    Commerce, should have sponsored a resolution of this character. It may well be
    that his resolution will return to plague us at some future date." That comment
    was written on October 5, 1939. 37 By then it was unnecessary to reply

    -- 181-184

### Biblical Census

    The Bible itself taught that unless specifically ordered by God, the census is
    evil because through it the enemy will know your strength:

        I Chronicles 21: Satan rose up against Israel and incited David to take a cen-
        sus of Israel. . . . This command was also evil in the sight of God. . . Then
        David said to God, "I have sinned greatly by doing this. Now I beg you to take
        away the guilt of your servant. I have done a very foolish thing." 78 On
        October 28, 1939, for the Jewish people of Warsaw, everything

    -- 195

### The Ghetto, The Train and the Print Shop

    Now the Reich knew exactly how many Jews were under their jurisdic-
    tion, how much nutrition to allocate—as low as 184 calories per person per
    day. They could consolidate Jews from the mixed districts of Warsaw, and
    bring in Jews from other nearby villages. The transports began arriving.
    White armbands with Jewish stars were distributed. Everyone, young or old,
    was required to wear one on the arm. Not the forearm, but the arm—visible,
    above the elbow. The Warsaw-Malkinia railway line ran right through the pro-
    posed ghetto. It was all according to Heydrich's September 21 Express Let-
    ter. Soon the demarcated ghetto would be surrounded by barbed wire.
    Eventually, a wall went up, sealing the residents of the ghetto from the outside
    world. Soon thereafter, the railway station would become the most feared lo-
    cation in the ghetto. 83

    The Nazi quantification and regimentation of Jewish demographics in
    Warsaw and indeed all of Poland was nothing less than spectacular—an al-
    most unbelievable feat. Savage conditions, secrecy, and lack of knowledge by
    the victims would forever obscure the details of exactly how the Nazis man-
    aged to tabulate the cross-referenced information on 360,000 souls within
    forty-eight hours.

    But this much is known: The Third Reich possessed only one method
    of tabulating censuses: Dehomag's Hollerith system. Moreover, IBM was in
    Poland, headquartered in Warsaw. In fact, the punch card print shop was just
    yards from the Warsaw Ghetto at Rymarska Street 6. That's where they pro-
    duced more than 20 million cards.

    -- 196

    The strategic alliance with Hitler continued to pay off in the cities and
    in the ghettos. But now IBM machines would demonstrate their special value
    along the railways and in the concentration camps of Europe. Soon the Jews
    would become Hollerith numbers.

    -- 203

### 'Blitzkrieg' efficiency

    HITLER'S ARMIES SWARMED OVER EUROPE THROUGHOUT the first months of 1940. The
    forces of the Reich slaughtered all opposition with a military machine
    unparalleled in human history. Blitzkrieg—lightning war—was more than a new
    word. Its very utterance signified coordinated death under the murderous
    onslaught of Hitler's massive air, sea, and 100,000-troop ground assaults.

    -- 204

    IBM had almost single-handedly brought modern warfare into the
    information age. Through its persistent, aggressive, unfaltering efforts, IBM
    virtually put the "blitz" in the krieg for Nazi Germany. Simply put, IBM orga-
    nized the organizers of Hitler's war.

    Keeping corporate distance in the face of the company's mounting
    involvement was now more imperative than ever. Although deniability was
    constructed with enough care to last for decades, the undeniable fact was
    that either IBM NY or its European headquarters in Geneva or its individual
    subsidiaries, depending upon the year and locale, maintained intimate
    knowledge of each and every application wielded by Nazis. This knowledge
    was inherendy revealed by an omnipresent paper trail: the cards themselves.
    IBM—and only IBM—printed all the cards. Billions of them.

    -- 213

### Even more discretion

    Only with great caution could Watson now publicly defend the Hitler
    agenda, even through euphemisms and code words. Most Americans would
    not tolerate anyone who even appeared to be a Nazi sympathizer or collabo-
    rator. So, as he had done since Kristallnacht in late 1938, Watson continued
    to insert corporate distance between himself and all involvement in the
    affairs of his subsidiaries in Nazi Europe—even as he micro-managed their
    day-to-day operations. More than ever, he now channeled his communica-
    tions to Nazi Europe through trusted intermediaries in Geneva and else-
    where on the Continent. He controlled subsidiary operations through
    attorneys and employees acting as nominee owners, following the pattern set
    in Czechoslovakia and Poland. 7

    [...]

    Peace was Watson's message.

    [...]

    Ironically, at that very moment, Watson and IBM were in fact Europe's most
    successful organizers not of peace, but of the ravages of war.

    -- 206-207

### Customized, proprietaty tech from a monopoly

How they knew how the card was user for, which would lead to ethical concerns
-- but the part of IBM -- and strategic ones -- by the part of the German government:

    IBM printed billions of its electrically sensitive cards each year for its
    European customers. But every order was different. Each set was meticu-
    lously designed not only for the singular client, but for the client's specific
    assignments. The design work was not a rote procedure, but an intense col-
    laboration. It began with a protracted investigation of the precise data needs
    of the project, as well as the people, items, or services being tabulated. This
    required IBM subsidiary "field engineers" to undertake invasive studies of
    the subject being measured, often on-site. Was it people? Was it cattle? Was it
    airplane engines? Was it pension payments? Was it slave labor? Different data
    gathering and card layouts were required for each type of application. 44

    [...]

    Once printed, each set of custom-designed punch cards bore its own
    distinctive look for its highly specialized purpose. Each set was printed with
    its own job-specific layout, with columns arrayed in custom-tailored configu-
    rations and then preprinted with unique column labels. Only IBM presses
    manufactured these cards, column by column, with the preprinted field topic:
    race, nationality, concentration camp, metal drums, combat wounds to leg,
    train departure vs. train arrival, type of horse, bank account, wages owed,
    property owed, physical racial features possessed—ad infinitum. 46

    Cards printed for one task could never be used for another. Factory pay-
    roll accounting cards, for example, could not be utilized by the SS in its on-
    going program of checking family backgrounds for racial features.

    [...]

    An IBM punch card could only be used once. After a period of months, the
    gargantuan stacks of processed cards were routinely destroyed. Billions more
    were needed each year by the Greater Reich and its Axis allies, requiring a
    sophisticated logisti- cal network of IBM authorized pulp mills, paper
    suppliers, and stock trans- port. Sales revenue for the lucrative supply of
    cards was continuously funneled to IBM via various modalities, including its
    Geneva nexus. 50 Slave labor cards were particularly complex on-going projects.
    The Reich was constandy changing map borders and Germanizing city and regional
    names. Its labor needs became more and more demanding. This type of punch card
    operation required numerous handwritten mock-ups and regular revisions. For
    example, MB Projects 3090 and 3091 tracking slave labor involved several
    mock-up cards, each clearly imprinted with Deho- mag's name along the edge.
    Written in hand on a typical sample was the pro- ject assignment: "work
    deployment of POWs and prisoners according to business branches." Toward the
    left, a column was hand-labeled "number of employed during the month" next to
    another column hand-marked "number of employed at month's end." The center and
    right-hand column headings were each scribbled in: French, Belgium, British,
    Yugoslavian, Polish. 51 Another card in the series was entitled "registration
    of male and female

    [...]

    The delicate machines, easily nudged out of whack by their con-
    stant syncopation, were serviced on-site, generally monthly, whether that site
    was in the registration center at Mauthausen concentration camp, the SS
    offices at Dachau, or the census bureau in any country. 54

    -- 214-217

### Business plan and practice

    Few in the financial community were sur- prised. IBM profits had been in a
    steep climb since the day Hitler came to power. 57 Clearly, the war was good to
    IBM coffers.  Indeed, in many ways the war seemed an ideal financial
    opportunity to Watson. Like many, he fully expected Germany to trample over all
    of Europe, creating a new economic order, one in which IBM would rule the data
    domain.  Like many, Watson expected that America would stay out of the war, and
    when it was over, businessmen like him would pick up the post-war economic
    pieces.  In fact, Watson began planning for the post-war boom and a complete

    "Our program," asserted Watson, "is for national committees in the individual
    countries to study their own problems from the standpoint of what they need
    from other countries and what they have to furnish other countries." It was the
    same Hitleresque message Wat- son had been preaching for years. Some countries,
    both men believed, were simply entided to the natural resources of another. War
    could be avoided by ceding these materials in advance. 58 No time was wasted in
    making plans.

    -- 217-218

But domestic pressue got too high in the US:

    The long delayed moment had come. That day, June 6, Watson wrote a
    reluctant letter to Adolf Hitler. This one would not be misaddressed or
    undelivered. This one would be sent by registered mail and released to the
    newspapers. Watson returned the medal Hitler had personally granted—and
    he chose to return it publicly via the media. The letter declared: "the present
    policies of your government are contrary to the causes for which I have been

    -- 222

    Dehomag was to become completely Nazified. The hierarchy had plans
    for Hollerith machines that stretched to virtually all the Reich's most urgent
    needs, from the conflict in Europe to Hitler's war against European Jewry.

    -- 227

But Germany was too dependent on IBM automation technologies. In fact dependency
on information technology was so high that equipment production could not supply
the demand. The automation process might have been exponential, beyond the
capacity of the system itself. Information was faster than physical, industrial
production:

    But the strategic alliance with IBM was too entrenched to simply switch off.
    Since the birth of the Third Reich, Germany had automated virtually its entire
    economy, as well as most government operations and Nazi Party activities, using
    a single technology: Hollerith. Elaborate data operations were in full swing
    every- where in Germany and its conquered lands. The country suddenly discov-
    ered its own vulnerable over-dependence on IBM machinery.

    [...]

    At the same time, Germany's war industry suffered from a chronic paper and pulp
    shortage due to a lack of supply and the diversion of basic pulping ingredients
    to war propellants.  Only four specialized paper plants in Germany could even
    produce Hollerith

    [...]

    Holleriths could not function without IBM's unique paper. Watson controlled the
    paper. 17 Printing cards was a stop-start process that under optimal conditions

    [...]

    Holleriths could not function without cards. Watson controlled the cards. 18
    Precision maintenance was needed monthly on the sensitive gears, tum-

    [...]

    Even working at peak capacity in tandem with recently opened IBM factories in
    Germany, Austria, Italy, and France, Nazi requests for sorters, tabulators, and
    collators were back-ordered twenty-four months. Hollerith systems could not
    function without machines or spare parts. Watson controlled the machines and
    the spare parts. 19

    Watson's monopoly could be replaced—but it would take years. Even
    if the Reich confiscated every IBM printing plant in Nazi-dominated Europe,
    and seized every machine, within months the cards and spare parts would
    run out. The whole data system would quickly grind to a halt. As it stood in
    summer 1941, the IBM enterprise in Nazi Germany was hardly a stand-
    alone operation; it depended upon the global financial, technical, and ma-
    terial support of IBM NY and i t s seventy worldwide subsidiaries. Watson
    controlled all of it.

    Without punch card technology, Nazi Germany would be completely
    incapable of even a fraction of the automation it had taken for granted,
    Returning to manual methods was unthinkable. The Race and Settlement

    -- 228-230

    If Watson allowed the Reich—in a fit of rage over the return of the medal—to
    oust IBM technologic supremacy in Nazi Germany, and if he allowed Berlin to
    embark upon its own ersatz punch card industry, Hitler's data automation
    program might speed toward self-destruction. No one could predict how
    drastically every Reich undertaking would be affected. But clearly, the blitz
    IBM attached to the German krieg would eventually be sub- tracted if not
    severely lessened. All Watson had to do was give up Dehomag as the Nazis
    demanded. If IBM did not have a technologic stranglehold over Germany, the
    Nazis would not be negotiating, they would simply seize what- ever they wanted.
    For Watson, it was a choice.

    [...]

    But Watson would not detach Dehomag from the global IBM empire.

    -- 235

    Albert empha- sized that in the very near future, "a minority of shares might
    be even materi- ally of higher value than the present majority." He added that
    the notion of stockholder "control" was actually becoming a passe notion in
    Germany since the Reich now direcdy or indirectly controlled virtually all
    business.  "A majority of shares," he wrote Watson, "does not mean as much as
    it used to . . . [since] a corporation, company, enterprise or plant
    manufacturing in Germany is so firmly, thoroughly and definitely subjected to
    the governmen- tal rules and regulations." 46

    -- 237

    For IBM, war would ironically be more advantageous than existing
    peace.

    Under the current state of affairs, IBM's assets were blocked in Ger-
    many until the conflict was over. Under an enemy custodian, those same
    marks would still be blocked—again until any war was over. As it stood, Hei-
    dinger was threatening daily to destroy Dehomag unless IBM sold or re-
    duced its ownership; and he was demanding to cash out his stock. But if war
    with the U.S. broke out, Heidinger and the other managers would be sum-
    marily relieved of their management authority since technically they repre-
    sented IBM NY. A government custodian chosen on the basis of keen
    business skills—and Albert might have the connections to select a reliable
    one—would be appointed to replace Heidinger and manage Dehomag. In
    fact, the Nazi receiver would diligendy manage all of IBM's European sub-
    sidiaries. The money would be waiting when the war was over. 56

    Plausible deniability would be real. Questions—would not be asked by IBM NY.
    Answers-would not be given by IBMers in Europe or Reich officials. 58

    -- 240-241

    [...]

    The company that lionized the word THINK now thought better of its
    guiding mandate.

    -- 241-242

    IBM should rely on its decided technologic edge, suggested Chauncey,
    because of the profound difficulty in starting a punch card industry from
    scratch, especially if New York could block French Bull competition. In spite
    of the quality of its devices, French Bull was a very small company with very
    few machines. Bull's one small factory could never supply the Reich's conti-
    nental needs. Ramping up for volume production—even if based within a
    Bull factory—would take months. Hitler didn't have months in his hour-to-
    hour struggle to dominate Europe. In a section entided "Length of Time for
    Competition to Come in Actuality," Chauncey argued, "Unless the authori-
    ties, or the new company, operate in the meantime from the French Bull fac-
    tory, it would appear that much time may elapse before such new company
    [could] ... furnish machines in Germany." 103

    -- 257

    It seemed that in spite of its autarkic impulses and collective rage
    against Watson, the cold fact remained: Nazi Germany needed punch cards.
    It needed them not next month or even next week. It needed them every
    hour of every day in every place. Only IBM could provide them.
    "My inclination is to fight," Chauncey declared straight out. But the
    battle would be difficult. He knew that IBM was fighting a two-front psycho-
    economic war: Heidinger's demand to cash in his stock, and Nazi Party
    demands to take over the subsidiary. Clearly, the two were organically linked,

    [...]

    As for IBM's fight with the Nazi Party, Chauncey reiterated his willing-
    ness to "make any representations to the authorities that our managers need
    not reveal any information of the activities of Dehomag's customers... . but I
    cannot get the actual persons out in the open." 107 That chance would now
    come. After weeks of remaining in the background, Dr. Edmund Veesen-
    mayer would finally come forward.

    -- 258

    IBM as a company would know the innermost details of Hitler's Holle-
    rith operations, designing the programs, printing the cards, and servicing the
    machines. But Watson and his New York directors could erect a wall of credi-
    ble deniability at the doors of the executive suite. In theory, only those down
    the hall in the New York headquarters who communicated direcdy with IBM
    Geneva, such as IBM European General Manager Schotte, could provide a
    link to the reality in Europe. But in fact, any such wall contained so many
    cracks, gaps, and hatches as to render it imaginary. The free flow of informa-
    tion, instructions, requests, and approvals by Watson remained detailed and
    continuous for years to come—until well into 1944.

    [...]

    Using codes and oblique references, they nonetheless all spoke the
    same language, even when the language was vague.

    [...]

    Millions of punch cards were routinely shipped from IBM in America
    directly to Nazi-controlled sources in Poland, France, Bulgaria, and Belgium,
    or routed circuitously through Sweden or colonies in Africa. When IBM's
    American presses did not fill orders, subsidiaries themselves would ship
    cards across frontiers from one IBM location to another. 125

    -- 264-265

Such knowledge would in fact interest the allies. But curiously the State
Deparment acted as a "postman" during "DURING IBM'S day-to-day struggle to
stay in the Axis during wartime" (page 277):

    The Department's desire to secretly advance the commercial causes of
    IBM persevered in spite of the nation's officially stated opposition to the
    Hitler menace. For this reason, it was vital to Watson that nothing be done to
    embarrass or even annoy the Department publicly. This caution was only
    heightened by an on-going FBI investigation into IBM's operation as a
    potential hotbed of Nazi sympathizers. Avoiding embarrassing moments was
    difficult given the far-flung global empire of IBMers so deeply involved with
    Fascist and Axis countries, and accustomed to speaking supportively of their
    clients' military endeavors.

    -- 277

That was before the US entering the war.

### The new board

    During all the genocide years, 1942-1945, the Dehomag that Watson
    fought to protect did remain intact. Ultimately, it was governed by a special
    Reich advisory committee representing the highest echelons of the Nazi hier-
    archy. The Dehomag advisory committee replaced the traditional corporate
    board of directors. As with any board, the committee's duty was to advise

    [...]

    Four men sat on the advisory board. One was a trustee. Second was
    Passow, chief of the Maschinelles Berichtwesen. Third was Heidinger. Fourth
    was Adolf Hitler's personal representative. 160

    Hitler's representative on Dehomag's advisory committee was Dr. Edmund
    Veesenmayer. 161

    -- 271

### General Ruling 11

    As America advanced toward the moment it would enter the war, the
    Roosevelt Administration had recendy espoused General Ruling 11, an
    emergency regulation forbidding any financial transactions with Nazi Ger-
    many without a special Treasury Department license involving written justifi-
    cations. Even certain corporate instructions of a financial nature were subject
    to the rule. This was something completely new to contend with in IBM's
    Nazi alliance. IBM would now be required to seek a complicated, bureau-
    cratic approval for each financial instruction it ordered for its overseas sub-
    sidiaries under Nazi control. General Ruling 11 would not affect subsidiaries
    in neutral countries, such as Sweden or Switzerland. Even still, it would
    severely hamper all communications with Dehomag itself, and open a gov-
    ernment window into many of IBM's complex transactions. 51
    How much time did IBM have?

    -- 288

    Now it appeared that General Ruling 11 had been violated.

    -- 291

    IBM would not place a stop on any of its Dehomag business, or any
    subsidiary's interaction with it. IBM filed another request with the Treasury
    Department, this time to send an instruction to all of its European sub-
    sidiaries and agencies, as well, as its divisions in Japan. The instruction: "In
    view of world conditions we cannot participate in the affairs of our compa-
    nies in various countries as we did in normal times. Therefore you are
    advised that you will have to make your own decisions and not call on us for
    any advice or assistance until further notice." It was sent to the State Depart-
    ment on October 10, 1941, with a request for comment. 77

    -- 293

    December, just days before Pearl Harbor, to circumvent Treasury license
    requirements and issue financial instructions to Dehomag. Ultimately, after
    the U.S. joined the war against Germany, Westerholt was appointed the cus-
    todian of CEC. 39 The Nazis were able to do with CEC as they pleased so
    long as IBM was paid. The looming competition with Bull never came
    to fruition. It was more of a bargaining chip than a genuine threat. Unable to
    replace IBM, the Third Reich pressured the company into relinquishing Wat-
    son's troublesome micro-managing in favor of the faster and more coordi-
    nated action the Reich required.

    -- 306

### Holland and France

    Germany wanted the Jews identified by bloodline not religion, pauper-
    ized, and then deported to camps, just as they were elsewhere in conquered
    Europe. The Jews of France stood vulnerable under the shadow of destruc-
    tion. Hitler was ready.

    In France, the Holleriths were not.

    -- 307

    In 1936, as Inspector of Population Registries, Lentz standardized local
    population registers and their data collection methodology throughout the
    Netherlands—an administrative feat that earned him a royal decoration. That
    same year, he outlined his personal vision in Allgemeines Statistisches Archiv,
    the journal of the German Statistical Society: "Theoretically," predicted
    Lentz, "the collection of data for each person can be so abundant and com-
    plete, that we can finally speak of a paper human representing the natural
    human." 46

    [...]

    His motto was "to record is to serve." 47

    -- 308-309

    Ten days after the census ordered by decree V06/41 was fully com-
    piled, punched, and sorted, Nazi authorities demanded all Jews wear the
    Jewish star. Again a number of Dutch people reacted with outrage and
    protest. British diplomats reported that in one town, when the burgomaster
    ordered Jews to affix the star, many non-Jews wore one as well. 87
    But it was not the outward visage of six gold points worn on the chest
    for all to see on the street, it was the 80 columns punched and sorted in a
    Hollerith facility that marked the Jews of Holland for deportation to concen-
    tration camps. The Germans understood this all too well.

    -- 316

    Arthur Seyss-Inquart, German Kommissar for Holland: 'Thanks to decree
    6/41, all Dutch Jews are now in the bag." 88

    FRANCE EXCELLED at many things. Punch card automation was not one of
    them. Although IBM had been able to install several hundred Hollerith
    devices, mainly for high-volume military, railway, and banking users, Reich
    forces had in large part confiscated those machines

    -- 317

    Oppressive Nazi rule could have dictated its iron will to all reluctant
    French authorities, and conquered the demographic uncertainties of a
    French Jewry in two zones if only the Holleriths could be deployed. That is
    precisely what Holleriths brought to any problem—organization where there
    was disorder and tabular certainty where there was confusion. The Nazis
    could have punch-carded the Jews of France into the same genocidal sce-
    nario in force elsewhere, including Holland. But in the aftermath of the MB's
    technologic ravages, France's punch card infrastructure was simply incapable
    of supporting the massive series of programs Berlin required. Even if the
    machines could have been gathered, transferred, or built—CEC just didn't
    have the punch cards.

    -- 319

    Rene Carmille, comp- troller general of the French Army, had for years been an
    ardent advocate of punch cards. More than that, he had machines in good working
    order at his government's Demographic Service. Carmille came forward and
    offered to end the census chaos. He promised that his tabulators could deliver
    the Jews of France. 119

    -- 324

    Carmille had been working for months on a national Personal Identifi-
    cation Number, a number that would not only be sequential, but descriptive.
    The thirteen-digit PIN number would be a manual "bar code" of sorts
    describing an individual's complete personal profile as well as professional
    skills in great detail. For example, one number would be assigned for metal
    workers, with a second modifying number for brass, and then a third modi-
    fying number for curtain rods. Tabulators could then be set to whisk
    through millions of cards until it located French metal workers, specializing
    in brass with experience in curtain rods. Those metal workers could also be
    pinpointed in any district. The system mimicked a concurrent Reich codifica-
    tion system that assigned a descriptive bar code-like number to every prod-
    uct and component in Germany. Carmille's number would ultimately evolve
    into France's social security number. 123

    -- 325

    "We are no longer dealing with general censuses, but we are really following
    individuals." Carmille made clear, "the new organization must now be envisioned
    in such a way that the information be obtained continuously, which means that
    the updating of information must be carefully regulated." 127 Carmille was now
    France's great Hollerith hope.

    -- 328-329

    Clearly, Carmille was running an active tabulator operation. Why wasn't
    he producing the Jewish lists?

    [...]

    Just days after the French mobilized in Algeria the Nazis discovered
    that Carmille was a secret agent for the French resistance. He had no inten-
    tion of delivering the Jews. It was all a cover for French mobilization.

    [...]

    Carmille had deceived the Nazis. In fact, he had been working with
    French counter-intelligence since 1911. During the worst days of Vichy,
    Carmille was always considered one of the highest-placed operatives of the
    French resistance, a member of the so-called "Marco Polo Network" of sabo-
    teurs and spies. Carmille's operation had generated some 20,000 fake iden-
    tity passes. And he had been laboring for months on a database of 800,000
    former soldiers in France who could be instandy mobilized into well-
    planned units to fight for liberation. Under his plan, 300,000 men would be
    ready to go. He had their names, addresses, their military specialties, and all
    their occupational skills. He knew which ones were metal workers specializ-
    ing in curtain rods, and which were combat-ready troops. 154
    As for column 11 asking for Jewish identity, the holes were never
    punched—the answers were never tabulated. 155 More than 100,000 cards
    of Jews sitting in his office—never handed over. 156 He foiled the entire
    enterprise.

    -- 332-333

    In early 1944, SS security officers ordered Carmille arrested. He was
    apprehended in Lyon at noon on February 3, 1944. He was taken to the
    Hotel Terminus where his interrogator was the infamous Butcher of Lyon,
    Klaus Barbie. Barbie was despised as a master of torture who had sadistically
    questioned many members of the resistance. Carmille went for two days
    straight under Barbie's hand. He never cracked. 159

    -- 334

    It never stopped in Holland. The Population Registry continued to
    spew out tabulations of names. The trains continued to roll.
    Meanwhile, in France, the Germans also deported Jews to death camps
    as often as possible. But in France, Nazi forces were compelled to continue
    their random and haphazard round-ups. 168

    Carmille was sent to Dachau, prisoner 76608, where he died of exhaus-
    tion on January 25, 1945. He was posthumously honored as a patriot
    although his role in dramatically reducing the number of Jewish deaths in
    France was never really known and in some cases doubted. How many lives
    he saved will never be tabulated. After the war, Lentz explained he was just a
    public servant. He was tried, but only on unrelated charges, for which he was
    sentenced to three years inprison. 169

    Holland had Lentz. France had Carmille. Holland had a well-entrenched
    Hollerith infrastructure. France's punch card infrastructure was in complete
    disarray.

    -- 336

### American Property

    So even though corporate parents, such as IBM, were not
    permitted to communicate with their own subsidiaries because they were in
    Axis territory, these companies were deemed American property to be pro-
    tected. In fact, since IBM only leased the machines, every Dehomag machine,
    whether deployed at the Waffen-SS office in Dachau or an insurance office in
    Rome, was considered American property to be protected. 10

    -- 342

### War, Computing, Cryptography and Meteoroloy

    IBM and its technology were in fact involved in the Allies' most top-
    secret operations. The Enigma code crackers at Bletchley Park in England
    used Hollerith machines supplied by IBM's British licensee, the British Tabu-
    lating Machine Company. Hut 7 at Bletchley Park was known as the Tabulating
    Machine Section. As early as January 1941, the British Tabulating Machine
    Company was supplying machines and punch cards not only to Bletchley
    Park, but to British intelligence units in Singapore and Cairo as well. 40

    Park, but to British intelligence units in Singapore and Cairo as well. 40
    By May 1942, IBM employees had joined America's own cryptographic
    service. A key man was Steve Dunwell, who left Endicott's Commercial Re-
    search Department to join other code breakers in Washington, D.C. The
    group used a gamut of punch card machines made by IBM as well as Rem-
    ington Rand to decipher intercepted Axis messages. Captured enemy code
    books were keyed into punch cards using overlapping strings of fifty digits.
    The punched cards were sorted. Each deciphered word was used to attack
    another word until a message's context and meaning could laboriously be
    established. At one point, Dunwell needed a special machine with electro-
    mechanical relays that could calculate at high speed the collective probability
    of words that might appear in a theoretical message bit. Dunwell sought per-
    mission from Watson to ask that the device be assembled at IBM. Watson
    granted it.

    It was an irony of the war that IBM equipment was used to encode and
    decode for both sides of the conflict. 42

    IBM was there even when the Allies landed at Normandy on June 6,
    1944. Hollerith machines were continuously used by the Weather Division of
    the Army Air Forces to monitor and predict t h e tempestuous storms afflicting
    the English Channel. When Al lied troops finally landed at Normandy, MRUs
    went in soon after the beachhead was secured. 43
    War had always been good to IBM. In America, war income was with-

    -- 348

    IBM machines were not just used to wage war. They were also used to
    track people. Holleriths organized millions for the draft. Allied soldiers miss-
    ing in action, as well as captured Axis prisoners, were cataloged by IBM sys-
    tems.

    -- 349

### Untouchable and beyond reach of any nation

    IBM and Watson were untouchable. Carter learned the immutable truth in the very
    words he had written months earlier:

        This [World War] is a conflict of warlike nationalistic states, each having cer-
        tain interests. Yet we frequently find these interests clashing diametrically
        with the opposing interests of international corporate structures, more huge
        and powerful than nations.

    [...]

    IBM was in some ways bigger than the war. Both sides could not afford
    to proceed without the company's all-important technology. Hitler needed
    IBM. So did the Allies.

    -- 352

### One could never escape his code (p. 367), Hollerith erfasst: the Logistics of Genocide (p. 375)

    For the Allies, IBM assistance came at a crucial point. But for the Jews
    of Europe it was too late. Hitler's Holleriths had been deployed against them
    for almost a decade and were continuing without abatement. Millions of
    Jews would now suffer the consequences of being identified and processed
    by IBM technologies.

    After nearly a decade of incremental solutions the Third Reich was
    ready to launch the last stage. In January 1942, a conference was held in
    Wannsee outside Berlin. This conference, supported by Reich statisticians
    and Hollerith experts, would outline the Final Solution of the Jewish prob-
    lem in Europe. Once more, Holleriths would be used, but this time the Jews
    would not be sent away from their offices or congregated into ghettos. Ger-
    many was now ready for mass shooting pits, gas chambers, crematoria, and
    an ambitious Hollerith-driven program known as "extermination by labor"
    where Jews were systematically worked to death like spent matches.
    For the Jews of Europe, it was their final encounter with German
    automation.

    -- 354

    The multitude of columns and codes punched into Hollerith and sorted
    for instant results was an expensive, never-ending enterprise designed to
    implement Hitler's evolving solutions to what was called the Jewish problem.
    From Germany's first identifying census in 1933, to its sweeping occupa-
    tional and social expulsions, to a net of ancestral tracings, to the Nuremberg
    definitions of 1935, to the confiscations, and finally to the ghettoizations, it
    was the codes that branded the individual and sealed his destiny. Each code
    was a brick in an inescapable wall of data. Trapped by their code, Jews could
    only helplessly wait to be sorted for Germany's next persecution. The system
    Germany created in its own midst, it also exported by conquest or subver-
    sion. As the war enveloped all Europe, Jews across the Continent found
    themselves numbered and sorted to one degree or another.

    By early 1942, a change had occurred. Nazi Germany no longer killed
    just Jewish people. It killed Jewish populations. This was the data-driven
    denouement of Hitler's war against the Jews.

    Hollerith codes, compilations, and rapid sorts had enabled the Nazi
    Reich to make an unprecedented leap from individual destruction to some-
    thing on a much larger scale.

    -- 369

    Der Fuhrer was now deter- mined to unleash a long contemplated campaign of
    systematic, automated genocide, thus once and for all ridding the world of
    Jews. 68

    -- 370

    By early 1944, Korherr was able to report to Eichmann a total of 5 million Jews
    eliminated by "natural decrease, con- centration camp inmates, ghetto inmates,
    and those who were [simply] put to death." 88

    [...]

    More than a statistical bureau, by its very nature, the Hollerith complex at
    Friedrichstrasse helped Hitler, Himmler, Heydrich, and Eichmann prioritize,
    schedule, and manage the seemingly impossible logistics of genocide across
    dozens of cities in more than twenty countries and territories. It was not just
    people who were counted and marshaled for deportation. Boxcars, locomotives,
    and intricate train timetables were sched- uled across battle-scarred
    borders—all while a war was being fought on two fronts. The technology had
    enabled Nazi Germany to orchestrate the death of millions without skipping a
    note.

    Amidst the whirlwind of the Final Solution, the Third Reich's transition
    from the blind persecution of a general population to the destruction of indi-
    viduals had come full circle. In genocide, the Jews lost their identity. They
    had been reduced to mere nameless data bits. Now each murdered Jew no
    longer even represented an individual death. Now every corpse comprised a
    mere component in a far larger statistical set adding up to total annihilation.

    -- 375

### Business Philosophy of "The Sollutions Company" (page 429)

    Perhaps IBM's business philosophy was best expressed by an executive
    of Beige Watson in an August 1939 letter to senior officers of IBM NY. The
    letter detailed the company's growing involvement in Japan's aircraft indus-
    try. The IBM Brussels executive declared: "It is none of our business to
    judge the reasons why an American corporation should or would help a for-
    eign Government, and consequently Mr. Decker and myself have left these
    considerations entirely out of our line of thought. ... we are, as IBM men,
    interested in the technical side of the application of our machines." 102
    But as European territory was liberated in late 1944 and early 1945,

    -- 399

    Fellinger even put IBM's interest before that of the Third Reich, con-
    stantly badgering Berlin to pay more rent, and clear up its delinquencies.
    He even demanded that the Wehrmacht pay for CEC machines the German
    military seized from occupied France. It took months of burdensome legal
    wrangling, but Fellinger successfully argued that the German military had no
    right to remove CEC's machines without properly compensating IBM. His
    argument hammered away at the theme that because the plundered machines
    were leased items, they never belonged to the French government, but to IBM.
    As such, the transferred devices were not subject to traditional rules of "war
    booty." Only after reams of Fellinger's dense briefs, supported by attestations
    by CEC Managing Director Roger Virgile, did the MB finally consent to nearly
    a million Reichsmarks in back rent for machines transported out of France. 19

    -- 407

    Eventually, after ceaseless efforts, IBM NY regained control of its Ger-
    man subsidiary. The name had been changed, the money regained, the
    machines recovered, the record clear. For IBM the war was over.

    But for the descendants of 6 million Jews and millions of other Euro-
    peans, the war would never be over. It would haunt them and people of con-
    science forever. After decades of documentation by the best minds, the most
    studied among them would confess that they never really understood the
    Holocaust process. Why did it happen? How could it happen? How were they
    selected? How did the Nazis get the names? They always had the names.

    What seemingly magical scheduling process could have allowed mil-
    lions of Nazi victims to step onto train platforms in Germany or nineteen
    other Nazi-occupied countries, travel for two and three days by rail, and then
    step onto a ramp at Auschwitz or Treblinka—and within an hour be marched
    into gas chambers. Hour a f t e r hour. Day a f t er day. Timetable after timetable.

    Like clockwork, and always with blitzkrieg efficiency.
    The survivors would never know. The liberators who fought would
    never know. The politicians who made speeches would never know. The
    prosecutors who prosecuted would never know. The debaters who debated
    would never know.

    The question was barely even raised.

    -- 429-430

    "IBM does not have much information about this period"

    -- 433

    IBM stuck to its story that the "Information Company" had no information about
    the documents in its own archives, and had transferred some documents to
    esteemed institutions for study.

    -- 452

## Index

* Contract irregularities and American taxpayers subsidizing Hollerith, 34.
* Statistics, "race statistics", intellectual shock troops, 53-55.
* Tax avoidance, 65-66.
* Plan for a tower centralizing all the information, 97-98.
* Organized sterilization, 99.
* Slogan: "Hollerith illuminates your company, provides surveillance and helps organize", 110.
* Powers Machine Company, specialized, old and still functioning, like a niche technology, 108.
* Punch card and equipment production in numbers, 123-124.
* Accounting manipulation, 126-130.
* Meeting with Mussolini, 137.
* Meeting with Hitler, '"Heil!" 108 Watson lifted his right arm halfway up before he caught himself', 138.
* Watson wearing a medal with swastikas, 140.
* Office of Automated Reporting (Maschinelles Berichtwesen) and an "universal punch code system", 158-159.
* Animal censuses, 211.
* Monopoly and anti-trust ligitations, proprietary technology, 36, 213-214; monopoly and Soviet government, 243.
* FBI investigation on germans at the IBM, 219-220.
* Examples use for punch cards in nazi-Germany, 215-216, 373; at page 230 it's mentioned the "Race and Setdement Office", "a marriage-assistance bureau for SS officers" "who fulfilled the `[racial]` requirements for marriage", a pre-tinder automated dating agency that could not run correctly due to difficult access to Hollerith machines.
* Bizarre "alien" corporation management by a trustee in war-declared situations (Alien Property Custodian) with plausible deniability, destruction of evidence and layers of indirection, 238-241.
* IBM and State Department, 242.
* Watson was a micromanager, micromanagement (many places in the book).
* Money/revenue flow, 252.
* Patent war, 258 and other pages.
* Nazi-Germany and other US companies, 259.
* Irish Republican Army, 260.
* Ustashi croatian militia, 260.
* International Telephone Company reorganization in Spain; company re-organization under fascist-regimes, 262.
* Competitors: Bull in France, Powers in the US, Kamatec in Holland, 263.
* Veesenmayer: "technical scheduler of actual genocide", 268.
* Network of Hollerith systems installed at railroad junctions; relation between punch cards and trains, 270.
* Tulard file, a form system from 1941, 322.
* Notice from the Jewish Underground, 331.
* Holland and France in numbers: death-ratios (Jews counted / murdered) of 73% versus 25%, 336.
* Control in Business Machines, corporation as an "international monster" (which sounds like a "transleviathan"), 339.
* Argument that Hollerith patents should belong to the US Government "in the first place", 340.
* Watson motive to be "in the international peace movement", 340.
* IBM guns, grenades and masks, 346.
* Final Solution, 370.
* Daily death-rate at Auschwitz getting higher and outpassing Hollerith capacity, giving place to improvised number schemes; decrease of order, 357-358.
* Mengele and his own distinct numbers tatooed on inmates, 357.
* Protocol for mass Jewish extermination, 370.
* Switzerland: "switchboard for Nazi-era commercial intrigue"; banks as annomization proxies, 395.
* Document fabrication "to demonstrate compliance when the opposite was true" and client "blacklisting", 397.
* Watson's letter to all subsidiaries on enemy territory stating that now they were on their own, which in practice was only partially true 293, 398.
* The role of a neutral country to put a subsidiary as a proxy - or a "nexus" (page 399) - between a corporation and it's branches on enemy territories; in the case of IBM it was on Geneva, Switzerland, "a clearing office between the local organizations (...) and the New York Headquarters", 395-399.
* Validity of "punch card signature", 407.
* IBM Soldiers, 409.
* Reparation avoidance after the war, 422.
* Simultaneus translation technology during the Nuremberg Trials, by IBM and free-of-charge, 425.
* Hollerith usage by Allies, 426.
* IBM exemption, 426.
* Another Census, 428.
* Book "The History of Computing in Europe", 429.
* Manual punch card sorting by concentration camp inmates, 432.
* The Hollerith Bunker, 432.
* Caloric intake rationing, 196, 443.
* Defensive Documentation, 446.
* IBM Klub and House of Data, 449.
* Investigation that required "Holocaust knowledge with an emphasis on Hitler-era finance, added to information-technology expertise, sifted through the dogged techniques of an investigative reporter", 454.
* Local and central processing facilities -- like Berlin and Oranienburg, 455.

## Further reading

* Quantifying the Holocaust: Hyperintense kill rates during the Nazi genocide:
  * https://www.buzzfeednews.com/article/danvergano/nazi-holocaust-death-rate-underestimated
  * http://advances.sciencemag.org/content/5/1/eaau7292
  * http://advances.sciencemag.org/content/5/1/eaau7292/tab-pdf
  * https://encyclopedia.ushmm.org/content/en/article/operation-reinhard-einsatz-reinhard

    > Operation Reinhard (1942–1943) was the largest single murder campaign of the
    > Holocaust, during which some 1.7 million Jews from German-occupied Poland were
    > murdered by the Nazis. Most perished in gas chambers at the death camps Belzec,
    > Sobibor, and Treblinka. However, the tempo, kill rates, and spatial dynamics of
    > these events were poorly documented. Using an unusual dataset originating from
    > railway transportation records, this study identifies an extreme phase of
    > hyperintense killing when >1.47 million Jews—more than 25% of the Jews killed
    > in all 6 years of World War II—were murdered by the Nazis in an intense,100-day
    > (~3-month) surge.  Operation Reinhard is shown to be an extreme event, based on
    > kill rate, number, and proportion (>99.9%) of the population murdered in camps,
    > highlighting its singularly violent character, even compared to other more
    > recent genocides. The Holocaust kill rate is some 10 times higher than
    > estimates suggested by authorities on comparative genocide.

* Unsorted:
  * [IBM Archives: 1933](https://www.ibm.com/ibm/history/history/year_1933.html)
  * [IBM100 - A Culture of Think](https://www.ibm.com/ibm/history/ibm100/us/en/icons/think_culture/transform/)
  * [Dehomag (Deutsche Hollerith Maschinen) D11 tabulator - Collections Search - United States Holocaust Memorial Museum](https://collections.ushmm.org/search/catalog/irn521586)
  * [Dehomag D11 sorter - Collections Search - United States Holocaust Memorial Museum](https://collections.ushmm.org/search/catalog/irn521587)
  * [How IBM Technology Jump Started the Holocaust](https://gizmodo.com/how-ibm-technology-jump-started-the-holocaust-5812025)
  * [IBM, Hitler and the Holocaust: A Terrible Tale of Capitalism Without Conscience | Corporate Greed & Corruption Chronicles](https://corporategreedchronicles.wordpress.com/2011/11/15/ibm-hitler-and-the-holocaust-a-terrible-tale-of-capitalism-without-conscience/)
  * [IBM & "Death's Calculator"](https://www.jewishvirtuallibrary.org/ibm-and-quot-death-s-calculator-quot)
  * [Computing at Columbia Timeline](http://www.columbia.edu/cu/computinghistory/index.html#1939)
  * [ibm carbine For Sale – Buy ibm carbine at GunBroker.com](https://www.gunbroker.com/All/search?Keywords=ibm%20carbine)
  * [Hollerith Census Machine dials | Marcin Wichary | Flickr](https://www.flickr.com/photos/mwichary/2632673143/in/photostream/)
  * [Henri Georges Trainson: Annexe III - Le réseau Marco-Polo](https://hgtrainson.blogspot.com/2011/08/annexe-iii-le-reseau-marco-polo.html)
  * [Réseau Marco Polo : définition de Réseau Marco Polo et synonymes de Réseau Marco Polo (français)](http://dictionnaire.sensagent.leparisien.fr/R%C3%A9seau%20Marco%20Polo/fr-fr/)
  * [Klaus Barbie - Wikipedia](https://en.wikipedia.org/wiki/Klaus_Barbie)
  * [Nikolaus “Klaus” Barbie: The Butcher of Lyon | Holocaust Encyclopedia](https://encyclopedia.ushmm.org/content/en/article/nikolaus-klaus-barbie-the-butcher-of-lyon)
  * [Klaus Barbie: women testify of torture at his hands](http://www.writing.upenn.edu/~afilreis/Holocaust/barbie.html)
  * [PBS Frontline: Klaus Barbie The American Connection (1983) - YouTube](https://www.youtube.com/watch?v=58FVOCktU5U)

[[!tag tecnology history sociology]]
