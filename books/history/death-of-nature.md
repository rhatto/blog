[[!meta title="The Death of Nature"]]

## Topics

* Bohm's process physics.
* Ilya Prigogine new thermodynamics.

## Excerpts

> Between the sixteenth andseventeenth cerfturies the image of an organic
> cosmos with a living female earth at its ceriter gave way to a mechanistic
> world view in which nature was reconstructed as dead and passive, to be
> dominated and controlled by hufuans. The Death efNature deals with the
> economic, cultural, and scientific changes through which this vast
> transformation came about. In seeking to understand how people conceptualized
> nature in the Scientific Revolution, I am asking not about unchanging
> essences, but about connections between social change and changing
> constructions of nattlre". Similarly. when women today attempt to change
> society's domination of nature, 1:\1~¥.,~e acting to overturn moder_n
> constructions of nature and women as culturally passive and subordinate.
>
> [...]
>
> Today's feminist and ecological consciousness can be used to examine the
> historical interconnections between women and nature that developed as the
> modern scientific and economic world took form in the sixteenth and
> seventeenth centuries-a transformation that shaped and pervades today's
> mainstream values and perceptions.  Feminist history in the broadest sense
> requires that we look at
>
> [...]
>
> My intent is instead to examine the values associated with the images of
> women and nature as they relate to the formation of our modern world and
> their implications for 'our lives today.
>
> In investigating the roots of our current environmental dilemma and its
> connections to science, technology, and the economy, we must reexamine the
> formation of a world view and a science that, by reconceptualizing reality as
> a machine rather than a living organism, sanctioned the domination of both
> nature and women. The contributions of such founding "fathers" of modern
> science as Francis Bacon, William Harvey, Rene Descartes, Thomas Hobbes, and
> Isaac Newton must be reevaluated. The fate of other options, alternative
> philosophies, and social groups shaped by the organic world view and
> resistant to the growing exploitative mentality needs reappraisal. To
> understand why one road rather than the other was taken requires a broad
> synthesis of both the natural and cultural environments of Western society at
> the historical turning point.  This book elaborates an ecological perspective
> that includes both

### Terminology

Nature, art, organic and mechanical:

> A distinction was commonly made between natura naturans, or nature creating,
> and natura naturata, the natural creation.
>
> Nature was contrasted with art (techne) and with artificially created things.
> It was personified as a female-being, e.g., Dame Nature; she was alternately
> a prudent lady, an empress, a mother, etc.  The course of nature and the laws
> of nature were the actualization of her force. The state of nature was the
> state of mankind prior to social organization and prior to the state of
> grace. Nature spirits, nature deities, virgin nymphs, and elementals were
> thought to reside in or be associated with natural objects.
>
> In both Western and non-Western cultures, nature was traditionally feminine.
>
> [...]
>
> In the early modern period, the term organic usually referred to the bodily
> organs, structures, and organization of living beings, while organicism was
> the doctrine that organic structure was the result of an inherent, adaptive
> property in matter. The word organical, however, was also sometimes used to
> refer to a machine or an instrument. Thus a clock was sometimes called an
> "organical body," while som~ machines were said to operate by organical,
> rather than mechanical, action if the touch of a person was involved.
>
> Mechanical referred to the machine and tool trades; the manual operations of
> the handicrafts; inanimate machines that lacked spontaneity, volition, and
> thought; and the mechanical sciences. 1

### Nature that nurtures and thats also uncontrollable, replaced by "the machine"

> NATURE AS NURTURE: CONTROLLING IMAGERY. Central to the organic theory was the
> identification of nature, especially the earth, with a nurturing mother: a
> kindly beneficent female who provided for the needs of mankind in an ordered,
> planned universe. But another opposing image of nature as female was also
> prevalent: wild and uncontrollable nature that could render violence, storms,
> droughts, and general chaos. Both were identified with the female sex and
> were projections of human perceptions onto the external world. The metaphor
> of the earth as a nurturing mother was gradually to vanish as a dominant
> image as the Scientific Revolution pro- ceeded to mechanize and to
> rationalize the world view. The second image, nature as disorder, called
> forth an important modern idea, that of power over nature. Two new ideas,
> those of mechanism and of the domination and mastery of nature, became core
> concepts of the modern world. An organically oriented mentality in which
> female principles played an important role was undermined and replaced by a
> mechanically oriented mentality that either eliminated or used female
> principles in an exploitative manner. As Western culture became increasingly
> mechanized in the 1600s, the female earth and virgin earth spirit were
> subdued by the machine. 1

### Mining and the female body

> The image of the earth as a living organism and nurturing mother had served
> as a cultural constraint restricting the actions of human beings. One does
> not readily slay a mother, dig into her entrails for gold or mutilate her
> body, although commercial mining would soon require that. As long as the
> earth was considered to be alive and sensitive, it could be considered a
> breach of human ethical behavior to carry out destructive acts against it.
> For most traditional cultures, minerals and metals ripened in the uterus of
> the Earth Mother, mines were compared to her vagina, and metallurgy was the
> human hastening of the birth of the living metal in the artificial womb of
> the furnace-an abortion of the metal's natural growth cycle before its time.
> Miners offered propitiation to the deities of the soil and subterranean
> world, performed ceremonial sacrifices, · and observed strict cleanliness,
> sexual abstinence, and fasting before violating the sacredness of the living
> earth by sinking a mine.  Smiths assumed an awesome responsibility in
> precipitating the metal's birth through smeltin,.g, fusing, and beating it
> with hammer and anvil; they were often accorded the status of shaman in
> tribal rituals and their tools were thought to hold special powers.

Is there a relation between torture (basanos), extraction of "truth" and
mining gold out of a mine? See discussions both on "The Counterrevolution"
and "Torture and Truth".

### Hidden norms: controlling images

> Controlling images operate as ethical restraints or as ethical sanctions-as
> subtle "oughts" or "ought-nots." Thus as the descriptive metaphors and images
> of nature change, a behavioral restraint can be changed into a sanction. Such
> a change in the image and description of nature was occurring during the
> course of the Scientific Revolution.
>
> It is important to recognize the normative import of descriptive statements
> about nature. Contemporary philosophers of language have critically
> reassessed the earlier positivist distinction between the "is" of science and
> the "ought" of society, arguing that descriptions and norms are not opposed
> to one another by linguistic sepa- ration into separate "is" and "ought"
> statements, but are contained within each other. Descriptive statements about
> the world can presuppose the normative; they are then ethic-laden.
>
> [...]
>
> The writer or culture may not be conscious of the ethical import yet may act
> in accordance with its dictates. The hidden norms may become conscious or
> explicit when an alternative or contradiction presents it- self. Because
> language contains a culture within itself, when language changes, a culture
> is also changing in important way~~ By examining changes in descriptions of
> nature, we can then perceive something of the changes in cultural values.

### Renaissance: hierarchical order

> The Renaissance view of nature and society was based on the organic analogy
> between the human body, or microcosm, and the larger world, or macrocosm.
>
> [...]
>
> But while the pastoral tradition symbolized nature as a benevolent female, it
> contained the implication that nature when plowed and cultivated could be
> used as a commodity and manipulated as a resource. Nature, tamed and subdued,
> could be transformed into a garden to provide both material and spiritual
> food to enhance the comfort and soothe the anxieties of men distraught by the
> demands of the urban world and the stresses of the marketplace. It depended
> on a masculine perception of nature as a mother and bride whose primary
> function was to comfort; nurture, and provide for the wellbeing of the male.
> In pastoral imagery, both nature and women are subordinate and essentially
> passive. They nurture but do not control or exhibit disruptive passion. The
> pastoral mode, although it viewed nature as benevolent, was a model created
> as an antidote to the pressures of urbanization and mechanization. It
> represented a fulfillment of human needs for nurture, but by conceiving of
> nature as passive, it nevertheless allowed for the possibility of its use and
> manipulation. Unlike the dialectical image of nature as the active uni- ty of
> opposites in tension, the Arcadian image rendered nature passive and
> manageable.

### Undressing

> An allegory (1160) by Alain of Lille, of the School of Chartres, portrays
> Natura, God's powerful but humble servant, as stricken with grief at the
> failure of man (in contrast to other species) to obey her laws. Owing to
> faulty supervision by Venus, human beings engage in adulterous sensual love.
> In aggressively penetrating the secrets of heaven, they tear Natura's
> undergarments, exposing her to the view of the vulgar. She complains that "by
> the unlawful assaults of man alone the garments of my modesty suffer disgrace
> and division."
>
> [...]
>
> Such basic attitudes toward ·male-female roles in biological generation where
> the female and the earth are both passive receptors could easily become
> sanctions for exploitation as the organic context was transformed by the rise
> of commercial capitalism.
>
> [...]
>
> The macrocosm theory, as we have seen, likened the cosmos to the human body,
> soul, and spirit with male and female reproductive components. Similarly, the
> geocosm theory compared the earth to the living human body, with breath,
> blood, sweat, and elimination systems.
>
> [...]
>
> The earth's springs were akin to the human blood system; its other various
> fluids were likened to the mucus, saliva, sweat, and other forins of
> lubrication in the human body, the earth being organized "'. .. much after
> the plan of our bodies, in which there are both veins and arteries, the
> former blood vessels, the latter air vessels ....  So exactly alike is the
> resemblance to our bodies in nature's formation of the earth, that our
> ancestors have spoken of veins [springs] of water." Just as the human body
> contained blood, marrow, mucus, saliva, tears, and lubricating fluids, so in
> the earth there were various fluids. Liquids that turned hard became metals,
> such as gold and silver; other fluids turned into stones, bitumens, and veins
> of sulfur. Like the human body, the earth gave forth sweat: "There is often a
> gathering of thin, scattered moisture like dew, which from many points flows
> into one spot. The dowsers call it sweat, because a kind of drop is either
> squeezed out by the pressure of the ground or raised by the heat."
>
> Leonardo da Vinci (1452-1519) enlarged the Greek analogy between the waters
> of the earth and the ebb and flow of human blood through the veins and heart
>
> [...]
>
> A widely held alchemical belief was the growth of the baser metals into gold
> in womblike matrices in the earth. The appearance of silver in lead ores or
> gold in silvery assays was evidence that this transformation was under way.
> Just as the child grew in the warmth of the female womb, so the growth of
> metals was fostered

### Matrix

> The earth in the Paracelsian philosophy was the mother or matrix
> giving birth to plants, animals, and men.

### Renaissance was diverse

> In general, the Renaissance view was that all things were permeated by life,
> there being no adequate method by which to designate the inanimate from the
> animate.  [...] but criteria by which to differentiate the living from the
> nonliving could not successfully be formulated. This was due not only to the
> vitalistic framework of the period but to striking similarities between them.
>
> [...]
>
> Popular Renaissance literature was filled with hundreds of images associating
> nature, matter, and the earth with the female sex.
>
> [...]
>
> In the 1960s, the Native-American became a symbol in the ecology movement's
> search for alternatives to Western exploitative attitudes. The Indian
> animistic belief-system and reverence for the earth as a · mother were
> contrasted with the Judeo-Christian heritage of dominion over nature and with
> capitalist practices resulting in the "tragedy of the commons" (exploitation
> of resources available for any person's or nation's use). But as will be
> seen, European culture was more complex and varied than this judgment allows.
> It ignores the Renaissance philosophy of the nurturing earth as well as those
> philosophies and social movements resistant to mainstream economic change.

### Mining as revealing the hidden secrets

> In his defense, the miner argued that the earth was not a real mother, but a
> wicked stepmother who hides and conceals the metals in her inner parts
> instead of making them available for human use.
>
> [...]
>
> In the old hermit's tale, we have a fascina,ting example·of the re:·
> lationship between images and values. The older view of nature as a kindly
> mother is challenged by the growing interests of the mining industry in
> Saxony, Bohemia, and the Harz Mountains, regions of newly found prosperity
> (Fig. 6). The miner, representing these newer commercial activities,
> transforms the irnage of the nurturing mother into that of a stepmother who
> wickedly conceals her bounty from the deserving and needy children. In the
> seventeenth century, the image will be seen to undergo yet another
> transformation, as natural philosopher Francis Bacon (1561-1626) sets forth
> the need for prying into nature's nooks and crannies in searching out her
> secrets for human improvement.
>
> -- 33
