[[!meta title="Manual da Espaçonave Terra"]]

[Manual De Instruções Para Espaçonave Terra](https://archive.org/details/86522184RBuckminsterFullerManualDeInstrucoesParaANaveEspacialTerraViaOptima1998).

    Contudo, e de súbito, e sem que na sociedade ninguém se apercebesse, surgiu o
    anti-corpo evolucionário contra a extinção na forma do computador e da sua
    automatização globalmente orientada, que tornou o homem obsoleto como
    especialista da produção e controle físicos -- mesmo em cima da hora.
 
    Como superespecialista, o computador pode perseverar dia e noite, separando o
    preto do branco a velocidades sobrehumanas. O computador pode também operar em
    graus de frio ou calor nos quais o homem pereceria. Como especialista, o homem
    vai ser completamente afastado pelo computador. O homem vai ser obrigado a
    reestabelecer, usar e disfrutar sua "globalidade" inata. O que se nos depara é
    lidar com a totalidades da Nave Espacial Terra e do universo. A evolução parece
    estar apostada em fazer o homem cumprir um destino muito mais elevado que ser
    apenas uma simples máquina muscular e reflexa -- um autómato escravo -- a
    automatização afasta os autómatos.

    -- 24 e 25

    Todas as nossas camas de hotel em todo o mundo encontram-se desocupadas dois
    terços do tempo.  As nossas salas de estar encontram-se vazias sete oitavos do
    tempo.

    --- 80
