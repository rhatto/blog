[[!meta title="Cybernetic Revolutionaries"]]

* [Cybernetic Revolutionaries | Technology and Politics in Allende's Chile](http://www.cyberneticrevolutionaries.com/).
* [Cybernetic Revolutionaries | The MIT Press](https://mitpress.mit.edu/books/cybernetic-revolutionaries).
* Further references [here](https://links.fluxo.info/tags/cybersyn).

## General

* Diagram of The Class War as a homeostatic system, 200.

## Control and descentralization

    Beer’s writings on management cybernetics differed from the contemporaneous
    work taking place in the U.S. military and think tanks such as RAND that led to the de-
    velopment of computer systems for top- down command and control. From the 1950s
    onward, Beer had drawn from his understanding of the human nervous system to
    propose a form of management that allowed businesses to adapt quickly to a changing
    environment. A major theme in Beer’s writings was finding a balance between central-
    ized and decentralized control, and in particular how to ensure the stability of the
    entire firm without sacrificing the autonomy of its component parts.

    Similarly, the Popular Unity government confronted the challenge of how to imple-
    ment substantial social, political, and economic changes without sacrificing Chile’s
    preexisting constitutional framework of democracy. A distinguishing feature of Chile’s
    socialist process was the determination to expand the reach of the state without sac-
    rificing the nation’s existing civil liberties and democratic institutions. Both Beer and
    Popular Unity were thus deeply interested in ways of maintaining organizational
    stability in the context of change and finding a balance between autonomy and
    cohesion.

    -- 16

## Adaptive Control

    The idea of control is commonly associated with domination. Beer offered a different
    definition: he defined control as self- regulation, or the ability of a system to adapt to
    internal and external changes and survive. This alternative approach to control re-
    sulted in multiple misunderstandings of Beer’s work, and he was repeatedly criticized
    for using computers to create top- down control systems that his detractors equated
    with authoritarianism and the loss of individual freedom. Such criticisms extended to
    the design of Project Cybersyn, but, as this book illustrates, they were to some extent
    ill- informed. To fully grasp how Beer approached the control problem requires a brief
    introduction to his cybernetic vocabulary.

    Beer was primarily concerned with the study of “exceedingly complex systems,”
    or “systems so involved that they are indescribable in detail.” 52 He contrasted exceed-
    ingly complex systems with simple but dynamic systems such as a window catch,
    which has few components and interconnections, and complex systems, which have a
    greater number of components and connections but can be described in considerable
    detail

    [...]

    In Beer’s opinion, traditional science did a good job of handling simple and complex
    systems but fell short in its ability to describe, let alone regulate, exceedingly complex
    systems. Cybernetics, Beer argued, could provide tools for understanding and control-
    ling these exceedingly complex systems and help these systems adapt to problems
    yet unknown. The trick was to “black- box” parts of the system without losing the key
    characteristics of the original. 53

    The idea of the black box originated in electrical engineering and referred to a sealed
    box whose contents are hidden but that can receive an electrical input and whose
    output the engineer can observe. By varying the input and observing the output, the
    engineer can discern something about the contents of the box without ever seeing its
    inner workings. Black- boxing parts of an exceedingly complex system preserved the
    behavior of the original but did not require the observer to create an exact representa-
    tion of how the system worked. Beer believed that it is possible to regulate exceedingly
    complex systems without fully understanding their inner workings, asserting, “It is not
    necessary to enter the black box to understand the nature of the function it performs”
    or to grasp the range of the subsystem’s behaviors. 54 In other words, it is more impor-
    tant to grasp what things do than to understand fully how they work. To regulate the
    behavior of such a system requires a regulator that has as much flexibility as the system
    it wishes to control and that can respond to and regulate all behaviors of subsystems
    that have been black- boxed.

    [...]

    Controlling an exceedingly complex system with high variety therefore requires a
    regulator that can react to and govern every one of these potential states, or, to put
    it another way, respond to the variety of the system. “Often one hears the optimistic
    demand: ‘give me a simple control system; one that cannot go wrong,’ ” Beer writes.
    “The trouble with such ‘simple’ controls is that they have insufficient variety to cope
    with the variety in the environment. . . . Only variety in the control mechanism can
    deal successfully with variety in the system controlled.” 56 This last observation—that
    only variety can control variety—is the essence of Ashby’s Law of Requisite Variety and
    a fundamental principle in Beer’s cybernetic work. 57

    The Law of Requisite Variety makes intuitive sense: it is impossible to truly control
    another unless you can respond to all attempts at subversion. This makes it extremely
    difficult, if not impossible, to control an exceedingly complex system if control is de-
    fined as domination. History is filled with instances of human beings’ trying to exert
    control over nature, biology, and other human beings—efforts that have failed because
    of their limited variety. Many of the most powerful medicines cannot adapt to all per-
    mutations of a disease. Recent work in the sociology of science has positioned Beer’s
    idea of control in contrast to the modernist ethos of many science and engineering
    endeavors, which have sought to govern ecosystems, bodily functions, and natural
    topographies. Despite the many successes associated with such projects, these efforts
    at control still have unexpected, and sometimes undesirable, results. 58

    Beer challenged the common definition of control as domination, which he viewed
    as authoritarian and oppressive and therefore undesirable. It was also “naïve, primi-
    tive and ridden with an almost retributive idea of causality.” What people viewed as
    control, Beer continued, was nothing more than “a crude process of coercion,” an
    observation that emphasized the individual agency of the entity being controlled. 59
    Instead of using science to dominate the outside world, scientists should focus on
    identifying the equilibrium conditions among subsystems and developing regulators
    to help the overall system reach its natural state of stability. Beer emphasized creating
    lateral communication channels among the different subsystems so that the changes in
    one subsystem could be absorbed by changes in the others. 60 This approach, he argued,
    took advantage of the flexibility of each subsystem. Instead of creating a regulator to fix
    the behavior of each subsystem, he found ways to couple subsystems together so that
    they could respond to each other and adapt. Such adaptive couplings helped maintain
    the stability of the overall system.

    Beer called the natural state of system stability homeostasis . 61 The term refers to the
    ability of a system to withstand disturbances in its external environment through its
    own dynamic self- regulation, such as that achieved by coupling subsystems to one
    another. Beer argued that reaching homeostasis is crucial to the survival of any system,
    whether it is mechanical, biological, or social. Control through homeostasis rather
    than through domination gives the system greater flexibility and facilitated adaptation,
    Beer argued. He therefore proposed an alternative idea of control, which he defined
    as “a homeostatic machine for regulating itself.” 62 In a 1969 speech before the United
    Nations Educational, Social, and Cultural Organization, Beer stated that the “sensible
    course for the manager is not to try to change the system’s internal behavior . . . but to
    change its structure —so that its natural systemic behavior becomes different. All of this
    says that management is not so much part of the system managed as it is the system’s
    own designer.” 63 In other words, cybernetic management as described by Beer looked
    for ways to redesign the structure of a company or state enterprise so that it would
    naturally tend toward stability and the desired behavior.

    In addition, cybernetic management sought to create a balance between horizontal
    and vertical forms of communication and control. Because changes in one subsystem
    could be absorbed and adapted to by changes in others (via lateral communication),
    each subsystem retained the ability to change its behavior, within certain limits, with-
    out threatening the overall stability of the system and could do so without direction
    from the vertical chain of command. To look at it another way, cybernetic manage-
    ment approached the control problem in a way that preserved a degree of freedom and
    autonomy for the parts without sacrificing the stability of the whole.
    The first edition of Beer’s 1959 book Cybernetics and Management did not make many

    -- 26-29

## The Liberty Machine

    The Liberty Machine modeled a sociotechnical system that functioned as a dis-
    seminated network, not a hierarchy; it treated information, not authority, as the basis
    for action, and operated in close to real time to facilitate instant decision making and
    eschew bureaucratic protocols. Beer contended that this design promoted action over
    bureaucratic practice and prevented top- down tyranny by creating a distributed net-
    work of shared information. The Liberty Machine distributed decision making across
    different government offices, but it also required all subordinate offices to limit their
    actions so as not to threaten the survival of the overall organization, in this case, a gov-
    ernment. The Liberty Machine thus achieved the balance between centralized control
    and individual freedom that had characterized Beer’s earlier work.

    [...]

    Beer posited that such a Liberty Machine could create a government where “com-
    petent information is free to act,” meaning that once government officials become
    aware of a problem, they could address it quickly; expert knowledge, not bureaucratic
    politics, would guide policy. However, Beer did not critically explore what constitutes
    “competent information” or how cybernetics might resolve disagreements within the
    scientific community or within other communities of expertise. Moreover, it is not
    clear how he separated bureaucracy from a system of checks and balances that might
    slow action but prevent abuse.

    -- 33

## Viable System Model

    The Viable System Model offered a management structure for the regulation of ex-
    ceedingly complex systems. It was based on Beer’s understanding of how the human
    nervous system functioned, and it applied these insights more generally to the behav-
    ior of organizations such as a company, government, or factory. 81

    [...]

    Beer maintained that the abstraction of the structure could be applied in numerous
    contexts, including the firm, the body, and the state. In keeping with Beer’s emphasis
    on performance rather than representation, it was not a model that accurately repre-
    sented what these systems were; rather, it was a model that described how these sys-
    tems behaved. The Viable System Model functioned recursively: the parts of a viable
    system were also viable, and their behavior could be described using the Viable System
    Model. Beer explains: “The whole is always encapsulated in each part. . . . This is a les-
    son learned from biology where we find the genetic blue- print of the whole organism
    in every cell.” 83 Thus, Beer maintained that the state, the company, the worker, and the
    cell all exhibit the same series of structural relationships.

    The Viable System Model devised ways to promote vertical and lateral communica-
    tion. It offered a balance between centralized and decentralized control that prevented
    both the tyranny of authoritarianism and the chaos of total freedom. Beer considered
    viable systems to be largely self- organizing. Therefore, the model sought to maximize
    the autonomy of its component parts so that they could organize themselves as they
    saw fit. At the same time, it retained channels for vertical control to maintain the stabil-
    ity of the whole system. These aspects of the Viable System Model shaped the design of
    Project Cybersyn and provide another illustration of how Beer and Popular Unity were
    exploring similar approaches to the problem of control.

    [...]

    The Viable System Model did not impose a hierarchical form of management in a
    traditional sense. The dynamic communication between System One and System Two
    enabled a form of adaptive man-

    [...]

    The Viable System Model draws a distinction between the bottom three levels of the
    system, which govern daily operations, and the upper two levels of management, which
    determine future development and the overall direction of the enterprise. Because the
    lower three levels manage day- to- day activities and filter upward only the most impor-
    tant information, the upper two levels are free to think about larger questions. In this
    sense, Beer’s model tackled the idea of information overload long before the Internet
    required us to wade into and make sense of an expanding sea of information.

    -- 35-38

## Management Cybernetics and Revolution

    The tension inherent in Beer’s model between individual autonomy and the welfare
    of the collective organism mirrors the struggle between competing ideologies found in
    Allende’s democratic socialism. Allende’s interpretation of Marx’s writings emphasized
    the importance of respecting Chile’s existing democratic processes in bringing about
    socialist reform, a possibility that Marx alluded to but never realized. 91 In contrast to
    the centralized planning found in the Soviet Union, Allende’s articulation of socialism
    stressed a commitment to decentralized governance with worker participation in man-
    agement, reinforcing his professed belief in individual freedoms. Yet he also acknowl-
    edged that in the face of political plurality the government would favor the “interest of
    those who made their living by their own work” and that revolution should be brought
    about from above with a “firm guiding hand.” 92

    [...]

    In October 1970, nine months before Beer heard from Flores, the cybernetician de-
    livered an address in London titled “This Runaway World—Can Man Gain Control?”
    In this lecture Beer unknowingly foretold his coming involvement with the Allende
    government. Commenting that government in its present form could not adequately
    handle the complex challenges of modern society, Beer concluded: “What is needed is
    structural change. Nothing else will do. . . . The more I reflect on these facts, the more
    I perceive that the evolutionary approach to adaptation in social systems simply will
    not work any more. . . . It has therefore become clear to me over the years that I am
    advocating revolution.” 94 Beer added, “Do not let us have our revolution the hard way,
    whereby all that mankind has successfully built may be destroyed. We do not need to
    embark on the revolutionary process, with bombs and fire. But we must start with a
    genuinely revolutionary intention: to devise wholly new methods for handling our
    problems.” 95 Less than one year later, Beer would be in Chile helping a government
    accomplish exactly this.

    -- 39-40

## Cyberfolk

    Thus Beer proposed building a new form of real- time communication, one that
    would allow the people to communicate their feelings directly to the government. He
    called this system Project Cyberfolk. In a handwritten report Beer describes how to
    build a series of “algedonic meters” capable of measuring how happy Chileans were
    with their government at any given time. 72 As noted in chapter 1, Beer used the word
    algedonic to describe a signal of pleasure or pain. An algedonic meter would allow the
    public to express its pleasure or pain, or its satisfaction or dissatisfaction with govern-
    ment actions.

    -- 89

## Constructing the Liberty Machine

    As scientific director Beer created a work culture closer to the startup culture of the
    1990s than to the chain- of- command bureaucracy that flourished in the 1960s and
    1970s and was characteristic of Chilean government agencies. He viewed his position
    as scientific director more as that of a “free agent” than a micromanager. After establish-
    ing offices at the State Technology Institute (INTEC) and the Sheraton, he informed the
    team that he would work at either location at his discretion and call on project team
    members as required. Moreover, he refused to stick to a traditional nine- to- five work
    schedule. Team members often found themselves working alongside the bearded cyber-
    netician into the wee hours of the morning. This schedule enabled them to attend to
    other projects at their regular jobs during the day and helped create an informal cama-
    raderie among team members that bolstered their enthusiasm for the project.

    [...]

    In a memo to the Cybersyn team, Beer explains that he broke Cybersyn into clearly de-
    fined subprojects that small teams could address intensively. This arrangement allowed
    for a “meeting of the minds” within the smaller group, and because the small team
    did not need approval from the larger group, it could progress quickly. At the same
    time Beer insisted that each team keep the others informed of its progress. He arranged
    large brainstorming sessions that brought together the members of different subteams.
    In these sessions, he instructed, “sniping and bickering are OUT. Brain- storming is es-
    sentially CREATIVE. . . . At least everyone gets to know everyone else, and how their
    minds work. This activity is essentially FUN: fun generates friendship, and drags us all
    out of our personal holes- in- the- ground.” Project leaders could then take ideas from
    the brainstorming sessions and use them to improve their part of the project, thus in-
    corporating the suggestions of others. Beer contrasted this “fun” style of management
    with the more common practice of bringing all interested parties together to make
    project decisions. That approach, he felt, eventually led to bickering, sniping, or sleep-
    ing. It “masquerades as ‘democratic,’ [but] is very wasteful,” he observed. 12 In addition,
    he required all project leaders to write a progress report at the end of each month and
    distribute it to the other team leaders. Beer viewed the brainstorming sessions and
    the written project reports as serving a function similar to the signals passed between
    the different organs of the body: they kept members of the team aware of activities
    elsewhere. They also allowed the different subteams to adapt to progress or setbacks
    elsewhere and helped Cybersyn maintain its viability as a coordinated project while it
    advanced toward completion.

    -- 97-99

## The October Strike

    Flores proposed setting up a central command center in the presidential palace that
    would bring together the president, the cabinet, the heads of the political parties in
    the Popular Unity coalition, and representatives from the National Labor Federation—
    approximately thirty- five people by Grandi’s estimation. Once these key people were
    brought together in one place and apprised of the national situation, Flores reasoned,
    they could then reach out to the networks of decision makers in their home institu-
    tions and get things done. This human network would help the government make
    decisions quickly and thus allow it to adapt to a rapidly changing situation. “Forget
    technology,” Flores said—this network consisted of “normal people,” a point that is
    well taken but also oversimplistic. 21 The solution he proposed was social and technical,
    as it configured machines and human beings in a way that could help the government
    adapt and survive.

    In addition to the central command hub in the presidential palace, Flores estab-
    lished a number of specialized command centers dedicated to transportation, industry,
    energy, banking, agriculture, health, and the supply of goods. Telex machines, many
    of which were already in place for Project Cybersyn, connected these specialized com-
    mand centers to the presidential palace. 22 Flores also created a secret telephone network
    consisting of eighty- four numbers and linking some of the most important people in
    the government, including members of the Popular Unity coalition and the National
    Labor Federation. According to Grandi, this phone network remained active through-
    out the remainder of Allende’s presidency. 23

    Both the telex and the telephone network allowed the command centers to re-
    ceive upward flows of current information from across the country and to disseminate
    government orders back down, bypassing the bureaucracy. Flores assembled a team at
    the presidential palace that would analyze the data sent over the network and compile
    these data into reports. High- ranking members of government used these reports to
    inform their decisions, which Flores’s team then communicated using the telex and
    telephone networks. This arrangement gave the government the ability to make more
    dynamic decisions.

    The Project Cybersyn telex room, housed in the State Development Corporation
    (CORFO), served as the industrial command center during the strike. In addition to
    transmitting the daily production data needed for the Cyberstride software, the CORFO
    telex machines now carried urgent messages about factory production. “There were
    enterprises that reported shortages of fuel,” Espejo recalled. Using the network, those
    in the industrial command center could “distribute this message to the enterprises that
    could help.” 24 The network also enabled the government to address distribution prob-
    lems, such as locating trucks that were available to carry the raw materials and spare
    parts needed to maintain production in Chilean factories, or determining which roads
    remained clear of obstructionist strike activity. Espejo recalled, “The sector committees
    were able to ask the enterprises to send raw materials, transport vehicles, or whatever
    to another enterprise” that needed them. At the same time, enterprises could send re-
    quests to the sector committees and have these requests addressed immediately. “It was
    a very practical thing,” Espejo continued, referring in particular to the state- appointed
    managers known as interventors. “You are the interventor of an enterprise, you are run-
    ning out of fuel, you ask the corresponding sector committee. . . . Or [the interventors]
    know that the raw materials they need are available in Valparaíso and that they need a
    truck to go and get it. With bureaucratic procedures it would have been more difficult
    to resolve these situations.” 25

    [...]

    After the strike, Silva said, “two concepts stayed in our mind: that
    information helps you make decisions and, above all, that it [the telex
    machine] helps you keep a record of this information, which is different from
    making a telephone call. [Having this record] lets you correct your mistakes
    and see why things happened.” Silva added that the energy command center relied
    primarily on the telex network because it gave up- to-

    [...]

    The telex network thus extended the reach of the social network that Flores had
    assembled in the presidential command center and created a sociotechnical network
    in the most literal sense. Moreover, the network connected the vertical command
    of the government to the horizontal activities that were taking place on the shop
    floor. To put it another way, the network offered a communications infrastructure
    to link the revolution from above, led by Allende, to the revolution from below, led
    by Chilean workers and members of grassroots organizations, and helped coordinate
    the activities of both in a time of crisis.

    -- 148-150

## Automation, autonomy and worker participation

    Beer was spinning ideas in “One Year of (Relative) Solitude,” but he was aiming for
    a new technological approach to the worker participation question that would create a
    more democratic and less stratified workplace. And he concluded that giving workers
    control of technology, both its use and its design, could constitute a new form of
    worker empowerment.

    This assertion differed substantially from how other industrial studies of the day
    approached the relationship of computer technology and labor in twentieth- century
    production. Such studies, especially those inspired by Marxist analysis, often presented
    computers and computer- controlled machinery as tools of capital that automated la-
    bor, led to worker deskilling, and gave management greater control of the shop floor.
    In Labor and Monopoly Capital (1974), Harry Braverman credits such machinery “as the
    prime means whereby production may be controlled not by the direct producer but by the owner
    and representatives of capital ” and cites computer technology as routinizing even highly
    skilled professions such as engineering. 53

    [...]

    In the 1950s Norbert Wiener, author of Cybernetics , believed computers would
    usher in a second industrial revolution and lead to the creation of an
    automatic factory. In The Human Use of Human Beings (1954), he worries that
    auto- mated machinery “is the precise economic equivalent of slave labor. Any
    labor which competes with slave labor must accept the economic conditions of
    slave labor.” 56

    -- 159-160

    Two factors explain the difference between Beer and Braverman, who were writing
    at about the same time. First, the computer system Beer designed did not automate
    labor. Given the Popular Unity commitment to raising employment levels, automating
    labor would not have made political sense. Second, Beer was writing and working in a
    different political context than Braverman. The context of Chilean socialism inspired
    Beer and gave him the freedom to envision new forms of worker participation that were
    more substantial than what Braverman saw in the United States. It also allowed Beer
    to see computer technology as something other than an abusive capitalist tool used by
    management to control labor. Beer’s approach also reflected his position as a hired sci-
    ence and technology consultant. His use of technology to address worker participation
    differed from the contemporaneous efforts of the Allende government on this issue,
    efforts that had focused on devising new governing committees within the industrial
    sector and electing worker representatives.

    [...]

    Beer’s proposal bears a close resemblance to the work on participatory design that
    emerged from the social democratic governments in Scandinavia in the 1970s. The
    history of participatory design is often tied to Scandinavian trade union efforts to em-
    power workers during that decade, and thus to create a more equitable power relation-
    ship between labor and capital in Scandinavian factories. 58 These efforts were either
    contemporaneous to Beer’s December report or began several years later, depending on
    historical interpretation. Like the aforementioned automation studies, early participa-
    tory design work viewed technologies such as computer systems as representing the
    interests of management, not labor. However, participatory design used the primacy of
    management as a starting point and then tried to change the dynamics of the labor-
    capital relationship by changing the social practices surrounding the design and use
    of technology.

    -- 161

    Furthermore, appointing worker representatives to control the use of Cybersyn
    would not guarantee that the system would be used in a way that represented the best
    interests of the rank and file. Studies of worker participation have shown that worker
    representatives often separate themselves from their co- workers on the shop floor and
    form a new group of administrators. As Juan Espinosa and Andrew Zimbalist write in
    their study of worker participation in Allende’s Chile, “It has been the historical experi-
    ence, with a few exceptions, that those interpreting workers’ priorities and needs have
    grown apart from the workers they are supposed to represent. . . . [They] become a new
    class of privileged administrators.” 63 Simply put, it would be impossible to give “the
    workers” control of Cybersyn as Beer suggested, even if Chilean workers possessed the
    skills to use the technology or build the factory models.

    Despite these oversights, Beer did realize that the October Strike was a transforma-
    tive event for Chilean workers. Their self- organization and improvisation during the
    strike played a central role in maintaining production, transportation, and distribu-
    tion across the country. During the strike, workers organized to defend their factories
    from paramilitary attacks, retooled their machines to perform new tasks, and set up
    new community networks to distribute essential goods directly to the Chilean people.
    Members of larger industrial belts collaborated with other groups of workers to seize
    private- sector enterprises that had stopped production during the strike. Historian Pe-
    ter Winn notes that during the strike workers came together regardless of politics,
    industrial sector, factory, or status, thus “generating the dynamism, organization, and
    will to stalemate the counterrevolutionary offensive and transform it into an opportu-
    nity for revolutionary advance.” 64 In short, the strike transformed the mindset of the
    Chilean working class and showed that workers could take control of their destiny and
    accelerate the revolutionary process.

    -- 162-163

## Self-organization

    Despite these oversights, Beer did realize that the October Strike was a transforma-
    tive event for Chilean workers. Their self- organization and improvisation during the
    strike played a central role in maintaining production, transportation, and distribu-
    tion across the country. During the strike, workers organized to defend their factories
    from paramilitary attacks, retooled their machines to perform new tasks, and set up
    new community networks to distribute essential goods directly to the Chilean people.
    Members of larger industrial belts collaborated with other groups of workers to seize
    private- sector enterprises that had stopped production during the strike. Historian Pe-
    ter Winn notes that during the strike workers came together regardless of politics,
    industrial sector, factory, or status, thus “generating the dynamism, organization, and
    will to stalemate the counterrevolutionary offensive and transform it into an opportu-
    nity for revolutionary advance.” 64 In short, the strike transformed the mindset of the
    Chilean working class and showed that workers could take control of their destiny and
    accelerate the revolutionary process.

    Although his information was limited, Beer was aware of workers’ activities during
    the strike, and was excited by them. In fact, the ideas he presented in his December
    report, “One Year of (Relative) Solitude,” were designed to support the “people’s auton-
    omy.” Beer wrote, “The new task [outlined in the report] is to try and get all this, plus
    the spontaneous things that I know are happening [such as the cordones industriales ]
    together.” 65 From his perspective, it looked as if Chilean workers were self- organizing
    to keep the larger revolutionary project viable. It is important to stress, especially given
    the criticism he would receive in the months that followed, that Beer viewed his role as
    using science and technology to help support these bottom- up initiatives.

    Although Beer’s take on participatory design was inspired by the events of the Oc-
    tober Strike, it also came from his understandings of cybernetics. “The basic answer of
    cybernetics to the question of how the system should be organized is that it ought to
    organize itself,” Beer writes in the pages of Decision and Control . 66 In his writings Beer of-
    ten cited nature as a complex system that remains viable through its self- organization.
    He argued that such systems do not need to be designed because they already exist. To
    modify the behavior of such a system, one need not control its every aspect but rather
    change one subsystem so that the overall system naturally drifts toward the desired
    goal. Perhaps the injection of worker action could drive Chile toward a new point of
    homeostatic equilibrium, one that was congruent with the overall goal of socialist
    transformation.

    -- 163-164

## Cybernetics

    Increasingly, Cybersyn was becoming a technological project divorced from its
    cybernetic and political origins.  The best- known component of the project,
    the telex network, was not even associ- ated with the overall Cybersyn system,
    let alone with Beer’s ideas about management cybernetics.

    In contrast, members of the core group had become serious students of cybernetics.
    Several months earlier they had formed a small study group known as the Group of
    14 and tasked themselves with learning more about cybernetics and related scientific
    work in psychology, biology, computer science, and information theory. They read the
    work of Warren Weaver, Claude Shannon, Heinz von Foerster, and Herbert Simon and
    invited Chilean biologists Humberto Maturana and Francisco Varela to speak to the
    group (both accepted). Maturana was arguably the first substantial connection between
    Chile and the international cybernetics community. In 1959, while a graduate student
    at Harvard, he had coauthored an important paper, “What the Frog’s Eye Tells the
    Frog’s Brain,” with Warren McCulloch, Jerome Lettvin, and Walter Pitts, all of whom
    were important figures in the growing field of cybernetics. 76

    -- 166

## Cybersyn Goes Public

    These initial press accounts illustrate a finding from science studies research, namely,
    that for a technology to be successful it must be taken up by people other than the in-
    ventors. What Bruno Latour, a sociologist of science, writes of scientific ideas also holds
    true for technologies: “You need them , to make your [scientific] paper a decisive one.” 16
    However, this appropriation creates a dangerous situation. Engineers need others to
    support their technologies so that the technology will be successful, but in the process
    the engineers lose control of their invention. Latour warns, “The total movement . . .
    of a statement, of an artefact, will depend to some extent on your action but to a much
    greater extent on that of a crowd over which you have little control.” 17 As Latour ob-
    serves, others may decide to accept the technology as it is, but they could also dismiss,
    appropriate, or change the technology in fundamental ways.

    -- 177

## Simple technologies

    To these criticisms, Beer responded that the system used simple technologies such
    as telex machines, drew from excellent programming talent in London and Santiago,
    and relied on many “human interfaces,” meaning it was not automated. He also said
    that he was tired of hearing the assertion that such a system could be built only in the
    United States, and stressed that building the futuristic control room required only “the
    managerial acceptance of the idea, plus the will to see it realized.” 18 But, he added, “I
    finally found both the acceptance and the will—on the other side of the world.” 19 This
    final comment was a not- so- subtle jab at his British compatriots, who over the years
    had questioned the legitimacy and feasibility of his cybernetic ideas.

    -- 178

## Necessary instability; power and control

    The comments Espejo, Flores, and Schwember telexed to Beer show that they ob-
    jected to other facets of the speech as drafted. They wrote that, while they agreed
    that cybernetic thinking might help the government increase social stability, they
    also wondered whether instability might be an important part of social progress. “His-
    torical development is a succession of equilibriums and unequilibriums [ sic ],” Espejo
    telexed. Disequilibrium “might be indispensable.” This is an interesting observation,
    although it was not raised as an objection to Cybersyn in subsequent press accounts.
    The Chileans also challenged Beer’s framing of the Chilean revolution as a control
    problem. “The social phenomena goes [ sic ] further than the control problem,” Espejo
    wrote; “there is for instance the problem of power.” If cybernetics looked only at con-
    trol and ignored power relationships, “there is the danger that cybernetics might be
    used for social repression,” Espejo continued, echoing the fears that had already ap-
    peared in the press. Beer responded: “I cannot write the next book in this one lec-
    ture.” 30 But perhaps Beer would have given greater thought to this issue had he known
    that his critics would be most concerned with whether Cybersyn facilitated social
    repression.

    [...]

    Beer writes that “the polarity between centralization and
    decentralization—one masquerading as oppression and the other as freedom—is a
    myth. Even if the homeostatic balance point turns out not to be always
    computable, it surely exists. The poles are two absurdities for any viable
    system, as our own bodies will tell us.” 31 The algedonic, or warning, signals
    that Cybersyn sent to alert higher management constituted a threat to factory
    freedom but it was a necessary one, for not alerting higher management might
    pose a greater threat to system survival. “The body politic cannot sustain the
    risk of autonomic inac- tion any more than we can as human beings,” Beer
    observed. 32 In proposing the idea of effective freedom, Beer was arguing (1)
    that freedom was something that could be calculated and (2) that freedom should
    be quantitatively circumscribed to ensure the stability of the overall system.
    For those who had followed Beer’s work over the years, effective freedom was a
    new term to describe the balance of centralized and decentral- ized control
    that Beer had advocated for more than a decade. It also reflected the same
    principles as Allende’s democratic socialism, which increased state power but
    preserved civil liberties. But for the uninitiated, the claim that a control
    system that explicitly limited freedom actually preserved and promoted freedom
    must have seemed like a political slogan straight out of 1984 . 33

    -- 180-181

    In fact, Hanlon was not alone in recognizing Cybersyn’s potential for
    centralized control. On 1 March Beer telexed to Espejo, “Accusations come from
    Britain and the USA. Invitations [to build comparable systems] come from Brazil
    and South Africa.” Considering the repressive governments that were in power in
    Brazil and South Africa in the early 1970s, it is easy to sympathize with
    Beer’s lament: “You can see what a false position I am in.” 46 Beer was
    understandably frustrated with these international misinterpretations of his
    cybernetic work.

    However, it took little political imagination to see how putting Cybersyn in a differ-
    ent social, political, and organizational context could make the system an instrument
    of centralized control. Beer had tried to embed political values in Cybersyn’s design,
    but he engineered them in the social and organizational aspects of the Cybersyn sys-
    tem, in addition to the technology itself. As safeguards, these social and organizational
    arrangements were not very strong. Archived telexes from the project team show that if
    the Cyberstride software detected a production indicator outside the accepted range of
    values, a member of the National Computer Corporation (ECOM) alerted the affected
    enterprise, those in the central telex room in CORFO, and Espejo in the CORFO infor-
    matics directorate—all at the same time.

    -- 183-184

## Feasibility

    Grosch’s letter to the editor underlines the assumption that industrialized nations,
    such as the United States and the nations of Western Europe, pioneered modern com-
    puter capabilities; nations of the developing world, such as Chile, did not. In his let-
    ter Grosch wrote that Project Cybersyn could not be built in a “strange and primitive
    hardware and software environment,” such as that found in Chile, and in such a short
    time.

    -- 186-187

    For the system to function, human beings also needed to be disciplined and brought
    into line. In the case of Cybersyn, integrating human beings into the system, and thus
    changing their behavior, proved just as difficult as building the telex network or pro-
    gramming the software—or perhaps even more difficult. While the Cybersyn team could
    exert some degree of control over the computer resources, construction of the operations
    room, or installation of a telex machine, they had very little control over what was tak-
    ing place within the factories, including levels of management participation or whether
    Cybersyn would be integrated into existing management practices. Espejo and Benadof
    lacked the authority to force the state- run factories to implement Cybersyn, and indus-
    trial managers remained unconvinced that it warranted their total compliance.

    -- 190

## Conclusions

    This history is a case study for better understanding the multifaceted relationship
    of technology and politics. In particular, I have used this history to address (1) how
    governments have envisioned using computer and communications technologies to
    bring about structural change in society; (2) the ways technologists have tried to em-
    bed political values in the design of technical systems; (3) the challenges associated
    with such efforts; and (4) how studying the relationship of technology and politics
    can reveal the important but often hidden role of technology in history and enhance
    our understanding of historical processes. Forty years later, this little- known story also
    has much to say about the importance of transnational collaboration, technological
    innovation, and the ways in which geopolitics influences technology.

    Computer and communications technologies have often been linked to processes
    of political, economic, and social transformation. But claims that these technologies
    can bring about structural change in society—like the frequent assertion that comput-
    ers will bring democracy or greater social equality—are often made in the absence
    of historical analysis.

    -- 212

    Project Cybersyn is an example of the difficulty of creating a sociotechnical system
    designed to change existing social relationships and power configurations and then
    enforce the new patterns over time. Scientific techniques may conceal biases with a
    veneer of neutrality and thus lead to undesirable results. For example, Allende charged
    the Project Cybersyn team with building a system that supported worker participation.
    Yet the scientific techniques Chilean engineers used to model the state- controlled fac-
    tories resembled Taylorism, a rationalized approach to factory production that disem-
    powered workers and gave management greater control over labor. Time analysis, for
    example, emerged in the context of capitalist production, prioritizing efficiency and
    productivity over other values, such as the quality of shop floor life. By using time-
    analysis techniques, Cybersyn engineers could have inadvertently created production
    relationships that were counter to the Popular Unity platform and then solidified them
    in the form of a computer model.

    Sociotechnical relationships must also remain intact for the system to maintain the
    desired configuration of power. Changing these technical, social, and organizational
    relationships may also change the distribution of power within the system. As I have
    shown, in some cases it is much easier to change a sociotechnical system than to hold it
    static. The history of Project Cybersyn suggests that the interpretation of sociotechnical
    relationships is especially malleable when a system is new, forms part of a controversial
    political project, or requires existing social, technical, and organizational relationships
    to change in substantial ways.

    This malleability makes it extremely difficult to marry a sociotechnical system to a
    specific set of political values, especially if the goal is to create dramatic changes in the
    status quo. In the case of Cybersyn, journalists, scientists, and government officials all

    [...]

    Once separated from the social and organizational relations that Beer imagined,
    the technology of Project Cybersyn could support many different forms of
    government, including totalitarianism. If Project Cybersyn had been implemented
    as Beer imagined, it might have become a system that supported such values as
    democracy, participation, and autonomy. But as its critics perceived, it would
    have been easy to circumvent the technological and organizational safeguards
    the team designed; therefore, it would have been easy for the system to support
    a different set of political values, especially in different social,
    organizational, and geographic settings.  Value- centered design is a
    complicated and challenging endeavor. Even if technolo-

    [...]

    Even if technologists attempt to build certain relationships into the design
    of a technological system, which itself is a fraught and socially negotiated
    process, they have no guarantee that others will adopt the system in the
    desired way—or that they will adopt the system at all.

    -- 215-216

    This history further reveals that different nations have very different experiences
    with computer technology and that these experiences are connected to the political,
    economic, and geographic contexts of these nations. Chilean democratic socialism
    prompted the creation of a computer technology that furthered the specific aims of
    the Chilean revolution and would not have made sense in the United States. The Chil-
    ean context also differed from that of the Soviet Union in fundamental ways. Because
    Chile was significantly smaller than the Soviet Union in its geography, population, and
    industrial output, building a computer system to help regulate the Chilean economy
    was a more manageable affair. In addition, the Soviet solution used computers for cen-
    tralized top- down control and collected a wealth of data about industrial production
    activities with the goal of improving state planning. In contrast, the Cybersyn team
    used Beer’s view of management cybernetics to create a system that emphasized action
    as well as planning; and the system sent limited quantities of information up the gov-
    ernment hierarchy, and tried to maximize factory self- management without sacrificing
    the health of the entire economy. As this contrast shows, technologies are the product
    of the people involved in their creation and the political and economic moments in
    which they are built.

    -- 218

    This particular transnational collaboration sheds light on processes of technologi-
    cal innovation in differently situated world contexts. Project Cybersyn, a case study
    of technological innovation, was a cutting- edge system using technologies that were
    far from the most technologically sophisticated. A network of telex machines trans-
    formed a middle- of- the- road mainframe computer into a new form of economic com-
    munication. Slide projectors presented new visual representations of economic data.
    Hand- drawn graphs showing data collected on a daily basis gave the government a
    macroscopic view of economic activity and identified the areas of the economy most
    in need of attention. Project Cybersyn thus challenges the assumption that advanced
    technologies need to be complex. Sophisticated systems can be built using simple tech-
    nologies, provided that particular attention is paid to how humans interact and the
    ways that technology can change the dynamics of these interactions. Project Cybersyn
    may be a useful example for thinking about sustainable design or the creation of tech-
    nologies for regions of the world with limited resources. 3

    This story of technological innovation also challenges the assumption that innova-
    tion results from private- sector competition in an open marketplace. Disconnection
    from the global marketplace, as occurred in Chile, can also lead to technological in-
    novation and even make it a necessity. This history has shown that the state, as well
    as the private sector, can support innovation. The history of technology also backs this
    finding; for example, in the United States the state played a central role in funding
    high- risk research in important areas such as computing and aviation. However, this
    lesson is often forgotten. As we recover from the effects of a financial crisis, brought
    on in large part by our extraordinary faith in the logic of the free market, it is a lesson
    that is worth remembering.

    -- 219-220

    Geopolitics also shapes our understandings of technological development and tech-
    nological change. If historians, technologists, designers, educators, and policy makers
    continue to give substantial and disproportionate attention to the technologies that
    triumph, a disproportionate number of which were built in the industrial centers of the
    world, they miss seeing the richness of the transnational cross- fertilization that occurs
    outside the industrial centers and the complex ways that people, ideas, and artifacts
    move and evolve in the course of their travels. Technological innovation is the result
    of complex social, political, and economic relationships that span nations and cultures.
    To understand the dynamics of technological development—and perhaps thereby do
    a better job of encouraging it—we must broaden our view of where technological in-
    novation occurs and give greater attention to the areas of the world marginalized by
    these studies in the past.

    -- 221

## Epilogue

    While on Dawson Island, Flores and the other prisoners reflected on their experi-
    ences during the previous three years and, as a group, tried to understand the com-
    plexities of Chilean socialism and what had gone wrong. Flores offered the group a
    cybernetic interpretation of events, which resonated with Allende’s former minister of
    mining, Sergio Bitar. When Bitar published a detailed history of the Allende govern-
    ment in 1986, he used cybernetics to explain in part what happened during Allende’s
    presidency. Bitar writes, “In the present case [the Allende government], systemic variety
    grew because of structural alterations and disturbance of the existing self- regulatory
    mechanisms (principally those of the market). But the directing center (the govern-
    ment) did not expand its variety controls with the necessary speed; nor could it replace
    the existing self- regulatory mechanism with new ones.” Bitar concludes that “when
    a complex system [the Chilean nation] is subject to transformation it is essential to
    master systemic variety at every moment.” 17 This choice of language, seemingly out of
    place in a study of political history, shows that Chile’s encounter with cybernetics not
    only led to the creation of Project Cybersyn but also shaped how some members of the
    Allende government made sense of the history they had lived.

    -- 229

    But the more Flores read, the more he began to see the limitations of cybernetic
    thinking. While Flores still felt that the Law of Requisite Variety and the Viable System
    Model were useful concepts, he believed they were insufficient for the situations he had
    encountered while in Allende’s cabinet. “My problem [in Allende’s cabinet] was not
    variety; my problem was the configuration of reality, persuading other people,” Flores
    said. 20 Understanding the configuration of reality became a driving intellectual pursuit
    for Flores, and he found the work of the Chilean biologists Maturana and Varela espe-
    cially useful toward this end. In addition to developing the theory of autopoiesis with
    Varela, Maturana had conducted extensive work on optics. His 1959 work with Jerry
    Lettvin, Warren McCulloch, and Walter Pitts analyzed the frog’s optical system and
    concluded that what a frog sees is not reality per se but rather a construction assembled
    by the frog’s visual system. What the frog sees is therefore a product of its biological
    structure. This distinction formed the foundation for much of Maturana and Varela’s
    later work in biology and cognition during the 1960s and 1970s, and later inspired the
    two biologists to break with traditional claims of scientific objectivity and emphasize
    the role of the observer. One of Maturana’s best- known claims—“Anything said is said
    by an observer”—illustrates this point. 21

    Flores’s dissatisfaction with cybernetics paralleled a similar dissatisfaction within
    the cybernetics community. Heinz von Foerster, who had worked with Maturana, Va-
    rela, and the Group of 14 in Chile, found it problematic that cybernetics claimed to
    create objective representations of real- world phenomena that were independent of
    an observer. 22 Von Foerster described this approach as “first- order cybernetics,” which
    he defined as “the cybernetics of observed systems.” However, von Foerster was influ-
    enced by Maturana’s work and, like Maturana, became convinced that the observer
    plays a central role in the construction of cybernetic models. In the fall of 1973 von
    Foerester taught a yearlong course at the University of Illinois on the “cybernetics of
    cybernetics,” or what became known as second- order cybernetics, “the cybernetics of
    observing systems.” 23 Although von Foerster was not the only person involved in the
    development of second- order cybernetics, studies of this intellectual transition have
    credited von Foerster for bridging the gap between first- order and second- order cyber-
    netic thinking. 24 Not surprisingly, Flores also took to the idea of second- order cybernet-
    ics, and in his later writing he would cite von Foerster’s edited volume Cybernetics of
    Cybernetics . 25

    [...]

    Flores credits Maturana for leading him to the work of Martin Heidegger. Like Ma-
    turana, Heidegger rejected the existence of an objective external world and saw objects/
    texts as coexisting with their observers/interpreters. Heidegger’s idea of “thrownness”
    also resonated with Flores—the idea that in everyday life we are thrown into the world
    and forced to act without the benefit of reflection, rational planning, or objective as-
    sessment. Looking back, Flores saw his time in the Allende cabinet as an example of
    thrownness rather than rational decision making. “My job was so demanding that I did
    not have the time to perfect [what I was doing]. I only had time to feel it. It was some-
    thing I felt.” 29 In the context of emergency, he had no time to study the laws of control
    laid down by cybernetics in order to determine how best to resolve government crises.
    Flores often had to lead with his gut, and his previous experiences and the traditions of
    Chilean society implicitly shaped his decisions. Flores also realized that “when you are
    minister and you say something, no matter what you say, it has consequences.” 30 It was
    therefore important to use words deliberately. Flores found that management through
    variety control did not allow intuitive forms of decision making, nor did it account for
    the previous experiences and cultural situation of decision makers or accommodate the
    importance of communicating effectively and with intention.

    [...]

    Understanding Computers and Cognition begins by critiquing the rationalist assump-
    tion that an objective, external world exists. The critique builds on the ideas of Hei-
    degger, Searle, Maturana, J. L. Austin, and Hans- Georg Gadamer to show that knowledge
    is the result of interpretation and depends on the past experiences of the interpreter
    and his or her situatedness in tradition. Winograd and Flores then argue that because
    computers lack such experiences and traditions, they cannot replace human beings as
    knowledge makers. “The ideal of an objectively knowledgeable expert must be replaced
    with a recognition of the importance of background,” Winograd and Flores write. “This
    can lead to the design of tools that facilitate a dialog of evolving understanding among
    a knowledgeable community.” 32 Building on this observation, the authors propose that
    computers should not make decisions for us but rather should assist human actions,
    especially human “communicative acts that create requests and commitments that
    serve to link us to others.” 33 Moreover, computer designers should not focus on creating
    an artifact but should view their labors as a form of “ontological design.” Computers
    should reflect who we are and how we interact in the world, as well as shape what we
    can do and who we will become. The American Society for Information Science named

    -- 230-231

    To some he was brusque, intimidating, direct to the point of rudeness, and off-
    putting. Yet his message and his success in both the academic and business
    communities transformed him into a cult figure for others.

    [...]

    “A civil democracy with a market economy is the best political construction so
    far because it allows people to be history makers,” the authors declare. 41
    Flores’s transformation from socialist minister was now complete: he had wholly
    remade himself in the image of neoliberalism.

    Thus, by the end of the 1990s, Flores and Beer had switched places. Flores had
    morphed into a wealthy international consultant driven by the conviction that orga-
    nization, communication, and action all were central to making businesses successful.
    Meanwhile, Beer had become increasingly interested in societal problems and chang-
    ing the world for the better. His last book, Beyond Dispute (1994), proposed a new
    method for problem solving based on the geometric configurations of the icosahedron,
    a polygon with twenty equilateral triangle faces. He called this new method “synteg-
    rity” and argued that it could serve as a new approach to conflict resolution in areas of
    the world such as the Middle East.

    -- 232-233

## Misc

    The strike also had the effect of radicalizing factions of the left,
    some of which began preparing for armed conflict. Political scientist
    Arturo Valenzuela notes: “ironically, it was the counter-mobilization
    of the petite bourgeoisie responding to real, contrived, and imaginary
    threats which finally engendered, in dialectical fashion, a significant
    and autonomous mobilization of the working class.”18 Rather than
    bringing an end to Chilean socialism, the strike pitted workers against
    small-business owners and members of the industrial bourgeoisie and
    created the class war that the right openly feared.

    [...]

    The solution he proposed was social and technical, as it configured
    machines and human beings in a way that could help the government adapt
    and survive.

    [...]

    Accusations come from Britain and the USA. Invitations [to build
    comparable systems] come from Brazil and South Africa.” Considering the
    repressive governments that were in power in Brazil and South Africa in
    the early 1970s, it is easy to sympathize with Beer’s lament: “You can
    see what a false position I am in.”46 Beer was understandably
    frustrated with these international misinterpretations of his
    cybernetic work.

    [...]

    This Government is shit, but it is my Government.’ ”51

    [...]

    The big problem was “not technology, it was not the computer, it was
    [the] people,” he concluded.70 Cybersyn, a sociotechnical system,
    depended on more than its hardware and software components. For the
    system to function, human beings also needed to be disciplined and
    brought into line. In the case of Cybersyn, integrating human beings
    into the system, and thus changing their behavior, proved just as
    difficult as building the telex network or programming the software—or
    perhaps even more difficult. While the Cybersyn team could exert some
    degree of control over the computer resources, construction of the
    operations room, or installation of a telex machine, they had very
    little control over what was taking place within the factories,
    including levels of management participation or whether Cybersyn would
    be integrated into existing

    [...]

    Beer, however, recognized the real possibility of a military coup. In
    his letter to the editor of Science for People, he considered whether
    Cybersyn might be altered by an “evil dictator” and used against the
    workers. Since Cybersyn team members were educating the Chilean people
    about such risks, he argued, the people could later sabotage these
    efforts. “Maybe even the dictator himself can be undermined; because
    ‘information constitutes control’—and if the people understand that
    they may defeat even the dictator’s guns,” Beer mused.79 I have found
    no evidence that members of the Cybersyn team were educating Chilean
    workers about the risks of using Cybersyn, although they might have
    been.

    [...]

    after the Pinochet military coup, information in Chile did constitute
    control but in a very different way than Beer imagined. The military
    created the Department of National Intelligence (DINA), an organization
    that used the information it gleaned from torture and surveillance to
    detain and “disappear” those the military government viewed as
    subversive

    [...]

    The cybernetic adventure is apparently coming to an end, or is it not?”
    Kohn asked. “The original objective of this project was to present new
    tools for management, but primarily to bring about a substantial change
    in the traditional practice of management.” In contrast, Kohn found
    that “management accepts your tools, but just them. . . . The final
    objective, ‘the revolution in management’ is not accepted, not even

    [...]

    Decybernation,” a reference to the technological components of Cybersyn
    that were being used independent of the cybernetic commitment to
    changing government organization. Beer wrote, “If we want a new system
    of government, we have to change the established order,” yet to change
    the established order required changing the very organization of the
    Chilean government. Beer reminded team members that they had created
    Cybersyn to support such organizational changes. Reduced to its
    component technologies, Cybersyn was “no longer a viable system but a
    collection of parts.” These parts could be assimilated into the current
    government system, but then “we do not get a new system of government,
    but an old system of government with some new tools. . . . These tools
    are not the tools we invented,” Beer wrote.81

    [...]

    Decybernation” was influenced by the ideas of the Chilean biologists
    Humberto Maturana and Francisco Varela. Understanding the import of
    Beer’s insistence on organizational change requires a brief explanation
    of how Maturana and Varela differentiated between organization and
    structure. According to the biologists, the “structure of a system”
    refers to its specific components and the relationships among these
    components. The “organization of the system” refers to the
    relationships that make the system what it is, regardless of its
    specific component parts. The structure of the system can change
    without changing the identity of the system, but if the organization of
    the system changes, the system becomes something else. In their 1987
    book The Tree of Knowledge

    [...]

    On 5 May the violent actions of the ultraright paramilitary group
    Fatherland and Liberty pushed the government to declare Santiago an
    emergency zone. Placing the city under martial law, Allende accused the
    opposition of “consciously and sinisterly creating the conditions to
    drag the country toward civil war.”92 The escalating conflict between
    the government and the opposition did not bode well for the future of
    Chilean socialism.

    [...]

    Marx, capital was evil and the enemy. For us, capital remains evil, but
    the enemy is STATUS QUO. . . . I consider that if Marx were alive
    today, he would have found the new enemy that I recognize in my
    title.”101 In “Status Quo” Beer used cybernetics to explore some of
    Marx’s more famous ideas and to update them for the modern world,
    taking into account new technological advances in communication and
    computing. According to Beer, the class struggle described by Marx was
    out of date and “represent[ed] the situation generated by the
    industrial revolution itself, and [was] ‘100 years old.’ ”102 Beer felt
    that capitalism had since created new forms of work and new
    exploitative relations.103

    [...]

    Bureaucracy always favors the status quo,” he argues, “because its own
    viability is at stake as an integral system.” In order to survive,
    bureaucracy must reproduce itself, Beer claimed. This process
    constrains freedom in the short term and prevents change in the long
    term.109 “This situation is a social evil,” Beer asserts. “It means
    that bureaucracy is a growing parasite on the body politic, that
    personal freedoms are usurped in the service demands the parasitic
    monster makes, and above all that half the national effort is deflected
    from worthwhile activities.” Beer concludes that since bureaucracy
    locks us into the status quo, “dismantling the bureaucracy can only be
    a revolutionary aim.”110 Beer had long railed against bureaucracy

    [...]

    Nevertheless, Beer’s cybernetic analysis failed to tell him how to
    advise his Chilean friends and help them save Chile’s political
    project. In fact, it led him to the opposite conclusion: that it was
    impossible for a small socialist country to survive within a capitalist
    world system. “If the final level

    [...]

    societary recursion is capitalistic, in what sense can a lower level of
    recursion become socialist?” he asks. “It makes little difference if
    capital in that socialist country is owned by capitalists whose subject
    is state controls, or by the state itself in the name of the people,
    since the power of capital to oppress is effectively wielded by the
    metasystem.”112 Or, to put it another way, Beer did not see how the
    Allende government could survive, given the magnitude of the economic
    pressure that a superpower like the United States was putting on the
    small country. But Beer continued to work for the Allende government
    even after he reached this conclusion, because his personal and
    professional investment in Chilean socialism outweighed the pessimistic
    judgment of cybernetics.113
