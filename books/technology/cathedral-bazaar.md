[[!meta title="The Cathedral & The Bazaar"]]
[[!tag jogo software foss economics]]

* [The Cathedral and the Bazaar](http://www.catb.org/~esr/writings/cathedral-bazaar/)
* Author: Eric S. Raymond
* ISBN: 978-0-596-00108-7
* Publisher: O'Reilly

## Review

While Raymond has innumerous insights on the dynamic of the free software
communities, he got political economy wrong, including, but not only by:

* Choosing to focus on Lockean philosophical considerations.
* Putting altruism as a mode of appearance for an egotistical reward strategy.

Reading this book years after the "Open Source Revolution" has begun, the whole
"Open Source X Free Software" debate looks even more important than what sometimes
were put as a metaphysical, esoteric dispute. Going beyond the requirement that
a software work is made available giving the "four freedoms", this debate puts
basic questions about the underlying production process all how societies chose
to divide labour and share wealth.

More than ever before this debate has huge importance and implications, given
our current state of affairs where economic models such as "freemium",
"opencore" and [siren servers](/books/sociedade/who-owns-the-future) are
privatizing and concentrating the notion of property, i.e, transforming even
personal property in a "service": you don't own your gadget or content like
music you purchase, because of DRM, EULAS and the inability to repair your
stuff, see the iRepair movement.

Raymond assumes that "the verdict of history seems to be free-market capitalism
is the globally optimal way to cooperate for economic efficiency" which, besides
being an "end of history"-type fallacy -- as we didn't tried yet many, many
possible economical systems, but only very few --, has wrong assumptions about
what is "optimal", "cooperation" and "efficient": just look about resource
depletion, absurd wealth concentration by the extremely rich and lack of
basic dignity for most of human population, not to mention animal/nature rights.

Capitalism is based in the need that something is scarce, if not naturally then
let make it artificial scarce. So there's no way a capitalist business will
sustain itself by giving everything free as in software -- as it's anyway out
of question that it will give anything free as in beer.

So while the bulk of Raymond ideas are revolutionary in the sense that capitalism
needs to constantly revolutionize itself, fade away in diminishing returns or
go to war mode -- when accumulated production is fanatically destroyed, it does
not offers insights for the main issue of how to replace capitalism which by
the previous definition is inequality-producing machine.

Embracing open source as a capitalism moto sounds like being a hacker until
page two, which is a prevailing ideology of the Silicon Valley elite, which
sounds much more a meritocracy than hacker culture. We should question things
instead of taking assumptions for granted.

That's curious, because Raymond cites Buckminster's Fuller "ephemeralization"
concept in the opening words of his "The Magic Cauldron" essay, which could
be explored to a new dimension if economics and politics are understood also
as technological apparatus we use to live better. An ephemeralization, as
Raymond explains, is "technology becoming both more effective and less
expensive as the physical resources invested in early designs are replaced
by more and more information content" (page 115).

So it's clear when Raymond makes assumptions he is actually making a choice on
capitalist markets and conservative politics (I don't like to use the term
libertarian: it causes confusions as it means different things on different
cultures).

If we change the assumptions, we can build different, new economies and
politics with other different emergent properties, like those based on values
of emancipation and solidarity. There are other Magic Cauldrons for the Free
Software spell.

## Ideas while reading the book

* Hypothesis: sustainability of "Open Source" economic model in Brazil was mostly embraced
  by the government, by an army of free lancers and by a small number of business; while
  open source is widely used in the country, it's mostly on the free rider mode: everyone
  using an open stack but develops unpublished code (either closed source os lazilly left
  out of public sight) or "poor gifts" in the expression of Raymond himself.

## Phenomenology

* Linus Law: "Given enough eyeballs, all bugs are shallow" (page 30);
  "debugging is parallelizable" (page 32).

* Delphi Effect: "the averaged opinion of a mass of equally expert (or equally
  ignorant) observers is quite a bit more reliable a predictor than the opinion
  of a single randomly chosen observer" (page 31).

* Brooks Law: "complexity and communication costs of a project rise with the
  square number of developers" (pages 32, 49).

## Freedom and hierarchy

* Kropotkin is cited at page 52: "principle of understanding" versus the
  "principle of command".

* Conservative vision: "The Linux world behaves in many respects like a free
  market or an ecology, a collection of selfish agents attempting to maximize
  utility, which in the process produces a self-correcting spontaneous order
  more elaborate and efficient than any amount of central planning could have
  achieved." (page 52). Right afterwards he negates the existence of true
  altruism.

## Economics

A very liberal point of view:

* Homesteading the Noosphere: "customs that regulate the ownership and control
  of open-source software [...] imply an underlying theory of property rights
  homologous to the Lockean theory of land tenure" (65).

* Open Source as a gift economy like a reputation game (81 - 83):

    Most ways humans have of organizing are adaptations to scarcity
    and want. Each way carries with it different ways of gaining social status.

    The simples way is the _command hierarchy_ [where] scarce goods are allocated
    by onde central authority and backed up by force. Command hierarchies scale
    very poorly; they become increasingly inefficient as they get larger.

    [...]

    Our society is predominantly an exchange economy. This is a sofisticated
    adaptation to scarcity that, unlike the command model, scales quite well.
    Allocation of scarce goods is done in a decentralized way through trade
    and voluntary coopreation.

    [...]

    Gift cultures are adaptations not to scarcity but to abundance. They arise
    in populations that do not have significant material scarcity problems
    with survival goods.

    [...]

    Abundance makes command relationships difficult to sustain and exchange
    relationships an almost pointless game. In gift cultures, social status
    is determined not by what you control but by _what you give away_.

    -- 80-81

He also explains that the reputation game is not the only drive in the
bazaar-style ecosystem: satisfaction, love, the "joy of craftsmanship" are also
motivations for software development (pages 82-83), which is compatible
with the gift economy model:

    How can one maximize quality if there is no metric for quality?
    If scarcity economics doesn't operate, what metrics are available
    besides peer evaluation?

    Other respondents related peer-esteem rewards and the joy of hacking
    to the levels above subsistence needs in Abraham Maslow's well-known
    'hierachy of values' model of human motivation.

    -- 82-83

Cites both Ayn Rand and Nietzsche at page 88 when talking about "selfless"
motives, besides their "whatever other failings", saying that both
are "desconstructing" 'altruism' into unacknowledged kinds of self-interest.

## The value of humility

    Furthermore, past bugs are not automatically held against a developer; the fact
    that a bug has been fixed is generally considered more importante than the fact
    that one used to be there. As one respontend observed, one can gain status by
    fixing 'Emacs bugs', but not by fixing 'Richard Stallman's bugs' -- and it
    would be considered extremely bad form to criticie Stallman for _old_ Emacs
    bugs that have since been fixed.
    
    This makes an interesting contrast with many parts of academia, in which
    trashing putatively defective work by others is an important mode of gaining
    reputation. In the hacker culture, such behavior is rather heavily tabooed --
    so heavily, in fact, that the absence of such behavior did no present itself to
    me as a datum until that one respondent with an unusual perdpective pointed it
    out nearly a full year after this essay was first published!

    The taboo against attacks on competence (not shared with academia) is even more
    revealing than the (shared) taboo on posturing, because we can relate it to a
    difference between academia and hackerdom in their communications and support
    structures.
    
    The hacker culture's medium of gifting is intangible, its communications
    channels are poor at expressing emotional nuance, and face-to-face contact
    among its members is the exception rather than the rule. This gives it a lower
    tolerance of noise than most other gift cultures, and goes a long way to
    explain both the taboo against posturing and the taboo against attacks on
    competence. Any significant incidence of flames over hackers' competence would
    intolerably disrupt the culture's reputation scoreboard.

    -- 90-91

What about Linus behavior, then?

    The same vulnerability to noise explains the model of public humility required
    of the hacker community's tribal elders. They must be seen to be free of boast
    and posturing so the taboo against dangerous noise will hold.
    
    Talking softly is also functional if one aspires to be a maintainer of a
    successful project; one must convince the community that one has good
    judgement, because most of the maintainer's job is going to be judging other
    people's code. Who would be inclined to contribute work to someone who clearly
    can't judge the quality of their own code, or whose behavior suggests they will
    attempt to unfairly hog the reputation return from the project? Potential
    contributors want project leaders with enough humility and class to be able to
    to say, when objectively appropriate, ``Yes, that does work better than my
    version, I'll use it''—and to give credit where credit is due.

    -- 91

## References

* [Homesteading the Noosphere](http://www.catb.org/~esr/writings/cathedral-bazaar/).
