[[!meta title="Maciunas Learning Machines"]]

[Maciunas’s Learning Machine](http://georgemaciunas.com/exhibitions/knowledge-as-art-chance-computability-and-improving-education-thomas-bayes-alan-turing-george-maciunas/george-maciunas/maciunas-learning-machine/).

## Index

* Mendeleev, 66.
* Shelf system, 72.

## Impressum

* Had a short epiphany while reading about revesible historical maps and diagrams.
  As those physics short movies played backward to discuss about the arrow of time.

* Could see the [cardgame](https://baralho.fluxo.info) as another kind of learning
  machine.

## Snippets

### Fluxus

    Maciunas chose this term as the title of an anthology which was never actually
    published as originally in- tended. 6 There was no copyright on the word
    Fluxus. 7 It was taken from the Latin verb, fluere, meaning to flow, stream, or
    to be in a state of flux, and had exactly the right dynamic associations to be
    able to bring together widely different positions. The word Fluxus draws
    attention to the essentially fluid and open-ended nature of art.
    Notwithstanding the ono- matopoeia, the lexical ambiguity of “Fluxus” made this
    avant-garde movement sound quasi- scientific and old-fashioned. Maciunas the
    impresario sought to coordinate all the various artistic activities so that
    Fluxus would be more than just a “new wave” (Maciunas) which came and went like
    so many other -isms, but would instead grow into a worldwide art “cur- rent”
    that would drown out all other artistic movements.

    It was in the 1960s that Fluxus took root as a label for an international
    avant-garde movement active in the border zone between art and non-art. Fluxus
    had less to do with an art theory than with a specific practice aimed at the
    trivialization of the aesthetic, and the infiltration of art by the everyday.
    With Dick Higgins, Fluxus can be defined as the intersection of art and life.
    Nor were its practitioners restricted to any particular medium. Fluxus was a
    hybrid form or intermedia, and so it could be music, performance, dance, and
    literature at the same time.

    Fluxus is often described as a mindset, very much according to the maxim: Fluxus is not
    something you do, it is something you are. 8 Yet attitude alone was never enough for Fluxus
    to leave its stamp on the history of art. History is made by active intervention and is written
    about only after pondering what has happened. Maciunas did both of these. Thanks to his
    background in graphic design, he was constantly busy with the publication of at times im-
    provised, at times professionally designed promotional materials. Right from the start,
    Maciunas sought to confer on Fluxus the status of the historic—not from without and not
    after the event, but from the very heart of the happening. This alone made him the group’s
    central figure.

    -- 9

### The Interactive User

    The declared aim was “to learn as if mechanically and without having to think
    too much.” 38

    [...]

    The idea of the interactive user was born. George Maciunas is one of them.

    [...]

    This interest in graphic forms of communi- cation can in turn be traced back to
    Maciunas’ profound aversion to books. Instead of spending hours of his time
    reading, he preferred to learn by taking in as much informa- tion as possible
    at a glance. This explains his fascination with diagrams, charts, maps, tables,
    systems of coordinates, and graphs. The charting of history, moreover, was but
    one facet of the visual information which was to preoccupy him throughout his
    life, not just as an architect, but as a knowledge worker.

### Chartjunk

    Maciunas used a limited set of symbols to specify history’s spatial dimension: circles to in-
    dicate places and asterisks to indicate battlefields and the destinations of expeditions, etc.
    This kind of geohistorical infrastructure on maps is subject to what George Kubler, in his
    1962 book Shape of Time, described as the iconic “reduction of knowledge.” There is no
    “chartjunk”—to borrow Edward Tufte’s concept—in Maciunas’ diagrams. 64 The elementary
    data and features which help to whittle down the sheer bulk of information to a manageable
    size are not themselves illustrative; they rather generate an epistemic field of operation
    based on consistency and visual homogeneity. The degree of knowledge contained in gen-
    eral maps of this nature is measurable by the precision of the graphic information provided
    and not by criteria for the aesthetic representation of space, whatever they might be.

### Transition from Atlas to Diagram

    It was to expose these different interpretations that Maciunas switched, as already men-
    tioned, his representation of history to chronological mode. The atlas of history leveled the
    time factor for a large geographical region and was thus faced with the insoluble problem of
    describing historical processes simultaneously. It was therefore replaced by the history
    chart. The descriptive map was superseded by the abstract diagram, analogue representa-
    tion by chronographic organization. The new design matrix was no longer an area, but
    rather a vertical timeline. For his diagrams, Maciunas preferred to use lined writing paper
    with a single vertical on the left margin. The lines provided a basic two-dimensional system
    of coordinates, which in turn served as the basic structure of a time-space chart (pl. 2). 66
    Historiographic space, which had taken up so much room in the atlas, was thus subordi-
    nated to a synchronoptical ordering principle.

    The transition from atlas to diagram was tantamount to a paradigm shift. The categories
    would henceforth be weighted by time rather than place. In purely formal terms, this shift
    from juxtaposition in space to consecutive presentation in time is borne out by Maciunas’
    switch from landscape format, with its implications of a geographical horizon, to portrait
    format, in which the crucial point of reference is the vertical time axis. This paradigm shift
    is highlighted by the extended title: (Space-Time) Atlas of Russian History (see illustration on
    p. 23). Maciunas typed the text, like all his later Fluxus publications, on an IBM Executive
    typewriter in the remarkably light sans serif font News Gothic and pasted what he had
    typed into the Atlas by way of a title page.

### Linear timescales

Both Barbeu-Dubourg and Maciunas still used a linear historic timescale but that already 
allowed a non-linear reading.

    The account of Russian history related in the various chronological charts which Maciunas
    produced in the course of his studies is the same as that told by the maps in the Atlas, or at
    least takes that version as its starting point. The approach naturally differs from chart to
    chart. The truncating of historiographic space in Maciunas’ oeuvre of Chronologies (in all
    covering periods between 867 and 1950) serves to heighten the drama of linear progress,
    which after all can be evoked only with the aid of a timeline. The predominance of time thus
    has the effect of turning the spotlight on the developmental paradigm. Even more impor-
    tant than territorial shifts on a grand scale are differences in the speed of change and mi-
    crohistorical processes. The conception of history in the Atlas is a political one. There are no
    maps in which Maciunas depicted the history of religion, art, business, law, or statecraft.
    These aspects therefore have to be left to his history diagrams.

### Atlases as an Enlightenment Ideology

    The great increase in the popularity of historical atlases in the nineteenth century would
    be inconceivable without the emergence of the nation state and the pursuit of aggressively
    expansionist and imperialist policies. 68 The history of the empire was to inform maps of
    the empire. The political function of the atlas of history was thus very similar to that of
    history painting. Its purpose was not so much to deliver comfort and relief—which was
    what history paintings had to do—as to nurture historical awareness. Such awareness as
    the basis for social development, however, was to be found only at the top of the learning
    curve that was preceded and facilitated by the positivistic acquisition of facts. To para-
    phrase Jürgen Habermas, social evolution is driven by changes in the knowledge poten-
    tial. 69 The historical sources show a milieu which believed in the reformation—meaning
    the improvement—of the world by education. Maciunas’ maps are of a piece with this en-
    lightenment ideology. As an imaginative matrix, they do not deliver an abstract model of
    history, but rather generate their own history—one whose narrative strategies elude any
    direct empirical verification. This metahistory is ideologically motivated. As the factual
    density increases, so the process of historical change picks up speed, culminating in the
    Russian Revolution. Maciunas’ mapping project was focused on that one event, an event
    which exemplifies most vividly the feasibility of history, which in turn allows for the idea
    that society can indeed be modeled.

    --- 29

### Making Capitalism History

    The longer Maciunas lived in the West, the stron- ger his ideological ties to
    the East that supplied the raw material for his emphatically social
    self-perception became. In the USA, the “pro-Soviet Lithuanian” in his own way
    turned out to be a “Russianist,” a “peculiar kind of Marxist-Leninist.” 77

    Articulating the specific relevance of history to the Fluxus movement, Maciunas did indeed
    trace its intellectual roots back to the history of Russia and the USSR. The link exposed
    aspects of the past which Maciunas tried to bring to bear on his own present, even using
    Fluxus to render history accessible as the source of an alternative future. It might be argued
    that there were at least three theses to be drawn from history: first, the thesis that Fluxus
    was in fact a continuation of the Russian Revolution with other means; second the thesis
    that avant-garde interventionism can bring about social change; and third, the thesis that
    art has a potential for cultural transgression in the sense of “spatial practice.” The word
    “thesis” is used deliberately on the grounds that Maciunas never elaborated any coherent
    theory of the avant-garde movement which he initiated. Fluxus did not simulate profundity.
    As in Zen, artistic practice was more important than abstract problems. Yet Fluxus was still
    conceptualized as a vanguard of social change resting on assumptions derived from history
    that could be developed into theories. The conceptual tool for this reflective turn on history
    was that provided by the Atlas and the Chronologies, which taken together prepared the
    ground for Maciunas’ avant-garde concept to be incorporated in discourse on cultural and
    intellectual history. The historiographic connection at the same time provided a form of
    ideological backup. Cartography and diagrams also proved to be an important instrument
    for opening up both spatial and mental scope.

    -- 34

    What Maciunas was striving for with Fluxus was the gradual elimination of the fine arts.
    His efforts were directed against the monadic work of art as a commercial entity. When he
    preached anti-individualism to his fellow artists, the anonymity of the collective he held
    up as a model was that of the Soviet kolkhoz. His purpose was to break down not only the
    traditional notion of the “work” with which modernism had identified, but all its economic
    aspects as well. 84 Being an artist was not a socially acceptable profession, but rather a
    purely voluntary sideline, he argued. True Fluxus artists therefore ought to earn their liv-
    ing with a regular nine-to-five job, which on top of everything else had to be just as socially
    constructive and useful as all other Fluxus activities. 85 Unlike the early Marx, who in his
    libertarian phase considered literary criticism in the evening as more important than
    drudgery during the day, Maciunas thought very little of immaterial work. As mentioned
    above, he was no friend of theory and intellectual art. What impressed him were rather
    disciplined revolutionaries such as Cuban leader Fidel Castro, who ran a country by day,
    and wrote propagandistic speeches by night. According to this model, Fluxus was to be
    pursued during the evening hours, or between five and ten to be exact, Maciunas having
    decreed that the hours between midnight and eight in the morning were for sleep. 86 This,
    in other words, was how his Fluxus-style planned economy looked. As noble as the inten-
    tions behind Maciunas’ doctrine were, the avant-garde attempt to turn “art into life” did
    not lead to the assimilation of the aesthetic, but instead merely paved the way to new art. 87
    Despite the vehemence of his critique, Maciunas was not in fact able to change this. On the
    contrary, it seems that he provoked just the opposite.

    -- 36

### Beuys and panta rhei

    The personal differences between the two artists [Maciunas and Beuys] were later
    to become even more pronounced in their conflicting perceptions of Fluxus. 134 Because of
    this, Beuys never really belonged to Maciunas’ inner circle. Working collectively was some-
    thing he rejected out of hand, as he was simply too “independent” (pl. 16), too much of a
    charismatic lone fighter striving for what we now know was to be a hugely successful solo
    career. 135 But he liked the name “Fluxus,” inspired by Heraclitus’ panta rhei, and in October
    1963, eight months after the Festum Fluxorum, used it for his second exhibition in the stables
    at the Van der Grinten farmhouse in Kranenburg. Furthermore, nearly one in ten of the
    drawings, small objects, and three-dimensional pictures listed in the Beuys catalogue for

    -- 49

### Monomorphic and polymorphic approaches

    The groundwork that had to be done for the process of informatization ahead entailed mak-
    ing several sketches as well as copious written notes. The first layout was penned onto the
    back of one such note, whose recto bears the letterhead of the Film-Maker’s Distribution
    Center (pl. 18). Lined up next to each other so as to imply absolute parity, the artists are
    grouped under two separate titles with “Happenings” on one side and “Fluxus” on the other.
    The incompatibility of the two implied by this dichotomy is reinforced by the distinction
    drawn between polymorphism and monomorphism. Happenings are “polymorphic,” mean-
    ing that many things happen at the same time. 203 Fluxus, on the other hand, is “monomor-
    phic” inasmuch as like Zen, it attempts to focus on one thing at a time—an enlightenment
    practice for which Maciunas had developed a strong affinity through his work on the Atlas
    of Prehistoric Chinese Art (1958). 204 This monomorphic approach gives rise to gag-like events,
    and even to good gags. Humor in all its many permutations was always an important crite-
    rion for Fluxus.

    -- 60

### Synchronic actions and dyachronic sequence

    Depending on the direction in which the information is combined,
    the result is either a simultaneous or a successive image of its history. Synchronic actions
    and a diachronic sequence of events are the two narrative styles in which history is related,
    even if the chronological account of Fluxus’ artistic activities is more consistent than that
    of the parallel happenings. Citing exact dates, the chart for the first time tells us who initi-
    ated what, when, and where.

    -- 63

### Genealogy and ancestrality

    Others have even gone so far as to describe it as the last great avant-garde of
    the twentieth century, although Maciunas himself preferred the pug- nacious
    notion of a “rear-garde.” 196

    -- 59

    Maciunas’ genealogy of Fluxus was less an attempt at historiographical hegemony than the
    product of his desire to put artistic self-historization in the foreground. The quest for his
    own identity takes Maciunas back to his roots.

    [...]

    Fluxus was heir to a number of early-twentieth-century avant-gardes and hence regarded
    itself as heir to a subversive counterculture. Stephen Foster even went so far as to claim that
    Fluxus was basically a reconfiguration of the avant-garde paradigm, by which he meant that
    it could reject the historical manifestation of modernism, but not its history. It followed
    that although Maciunas was indubitably anti-art, anti-establishment, and anti-commercial
    culture, he was definitely not anti-history. 220 Foster, however, failed to take account of the
    fact that even the historical avant-gardes had had to operate with models of history, de-
    spite—or even because of—their claim to be breaking with the past. Genealogy regulates
    the balance between continuity and innovation. Criticism of the new is softened by the le-
    gitimizing power of ancestry and origin. 221 Artists secure their position in history through
    tradition. To become a subject of history at all, every avant-garde movement both past and
    present has to accept the existence of antecedents.

    -- 62

    Foster describes the relationship between Fluxus and its historical models as ambivalent.
    According to him, Fluxus had been happy enough to be included in the avant-garde canon,
    but at the same time had fought energetically to distance itself from it. Being attracted to
    and repelled by art history in equal measure, Fluxus had developed what was essentially a
    two-pronged operational strategy. While by and large rejecting modernism, it nevertheless
    acknowledged the overarching authority of history, and hence its own place in history. This
    is why Maciunas deliberately and repeatedly attempted to prove that Fluxus had its own
    history. 222 While Foster’s thesis is not unfounded, at least with regard to the transhistorical
    quality of the Fluxus myth, its relevance to Maciunas’ diagrams, if it has any at all, extends
    only as far as the “optical unconscious.” According to Rosalind Krauss, the “optical uncon-
    scious” is that latent pictorial content which is not apparent at first glance, but which as a
    message implicit in the work must still be included in any reflection on the same. 223 Besides,
    as the Big Chart clearly illustrates, the bonds of kinship are almost impossible to sever, even
    if Fluxus preferred to engage only with certain aspects of the work of its direct progenitors.
    Maciunas’ own visual strategy aimed to bridge the gap between past and present, without
    any ifs and buts.

    -- 63

### Mapping knowledge

    Compared with many fellow artists of his generation, Maciunas had a very broad education.
    He studied art, graphic design, and architecture at the Cooper Union School of Art in New
    York (1949–1952), architecture and musicology at the Carnegie Institute of Technology in
    Pittsburgh (1952–1954), and gained a thorough grounding in art history at the Institute of
    Fine Arts at New York University (1955–1960). As was the norm for American students at the
    time, Maciunas used his own diagrams and lists of facts and figures to give himself a better
    sense of the larger picture. 226 For Maciunas as graphic designer, the written word in diagram-
    matic form became a key motif. He was also an exceptional “learning machine” himself.
    Throughout his studies, he repeatedly ventured into languages, logic, psychology, and even
    physiology. His broad range of interests and universal approach required appropriate forms of
    knowledge management to enable him to keep track of this plethora of material. The diagram
    seemed ideal inasmuch as it lent itself to the ordering of facts, the reduction of complexity,
    and the accrual of meaning. With the diagram, the ever expanding field of knowledge could
    be made manageable within a limited time, as Maciunas recalled when he looked back at his
    student years in his “Preliminary Proposal for a 3-Dimensional System of Information Stor-
    age and Presentation.” 227 In this short text, written in the late 1960s, Maciunas condemned
    the inefficiency of the American education system, which for him was a result of premature
    specialization and the fragmentation of knowledge. Maciunas knew what he was talking
    about. After all, he had spent eleven years, and hence much longer than average, at college and
    had gained first-hand experience of several educational establishments of renown. He sum-
    marized his proposals for improvements in the Learning Machine of 1969 (pl. 21).

    Maciunas used this chart-like taxonomy with its easily comprehensible structure to excori-
    ate the slow, linear-narrative method of traditional sources of information such as books,
    lectures, television, and movies, as well as of newer computer technology. As one who did not
    really enjoy reading books, Maciunas replaced text-based knowledge transfer with a visual sys-
    tem of information that allowed a wealth of information to be grasped simultaneously. What
    he had in mind was a three-dimensional diagram whose main advantage would be its provision
    of a time-saving overview. Even if Maciunas’ Learning Machine, conceived as an all-inclusive
    view of all areas of knowledge, never progressed beyond a two-dimensional tree of categories,
    its interdisciplinary frame of reference nevertheless provided a binding blueprint for future
    educational programs. The purpose of networked thinking was to prevent the wood from being
    obscured by the trees. Maciunas believed that this was the only way to overcome the blinkered
    vision caused by specialization. Students with a broad education should be offered a four-year
    curriculum which would allow them to specialize only gradually. As part of these reform pro-
    posals, Maciunas also advocated interdepartmental seminars from which the art major pro-
    gram in particular would greatly benefit. Otherwise, or so he feared, the chances of survival for
    budding professional artists would sink drastically. The gravity of the situation was expressed
    in Maciunas’ Curriculum Plan (c. 1968–1969) with a typographic SOS (pl. 22). From Maciunas’
    point of view, it was high time the communication of knowledge was revolutionized.

    -- 65

### Net-works

    The net is an ideal metaphor for the interrelatedness of all things. With its nodes, connect-
    ing lines, and points of intersection, it forms a complex system of relationships. The net-
    work, meanwhile, is a tool for making this figure of speech an artistic reality. As a medium
    of action, it is predicated on participation. It demands the involvement of a collective in a
    highly complex process of exchange. The avant-garde was quick to appropriate this transfer
    principle and in the course of its own globalization applied it—as a migration effect, as it
    were—to the formation of an international network. What Hubert van den Berg has so fit-
    tingly described as the “supranationality of the avant-garde” is especially true of Fluxus. 232
    Yet these global nets are not pursued indiscriminately; instead, they are subject to a norma-
    tive selection procedure, of which Maciunas was an early and exemplary practitioner.

    [...]

    Global collaboration, such as the quasi-synchronous networking that has been
    practiced by artists using computer- assisted networks since the 1980s, was
    something that Maciunas had sought to initiate in countless letters to friends
    and fellow artists as early as the 1960s. 236 It was an undertaking for which
    he might have been inspired by the New York Correspondence School, the world’s
    first mail art network founded by Ray Johnson in 1962. To judge by Maciunas’
    own corre- spondence, he collected contacts with fellow artists the way other
    people collect stamps.  These network communications in the form of
    cross-border exchanges of letters were suf- ficient to facilitate an
    interweaving of ideas which, to paraphrase Roy Ascott, lends the term
    “associative thinking” considerable interpretative latitude. 237 When,
    following the tri- umph of the computer, the electronic network embarked on
    what was to be a transdisci- plinary career in disciplines such as sociology,
    psychology, and ethnology, it turned out that the human resource of the social
    network was one of Fluxus’ salient characteristics.  And even today, old boy
    and old girl networks seem to work perfectly when the task in hand is to keep
    Fluxus alive.

    -- 67

    If, however, the Italian communications theorist Tatiana Bazzichelli is right, then the his-
    tory of “networking” in art essentially began with Maciunas’ net-based diagrams of history,
    art history, and the Fluxus movement. 238 That Maciunas, whose sophisticated diagrams link
    together the most heterogeneous knowledge cultures, artistic currents, and artists, is one
    of postmodernism’s greatest networkers is certainly not in doubt. His fascination with ta-
    bles as the archetypal diagram and with charts and maps was profoundly optimistic, and
    evidence of the widespread assumption that the main thrust and patterns of history can
    indeed be captured in graphic form. Between 1953 and 1973, he produced some two dozen
    maps, charts, and diagrams, which together with further unfinished works from the years
    up to his death in 1978 form a vivid account of the various ways in which politics, business,
    cultural history, poetry, music, and art are intertwined.

    -- 68

    As crucial as Maciunas’ contribution to the development of diagrammatic networking was,
    the ascendancy of networked knowledge began long before him. 244 The pioneers here were
    the great taxonomists of the eighteenth century, such as the Swedish botanist Carolus Lin-
    naeus, who sought to classify plants on the basis of their similarities and affinities. Whereas
    Linnaeus has gone down in history as the great Enlightenment botanist, his scientific mag-
    num opus Systema naturae of 1735 is remarkable not only for its content, but also for its
    method, which basically entailed the descriptive, comparative naming of the entire plant
    and animal kingdom. Linnaeus doggedly promoted this very static style of observation in
    his publications and in doing so developed a systems theory of the living world, as espoused
    in his countless taxonomic plates.

    Maciunas got down to work taking a similarly rigorous approach to classification. The total
    dominance of administration over life posited by Adorno was second nature to Maciunas.
    Whether the task in hand was the ordering of his everyday life, the arrangement of Fluxus
    concerts, the creation of a typology of his friends’ restrooms or of animal excrement, he was
    an obsessive systematist, notorious for his passion for ordering both art and life. However,
    this drive to lend shape and order to everything is at its most clearly apparent in his
    diagrams.

    -- 70

### Misc

    Thus the Atlas of Russian History ranks among those forms of knowledge-driven
    visualization systems that can be grouped together under the term “operative
    pictoriality.” 51

    One key feature of “operative pictoriality” is the interaction on a map of the
    visual and the discursive. The latter takes the form of keywords used to
    chronicle historical events—trans- formative processes of which each map can
    provide no more than a snapshot showing them at a certain point in time, or at
    a particular stage in their unfolding. The Atlas of Russian His- tory is
    remarkable for another quality as well, namely in the way it uses recurring
    terminol- ogy. As a kind of hyperlink, this terminology facilitates navigation
    through the Atlas, which after all works on the principle of anticipation.

    [...]

    Each map thus forms a layer, conceived less as a geographical expanse than as a semantic
    stratum, so that it could be described as a predigital precursor of the hypertext.

    [...]

    Just as maps have always been a means of demonstrating power and dominance, so the
    Atlas of Russian History is a means of appropriating knowledge. Extrahistorical voids, espe-
    cially on the early maps, do not contradict this, since every map delivers a “scene” of history
    and hence a snapshot of Maciunas’ own individual knowledge. And in a more general sense,
    the ahistorical dimension comprises a productive unconsciousness of social processes. From
    the ethno-psychoanalytical point of view, the absence of events in the emptier maps can be
    read as periods of latency, which because of their potential for change are in fact essential
    to the process of cultural maturation. 54

    [...]

    The cartography ends more or less abruptly in the late nineteenth century. The
    heroic phase of Soviet history that was to follow in the early twentieth
    century was too complex to be contained, let alone mapped, in the traditional
    atlas format. To a certain extent, therefore, Maciunas can be said to have
    reached the limits of what the charting and mapping of his- tory could achieve.
    The limit he had reached was systemic, of the kind Gregory Bateson examined in
    his book Mind and Nature (1979): “All description, explanation, or representa-
    tion is necessarily in some sense a mapping of derivatives from the phenomena
    to be de- scribed onto some surface or matrix or system of coordinates. In the
    case of an actual map, the receiving matrix is commonly a flat sheet of paper
    of finite extent, and difficulties occur when that which is to be mapped is too
    big or, for example, spherical. . . . Every receiving matrix,” Bateson
    concluded, “will have its formal characteristics which will in principle be
    distortive of the phenomena to be mapped onto it.” 59

    [...]

    The distortion of phenomena in the Atlas of Russian History consisted in its
    gross simplifica- tion of complex geohistorical processes as factographic
    fallout. To be able to capture that “hot” phase in a chronology which, owing to
    the large number of fast-moving events that have to be taken into account, has
    the character of “differential elements”—to borrow Claude Lévi-Strauss’
    definition for the study of anthropology—Maciunas had no choice but to change
    his mode of presentation. He therefore switched from two-dimensional mapping of
    history to the historiogram, which could be expanded in three dimensions
    without any major structural changes and thus lent itself more readily to the
    ever greater factual density Maciunas now grappled with.

    [...]

    Usually, geographical maps are static representations. The snapshots of history
    they pro- vide have no room for the dynamic dimension of historical processes.
    The arrows Maciunas used in the Atlas of Russian History are an attempt to
    restore a sense of dynamism. The vectors are necessary to the mental animation
    of systems, and signify large-scale move- ments such as migrations or
    invasions. Yet they can only ever mark out the general direc- tion, never the
    exact route taken. It is the arrows, moreover, which lend the charts the dia-
    grammatic character that appeals so strongly to non-cartographers such as
    Maciunas. The rudimentary nature of the cartographic information provided on
    the various sheets also belongs in this category. Because Maciunas dispenses
    with a frame, a grid, and a specifica- tion of scale, the representational
    space of his history charts tends to resemble pictures rather than maps. 61

    [...]

    The history of the empire was to inform maps of the empire. The political
    function of the atlas of history was thus very similar to that of history
    painting. Its purpose was not so much to deliver comfort and relief—which was
    what history paintings had to do—as to nurture historical awareness. Such
    awareness as the basis for social development, however, was to be found only at
    the top of the learning curve that was preceded and facilitated by the
    positivistic acquisition of facts. To para- phrase Jürgen Habermas, social
    evolution is driven by changes in the knowledge poten- tial. 69 The historical
    sources show a milieu which believed in the reformation—meaning the
    improvement—of the world by education. Maciunas’ maps are of a piece with this
    en- lightenment ideology. As an imaginative matrix, they do not deliver an
    abstract model of history, but rather generate their own history—one whose
    narrative strategies elude any direct empirical verification. This metahistory
    is ideologically motivated. As the factual density increases, so the process of
    historical change picks up speed, culminating in the Russian Revolution.
    Maciunas’ mapping project was focused on that one event, an event which
    exemplifies most vividly the feasibility of history, which in turn allows for
    the idea that society can indeed be modeled.

    [...]


    At heart, he was always an inveterate knowledge worker. Lexical knowledge was
    painstakingly transferred to filing cards or recorded with the aid of excerpts,
    and the compilation drawn from these docu- ments then reworked as maps and
    charts to enable it to unfold its full potential.

    -- 37

    What we do have are various preparatory studies for an art history diagram.
    These take the form of lecture notes and other notes jotted down while reading,
    which the art history stu- dent Maciunas pieced together to create text-image
    collages (pl. 9). What is remarkable about these works is the quality of both
    the pasted-in stamp-sized black-and-white photo- graphs and the comments neatly
    penned in ink. Maciunas kept the hole-punched, letter- format pages in ring
    binders, one practical advantage of which was that new pages could be added
    without upsetting the basic chronological order. Only after collecting
    countless pieces of information did he proceed to the next stage, which was to
    interconnect the data so as to open up the larger historiographic context. To
    do this, he pasted together his data sheets to create much larger datascapes.
    These were then divided up into geometrical sections and furnished with
    semantically charged headings as an aid to orientation. The next step was to
    fill in the details. The thematic segments are linked by penciled-in arrows,
    there being no more room for further illustrations in this knowledge map.

    -- 42

    The practical problems that he was constantly facing when creating
    these knowledge charts could not be averted simply by greatly enlarging the format. That
    is what he had tried to do with his incomplete History of Art Chart, in doing so turning it
    into the enormous two-by-four-meter diagram that Pierre Restany referred to as a “giant
    family tree.” 101 The problems to which this kind of enlargement gave rise can be under-
    stood even without looking at the back of these painstakingly assembled montages, as the
    Scotch tape adhesive has long since seeped through to the front, exposing the patchwork
    for all to see. Technical problems were in any case endemic in Maciunas’ system of informa-
    tion presentation, and it was therefore only logical that he would eventually decide to ex-
    pand into the third dimension: “1st category on drawer faces, 2nd category on horizontal
    drawer interior surface and 3rd category on vertical multiple surfaces of drawer interior-
    faces of filing cards.” These ideas were the basis of the History of Art 3 dimentional [sic] Chart
    (1958–1966), although this is all we know about this spectacular work whose whereabouts
    are unknown. 102

    The fundamental question which has to be asked about a project of this magnitude concerns
    its purpose. How can such a complex scheme be put to use? Viewers are confronted with an
    immense amount of information, far more than they can possibly hope to take in. Instead
    of obtaining an overview and orientation, they are liable to be overwhelmed by detail. And
    because the history charts are designed to be viewed close up, the larger contexts are in any
    case doomed. Maciunas’ diagrams cannot be grasped at a glance; they rather have to be read
    like a reference book. Treating them in this way, however, strips the heavily visual ordering
    system of its meaning. Instead of speeding up the process of knowledge acquisition and
    hence saving time, the charts actually do the opposite. Maciunas was well aware of this
    problem. Only simple tables such as the Time-Space Chart were a real alternative.

    -- 43

    [...]

    The result was a shift of focus. Instead of seeking to merge art into life, as the first generation
    of Fluxartists had tried to do, this new generation regarded even just living their own lives
    as a creative process, as social praxis. Together they shared a belief in the value of exchang-
    ing experience.

    [...]

    The American art critic James Lewes went to the trouble of analyzing Maciunas’ Fluxlist
    (c. 1964–1974), the publications of the Gilbert and Lila Silverman Fluxus Collection, and
    nineteen exhibition catalogues in order to assess how Maciunas and his fellow Fluxists, art
    historians, and curators viewed the affinity of some 350 artists to Fluxus. The results of his
    work, presented as a chart, prove that Maciunas’ own verdicts were influential, but not
    necessarily binding. 177 Maciunas’ diagrams make it quite clear that he was trying hard to
    steer Fluxus in one particular direction and to make sure it stayed on course. Exercising the
    authority he had bestowed on himself, Maciunas claimed sole power of definition and was
    therefore constantly redefining the Fluxus hardcore. The admission of new members and
    expulsion of old ones were at first announced in the Newsletter or documented in the mani-
    festos, and it was this personnel policy which eventually gave rise to the Expanded Arts Dia-
    gram. To be accepted into the fold, would-be Fluxists first had to prove themselves to be
    kindred spirits, and express a genuine desire to promote Fluxus as a group without trying
    to stand out from the crowd.

    [...]

    As the Japanese artist Chieko (later Mieko) Shiomi, who had linked up with Maciunas in
    New York, made clear, Breton foreshadowed another negative habit. Shiomi condemned
    autocratic practices, arguing that Maciunas himself had been eminently more guilty of the
    alleged misconduct than had the expellees. 184 Citing Tristan Tzara, she recalled some of the
    grave mistakes Breton had made as head of the surrealist movement, although the real
    target of her censure was of course Maciunas: “But here is Tristan Zala’(?)s words to Breton.
    The mistake of Breton was that he didn’t recognize the time to put period to the activity of surreal-
    ism and as a result Breton made it soiled and decadent. Nobody can revive the once dead activity
    by any artificial means.” Trying to keep Fluxus alive artificially, Shiomi concluded, was
    therefore not advisable. “And also this would be true that even if the groupe [sic] were dead,
    each person in it can survive and found renewed groupe. You, as George Maciunas, too.” 185
    Maciunas, however, was careful not to heed this kind of advice, as that would have meant
    relinquishing his leading role as organizer and chronicler of Fluxus, which after all had
    done much for his reputation as spiritus rector. Notwithstanding all that Maciunas had
    done for Fluxus, the image of the tyrant “in the style of Tzara and Breton” was one he never
    really shook off. 186

    -- 58

    As one who aspired to have contacts all over the world, Maciunas felt a stronger affinity for
    Dada than for futurism or surrealism. While F. T. Marinetti sought to impress upon the
    futurists the importance of italianitá, and André Breton went out of his way to ensure that
    surrealism retained its French roots (at least until World War II put an abrupt end to the
    patriotic dream of a grande nation of surrealism), Dadaism’s inherent internationalism was
    apparent even in the name. “The word Dada in itself indicates the internationalism of the
    movement which is bound to no frontiers, religions or professions,” as Richard Huelsenbeck
    put it in his First German Dada Manifesto, also called the Collective Dada Manifesto. “Dada is
    the international expression of our times . . . the Dada Club consequently has members all
    over the world, in Honolulu as well as New Orleans and Meseritz.” 233 Avant-garde interna-
    tionalism was a programmatic offshoot of the movement’s international core, even if this
    core was itself a product of historical circumstances. The same tendency is apparent in the
    name “Fluxus” as well, which like the word “Dada” was found by leafing through a reference
    book. “Fluxus” is a Latin word—a lexical and hence international concept.

    -- 67
