[[!meta title="Post-Scarcity Anarchism"]]

* Murray Bookchin.
* Working Classic Series.
* AK Press - 2004.

## Towards a Liberatory Technology

    The year 1848 stands out as a turning point in the history of modern revolutions.
    This was the year when Marxism made its debut as a distinct ideology in the pages
    of de Communist Manifesto, and when the proletariat, represented by the Parisian
    workers, made its debut as a distinct political force on the barricades of June.
    It could also be said that 1948, a year close to the halfway mark of the nineteenth
    century, represents the culmination of the traditional steam-powered technology
    initiated by the Newcomen engine a century and a half earlier.

    -- 43

    I have reviewed these technological developments because both their promise and
    their limitations excercised a profound influence on nineteenth century revolutionary
    thought. The innovations in textile and iron-making technology provided a new sense
    of promise, indeed a new stimulus, to socialist and utopian thought. It seemed to the
    revolutionary theorist that for the first time in history he could anchor his dream
    of a liberatory society in the visible prospect of material abundance and increased
    leisure for the mass of humanity. Socialism, the theorists argued, could be based
    on self-interest rather than on man's dubious nobility of mind and spirit. Technological
    innovation had transmuted the socialist ideal from a vague humanitarian hope into a
    practical program.

    The newly acquired practicality compelled many socialist theorists, particularly Marx
    and Engels, to grapple with the technological limitations of their time. They were faced
    with a strategic issue: in all previous revolutions, technology had not yet developed to
    a level where men could be freed from material want, toil and the struggle over the
    necessities of life. However glowing and lofty were the revolutionary ideals of the past,
    the vas majority of the people, burdened by material want, had to leave the stage
    of history after the revolution, return to work, and deliver the management of society
    to a new leisured class of exploiters. Indeed, any attempt to equalize the wealth of
    society at a low level of technological development would not have eliminated want, but
    would have merely made it into a general feature of society as a whole, thereby recreating
    all the conditions for a new struggle over the material things of life, for new forms of
    property, and eventually for a new system of class domination.

    [...]

    Virtually all the utopias, theories and revolutionary programs of the early nineteenth
    century were faced with problems of necessity -- of how to allocate labor and material
    goods at a relatively low level of technological development.

    [...]

    The fact that men would have to devote a substantial portion of their time to toil,
    for which they would get scant returns, formed a major premise of all socialist ideology
    -- authoritarian and liberarian, utopian and scientific, Marxist and anarchist.
    Implicit in the Marxist notion of a planned economy was the fact, incontestably clear
    in the Marx's day, that socialism would still be burdened by relatively scarce resources.
    Men would have to plan -- in effect -- to restrict -- the distribution of goods and would
    have to rationalize -- in effect, to intensify -- the use of labor.

    -- 44-45

    The problem of dealing with want and work -- and age-old problem perpetuated by the early
    Industrial Revolution -- produced the great divergence in revolutionary ideas between
    socialism and anarchism. Freedom would still be circunscribed by necessity in the event
    of a revolution. How was this world of necessity to be "administered"? How could the
    allocation of goods and duties be decided? Marx left this decision to a state power,
    a transitional "proletarian" state power, to be sure, but nevertheless a coercive body,
    established above society. According to Marx, the state "wither away" as technology
    developed and enlarged the domain of freedom, granting humanity material plenty and the
    leisure to control its affairs directly. This strange calculus, in which necessity
    and freedom were mediated by the state, differed very little politically from the common
    run of bourgeois democratic radical opinion in the last century. The anarchist hope for
    the abolition of the state, on the other hand, rested largely on a belief in the viability
    of man's social instincts. Bakunin, for example, thought custom would compel any individuals
    with antisocial proclivities to abide by collectivist values and nedds without obliging
    society to use coercion. Kropotkin, who exercised more influence among anachists in this
    area of speculation, invoked man's propensity for mutual aid -- essentially a social instinct
    -- as the guarantor of solidarity in an anarchist community (a concept which he derived from
    his study of animal and social evolution).

    The fact remains, however, that in both cases -- the Marxist and anarchist -- the answer
    to the problem of want and work was shot through with ambiguity. [...] but given the
    limited technological development of the last century, [...] both schools depended on
    an act of faith to cope with the problem of want and work. Anarchists could argue against
    the Marxists that any transitional state, however revolutionary its rethoric and democratic
    its structure, would be self-perpetuating; it would tend to become an end in itself and
    to preserve the very material and social conditions it had been created to remove. For
    such a state to "wither away" (that is, to promote its own dissolution) would require
    its leaders and bureaucracy to be people of superhuman moral qualities. The Marxists,
    in turn, could invoke history to show that custom and mutualistic propensities were never
    effective barriers to the pressures of material need, or to the onslaught of property,
    or to the development of exploitation and class domination. Accordingly, they dismissed
    anarchism as an ethical doctrine which revived the mystique of the natural man and his
    inborn social virtues.

    -- 46-47

    That the socialist notions of the last generation now seem to be anachronisms is not
    due to any superior insights that prevail today. The last three decades, particularly
    the years of the late 1950's, mark a turning point in the techological development,
    a technological revolution that negates all the values, political schemes and social
    perspectives held by mankind throughout all previous recorded history. [...] As we shall
    see, a new technology has developed that could largely replace the realm of necessity
    by the realm of freedom.

    -- 48

    Almost every account of applied automation today must be regarded as
    provisional: as soon as one describes a partially automated industry,
    technological advances make the description obsolete.

    -- 56
