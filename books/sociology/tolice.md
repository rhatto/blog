[[!meta title="A Tolice da Inteligência Brasileira"]]

* Autor: Jesse Souza

Violência simbólica
-------------------

    Ora, como diria o insuspeito Max Weber, os ricos e felizes, em todas as
    épocas e em todos os lugares, não querem apenas ser ricos e felizes. Querem
    saber que têm "direito" à riqueza e felicidade. Isso significa que o privilégio
    -- mesmo o flagrantemente injusto, como o que se transmite por herança --
    necessita ser "legitimado", ou seja, aceito mesmo por aqueles que foram
    excluídos de todos os privilégios.

    [...]

    É por conta disso que os privilegiados são os donos dos jornais, das
    editoras, das universidades, das TVs e do que se decide nos tribunais e nos
    partidos políticos. Apenas dominando todas essas estruturas é que se pode
    monopolizar os recursos naturais que deveriam ser de todos, e explorar o
    trabalho da imensa maioria de não-privilegiados soba a forma de taxa de lucro,
    juro, renda da terra ou aluguel.

    A soma dessas rendas de capital no Brasil é monopolizada em grande parte
    pelo 1% mais rico da população. É o trabalho dos 99% restantes que se transfere
    em grande medida para o bolso do 1% mais rico.

    [...]

    A tese central deste livro é que tamanha "violência simbólica" só é
    possível pelo sequestro da "inteligência brasileira" para o serviço não da
    imensa maioria da população, mas do 1% mais rico. [...] Esse serviço que a
    imensa maioria dos intelectuais brasileiros sempre prestou e ainda presta é o
    que possibilita a justificação, por exemplo, de que os problemas brasileiros
    não vêm da grotesca concentração da riqueza social em pouquíssimas mãos, mas
    sim da "corrupção apenas do Estado".

    E isso leva a uma falsa oposição entre Estado demonizado e mercado --
    concentrado e superfaturado como é o mercado brasileiro --, como o reino da
    virtude e da eficiência. E em um contexto no qual não existe fortuna de
    brasileiro que não tenha sido construída à sombra de financiamentos e
    privilégios estatais nem corrupção estatal sistemática sem conivência e
    estímulo do mercado. E também em um cenário em que as classes sociais que mais
    apoiam essa bandeira como se fosse sua -- os extratos conservadores da classe
    média tradicional e setores ascendentes da nova classe trabalhadora -- são
    precisamente as classes que mais sofrem com os bens e serviços superfaturados e
    de qualidade duvidosa que o 1% mais rico vende a elas.

    -- 9 a 11

Crítica das ideias
------------------

    Este livro é uma história das ideias dominantes do Brasil moderno
    e de sua institucionalização.

    [...]

    Retira-se dos indivíduos a possibilidade de compreender a totalidade da
    sociedade e de suas reais contradições e conflitos, os quais são substituídos
    por falsas questões. A fragmentação do conhecimento serve aos interesses dos
    que estão ganhando na sociedade, já que evidencia sua mudança possível. A ação
    da mudança, a capacidade moral e política de escolher caminhos alternativos
    pela vontade de intervir no mundo, pressupõe "conhecimento do mundo" para não
    ser "escolha cega". É isso que faz com que todo o conhecimento fragmentário e
    superficial seja necessariamente conservador. Ele ajuda a manter e justificar o
    que já existe.

    -- 12 e 13

Interpretações que explicam o mundo
-----------------------------------

    Os seres humanos são animais que se interpretam. Isso significa que não existe
    "comportamento automático", este é sempre influenciado por uma "forma
    específica de interpretar e compreender a vida". Essas interpretações que guiam
    nossas escolhas na vida foram obras de profetas religiosos no passado. Nos
    últimos duzentos anos essas interpretações, que explicam o mundo e nos dizem
    como devemos agir nele, foram obras de intelectuais seculares. O mais
    importante desses intelectuais no Ocidente moderno foi -- juntamente com Karl
    Marx -- o sociólogo alemão Max Weber. Afinal, foi da pena de Weber que se
    originou a forma predominante como todo o Ocidente moderno se autointerpreta e
    se legitima. As ideias dominantes que circulam na imprensa, nas salas de aula,
    nas discussões parlamentares, nas conversas de botequim -- em todo lugar -- são
    sempre formas mais simplificadas de ideias produzidas por grandes pensadores.

    Daí a importância de se recuperar o sentido original dessas ideias que são tão
    relevantes para nossas vidas ainda que, normalmente, não nos demos conta disso.

    [...]

    Não existe ordem social moderna sem uma legitimação pretensamente científica
    desta mesma ordem.

    Talvez o uso de Max Weber e sua obra seja um dos exemplos mais significativos
    do caráter bifronte da ciência: tanto como mecanismo de esclarecimento do mundo
    quanto como mecanismo de encobrimento das relações de poder que permitem a
    reprodução de privilégios injustos de toda a espécie.

    [...]

    Para a versão liberal e afirmativa, Weber fornece, por um lado, sua análise
    da "revolução simbólica" do protestantismo ascético; para ele, a efetiva
    revolução moderna, na medida em que transformou a "consciência" dos indivíduos
    e, a partir daí, a realidade externa, é a figura do protestante ascético,
    que com vontade férrea e armas da disciplina e do autocontrole cria o fundamento
    histórico para a noção do "sujeito moderno".

    [...]

    Mas Weber, e nisso reside sua influência e atualidade extraordinária,
    também compreendia, no entanto, o lado sombrio do racionalismo ocidental.
    Se o pioneiro protestante ainda possuía perspectivaas éticas na sua conduta,
    seu "filho" e, muito especialmente, seu "neto", habitantes do mundo secularizado,
    são percebidos por Weber de modo bastante diferente. Para descrevê-los, Weber
    utiliza dois "tipos ideais" [...], o "especialista sem espírito", que tudo
    conhece sobre seu pequeno mundo de atividade e nada sabe (nem quer saber)
    acerca de contextos mais amplos que determinam seu pequeno mundo, e, por
    outro, o "homem do prazer sem coração", que tende a amesquinhar seu mundo
    sentimental e emotivo à busca de prazeres momentâneos e imediatos.

    -- 17 a 19

Patrimonialismo brasileiro
--------------------------

    Toda a ambiguidade de Max Weber em relação ao capitalismo -- produtor de seres
    amesquinhados precisamente nas dimensões cognitiva e moral -- e à própria
    sociedade americana -- [...] foi cuidadosa e intencionalmente posta de lado.

    -- 27

    No começo, o aspecto mais importante era simplesmente legitimar científica
    e politicamente -- com farto financiamento das agências estatais norte-americanas
    nos Estados Unidos e fora dele -- a superioridade norte-americana em relação
    a todas as outras sociedades.

    -- 27
