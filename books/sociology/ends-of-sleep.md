[[!meta title="24/7 - Late Capitalism and the Ends of Sleep"]]

* Author: Jonathan Crary
* Publisher: Verso
* Year: 2013

## Concepts

* Serialization, from Jean-Paul Sartre’s Critique of Dialectical Reason.
