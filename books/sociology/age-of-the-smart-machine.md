[[!meta title="In the Age of the Smart Machine"]]

## Index

* Deskilling, diplacement of "the human body and its know-how" and reskilling, 57.
* Rebellion against the automated door, 21-23.
* Humanization (Marx) as "tempering animality with rationality" in the progress of civilization, 30.
* Uncivilized, savage worker's "spontaneous, instinctually gratifying behavior"
  in the past, signaling the problem of "how to get the human body to remain in one place,
  pay attention, and perform consistently over a fixed period of time", 31-34.
* Paradox of the body; body's dual role in production: effort and skill (No Pain no Gain), 36.
* "Singer Sewing Machine Company was not able to produce perfectly interchangeable parts.
  As a result, they relied on skilled fitters to assemble each product.", 39.
* Continous Process as a possible way to break the effort-skill body paradox and the U-curve
  of social integration, 50-56
* Transition from oral communication to written communication (pages 77, 100);
  it's followed by a transition where calculations were transferred from mental
  operations to calculating machines.
* Characteristics of action-centered skills, 106.
* Typewriters, 115.
* Feminization of clerical work, 116-117.
* Secretaries: _dedicated_  (acting as buffers, sorters and organizers) versus _pool_ modes (treated as input-output devices), 122-123.

## Impressions

* Intro mentions a control room like the Star Trek bridge. It makes me relate
  to the skilled worker at one of its limits - those of the austronaut. Highly
  skilled and disciplined, could be an interesting comparison.

* The pathway from motor knowledge to abstract knowledge recalls Piaget's discussion
  about intelligence.

* Also some bridges can be built with Nicolelis' discussion of technology
  transforming itself in extensions of the brain.

* I to, sometimes, can feel my systems. How they're running, which are
  the bottlenecks, what should I look for. Load average from a server is
  something you can "feel" just by delays in your terminal.

* Transitional generations might feel a strange feeling.

## Excerpts

### Choices on knowledge, authority and collaboration

    The choices that we face concern the conception and distribution of
    knowledge in the workplace. Imagine the following scenario: Intelli-
    gence is lodged in the smart machine at the expense of the human
    capacity for critical judgment. Organizational members become ever
    more dependent, docile, and secretly cynical. As more tasks must be
    accomplished through the medium of information technology (I call
    this "computer-mediated work"), the sentient body loses its salience
    as a source of knowledge, resulting in profound disorientation and loss
    of meaning. People intensify their search for avenues of escape through
    drugs, apathy, or adversarial conflict, as the majority of jobs in our
    offices and factories become increasingly isolated, remote, routine, and
    perfunctory. Al ternativel y, imagine this scenario: Organizational lead-
    ers recognize the new forms of skill and knowledge needed to truly
    exploit the potential of an intelligent technology. They direct their
    resources toward creating a work force that can exercise critical judg-
    ment as it manages the surrounding machine systems. Work becomes
    more abstract as it depends upon understanding and manipulating infor-
    mation. This marks the beginning of new forms of mastery and provides
    an opportunity to imbue jobs with more comprehensive meaning. A
    new array of work tasks offer unprecedented opportunities for a wide
    range of employees to add value to products and services.

    [...]

    The choices that we make will shape relations of authority in the
    workplace. Once more, imagine: Managers struggle to retain their tra-
    ditional sources of authority, which have depended in an important
    way upon their exclusive control of the organization's knowledge base.
    They use the new technology to structure organizational experience
    in ways that help reproduce the legitimacy of their traditional roles.
    Managers insist on the prerogatives of command and seek methods that
    protect the hierarchical distance that distinguishes them from their
    subordinates. Employees barred from the new forms of mastery relin-
    quish their sense of responsibility for the organization's work and use
    obedience to authority as a means of expressing their resentment.
    Imagine an alternative: This technological transformation engenders a
    new approach to organizational behavior, one in which relationships
    are more intricate, collaborative, and bound by the mutual responsibili-
    ties of colleagues. As the new technology integrates information across
    time and space, managers and workers each overcome their narrow
    functional perspectives and create new roles that are better suited to
    enhancing value-adding activities in a data-rich environment. As the
    quality of skills at each organizational level becomes similar, hierarchi-
    cal distinctions begin to blur. Authority comes to depend more upon
    an appropriate fit between knowledge and responsibility than upon the
    ranking rules of the traditional organizational pyramid.

    [...]

    Imagine this scenario: The new technology becomes the source of surveillance
    techniques that are used to ensnare organizational members or to subtly bully
    them into confor- mity. Managers employ the technology to circumvent the
    demanding work of face-to-face engagement, substituting instead techniques of
    remote management and automated administration. The new techno- logical
    infrastructure becomes a battlefield of techniques, with manag- ers inventing
    novel ways to enhance certainty and control while em- ployees discover new
    methods of self-protection and even sabotage.  Imagine the alternative: The new
    technological milieu becomes a re- source from which are fashioned innovative
    methods of information sharing and social exchange. These methods in turn
    produce a deep- ened sense of collective responsibility and joint ownership, as
    access to ever-broader domains of information lend new objectivity to data and
    preempt the dictates of hierarchical authority.

    -- 5-7

### A paradox

    From the unmanned factory to the automated cockpit, visions of the future hail
    information technology as the final answer to "the labor question," the
    ultimate opportunity to rid our- selves of the thorny problems associated with
    training and managing a competent and committed work force. These very same
    technologies have been applauded as the hallmark of a second industrial
    revolution, in which the classic conflicts of knowledge and power associated
    with an earlier age will be synthesized in an array of organizational inno-
    vations and new procedures for the production of goods and services, all
    characterized by an unprecedented degree of labor harmony and widespread
    participation in management process. I Why the paradox?

    -- 7-8

### Informate and automate: the duality of Information Technology

    Thus, information technology, even when it is applied to automati-
    cally reproduce a finite activity, is not mute. It not only imposes infor-
    mation (in the form of programmed instructions) but also produces
    information. It both accomplishes tasks and translates them into infor-
    mation. The action of a machine is entirely invested in its object, the
    product. Information technology, on the other hand, introduces an ad-
    ditional dimension of reflexivity: it makes its contribution to the prod-
    uct, but it also reflects back on its activities and on the system of activi-
    ties to which it is related. Information technology not only produces
    action but also produces a voice that symbolically renders events, ob-
    jects, and processes so that they become visible, knowable, and share-
    able in a new way.

    -- 9

    [...]

    An emphasis on the informating capacity of intelligent technology can provide a
    point of origin for new conceptions of work and power. A more re- stricted
    emphasis on its automating capacity can provide the occasion for that second
    kind of revolution-a return to the familiar grounds of industrial society with
    divergent interests battling for control, aug- mented by an array of new
    material resources with which to attack and defend.

    -- 11-12

### The natural attitude

    The most treacherous enemy of such research is what philosophers
    call "the natural attitude," our capacity to live daily life in a way that
    takes for granted the objects and activities that surround us. Even when
    we encounter new objects in our environment, our tendency is to expe-
    rience them in terms of categories and qualities with which we are
    already familiar. The natural attitude allows us to assume and predict a
    great many things about each other's behavior without first establishing
    premises at the outset of every interaction. The natural attitude can
    also stand in the way of awareness, for ordinary experience has to be
    made extraordinary in order to become accessible to reflection. This
    occurs when we encounter a problem: when our actions do not yield
    the expected results, we are caught by surprise and so are motivated
    to reflect upon our initial assumptions. 2 Awareness requires a rupture
    with the world we take for granted; then old categories of experience
    are called into question and revised. For example, in the early days of
    photography, the discrepancies between the camera's eye and the hu-
    man eye were avidly discussed, but, "once they began to think photo-
    graphically, people stopped talking about photographic distortion, as it
    was called.,,3

    -- 13

### The Control Room

Whoa, the description of the Control Room from the Piney Wood Mill recalled me
the Cybersyn Control Room built -- and then destroyed -- less than a decade
before:

    Workers sit on orthopedically designed swivel chairs covered with a royal blue
    fabric, facing video display ter- minals. The terminals, which display process
    information for the purposes of monitoring and control, are built into polished
    oak cabi- nets. Their screens glow with numbers, letters, and graphics in vivid
    red, green, and blue. The floor here is covered with slate-gray carpet- ing;
    the angled countertops on which the terminals sit are rust brown and edged in
    black. The walls are covered with a wheat-colored fabric and the molding
    repeats the polished oak of the cabinetry. The dropped ceiling is of a bronzed
    metal, and from it is suspended a three dimen- sional structure into which
    lights have been recessed and angled to provide the right amount of
    illumination without creating glare on the screens. The color scheme is
    repeated on the ceiling-soft tones of beige, rust, brown, and gray in a
    geometric design.

    -- 20-21

### Technology, work and the body

    Technology represents intelligence systematically applied to the
    problem of the body. It functions to amplify and surpass the organic
    limits of the body; it compensates for the body's fragility and vulnera-
    bility. Industrial technology has substituted for the human body in
    many of the processes associated with production and so has redefined
    the limits of production formerly imposed by the body. As a result,
    society's capacity to produce things has been extended in a way that is
    unprecedented in human history. This achievement has not been with-
    out its costs, however. In diminishing the role of the worker's body in
    the labor process, industrial technology has also tended to diminish the
    importance of the worker. In creating jobs that require less human
    effort, industrial technology has also been used to create jobs that re-
    quire less human talent. In creating jobs that demand less of the body,
    industrial production has also tended to create jobs that give less to the
    body, in terms of opportunities to accrue knowledge in the production
    process. These two-sided consequences have been fundamental for the
    growth and development of the industrial bureaucracy, which has de-
    pended upon the rationalization and centralization of knowledge as the
    basis of control.

    [...]

    Throughout most of human history, work has ines- capably meant the exertion and
    often the depletion of the worker's body. Yet only in the context of such
    exertion was it possible to learn a trade and to master skills. Since the
    industrial revolution, the acceler- ated progress of automation has generally
    meant a reduction in the amount of effort required of the human body in the
    labor process. It has also tended to reduce the quality of skills that a worker
    must bring to the activity of making something. Industrial technology has been
    developed in a manner that increases its capacity to spare the human body,
    while at the same time it has usurped opportunities for the devel- opment and
    performance of skills that only the body can learn and remember.

    -- 22-23

    The progress of automation has been associated with both a general
    decline in the degree of know-how required of the worker and a de-
    cline in the degree of physical punishment to which he or she must be
    subjected. Information technology, however, does have the potential
    to redirect the historical trajectory of automation. The intrinsic power
    of its informating capacity can change the basis upon which knowledge
    is developed and applied in the industrial production process by lifting
    knowledge entirely out of the body's domain. The new technology sig-
    nals the transposition of work activities to the abstract domain of infor-
    mation. Toil no longer implies physical depletion. "Work" becomes
    the manipulation of symbols, and when this occurs, the nature of skill
    is redefined. The application of technology that preserves the body may
    no longer imply the destruction of knowledge; instead, it may imply
    the reconstruction of knowledge of a different sort.

    -- 23

    There is reason enough to want to avoid exhausting work, but the
    constancy of repugnance was not confined to forms of labor that were
    extremely punishing. As noted earlier, in the membership practices of
    some guilds, even the craftsworker was liable to be an object of con-
    tempt because of the manual nature of that work. Such repugnance is
    in itself an act of distancing. It is both a rejection of the animal body
    and an affirmation of one's ability to translate the impulses of that body
    into the infinitely more subtle behavioral codes that mediate power in
    complex organizations. Once this translation occurs, the body is no
    longer the vehicle for involuntary affective or physical displays. Instead,
    it becomes the instrument of carefully crafted gestures and behaviors
    designed to achieve a calculated effect in an environment where inter-
    personal influence and even a kind of rudimentary psychological insight
    are critical to success. In the interpersonal world of court society, the
    body's knowledge involved the ability to be attuned to the psycho-
    logical needs and demands of others, particularly of superiors, and
    to produce subtly detailed nonverbal behavior that reflected this
    awareness.

    -- 28-29

    The differences between the work performed by the skilled
    workers and the laborers was not of an "intellectual" versus manual
    activity. The difference lay in the content of a similarly heavy manual
    work: a content of rationality of participation for skilled workers versus
    one of total indifference for laborers. 5 5

    The work of the skilled craftsperson may not have been "intellec-
    tual," but it was knowledgeable. These nineteenth-century workers
    participated in a form of knowledge that had always defined the activity
    of making things. It was knowledge that accrues to the sentient body
    in the course of its activity; knowledge inscribed in the laboring body-
    in hands, fingertips, wrists, feet, nose, eyes, ears, skin, muscles, shoul-
    ders, arms, and legs-as surely as it was inscribed in the brain. It was
    knowledge filled with intimate detail of materials and ambience-the
    color and consistency of metal as it was thrust into a blazing fire, the
    smooth finish of the clay as it gave up its moisture, the supple feel of
    the leather as it was beaten and stretched, the strength and delicacy of
    glass as it was filled with human breath. These details were known,
    though in the practical action of production work, they were rarely
    made explicit. Few of those who had such knowledge would have been
    able to explain, rationalize, or articulate it. Such skills were learned
    through observation, imitation, and action more than they were taught,
    reflected upon, or verbalized. For example, James J. Davis, later to
    become Warren Harding's Secretary of Labor, learned the skill of pud-
    dling iron by working as his father's helper in a Pennsylvania foundry:
    "None of us ever went to school and learned the chemistry of it from
    books. . . . We learned the trick by doing it, standing with our faces in
    the scorching heat while our hands puddled the metal in its glaring
    bath. ,,56

    -- 40

### The Scientific management

Taylor, when "worker's know-how was expropriated to the ranks of management",
using information technology -- that _automates_ and _informates_ -- before
computer adoption, 41-44.

    Scientific management frequently meant not only that individual effort was
    simplified (either because of labor-saving equipment or new organizational
    methods that fragmented tasks into their simplest components), but also that
    the pace of effort was intensified, thus raising the level of fatigue and
    stress.  Effort was purified-stripped of waste-but not yet eased, and resis-
    tance to scientific management harkened back to the age-old issue of the
    intensity and degree of physical exertion to which the body should be subject.
    As long as effort was organized by the traditional practices of a craft, it
    could be experienced as within one's own control and, being inextricably linked
    to skill, as a source of considerable pride, satisfaction, and independence.
    Stripped of this context and mean- ing, demands for greater effort only
    intensified the desire for self- . 69 protectIon.

    Taylor had believed that the transcendent logic of science, together
    with easier work and better, more fairly determined wages, could inte-
    grate the worker into the organization and inspire a zest for production.
    Instead, the forms of work organization that emerged with scientific
    management tended to amplify the divergence of interests between
    management and workers. Scientific management revised many of the
    assumptions that had guided the traditional employer-employee rela-
    tionship in that it allowed a minimal connection between the organiza-
    tion and the individual in terms of skill, training, and the centrality of
    the worker's contribution. It also permitted a new flexibility in work
    force management, promoting the maximum interchangeability of per-
    sonnel and the minimum dependence on their ability, availability, or
    motivation. 70

    [...]

    A machinist gained prominence when he debated Taylor in 1 914 and
    remarked, "we don't want to work as fast as we are able to. We want
    to work as fast as we think it's comfortable for us to work. We haven't
    come into existence for the purpose of seeing how great a task we can
    perform through a lifetime. We are trying to regulate our work so as
    to make it auxiliary to our lives. ,,73

    -- 45-46

Fordism:

    "The instruction cards on which Taylor set so much value, Ford was able to
    discard. The conveyor belt, the traveling platform, the overhead rails and
    material conveyors take their place. . . . Motion analysis has become largely
    unnecessary, for the task of the assembly line worker is reduced to a few
    manipulations. Taylor's stop-watch nevertheless remains measuring the time of
    operations to the fraction of a second. ,,74

    The fragmentation of tasks characteristic of the new Ford assembly
    line achieved dramatic increases in productivity due to the detailed
    time study of thousands of operations and the invention of the conveyor
    belt and other equipment that maximized the continuity of assembly.

    [...]

    Effort is simplified (though its pace is frequently intensified) while skill
    demands are reduced by new methods of task organization and new forms of
    machinery.

    The continuity of assembly depended upon the production of interchangeable
    parts for uniform products.

    -- 47

Effects:

    For the majority of industrial workers in the generations that followed, there
    would be fewer opportunities to develop or maintain craft skills. Mass
    production depended upon interchangeability for the standardization of
    production; this principle required manufacturing operations to free themselves
    from the particularistic know-how of the craftsworker.

    [...]

    Thus, applications of industrial technology have simplified, and gen-
    erally reduced, physical effort, but because of the bond between effort
    and skill, they have also tended to reduce or eliminate know-how. 78

    [...]

    Self-preservation would induce the worker to accept automation.

    [...]

    the machine assumes responsibility

    [...]

    In Braverman's influential critique of what he called the "degradation
    of work" in this century, he used Bright's study to make a very different
    point. Where Bright saw the glass half full because the physical demands
    of work were curtailed, Braverman saw the glass being drained, as work-
    ers' skills were absorbed by technology. For Braverman, the transfer of
    skill into machinery represented a triumph of "dead labor over living
    labor," a necessity of capitalist logic. As machinery is enlarged and per-
    fected, the worker is made puny and insignificant. By substituting capital
    (in the form of machinery) for labor, Braverman believed that employers
    merely seized the opportunity to exert greater control over the labor
    process. As the work force encountered fewer opportunities for skill
    development, it would become progressively less capable and, thus, less
    bl ... 85

    -- 48-49

### The Transfer

The transition from manual to automated, the process of transferring knowledged
from the body to the machine is a sistematization of the transference of
knowledge from art (work whose reproduction is challenging) to technics
(pragmatized art, the art of practical, efficient life):

    However, the term transfer must be doubly laden if it is to adequately describe
    this process. Knowledge was first transferred from one quality of knowing to
    another-from knowing that was sentient, embedded, and experience-based to know-
    ing that was explicit and thus subject to rational analysis and perpetual
    reformulation. The mechanisms used to accomplish this transfer were themselves
    labor intensive (that is, they depended upon first-hand ob- servation of
    time-study experts) and were designed solely in the con- text of, and with the
    express purpose of, enabling a second transfer- one that entailed the migration
    of knowledge from labor to manage- ment with its pointed implications for the
    distribution of authority and the division of labor in the industrial
    organization.

    -- 56-57

    The worker's capacity "to know" has been lodged in sentience and
    displayed in action. The physical presence of the process equipment
    has been the setting that corresponded to this knowledge, which could,
    in turn, be displayed only in that context. As long as the action context
    remained intact, it was possible for knowledge to remain implicit. In
    this sense, the worker knew a great deal, but very little of that knowl-
    edge was ever articulated, written down, or made explicit in any fash-
    ion. Instead, operators went about their business, displaying their
    know-how and rarely attempting to translate that knowledge into terms
    that were publicly accessible. This is what managers mean when they
    speak of the "art" involved in operating these plants.

    -- 59

### From action-centered to intellective skill

    This does not imply that action-centered skills exist independent
    of cognitive activity. Rather, it means that the processes of learning,
    remembering, and displaying action-centered skills do not necessarily
    require that the knowledge they contain be made explicit. Physical
    cues do not require inference; learning in an action-centered context is
    more likely to be analogical than analytical. In contrast, the abstract
    cues available through the data interface do require explicit inferential
    reasoning, particularly in the early phases of the learning process. It is
    necessary to reason out the meaning of those cues-what is their rela-
    tion to each other and to the world "out there"?

    -- 73

    As information technology restructures the work situation, it ab-
    stracts thought from action. Absorption, immediacy, and organic re-
    sponsiveness are superseded by distance, coolness, and remoteness.
    Such distance brings an opportunity for reflection.

    [...]

    The thinking this operator refers to is of a different quality from the
    thinking that attended the display of action-centered skills. It combines
    abstraction, explicit inference, and procedural reasoning. Taken to-
    gether, these elements make possible a new set of competencies that I
    call intellective skills. As long as the new technology signals only deskil-
    ling-the diminished importance of action-centered skills-there will
    be little probability of developing critical judgment at the data inter-
    face. To rekindle such judgment, though on a new, more abstract foot-
    ing, a reskilling process is required. Mastery in a computer-mediated
    environment depends upon developing intellective skills.

    -- 75-76

    [...]

    The second dimension of this crisis involves the ambiguity of action.
    It is conveyed in the question, what have I done? The computer system
    now interpolates between the worker and the action context, and as it
    does so, it represents to the worker his or her effects on the world.
    However, reading symbols does not provoke the same feeling of having
    done something as one gets from more direct, organic involvement in
    execution. There is a continual questioning of action-Have I done
    anything? How can I be sure?

    -- 81

It seems clear to me that, despite de physical alleviation introduced by
intellectual work, it still does not free workers from fatigue. It just put it
in a different framework: mental exhaustion and
[burn-out](/books/sociedade/burnout-society). Only dead, abstracted "work"
won't lead tiredness. But then it won't be work anymore.

### Evolution of white-collar work

    The evolution of white-collar work has followed a historical path
    that is in many ways the precise opposite of that taken by blue-collar
    work. Manufacturing has its roots in the work of skilled craft. In most
    cases, that work was successively gutted of the elements that made it
    skillful-leaving behind jobs that were simplified and routinized. An
    examination of work at the various levels of the management hierarchy
    reveals a different process. Elements of managerial work most easily
    subjected to rationalization were "carved out" of the manager's activit-
    ies. The foundational example of this process is the rationalization of
    executive work, which was accomplished by ejecting those elements
    that could be explicated and systematized, preserving intact the skills
    that comprise executive craft. It was the carving out of such elements
    that created the array of functions we now associate with middle man-
    agement. A similar process accounts for the origins of clerical work. In
    each case, the most easily rationalized features of the activities at one
    level were carved out, pushed downward, and used to create wholly
    new lower-level jobs. In this process, higher-level positions were not
    eliminated; on the contrary, they came to be seen more than ever as
    the depository of the organization's skills.

    [...]

    White-collar employees used their bodies, too, but
    in the service of actino-with, for interpersonal communication and coor-
    dination. It was not until the intensive introduction of office machinery,
    and with it scientific management, that this distinct orientation was
    challenged. During this period, an effort was made to invent a new kind
    of clerical work-work that more closely resembled the laboring body
    continually actino-on the inanimate objects, paper and equipment, that
    were coming to define modern office work. Automation in the factory
    had diverse effects, frequently limiting human effort and physical
    suffering, though sometimes exacerbating it. But the discontinuity in
    the nature of clerical work introduced with office machinery, together
    with the application of Tayloristic forms of work organization, did
    much to increase the physical suffering of the clerk. While it remained
    possible to keep a white collar clean, the clerk's position was severed
    from its earlier responsibilities of social coordination and was con-
    verted instead to an emphasis on regularity of physical effort and mental
    concentration.

    -- 98

    Many successful merchants and entre-
    preneurs were well known for the speed of their mental calculations,
    and Eaton's how-to book provides a chapter on tricks and shortcuts to
    aid in rapid mental arithmetic. 6 Owner-managers frequently sur-
    rounded themselves with sons, nephews, and cousins-a move that fa-
    cilitated oral communication through shared meaning and context and
    eased the pressure for written documentation. 7

    [...]

    Detailed empirical studies of modern executives' work, several of
    which have been published over the last thirty years, are greeted with
    the curiosity and fascination usually reserved for anthropological ac-
    counts of obscure primitive societies. It is as if these researchers had
    brought back accounts from an organizational region that is concealed
    from observation and protected from rational analysis. Perhaps this
    sense of mystery surrounds top management activities because they
    derive from a set of skills that are embedded in individual action, in
    much the same way as those of the craftsperson. In both cases, skilled
    performance is characterized by sentient participation, contextuality,
    action-dependence, and personalism.

    What is different is that the craftsperson used action-centered skills
    in the service of actino-on materials and equipment, while the top man-
    ager's action-centered skills are applied in the service of actino-with.
    Like the seventeenth-century courtier, the top manager uses his or her
    bodily presence as an instrument of interpersonal power, influence,
    learning, and communication. The know-how that is developed in the
    course of managerial experience in actino-with remains largely implicit:
    managers themselves have difficulty describing what they do. Only the
    cleverest research can translate such embedded practice into expli-
    cated material suitable for analysis and discussion.

    [...]

    "The process is the sensing of the organization as a whole and the total
    situation relevant to it. It transcends the capacity of merely intellectual
    methods, and the techniques of discriminating the factors of the situa-
    tion. The terms pertinent to it are 'feeling,' 'judgment,' 'sense,' 'pro-
    portion,' 'balance,' 'appropriateness.' It is a matter of art rather than
    science, and is aesthetic rather than logical. For this reason it is recog-
    nized rather than described and is known by its effects rather than by
    analysis. ,,8

    -- 100-101

    Kotter stresses the implicit quality of the general managers' knowledge,
    noting that their agendas tended to be informal, nonquantitative, mental road
    maps highly related to "people" issues, rather than systematic, formal planning
    documents.

    -- 102

    Daniel Isenberg's research on "how senior managers think" has pen-
    etrated another layer of this, usually inarticulate, domain of executive
    management. 12 Isenberg found that top managers think in ways that are
    highly "intuitive" and integrated with action. 13 He concluded that the
    intuitive nature of executive behavior results from the inseparability of
    their thinking from their actions: "Since managers often 'know' what
    is right before they can analyze and explain it, they frequently act first
    and think later. Thinking is inextricably tied to action. . . . Managers
    develop thought about their companies and organizations not by ana-
    lyzing a problematic situation and then acting, but by thinking and
    acting in close concert.,,14 One manager described his own immersion
    in the action cycle: "It's as if your arms, your feet, and your body just
    move instinctively. You have a preoccupation with working capital, a
    preoccupation with capital expenditure, a preoccupation with people
    . . . and all this goes so fast that you don't even know whether it's
    completely rational, or it's part rational, part intuitive. 15

    [...]

    Kanter con-
    cluded that the manager's ability to "win acceptance" and to communi-
    cate was often more important than any substantive knowledge of the
    business. The feelings of comfort, efficiency, and trust that come with
    such shared meaning are triggered in a variety of ways by the manager's
    comportment. The nuances of nonverbal behavior and the signals em-
    bedded in physical appearance are an important aspect of such group
    participation. Because the tasks at the highest levels of the corporation
    are the most ambiguous, senior executives come to rely most heavily
    on the communicative ease that results from this shared intuitive world.

    -- 103

    Top managers' days and nights are filled to the breaking point with a myriad of
    activities, contacts, events, discussions, and meetings, which tend to be
    brief, rapid, and fragmented. Many students of managerial activity have
    proposed ways

    -- 105

    "Today the manager is the real data bank. . . . Unfortunately he is a
    walking and a talking data bank, but not a writing one. When he is busy,
    information ceases to flow. When he departs, so does the data bank.,,28 Lodged
    in the body and dependent upon presence and active display, the implicit heart
    of the executive's special genius appears to evade rationalization.

    -- 106-107

That's brilliant:

    In the case of executive activity, those elements most accessible
    to explication, and therefore rationalization, were carved out of the
    executive's immediate domain of concern. These more analytical or
    routine activities were projected into the functions of middle manage-
    ment, just as those functions were also absorbing new responsibilities
    for planning and coordination that had resulted from systematic analy-
    sis of the production process. Thus, the activities that made the execu-
    tive most special, based on action-centered skill, were left intact, while
    the more explicit and even routine aspects of executive responsibilities
    were pushed downward and materialized in a variety of middle-
    management functions. This contrasts with the case of craft workers,
    in which the action-centered skills that had made them so special were
    resealched, systematized, and expropriated upward. To put it bluntly,
    workers lost what was best in their jobs, the body as skill in the service
    of actin8-on, while executives lost what was worst in their jobs, retaining
    full enjoyment of the skilled body as an instrument of actin8-with.

    -- 107-108

In other words, automation and the robotization of the body that follows flows
downward in that particular kink of enterprise -- capitalist business and other
hierachical type of organizations with information-based management. From
_action with_ to _acting on_ (page 119).

Intelligence is extracted from the worked, deskilled, automated and robotized.
From oral to written communication, from her memory to a memory bank of some
sort. From her artisan skills of interpersonal relationships to standardized
procedures. In a movement downward the hierarchy.

How that phenomemon predates or is contemporanean to cybernetic-inspired
corporate management?

Also, today we see a discourse on replacing even top management with A/I
working according to "smartcontracts", which might be an assymptotic
ideological consequence of automating things downward, but that might be proved
wrong if we consider that there's no way these organizations could work without
any craftsmanship at it's top.

Sounds like if there's no way to fully automated a capitalist bussiness or
government body, even replacing it's management but at the same time there's
an urge to do just that. The net effect is an overconcentration of power
to an ever-diminishing managerial elite.

If more value is given to non-automated work, then this overconcentration
is directly related to wealth concentration.

I guess this whole mainstream discourse on automation is entirelly flawed.
It separates mind and body, hates the body, want it automated, a slave of the mind
enslaved in a dellusion to free itself even more as the mind is considered a slave
of the brain, it's material support.

The next step after the creation of middle-management was it's removal from
the organization by downsizing/delayering/outsourcing which happened after
this book was published.

    In 1925, the same year that Mary Parker Follett made her speech
    exhorting managers to become more scientific, William Henry
    Leffingwell published his well-known text, Office Mana8ement: Principles
    and Practice, which he dedicated to the Taylor Society in appreciation
    of its "inspirational and educational influence." Leffingwell presented
    a copy of his book to Carl Barth, one of Taylor's best-known disciples.
    That copy bore the following inscription: "It is with deep appreciation
    of the honor of knowing one of management's greatest minds that I sit
    at your feet and sign my name." Leffingwell was obsessed with the
    notion of bringing rational discipline to the office in much the same
    way that Taylor and his men were attempting to transform the shop
    floor. Though his was not the only treatise on the subject, it quickly
    became one of the most influential. 56 In an earlier work, published in
    1 91 7, Leffingwell had discussed "mechanical applications of the princi-
    ples of scientific management to the office." His new text was written
    to address the need for "original thought" concerning the fundamental
    principles of his discipline and their relationship to office management.
    Leffingwell summed up the message of his book with one sentence:
    "In a word, the aim of this new conception of office management is
    simplification. "

    [...]

    The overwhelming purpose of Leffingwell's approach to simplifica-
    tion was to fill the clerical workday with activities that were linked
    to a concrete task and to eliminate time spent on coordination and
    communication. This concern runs through almost every chapter of his
    850-page text; it is revealed most prominently in his minutely detailed
    discussions of the physical arrangement of the office and in his views
    on the organization, flow, planning, measurement, and control of office
    work.

    Leffingwell advocated what he called "the straight-line flow of
    work" as the chief method by which to eliminate any requirement for
    communication or coordination. The ideal condition, he said, was that
    desks should be so arranged that work could be passed from one to the
    other "without the necessity of the clerk even rising from his seat. . .

    [...]

    . . . Routine. . . tends to reduce communication. ,,58 Layout,
    standardization of methods, a well-organized messenger service, desk
    correspondence distributors, reliance on written instructions, delivery
    bags, pneumatic tubes, elevators, automatic conveyors, belt conveyors,
    cables, telautographs, telephones, phonographs, buzzers, bells, and
    horns-these were just some of the means Leffingwell advocated in
    order to insulate the clerk from extensive communicative demands.

    -- 117-119

Mind how such changes of reducing interpersonal communication, despite
raising production efficiency, also reduces worker self-organizing capacity
and class awareness.

    The requirements of actino-on associated with these new clerical jobs
    demanded more from the body as a source of effort than from skilled
    action or intellective competence. It is only at this stage, and in the
    context of this discontinuity, that the fate of the clerical job can be
    fruitfully compared to that of skilled work in industry.

    [...]

    Frequently, the jobs that were created had the
    effect of driving office workers into the role of laboring bodies, en-
    gulfing them in the private sentience of physical effort. Complaints
    about these jobs became complaints about bodies in pain. In 1 960 the
    International Labour Organization published a lengthy study of mecha-
    nization and automation in the office.

    [...]

    Clerks complained of being "treated like trained animals" because of the
    "uniformity and excessive simplification of the work of many machine
    operators."

    -- 119-120

Another form of [labor camp](/books/history/ibm-holocaust), it's mirror image:

    "Tabulat- ing machine operators, for instance, even when the controls are set
    for them and an automatic device stops the machine when something goes wrong,
    cannot let their attention flag. . . . The strain of this kind of close
    attentiveness to a repetitive operation has resulted in a rIsIng number of
    cases of mental and nervous disorders among clerical work- ers . . . physical
    and intellectual debility; disturbances of an emotional nature such as
    irritability, nervousness, hypersensitivity; insomnia; vari- ous functional
    disturbances-headaches, digestive and heart troubles; state of depression, etc.
    ,,61

    -- 120-121

    The Office, featured an article in 1 969 by the director
    of a New Jersey industrial engineering firm who said: "We know from
    our company's studies that manpower utilization in most offices-even
    those that are subject to work measurement controls-rarely exceeds
    60%. In some operations the percentage of utilization may fall below
    40%. At least 17% of the time, employees are literally doing nothing
    except walking around or talking. . . . While many companies have
    squeezed out much of the excess labor costs in their production opera-
    tions, only a few have given serious attention to the so called indirect
    labor or service operations. ,,62

    [...]

    "Clerical jobs are mea- sured just like factory jobs.

    Clerical costs can be controlled on
    any routine, Le., repetitive or semi-repetitive work. Non-repetitive
    tasks, such as research and development, cannot be economically mea-
    sured. Similarly, jobs such as receptionists, confidential secretaries,
    etc., do not lend themselves to control. ,,65

    -- 121-122

### Office technology as exile and integration

The whole chapter is worth reading. Some excerpts:

    One afternoon, after several weeks of participant observation and
    discussions with clerks and supervisors, I was returning to the office
    from a lunch with a group of employees when two of them beckoned
    me over to their desks, indicating that they had something to show me.
    They seated themselves at their workstations on either side of a tall
    gray partition. Then they pointed out a small rupture in the orderly,
    high-tech appearance of their work space: the metal seam in the parti-
    tion that separated their desks had been pried open.

    With the look of mischievous co-conspirators, they confided that
    they had inflicted this surgery upon the wall between them. Why? The
    small opening now made it possible to peek through and see if the
    other worker was at her seat, without having to stand up and peer over
    or around the wall. Through that aperture questions could be asked,
    advice could be given, and dinner menus could be planned. At the time
    I took this to be the effort of two women to humanize their surround-
    ings. While I still believe that is true, the weeks, months, and years that
    followed led me to a fuller appreciation of the significance of their
    action.

    Installing those partitions was the final step that completed the
    clerks' relegation to the realm of the machine. Exiled from the inter-
    personal world of office routines, each clerk became isolated and soli-
    tary. That interpersonal world involves the work of managing; it is the
    domain in which coordination and communication occur. These clerks
    not only had been denied benign forms of social intercourse but also
    had been expelled from the managerial world of actino-with that had
    formerly required them to accept, in some small degree, responsibility
    for the coordination of their office. Installing the partitions was one
    concrete technique, among others, designed to create the discontinuity
    needed to achieve Leffingwell's goal: to convert the clerk from an inter-
    personal operator to a laboring body, substituting communicative and
    coordinative responsibilities with the physical demands of continuous
    production.

    -- 125

    In many cases, organizational functions, events, and processes have been so
    extensively informated-converted into and displayed as information-that the
    technology can be said to have "textualized" the organizational environment.

    -- 126

    Why was it felt to be important and natural to check the ledgers?
    Many of the clerks experienced a loss of certainty similar to that of the
    pulp mill operators when they were deprived of concrete referents. In
    the office the referent function operated at a higher level of abstraction
    than in the mills. For these clerks, written words on pieces of paper
    had become a concrete and credible medium-for several reasons.
    First, paper is a three-dimensional object that carries sensory weight-
    it can be touched; carried; folded; in short, dominated. Secondly, writ-
    ing is a physical activity. The pen gives voice to the hand. Each written
    word is connected to the writer both through the intellectual relation-
    ship of authorship and through the immediate physical relationship of
    fingers and pen. In the act of writing there is a part of the self that is
    invested in and so identified with the thing written. It comes to be
    experienced as an extension of the self rather than an "otherness."
    This identification occurs so subtly, that it is rarely noticed until it has
    been taken away. Electronic text confronts the clerk with a stark sense
    of otherness. Text is impersonal; letters and numbers seem to appear
    without having been derived from an embodied process of authorship.
    They stand autonomously over and against the clerk who engages with
    them. A benefits analyst described the sensation:

        You can't justify anything now; you can't be sure of it or prove it
        because you have nothing down in writing. Without writing, you can't
        remember things, you can't keep track of things, there's no reasoning
        without writing. What we have now-you don't know where it comes
        from. It just comes at you.

    -- 130-131

Concentrating on concentrating: nano-genealogy of clerical work
---------------------------------------------------------------

Sounds like there's a paradox between the simplification of work -- the diminishing
knowledge required to do the task -- and the increased need for concentration in
the task accomplished -- not only because it was dificult to rollback transactions,
but also because of an increased pressure to do more.

        We really did not have a need for such intensive concentration be-
        fore. There are times when you are looking at the screen but you
        are not seeing what is there. That is a disaster. Even when you get
        comfortable with the system, you still have to concentrate; it's iust
        that you are not concentrating on concentrating. You learn how to
        do it, but the need doesn't go away.

    -- 131

Here I get a curious feeling. Which makes me get back to the origin of the term
"clerk" and "clerical work". This is what Norbert Elias tells us from his second
volume of [The Civilizing Process](/books/sociology/processo-civilizador):

    They entered this appararus by two main routes: 103 first through their growing
    share of secular posts, that is, positions previously filled by nobles; and secondly
    through their share of ecclesiastical poset, that is as clerks. The term _clerc_ began
    slowly to change its meaning from about the end of the twelfth century onwards;
    its ecclesiastical connotation receded and it referred more and more to a man who
    had studied, who could read and write Latin , though it may be that the first
    stages of an ecclesiastical career were for a time a prerequisite for this. Then, in
    conjunction with the extension of the administrative apparatus, both the them
    _clerc_ and certain kinds of university study were increasingly secularized. People
    no longer learned Latin exclusively to become members of the clergy, theu also
    learned it to become officials. To be sure, there were still bourgeois who entered
    the king's council simply on account of their commercial or organizational
    competence. But the majority of bourgeois attained the higher regions of
    government through study, through knowledge of canon and Roman law. Study
    became a normal means of social advancement for the sons of leading urban
    strata. Bourgeois elements slowly pushed back the noble and ecclesiastical
    elements in the government. The class of royal servants, of ''officials", became --
    in contrast to the situarion in Germany -- an exclusively bourgeois formation.

    [103] https://www.worldcat.org/title/philippe-le-long-roi-de-france-1316-1322-le-mecanisme-du-gouvernement/oclc/489867779

    -- 332

The same excerpt but from the portuguese translation:

    Eles ingressaram na máquina do governo através de dois caminhos principais:103
    inicialmente, graças a sua crescente participação em cargos seculares, isto é,
    em posições antes ocupadas por nobres e, depois, devido a sua participação em
    postos antes eclesiásticos, isto é, como amanuenses. O termo _clerc_ começou a
    mudar lentamente de significado a partir de fins do século XII, recuando para
    um plano inferior sua conotação eclesiástica e aplicando-se mais e mais a
    indivíduos que haviam estudado, que podiam ler e escrever latim, embora possa
    ser verdade que os primeiros estágios de uma carreira eclesiástica fossem, por
    algum tempo, precondição para isso. Em seguida, em paralelo com a ampliação da
    máquina administrativa, o termo _clerc_ e certos tipos de estudos universitários
    foram cada vez mais secularizados. As pessoas não aprendiam latim
    exclusivamente para se tornarem membros do clero, mas também para ingressar na
    carreira de servidores públicos. Para sermos exatos, também havia burgueses que
    passavam a integrar o conselho do rei simplesmente devido a sua competência
    comercial ou organizacional. A maioria dos burgueses, porém, chegava aos altos
    escalões do governo através do estudo, do conhecimento dos cânones e do Direito
    Romano. O estudo tornou-se um meio normal de progresso social para os filhos
    dos principais estratos urbanos. Lentamente, elementos burgueses suplantaram os
    elementos nobres e eclesiásticos no governo. A classe de servidores reais, ou
    “funcionários”, tornou-se —, em contraste com a situação vigente nos
    territórios germânicos — uma formação social exclusivamente burguesa.

    -- Da seção 22 da parte "Distribuição das Taxas de Poder no Interior da Unidade
       de Governo: Sua Importância para a Autoridade Central: A Formação do “Mecanismo
       Régio”"

The development both of the term _clerc_ and the change this activity took deserves
some attention.

In a sense, the clergy lives in a form of isolation, of exile.

Or, in another sentence, a clerc was someone who renounced the sensorial and the
material word to live a monastic life. What I just said?

    monastery (n.)

    c. 1400, from Old French monastere "monastery" (14c.) and directly from Late
    Latin monasterium, from Ecclesiastical Greek monasterion "a monastery," from
    monazein "to live alone," from monos "alone" (from PIE root *men- (4) "small,
    isolated"). With suffix -terion "place for (doing something)." Originally
    applied to houses of any religious order, male or female.

    -- https://www.etymonline.com/word/monastery

    men- (4)

    Proto-Indo-European root meaning "small, isolated."

    It forms all or part of: malmsey; manometer; monad; monarchy; monastery;
    monism; monist; monk; mono; mono-; monoceros; monochrome; monocle; monocular;
    monogamy; monogram; monolith; monologue; monomania; Monophysite; monopoly;
    monosyllable; monotony.

    It is the hypothetical source of/evidence for its existence is provided by:
    Greek monos "single, alone," manos "rare, sparse;" Armenian manr "thin,
    slender, small."

    -- https://www.etymonline.com/word/*men-?ref=etymonline_crossreference

    Noun

    monastērium n (genitive monastēriī); second declension

        (Medieval Latin) monastery quotations ▼
        (Medieval Latin) cell; area used by a monk.

    -- https://en.wiktionary.org/wiki/monasterium

    From Old French monastere, from Latin monastērium, from Ancient Greek
    μοναστήριον (monastḗrion, “hermit's cell”), from μόνος (mónos, “alone”).
    Doublet of minster.

    -- https://en.wiktionary.org/wiki/monastery

Clerical work can be considered those isolated, repetitive, monotonous tasks
separated from the daily, communal life.

Curiously enough, the `*men-` root also means:

    men- (1)

    Proto-Indo-European root meaning "to think," with derivatives referring to
    qualities and states of mind or thought.

    It forms all or part of: admonish; Ahura Mazda; ament; amentia; amnesia;
    amnesty; anamnesis; anamnestic; automatic; automaton; balletomane; comment;
    compos mentis; dement; demonstrate; Eumenides; idiomatic; maenad; -mancy;
    mandarin; mania; maniac; manic; mantic; mantis; mantra; memento; mens rea;
    mental; mention; mentor; mind; Minerva; minnesinger; mnemonic; Mnemosyne;
    money; monition; monitor; monster; monument; mosaic; Muse; museum; music;
    muster; premonition; reminiscence; reminiscent; summon.

    It is the hypothetical source of/evidence for its existence is provided by:
    Sanskrit manas- "mind, spirit," matih "thought," munih "sage, seer;" Avestan
    manah- "mind, spirit;" Greek memona "I yearn," mania "madness," mantis "one who
    divines, prophet, seer;" Latin mens "mind, understanding, reason," memini "I
    remember," mentio "remembrance;" Lithuanian mintis "thought, idea," Old Church
    Slavonic mineti "to believe, think," Russian pamjat "memory;" Gothic gamunds,
    Old English gemynd "memory, remembrance; conscious mind, intellect."

    [...]

    men- (2)

    Proto-Indo-European root meaning "to project."

    men- (3)

    Proto-Indo-European root meaning "to remain." It forms all or part of:
    maisonette; manor; manse; mansion; menage; menial; immanent; permanent; remain;
    remainder.

    -- https://www.etymonline.com/word/*men-?ref=etymonline_crossreference

To think in isolation, projecting, calculating. "Automaton" shares the same root.

We can also say tha clerical work can refer to a dedication to spiritualism or
philosophycal inquiry, freed from mundane affairs, desires an necessities.

To be continued:

* The monastic way is a mode of existence. But is different if someone chooses this path or is forced to it.
* Monotasking during large periods of time was enabled by civilization. Multitasking was the way if you had to pay attention all the time
  for dangers to your life. See [The burn-out society](/books/sociology/burnout-society) for discussion. It's related to the
  differentiation, specialization and automation of tasks. One needs someone's else, a third-party protection to be able to abstain from
  the environment and even from oneself and focus on abstract and to be able to deep reflection and medidation.
* How some contemporaneous clerical work tends more to multitasking and attention deficit.
* Any equivalent term in portuguese to "clerical work"?
* Class differentiation among both the catholic clergy and the modern monastic automated office, with high ranks of technomonks
  doing the thinking (and acting-with) and the lower clerks acting like automatons (acting-on).

[[!tag sociology technology history]]
