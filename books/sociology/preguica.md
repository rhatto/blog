[[!meta title="O direito à preguiça"]]

* Autor: Paul Lafarge
* Ano: 1880
* Info: [O Direito à Preguiça – Wikipédia, a enciclopédia livre](https://pt.wikipedia.org/wiki/O_Direito_%C3%A0_Pregui%C3%A7a)

## Temas

* Redução da jornada de trabalho.
* Automatização da produção.
* Ambiguidade interessante que inverte e brinca com a ideologia do trabalho,
  onde um proletariado com fixação pelo sofrimento e viciado por atividades
  extenuantes que força a burguesia supercomsumir a empregar-lhe na indústria.

## Trechos

    Uma boa operária só faz com o fuso cinco malhas por minuto, alguns teares
    circulares para tricotar fazem trinta mil no mesmo tempo. Cada minuto à
    máquina equivale, portanto, a cem horas de trabalho da operaria; ou então
    cada minuto de trabalho da máquina dá à operária dez dias de repouso.
    Aquilo que se passa com a indústria de malhas é mais ou menos verdade
    para todas as indústrias renovadas pela mecânica moderna. Mas que vemos
    nós? A medida que a máquina se aperfeiçoa e despacha o trabalho do
    homem com uma rapidez e uma precisão incessantemente crescentes, o
    operário, em vez de prolongar o seu repouso proporcionalmente, redobra de
    ardor, como se quisesse rivalizar com a máquina. Ó concorrência absurda e
    mortal!

    -- 12-13

    Uma vez acocorada na preguiça absoluta e desmoralizada pelo prazer
    forçado, a burguesia, apesar das dificuldades que teve nisso, adaptou-se ao
    seu novo estilo de vida. Encarou com horror qualquer alteração. A visão das
    miseráveis condições de existência aceites com resignação pela classe
    operária e a da degradação orgânica gerada pela paixão depravada pelo
    trabalho aumentava ainda mais a sua repulsa por qualquer imposição de
    trabalho e por qualquer restrição de prazeres.
    
    Foi precisamente então que, sem ter em conta a desmoralização que a
    burguesia tinha imposto a si própria como um dever social, os proletários
    resolveram infligir o trabalho aos capitalistas. Ingénuos, tomaram a sério as
    teorias dos economistas e dos moralistas sobre o trabalho e maltrataram os
    rins para infligir a sua prática aos capitalistas. O proletariado arvorou a
    divisa: Quem não trabalha, não come; Lyon, em 1831, levantou-se pelo
    chumbo ou pelo trabalho, os federados de 1871 declararam o seu
    levantamento a revolução do trabalho.
    
    A estes ímpetos de furor bárbaro, destrutivo de todo o prazer e de toda a
    preguiça burguesas, os capitalistas só podiam responder com uma
    repressão feroz, mas sabiam que, se tinham conseguido reprimir estas
    explosões revolucionárias, não tinham afogado no sangue dos seus
    gigantescos massacres a absurda idéia do proletariado de querer infligir o
    trabalho às classes ociosas e fartas, e foi para desviar essa infelicidade que
    se rodearam de pretorianos, de polícias, de magistrados, de carcereiros
    mantidos numa improdutividade laboriosa. Já não se podem ter ilusões
    sobre o caráter dos exércitos modernos, são mantidos em permanência
    apenas para reprimir "o inimigo interno"; e assim que os fortes de Paris e de
    Lyon não foram construídos para defender a cidade contra o estrangeiro,
    mas para o esmagar no caso de revolta. E se fosse preciso um exemplo sem
    réplica, citemos o exército da Bélgica, desse país de Cocagne do
    capitalismo; à sua neutralidade é garantida pelas potências européias e, no

    entanto, o seu exército é um dos mais fortes em proporção da população. Os
    gloriosos campos de batalha do bravo exército belga são as planícies do
    Borinage e de Charleroi, é no sangue dos mineiros e dos operários
    desarmados que os oficiais belgas ensangüentam as suas espadas e
    ganham os seus galões. As nações européias não tem exércitos nacionais,
    mas sim exércitos mercenários, que protegem os capitalistas contra o furor
    popular que os queria condenar a dez horas de mina ou de fábrica de fiação.
    Portanto, ao apertar o cinto, a classe operária desenvolveu para além do
    normal o ventre da burguesia condenada ao superconsumo.

    Para ser aliviada no seu penoso trabalho, a burguesia retirou da classe
    operária uma massa de homens muito superior à que continuava dedicada à
    produção útil e condenou-a, por seu turno, à improdutividade e ao
    superconsumo. Mas este rebanho de bocas inúteis, apesar da sua
    voracidade insaciável, não basta para consumir todas as mercadorias que os
    operários, embrutecidos pelo dogma do trabalho, produzem como maníacos,
    sem os quererem consumir e sem sequer pensarem se se encontrarão
    pessoas para os consumir.
    
    Em presença desta dupla loucura dos trabalhadores, de se matarem de
    supertrabalho e de vegetarem na abstinência, o grande problema da
    produção capitalista já não é encontrar produtores e multiplicar as suas
    forças, mas descobrir consumidores, excitar os seus apetites e criar-lhes
    necessidades fictícias. Uma vez que os operários europeus, que tremem de
    frio e de fome, recusam usar os tecidos que eles próprios tecem, beber os
    vinhos que eles próprios colhem, os pobres fabricantes, como espertalhões,
    devem correr aos antípodas para procurar quem os usará e quem os beberá:
    são centenas de milhões e de biliões que a Europa exporta todos os anos
    para os quatro cantos do mundo, para populações que não têm nada que
    fazer com esses produtos.

    [...]

    Mas tudo é insuficiente: o burguês que se farta, a classe doméstica que
    ultrapassa a classe produtiva, as nações estrangeiras e bárbaras que se
    enchem de mercadorias européias; nada, nada pode conseguir dar vazão às
    montanhas de produtos que se amontoam maiores e mais altas do que as
    pirâmides do Egito: a produtividade dos operários europeus desafia todo o
    consumo, todo o desperdício. Os fabricantes, doidos, já não sabem que
    fazer, já não conseguem encontrar matéria-prima para satisfazer a paixão
    desordenada, depravada, que os seus operários têm pelo trabalho. Nos
    nossos distritos onde há lã, desfiam-se trapos manchados e meio podres,
    fazem-se com eles panos chamados de renascimento, que duram o mesmo
    que as promessas eleitorais;

    [...]

    Todos os nossos produtos são adulterados para facilitar o seu escoamento e
    abreviar a sua existência. A nossa época será chamada a idade da falsificação,
    tal como as primeiras épocas da humanidade receberam os nomes de idade da
    pedra, idade de bronze, pelo caráter da sua produção.

    -- 15-17

    Eis a grande experiência inglesa, eis a experiência de alguns capitalistas
    inteligentes, ela demonstra irrefutavelmente que, para reforçar a
    produtividade humana, tem de se reduzir as horas de trabalho e multiplicar
    os dias de pagamento e os feriados, e o povo francês não está convencido.
    Mas se uma miserável redução de duas horas aumentou em dez anos a
    produção inglesa em cerca de um terço (7), que ritmo vertiginoso imprimiria
    à produção francesa uma redução geral de três horas no dia de trabalho? Os
    operários não conseguem compreender que, cansando-se excessivamente,
    esgotam as suas forças antes da idade de se tornarem incapazes para
    qualquer trabalho; que absorvidos, embrutecidos por um único vício, já não
    são homens, mas sim restos de homens; que matam neles todas as belas
    faculdades para só deixarem de pé, e luxuriante, a loucura furiosa do
    trabalho.

    -- 18

    O idiotas! é porque trabalhais demais que a ferramenta industrial se desenvolve
    lentamente.

    -- 19

    O protestantismo, que era a religião cristã adaptada às novas
    necessidades industriais e comerciais da burguesia, preocupou-se menos
    com o descanso popular; destronou no céu os santos para abolir na terra as
    suas festas. A reforma religiosa e o livre pensamento filosófico não eram
    senão pretextos que permitiram à burguesia jesuíta e voraz escamotear os
    dias de festa do popular.

    -- 19 (nota de rodapé)

    "O preconceito da escravatura dominava o espírito de Pitágoras e de
    Aristóteles", escreveu-se desdenhosamente; e no entanto Aristóteles previa
    que "se cada utensílio pudesse executar sem intimação, ou então por si só, a
    sua função própria, tal como as obras-primas de Dédalo se moviam por si
    mesmas ou tal como os tripés de Vulcano que se punham espontaneamente
    ao seu trabalho sagrado; se, por exemplo, as lançadeiras dos tecelões
    tecessem por si próprias, o chefe de oficina já não teria necessidade de
    ajudantes, nem o senhor de escravos".
    
    O sonho de Aristóteles é a nossa realidade. As nossas máquinas a vapor,
    com membros de aço, infatigáveis, de maravilhosa e inesgotável
    fecundidade, realizam por si próprias docilmente o seu trabalho sagrado; e,
    no entanto, o gênio dos grandes filósofos do capitalismo continua a ser
    dominado pelo preconceito do salariado, a pior das escravaturas. Ainda não
    compreendem que a máquina é o redentor da humanidade, o Deus que
    resgatará o homem das sórdidas artes e do trabalho assalariado, o Deus que
    lhe dará tempos livres e a liberdade.

    -- 26
