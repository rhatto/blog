[[!meta title="A sociedade contra o Estado"]]

* Autor: Pierre Clastres

## Trechos

    Se entendermos por técnica o conjunto dos processos de que se munem os homens,
    não para assegurarem o domínio absoluto da natureza (isso só vale para o nosso
    mundo e seu insano projeto cartesiano cujas consequências ecológicas mal
    começamos a media), mas para garantir um domínio do meio natural _adaptado e
    relativo às suas necessidades_, então não mais podemos falar em inferioridade
    técnica das sociedades primitivas: elas demonstram uma capacidade de satisfazer
    suas necessidades pelo menos igual àquela de que se orgulha a sociedade
    industrial e técnica.
    
    [...]
    
    Não existe portanto hierarquia no campo da técnica, nem tecnologia superior
    ou inferior; só se pode medir um equipamento tecnológico pela sua capacidade
    de satisfazer, num determinado meio, as necessidades da sociedade.
    
    -- 203
