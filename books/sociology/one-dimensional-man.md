[[!meta title="One-Dimensional Man"]]

* Author: Hebert Marcuse
* Terms: institutionalized, adjusted sublimation

## Snippets

### Intro

    From the beginning, any critical theory of society is thus confronted with the
    problem of historical objectivity, a problem which arises at the two points
    where the analysis implies value judgments:

    1. the judgment that human life is worth living, or rather can be and ought to
    be made worth living. This judgment underlies all intellectual effort; it is
    the a priori of social theory, and its rejection (which is perfectly logical)
    rejects theory itself;

    2. the judgment that, in a given society, specific possibilities exist for the
    amelioration of human life and specific ways and means of realizing these
    possibilities. Critical analysis has to demonstrate the objective validity of
    these judgments, and the demonstration has to proceed on empirical grounds. The
    established society has available an ascertainable quantity and quality of
    intellectual and material resources. How can these resources be used for the
    optimal development and satisfaction of individual needs and faculties with a
    minimum of toil and misery? Social theory is historical theory, and history is
    the realm of chance in the realm of necessity. Therefore, among the various
    possible and actual modes of organizing and utilizing the available resources,
    which ones offer the greatest chance of an optimal development?

    [...]

    The “possibilities” must be within the reach of the respective society; they
    must be definable goals of practice. By the same token, the abstraction from
    the established institutions must be expressive of an actual tendency—that is,
    their transformation must be the real need of the underlying population. Social
    theory is concerned with the historical alternatives which haunt the
    established society as subversive tendencies and forces. The values attached to
    the alternatives do become facts when they are translated into reality by
    historical practice. The theoretical concepts terminate with social change.

    But here, advanced industrial society confronts the critique with a situation
    which seems to deprive it of its very basis. Technical progress, extended to a
    whole system of domination and coordination, creates forms of life (and of
    power) which appear to reconcile the forces opposing the system and to defeat
    or refute all protest in the name of the historical prospects of freedom from
    toil and domination. Contemporary society seems to be capable of containing
    social change—qualitative change which would establish essentially different
    institutions, a new direction of the productive process, new modes of human
    existence.

    [...]

    As a technological universe, advanced industrial society is a political
    universe, the latest stage in the realization of a specific historical
    project—namely, the experience, transformation, and organization of nature as
    the mere stuff of domination.

    As the project unfolds, it shapes the entire universe of discourse and action,
    intellectual and material culture. In the medium of technology, culture,
    politics, and the economy merge into an omnipresent system which swallows up or
    repulses all alternatives. The productivity and growth potential of this system
    stabilize the society and contain technical progress within the framework of
    domination. Technological rationality has become political rationality.

### Freedom in negative terms

    Contemporary industrial civilization demonstrates that it has reached the stage
    at which “the free society” can no longer be adequately defined in the
    traditional terms of economic, political, and intellectual liberties, not
    because these liberties have become insignificant, but because they are too
    significant to be confined within the traditional forms. New modes of
    realization are needed, corresponding to the new capabilities of society.

    Such new modes can be indicated only in negative terms because they would
    amount to the negation of the prevailing modes. Thus economic freedom would
    mean freedom from the economy—from being controlled by economic forces and
    relationships; freedom from the daily struggle for existence, from earning a
    living. Political freedom would mean liberation of the individuals from
    politics over which they have no effective control. Similarly, intellectual
    freedom would mean the restoration of individual thought now absorbed by mass
    communication and indoctrination, abolition of “public opinion” together with
    its makers. The unrealistic sound of these propositions is indicative, not of
    their utopian character, but of the strength of the forces which prevent their
    realization. The most effective and enduring form of warfare against liberation
    is the implanting of material and intellectual needs that perpetuate obsolete
    forms of the struggle for existence.

    The intensity, the satisfaction and even the character of human needs, beyond
    the biological level, have always been preconditioned. Whether or not the
    possibility of doing or leaving, enjoying or destroying, possessing or
    rejecting something is seized as a need depends on whether or not it can be
    seen as desirable and necessary for the prevailing societal institutions and
    interests. In this sense, human needs are historical needs and, to the extent
    to which the society demands the repressive development of the individual, his
    needs themselves and their claim for satisfaction are subject to overriding
    critical standards.

### The irrationality of the rational

    We are again confronted with one of the most vexing aspects of advanced
    industrial civilization: the rational character of its irrationality. Its
    productivity and efficiency, its capacity to increase and spread comforts, to
    turn waste into need, and destruction into construction, the extent to which
    this civilization transforms the object world into an extension of man’s mind
    and body makes the very notion of alienation questionable.

    [...]

    But in the contemporary period, the technological controls appear to be the
    very embodiment of Reason for the benefit of all social groups and interests—to
    such an extent that all contradiction seems irrational and all counteraction
    impossible.

    No wonder then that, in the most advanced areas of this civilization, the
    social controls have been introjected to the point where even individual
    protest is affected at its roots. The intellectual and emotional refusal “to go
    along” appears neurotic and impotent.

    [...]

    But the term “introjection” perhaps no longer describes the way in which the
    individual by himself reproduces and perpetuates the external controls
    exercised by his society. Introjection suggests a variety of relatively
    spontaneous processes by which a Self (Ego) transposes the “outer” into the
    “inner.” Thus introjection implies the existence of an inner dimension
    distinguished from and even antagonistic to the external exigencies—an
    individual consciousness and an individual unconscious apart from public
    opinion and behavior.3 The idea of “inner freedom” here has its reality: it
    designates the private space in which man may become and remain “himself.”

    Today this private space has been invaded and whittled down by technological
    reality. Mass production and mass distribution claim the entire individual, and
    industrial psychology has long since ceased to be confined to the factory. The
    manifold processes of introjection seem to be ossified in almost mechanical
    reactions. The result is, not adjustment but mimesis: an immediate
    identification of the individual with his society and, through it, with the
    society as a whole.

### One-dimensionality

    Thus emerges a pattern of one-dimensional thought and behavior in which ideas,
    aspirations, and objectives that, by their content, transcend the established
    universe of discourse and action are either repelled or reduced to terms of
    this universe. They are redefined by the rationality of the given system and of
    its quantitative extension.

    The trend may be related to a development in scientific method: operationalism
    in the physical, behaviorism in the social sciences. The common feature is a
    total empiricism in the treatment of concepts; their meaning is restricted to
    the representation of particular operations and behavior. The operational point
    of view is well illustrated by P. W. Bridgman’s analysis of the concept of
    length:5

        We evidently know what we mean by length if we can tell what the length of any
        and every object is, and for the physicist nothing more is required. To find
        the length of an object, we have to perform certain physical operations. The
        concept of length is therefore fixed when the operations by which length is
        measured are fixed: that is, the concept of length involves as much and nothing
        more than the set of operations by which length is determined. In general, we
        mean by any concept nothing more than a set of operations; the concept is
        synonymous with the corresponding set of operations.

    Bridgman has seen the wide implications of this mode of thought for the society
    at large:6

        To adopt the operational point of view involves much more than a mere
        restriction of the sense in which we understand ‘concept,’ but means a
        far-reaching change in all our habits of thought, in that we shall no longer
        permit ourselves to use as tools in our thinking concepts of which we cannot
        give an adequate account in terms of operations.

    Bridgman’s prediction has come true. The new mode of thought is today the
    predominant tendency in philosophy, psychology, sociology, and other fields.
    Many of the most seriously troublesome concepts are being “eliminated” by
    showing that no adequate account of them in terms of operations or behavior can
    be given.

    [...]

    Outside the academic establishment, the “far-reaching change in all our habits
    of thought” is more serious. It serves to coordinate ideas and goals with those
    exacted by the prevailing system, to enclose them in the system, and to repel
    those which are irreconcilable with the system. The reign of such a
    one-dimensional reality does not mean that materialism rules, and that the
    spiritual, metaphysical, and bohemian occupations are petering out. On the
    contrary, there is a great deal of “Worship together this week,” “Why not try
    God,” Zen, existentialism, and beat ways of life, etc. But such modes of
    protest and transcendence are no longer contradictory to the status quo and no
    longer negative. They are rather the ceremonial part of practical behaviorism,
    its harmless negation, and are quickly digested by the status quo as part of
    its healthy diet.

    [...]

    Such limitation of thought is certainly not new. Ascending modern rationalism,
    in its speculative as well as empirical form, shows a striking contrast between
    extreme critical radicalism in scientific and philosophic method on the one
    hand, and an uncritical quietism in the attitude toward established and
    functioning social institutions. Thus Descartes’ ego cogitans was to leave the
    “great public bodies” untouched, and Hobbes held that “the present ought always
    to be preferred, maintained, and accounted best.” Kant agreed with Locke in
    justifying revolution if and when it has succeeded in organizing the whole and
    in preventing subversion.

### Progress, abolition of labor, totalitarianism

    The society bars a whole type of oppositional operations and behavior;
    consequently, the concepts pertaining to them are rendered illusory or
    meaningless. Historical transcendence appears as metaphysical transcendence,
    not acceptable to science and scientific thought. The operational and
    behavioral point of view, practiced as a “habit of thought” at large, becomes
    the view of the established universe of discourse and action, needs and
    aspirations.

    “Progress” is not a neutral term; it moves toward specific ends, and these ends
    are defined by the possibilities of ameliorating the human condition. Advanced
    industrial society is approaching the stage where continued progress would
    demand the radical subversion of the prevailing direction and organization of
    progress. This stage would be reached when material production (including the
    necessary services) becomes automated to the extent that all vital needs can be
    satisfied while necessary labor time is reduced to marginal time. From this
    point on, technical progress would transcend the realm of necessity, where it
    served as the instrument of domination and exploitation which thereby limited
    its rationality; technology would become subject to the free play of faculties
    in the struggle for the pacification of nature and of society.

    Such a state is envisioned in Marx’s notion of the “abolition of labor.” The
    term “pacification of existence” seems better suited to designate the
    historical alternative of a world which—through an international conflict which
    transforms and suspends the contradictions within the established
    societies—advances on the brink of a global war. “Pacification of existence”
    means the development of man’s struggle with man and with nature, under
    conditions where the competing needs, desires, and aspirations are no longer
    organized by vested interests in domination and scarcity—an organization which
    perpetuates the destructive forms of this struggle.

    Today’s fight against this historical alternative finds a firm mass basis in
    the underlying population, and finds its ideology in the rigid orientation of
    thought and behavior to the given universe of facts. Validated by the
    accomplishments of science and technology, justified by its growing
    productivity, the status quo defies all transcendence. Faced with the
    possibility of pacification on the grounds of its technical and intellectual
    achievements, the mature industrial society closes itself against this
    alternative. Operationalism, in theory and practice, becomes the theory and
    practice of containment. Underneath its obvious dynamics, this society is a
    thoroughly static system of life: self-propelling in its oppressive
    productivity and in its beneficial coordination. Containment of technical
    progress goes hand in hand with its growth in the established direction. In
    spite of the political fetters imposed by the status quo, the more technology
    appears capable of creating the conditions for pacification, the more are the
    minds and bodies of man organized against this alternative.

    The most advanced areas of industrial society exhibit throughout these two
    features: a trend toward consummation of technological rationality, and
    intensive efforts to contain this trend within the established institutions.
    Here is the internal contradiction of this civilization: the irrational element
    in its rationality. It is the token of its achievements. The industrial society
    which makes technology and science its own is organized for the
    ever-more-effective domination of man and nature, for the ever-more-effective
    utilization of its resources. It becomes irrational when the success of these
    efforts opens new dimensions of human realization. Organization for peace is
    different from organization for war; the institutions which served the struggle
    for existence cannot serve the pacification of existence. Life as an end is
    qualitatively different from life as a means.

    [...]

    Qualitative change also involves a change in the technical basis on which this
    society rests—one which sustains the economic and political institutions
    through which the “second nature” of man as an aggressive object of
    administration is stabilized.

    [...]

    To be sure, labor must precede the reduction of labor, and industrialization
    must precede the development of human needs and satisfactions. But as all
    freedom depends on the conquest of alien necessity, the realization of freedom
    depends on the techniques of this conquest. The highest productivity of labor
    can be used for the perpetuation of labor, and the most efficient
    industrialization can serve the restriction and manipulation of needs.

    When this point is reached, domination—in the guise of affluence and
    liberty—extends to all spheres of private and public existence, integrates all
    authentic opposition, absorbs all alternatives. Technological rationality
    reveals its political character as it becomes the great vehicle of better
    domination, creating a truly totalitarian universe in which society and nature,
    mind and body are kept in a state of permanent mobilization for the defense of
    this universe.

### Revolution

    The classical Marxian theory envisages the transition from capitalism to
    socialism as a political revolution: the proletariat destroys the political
    apparatus of capitalism but retains the technological apparatus, subjecting it
    to socialization. There is continuity in the revolution: technological
    rationality, freed from irrational restrictions and destructions, sustains and
    consummates itself in the new society. It is interesting to read a Soviet
    Marxist statement on this continuity, which is of such vital importance for the
    notion of socialism as the determinate negation of capitalism

    [...]

    To be sure, Marx held that organization and direction of the productive
    apparatus by the “immediate producers” would introduce a qualitative change in
    the technical continuity: namely, production toward the satisfaction of freely
    developing individual needs. However, to the degree to which the established
    technical apparatus engulfs the public and private existence in all spheres of
    society—that is, becomes the medium of control and cohesion in a political
    universe which incorporates the laboring classes—to that degree would the
    qualitative change involve a change in the technological structure itself. And
    such change would presuppose that the laboring classes are alienated from this
    universe in their very existence, that their consciousness is that of the total
    impossibility to continue to exist in this universe, so that the need for
    qualitative change is a matter of life and death. Thus, the negation exists
    prior to the change itself, the notion that the liberating historical forces
    develop within the established society is a cornerstone of Marxian theory.2

### Hell

    Those whose life is the hell of the Affluent Society are kept in line by a
    brutality which revives medieval and early modern practices. For the other,
    less underprivileged people, society takes care of the need for liberation by
    satisfying the needs which make servitude palatable and perhaps even
    unnoticeable, and it accomplishes this fact in the process of production
    itself.

### Automation

    (1) Mechanization is increasingly reducing the quantity and intensity of physical
    energy expended in labor. This evolution is of great bearing on the Marxian
    concept of the worker (proletarian). To Marx, the proletarian is primarily the
    manual laborer who expends and exhausts his physical energy in the work
    process, even if he works with machines. The purchase and use of this physical
    energy, under subhuman conditions, for the private appropriation of
    surplus-value entailed the revolting inhuman aspects of exploitation; the
    Marxian notion denounces the physical pain and misery of labor. This is the
    material, tangible element in wage slavery and alienation—the physiological and
    biological dimension of classical capitalism.

        “Pendant les siècles passés, une cause importante d’aliénation résidait dans le
        fait que l’être humain prêtait son individualité biologique à l’organisation
        technique: il était porteur d’outils; les ensembles techniques ne pouvaient se
        constituer qu’en incorporant l’homme comme porteur d’outils. Le caractère
        déformant de la profession était à la fois psychique et somatique.”3

        3. “During the past centuries, one important reason for alienation was that the
        human being lent his biological individuality to the technical apparatus: he
        was the bearer of tools; technical units could not be established without
        incorporating man as bearer of tools into them. The nature of this occupation
        was such that it was both psychologically and physiologically deforming in its
        effect.” Gilbert Simondon, Du Mode d’existence des objets techniques (Paris:
        Aubier, 1958), p. 103, note.

    Now the ever-more-complete mechanization of labor in advanced capitalism, while
    sustaining exploitation, modifies the attitude and the status of the exploited.
    Within the technological ensemble, mechanized work in which automatic and
    semi-automatic reactions fill the larger part (if not the whole) of labor time
    remains, as a life-long occupation, exhausting, stupefying, inhuman
    slavery—even more exhausting because of increased speed-up, control of the
    machine operators (rather than of the product), and isolation of the workers
    from each other.4 To be sure, this form of drudgery is expressive of arrested,
    partial automation, of the coexistence of automated, semi-automated, and
    non-automated sections within the same plant, but even under these conditions,
    “for muscular fatigue technology has substituted tension and/or mental
    effort.”5 For the more advanced automated plants, the transformation of
    physical energy into technical and mental skills is emphasized:

        “… skills of the head rather than of the hand, of the logician rather than the
        craftsman; of nerve rather than muscle; of the pilot rather than the manual
        worker; of the maintenance man rather than the operator.”6

    This kind of masterly enslavement is not essentially different from that of the
    typist, the bank teller, the high-pressure salesman or saleswoman, and the
    television announcer. Standardization and the routine assimilate productive and
    non-productive jobs. The proletarian of the previous stages of capitalism was
    indeed the beast of burden, by the labor of his body procuring the necessities
    and luxuries of life while living in filth and poverty. Thus he was the living
    denial of his society.7 In contrast, the organized worker in the advanced areas
    of the technological society lives this denial less conspicuously and, like the
    other human objects of the social division of labor, he is being incorporated
    into the technological community of the administered population. Moreover, in
    the most successful areas of automation, some sort of technological community
    seems to integrate the human atoms at work. The machine seems to instill some
    drugging rhythm in the operators:

        “It is generally agreed that interdependent motions performed by a group of
        persons which follow a rhythmic pattern yield satisfaction—quite apart from
        what is being accomplished by the motions”;8 and the sociologist-observer
        believes this to be a reason for the gradual development of a “general climate”
        more “favorable both to production and to certain important kinds of human
        satisfaction.” He speaks of the “growth of a strong in-group feeling in each
        crew” and quotes one worker as stating: “All in all we are in the swing of
        things …”9

    The phrase admirably expresses the change in mechanized enslavement:
    things swing rather than oppress, and they swing the human instrument—not only
    its body but also its mind and even its soul. A remark by Sartre elucidates the
    depth of the process:

        “Aux premiers temps des machines semi-automatiques, des enquêtes ont montré que
        les ouvrières spécialisées se laissaient aller, en travaillant, à une rêverie
        d’ordre sexuel, elles se rappellaient la chambre, le lit, la nuit, tout ce qui
        ne concerne que la personne dans la solitude du couple fermé sur soi. Mais
        c’est la machine en elle qui rêvait de caresses.…”10 The machine process in the
        technological universe breaks the innermost privacy of freedom and joins
        sexuality and labor in one unconscious, rhythmic automatism—a process which
        parallels the assimilation of jobs.10

        10. “Shortly after semi-automatic machines were introduced, investigations
        showed that female skilled workers would allow themselves to lapse while
        working into a sexual kind of daydream; they would recall the bedroom, the bed,
        the night and all that concerns only the person within the solitude of the
        couple alone with itself. But it was the machine in her which was dreaming of
        caresses …” Jean-Paul Sartre, Critique de la raison dialectique, tome I (Paris:
        Gallimard, 1960), p. 290.

    The machine process in the technological universe breaks the innermost privacy
    of freedom and joins sexuality and labor in one unconscious, rhythmic
    automatism—a process which parallels the assimilation of jobs.

    [...]

    (2) The assimilating trend shows forth in the occupational stratification. In
    the key industrial establishments, the “blue-collar” work force declines in
    relation to the “white-collar” element; the number of non-production workers
    increases.11 This quantitative change refers back to a change in the character
    of the basic instruments of production.12 At the advanced stage of
    mechanization, as part of the technological reality, the machine is not

    “une unité absolue, mais seulement une réalité technique individualisée,
    ouverte selon deux voies: celle de la relation aux éléments, et celle des
    relations interindividuelles dans l’ensemble technique.”13

    13. “an absolute unity, but only an individualized technical reality open in
    two directions, that of the relation to the elements and that of the relation
    among the individuals in the technical whole.” Gilbert Simondon, loc. cit., p.
    146.

    [...]

    To the extent to which the machine becomes itself a system of mechanical tools
    and relations and thus extends far beyond the individual work process, it
    asserts its larger dominion by reducing the “professional autonomy” of the
    laborer and integrating him with other professions which suffer and direct the
    technical ensemble. To be sure, the former “professional” autonomy of the
    laborer was rather his professional enslavement. But this specific mode of
    enslavement was at the same time the source of his specific, professional power
    of negation—the power to stop a process which threatened him with annihilation
    as a human being. Now the laborer is losing the professional autonomy which
    made him a member of a class set off from the other occupational groups because
    it embodied the refutation of the established society.

    The technological change which tends to do away with the machine as individual
    instrument of production, as “absolute unit,” seems to cancel the Marxian
    notion of the “organic composition of capital” and with it the theory of the
    creation of surplus value. According to Marx, the machine never creates value
    but merely transfers its own value to the product, while surplus value remains
    the result of the exploitation of living labor. The machine is embodiment of
    human labor power, and through it, past labor (dead labor) preserves itself and
    determines living labor. Now automation seems to alter qualitatively the
    relation between dead and living labor; it tends toward the point where
    productivity is determined “by the machines, and not by the individual
    output.”14 Moreover, the very measurement of individual output becomes
    impossible:

        “Automation in its largest sense means, in effect, the end of measurement of
        work.… With automation, you can’t measure output of a single man; you now have
        to measure simply equipment utilization. If that is generalized as a kind of
        concept … there is no longer, for example, any reason at all to pay a man by
        the piece or pay him by the hour,” that is to say, there is no more reason to
        keep up the “dual pay system” of salaries and wages.”15

    Daniel Bell, the author of this report, goes further; he links this
    technological change to the historical system of industrialization itself: the
    meaning of industrialization did not arise with the introduction of factories,
    it “arose out of the measurement of work. It’s when work can be measured, when
    you can hitch a man to the job, when you can put a harness on him, and measure
    his output in terms of a single piece and pay him by the piece or by the hour,
    that you have got modern industrialization.”16

### Servitude

    (4) The new technological work-world thus enforces a weakening of the negative
    position of the working class: the latter no longer appears to be the living
    contradiction to the established society. This trend is strengthened by the
    effect of the technological organization of production on the other side of the
    fence: on management and direction. Domination is transfigured into
    administration.21 The capitalist bosses and owners are losing their identity as
    responsible agents; they are assuming the function of bureaucrats in a
    corporate machine. Within the vast hierarchy of executive and managerial boards
    extending far beyond the individual establishment into the scientific
    laboratory and research institute, the national government and national
    purpose, the tangible source of exploitation disappears behind the façade of
    objective rationality. Hatred and frustration are deprived of their specific
    target, and the technological veil conceals the reproduction of inequality and
    enslavement.22 With technical progress as its instrument, unfreedom—in the
    sense of man’s subjection to his productive apparatus—is perpetuated and
    intensified in the form of many liberties and comforts. The novel feature is
    the overwhelming rationality in this irrational enterprise, and the depth of
    the preconditioning which shapes the instinctual drives and aspirations of the
    individuals and obscures the difference between false and true consciousness.
    For in reality, neither the utilization of administrative rather than physical
    controls (hunger, personal dependence, force), nor the change in the character
    of heavy work, nor the assimilation of occupational classes, nor the
    equalization in the sphere of consumption compensate for the fact that the
    decisions over life and death, over personal and national security are made at
    places over which the individuals have no control. The slaves of developed
    industrial civilization are sublimated slaves, but they are slaves, for slavery
    is determined

        “pas par l’obéissance, ni par la rudesse des labeurs, mais par le statu
        d’instrument et la réduction de l’homme à l’état de chose.”23

        23. “neither by obedience nor by hardness of labor but by the status of being a
        mere instrument, and the reduction of man to the state of a thing.” François
        Perroux, La Coexistence pacifique, (Paris, Presses Universitaires, 1958), vol.
        III, p. 600.

    This is the pure form of servitude: to exist as an instrument, as a thing. And
    this mode of existence is not abrogated if the thing is animated and chooses
    its material and intellectual food, if it does not feel its being-a-thing, if
    it is a pretty, clean, mobile thing. Conversely, as reification tends to become
    totalitarian by virtue of its technological form, the organizers and
    administrators themselves become increasingly dependent on the machinery which
    they organize and administer. And this mutual dependence is no longer the
    dialectical relationship between Master and Servant, which has been broken in
    the struggle for mutual recognition, but rather a vicious circle which encloses
    both the Master and the Servant. Do the technicians rule, or is their rule that
    of the others, who rely on the technicians as their planners and executors?

    [...]

    A vicious circle seems indeed the proper image of a society which is
    self-expanding and self-perpetuating in its own preestablished direction—driven
    by the growing needs which it generates and, at the same time, contains.

### Culture

    The greatness of a free literature and art, the ideals of humanism, the sorrows
    and joys of the individual, the fulfillment of the personality are important
    items in the competitive struggle between East and West. They speak heavily
    against the present forms of communism, and they are daily administered and
    sold. The fact that they contradict the society which sells them does not
    count. Just as people know or feel that advertisements and political platforms
    must not be necessarily true or right, and yet hear and read them and even let
    themselves be guided by them, so they accept the traditional values and make
    them part of their mental equipment. If mass communications blend together
    harmoniously, and often unnoticeably, art, politics, religion, and philosophy
    with commercials, they bring these realms of culture to their common
    denominator—the commodity form. The music of the soul is also the music of
    salesmanship. Exchange value, not truth value counts. On it centers the
    rationality of the status quo, and all alien rationality is bent to it.

    As the great words of freedom and fulfillment are pronounced by campaigning
    leaders and politicians, on the screens and radios and stages, they turn into
    meaningless sounds which obtain meaning only in the context of propaganda,
    business, discipline, and relaxation. This assimilation of the ideal with
    reality testifies to the extent to which the ideal has been surpassed. It is
    brought down from the sublimated realm of the soul or the spirit or the inner
    man, and translated into operational terms and problems. Here are the
    progressive elements of mass culture. The perversion is indicative of the fact
    that advanced industrial society is confronted with the possibility of a
    materialization of ideals. The capabilities of this society are progressively
    reducing the sublimated realm in which the condition of man was represented,
    idealized, and indicted. Higher culture becomes part of the material culture.
    In this transformation, it loses the greater part of its truth.

    [...]

    Domination has its own aesthetics, and democratic domination has its democratic
    aesthetics. It is good that almost everyone can now have the fine arts at his
    fingertips, by just turning a knob on his set, or by just stepping into his
    drugstore. In this diffusion, however, they become cogs in a culture-machine
    which remakes their content.

    [...]

    Obviously, the physical transformation of the world entails the mental
    transformation of its symbols, images, and ideas. Obviously, when cities and
    highways and National Parks replace the villages, valleys, and forests; when
    motorboats race over the lakes and planes cut through the skies—then these
    areas lose their character as a qualitatively different reality, as areas of
    contradiction.

    And since contradiction is the work of the Logos—rational confrontation of
    “that which is not” with “that which is”—it must have a medium of
    communication. The struggle for this medium, or rather the struggle against its
    absorption into the predominant one-dimensionality, shows forth in the
    avant-garde efforts to create an estrangement which would make the artistic
    truth again communicable.

    Bertolt Brecht has sketched the theoretical foundations for these efforts. The
    total character of the established society confronts the playwright with the
    question of whether it is still possible to “represent the contemporary world
    in the theater”—that is, represent it in such a manner that the spectator
    recognizes the truth which the play is to convey. Brecht answers that the
    contemporary world can be thus represented only if it is represented as subject
    to change3—as the state of negativity which is to be negated. This is doctrine
    which has to be learned, comprehended, and acted upon; but the theater is and
    ought to be entertainment, pleasure. However, entertainment and learning are
    not opposites; entertainment may be the most effective mode of learning. To
    teach what the contemporary world really is behind the ideological and material
    veil, and how it can be changed, the theater must break the spectator’s
    identification with the events on the stage.
    Not empathy and feeling, but distance and reflection are required. The
    “estrangement-effect” (Verfremdungseffekt) is to produce this dissociation in
    which the world can be recognized as what it is. “The things of everyday life
    are lifted out of the realm of the self-evident.…”4 “That which is ‘natural’
    must assume the features of the extraordinary. Only in this manner can the laws
    of cause and effect reveal themselves.”5

    [...]

    The efforts to recapture the Great Refusal in the language of literature suffer
    the fate of being absorbed by what they refute. As modern classics, the
    avant-garde and the beatniks share in the function of entertaining without
    endangering the good conscience of the men of good will. This absorption is
    justified by technical progress; the refusal is refuted by the alleviation of
    misery in the advanced industrial society. The liquidation of high culture is a
    byproduct of the conquest of nature, and of the progressing conquest of
    scarcity.

    Invalidating the cherished images of transcendence by incorporating them into
    its omnipresent daily reality, this society testifies to the extent to which
    insoluble conflicts are becoming manageable—to which tragedy and romance,
    archetypal dreams and anxieties are being made susceptible to technical
    solution and dissolution. The psychiatrist takes care of the Don Juans, Romeos,
    Hamlets, Fausts, as he takes care of Oedipus—he cures them. The rulers of the
    world are losing their metaphysical features. Their appearance on television,
    at press conferences, in parliament, and at public hearings is hardly suitable
    for drama beyond that of the advertisement,14 while the consequences of their
    actions surpass the scope of the drama.

### Adjusted desublimation

    In contrast to the pleasures of adjusted desublimation, sublimation preserves
    the consciousness of the renunciations which the repressive society inflicts
    upon the individual, and thereby preserves the need for liberation. To be sure,
    all sublimation is enforced by the power of society, but the unhappy
    consciousness of this power already breaks through alienation. To be sure, all
    sublimation accepts the social barrier to instinctual gratification, but it
    also transgresses this barrier.

    The Superego, in censoring the unconscious and in implanting conscience, also
    censors the censor because the developed conscience registers the forbidden
    evil act not only in the individual but also in his society. Conversely, loss
    of conscience due to the satisfactory liberties granted by an unfree society
    makes for a happy consciousness which facilitates acceptance of the misdeeds of
    this society. It is the token of declining autonomy and comprehension.
    Sublimation demands a high degree of autonomy and comprehension; it is
    mediation between the conscious and the unconscious, between the primary and
    secondary processes, between the intellect and instinct, renunciation and
    rebellion. In its most accomplished modes, such as in the artistic oeuvre,
    sublimation becomes the cognitive power which defeats suppression while bowing
    to it.

    In the light of the cognitive function of this mode of sublimation, the
    desublimation rampant in advanced industrial society reveals its truly
    conformist function. This liberation of sexuality (and of aggressiveness) frees
    the instinctual drives from much of the unhappiness and discontent that
    elucidate the repressive power of the established universe of satisfaction. To
    be sure, there is pervasive unhappiness, and the happy consciousness is shaky
    enough—a thin surface over fear, frustration, and disgust. This unhappiness
    lends itself easily to political mobilization; without room for conscious
    development, it may become the instinctual reservoir for a new fascist way of
    life and death. But there are many ways in which the unhappiness beneath the
    happy consciousness may be turned into a source of strength and cohesion for
    the social order. The conflicts of the unhappy individual now seem far more
    amenable to cure than those which made for Freud’s “discontent in
    civilization,” and they seem more adequately defined in terms of the “neurotic
    personality of our time” than in terms of the eternal struggle between Eros and
    Thanatos.

    [...]

    In accordance with the terminology used in the later works of Freud: sexuality
    as “specialized” partial drive; Eros as that of the entire organism.

### Crust

    In this general necessity, guilt has no place. One man can give the signal that
    liquidates hundreds and thousands of people, then declare himself free from all
    pangs of conscience, and live happily ever after. The antifascist powers who
    beat fascism on the battlefields reap the benefits of the Nazi scientists,
    generals, and engineers; they have the historical advantage of the late-comer.
    What begins as the horror of the concentration camps turns into the practice of
    training people for abnormal conditions—a subterranean human existence and the
    daily intake of radioactive nourishment. A Christian minister declares that it
    does not contradict Christian principles to prevent with all available means
    your neighbor from entering your bomb shelter. Another Christian minister
    contradicts his colleague and says it does. Who is right? Again, the neutrality
    of technological rationality shows forth over and above politics, and again it
    shows forth as spurious, for in both cases, it serves the politics of
    domination.

    [...]

    It seems that even the most hideous transgressions can be repressed in such a
    manner that, for all practical purposes, they have ceased to be a danger for
    society. Or, if their eruption leads to functional disturbances in the
    individual (as in the case of one Hiroshima pilot), it does not disturb the
    functioning of society. A mental hospital manages the disturbance.

### Game

    The Happy Consciousness has no limits—it arranges games with death and
    disfiguration in which fun, team work, and strategic importance mix in
    rewarding social harmony. The Rand Corporation, which unites scholarship,
    research, the military, the climate, and the good life, reports such games in a
    style of absolving cuteness, in its “RANDom News,” volume 9, number 1, under
    the heading BETTER SAFE THAN SORRY. The rockets are rattling, the H-bomb is
    waiting, and the space-flights are flying, and the problem is “how to guard the
    nation and the free world.” In all this, the military planners are worried, for
    “the cost of taking chances, of experimenting and making a mistake, may be
    fearfully high.” But here RAND comes in; RAND relieves, and “devices like
    RAND’S SAFE come into the picture.” The picture into which they come is
    unclassified. It is a picture in which “the world becomes a map, missiles
    merely symbols [long live the soothing power of symbolism!], and wars just
    [just] plans and calculations written down on paper …” In this picture, RAND
    has transfigured the world into an interesting technological game, and one can
    relax—the “military planners can gain valuable ‘synthetic’ experience without
    risk.”

    PLAYING THE GAME

    To understand the game one should participate, for understanding is “in the
    experience.”

    Because SAFE players have come from almost every department at RAND as well as
    the Air Force, we might find a physicist, an engineer, and an economist on the
    Blue team. The Red team will represent a similar cross-section.

    The first day is taken up by a joint briefing on what the game is all about and
    a study of the rules. When the teams are finally seated around the maps in
    their respective rooms the game begins. Each team receives its policy statement
    from the Game Director. These statements, usually prepared by a member of the
    Control Group, give an estimate of the world situation at the time of playing,
    some information on the policy of the opposing team, the objectives to be met
    by the team, and the team’s budget. (The policies are changed for each game to
    explore a wide range of strategic possibilities.)

### Guilt

    Obviously, in the realm of the Happy Consciousness, guilt feeling has no place,
    and the calculus takes care of conscience. When the whole is at stake, there is
    no crime except that of rejecting the whole, or not defending it. Crime, guilt,
    and guilt feeling become a private affair. Freud revealed in the psyche of the
    individual the crimes of mankind, in the individual case history the history of
    the whole. This fatal link is successfully suppressed. Those who identify
    themselves with the whole, who are installed as the leaders and defenders of
    the whole can make mistakes, but they cannot do wrong—they are not guilty. They
    may become guilty again when this identification no longer holds, when they are
    gone.

### The Happy Conciousness

    The Happy Consciousness—the belief that the real is rational and that the
    system delivers the goods—reflects the new conformism which is a facet of
    technological rationality translated into social behavior.

### Language, memory and history

    The unified, functional language is an irreconcilably anti-critical and
    anti-dialectical language. In it, operational and behavioral rationality
    absorbs the transcendent, negative, oppositional elements of Reason.

    I shall discuss17 these elements in terms of the tension between the “is” and
    the “ought,” between essence and appearance, potentiality and
    actuality—ingression of the negative in the positive determinations of logic.
    This sustained tension permeates the two-dimensional universe of discourse
    which is the universe of critical, abstract thought. The two dimensions are
    antagonistic to each other; the reality partakes of both of them, and the
    dialectical concepts develop the real contradictions. In its own development,
    dialectical thought came to comprehend the historical character of the
    contradictions and the process of their mediation as historical process. Thus
    the “other” dimension of thought appeared to be historical dimension—the
    potentiality as historical possibility, its realization as historical event.

    The suppresssion of this dimension in the societal universe of operational
    rationality is a suppression of history, and this is not an academic but a
    political affair. It is suppression of the society’s own past—and of its
    future, inasmuch as this future invokes the qualitative change, the negation of
    the present. A universe of discourse in which the categories of freedom
    have become interchangeable and even identical with their opposites is not only
    practicing Orwellian or Aesopian language but is repulsing and forgetting the
    historical reality—the horror of fascism; the idea of socialism; the
    preconditions of democracy; the content of freedom. If a bureaucratic
    dictatorship rules and defines communist society, if fascist regimes are
    functioning as partners of the Free World, if the welfare program of
    enlightened capitalism is successfully defeated by labeling it “socialism,” if
    the foundations of democracy are harmoniously abrogated in democracy, then the
    old historical concepts are invalidated by up-to-date operational
    redefinitions. The redefinitions are falsifications which, imposed by the
    powers that be and the powers of fact, serve to transform falsehood into truth.

    The functional language is a radically anti-historical language: operational
    rationality has little room and little use for historical reason.18 Is this
    fight against history part of the fight against a dimension of the mind in
    which centrifugal faculties and forces might develop—faculties and forces that
    might hinder the total coordination of the individual with the society?
    Remembrance of the past may give rise to dangerous insights, and the
    established society seems to be apprehensive of the subversive contents of
    memory. Remembrance is a mode of dissociation from the given facts, a mode of
    “mediation” which breaks, for short moments, the omnipresent power of the given
    facts. Memory recalls the terror and the hope that passed. Both come to life
    again, but whereas in reality, the former recurs in ever new forms, the latter
    remains hope. And in the personal events which reappear in the individual
    memory, the fears and aspirations of mankind assert themselves—the universal in
    the particular. It is history which memory preserves. It succumbs to the
    totalitarian power of the behavioral universe

    [...]

    The closed language does not demonstrate and explain—it communicates decision,
    dictum, command. Where it defines, the definition becomes “separation of good
    from evil”; it establishes unquestionable rights and wrongs, and one value as
    justification of another value. It moves in tautologies, but the tautologies
    are terribly effective “sentences.” They pass judgment in a “prejudged form”;
    they pronounce condemnation. For example, the “objective content,” that is, the
    definition of such terms as “deviationist,” “revisionist,” is that of the penal
    code, and this sort of validation promotes a consciousness for which the
    language of the powers that be is the language of truth.24

    [...]

    As the substance of the various regimes no longer appears in alternative modes
    of life, it comes to rest in alternative techniques of manipulation and
    control. Language not only reflects these controls but becomes itself an
    instrument of control even where it does not transmit orders but information;
    where it demands, not obedience but choice, not submission but freedom.

    [...]

    What is taking place is a sweeping redefinition of thought itself, of its
    function and content. The coordination of the individual with his society
    reaches into those layers of the mind where the very concepts are elaborated
    which are designed to comprehend the established reality. These concepts are
    taken from the intellectual tradition and translated into operational terms—a
    translation which has the effect of reducing the tension between thought and
    reality by weakening the negative power of thought.

### Science and technology of domination

    The principles of modern science were a priori structured in such a way that
    they could serve as conceptual instruments for a universe of self-propelling,
    productive control; theoretical operationalism came to correspond to practical
    operationalism. The scientific method which led to the ever-more-effective
    domination of nature thus came to provide the pure concepts as well as the
    instrumentalities for the ever-more-effective domination of man by man through
    the domination of nature. Theoretical reason, remaining pure and neutral,
    entered into the service of practical reason. The merger proved beneficial to
    both. Today, domination perpetuates and extends itself not only through
    technology but as technology, and the latter provides the great legitimation of
    the expanding political power, which absorbs all spheres of culture.

    In this universe, technology also provides the great rationalization of the
    unfreedom of man and demonstrates the “technical” impossibility of being
    autonomous, of determining one’s own life. For this unfreedom appears neither
    as irrational nor as political, but rather as submission to the technical
    apparatus which enlarges the comforts of life and increases the productivity of
    labor. Technological rationality thus protects rather than cancels the
    legitimacy of domination, and the instrumentalist horizon of reason opens on a
    rationally totalitarian society:

        “One might call autocratic a philosophy of technics which takes the technical
        whole as a place where machines are used to obtain power. The machine is only a
        means; the end is the conquest of nature, the domestication of natural forces
        through a primary enslavement: The machine is a slave which serves to make
        other slaves. Such a domineering and enslaving drive may go together with the
        quest for human freedom. But it is difficult to liberate oneself by
        transferring slavery to other beings, men, animals, or machines; to rule over a
        population of machines subjecting the whole world means still to rule, and all
        rule implies acceptance of schemata of subjection.” Gilbert Simondon, Du Mode
        d’existence des objets techniques (Paris, Aubier, 1958), p. 127.

    [...]

    The incessant dynamic of technical progress has become permeated with political
    content, and the Logos of technics has been made into the Logos of continued
    servitude. The liberating force of technology—the instrumentalization of
    things—turns into a fetter of liberation; the instrumentalization of man.

    [...]

    No matter how one defines truth and objectivity, they remain related to the
    human agents of theory and practice, and to their ability to comprehend and
    change their world. This ability in turn depends on the extent to which matter
    (whatever it may be) is recognized and understood as that which it is itself in
    all particular forms. In these terms, contemporary science is of immensely
    greater objective validity than its predecessors. One might even add that, at
    present, the scientific method is the only method that can claim such validity;
    the interplay of hypotheses and observable facts validates the hypotheses and
    establishes the facts. The point which I am trying to make is that science, by
    virtue of its own method and concepts, has projected and promoted a universe in
    which the domination of nature has remained linked to the domination of man—a
    link which tends to be fatal to this universe as a whole. Nature,
    scientifically comprehended and mastered, reappears in the technical apparatus
    of production and destruction which sustains and improves the life of the
    individuals while subordinating them to the masters of the apparatus. Thus the
    rational hierarchy merges with the social one. If this is the case, then the
    change in the direction of progress, which might sever this fatal link, would
    also affect the very structure of science—the scientific project. Its
    hypotheses, without losing their rational character, would develop in an
    essentially different experimental context (that of a pacified world);
    consequently, science would arrive at essentially different concepts of nature
    and establish essentially different facts. The rational society subverts the
    idea of Reason.

    I have pointed out that the elements of this subversion, the notions of another
    rationality, were present in the history of thought from its beginning. The
    ancient idea of a state where Being attains fulfillment, where the tension
    between “is” and “ought” is resolved in the cycle of an eternal return,
    partakes of the metaphysics of domination. But it also pertains to the
    metaphysics of liberation—to the reconciliation of Logos and Eros. This idea
    envisages the coming-to-rest of the repressive productivity of Reason, the end
    of domination in gratification.

    [...]

    By way of summary, we may now try to identify more clearly the hidden subject
    of scientific rationality and the hidden ends in its pure form. The scientific
    concept of a universally controllable nature projected nature as endless
    matter-in-function, the mere stuff of theory and practice. In this form, the
    object-world entered the construction of a technological universe—a universe of
    mental and physical instrumentalities, means in themselves. Thus it is a truly
    “hypothetical” system, depending on a validating and verifying subject.

    The processes of validation and verification may be purely theoretical ones,
    but they never occur in a vacuum and they never terminate in a private,
    individual mind. The hypothetical system of forms and functions becomes
    dependent on another system—a pre-established universe of ends, in which and
    for which it develops. What appeared extraneous, foreign to the theoretical
    project, shows forth as part of its very structure (method and concepts); pure
    objectivity reveals itself as object for a subjectivity which provides the
    Telos, the ends. In the construction of the technological reality, there is no
    such thing as a purely rational scientific order; the process of technological
    rationality is a political process.

    Only in the medium of technology, man and nature become fungible objects of
    organization. The universal effectiveness and productivity of the apparatus
    under which they are subsumed veil the particular interests that organize the
    apparatus. In other words, technology has become the great vehicle of
    reification—reification in its most mature and effective form. The social
    position of the individual and his relation to others appear not only to be
    determined by objective qualities and laws, but these qualities and laws seem
    to lose their mysterious and uncontrollable character; they appear as
    calculable manifestations of (scientific) rationality. The world tends to
    become the stuff of total administration, which absorbs even the
    administrators. The web of domination has become the web of Reason itself, and
    this society is fatally entangled in it. And the transcending modes of thought
    seem to transcend Reason itself.

### Positive and Negative Thinking

    In terms of the established universe, such contradicting modes of thought are
    negative thinking. “The power of the negative” is the principle which governs
    the development of concepts, and contradiction becomes the distinguishing
    quality of Reason (Hegel). This quality of thought was not confined to a
    certain type of rationalism; it was also a decisive element in the empiricist
    tradition. Empiricism is not necessarily positive; its attitude to the
    established reality depends on the particular dimension of experience which
    functions as the source of knowledge and as the basic frame of reference. For
    example, it seems that sensualism and materialism are per se negative toward a
    society in which vital instinctual and material needs are unfulfilled. In
    contrast, the empiricism of linguistic analysis moves within a framework which
    does not allow such contradiction—the self-imposed restriction to the prevalent
    behavioral universe makes for an intrinsically positive attitude. In spite of
    the rigidly neutral approach of the philosopher, the pre-bound analysis
    succumbs to the power of positive thinking.

    Before trying to show this intrinsically ideological character of linguistic
    analysis, I must attempt to justify my apparently arbitrary and derogatory play
    with the terms “positive” and “positivism” by a brief comment on their origin.
    Since its first usage, probably in the school of Saint-Simon, the term
    “positivism” has encompassed (1) the validation of cognitive thought by
    experience of facts; (2) the orientation of cognitive thought to the physical
    sciences as a model of certainty and exactness; (3) the belief that progress in
    knowledge depends on this orientation. Consequently, positivism is a struggle
    against all metaphysics, transcendentalisms, and idealisms as obscurantist and
    regressive modes of thought. To the degree to which the given reality is
    scientifically comprehended and transformed, to the degree to which society
    becomes industrial and technological, positivism finds in the society the
    medium for the realization (and validation) of its concepts—harmony between
    theory and practice, truth and facts. Philosophic thought turns into
    affirmative thought; the philosophic critique criticizes within the societal
    framework and stigmatizes non-positive notions as mere speculation, dreams or
    fantasies.1

    [...]

    The contemporary effort to reduce the scope and the truth of philosophy is
    tremendous, and the philosophers themselves proclaim the modesty and inefficacy
    of philosophy. It leaves the established reality untouched; it abhors
    transgression.

    Austin’s contemptuous treatment of the alternatives to the common usage of
    words, and his defamation of what we “think up in our armchairs of an
    afternoon”; Wittgenstein’s assurance that philosophy “leaves everything as it
    is”—such statements2 exhibit, to my mind, academic sado-masochism,
    self-humiliation, and self-denunciation of the intellectual whose labor does
    not issue in scientific, technical or like achievements. These affirmations of
    modesty and dependence seem to recapture Hume’s mood of righteous contentment
    with the limitations of reason which, once recognized and accepted, protect man
    from useless mental adventures but leave him perfectly capable of orienting
    himself in the given environment. However, when Hume debunked substances, he
    fought a powerful ideology, while his successors today provide an intellectual
    justification for that which society has long since accomplished—namely, the
    defamation of alternative modes of thought which contradict the established
    universe of discourse.

### Language, philosophy and the restricted experience

    The almost masochistic reduction of speech to the humble and common is made
    into a program: “if the words ‘language,’ ‘experience,’ ‘world,’ have a use, it
    must be as humble a one as that of the words ‘table,’ ‘lamp,’ ‘door.’

    [...]

    The self-styled poverty of philosophy, committed with all its concepts to the
    given state of affairs, distrusts the possibilities of a new experience.
    Subjection to the rule of the established facts is total—only linguistic facts,
    to be sure, but the society speaks in its language, and we are told to obey.
    The prohibitions are severe and authoritarian: “Philosophy may in no way
    interfere with the actual use of language.”9 “And we may not advance any kind
    of theory. There must not be anything hypothetical in our considerations. We
    must do away with all explanation, and description alone must take its
    place.”10

    One might ask what remains of philosophy? What remains of thinking,
    intelligence, without anything hypothetical, without any explanation? However,
    what is at stake is not the definition or the dignity of philosophy. It is
    rather the chance of preserving and protecting the right, the need to think and
    speak in terms other than those of common usage—terms which are meaningful,
    rational, and valid precisely because they are other terms. What is involved is
    the spread of a new ideology which undertakes to describe what is happening
    (and meant) by eliminating the concepts capable of understanding what is
    happening (and meant).

    To begin with, an irreducible difference exists between the universe of
    everyday thinking and language on the one side, and that of philosophic
    thinking and language on the other. In normal circumstances, ordinary language
    is indeed behavioral—a practical instrument. When somebody actually says “My
    broom is in the corner,” he probably intends that somebody else who had
    actually asked about the broom is going to take it or leave it there, is going
    to be satisfied, or angry. In any case, the sentence has fulfilled its function
    by causing a behavioral reaction: “the effect devours the cause; the end
    absorbs the means.”11

    In contrast, if, in a philosophic text or discourse, the word “substance,”
    “idea,” “man,” “alienation” becomes the subject of a proposition, no such
    transformation of meaning into a behavioral reaction takes place or is intended
    to take place. The word remains, as it were, unfulfilled—except in thought,
    where it may give rise to other thoughts. And through a long series of
    mediations within a historical continuum, the proposition may help to form and
    guide a practice. But the proposition remains unfulfilled even then—only the
    hubris of absolute idealism asserts the thesis of a final identity between
    thought and its object. The words with which philosophy is concerned can
    therefore never have a use “as humble … as that of the words ‘table,’ ‘lamp,’
    ‘door.’ ”

    [...]

    Viewed from this position, the examples of linguistic analysis quoted above
    become questionable as valid objects of philosophic analysis. Can the most
    exact and clarifying description of tasting something that may or may not taste
    like pineapple ever contribute to philosophic cognition? [...] The object of
    analysis, withdrawn from the larger and denser context in which the speaker
    speaks and lives, is removed from the universal medium in which concepts are
    formed and become words. What is this universal, larger context in which people
    speak and act and which gives their speech its meaning—this context which does
    not appear in the positivist analysis, which is a priori shut off by the
    examples as well as by the analysis itself?

    This larger context of experience, this real empirical world, today is still
    that of the gas chambers and concentration camps, of Hiroshima and Nagasaki, of
    American Cadillacs and German Mercedes, of the Pentagon and the Kremlin, of the
    nuclear cities and the Chinese communes, of Cuba, of brainwashing and
    massacres. But the real empirical world is also that in which all these things
    are taken for granted or forgotten or repressed or unknown, in which people are
    free. It is a world in which the broom in the corner or the taste of something
    like pineapple are quite important, in which the daily toil and the daily
    comforts are perhaps the only items that make up all experience. And this
    second, restricted empirical universe is part of the first; the powers that
    rule the first also shape the restricted experience.

    [...]

    Ordinary language in its “humble use” may indeed be of vital concern to
    critical philosophic thought, but in the medium of this thought words lose
    their plain humility and reveal that “hidden” something which is of no interest
    to Wittgenstein. [...] Such an analysis uncovers the history13 in everyday
    speech as a hidden dimension of meaning—the rule of society over its language.

    [...]

    Orienting itself on the reified universe of everyday discourse, and exposing
    and clarifying this discourse in terms of this reified universe, the analysis
    abstracts from the negative, from that which is alien and antagonistic and
    cannot be understood in terms of the established usage. By classifying and
    distinguishing meanings, and keeping them apart, it purges thought and speech
    of contradictions, illusions, and transgressions. But the transgressions are
    not those of “pure reason.” They are not metaphysical transgressions beyond the
    limits of possible knowledge, they rather open a realm of knowledge beyond
    common sense and formal logic.

    In barring access to this realm, positivist philosophy sets up a
    self-sufficient world of its own, closed and well protected against the
    ingression of disturbing external factors. In this respect, it makes little
    difference whether the validating context is that of mathematics, of logical
    propositions, or of custom and usage. In one way or another, all possibly
    meaningful predicates are prejudged. The prejudging judgment might be as broad
    as the spoken English language, or the dictionary, or some other code or
    convention. Once accepted, it constitutes an empirical a priori which cannot be
    transcended.

    [...]

    The therapeutic character of the philosophic analysis is strongly emphasized—to
    cure from illusions, deceptions, obscurities, unsolvable riddles, unanswerable
    questions, from ghosts and spectres. Who is the patient? Apparently a certain
    sort of intellectual, whose mind and language do not conform to the terms of
    ordinary discourse. There is indeed a goodly portion of psychoanalysis in this
    philosophy—analysis without Freud’s fundamental insight that the patient’s
    trouble is rooted in a general sickness which cannot be cured by analytic
    therapy. Or, in a sense, according to Freud, the patient’s disease is a protest
    reaction against the sick world in which he lives. But the physician must
    disregard the “moral” problem. He has to restore the patient’s health, to make
    him capable of functioning normally in his world.

    The philosopher is not a physician; his job is not to cure individuals but to
    comprehend the world in which they live—to understand it in terms of what it
    has done to man, and what it can do to man. For philosophy is (historically,
    and its history is still valid) the contrary of what Wittgenstein made it out
    to be when he proclaimed it as the renunciation of all theory, as the
    undertaking that “leaves everything as it is.”

    [...]

    The neo-positivist critique still directs its main effort against metaphysical
    notions, and it is motivated by a notion of exactness which is either that of
    formal logic or empirical description. Whether exactness is sought in the
    analytic purity of logic and mathematics, or in conformity with ordinary
    language—on both poles of contemporary philosophy is the same rejection or
    devaluation of those elements of thought and speech which transcend the
    accepted system of validation. This hostility is most sweeping where it takes
    the form of toleration—that is, where a certain truth value is granted to the
    transcendent concepts in a separate dimension of meaning and significance
    (poetic truth, metaphysical truth). For precisely the setting aside of a
    special reservation in which thought and language are permitted to be
    legitimately inexact, vague, and even contradictory is the most effective way
    of protecting the normal universe of discourse from being seriously disturbed
    by unfitting ideas. Whatever truth may be contained in literature is a “poetic”
    truth, whatever truth may be contained in critical idealism is a “metaphysical”
    truth—its validity, if any, commits neither ordinary discourse and behavior,
    nor the philosophy adjusted to them.

    This new form of the doctrine of the “double truth” sanctions a false
    consciousness by denying the relevance of the transcendent language to the
    universe of ordinary language, by proclaiming total non-interference. Whereas
    the truth value of the former consists precisely in its relevance to and
    interference with the latter.

### Philosophy and science

    This intellectual dissolution and even subversion of the given facts is the
    historical task of philosophy and the philosophic dimension. Scientific method,
    too, goes beyond the facts and even against the facts of immediate experience.
    Scientific method develops in the tension between appearance and reality. The
    mediation between the subject and object of thought, however, is essentially
    different. In science, the medium is the observing, measuring, calculating,
    experimenting subject divested of all other qualities; the abstract subject
    projects and defines the abstract object.

    In contrast, the objects of philosophic thought are related to a consciousness
    for which the concrete qualities enter into the concepts and into their
    interrelation. The philosophic concepts retain and explicate the pre-scientific
    mediations (the work of everyday practice, of economic organization, of
    political action) which have made the object-world that which it actually is—a
    world in which all facts are events, occurrences in a historical continuum.

    The separation of science from philosophy is itself a historical event.
    Aristotelian physics was a part of philosophy and, as such, preparatory to the
    “first science”—ontology. The Aristotelian concept of matter is distinguished
    from the Galilean and post-Galilean not only in terms of different stages in
    the development of scientific method (and in the discovery of different
    ‘layers” of reality), but also, and perhaps primarily, in terms of different
    historical projects, of a different historical enterprise which established a
    different nature as well as society. Aristotelian physics becomes objectively
    wrong with the new experience and apprehension of nature, with the historical
    establishment of a new subject and object-world, and the falsification of
    Aristotelian physics then extends backward into the past and surpassed
    experience and apprehension.15

### A funny paragraph

    The neglect or the clearing up of this specific philosophic dimension has led
    contemporary positivism to move in a synthetically impoverished world of
    academic concreteness, and to create more illusory problems than it has
    destroyed. Rarely has a philosophy exhibited a more tortuous esprit de sérieux
    than that displayed in such analyses as the interpretation of Three Blind Mice
    in a study of “Metaphysical and Ideographic Language,” with its discussion of
    an “artificially constructed Triple principle-Blindness-Mousery asymmetric
    sequence constructed according to the pure principles of ideography.”17

    Perhaps this example is unfair. [...] Examples are skillfully held in balance
    between seriousness and the joke

[Three Blind Mice](https://en.wikipedia.org/wiki/Three_Blind_Mice) is a crusty rhyme.

### A suspect language

    Analytic philosophy often spreads the atmosphere of denunciation and
    investigation by committee. The intellectual is called on the carpet. What do
    you mean when you say …? Don’t you conceal something? You talk a language which
    is suspect. You don’t talk like the rest of us, like the man in the street, but
    rather like a foreigner who does not belong here. We have to cut you down to
    size, expose your tricks, purge you. We shall teach you to say what you have in
    mind, to “come clear,” to “put your cards on the table.” Of course, we do not
    impose on you and your freedom of thought and speech; you may think as you
    like. But once you speak, you have to communicate your thoughts to us—in our
    language or in yours. Certainly, you may speak your own language, but it must
    be translatable, and it will be translated. You may speak poetry—that is all
    right. We love poetry. But we want to understand your poetry, and we can do so
    only if we can interpret your symbols, metaphors, and images in terms of
    ordinary language.

    The poet might answer that indeed he wants his poetry to be understandable and
    understood (that is why he writes it), but if what he says could be said in
    terms of ordinary language he would probably have done so in the first place.
    He might say: Understanding of my poetry presupposes the collapse and
    invalidation of precisely that universe of discourse and behavior into which
    you want to translate it. My language can be learned like any other language
    (in point of fact, it is also your own language), then it will appear that my
    symbols, metaphors, etc. are not symbols, metaphors, etc. but mean exactly what
    they say. Your tolerance is deceptive. In reserving for me a special niche of
    meaning and significance, you grant me exemption from sanity and reason, but in
    my view, the madhouse is somewhere else.

    [...]

    Under these circumstances, the spoken phrase is an expression of the individual
    who speaks it, and of those who make him speak as he does, and of whatever
    tension or contradiction may interrelate them. In speaking their own language,
    people also speak the language of their masters, benefactors, advertisers. Thus
    they do not only express themselves, their own knowledge, feelings, and
    aspirations, but also something other than themselves. Describing “by
    themselves” the political situation, either in their home town or in the
    international scene, they (and “they” includes us, the intellectuals who know
    it and criticize it) describe what “their” media of mass communication tell
    them—and this merges with what they really think and see and feel.

    [...]

    But this situation disqualifies ordinary language from fulfilling the
    validating function which it performs in analytic philosophy. “What people mean
    when they say …” is related to what they don’t say. Or, what they mean cannot
    be taken at face value—not because they lie, but because the universe of
    thought and practice in which they live is a universe of manipulated
    contradictions.

### Metalanguage

    Here the problem of “metalanguage” arises; the terms which analyze the meaning
    of certain terms must be other than, or distinguishable from the latter. They
    must be more and other than mere synonyms which still belong to the same
    (immediate) universe of discourse. But if this metalanguage is really to break
    through the totalitarian scope of the established universe of discourse, in
    which the different dimensions of language are integrated and assimilated, it
    must be capable of denoting the societal processes which have determined and
    “closed” the established universe of discourse. Consequently, it cannot be a
    technical metalanguage, constructed mainly with a view of semantic or logical
    clarity. The desideratum is rather to make the established language itself
    speak what it conceals or excludes, for what is to be revealed and denounced is
    operative within the universe of ordinary discourse and action, and the
    prevailing language contains the metalanguage.

### Ordinary universe of discourse

    The crimes against language, which appear in the style of the newspaper,
    pertain to its political style. Syntax, grammar, and vocabulary become moral
    and political acts. Or, the context may be an aesthetic and philosophic one:
    literary criticism, an address before a learned society, or the like.

    [...]

    For such an analysis, the meaning of a term or form demands its development in
    a multi-dimensional universe, where any expressed meaning partakes of several
    interrelated, overlapping, and antagonistic “systems.”

    [...]

    in reality, we understand each other only through whole areas of
    misunderstanding and contradiction. The real universe of ordinary language is
    that of the struggle for existence. It is indeed an ambiguous, vague, obscure
    universe, and is certainly in need of clarification. Moreover, such
    clarification may well fulfill a therapeutic function, and if philosophy would
    become therapeutic, it would really come into its own.

    Philosophy approaches this goal to the degree to which it frees thought from
    its enslavement by the established universe of discourse and behavior,
    elucidates the negativity of the Establishment (its positive aspects are
    abundantly publicized anyway) and projects its alternatives. To be sure,
    philosophy contradicts and projects in thought only. It is ideology, and this
    ideological character is the very fate of philosophy which no scientism and
    positivism can overcome. Still, its ideological effort may be truly
    therapeutic—to show reality as that which it really is, and to show that which
    this reality prevents from being.

    In the totalitarian era, the therapeutic task of philosophy would be a
    political task, since the established universe of ordinary language tends to
    coagulate into a totally manipulated and indoctrinated universe. Then politics
    would appear in philosophy, not as a special discipline or object of analysis,
    nor as a special political philosophy, but as the intent of its concepts to
    comprehend the unmutilated reality. If linguistic analysis does not contribute
    to such understanding; if, instead, it contributes to enclosing thought in the
    circle of the mutilated universe of ordinary discourse, it is at best entirely
    inconsequential. And, at worst, it is an escape into the non-controversial, the
    unreal, into that which is only academically controversial.

### Universal Ghosts

    Contemporary analytic philosophy is out to exorcize such “myths” or
    metaphysical “ghosts” as Mind, Consciousness, Will, Soul, Self, by dissolving
    the intent of these concepts into statements on particular identifiable
    operations, performances, powers, dispositions, propensities, skills, etc. The
    result shows, in a strange way, the impotence of the destruction—the ghost
    continues to haunt. While every interpretation or translation may describe
    adequately a particular mental process, an act of imagining what I mean when I
    say “I,” or what the priest means when he says that Mary is a “good girl,” not
    a single one of these reformulations, nor their sum-total, seems to capture or
    even circumscribe the full meaning of such terms as Mind, Will, Self, Good.
    These universals continue to persist in common as well as “poetic” usage, and
    either usage distinguishes them from the various modes of behavior or
    disposition that, according to the analytic philosopher, fulfill their meaning.

    [...]

    However, this dissolution itself must be questioned—not only on behalf of the
    philosopher, but on behalf of the ordinary people in whose life and discourse
    such dissolution takes place. It is not their own doing and their own saying;
    it happens to them and it violates them as they are compelled, by the
    “circumstances,” to identify their mind with the mental processes, their self
    with the roles and functions which they have to perform in their society.
    If philosophy does not comprehend these processes of translation and
    identification as societal processes—i.e., as a mutilation of the mind (and the
    body) inflicted upon the individuals by their society—philosophy struggles only
    with the ghost of the substance which it wishes to de-mystify. The mystifying
    character adheres, not to the concepts of “mind,” “self,” “consciousness,” etc.
    but rather to their behavioral translation. The translation is deceptive
    precisely because it translates the concept faithfully into modes of actual
    behavior, propensities, and dispositions and, in so doing, it takes the
    mutilated and organized appearances (themselves real enough!) for the reality.

    [...]

    Moreover, the normal restriction of experience produces a pervasive tension,
    even conflict, between “the mind” and the mental processes, between
    “consciousness” and conscious acts. If I speak of the mind of a person, I do
    not merely refer to his mental processes as they are revealed in his
    expression, speech, behavior, etc., nor merely of his dispositions or faculties
    as experienced or inferred from experience. I also mean that which he does not
    express, for which he shows no disposition, but which is present nevertheless,
    and which determines, to a considerable extent, his behavior, his
    understanding, the formation and range of his concepts.

    Thus “negatively present” are the specific “environmental” forces which
    precondition his mind for the spontaneous repulsion of certain data,
    conditions, relations. They are present as repelled material. Their absence is
    a reality—a positive factor that explains his actual mental processes, the
    meaning of his words and behavior. Meaning for whom? Not only for the
    professional philosopher, whose task it is to rectify the wrong that pervades
    the universe of ordinary discourse, but also for those who suffer this wrong
    although they may not be aware of it—for Joe Doe and Richard Roe. Contemporary
    linguistic analysis shirks this task by interpreting concepts in terms of an
    impoverished and preconditioned mind. What is at stake is the unabridged and
    unexpurgated intent of certain key concepts, their function in the unrepressed
    understanding of reality—in non-conformist, critical thought.

    Are the remarks just submitted on the reality content of such universals as
    “mind” and “consciousness” applicable to other concepts, such as the abstract
    yet substantive universals, Beauty, Justice, Happiness, with their contraries?
    It seems that the persistence of these untranslatable universals as nodal
    points of thought reflects the unhappy consciousness of a divided world in
    which “that which is” falls short of, and even denies, “that which can be.” The
    irreducible difference between the universal and its particulars seems to be
    rooted in the primary experience of the inconquerable difference between
    potentiality and actuality—between two dimensions of the one experienced world.
    The universal comprehends in one idea the possibilities which are realized, and
    at the same time arrested, in reality.

    [...]

    This description is of precisely that metaphysical character which positivistic
    analysis wishes to eliminate by translation, but the translation eliminates
    that which was to be defined.

    [...]

    The protest against the vague, obscure, metaphysical character of such
    universals, the insistence on familiar concreteness and protective security of
    common and scientific sense still reveal something of that primordial anxiety
    which guided the recorded origins of philosophic thought in its evolution from
    religion to mythology, and from mythology to logic; defense and security still
    are large items in the intellectual as well as national budget. The unpurged
    experience seems to be more familiar with the abstract and universal than is
    the analytic philosophy; it seems to be embedded in a metaphysical world.

    Universals are primary elements of experience—universals not as philosophic
    concepts but as the very qualities of the world with which one is daily
    confronted.

    [...]

    The substantive character of “qualities” points to the experiential origin of
    substantive universals, to the manner in which concepts originate in immediate
    experience.

    [...]

    But precisely the relation of the word to a substantive universal (concept)
    makes it impossible, according to Humboldt, to imagine the origin of language
    as starting from the signification of objects by words and then proceeding to
    their combination (Zusammenfügung): In reality, speech is not put together from
    preceding words, but quite the reverse: words emerge from the whole of speech
    (aus dem Ganzen der Rede).7

    The “whole” that here comes to view must be cleared from all misunderstanding
    in terms of an independent entity, of a “Gestalt,” and the like. The concept
    somehow expresses the difference and tension between potentiality and
    actuality—identity in this difference. It appears in the relation between the
    qualities (white, hard; but also beautiful, free, just) and the corresponding
    concepts (whiteness, hardness, beauty, freedom, justice). The abstract
    character of the latter seems to designate the more concrete qualities as
    part-realizations, aspects, manifestations of a more universal and more
    “excellent” quality, which is experienced in the concrete.8 And by virtue of
    this relation, the concrete quality seems to represent a negation as well as
    realization of the universal.

    [...]

    These formulations do not alter the relation between the abstract concept and
    its concrete realizations: the universal concept denotes that which the
    particular entity is, and is not. The translation can eliminate the hidden
    negation by reformulating the meaning in a non-contradictory proposition, but
    the untranslated statement suggests a real want. There is more in the abstract
    noun (beauty, freedom) than in the qualities (“beautiful,” “free”) attributed
    to the particular person, thing or condition. The substantive universal intends
    qualities which surpass all particular experience, but persist in the mind, not
    as a figment of imagination nor as more logical possibilities but as the
    “stuff” of which our world consists.

    [...]

    Now there is a large class of concepts—we dare say, the philosophically
    relevant concepts—where the quantitative relation between the universal and the
    particular assumes a qualitative aspect, where the abstract universal seems to
    designate potentialities in a concrete, historical sense. However “man,”
    “nature,” “justice,” “beauty” or “freedom” may be defined, they synthetize
    experiential contents into ideas which transcend their particular realizations
    as something that is to be surpassed, overcome. Thus the concept of beauty
    comprehends all the beauty not yet realized; the concept of freedom all the
    liberty not yet attained.

    Or, to take another example, the philosophic concept “man” aims at the fully
    developed human faculties which are his distinguishing faculties, and which
    appear as possibilities of the conditions in which men actually live.

    [...]

    Such universals thus appear as conceptual instruments for understanding the
    particular conditions of things in the light of their potentialities. They are
    historical and supra-historical; they conceptualize the stuff of which the
    experienced world consists, and they conceptualize it with a view of its
    possibilities, in the light of their actual limitation, suppression, and
    denial. Neither the experience nor the judgment is private. The philosophic
    concepts are formed and developed in the consciousness of a general condition
    in a historical continuum; they are elaborated from an individual position
    within a specific society. The stuff of thought is historical stuff—no matter
    how abstract, general, or pure it may become in philosophic or scientific
    theory. The abstract-universal and at the same time historical character of
    these “eternal objects” of thought is recognized and clearly stated in
    Whitehead’s Science and the Modern World:10

        “Eternal objects are … in their nature, abstract. By ‘abstract’ I mean that
        what an eternal object is in itself—that is to say, its essence—is
        comprehensible without reference to some one particular experience. To be
        abstract is to transcend the particular occasion of actual happening. But to
        transcend an actual occasion does not mean being disconnected from it. On the
        contrary, I hold that each eternal object has its own proper connection with
        each such occasion, which I term its mode of ingression into that occasion.”
        “Thus the metaphysical status of an eternal object is that of a possibility for
        an actuality. Every actual occasion is defined as to its character by how these
        possibilities are actualized for that occasion.”

    Elements of experience, projection and anticipation of real possibilities
    enter into the conceptual syntheses—in respectable form as hypotheses, in
    disreputable form as “metaphysics.” In various degrees, they are unrealistic
    because they transgress beyond the established universe of behavior, and they
    may even be undesirable in the interest of neatness and exactness. Certainly,
    in philosophic analysis,

        “Little real advance … is to be hoped for in expanding our universe to
        include so-called possible entities,”11

    but it all depends on how Ockham’s Razor is applied, that is to say, which
    possibilities are to be cut off. The possibility of an entirely different
    societal organization of life has nothing in common with the “possibility” of a
    man with a green hat appearing in all doorways tomorrow, but treating them with
    the same logic may serve the defamation of undesirable possibilities.
    Criticizing the introduction of possible entities, Quine writes that such an
    “overpopulated universe is in many ways unlovely. It offends the aesthetic
    sense of us who have a taste for desert landscapes, but this is not the worst
    of it. [Such a] slum of possibles is a breeding ground for disorderly
    elements.”12

    Contemporary philosophy has rarely attained a more authentic formulation of the
    conflict between its intent and its function. The linguistic syndrome of
    “loveliness,” “aesthetic sense,” and “desert landscape” evokes the liberating
    air of Nietzsche’s thought, cutting into Law and Order, while the “breeding
    ground for disorderly elements” belongs to the language spoken by the
    authorities of Investigation and Information. What appears unlovely and
    disorderly from the logical point of view, may well comprise the lovely
    elements of a different order, and may thus be an essential part of the
    material from which philosophic concepts are built. Neither the most refined
    aesthetic sense nor the most exact philosophic concept is immune against
    history. Disorderly elements enter into the purest objects of thought. They too
    are detached from a societal ground, and the contents from which they abstract
    guide the abstraction.

### Historicism

    Thus the spectre of “historicism” is raised. If thought proceeds from
    historical conditions which continue to operate in the abstraction, is there
    any objective basis on which distinction can be made between the various
    possibilities projected by thought—distinction between different and
    conflicting ways of conceptual transcendence? Moreover, the question cannot be
    discussed with reference to different philosophic projects only.13 To the
    degree to which the philosophical project is ideological, it is part of a
    historical project—that is, it pertains to a specific stage and level of the
    societal development, and the critical philosophic concepts refer (no matter
    how indirectly!) to alternative possibilities of this development.

    The quest for criteria for judging between different philosophic projects thus
    leads to the quest for criteria for judging between different historical
    projects and alternatives, between different actual and possible ways of
    understanding and changing man and nature. I shall submit only a few
    propositions which suggest that the internal historical character of the
    philosophic concepts, far from precluding objective validity, defines the
    ground for their objective validity.

    [...]

    The objects of thought and perception as they appear to the individuals prior
    to all “subjective” interpretation have in common certain primary qualities,
    pertaining to these two layers of reality: (1) to the physical (natural)
    structure of matter, and (2) to the form which matter has acquired in the
    collective historical practice that has made it (matter) into objects for a
    subject. The two layers or aspects of objectivity (physical and historical) are
    interrelated in such a way that they cannot be insulated from each other; the
    historical aspect can never be eliminated so radically that only the “absolute”
    physical layer remains.

    [...]

    I shall now propose some criteria for the truth value of different historical
    projects.

    [...]

    (1) The transcendent project must be in accordance with the real possibilities
    open at the attained level of the material and intellectual culture.

    (2) The transcendent project, in order to falsify the established totality,
    must demonstrate its own higher rationality in the threefold sense that

    (a) it offers the prospect of preserving and improving the productive
    achievements of civilization;

    (b) it defines the established totality in its very structure, basic
    tendencies, and relations;

    (c) its realization offers a greater chance for the pacification of existence,
    within the framework of institutions which offer a greater chance for the free
    development of human needs and faculties.

### Determinate choice

    If the historical continuum itself provides the objective ground for
    determining the truth of different historical projects, does it also determine
    their sequence and their limits? Historical truth is comparative; the
    rationality of the possible depends on that of the actual, the truth of the
    transcending project on that of the project in realization. Aristotelian
    science was falsified on the basis of its achievements; if capitalism were
    falsified by communism, it would be by virtue of its own achievements.
    Continuity is preserved through rupture: quantitative development becomes
    qualitative change if it attains the very structure of an established system;
    the established rationality becomes irrational when, in the course of its
    internal development, the potentialities of the system have outgrown its
    institutions. Such internal refutation pertains to the historical character of
    reality, and the same character confers upon the concepts which comprehend this
    reality their critical intent. They recognize and anticipate the irrational in
    the established reality—they project the historical negation.

    Is this negation a “determinate” one—that is, is the internal succession of a
    historical project, once it has become a totality, necessarily pre-determined
    by the structure of this totality? If so, then the term “project” would be
    deceptive. That which is historical possibility would sooner or later be real;
    and the definition of liberty as comprehended necessity would have a repressive
    connotation which it does not have. All this may not matter much. What does
    matter is that such historical determination would (in spite of all subtle
    ethics and psychology) absolve the crimes against humanity which civilization
    continues to commit and thus facilitate this continuation.

    I suggest the phrase “determinate choice” in order to emphasize the ingression
    of liberty into historical necessity; the phrase does no more than condense the
    proposition that men make their own history but make it under given conditions.
    Determined are (1) the specific contradictions which develop within a
    historical system as manifestations of the conflict between the potential and
    the actual; (2) the material and intellectual resources available to the
    respective system; (3) the extent of theoretical and practical freedom
    compatible with the system. These conditions leave open alternative
    possibilities of developing and utilizing the available resources, alternative
    possibilities of “making a living,” of organizing man’s struggle with nature.

    [...]

    the truth of a historical project is not validated ex post through success,
    that is to say, by the fact that it is accepted and realized by the society.
    Galilean science was true while it was still condemned; Marxian theory was
    already true at the time of the Communist Manifesto; fascism remains false even
    if it is in ascent on an international scale (“true” and “false” always in the
    sense of historical rationality as defined above). In the contemporary period,
    all historical projects tend to be polarized on the two conflicting
    totalities—capitalism and communism, and the outcome seems to depend on two
    antagonistic series of factors: (1) the greater force of destruction; (2) the
    greater productivity without destruction. In other words, the higher historical
    truth would pertain to the system which offers the greater chance of
    pacification.

### Negative Thinking

    To the degree to which the established society is irrational, the analysis in
    terms of historical rationality introduces into the concept the negative
    element—critique, contradiction, and transcendence.

    This element cannot be assimilated with the positive. It changes the concept in
    its entirety, in its intent and validity. Thus, in the analysis of an economy,
    capitalist or not, which operates as an “independent” power over and above the
    individuals, the negative features (overproduction, unemployment, insecurity,
    waste, repression) are not comprehended as long as they appear merely as more
    or less inevitable by-products, as “the other side” of the story of growth and
    progress.

    True, a totalitarian administration may promote the efficient exploitation of
    resources; the nuclear-military establishment may provide millions of jobs
    through enormous purchasing power; toil and ulcers may be the by-product of the
    acquisition of wealth and responsibility; deadly blunders and crimes on the
    part of the leaders may be merely the way of life. One is willing to admit
    economic and political madness—and one buys it. But this sort of knowledge of
    “the other side” is part and parcel of the solidification of the state of
    affairs, of the grand unification of opposites which counteracts qualitative
    change, because it pertains to a thoroughly hopeless or thoroughly
    preconditioned existence that has made its home in a world where even the
    irrational is Reason.

    The tolerance of positive thinking is enforced tolerance—enforced not by any
    terroristic agency but by the overwhelming, anonymous power and efficiency of
    the technological society. As such it permeates the general consciousness—and
    the consciousness of the critic. The absorption of the negative by the positive
    is validated in the daily experience, which obfuscates the distinction between
    rational appearance and irrational reality.

    [examples follow]

    These examples may illustrate the happy marriage of the positive and the
    negative—the objective ambiguity which adheres to the data of experience. It is
    objective ambiguity because the shift in my sensations and reflections responds
    to the manner in which the experienced facts are actually interrelated. But
    this interrelation, if comprehended, shatters the harmonizing consciousness and
    its false realism. Critical thought strives to define the irrational character
    of the established rationality (which becomes increasingly obvious) and to
    define the tendencies which cause this rationality to generate its own
    transformation. “Its own” because, as historical totality, it has developed
    forces and capabilities which themselves become projects beyond the established
    totality. They are possibilities of the advancing technological rationality
    and, as such, they involve the whole of society. The technological
    transformation is at the same time political transformation, but the political
    change would turn into qualitative social change only to the degree to which it
    would alter the direction of technical progress—that is, develop a new
    technology. For the established technology has become an instrument of
    destructive politics.

    Such qualitative change would be transition to a higher stage of civilization
    if technics were designed and utilized for the pacification of the struggle for
    existence. In order to indicate the disturbing implications of this statement,
    I submit that such a new direction of technical progress would be the
    catastrophe of the established direction, not merely the quantitative evolution
    of the prevailing (scientific and technological) rationality but rather its
    catastrophic transformation, the emergence of a new idea of Reason, theoretical
    and practical.

    The new idea of Reason is expressed in Whitehead’s proposition: “The function
    of Reason is to promote the art of life.”1 In view of this end, Reason is the
    “direction of the attack on the environment” which derives from the “threefold
    urge: (1) to live, (2) to live well, (3) to live better.”2

Then read the rest of the whole chapter 9. It's interesting enough that deserves
to be quoted on its entirety. It talks about the completion of the
Technological Project. Like this:

    Civilization produces the means for freeing Nature from its own brutality, its
    own insufficiency, its own blindness, by virtue of the cognitive and
    transforming power of Reason. And Reason can fulfill this function only as
    post-technological rationality, in which technics is itself the instrumentality
    of pacification, organon of the “art of life.” The function of Reason then
    converges with the function of Art.

    The Greek notion of the affinity between art and technics may serve as a
    preliminary illustration. The artist possesses the ideas which, as final
    causes, guide the construction of certain things—just as the engineer possesses
    the ideas which guide, as final causes, the construction of a machine. For
    example, the idea of an abode for human beings determines the architect’s
    construction of a house; the idea of wholesale nuclear explosion determines the
    construction of the apparatus which is to serve this purpose. Emphasis on the
    essential relation between art and technics points up the specific rationality
    of art.

    [...]

    In the contemporary era, the conquest of scarcity is still confined to small
    areas of advanced industrial society. Their prosperity covers up the Inferno
    inside and outside their borders; it also spreads a repressive productivity and
    “false needs.” It is repressive precisely to the degree to which it promotes
    the satisfaction of needs which require continuing the rat race of catching up
    with one’s peers and with planned obsolescence, enjoying freedom from using the
    brain, working with and for the means of destruction. The obvious comforts
    generated by this sort of productivity, and even more, the support which it
    gives to a system of profitable domination, facilitate its importation in less
    advanced areas of the world where the introduction of such a system still means
    tremendous progress in technical and human terms.

    However, the close interrelation between technical and political-manipulative
    know-how, between profitable productivity and domination, lends to the conquest
    of scarcity the weapons for containing liberation. To a great extent, it is the
    sheer quantity of goods, services, work, and recreation in the overdeveloped
    countries which effectuates this containment. Consequently, qualitative change
    seems to presuppose a quantitative change in the advanced standard of living,
    namely, reduction of overdevelopment.

    The standard of living attained in the most advanced industrial areas is not a
    suitable model of development if the aim is pacification. In view of what this
    standard has made of Man and Nature, the question must again be asked whether
    it is worth the sacrifices and the victims made in its defense. The question
    has ceased to be irresponsible since the “affluent society” has become a
    society of permanent mobilization against the risk of annihilation, and since
    the sale of its goods has been accompanied by moronization, the perpetuation of
    toil, and the promotion of frustration.

    Under these circumstances, liberation from the affluent society does not mean
    return to healthy and robust poverty, moral cleanliness, and simplicity. On the
    contrary, the elimination of profitable waste would increase the social wealth
    available for distribution, and the end of permanent mobilization would reduce
    the social need for the denial of satisfactions that are the individual’s
    own—denials which now find their compensation in the cult of fitness, strength,
    and regularity.

    [...]

    The crime is that of a society in which the growing population aggravates the
    struggle for existence in the face of its possible alleviation. The drive for
    more “living space” operates not only in international aggressiveness but also
    within the nation. Here, expansion has, in all forms of teamwork, community
    life, and fun, invaded the inner space of privacy and practically eliminated
    the possibility of that isolation in which the individual, thrown back on
    himself alone, can think and question and find. This sort of privacy—the sole
    condition that, on the basis of satisfied vital needs, can give meaning to
    freedom and independence of thought—has long since become the most expensive
    commodity, available only to the very rich (who don’t use it). In this respect,
    too, “culture” reveals its feudal origins and limitations. It can become
    democratic only through the abolition of mass democracy, i.e., if society has
    succeeded in restoring the prerogatives of privacy by granting them to all and
    protecting them for each.

    [...]

    To take an (unfortunately fantastic) example: the mere absence of all
    advertising and of all indoctrinating media of information and entertainment
    would plunge the individual into a traumatic void where he would have the
    chance to wonder and to think, to know himself (or rather the negative of
    himself) and his society. Deprived of his false fathers, leaders, friends, and
    representatives, he would have to learn his ABC’s again. But the words and
    sentences which he would form might come out very differently, and so might his
    aspirations and fears.

    To be sure, such a situation would be an unbearable nightmare. While the people
    can support the continuous creation of nuclear weapons, radioactive fallout,
    and questionable foodstuffs, they cannot (for this very reason!) tolerate being
    deprived of the entertainment and education which make them capable of
    reproducing the arrangements for their defense and/or destruction. The
    non-functioning of television and the allied media might thus begin to achieve
    what the inherent contradictions of capitalism did not achieve—the
    disintegration of the system. The creation of repressive needs has long since
    become part of socially necessary labor—necessary in the sense that without it,
    the established mode of production could not be sustained. Neither problems of
    psychology nor of aesthetics are at stake, but the material base of domination.

### Imagination

    In reducing and even canceling the romantic space of imagination, society has
    forced the imagination to prove itself on new grounds, on which the images are
    translated into historical capabilities and projects. The translation will be
    as bad and distorted as the society which undertakes it. Separated from the
    realm of material production and material needs, imagination was mere play,
    invalid in the realm of necessity, and committed only to a fantastic logic and
    a fantastic truth. When technical progress cancels this separation, it invests
    the images with its own logic and its own truth; it reduces the free faculty of
    the mind. But it also reduces the gap between imagination and Reason. The two
    antagonistic faculties become interdependent on common ground. In the light of
    the capabilities of advanced industrial civilization, is not all play of the
    imagination playing with technical possibilities, which can be tested as to
    their chances of realization? The romantic idea of a “science of the
    Imagination” seems to assume an ever-more-empirical aspect.

    [...]

    Imagination has not remained immune to the process of reification. We are
    possessed by our images, suffer our own images. Psychoanalysis knew it well,
    and knew the consequences. However, “to give to the imagination all the means
    of expression” would be regression. The mutilated individuals (mutilated also
    in their faculty of imagination) would organize and destroy even more than they
    are now permitted to do. Such release would be the unmitigated horror—not the
    catastrophe of culture, but the free sweep of its most repressive tendencies.
    Rational is the imagination which can become the a priori of the reconstruction
    and redirection of the productive apparatus toward a pacified existence, a life
    without fear. And this can never be the imagination of those who are possessed
    by the images of domination and death.

    To liberate the imagination so that it can be given all its means of expression
    presupposes the repression of much that is now free and that perpetuates a
    repressive society. And such reversal is not a matter of psychology or ethics
    but of politics, in the sense in which this term has here been used throughout:
    the practice in which the basic societal institutions are developed, defined,
    sustained, and changed. It is the practice of individuals, no matter how
    organized they may be. Thus the question once again must be faced: how can the
    administered individuals—who have made their mutilation into their own
    liberties and satisfactions, and thus reproduce it on an enlarged
    scale—liberate themselves from themselves as well as from their masters? How is
    it even thinkable that the vicious circle be broken?

### Qualitative Change

    Qualitative change is conditional upon planning for the whole against these
    interests, and a free and rational society can emerge only on this basis.

    The institutions within which pacification can be envisaged thus defy the
    traditional classification into authoritarian and democratic, centralized and
    liberal administration. Today, the opposition to central planning in the name
    of a liberal democracy which is denied in reality serves as an ideological prop
    for repressive interests. The goal of authentic self-determination by the
    individuals depends on effective social control over the production and
    distribution of the necessities (in terms of the achieved level of culture,
    material and intellectual).

    Here, technological rationality, stripped of its exploitative features, is the
    sole standard and guide in planning and developing the available resources for
    all. Self-determination in the production and distribution of vital goods and
    services would be wasteful. The job is a technical one, and as a truly
    technical job, it makes for the reduction of physical and mental toil. In this
    realm, centralized control is rational if it establishes the preconditions for
    meaningful self-determination. The latter can then become effective in its own
    realm—in the decisions which involve the production and distribution of the
    economic surplus, and in the individual existence.

    In any case, the combination of centralized authority and direct democracy is
    subject to infinite variations, according to the degree of development.
    Self-determination will be real to the extent to which the masses have been
    dissolved into individuals liberated from all propaganda, indoctrination, and
    manipulation, capable of knowing and comprehending the facts and of evaluating
    the alternatives. In other words, society would be rational and free to the
    extent to which it is organized, sustained, and reproduced by an essentially
    new historical Subject.

    At the present stage of development of the advanced industrial societies, the
    material as well as the cultural system denies this exigency. The power and
    efficiency of this system, the thorough assimilation of mind with fact, of
    thought with required behavior, of aspirations with reality, militate against
    the emergence of a new Subject. They also militate against the notion that the
    replacement of the prevailing control over the productive process by “control
    from below” would mean the advent of qualitative change. This notion was valid,
    and still is valid, where the laborers were, and still are, the living denial
    and indictment of the established society. However, where these classes have
    become a prop of the established way of life, their ascent to control would
    prolong this way in a different setting.  And yet, the facts are all there
    which validate the critical theory of this society and of its fatal
    development: the increasing irrationality of the whole; waste and restriction
    of productivity; the need for aggressive expansion; the constant threat of war;
    intensified exploitation; dehumanization. And they all point to the historical
    alternative: the planned utilization of resources for the satisfaction of vital
    needs with a minimum of toil, the transformation of leisure into free time, the
    pacification of the struggle for existence.

### Terrorized beauty

    Beauty reveals its terror as highly classified nuclear plants and laboratories
    become “Industrial Parks” in pleasing surroundings; Civil Defense Headquarters
    display a “deluxe fallout-shelter” with wall-to-wall carpeting (“soft”), lounge
    chairs, television, and Scrabble, “designed as a combination family room during
    peacetime (sic!) and family fallout shelter should war break out.”1 If the
    horror of such realizations does not penetrate into consciousness, if it is
    readily taken for granted, it is because these achievements are (a) perfectly
    rational in terms of the existing order, (b) tokens of human ingenuity and
    power beyond the traditional limits of imagination.

### What brings chance: practice

    Dialectical theory is not refuted, but it cannot offer the remedy. It cannot be
    positive. To be sure, the dialectical concept, in comprehending the given
    facts, transcends the given facts. This is the very token of its truth. It
    defines the historical possibilities, even necessities; but their realization
    can only be in the practice which responds to the theory, and, at present, the
    practice gives no such response.

    On theoretical as well as empirical grounds, the dialectical concept pronounces
    its own hopelessness. The human reality is its history and, in it,
    contradictions do not explode by themselves. The conflict between streamlined,
    rewarding domination on the one hand, and its achievements that make for
    self-determination and pacification on the other, may become blatant beyond any
    possible denial, but it may well continue to be a manageable and even
    productive conflict, for with the growth in the technological conquest of
    nature grows the conquest of man by man. And this conquest reduces the freedom
    which is a necessary a priori of liberation. This is freedom of thought in the
    only sense in which thought can be free in the administered world—as the
    consciousness of its repressive productivity, and as the absolute need for
    breaking out of this whole. But precisely this absolute need does not prevail
    where it could become the driving force of a historical practice, the effective
    cause of qualitative change. Without this material force, even the most acute
    consciousness remains powerless.

    No matter how obvious the irrational character of the whole may manifest itself
    and, with it, the necessity of change, insight into necessity has never
    sufficed for seizing the possible alternatives. Confronted with the omnipresent
    efficiency of the given system of life, its alternatives have always appeared
    utopian. And insight into necessity, the consciousness of the evil state, will
    not suffice even at the stage where the accomplishments of science and the
    level of productivity have eliminated the utopian features of the
    alternatives—where the established reality rather than its opposite is utopian.

    [...]

    The enchained possibilities of advanced industrial societies are: development
    of the productive forces on an enlarged scale, extension of the conquest of
    nature, growing satisfaction of needs for a growing number of people, creation
    of new needs and faculties. But these possibilities are gradually being
    realized through means and institutions which cancel their liberating
    potential, and this process affects not only the means but also the ends. The
    instruments of productivity and progress, organized into a totalitarian system,
    determine not only the actual but also the possible utilizations.

    [...]

    But the struggle for the solution has outgrown the traditional forms. The
    totalitarian tendencies of the one-dimensional society render the traditional
    ways and means of protest ineffective—perhaps even dangerous because they
    preserve the illusion of popular sovereignty. This illusion contains some
    truth: “the people,” previously the ferment of social change, have “moved up”
    to become the ferment of social cohesion. Here rather than in the
    redistribution of wealth and equalization of classes is the new stratification
    characteristic of advanced industrial society.
