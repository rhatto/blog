[[!meta title="O Segundo Sexo"]]

* Autora: Simone de Beauvoir
* Info: [Le Deuxième Sexe – Wikipédia, a enciclopédia livre](https://pt.wikipedia.org/wiki/Le_Deuxi%C3%A8me_Sexe).

## Trechos

    Se as dificuldades são mais evidentes na mulher independente é porque
    ela não escolheu a resignação e sim a luta. Todos os problemas vivos
    encontram na morte uma solução silenciosa.

    -- 456

    É absurdo pretender que a orgia, o vício, o êxtase, a paixão se tornariam
    impossíveis se o homem e a mulher fossem concretamente semelhantes.
    
    -- 499

    Atentemos para o fato de que nossa imaginação despovoa sempre o futuro;
    êste não passa de uma abstração para nós; cada um de nós nele deplora surdamente
    a ausência do que foi; mas a humanidade de amanhã irá vivê-lo em sua carne
    e em sua liberdade, ele será seu presente e ela por sua vez o preferirá;
    entre os sexos surgirão novas relações carnais e afetivas de que não temos
    idéia; já apareceram entre homens e mulheres amizades, rivalidades, cumplicidades,
    camaradagens, castas ou sexuais, que os séculos passados não teriam sabido inventar.

    -- ?
