[[!meta title="Os Jogos e os Homens"]]

A máscara e a vertigem

## Referências

* [O universo lúdico proposto por Caillois](http://www.efdeportes.com/efd127/o-universo-ludico-proposto-por-caillois.htm).
* [Man, Play and Games - Wikipedia](https://en.wikipedia.org/wiki/Man,_Play_and_Games).

## Meta

* Author: [Roger Caillois](https://en.wikipedia.org/wiki/Roger_Caillois).
* Editora: Cotovia
* Ano: 1990
* ISBN: 972-9013-28-4
* Tradução: José Garcez Palha

Original:

* Editora: Gallimard
* Ano: 1958

## Índice

* Jogo, noção de totalidade fechada, preciosa inovação num mundo incerto, 10; noção complexa, 11.

* Regras versus sem regras (jogos de imitação por exemplo), 28.

* Regras das regras: jogos como atividades: 1. livres; 2; delimitadas; 3.
  incertas; 4. improdutivas; 5.regulamentadas; 6. fictícias (págs 29-30).

* Jogos e animais: 35, 40; alea aparece apenas no humano (38).

* Papel civilizador dos jogos e sua corrupção, 68.

## Básico

    Esta noção de totalidade fechada, completa e imutável de início, concebida para
    funcionar sem outra intervenção externa que não seja a energia que lhe dá o
    movimento, constitui decerto uma preciosa inovação um mundo essencialmente em
    mudança, cujos dados são praticamente infinitos e, por outro lado, se
    transforma sem cessar.

    -- 10

    O termo <<jogo>> combina, então, em si as ideias de limites, liberdade e
    invenção. Num registro próximo, exprime uma notável combinação onde se lêem
    conjuntamente os conceitos de sorte e de destreza, dos recursos recebidos do
    azar ou da sorte e da mais ou menos arguta inteligência que as põe em prática e
    que trata de tirar delas o máximo proveito.  Expressões como _ter bom jogo_
    correspondem ao primeito dos sentidos, outras como _jogar pelo seguro_ ou
    mostrar o jogo ou, inversamente, _dissimulr o jogo_, referem-se
    inextricavelmente a ambos: vantagens à partida e um delinear astuto duma
    sapiente estratégia.

    O conceito de risco vem imediatamente complicar os dados já de si confusos:
    a avaliação dos recursos disponíveis e o cálculo das eventualidades previstas
    fazem-se de súbito acompanhar duma outra especulação, uma espécie de aposta
    que supõe uma comparação entre o risco aceite e o resultado previsto. Daí
    decorrem expressões como pôr em jogo, jogar forte, jogas as sobras, jogar a
    sua carreira, jogar a sua vida, ou ainda a constatação de que sai mais cara
    a mecha do que o sebo, ou seja, que o maior dos gahos que se possa esperar
    do jogo será sempre inferor ao preço da luz que o ilumina.

    O _jogo_, aparece novamente como uma noção particularmente complexa que associa
    um estado de facto, uma cartada favorável ou deplorável, onde o acaso e
    soberano e onde o jogador recebe, por fortuna ou por desgraça, sem nada poder
    fazer, a uma aptidão para tirar o melhor partido dos seus desiguais recursos.
    Estes, delapidados pela negligência, só serão frutificados por um cálculo
    sagaz, por uma escolha entre a prudência e a audácia que assim colabora com uma
    segunda coordenada, isto é, em que medida o jogador se dispõe a apostar mais do
    que lhe escapa do que naquilo que controla.

    Todo o jogo é um sistema de regras que definem o que é e o que não é do jogo,
    ou seja, o permitido e o proibido. Estas convenções são simultaneamente
    arbitrárias, imperativas e inapeláveis.

    -- 11

    Finalmente a palavra _jogo_, apela para uma ideia de amplitude, de facilidade
    de movimentos, uma liberdade útil mas não excessiva, quando se fala de _jogo_
    de uma engrenagem ou quando se diz que um navio _joga_ a sua âncora. Esta
    amplitude torna possível uma indispensável mobilidade. É o jogo que subsiste
    entre os diversos elementos que permite o funcionamento de um mecanismo. Por
    outro lado, esse jogo não deve ser exagerado pos a máquina enlouqueceria. Desta
    feita, este espaço cuidadosamente contado impede o bloqueio e o desajusta.
    _Jogo_ significa, portanto, a liberdade que deve permanecer no seio do próprio
    rigor, para que este último adquira ou conserve a sua eficácia. Além do mais,
    todo o mecanismo pode ser considerado como uma espécie de jogo numa outra
    acepção do termo, que o dicionário precisa da seguinte forma: <<Acção regular e
    combinada de diversas partes duma máquina.>> Uma máquina, de facto, é um
    __puzzle__ de peças concebidas para se adaptarem umas às outras e para
    funcionar em harmonia. Mas, no interior deste jogo, todo ele exactidão,
    intervém, dando-lhe vida, um jogo de outro tipo. O primeiro é combinação
    exacta, relojoaria perfeita, o segundo é elasticidade e margem de movimentos.

    -- 12

    São estas as variadas e ricas acepções que mostram em que medida, não o jogo em
    si, mas as disposições psicológicas que ele traduz e fomenta, podem
    efecivamente constituir importantes factores civilizacionais. Globalmente,
    estes diferentes sentidos implicam noções de totalidade, regra e liberdade. Um
    deles associa a existência de limites à faculdade de inventar dentro desses
    limites. Um outro inicia-se entre os recursos herdados da sorte e a arte de
    alcançar a vitória, socorrendo-se apenas dos recursos íntimos, inalienáveis,
    que dependem exclusivamente do zelo e da obstinação individual. Um terceiro
    opõe o cálculo e o risco. Um outro ainda convida a conceber leis imperiosas e
    simultaneamente sem outra sanção para além da sua própria destruição, ou
    preconiza a conveiência e manter alguma lacuna ou disponibilidade no seio da
    mais rigorosa das economias.

    Há casos em que os limites se esfumam, em que a regra se dissolve e há outros
    em que, pelo contrario, a librdade e a invenção estão prestes a desaparecer.
    Mas o jogo significa que os dois pólos subsistem e que há uma relação que se
    mantém entre um e outro.  Propõe e difunde estruturas abstractas, imagens de
    locais fechados e reservados, onde podem ser levadas a cabo concorrências
    ideais. Essas estruturas, essas concorrências são, igualmente, modelos para as
    instituições e para os comportamentos individuais. Não são segura e
    directamente aplicáveis a um real sempre problemático, equívoco, emaranhado e
    variado onde os interesses e as paixões não se deixam facilmente dominar mas
    onde a violência e a traição são moeda corrente. Contudo, os modelos sugeridos
    pelos jogos constituem também antecipações do universo regrado que deverá
    substituir a anarquia natural.

    -- 12-13

## Azar e a matemática

    Os jogos de competição conduzem ao desporto, os jogos de imitação e de ilusão
    prefiguram as religiões do espectáculo. Os jogos de azar e de combinação
    estiveram na origem de vários desenvolvimentos das matemáticas, do cálculo de
    probabilidades à topologia.

    -- 15

## Das regras

    As emaranhadas e confusas leis da vida diária são substituídas, nesse espaço
    definido e durante esse tempo determinado, por regras precisas, arbitrárias,
    irrecusáveis, que têm de se aceitar como tais e que presidem ao correcto
    desenrolar da partida.  Se o trapaceiro as viola, pelo menos finge
    respeitá-las. Não as discute: abusa da lealdade dos outros jogadores. Sob este
    ponto de vista, temos de estar de acordo com os autores que sublinharam que a
    desonestidade do trapaceiro não destrói o jogo. O que o destrói é o pessimista
    que denuncia o carácter absurdo das leis, a sua natureza meramente
    convencional, e que se recusa a jogar porque o jogo não tem sentido. Os seus
    argumentos são irrefutáveis.  O jogo não tem outro sentido senão enquanto jogo.
    É precisamente por isso que as suas regras são imperiosas e absolutas,
    transcendendo toda e qualquer discussão. Não há nenhuma razão para que elas
    sejam desta e não doutra forma. Quem não as admitir de acordo com esta
    perspectiva tem necessariamente de as considerar uma manifesta extravagância.

    -- 27

## Desfecho

    A dúvida acerca do resultado deve permanecer até o fim. Quando, numa partida de
    cartas, o resultado já não oferece dúvida, não se joga mais, os jogaores põem
    suas cartas na mesa. Na lotaria e na rolea, aposta-se num número que pode sair
    ou não. Numa prova desportiva, as forças dos campeões devem ser equilibradas
    para que cada um possa defender a sua oportunidade até ao fim. Por definição,
    os jogos de habilidade envolvem, para o jogador, o risco de falhar a jogada,
    uma ameaça de derrota, sem que o jogo deixaria de divertir, como acontece a
    quem, por excesso de treino ou de habilidade, ganha sem esforço e
    infalivelmente.
    
    Um desfecho conhecido __a priori__, sem possibilidade de erro ou de
    surpresa, conduzindo claramente a um resultado inelutável, é incompatível com a
    natureza do jogo. É necessária uma renovação constante e imprevisível da
    situação, como a que se produz ao atacar e ao ripostar, no caso da esgrima ou
    do futebol, a cada mudança de bola, no ténis ou ainda no zadrez de cada vez que
    um dos adversários altera uma peça. O jogo consiste na necessidade de enconrar,
    de inventar imediatamente uma resposta _que é livre dentro dos limites das
    regras_. Essa liberdade de acção do jogador, essa margem concedida à acção, é
    essencial ao jogo e explica, em parte, o prazer que ele suscita.
  
    -- 27-28

## Ilynx nos insetos

O cara viaja um pouco demais na maionese, mas esse trecho de todo modo é curioso:

    Estes casos de intoxicação voluntária não são casos isolados. Uma outra
    variedade de formiga, a __iridomyrmex sanguineus__ do Queensland, procura as
    larvas de uma pequena borboleta cinzenta para beber o embriagante líquido que
    elas expelem. Comprimem com as mandíbulas a carne sumarenta dessas larvas para
    as fazerem expelir o suco que contêm. Ao esgotarem uma larva, passam a outra. O
    problema é que as larvas da borboleta devoram os ovos da __iridomyrmex__. Há
    alturas em que o inseto que exala o tal odor <<conhece>> o seu poder e alicia a
    formiga para o vício.

    -- 73-74

## Jogo, máquina e sistema

Foi lendo mais ou menos o seguinte trecho que me veio a ideia de que pode haver
alguma articulação -- que aliás é um dos sentidos da palavra jogo, "o mecanismo
ter jogo" -- entre os conceitos de máquina, sistema e jogo:

    Qualquer instituição funciona, em parte, como um jogo, de tal forma que se
    apresenta também como um jogo que foi necessário instaurar, baseado em novos
    princípios e que ocupou o lugar reservado a um jogo antigo. Esse jogo singular
    responde a outras necessidades, favorece certas normas e legislações, exige
    novas atitudes e novas aptidões. Deste ponto de vista, uma modificação nas
    regras do jogo surge sob a forma de uma revolução. Vejamos um exemplo. Os
    benefícios ou as responsabilidade atribuídas, em tempos, a cada pessoa em
    função do seu nascimento, devem agora ser obtidos por mérito, por resultados
    num concurso ou num exame. O que equivale a dizer que os princípios que
    presidem às diferentes espécies de jogos -- azar ou destreza, sorte ou
    superioridade demonstrada -- também se manifestam fora do universo fechado do
    jogo. Ora, é preciso lembrar que esse mundo é governado por aqueles princípios
    de forma absoluta, sem resistência, digamos, como um mundo fictício sem
    matéria, sem peso, enquanto que, no universo confuso e inextricável das
    relações humanas reais, a sua acção nunca surge isoladamente, nem é sequer
    soberana, ou previamente limitada. Os princípios do jogo desencadeiam
    consequências imprevisíveis. A sua ação, para melhor ou para pior,
    caracteriza-se por uma natural fecundidade.

    [...]

      __a busca da repetição, da simetria, ou, contrariamente, a alegria de
      improvisar, de inventar, de variar as soluções até ao infinito;__

    -- 87

## A máscara de Balta Nunes

    No sec. XVIII, Veneza é, em parte, uma civilização da máscara. Esta serve para
    todos os efeitos e a sua utilização encontra-se regulamentada. Aqui se descreve
    uma delas, a da _bautta_ (_Les Agents secrets de Venise au XIIIe siècle,
    compilação e publicação de Giovanni Comisso, Paris, 1944, p. 37, nota 1:

    "A 'bautta' consistia numa espécie de mantelete com capuz negro e máscara. A
    origem do nome está no grito _bau, bau_, com que se mete medo às crianças. Em
    Veneza todos a usavam, a começar pelo Doge quando queria andar livremente pela
    cidade. Era imposta aos nobres, homens ou mulheres, nos locais públicos, para
    refrear o luxo e igualmente para impedir que os patrícios fossem atingidos na
    sua dignidade em situações de contacto com o povo. Nos teatros, os porteiros
    deviam assegurar-se de que os nobres traziam a _bautta_ bem posta na caa mas,
    assim que entravam na sala, guardavam-na ou tiravam-na a seu bel-prazer. Os
    patrícios, quando necessitavam de conferenciar, por razões de Estado, com os
    embaixadores, também deviam usar a _bautta_ e, o protocolo exigia o mesmo aos
    embaixadores"

    -- 226

Impressionante como a descrição casa com o caso Balta Nunes.

Seria pura coincidência? Ou revela um conhecimento cultural do idealizador da
operação usado com sofisticação perversa? Em tempos em que a itália dos anos
70-90 serve de modelo à caça às bruxas no judiciário não seria de se
impressionar tanto, né não?
