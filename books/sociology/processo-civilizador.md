[[!meta title="O Processo civilizador - Volume 1"]]

## Índice

* Processo civiliza-DOR.
* Memória, autocontrole, adestramento, custo da civilização para os indivíduos, vide introdução.
* Controle social, "restrições ao jogo de emoções".
* O Uso da Faca à Mesa, Do Uso do Garfo à Mesa: ótima dissertação.
* Hilário: "Mudanças de Atitude em Relação a Funções Corporais", sobre urinar, cagar, peidar publicamente, etc.
* [Mittelalterliches Hausbuch](https://commons.wikimedia.org/wiki/Category:Mittelalterliches_Hausbuch_von_Schloss_Wolfegg)
  (Livro de imagens da Idade Média) ([esta é uma das representações que Elias comenta](https://upload.wikimedia.org/wikipedia/commons/2/20/Pinker-HausBuch1480.jpg))
  e [Bruegel](https://www.pieter-bruegel-the-elder.org/).
* Crítica a Talcott Parsons e à apologia ao conceito de "alma" ou "mente" como "fantasma na máquina" (caixa preta,
  conceito explanado por Gilbert Ryle em seu livro The Concept of Mind (1949)).

## Excertos

### Da Introdução de Renato Janine Ribeiro

    Mais que contar anedotas, porém, Elias está mostrando algo que sempre lhe foi
    muito caro, enquanto teoria: o desenvolvimento dos modos de conduta, a
    “civilização dos costumes” (como se chamou a tradução francesa deste livro),
    prova que não existe atitude natural no homem. Acostumamo-nos a imaginar que
    tal ou qual forma de trato é melhor porque melhor expressa a natureza humana —
    nada disso, diz Elias, na verdade o que houve foi um condicionamento (por este
    lado, ele é levemente behaviorista) e um adestramento (por aqui, ele remete a
    Nietzsche e a Freud). Dos débitos para com os psicólogos ele próprio fala, bem
    como de seus referenciais sociológicos, na Introdução de 1968, que vai no fim
    deste volume. Seria bom recordarmos, um pouco, Nietzsche.

    Num de seus mais importantes livros, Da genealogia da moral, Nietzsche insiste
    em como foi difícil e que custos teve, para o homem, a instauração da moral (ou
    mesmo, se quisermos, de várias morais). Em outras palavras, a moralidade não é
    um traço natural, nem legado da graça de Deus — ela foi adquirida por um
    processo de adestramento que terminou fazendo, do homem, um animal
    interessante, um ser previdente e previsível. Foi preciso que, pela dor, ele
    constituísse uma memória, mas não no sentido aparente de apenas não esquecer o
    passado: onde ela mais importa é quando se faz prospectiva, quando se torna
    como que um programa de atuação — marcando o sujeito para lembrar bem o que
    prometeu, o que disse, de modo a não o descumprir. A memória importa não tanto
    pelo conhecimento que traz, mas pela ação que ela governa. O seu custo é a dor.
    Foi preciso torturar para produzi-la — e Pierre Clastres, num artigo, retomou
    esta ideia, descrevendo os ritos de iniciação dos rapazes índios como sendo
    lições de memória futura, inscrição no corpo e na mente da lei da igualdade.*

    É desta maneira que Norbert Elias pensa. Pode respeitar os costumes que se
    civilizaram (transparece até mesmo sua simpatia por eles), mas sempre tem em
    mente que o condicionamento foi e é caro. Uma responsabilidade enorme vai
    pesando sobre o homem à medida que ele se civiliza. E isso tanto se entende à
    luz das torturas, físicas ou psíquicas (destas ele fala, em belas páginas,
    sobre a educação das crianças), que Nietzsche havia identificado na origem da
    cultura, quanto à luz do que Freud diz, no fim da vida, sobre a própria
    civilização: quanto mais aumenta, mais cresce a infelicidade.

    Sabemos que esta equação foi e tem sido bastante contestada — curioso acaso que
    a Introdução de Norbert Elias date do mesmo ano de 1968 que marcou a explosão
    do movimento estudantil e, paralelamente, a publicação do livro de Marcuse,
    Eros e civilização,** que é justamente a primeira grande crítica dirigida à
    ideia de que o custo da Kultur está na infelicidade, no crescente recalcamento
    das pulsões cuja satisfação pode nos fazer felizes. É este um ponto a discutir,
    e sobre o qual duvido que haja resposta convincente, pelo menos por ora.

    [...]

    Richard Sennett, em seu O declínio do homem público,*** propõe justamente, ao
    contrário de Elias, entender como distintivo dos últimos duzentos anos um
    “triunfo da intimidade”, uma ênfase cada vez maior na publicação do que outrora
    seria íntimo e recatado. A própria psicanálise representaria, com o papel dado
    à vida sexual no tratamento, um dos exemplos de como apostamos na revelação de
    nossos afetos mais secretos com a esperança de assim encontrarmos uma vida
    melhor, ou uma cura.

    [...]

    O que pode também ser discutido, nesta obra de Elias, é a ideia de que existe
    um sentido na história. Com frequência, ele volta à sua ideia reguladora de que
    fenômenos à primeira vista carentes de sentido se examinados a olho nu ou na
    escala do tempo imediato revelam, porém, seu nexo quando postos contra uma
    medida de longo prazo. (Temos, aí, mais uma convergência de Elias com os
    historiadores franceses das mentalidades, adeptos da “longa duração” como a
    medida mais adequada para estudar a história.) Esta medida de longo prazo, ou
    “curva de civilização”, como a chama, adquire especial importância quando passa
    a definir pelo menos os últimos setecentos anos da aventura humana. É verdade
    que Elias não chega a apresentar essa “evolução” como sendo a única possível,
    menos ainda como necessária, para o homem. Mas não é menos verdade que a seu
    ver ela é definitiva, e desde que tomou conta do Ocidente foi assumindo um
    caráter irreversível, a tal ponto (fica pelo menos sugerido) que terminará por
    mundializar-se, alterando também os costumes dos povos que, mais primitivos,
    vivem hoje de um modo que se compara à Europa medieval.

### Notas

Do Capítulo I:

    Oswald Spengler, The Decline of the West (Londres, 1926), p.21: “A todas as
    culturas se abrem possibilidades novas de expressão que surgem, amadurecem,
    decaem, e nunca mais voltam… Essas culturas, essências vitais sublimadas,
    crescem com a mesma soberba falta de propósito das flores do campo. Pertencem,
    como as plantas e os animais, à Natureza viva de Goethe, e não à Natureza morta
    de Newton.”

### Kultur e Zivilization

    O conceito de “civilização” refere-se a uma grande variedade de fatos: ao nível
    da tecnologia, ao tipo de maneiras, ao desenvolvimento dos conhecimentos
    científicos, às ideias religiosas e aos costumes.
    
    [...]
    
    Já no emprego que lhe é dado pelos alemães Zivilisation, significa algo de fato
    útil, mas, apesar disso, apenas um valor de segunda classe, compreendendo
    apenas a aparência externa de seres humanos, a superfície da existência humana.
    A palavra pela qual os alemães se interpretam, que mais do que qualquer outra
    expressa-lhes o orgulho em suas próprias realizações e no próprio ser, é
    Kultur.

    [...]

    O que se manifesta nesse conceito de Kultur, na antítese entre profundeza e
    superficialidade, e em muitos conceitos correlatos é, acima de tudo, a
    autoimagem do estrato intelectual de classe média.

### Vanguarda

    Conforme dito acima, o movimento literário da segunda metade do século XVIII
    não tem caráter político, embora, no sentido o mais amplo possível, constitua
    manifestação de um movimento social, uma transformação da sociedade. Para
    sermos exatos, a burguesia como um todo nele ainda não encontrava expressão.
    Ele começou sendo a efusão de uma espécie de vanguarda burguesa, o que
    descrevemos aqui como intelligentsia de classe média: numerosos indivíduos na
    mesma situação e de origens sociais semelhantes espalhados por todo o país,
    pessoas que se compreendiam porque estavam na mesma situação. Só raramente
    membros dessa vanguarda se reuniam em algum lugar como grupo durante um período
    maior ou menor de tempo. Quase sempre viviam isolados ou sós formando uma elite
    em relação ao povo, mas pessoas de segunda classe aos olhos da aristocracia
    cortesã.

    Repetidamente, encontramos nessas obras a ligação entre tal posição social e os
    ideais nelas postulados: o amor à natureza e à liberdade, a exaltação
    solitária, a rendição às emoções do coração, sem o freio da “razão fria”. No
    Werther, cujo sucesso demonstra como esses sentimentos eram típicos de uma dada
    geração, isto é dito de maneira bem clara e inequívoca.

    [...]

    E em 15 de março de 1772: “Rilho os dentes… Após o jantar na casa do conde,
    andamos de um lado para outro no grande parque. Aproxima-se a hora social.
    Penso, sabe Deus sobre nada.” Ele permanece ali, os nobres chegam. As mulheres
    murmuram entre si, alguma coisa circula entre os homens. Finalmente, o conde,
    um tanto embaraçado, pede-lhe que se retire. A nobreza sente-se insultada ao
    ver um burguês entre seus membros.
    
    “‘Sabe’”, diz o conde, “‘acho que os convivas estão aborrecidos em vê-lo
    aqui.’… Afastei-me discretamente da ilustre companhia e me dirigi a M., a fim
    de observar o pôr do sol do alto da colina, enquanto lia no meu Homero o canto
    que celebra como Ulisses foi hospitaleiramente recebido pelos excelentes
    guardadores de porcos.”

    Por um lado, superficialidade, cerimônia, conversas
    formais; por outro, vida interior, profundidade de sentimento, absorção em
    livros, desenvolvimento da personalidade individual. Temos o mesmo contraste
    referido por Kant, na antítese entre Kultur e civilização, aplicado aqui a uma
    situação social muito específica.

    No Werther, Goethe mostra também com particular clareza as duas frentes entre
    as quais vive a burguesia. “O que mais me irrita”, lemos na anotação de 24 de
    dezembro de 1771, “é nossa odiosa situação burguesa. Para ser franco, sei tão
    bem como qualquer outra pessoa como são necessárias as diferenças de classe,
    quantas vantagens eu mesmo lhes devo. Apenas não deviam se levantar diretamente
    como obstáculos no meu caminho.” Coisa alguma caracteriza melhor a consciência
    de classe média do que essa declaração. As portas debaixo devem permanecer
    fechadas. As que ficam acima têm que estar abertas. E como todas as classes
    médias, esta estava aprisionada de uma maneira que lhe era peculiar: não podia
    pensar em derrubar as paredes que bloqueavam a ascensão por medo de que as que
    a separavam dos estratos mais baixos pudessem ceder ao ataque.

    Todo o movimento foi de ascensão para a nobreza: o bisavô de Goethe fora
    ferreiro,13 seu avô alfaiate e, em seguida, estalajadeiro, com uma clientela
    cortesã, e maneiras cortesãs-burguesas. Já abastado, seu pai tornou-se
    conselheiro imperial, burguês rico, de meios independentes, possuidor de
    título. Sua mãe era filha de uma família patrícia de Frankfurt.

    O pai de Schiller era cirurgião e, mais tarde, major, mal remunerado; mas seu
    avô, seu bisavô e seu tataravô haviam sido padeiros. De origens sociais
    semelhantes, ora mais próximas ora mais remotas, dos ofícios e da administração
    de nível médio vieram Schubart, Bürger, Winkelmann, Herder, Kant, Friedrich
    August Wolff, Fichte, e muitos outros membros do movimento.

    [...]

    De modo geral, permaneceram muito altas, segundo os padrões ocidentais, as
    paredes entre a intelligentsia de classe média e a classe superior
    aristocrática na Alemanha.

    [...]

    A burguesia comercial, que poderia ter servido como público para os escritores,
    é relativamente subdesenvolvida na maioria dos Estados alemães no século XVIII.
    A ascensão para a prosperidade apenas ensaia os primeiros passos nesse período.
    Até certo ponto, por conseguinte, os escritores e intelectuais alemães como que
    flutuam no ar. Mente e livros são seu refúgio e domínio, e as realizações na
    erudição e na arte seu motivo de orgulho. Dificilmente existe para esta classe
    oportunidade de ação política, de metas políticas. Para ela, o comércio e a
    ordem econômica, em conformidade com a estrutura da vida que levam e da
    sociedade onde se integram, são interesses marginais.
    O comércio, as comunicações e as indústrias são relativamente subdesenvolvidos
    e ainda necessitam, na maior parte, de proteção e promoção mediante uma
    política mercantilista, e não de libertação de suas restrições. O que legitima
    a seus próprios olhos a intelligentsia de classe média do século XVIII, o que
    fornece os alicerces à sua autoimagem e orgulho, situa-se além da economia e da
    política. Reside no que, exatamente por esta razão, é chamado de das rein
    Geistige (o puramente espiritual) em livros, trabalho de erudição, religião,
    arte, filosofia, no enriquecimento interno, na formação intelectual (Bildung)
    do indivíduo, principalmente através de livros, na personalidade.

    [...]

    Uma descrição muito esclarecedora da diferença entre esta classe intelectual
    alemã e sua contrapartida francesa é também encontrada nas conversas de Goethe
    com Eckermann: Ampère chega a Weimar. (Goethe não o conhecia pessoalmente, mas
    com frequência o elogiara para Eckermann) Para espanto de todo mundo,
    descobre-se que o festejado Monsieur Ampère é “um alegre jovem na casa dos 20
    anos”. Eckermann manifesta surpresa e Goethe responde (quinta-feira, 23 de maio
    de 1827):

        Não tem sido fácil para você em sua terra nativa, e nós no centro da
        Alemanha tivemos que pagar muito caro pela pouca sabedoria que possuímos. Isto
        porque, no fundo, levamos uma vida isolada, paupérrima! Pouquíssima cultura nos
        chega do próprio povo e todos os nossos homens de talento estão dispersos pelo
        país. Um está em Viena, outro em Berlim, um terceiro em Königsberg, o quarto em
        Bonn ou Düsseldorf, todos separados entre si por 50 ou 100 milhas, de modo que
        é uma raridade o contato pessoal ou uma troca pessoal de ideias. Sinto o que
        isto significa quando homens como Alexander von Humboldt passam por aqui e
        fazem com que meus estudos progridam mais num único dia do que se eu tivesse
        viajado um ano inteiro em meu caminho solitário.

        Mas agora imagine uma cidade como Paris, onde as mentes mais notáveis de todo o
        reino estão reunidas num único lugar, e em seu intercâmbio, competição e
        rivalidade diárias eles se ensinam e se estimulam a prosseguir, onde o melhor
        de todas as esferas da natureza e da arte de toda a superfície da terra pode
        ser visto em todas as ocasiões. Imagine essa metrópole onde cada ponta que se
        transpõe e cada praça que se cruza evocam um grande passado. E em tudo isto não
        pense na Paris de uma época monótona e embotada, mas na Paris do século XIX,
        onde durante três gerações, graças a homens como Molière, Voltaire e Diderot,
        essa riqueza de ideias foi posta em circulação como em nenhuma outra parte de
        todo o globo, e compreenderá que uma boa mente como a de Ampère, tendo se
        desenvolvido em meio a tal abundância, pode muito bem chegar a ser alguma coisa
        no seu 24o ano de vida.

    [...]

    Na França, a conversa é um dos mais importantes meios de comunicação e, além
    disso, há séculos é uma arte; na Alemanha, o meio de comunicação mais
    importante é o livro, e é uma língua escrita unificada, e não uma falada, que
    essa classe intelectual desenvolve. Na França, até os jovens vivem em um
    ambiente de rica e estimulante intelectualidade; mas o jovem membro da classe
    média alemã tem que subir a muito custo em relativa solidão e isolamento.

### Civilização como máquina automática em constante reforma

    No seu Ami des hommes, argumenta Mirabeau em certa altura que a superabundância
    de dinheiro reduz a população, de modo que aumenta o consumo por indivíduo.
    Acha que esse excesso de dinheiro, caso se torne grande demais, “expulsa a
    indústria e as artes, lançando, desta maneira, os Estados na pobreza e no
    despovoamento”. E continua: “À vista disto, notamos como o ciclo de barbárie a
    decadência, passando pela civilização e a riqueza, poderia ser invertido por um
    ministro alerta e hábil, e nova corda seria dada à máquina antes que ela
    parasse.”28 Esta frase realmente sumaria tudo o que se tornaria característico,
    em termos muito gerais, do ponto de vista fundamental dos fisiocratas: a
    concepção de economia, população e, finalmente, costumes como um todo
    inter-relacionado, desenvolvendo-se ciclicamente; e a tendência política
    reformista que dirige finalmente este conhecimento aos governantes, a fim de
    capacitá-los, pela compreensão dessas leis, a orientar os processos sociais de
    uma maneira mais esclarecida e racional do que até então.
    
    [...]

    A crítica de Mirabeau, nobre proprietário de terras, à riqueza, ao luxo, e a
    todos os costumes vigentes dá uma coloração especial a suas ideias. A
    verdadeira civilização, pensa, situa-se em um ciclo entre a barbárie e a falsa
    civilização, “decadente”, gerada pela superabundância de dinheiro. A missão do
    governo esclarecido é dirigir este automatismo, de modo que a sociedade possa
    florescer em um curso médio entre a barbárie e a decadência. Aqui, toda a faixa
    de problemas latentes em “civilização” já é discernível no momento da formação
    do conceito. Já nessa fase ela está ligada à ideia de decadência ou “declínio”,
    que reemerge repetidamente, em forma visível ou velada segundo o ritmo das
    crises cíclicas. Mas podemos também ver claramente que este desejo de reforma
    permanece sem exceção dentro do contexto do sistema social vigente, manipulado
    de cima, e que não opõe, ao que critica nos costumes do tempo, uma imagem ou
    conceito absolutamente novos, mas, em vez disso, parte da ordem existente,
    desejando melhorá-la: através de medidas hábeis e esclarecidas tomadas pelo
    governo, a “falsa civilização” mais uma vez se tornará boa e autêntica.

    [...]

    Nesses mesmos anos, a palavra civilisation surge pela primeira vez como um
    conceito amplamente usado e mais ou menos preciso. Na primeira edição da
    Histoire philosophique et politique des établissements et du commerce des
    Européens dans les deux Indes (1770), do padre Raynal, a palavra não ocorre nem
    uma única vez; na segunda (1774), ela é “usada frequentemente e sem a menor
    variação de significado como termo indispensável e geralmente entendido”.30

    No Système de la nature, de Holbach, publicado em 1770, não aparece a palavra
    civilisation. Mas no seu Système sociale, editado em 1774, ela é usada com
    frequência. Diz ele, por exemplo: “Nada há que oponha mais obstáculos no
    caminho da felicidade pública, do progresso da razão humana, de toda a
    civilização dos homens do que as guerras contínuas para as quais príncipes
    estouvados são atraídos a cada momento.”31 Ou, em outro trecho: “A razão humana
    não é ainda suficientemente exercitada; a civilização dos povos não se
    completou ainda; obstáculos inumeráveis se opuseram até agora ao progresso do
    conhecimento útil, cujo avanço só poderá contribuir para o aperfeiçoamento de
    nosso governo, nossas leis, nossa educação, nossas instituições e nossa
    moral.”32

    O conceito subjacente a esse movimento esclarecido de reforma, socialmente
    crítico, é sempre o mesmo: que o aprimoramento das instituições, da educação e
    da lei será realizado pelo aumento dos conhecimentos. Isto não significa
    “erudição” no sentido alemão do século XVIII, porquanto os que aqui se
    expressam não são professores universitários, mas escritores, funcionários,
    intelectuais, cidadãos refinados dos mais diversos tipos, unidos através do
    medium da “boa sociedade”, os salons. O progresso será obtido, por conseguinte,
    em primeiro lugar pela ilustração dos reis e governantes em conformidade com a
    “razão” ou a “natureza”, o que vem a ser a mesma coisa, e em seguida pela
    nomeação, para os principais cargos, de homens esclarecidos (isto é,
    reformistas). Certo aspecto desse processo progressista total passou a ser
    designado por um conceito fixo: civilisation. O que era visível na versão
    individual que Mirabeau tinha do conceito, o que não fora ainda polido pela
    sociedade, e que era característico de todos os movimentos de reforma, era
    encontrado também aqui: uma meia afirmação e uma meia negação da ordem vigente.
    A sociedade, deste ponto de vista, atingira uma fase particular na rota para a
    civilização. Mas era insuficiente. Não podia ficar parada nesse ponto. O
    processo continuava e devia ser levado adiante: “a civilização dos povos ainda
    não se completou.” Duas ideias se fundem no conceito de civilização. Por um
    lado, ela constitui um contraconceito geral a outro estágio da sociedade, a
    barbárie. Este sentimento há muito permeava a sociedade de corte. Encontrara
    sua expressão aristocrática de corte em termos como politesse e civilité.

    Mas os povos não estão ainda suficientemente civilizados, dizem os homens do
    movimento de reforma de corte/classe média. A civilização não é apenas um
    estado, mas um processo que deve prosseguir. Este é o novo elemento manifesto
    no termo civilisation. Ele absorve muito do que sempre fez a corte acreditar
    ser — em comparação com os que vivem de maneira mais simples, mais incivilizada
    ou mais bárbara — um tipo mais elevado de sociedade: a ideia de um padrão de
    moral e costumes, isto é, tato social, consideração pelo próximo, e numerosos
    complexos semelhantes. Nas mãos da classe média em ascensão, na boca dos
    membros do movimento reformista, é ampliada a ideia sobre o que é necessário
    para tornar civilizada uma sociedade. O processo de civilização do Estado, a
    Constituição, a educação e, por conseguinte, os segmentos mais numerosos da
    população, a eliminação de tudo o que era ainda bárbaro ou irracional nas
    condições vigentes, fossem as penalidades legais, as restrições de classe à
    burguesia ou as barreiras que impediam o desenvolvimento do comércio — este
    processo civilizador devia seguir-se ao refinamento de maneiras e à pacificação
    interna do país pelos reis.

### Ruderia

    Erasmo fala, por exemplo, da maneira como as pessoas olham.

    [...]

    A postura, os gestos, o vestuário, as expressões faciais — este comportamento
    “externo” de que cuida o tratado é a manifestação do homem interior, inteiro.
    Erasmo sabe disso e, vez por outra, o declara explicitamente: “Embora este
    decoro corporal externo proceda de uma mente bem-constituída não obstante
    descobrimos às vezes que, por falta de instrução, essa graça falta em homens
    excelentes e cultos.” Não deve haver meleca nas narinas, diz ele mais adiante.
    O camponês enxuga o nariz no boné ou no casaco e o fabricante de salsichas no
    braço ou no cotovelo. Ninguém demonstra decoro usando a mão e, em seguida,
    enxugando-a na roupa. É mais decente pegar o catarro em um pano,
    preferivelmente se afastando dos circunstantes. Se, quando o indivíduo se assoa
    com dois dedos, alguma coisa cai no chão, ele deve pisá-la imediatamente com o
    pé. O mesmo se aplica ao escarro.

    Com o mesmo infinito cuidado e naturalidade com que essas coisas são ditas — a
    mera menção das quais choca o homem “civilizado” de um estágio posterior, mas
    de diferente formação afetiva — somos ensinados a como sentar ou cumprimentar
    alguém. São descritos gestos que se tornaram estranhos para nós, como, por
    exemplo, ficar de pé sobre uma perna só. E bem que caberia pensar que muitos
    dos movimentos estranhos de caminhantes e dançarinos que vemos em pinturas ou
    estátuas medievais não representam apenas o “jeito” do pintor ou escultor, mas
    preservam também gestos e movimentos reais que se tornaram estranhos para nós,
    materializações de uma estrutura mental e emocional diferente.

    [...]

    Conforme já mencionado, os pratos são também raros. Quadros mostrando cenas de
    mesa dessa época ou anterior sempre retratam o mesmo espetáculo, estranho para
    nós, que é indicado no tratado de Erasmo. A mesa é às vezes forrada com ricos
    tecidos, às vezes não, mas sempre são poucas as coisas que nela há: recipientes
    para beber, saleiro, facas, colheres, e só. Às vezes, vemos fatias de pão, as
    quadrae, que em francês são chamadas de tranchoir ou tailloir. Todos, do rei e
    rainha ao camponês e sua mulher, comem com as mãos. Na classe alta há maneiras
    mais refinadas de fazer isso, Deve-se lavar as mãos antes de uma refeição, diz
    Erasmo. Mas não há ainda sabonete para esse fim. Geralmente, o conviva estende
    as mãos e o pajem derrama água sobre elas. A água é às vezes levemente
    perfumada com camomila ou rosmaninho.5 Na boa sociedade, ninguém põe ambas as
    mãos na travessa. É mais refinado usar apenas três dedos de uma mão. Este é um
    dos sinais de distinção que separa a classe alta da baixa.

    Os dedos ficam engordurados. “Digitos unctos vel ore praelingere vel ad tunicam
    extergere… incivile est”, diz Erasmo. Não é polido lambê-los ou enxugá-los no
    casaco. Frequentemente se oferece aos outros o copo ou todos bebem na caneca
    comum. Mas Erasmo adverte: “Enxugue a boca antes.” Você talvez queira oferecer
    a alguém de quem gosta a carne que está comendo. “Evite isso”, diz Erasmo. “Não
    é muito decoroso oferecer a alguém alguma coisa semimastigada.” E acrescenta:
    “Mergulhar no molho o pão que mordeu é comportar-se como um camponês e
    demonstra pouca elegância retirar da boca a comida mastigada e recolocá-la na
    quadra. Se não consegue engolir o alimento, vire-se discretamente e cuspa-o em
    algum lugar.”

    [...]

    Diversoria trata das diferenças entre as maneiras observadas em estalagens
    alemãs e francesas. Descreve, por exemplo, o interior de uma estalagem alemã:
    cerca de 80 ou 90 pessoas estão sentadas, salientando o autor que não são
    apenas pessoas comuns, mas também homens ricos, nobres, homens, mulheres, e
    crianças, todos juntos. E cada um está fazendo o que julga necessário. Um lava
    as roupas e pendura as peças molhadas em cima do forno. Outro lava as mãos. Mas
    a tigela é tão limpa, diz o autor, que a pessoa precisa de outra para se limpar
    da água… É forte o cheiro de alho e outros odores desagradáveis. Pessoas
    escarram por toda parte. Alguém está limpando as botas em cima da mesa. Em
    seguida, a refeição é trazida. Todos molham o pão na travessa, mordem, e
    molham-no novamente. O lugar é sujo e ruim o vinho. Se alguém pede vinho
    melhor, o estalajadeiro responde: já hospedei muitos nobres e condes. Se o
    vinho não lhe serve, procure outras acomodações.

    [...]

    Com a mesma simplicidade e clareza com que ele e Della Casa discutem questões,
    tais como maior tato e decoro, Erasmo diz também: não se mova para a frente e
    para trás na cadeira. Quem faz isso “speciem habet subinde ventris flatum
    emittentis ant emittere conantis” (dá a impressão de constantemente soltar ou
    tentar soltar ventosidades intestinais).

    [...]

    É contra o bom-tom segurar a faca ou a colher com toda mão, como se fosse
    um porrete: segure-as sempre com os dedos.

### Conduta

    A tendência cada vez maior das pessoas de se observarem e aos demais é um dos
    sinais de que toda a questão do comportamento estava, nessa ocasião, assumindo
    um novo caráter: as pessoas se moldavam às outras mais deliberadamente do que
    na Idade Média.

    Dizia-se a elas: façam isto, não façam aquilo. Mas de modo geral muita coisa
    era tolerada. Durante séculos, aproximadamente as mesmas regras, elementares
    segundo nossos padrões, foram repetidas, obviamente sem criar hábitos firmes.
    Neste momento, a situação muda. Aumenta a coação exercida por uma pessoa sobre
    a outra e a exigência de “bom comportamento” é colocada mais enfaticamente.
    Todos os problemas ligados a comportamento assumem nova importância. O fato de
    que Erasmo tenha reunido em um trabalho em prosa regras de conduta que haviam
    sido transmitidas principalmente em versos mnemônicos ou espalhadas em tratados
    sobre outros assuntos, e que tenha pela primeira vez dedicado um livro inteiro
    à questão do comportamento em sociedade, e não apenas à mesa, é um claro sinal
    da crescente importância do tema, como também o foi o sucesso do livro.35 E o
    aparecimento de trabalhos semelhantes, como o Cortesão, de Castiglione, ou o
    Galateo, de Della Casa, para citar apenas os mais conhecidos, aponta na mesma
    direção. Os processos sociais subjacentes já foram indicados e serão discutidos
    adiante em mais detalhes: os velhos laços sociais estão, se não quebrados, pelo
    menos muito frouxos e em processo de transformação. Indivíduos de diferentes
    origens sociais são reunidos de cambulhada. Acelera-se a circulação social de
    grupos e indivíduos que sobem e descem na sociedade.

    Em seguida, lentamente, durante o século XVI, mais cedo aqui, mais tarde ali e
    em quase toda parte com numerosos reveses até bem dentro do século XVII, uma
    hierarquia social mais rígida começa a se firmar mais uma vez e, de elementos
    de origens sociais diversas, forma-se uma nova classe superior, uma nova
    aristocracia. Exatamente por esta razão, a questão de bom comportamento
    uniforme torna-se cada vez mais candente, especialmente porque a estrutura
    alterada da nova classe alta expõe cada indivíduo de seus membros, em uma
    extensão sem precedentes, às pressões dos demais e do controle social. E é
    neste contexto que surgem os trabalhos de Erasmo. Castiglione, Della Casa e
    outros autores sobre as boas maneiras. Forçadas a viver de uma nova maneira em
    sociedade, as pessoas tornam-se mais sensíveis às pressões das outras. Não
    bruscamente, mas bem devagar, o código do comportamento torna-se mais rigoroso
    e aumenta o grau de consideração esperado dos demais. O senso do que fazer e
    não fazer para não ofender ou chocar os outros torna-se mais sutil e, em
    conjunto com as novas relações de poder, o imperativo social de não ofender os
    semelhantes torna-se mais estrito, em comparação com a fase precedente.  As
    regras de courtoisie prescreviam também “Nada diga que possa provocar conflito
    ou irritar os outros”: Non dicas verbum cuiquam quot ei sit acerbum.36

    [...]

    A regra de não estalar os lábios quando se come é também encontrada com
    frequência em instruções medievais. Sua ocorrência no início do livro, porém,
    mostra claramente o que mudou. Demonstra não só quanta importância é nesse
    momento atribuída ao “bom comportamento”, mas, acima de tudo, como aumentou a
    pressão que as pessoas exercem reciprocamente umas sobre as outras. Torna-se
    imediatamente claro que esta maneira polida, extremamente gentil e
    relativamente atenciosa de corrigir alguém, sobretudo quando exercida por um
    superior, é um meio muito mais forte de controle social, muito mais eficaz para
    inculcar hábitos duradouros do que o insulto, a zombaria ou ameaça de violência
    física.

    Nos diversos países formam-se sociedades pacificadas. O velho código de
    comportamento é transformado, mas apenas de maneira muito gradual. O controle
    social, no entanto, torna-se mais imperativo. E, acima de tudo, lentamente muda
    a natureza e o mecanismo do controle das emoções. Na Idade Média, o padrão de
    boas e más maneiras, a despeito de todas as disparidades regionais e sociais,
    evidentemente não mudou de qualquer forma decisiva. Repetidamente, ao longo dos
    séculos, as mesmas boas e más maneiras são mencionadas. O código social só
    conseguiu consolidar hábitos duradouros numa quantidade limitada de pessoas.
    Nesse momento, com a transformação estrutural da sociedade, com o novo modelo
    de relações humanas, ocorre, devagar, uma mudança: aumenta a compulsão de
    policiar o próprio comportamento. Em conjunto com isto é posto em movimento o
    modelo de comportamento.

    [...]

    8. Não é tarefa das mais fáceis tornar esse movimento bem visível, sobretudo
    porque ele ocorre com grande lentidão — em passos bem pequenos, por assim dizer
    — e porque nele acontecem também múltiplas flutuações, seguindo curvas mais
    curtas ou mais longas. É evidente que não basta estudar isoladamente cada única
    fase a qual esta ou aquela declaração sobre costumes e maneiras se refere.
    Temos que tentar enfocar o próprio movimento, ou pelo menos um grande segmento
    dele, como um todo, como se acelerado. Imagens devem ser postas juntas em uma
    série, a fim de nos proporcionar uma visão geral, de um aspecto particular, do
    processo que se desenrola: a transformação gradual de comportamento e emoções,
    o patamar, que se alarga, da aversão.

    [...]

    o movimento deve ser estudado em toda a sua polifonia de muitas camadas, não
    como uma linha, mas como uma espécie de fuga, com uma sucessão de
    movimentos-motifs semelhantes, em níveis diferentes.

    [...]

    Cabe à pessoa de mais alta posição no grupo desdobrar primeiro seu guardanapo e
    os demais devem esperar até que ele o faça, antes de abrirem os seus. Quando as
    pessoas são aproximadamente iguais, todas devem desdobrá-los juntas sem
    cerimônia. [N.B. Com a “democratização” da sociedade e da família isto se
    tornou a regra. A estrutura social, neste caso ainda do tipo
    hierárquico-aristocrático, reflete-se na mais elementar das relações humanas.]É
    errado usar o guardanapo para enxugar o rosto, e mais ainda limpar os dentes
    com ele, e seria uma das mais graves infrações da civilidade usá-lo para se
    assoar… O emprego que pode e deve dar ao guardanapo é o de enxugar a boca,
    lábios, e dedos quando estiverem engordurados, limpar a faca antes de cortar o
    pão e fazer o mesmo com a colher e o garfo depois de usá-los. [N.B. Este é um
    dos muitos exemplos do extraordinário controle do comportamento concretizados
    em nossos hábitos à mesa. O emprego de cada utensílio é limitado e definido por
    grande número de regras bem precisas. Nenhuma delas é evidente por si mesma,
    como pareceram a gerações posteriores. Seu uso foi desenvolvido aos poucos em
    conjunto com a estrutura e mudanças nas relações humanas.]

### Dinâmica

    A proibição não é nem de longe tão autoevidente como hoje. Vemos como, aos
    poucos, transforma-se em um hábito internalizado, em parte do “autocontrole”.

    As mudanças no padrão são muito instrutivas (Exemplo K, abaixo). Em alguns
    aspectos são muito extensas. A diferença já se constata no que não mais precisa
    ser dito. Muitos capítulos tornam-se menores. Muitas “más maneiras” antes
    discutidas em detalhe merecem apenas uma referência de passagem. O mesmo se
    aplica a numerosas funções corporais anteriormente comentadas em grande
    extensão e minúcia. O tom é em geral menos suave e, não raro, muito mais duro
    do que na primeira versão.

    [...]

    Ouvimos pessoas de diferentes épocas falando mais ou menos sobre o mesmo
    assunto. Desta maneira, as mudanças se tornaram mais claras do que se as
    tivéssemos descrito em nossas próprias palavras. Pelo menos do século XVI em
    diante, as injunções e proibições pelas quais é modelado o indivíduo (de
    conformidade com o padrão observado na sociedade) estão em movimento
    ininterrupto. Este movimento, por certo, não é perfeitamente retilíneo, mas,
    através de todas as suas flutuações e curvas individuais, uma tendência global
    clara é apesar de tudo perceptível, se estas vozes dos séculos passados são
    ouvidas em conjunto.

    Os tratados do século XVI sobre as boas maneiras são obra da nova aristocracia
    de corte, que está se aglutinando aos poucos a partir de elementos de várias
    origens sociais. Com ela surge um diferente código de comportamento.

    De Courtin, na segunda metade do século XVII, fala a partir de uma sociedade de
    corte que é a mais plenamente consolidada — a da corte de Luís XIV. E se dirige
    principalmente a pessoas de categoria, pessoas que não vivem diretamente na
    corte, mas que desejam conhecer bem as maneiras e costumes que nela têm curso.

    Afirma ele no prefácio: “Este tratado não se destina à impressão, mas apenas a
    atender ao cavalheiro de província que solicitou ao autor, como amigo
    particular seu, que ministrasse alguns preceitos de civilidade ao seu filho,
    que ele tencionava enviar à corte quando completasse seus estudos… Ele (o
    autor) empreendeu este trabalho apenas para conhecimento de gentes
    bem-nascidas; apenas a elas é dirigido; e particularmente à juventude, que
    poderá encontrar alguma utilidade nestes pequenos conselhos, já que nem todos
    têm a oportunidade nem dispõem de meios para virem à corte, em Paris, aprender
    os refinamentos da polidez.”

    Pessoas que vivem ou fazem parte do círculo que dá exemplo não precisam de
    livros para saber como “alguém” deve se comportar. Isto é óbvio. Por isso é
    importante descobrir com que intenções e para que público esses preceitos são
    escritos e publicados — preceitos que originariamente são o segredo distintivo
    dos fechados círculos da aristocracia de corte.

Escalada das boas maneiras como forma de manutenção da distinção social:

    O público visado é muito claro. Enfatiza-se que os conselhos são apenas para as
    honnêtes gens, isto é, de modo geral, gente da classe alta. Em primeiro lugar,
    o livro atende à necessidade da nobreza provinciana de se informar sobre o
    comportamento na corte e, além disso, à de estrangeiros ilustres. Mas pode-se
    supor que o sucesso apreciável deste livro resultou, entre outras coisas, do
    interesse despertado nos principais estratos burgueses. Há muito material que
    demonstra como, nesse período, os costumes, comportamento e modas da corte
    espraiavam-se ininterruptamente pelas classes médias altas, onde eram imitados
    e mais ou menos alterados de acordo com as diferentes situações sociais. Perdem
    assim, dessa maneira e até certo ponto, seu caráter como meio de identificação
    da classe alta. São, de certa forma, desvalorizados. Este fato obriga os que
    estão acima a se esmerarem em mais refinamentos e aprimoramento da conduta. E é
    desse mecanismo o desenvolvimento de costumes de corte, sua difusão para baixo,
    sua leve deformação social, sua desvalorização como sinais de distinção — que o
    movimento constante nos padrões de comportamento na classe alta recebe em parte
    sua motivação. O importante é que nessa mudança, nas invenções e modas do
    comportamento na corte, que à primeira vista talvez pareçam caóticas e
    acidentais, com o passar do tempo emergem certas direções ou linhas de
    desenvolvimento. Elas incluem, por exemplo, o que pode ser descrito como o
    avanço do patamar do embaraço e da vergonha sob a forma de “refinamento” ou
    como “civilização”. Um dinamismo social específico desencadeia outro de
    natureza psicológica, que manifesta suas próprias lealdades.

Tecnologia:

    Estes são apenas alguns exemplos de como se formou nosso ritual diário. Se esta
    série fosse continuada até o presente, outras mudanças de detalhe seriam
    notadas: novos imperativos são acrescentados, relaxam-se outros antigos, emerge
    uma riqueza de variações nacionais e sociais, e se constata a infiltração na
    classe média, na classe operária e no campesinato do ritual uniforme da
    civilização. A regulação dos impulsos que sua aquisição requer varia muito em
    força. Mas a base essencial do que é obrigatório e do que é proibido na
    sociedade civilizada — o padrão da técnica de comer, a maneira de usar faca,
    garfo, colher, prato individual, guardanapo e outros utensílios — estes
    permanecem imutáveis em seus aspectos essenciais. Até mesmo o surgimento da
    tecnologia em todas as áreas — inclusive na da cozinha —, com a introdução de
    novas formas de energia, deixou virtualmente inalteradas as técnicas à mesa e
    outras formas de comportamento. Só com uma verificação muito minuciosa é que
    observamos os traços de uma tendência que continua a desenvolver-se.

    O que muda ainda, acima de tudo, é a tecnologia da produção. Já a tecnologia do
    consumo foi desenvolvida por formações sociais que eram, em um grau nunca
    igualado antes, classes de consumo. Com seu declínio social, o rápido e intenso
    refinamento das técnicas de consumo cessa, estas passam ao que se torna então a
    esfera privada da vida (em contraste com a ocupacional). Consequentemente, o
    ritmo de movimento e mudança nessas esferas, que havia sido relativamente
    rápido durante o estágio das cortes absolutas, reduz-se mais uma vez.

Forma da curva:

    Não obstante, a forma geral da curva é por toda a parte mais ou menos a mesma:
    em primeiro lugar, a fase medieval, com certo clímax no florescimento da
    sociedade feudal e cortês, assinalada pelo hábito de comer com as mãos. Em
    seguida, uma fase de movimento e mudança relativamente rápidos, abrangendo
    aproximadamente os séculos XVI, XVII e XVIII, na qual a compulsão para uma
    conduta refinada à mesa pressiona constantemente na mesma direção, na de um
    novo padrão de maneiras à mesa.

    Daí em diante, observamos uma fase que permanece dentro do padrão já atingido,
    embora com um movimento muito lento sempre numa certa direção. O refinamento da
    conduta diária nunca perde de todo, nem mesmo neste período, sua importância
    como instrumento de diferenciação social. Mas, desde essa fase, não desempenha
    o mesmo papel que na fase precedente. Mais do que antes, o dinheiro torna-se a
    base das disparidades sociais. E o que as pessoas concretamente realizam e
    produzem torna-se mais importante que suas maneiras.

"Delicadeza" e padronização:

    É semelhante a curva seguida por outros hábitos e costumes. Inicialmente, a
    sopa costuma ser bebida, seja na sopeira comum seja com a concha usada por
    várias pessoas. Nos escritos corteses, é prescrito o uso da colher. Ela,
    também, será então usada por várias pessoas. Outro passo é mostrado na citação
    extraída de Calviac, por volta de 1560. Diz ele que era costume alemão permitir
    que cada conviva usasse sua própria colher. O passo seguinte é indicado pelo
    texto de Courtin, relativo ao ano de 1672. Nessa ocasião, não se toma mais a
    sopa na sopeira comum, mas derrama-se um pouco no próprio prato, usando-se a
    própria colher. Mas havia pessoas, somos informados no texto, que eram tão
    delicadas que não queriam tomar a sopa de uma sopeira em que outros haviam
    mergulhado uma colher já usada. Era, por conseguinte, necessário limpar a
    colher com o guardanapo antes de colocá-la na sopeira. E algumas pessoas
    queriam ainda mais. Para elas, a pessoa não devia absolutamente pôr novamente
    na sopeira uma colher usada. Devia, sim, pedir uma colher limpa para esse fim.

    Descrições como essas demonstram não só que todo o ritual de viver juntos
    estava em movimento, mas também que as pessoas se conscientizavam dessa
    mudança.

    Nesse tempo, gradualmente, o costume, ora aceito como natural, de tomar sopa
    está sendo estabelecido: todos têm seu próprio prato e colher e a sopa é
    servida com um implemento especializado. O ato de comer adquirira um novo
    estilo, correspondendo às novas necessidades da vida social.

    Coisa alguma nas maneiras à mesa é evidente por si mesma ou produto, por assim
    dizer, de um sentimento “natural” de delicadeza. A colher, garfo e guardanapo
    não foram inventados como utensílios técnicos com finalidades óbvias e
    instruções claras de uso. No decorrer de séculos, na relação social e no
    emprego direto, suas funções foram gradualmente sendo definidas, suas formas
    investigadas e consolidadas. Todos os costumes no ritual em mutação, por mais
    insignificantes, estabeleceram-se com infinita lentidão, até mesmo formas de
    comportamento que nos parecem elementares ou simplesmente “razoáveis”, tal como
    o costume de ingerir líquidos apenas com a colher. Todos os movimentos da mão —
    como, por exemplo, a maneira como se segura e movimenta a faca, colher e garfo
    — são padronizados apenas gradualmente, e só vemos o mecanismo de padronização
    em sua sequência, se examinamos como um todo a série de imagens. Há um círculo
    na corte mais ou menos limitado que inicialmente cria os modelos apenas para
    atender às necessidades de sua própria situação social e em conformidade com a
    condição psicológica correspondente à mesma. Mas é evidente que a estrutura e o
    desenvolvimento da sociedade francesa como um todo fazem com que estratos cada
    vez mais amplos se mostrem desejosos, e mesmo sequiosos, de adotar os modelos
    desenvolvidos em uma classe mais alta: eles se difundem, também com grande
    lentidão, por toda a sociedade, e certamente não sem passarem nesse processo
    por algumas modificações.

Transmissão e abrangência da análise:

    A transmissão dos modelos de uma unidade social a outra, ora do centro de uma
    sociedade para seus postos fronteiriços (como, por exemplo, da corte parisiense
    para outras cortes), ora na mesma unidade político-social como, por exemplo, na
    França ou Saxônia, de cima para baixo ou de baixo para cima, deve ser
    considerada, em todo o processo civilizador, como um dos mais importantes dos
    movimentos individuais.

    [...]

    a observação das maneiras e suas transformações expõe apenas um segmento muito
    simples e de fácil acesso do que é um processo de mudança social muito mais
    abrangente.

Fala, jargão a partir de um duplo movimento:

    Neste particular, também, como aconteceu com as maneiras, ocorre uma espécie de
    movimento em duplo sentido: a burguesia é, por assim dizer, “acortesada” e, a
    aristocracia, “aburguesada”. Ou, para ser mais preciso, a burguesia é
    influenciada pelo comportamento da corte e vice-versa. A influência de baixo
    para cima é certamente muito mais fraca no século XVII na França do que no
    século XVIII. Mas não está de todo ausente. O castelo de Vaux-le Vicomte, de
    propriedade do intendente burguês das finanças, Nicolas Fougeut, é anterior à
    régia Versalhes e de muitas maneiras lhe serviu de modelo. Este é um claro
    exemplo. A riqueza dos principais estratos burgueses compele os que estão acima
    a competir com eles. E a chegada incessante de burgueses aos círculos da corte
    gera também um movimento específico na fala: a nova substância humana traz
    também consigo uma nova substância linguística, o “jargão” da burguesia, para
    os círculos aristocráticos. Elementos seus estão sendo constantemente
    assimilados pela linguagem da corte, refinados, polidos, transformados. São, em
    uma palavra, “acortejados”, isto é, adaptados ao padrão de sensibilidade dos
    círculos de corte. Transformam-se, assim, em meios para distinguir as gens de
    la cour da burguesia e depois, talvez muito depois, penetram de novo na
    burguesia, assim refinados e modificados, a fim de se tornarem “especificamente
    burgueses”.

    [...]

    Em quase todos esses casos, a forma linguística que aqui aparece como de corte
    tornou-se de fato o costume nacional. Mas há também exemplos de formas
    linguísticas de corte que são gradualmente abandonadas como “refinadas demais”,
    “afetadas demais”.

    [...]

    10. Se na França as gens de la cour dizem “Esta frase está correta e esta
    incorreta”, uma pergunta importante surge que merece pelo menos ser abordada de
    passagem: “Por que padrões ela está realmente julgando o que é correto e
    incorreto na linguagem? Que critérios usa para selecionar, polir e modificar
    expressões?”

    Às vezes, essa própria gente reflete sobre o assunto. O que diz sobre ele é, à
    primeira vista, surpreendente e, de qualquer modo, sua importância ultrapassa a
    esfera da linguagem. Frases, palavras e nuances são corretas porque eles, os
    membros da elite social, as usam. E são incorretas porque inferiores sociais as
    usam.

    [...]

    “É bem possível,” responde a senhora, “que haja muitas pessoas bem-educadas que
    não conheçam suficientemente bem a delicadeza de nossa língua… uma delicadeza
    que é sentida por apenas um pequeno número de pessoas bem-falantes e que as
    leva a não dizer que um homem virou defunto a fim de dizer que ele faleceu.”

    Um pequeno círculo de pessoas é bem versado nessa delicadeza de linguagem.
    Falar como elas é igual a falar corretamente. O que os outros dizem não conta.
    Os juízos de valor são apodícticos.

    [...]

    Palavras antiquadas são impróprias para a fala comum, séria. Palavras muito
    novas despertam suspeita de afetação — poderíamos talvez dizer, de esnobismo.
    Palavras eruditas que recendem a latim ou grego são suspeitas a todas as gens
    du monde. Cercam os que as usam de uma atmosfera de pedantismo, se são
    conhecidas outras palavras que dizem a mesma coisa com simplicidade.

Motivos ou razões "higiênicas":

    A linguagem é uma das formas assumidas pela vida social ou mental. Grande parte
    do que se pode observar na maneira como a linguagem é plasmada torna-se também
    evidente em outras formas que a sociedade assume. O modo como pessoas
    argumentam que este ou aquele comportamento ou costume à mesa é melhor que
    outro, por exemplo, mal se pode distinguir da maneira como alegam que uma
    expressão linguística é preferível a outra.

    Isto não corresponde à expectativa que talvez tenha um observador do século XX.
    Ele, por exemplo, acha, talvez, que a eliminação do hábito de “comer com as
    mãos”, a adoção do garfo, as louças e talheres individuais, e todos os demais
    rituais de seu próprio padrão podem ser explicados por “razões higiênicas”.
    Isto porque é esta a maneira como ele mesmo explica, de modo geral, esses
    costumes. Mas o fato é que, em data tão recente como a segunda metade do século
    XVIII, praticamente nada desse tipo condicionava o maior controle que as
    pessoas impunham a si mesmas. De qualquer modo, as chamadas “explicações
    racionais” têm bem pouca importância em comparação com outras.

    [...]

    Assim como aconteceu com a maneira por que foi moldada a fala, também na
    formação de outros aspectos do comportamento em sociedade as motivações sociais
    e a adaptação do comportamento aos modelos vigentes em círculos influentes
    foram, de longe, os motivos mais importantes. Até mesmo as expressões usadas na
    motivação do “bom comportamento” à mesa eram, com frequência, as mesmas usadas
    para motivar a “fala correta”.

Tendência ao aumento do embaraço:

    Esta délicatesse, esta sensibilidade, e um sentimento altamente desenvolvido de
    embaraço, são no início aspectos característicos de pequenos círculos da corte
    e, depois, da sociedade da corte como um todo. Isto se aplica à linguagem
    exatamente da mesma maneira que aos hábitos à mesa. Não se diz nem se pergunta
    em que se baseia essa delicadeza e por que ela exige que se faça isto e não
    aquilo. O que se observa é apenas que a “delicadeza” — ou melhor, o patamar do
    embaraço — está avançando. Juntamente com uma situação social muito específica,
    os sentimentos e emoções começam a ser transformados na classe alta, e a
    estrutura da sociedade como um todo permite que as emoções assim modificadas se
    difundam lentamente pela sociedade. Nada indica que a condição afetiva, o grau
    de sensibilidade, sejam mudados pelo que descrevemos como “evidentemente
    racional”, isto é, pela compreensão demonstrável de dadas conexões causais.
    Courtin não diz, como se diria mais tarde, que algumas pessoas acham
    “anti-higiênico” ou “prejudicial à saúde” tomar sopa na mesma sopeira com
    outras pessoas. Não há dúvida de que a delicadeza de sentimentos é aguçada sob
    pressão da situação da corte, isto de uma maneira que mais tarde será
    parcialmente justificada por estudos científicos, mesmo que grande parte dos
    tabus que as pessoas gradualmente se impõem em seus contatos recíprocos, parte
    esta muito maior do que em geral se pensa, não tenha a menor ligação com a
    “higiene”, sendo motivada — ainda hoje — apenas por uma “delicadeza de
    sentimentos”. De qualquer modo, o processo se desenvolve em alguns aspectos de
    uma maneira que é o exato oposto do que em geral hoje se supõe. Em primeiro
    lugar, ao longo de um período extenso e em conjunto com uma mudança específica
    nas relações humanas, isto é, na sociedade, é elevado o patamar de embaraço. A
    estrutura das emoções, a sensibilidade, e o comportamento das pessoas mudam, a
    despeito de variações, em uma direção bem clara. Então, num dado momento, esta
    conduta é reconhecida como “higienicamente correta”, isto é, é justificada por
    uma clara percepção de conexões causais, o que lhe dá mais consistência e
    eficácia. A expansão do patamar do embaraço talvez se ligue ocasionalmente a
    experiências mais ou menos indefinidas e, de início, racionalmente
    inexplicáveis, de como certas doenças são transmitidas ou, mais exatamente,
    talvez se ligue a medos e preocupações vagos e, por conseguinte, não
    esclarecidos, que apontam ambiguamente na direção que mais tarde será
    confirmada pela racionalização. A “compreensão racional”, porém, não é o que
    condiciona a “civilização” dos hábitos à mesa ou outras formas de
    comportamento.

Difusão e cristalização:

    Neste contexto, é altamente instrutivo o estreito paralelo entre a
    “civilização” dos hábitos à mesa e da fala. Fica claro que a mudança do
    comportamento à mesa é parte de uma transformação muito extensa por que passam
    sentimentos e atitudes humanas. Também se vê em que grau as forças motivadoras
    desse fenômeno se originam na estrutura social, na maneira como as pessoas
    estão ligadas entre si. Vemos com mais clareza como círculos relativamente
    pequenos iniciam o movimento e como o processo, aos poucos, se transmite a
    segmentos maiores. Esta difusão, porém, pressupõe contatos muito específicos e,
    por conseguinte, uma estrutura bem-definida da sociedade. Além do mais, ela
    certamente não poderia ter ocorrido se não houvessem sido estabelecidas para
    classes mais amplas, e não apenas para os círculos que criaram o modelo,
    condições de vida — ou, em outras palavras, uma situação social — que tornassem
    possível e necessária uma transformação gradual das emoções e do comportamento,
    um avanço no patamar do embaraço.

    O processo que assim emerge lembra, na sua forma — embora não em substância —,
    processos químicos nos quais um líquido, cujo todo é sujeito a condições de
    mudança química (como, por exemplo, a cristalização), começa adquirindo forma
    cristalina em um pequeno núcleo enquanto o resto só gradualmente se cristaliza
    em torno dele. Nada seria mais errôneo do que considerar o núcleo da
    cristalização como causa da transformação.

### Família como unidade de consumo

    O fato de desaparecer gradualmente, o costume de colocar na mesa grandes
    pedaços de animal para serem trinchados liga-se a muitos fatores. Um dos mais
    importantes talvez seja a redução gradual do tamanho da unidade familiar,59
    como parte do movimento de famílias mais numerosas para famílias menores; em
    seguida, ocorre a transferência de atividades de produção e processamento, como
    fiação, tecelagem e abate de animais, da casa para especialistas, artesãos,
    mercadores e fabricantes, que as desempenham profissionalmente enquanto a
    família torna-se basicamente uma unidade de consumo.

### Supressão de traços animais e ocultamento do desagradável

    Será mostrado que as pessoas, no curso do processo civilizatório, procuram
    suprimir em si mesmas todas as características que julgam “animais”. De igual
    maneira, suprimem essas características em seus alimentos.

    [...]

    A tendência cada vez mais forte de remover o desagradável da vista aplica-se,
    com raras exceções, ao trincho do animal inteiro.

    O ato de trinchar, conforme demonstram os exemplos, outrora constituiu parte
    importante da vida social da classe alta. Depois, o espetáculo passou a ser
    julgado crescentemente repugnante. O trincho em si não desaparece, uma vez que
    o animal, claro, tem que ser cortado antes de ser comido. O repugnante, porém,
    é removido para o fundo da vida social. Especialistas cuidam disso no açougue
    ou na cozinha. Repetidamente iremos ver como é característico de todo o
    processo que chamamos de civilização esse movimento de segregação, este
    ocultamento “para longe da vista” daquilo que se tornou repugnante. A curva que
    ocorre do trincho de grande parte do animal ou do animal inteiro, passando pelo
    avanço do patamar da repugnância à vista dos animais mortos, para a
    transferência do trincho a enclaves especializados por trás das cenas,
    constitui uma típica curva civilizadora.

    Resta a ser investigado até que ponto processos parecidos são subjacentes a
    fenômenos semelhantes em outras sociedades. Na antiga civilização chinesa, mais
    que em qualquer outra, o ocultamento do ato de trinchar por trás das cenas foi
    efetuado mais cedo e mais radicalmente do que no Ocidente. Na China, o processo
    é levado tão longe que se trincha e corta toda carne em um lugar inteiramente
    reservado e a faca é inteiramente banida do uso à mesa.

    [...]

    Fortes movimentos retroativos não são certamente impensáveis. É bem sabido que
    as condições de vida na Primeira Guerra Mundial automaticamente provocaram a
    suspensão de alguns dos tabus da civilização de tempos de paz. Nas trincheiras,
    oficiais e soldados, quando necessário, comiam usando facas e mãos. O patamar
    de delicadeza encolheu-se com grande rapidez sob a pressão de uma situação
    inescapável.

    À parte essas interrupções, sempre possíveis e que podem também levar a novas
    configurações de costumes, é bastante clara a linha do desenvolvimento no
    emprego da faca.60 A regulação e o controle das emoções intensificam-se. As
    instruções e proibições a respeito de um instrumento ameaçador tornam-se cada
    vez mais numerosas e diferenciadas. Finalmente, o emprego do símbolo ameaçador
    é tão limitado quanto possível.

    Não podemos evitar comparar a direção dessa curva de civilização com o costume
    há muito praticado na China. Neste país, como se sabe, a faca desapareceu há
    muitos séculos como utensílio de mesa. Para muitos chineses, é inteiramente
    incivil a maneira como os europeus comem. “Os europeus são bárbaros”, dizem
    eles, “eles comem com espadas.” Podemos supor que este costume está ligado ao
    fato de que desde há muito tempo a classe alta, que criava os modelos na China,
    não foi guerreira, mas uma classe pacífica em altíssimo grau, uma sociedade de
    funcionários públicos eruditos.

### Pedagogia da proibição

    Algumas formas de comportamento são proibidas não porque sejam anti-higiênicas,
    mas por que são feias à vista e geram associações desagradáveis. A vergonha de
    dar esse espetáculo, antes ausente, e o medo de provocar tais associações,
    difundem-se gradualmente dos círculos que estabelecem o padrão para outros mais
    amplos, através de numerosas autoridades e instituições. Não obstante, uma vez
    sejam despertados e firmemente estabelecidos na sociedade, esses sentimentos
    através de certos rituais, como o que envolve o garfo, são constantemente
    reproduzidos enquanto a estrutura das relações humanas não for fundamentalmente
    alterada. A geração mais antiga, para quem esse padrão de conduta é aceito como
    natural, insiste com as crianças, que não vêm ao mundo já munidas desses
    sentimentos e deste padrão, para que se controlem mais ou menos rigorosamente
    de acordo com os mesmos e contenham seus impulsos e inclinações. Se tenta tocar
    alguma coisa pegajosa, úmida ou gordurosa com os dedos, a criança é
    repreendida: “Você não deve fazer isso. Gente fina não faz isso.” E o desagrado
    com tal conduta, que é assim despertado pelo adulto, finalmente cresce com o
    hábito, sem ser induzido por outra pessoa.

    Em grande parte, contudo, a conduta e vida instintiva da criança são postas à
    força, mesmo sem palavras, no mesmo molde e na mesma direção pelo fato de que
    um dado uso da faca e do garfo, por exemplo, está inteiramente firmado no mundo
    adulto — isto é, pelo exemplo do meio. Uma vez que a pressão e coação exercidas
    por adultos individuais é aliada da pressão e exemplo de todo o mundo em volta,
    a maioria das crianças, quando crescem, esquece ou reprime relativamente cedo o
    fato de que seus sentimentos de vergonha e embaraço, de prazer e desagrado, são
    moldados e obrigados a se conformar a certo padrão de pressão e compulsão
    externas. Tudo isso lhes parece altamente pessoal, algo “interno”, implantado
    neles pela natureza. Embora seja ainda bem visível nos escritos de Courtin e La
    Salle que os adultos, também, foram inicialmente dissuadidos de comer com os
    dedos por consideração para com o próximo, por “polidez”, para poupar a outros
    um espetáculo desagradável, e a si mesmos a vergonha de serem vistos com as
    mãos sujas, mais tarde isto se torna cada vez mais um automatismo interior, a
    marca da sociedade no ser interno, o superego, que proíbe ao indivíduo comer de
    qualquer maneira que não com o garfo. O padrão social a que o indivíduo fora
    inicialmente obrigado a se conformar por restrição externa é finalmente
    reproduzido, mais suavemente ou menos, no seu íntimo através de um autocontrole
    que opera mesmo contra seus desejos conscientes.

    Desta forma, o processo sócio-histórico de séculos, no curso do qual o padrão
    do que é julgado vergonhoso e ofensivo é lentamente elevado, reencena-se em
    forma abreviada na vida do ser humano individual. Se quiséssemos expressar
    processos repetitivos desse tipo sob a forma de leis, poderíamos falar, como um
    paralelo às leis da biogênese, de uma lei fundamental de sociogênese e
    psicogênese.

    [...]

    Mas, ao mesmo tempo, é muito claro que esse tratado tem precisamente a função
    de cultivar sentimentos de vergonha. A referência à onipresença de anjos, usada
    para justificar o controle de impulsos aos quais a criança está acostumada, é
    bem característica. A maneira como a ansiedade é despertada nos jovens, a fim
    de forçá-los a reprimir o prazer, de acordo com o padrão de conduta social,
    muda com a passagem dos séculos. Aqui a ansiedade despertada em conexão com a
    renúncia à satisfação instintiva é explicada a si mesmo e aos demais em termos
    de espíritos externos. Algum tempo depois, a restrição autoimposta, juntamente
    com o medo, a vergonha e a recusa a cometer qualquer infração, frequentemente
    aparece, pelo menos na classe alta, na sociedade aristocrática de corte, como
    vergonha e medo a outras pessoas. Em círculos mais amplos, reconhecidamente, a
    referência a anjos da guarda é usada durante muito tempo como instrumento para
    condicionar crianças. Diminui um pouco quando “razões higiênicas” e de saúde
    recebem mais ênfase e se pretende obter um certo grau de controle dos impulsos
    e das emoções. Essas razões higiênicas passam, então, a desempenhar um papel
    importante nas ideias dos adultos sobre o que é civilizado, em geral sem que se
    perceba que relação elas têm com o condicionamento das crianças que está sendo
    praticado. Apenas a partir dessa percepção, contudo, é que o que há nelas de
    racional pode ser distinguido do que é apenas aparentemente racional, isto é,
    fundamentado principalmente na repugnância e nos sentimentos de vergonha dos
    adultos.

    [...]

    Note-se que em seu tratado [de Erasmo] não são frequentes os argumentos de
    natureza médica. Quando aparecem, é quase sempre, como no caso acima, para se
    opor à exigência de que se restrinjam funções naturais, ao passo que mais
    tarde, sobretudo no século XIX, eles servem quase sempre como instrumentos para
    compelir ao controle e à renúncia de uma satisfação instintiva. Só no século XX
    é que aparece uma ligeira relaxação.

Interessante como Elias mostra que normas presentes numa edição de um manual de
boas maneiras -- como por exemplo de La Salle, vide _Algumas Observações sobre
os Exemplos e sobre Estas Mudanças em Geral,_ foram omitidas ou simplificadas
numa edição posterior, provavelmente por já estarem internalizadas e naturalizadas
geracionalmente -- de adultos para crianças -- nos indivíduos e difundidas na
sociedade, tornando até indelicada a mera menção de comportamentos
desagradáveis mencionadas na edição anterior ou mesmo em manuais de outras
épocas, como por exemplo o ato de cagar na rua:

    Os exemplos extraídos da obra de La Salle devem ser suficientes para indicar
    como estava se desenvolvendo o sentimento de delicadeza. Mais uma vez, é muito
    instrutiva a diferença entre as edições de 1729 e 1774. Indubitavelmente, mesmo
    a edição anterior já menciona um padrão de delicadeza muito diferente do que é
    encontrado no tratado de Erasmo. A exigência de que todas as funções naturais
    sejam vedadas à vista de outras pessoas é feita de maneira inequívoca, mesmo
    que o modo pelo qual é formulada indique que o comportamento das pessoas —
    adultas e crianças — não se conforma ainda à mesma. Embora diga que não é muito
    delicado até mesmo falar de tais funções ou das partes do corpo nelas
    envolvidas, La Salle, ainda assim as descreve com uma minúcia de detalhes que
    nos espanta. Dá às coisas seus verdadeiros nomes, já que não constam da
    Civilité, de Courtin, datada de 1672, que se destinava ao uso das classes
    altas.

    Na segunda edição do livro de La Salle, igualmente, são omitidas todas as
    referências detalhadas. Cada vez mais, essas necessidades são “ignoradas”.
    Simplesmente lembrá-las torna-se embaraçoso para a pessoa na presença de outras
    que não sejam muito íntimas e, em sociedade, tudo o que mesmo remota ao
    associativamente as lembre é evitado.

    Ao mesmo tempo, os exemplos deixam claro a lentidão com que se desenvolvia o
    processo de suprimir essas funções da vida social. Material suficiente66
    sobreviveu exatamente porque o silêncio sobre esses assuntos não era observado
    antes ou o era menos. O que em geral falta é a ideia de que informação desse
    tipo tenha mais do que valor de curiosidade e, por isso mesmo, ela raramente é
    sintetizada em uma ideia da linha geral do desenvolvimento. Não obstante, se
    adotamos um ponto de vista abrangente, emerge um padrão que é típico do
    processo civilizatório.  4. No início, essas funções e sua exposição são
    acompanhadas apenas de leves sentimentos de vergonha e repugnância e, por isso
    mesmo, sujeitas apenas a modesto isolamento e controle. São aceitas como tão
    naturais como pentear os cabelos ou calçar os sapatos. As crianças eram
    portanto condicionadas de maneira análoga para uma coisa, ou outra.

    [...]

    Durante muito tempo, a rua, e quase todos os locais onde a pessoa por acaso se
    encontrasse, serviam para a mesma finalidade que o muro do pátio mencionado
    acima. Não é nem mesmo raro recorrer à escada, aos cantos da sala, ou aos
    beirais das muralhas de um castelo, se a pessoa sente tais necessidades. Os
    Exemplos E e F deixam isto claro. Mas mostram também que, dada a
    interdependência específica e permanente das muitas pessoas que viviam na
    corte, uma pressão era exercida de cima no sentido de um controle mais rigoroso
    dos impulsos e, por conseguinte, para maior autodomínio.

    O controle mais rigoroso de impulsos e emoções é inicialmente imposto por
    elementos de alta categoria social aos seus inferiores ou, no máximo, aos seus
    socialmente iguais. Só relativamente mais tarde, quando a classe burguesa,
    compreendendo um maior número de pares sociais, torna-se a classe superior,
    governante, é que a família vem a ser a única — ou, para ser mais exata, a
    principal e dominante — instituição com a função de instilar controle de
    impulsos. Só então a dependência social da criança face aos pais torna-se
    particularmente importante como alavanca para a regulação e moldagem
    socialmente requeridas dos impulsos e das emoções.

### Segunda natureza

    No estágio das cortes feudais, e ainda mais nas dos monarcas absolutos, elas
    próprias desempenhavam em grande parte essa função para a classe alta. No
    estágio posterior, boa parte do que se tornou “segunda natureza” para nós não
    havia sido ainda inculcado dessa forma, como um autocontrole automático, um
    hábito que, dentro de certos limites, funciona também quando a pessoa está
    sozinha. Ao contrário, o controle dos instintos era inicialmente imposto apenas
    quando na companhia de outras pessoas, isto é, mais conscientemente por razões
    sociais. Tanto o tipo como o grau de controle correspondem à posição social da
    pessoa que os impõe, em relação à posição daqueles em cuja companhia está. Isto
    muda lentamente, à medida que as pessoas se aproximam mais socialmente e se
    torna menos rígido o caráter hierárquico da sociedade. Aumentando a
    interdependência com a elevação da divisão do trabalho, todos se tornam cada
    vez mais dependentes dos demais, os de alta categoria social dos socialmente
    inferiores e mais fracos. Estes últimos tornam-se a tal ponto iguais aos
    primeiros que eles, os socialmente superiores, sentem vergonha até mesmo de
    seus inferiores. Só nesse momento é que a armadura dos controles é vestida em
    um grau aceito como natural nas sociedades democráticas industrializadas.

    [...]

    Há pessoas diante das quais nos sentimos envergonhados e outras com quem isso
    não acontece. O sentimento de vergonha é evidentemente uma função social
    modelada segundo a estrutura social.

### Polidez: condicionamento, isolamento e segredo; distância entre adultos e crianças

    Nessa sociedade hierarquicamente estruturada, todos os atos praticados na
    presença de numerosas pessoas adquiriam valor de prestígio. Por este motivo, o
    controle das emoções, aquilo que chamamos “polidez”, revestia uma forma
    diferente da que adotou mais tarde, época em que diferenças externas em
    categoria haviam sido parcialmente niveladas. O que se menciona aqui é um caso
    especial de intercâmbio entre iguais (que uma pessoa não deve servir outra), e
    que mais tarde se torna a prática geral. Em sociedade, todos se servem
    pessoalmente e todos começam a comer no mesmo momento.

    A situação é análoga no tocante à exposição do corpo. Inicialmente, torna-se
    uma infração repugnante mostrar-se de qualquer maneira diante de pessoas de
    categoria mais alta ou igual. Mas, no caso de inferiores, a seminudez ou mesmo
    a nudez pode até ser sinal de benevolência. Porém, depois, quando todos se
    tornam socialmente mais iguais, a prática lentamente se torna malvista em
    qualquer caso. A referência social à vergonha e ao embaraço desaparece cada vez
    mais da consciência. Exatamente porque a injunção social de não se mostrar ou
    desincumbir-se de funções naturais opera nesse momento no tocante a todos e é
    gravada nesta forma na criança, ela parece ao adulto uma injunção de seu
    próprio ser interno e assume a forma de um autocontrole mais ou menos total e
    automático.

    5. Este isolamento das funções naturais da vida pública, e a correspondente
    regulação ou moldagens das necessidades instintivas, porém, só se tornaram
    possíveis porque, juntamente com a sensibilidade crescente, surgiu um
    aparelhamento técnico que solucionou de maneira muito satisfatória o problema
    de eliminação dessas funções na vida social e seu deslocamento para locais mais
    discretos. A situação não foi diferente no tocante às maneiras à mesa. O
    processo de mudança social e o avanço das fronteiras da vergonha e do patamar
    de repugnância não podem ser explicados por qualquer condição isolada e,
    decerto, não pelo desenvolvimento da tecnologia ou pelas descobertas
    científicas. Muito ao contrário, não seria difícil demonstrar as bases
    sociogenéticas e psicogenéticas dessas invenções e descobertas

    Uma vez posta em movimento a reformulação das necessidades humanas, devido à
    transformação generalizada das relações entre os homens, o desenvolvimento de
    aparelhagem técnica correspondente ao padrão mudado consolidou os novos hábitos
    em um grau extraordinário. Este aparelho contribuiu para a reprodução constante
    do padrão e para sua disseminação.

    Não deixa de ser interessante observar que hoje [década de 1930], época em que
    este padrão de conduta se consolidou tanto que é aceito como inteiramente
    natural, certa relaxação está começando, sobretudo em comparação com o século
    XIX, pelo menos no que diz respeito a referências verbais a funções corporais.
    [...] Mas isto, tal como os costumes modernos de banho e dança, é possível
    apenas porque o nível habitual de autocontrole, técnica e institucionalmente
    consolidado, a capacidade do indivíduo de restringir suas necessidades e
    comportamento de acordo com os sentimentos mais atuais sobre o que é
    desgostoso, foram atingidos. O que se observa é uma relaxação dentro do
    contexto de um padrão já firmemente radicado.

    6. O padrão que está emergindo em nossa fase de civilização caracteriza-se por
    uma profunda discrepância entre o comportamento dos chamados “adultos” e das
    crianças. Estas têm no espaço de alguns anos que atingir o nível avançado de
    vergonha e nojo que demorou séculos para se desenvolver. A vida instintiva
    delas tem que ser rapidamente submetida ao controle rigoroso e modelagem
    específica que dão à nossa sociedade seu caráter e que se formou na lentidão
    dos séculos. Nisto, os pais são apenas os instrumentos, amiúde inadequados, os
    agentes primários do condicionamento. Através deles e de milhares de outros
    instrumentos, é sempre a sociedade como um todo, o conjunto de seres humanos,
    que exerce pressão sobre a nova geração, levando-a mais perfeitamente, ou
    menos, para seus fins.

    [...]

    O grau de comedimento e controle esperado pelos adultos entre si não era maior
    do que o imposto às crianças. Era pequena, medida pelos padrões de hoje, a
    distância que separava adultos de crianças.

    [...]

    De modo geral, sob as pressões do condicionamento, impulsos desse tipo
    desapareceram da consciência do adulto no estado de vigília. Só a psicanálise é
    que os descobre sob a forma de desejos insatisfeitos ou irrealizáveis, que são
    descritos como o nível inconsciente ou onírico da mente. E estes desejos têm,
    de fato, em nossa sociedade, o caráter de um resíduo “infantil” porque o padrão
    social dos adultos torna necessária a completa supressão e transformação dessas
    tendências, de modo que elas parecem, quando ocorrem em adultos, um “resto” da
    infância.

    [...]

    A sociedade está, aos poucos, começando a suprimir o componente de prazer
    positivo de certas funções mediante o engendramento da ansiedade ou, mais
    exatamente, está tornando esse prazer “privado” e “secreto” (isto é,
    reprimindo-o no indivíduo), enquanto fomenta emoções negativamente carregadas —
    desagrado, repugnância, nojo — como os únicos sentimentos aceitáveis em
    sociedade. Mas exatamente por causa desse aumento da proibição social de muitos
    impulsos, pela sua “repressão” na superfície da vida social e na consciência do
    indivíduo, necessariamente aumenta a distância entre a estrutura da
    personalidade e o comportamento de adultos e crianças.

    [...]

    Está avançando a “conspiração do silêncio”. Ela se baseia na pressuposição —
    que evidentemente não podia ser feita à época da edição anterior — de que todos
    os detalhes são conhecidos dos adultos e podem ser controlados no seio da
    família.

### Interesse instintivo remanescente

    O uso do lenço tornou-se generalizado, pelo menos entre as pessoas que alegam
    saber “como se comportar”. Mas o uso das mãos não desapareceu inteiramente.
    [...] Quase parece que inclinações que ficaram sujeitas a certo controle e
    comedimento com a adoção do lenço estariam, dessa maneira, procurando uma nova
    maneira de se manifestar. De qualquer modo, uma tendência instintiva que
    aparece hoje, no máximo, no inconsciente, nos sonhos, na esfera privada, ou
    mais conscientemente apenas em locais fechados, ou seja, o interesse pelas
    secreções corporais, mostra-se aqui em um estágio mais antigo do processo
    histórico, com mais clareza e franqueza, numa forma que hoje é “normalmente”
    visível apenas em crianças.

### Culpa e neuroses

    Como em outros hábitos infantis, o aviso médico aparece agora em conjunto ou em
    lugar do social como instrumento de condicionamento, com referência ao mal que
    pode decorrer de fazer “tal coisa” com frequência excessiva. Vemos aí um
    exemplo de mudança na maneira de condicionar alguém, que já havia sido
    considerada de outros pontos de vista. Até essa ocasião, os hábitos eram quase
    sempre julgados claramente em sua relação com outras pessoas e se eram
    proibidos, pelo menos na classe alta secular, era porque podiam ser incômodos
    ou embaraçosos para terceiros ou porque revelasse “falta de respeito”. Mas
    agora, os hábitos são condenados cada vez mais como tais, em si, e não pelo que
    possam acarretar a outras pessoas. Desta maneira, impulsos ou inclinações
    socialmente indesejáveis são reprimidos com mais rigor. São associados ao
    embaraço, ao medo, à vergonha ou à culpa, mesmo quando o indivíduo está
    sozinho. Grande parte do que chamamos de razões de “moralidade” ou “moral”
    preenche as mesmas funções que as razões de “higiene” ou “higiênicas”:
    condicionar as crianças a aceitar determinado padrão social. A modelagem por
    esses meios objetiva a tornar automático o comportamento socialmente desejável,
    uma questão de autocontrole, fazendo com que o mesmo pareça à mente do
    indivíduo resultar de seu livre arbítrio e ser de interesse de sua própria
    saúde ou dignidade humana. Só com o aparecimento dessa maneira de consolidar
    hábitos ou, em outras palavras, de condicionamento, que ganha predominância com
    a ascensão da classe média, é que o conflito entre impulsos e tendências
    socialmente inadmissíveis, por um lado, e o padrão de exigências sociais feitas
    ao indivíduo, por outro, assume a forma rigorosamente definida e fundamental às
    teorias psicológicas dos tempos modernos — acima de tudo, à psicanálise. É bem
    possível que sempre tenha havido “neuroses”. Mas as “neuroses” que vemos hoje
    por toda parte são uma forma histórica específica de conflito que precisa de
    uma elucidação psicogenética e sociogenética.

### Estágios

    No estágio da aristocracia de corte, as restrições impostas às inclinações e
    emoções baseavam-se principalmente em consideração e respeito devidos a outras
    pessoas e, acima de tudo, aos superiores sociais. No estágio subsequente, a
    renúncia e o controle de impulsos são muito menos determinados por pessoas
    particulares. Expressadas provisória e aproximativamente, nesse instante, mais
    diretamente do que antes, são as compulsões menos visíveis e mais impessoais da
    interdependência social, a divisão do trabalho, o mercado, a competição, que
    impõem restrições e controle aos impulsos e emoções. São essas pressões, e os
    correspondentes tipos de explicação e condicionamento acima mencionados, que
    fazem parecer que o comportamento socialmente desejável seja gerado
    voluntariamente pelo próprio indivíduo, por sua própria iniciativa. Isto se
    aplica à regulação e às restrições de impulsos necessárias ao “trabalho”, e
    também a todo padrão de acordo com o qual eles são modelados nas sociedades
    industrializadas burguesas.

    [...]

    Em ambos os casos, na sociedade aristocrática da corte e nas sociedades
    burguesas dos séculos XIX e XX, as classes superiores são socialmente
    controladas em escala particularmente alta. O papel fundamental desempenhado
    por essa crescente dependência das classes superiores, como mola propulsora da
    civilização, será demonstrado adiante.

    [...]

    Tabus e restrições de vários tipos acompanham a expectoração de catarro, como
    de outras funções corporais, em muitas sociedades, tanto “primitivas” como
    “civilizadas”. O que as distingue é o fato de que, nas primeiras, eles são
    sempre mantidos por medo de outras pessoas, ou seres, mesmo que imaginários —
    isto é, por controles externos — ao passo que, nas últimas, são transformados
    mais ou menos completamente em controles internos. As tendências proibidas (por
    exemplo, a tendência para escarrar) desaparecem em parte da consciência, sob
    pressão desse controle interno, ou, como poderia ser também chamado, do
    superego e do “hábito de previsão”. O que sobra na consciência como motivação
    da ansiedade é alguma consideração de longo prazo. Assim, em nossa época o medo
    de escarrar, e os sentimentos de vergonha e repugnância nos quais isto se
    expressa, concentram-se na ideia mais precisamente definida e logicamente
    compreensível de certas doenças e suas “causas”, e não em torno da imagem de
    influências mágicas, deuses, espíritos, ou demônios. Mas a série de exemplos
    mostra também com grande clareza que a compreensão racional das origens de
    certas doenças, do perigo do esputo como transmissor, não é a causa primária do
    medo e da repugnância nem a mola propulsora da civilização, a força por trás
    das mudanças no comportamento no tocante ao hábito de escarrar.

    [...]

    Vale a pena deixar esclarecido, de uma vez por todas, que algo que sabemos ser
    prejudicial à saúde de maneira alguma desperta necessariamente sentimentos de
    desagrado ou vergonha. E, reciprocamente, algo que desperta esses sentimentos
    não tem que ser prejudicial à saúde. Alguém que coma hoje barulhentamente ou
    com as mãos provoca profundo desagrado, mas sem que haja o menor receio pela
    saúde. E nem a ideia de ler com má iluminação nem a de gás venenoso, por
    exemplo, despertam sentimentos de desagrado ou vergonha remotamente
    semelhantes, embora sejam óbvias suas más consequências para a saúde. Desta
    maneira, o nojo da expectoração, e os tabus que a cercam, aumentam muito antes
    que as pessoas tenham uma ideia clara da transmissão de certas doenças pelo
    escarro. O que inicialmente prova e agrava os sentimentos de nojo e as
    restrições é a transformação das relações e dependência humanas.

    [...]

    A motivação fundada na consideração social surge muito antes da motivação por
    conhecimento científico. O rei, como “sinal de respeito”, exige esse
    comportamento de seus cortesãos. Nos círculos da corte, este sinal da
    dependência em que ela vive, a crescente compulsão para controlar-se e
    moderar-se torna-se uma “marca de distinção” a mais, que é imediatamente
    imitada abaixo e difundida com a ascensão de classes mais numerosas. E aqui,
    como nas precedentes curvas de civilização, a admoestação “Isso não se faz”,
    com a qual a moderação, o medo e a repugnância são inculcados, só muito depois
    é ligada, como resultado de certa “democratização”, a uma teoria científica,
    com um argumento que se aplica por igual a todos os homens, qualquer que seja
    sua posição ou status. O impulso primário a essa lenta repressão de uma
    inclinação que antes era forte e geral não vem da compreensão racional das
    causas das doenças, mas — conforme será discutido em detalhe mais adiante — de
    mudanças na maneira como as pessoas vivem juntas na estrutura da sociedade.

    4. A modificação de hábito de escarrar e, finalmente, a eliminação mais ou
    menos completa de sua necessidade constitui um bom exemplo da maleabilidade da
    vida psíquica. Pode ser que essa necessidade tenha sido compensada por outras
    (como, por exemplo, a necessidade de fumar) ou debilitada por certas mudanças
    na dieta. Mas é certo que o grau de supressão que se tornou possível neste caso
    não aconteceu no tocante a muitas outras inclinações. A tendência a escarrar,
    como a de examinar o esputo, mencionada nesses exemplos, é substituível. Ela se
    manifesta agora apenas em crianças e em análise de sonhos, e sua eliminação é
    vista no riso específico que nos sacode quando “essas coisas” são mencionadas
    abertamente.

    Outras necessidades não são tão substituíveis ou maleáveis na mesma extensão. E
    isto coloca a questão do limite da transformabilidade da personalidade humana.
    Sem dúvida ela é submissa a certas fidelidades que podem ser chamadas
    “naturais”. O processo histórico modifica-a dentro desses limites. O grau em
    que a vida e o comportamento humanos podem ser moldados por processos
    históricos não foi ainda analisado em suficiente detalhe. De qualquer modo,
    tudo isso mostra, mais uma vez, como processos naturais e históricos se
    influenciam mútua e inseparavelmente. A formação de sentimentos de vergonha e
    asco e os avanços no patamar da delicadeza são simultaneamente processos
    naturais e históricos. Essas formas de emoções são manifestações da natureza
    humana em condições sociais específicas e reagem, por sua vez, sobre o processo
    sócio-histórico como um de seus elementos.

    É difícil concluir se a oposição radical entre “civilização” e “natureza” é
    mais do que uma expressão das tensões da própria psique “civilizada”, de um
    desequilíbrio específico na vida psíquica gerado no estágio recente da
    civilização ocidental. De qualquer modo, a vida psíquica de povos “primitivos”
    não é menos historicamente (isto é, socialmente) marcada do que a dos povos
    “civilizados”, mesmo que os primeiros mal estejam conscientes de sua própria
    história. Não há um ponto zero na historicidade do desenvolvimento humano, da
    mesma forma que não o há na socialidade, na interdependência social dos homens.
    Nos povos “primitivos” e “civilizados”, observam-se as mesmas proibições e
    restrições socialmente induzidas juntamente com suas equivalentes psíquicas,
    socialmente induzidas: ansiedades, prazer a aversão, desagrado e deleite. No
    mínimo, por conseguinte, não é muito claro o que se tem em vista quando o
    chamado padrão primitivo é oposto, como “natural”, ao “civilizado”, como social
    e histórico. No que interessa às funções psíquicas do homem, processos naturais
    e históricos trabalham indissoluvelmente juntos.

### Fundo da vida social

    Tal como a maior parte das demais funções corporais, o sono foi sendo
    transferido para o fundo da vida social. [...] Suas paredes visíveis e
    invisíveis vedam os aspectos mais “privados”, “íntimos”, irrepreensivelmente
    “animais” da existência humana, à vista de outras pessoas.

    [...]

    Uma camisola especial começou a ser adotada lentamente, mais ou menos na
    ocasião em que acontecia o mesmo com o garfo e o lenço. Tal como outros
    “implementos de civilização”, espalhou-se de forma bem gradual pela Europa. E,
    como eles, era símbolo de uma mudança decisiva que ocorria nessa época nos
    seres humanos. Aumentava a sensibilidade com tudo aquilo que entrava em contato
    com o corpo. A vergonha passou a acompanhar formas de comportamento que antes
    haviam estado livres desse sentimento.

    [...]

    Neste particular, também, houve certa reação e relaxação desde a guerra. Isto
    esteve claramente ligado à crescente mobilidade da sociedade, à difusão dos
    esportes, a excursões e viagens, e também à separação relativamente cedo dos
    jovens da comunidade familiar. A transição da camisola para o pijama — isto é,
    para um trajo de dormir mais “socialmente apresentável” — constitui um sintoma
    desta situação. A mudança não é, como se pensa algumas vezes, apenas um
    movimento de retrogressão, uma diminuição dos sentimentos de vergonha ou
    delicadeza ou, quem sabe, uma liberação e descontrole de ânsias instintivas,
    mas o desenvolvimento de uma forma que se ajusta a nosso padrão avançado de
    delicadeza e da situação específica em que a atual vida social coloca o
    indivíduo.

    [...]

    As roupas de dormir da fase precedente despertavam sentimentos de vergonha e
    embaraço exatamente porque eram relativamente informes. Não havia intenção de
    que fossem vistas por pessoa fora do círculo familiar.

### Ansiedades pelo condicionamento

    “Se for forçado por necessidade inevitável a dividir a cama com outra pessoa…
    em uma viagem, não é correto ficar tão perto dele que o perturbe ou mesmo o
    toque”, escreve La Salle (Exemplo D) e: “Você não deve nem se despir nem ir
    para a cama na presença de qualquer outra pessoa.”

    Na edição de 1774, os detalhes são mais uma vez evitados em todos os casos
    possíveis. E o tom é visivelmente mais severo: “Se for forçado a dividir a cama
    com uma pessoa do mesmo sexo, o que raramente acontece, deve manter um rigoroso
    e vigilante recato” (Exemplo E). Este é o tom da injunção moral. Até mesmo dar
    uma razão tornou-se penoso para o adulto. Pela ameaça do tom, a criança é
    levada a associar essa situação a perigo. Quanto mais o padrão “natural” de
    delicadeza e vergonha parece aos adultos e quanto mais o controle civilizado de
    ânsias instintivas é aceito como natural, mais incompreensível se torna para os
    adultos que as crianças não sintam “por natureza” esta delicadeza e vergonha.
    Necessariamente as crianças tocam repetidamente o patamar adulto de embaraço e
    — uma vez que não estão ainda adaptadas — transgridem os tabus da sociedade,
    cruzam o patamar adulto de vergonha, e penetram em zonas de perigo emocionais
    que o próprio adulto só com dificuldade consegue controlar. Nesta situação, o
    adulto não explica as exigências que faz em matéria de comportamento. Não tem
    como fazê-lo adequadamente. Está tão condicionado que se conforma de maneira
    mais ou menos automática a um padrão social. Qualquer outro comportamento,
    qualquer desobediência às proibições ou restrições que prevalecem em sua
    sociedade, implica perigo e uma desvalorização das restrições que ele mesmo se
    impõe. A conotação peculiarmente emocional tão amiúde ligada a exigências
    morais, à severidade agressiva e ameaçadora com que são frequentemente
    defendidas, refletem a ameaça que qualquer desafio às proibições representa
    para o equilíbrio instável de todos aqueles a cujos olhos o padrão de
    comportamento da sociedade se tornou mais ou menos uma “segunda natureza”.
    Essas atitudes são sintomas da ansiedade despertada nos adultos em todos os
    casos em que a estrutura de sua própria vida instintiva, e com ela sua própria
    existência social e ordem social onde se radica, é, mesmo remotamente,
    ameaçada.

    [...]

    a parede entre pessoas, a reserva, a barreira emocional erigida pelo
    condicionamento entre um corpo e outro cresceram sem cessar. Desde cedo as
    crianças são treinadas nesse isolamento dos demais, com todos os hábitos e
    experiências que isto traz.

    [...]

    Com uma certa impotência, o observador do século XIX e, até certo ponto, do
    século XX, vê-se diante de modelos e regras de condicionamento do passado. E
    até que compreendamos que nosso próprio patamar de repugnância, nossa própria
    estrutura de sentimentos, evoluíram — em um processo estruturado — e seguem
    evoluindo, continua realmente quase incompreensível, do atual ponto de vista,
    como diálogos como esses pudessem ser incluídos em um livro escolar ou
    deliberadamente produzidos como material de leitura para crianças. Mas esta é
    exatamente a razão por que nosso próprio padrão, incluindo nossa atitude em
    relação às crianças, deve ser compreendido como algo que evoluiu.

### Constituição psíquisa: "a conspiração do silêncio"

    Vemos, mais uma vez, como é importante para a compreensão de uma constituição
    psíquica mais antiga, e de nossa própria, observar o aumento dessa distância, a
    formação gradual de uma área segregada especial na qual as pessoas vêm, aos
    poucos, a passar os primeiros doze, quinze e agora quase vinte anos de sua
    vida. O desenvolvimento biológico humano em tempos mais antigos não tomou um
    curso diferente do de hoje. Só com relação a essa mudança social podemos
    compreender melhor todos os problemas de “crescer” como se apresentam hoje e,
    com eles, os “resíduos infantis” na estrutura de personalidade de pessoas
    crescidas (adultos). A diferença mais pronunciada entre as roupas de crianças e
    adultos em nosso tempo é apenas uma expressão particularmente visível desse
    fato. E, também essa diferença era mínima no tempo de Erasmo e durante um longo
    período depois.

    [...]

    Entre a maneira de falar sobre relações sexuais representada por Erasmo e a
    representada aqui por Von Raumer, é visível uma curva de civilização semelhante
    à mostrada em mais detalhe na manifestação de outros impulsos. No processo
    civilizador, a sexualidade, também, é cada vez mais transferida para trás da
    cena da vida social e isolada em um enclave particular, a família nuclear. De
    maneira idêntica, as relações entre os sexos são segregadas, colocadas atrás de
    paredes da consciência.

    [...]

    Uma vez que no curso do processo civilizador o impulso sexual, como tantos
    outros, está sujeito a controle e transformação cada vez mais rigorosos, muda o
    problema que ele coloca. A pressão aplicada sobre adultos para privatizar todos
    os seus impulsos (em especial, os sexuais), a “conspiração de silêncio”, as
    restrições socialmente geradas à fala, o caráter emocionalmente carregado da
    maioria das palavras relativas a ardores sexuais, tudo isto constrói uma grossa
    parede de sigilo em volta do adolescente. O que torna o esclarecimento sexual
    tão difícil — a derrubada desse muro, que um dia será necessário — não é só a
    necessidade de fazer o adolescente conformar-se ao mesmo padrão de controle de
    instintos e de domínio como o adulto. É, acima de tudo, a estrutura de
    personalidade dos próprios adultos que torna difícil falar sobre essas coisas
    secretas.

    [...]

    À vista de tudo isso, torna-se claro como deve ser colocada a questão da
    infância. Os problemas psicológicos de indivíduos que crescem não podem ser
    compreendidos se forem considerados como se desenvolvendo uniformemente em
    todas as épocas históricas. Os problemas relativos à consciência e impulsos
    instintivos da criança variam com a natureza das relações entre ela e os
    adultos. Essas relações têm em todas as sociedades uma forma específica
    correspondente às peculiaridades de sua estrutura. [...] Por isso
    mesmo, os problemas decorrentes da adaptação e modelação de adolescentes ao
    padrão dos adultos — por exemplo, os problemas específicos da puberdade em
    nossa sociedade civilizada — só podem ser compreendidos em relação à fase
    histórica, à estrutura da sociedade como um todo, que exige e mantém esse
    padrão de comportamento adulto e esta forma especial de relacionamento entre
    adultos e crianças.

### Relação civilizacional tendencial entre ganhos de liberdade exterior e aumento de coerção interior

    Há evidência de sobra de que, nessa aristocracia de corte, a restrição a
    relações sexuais ao casamento era frequentemente considerada como burguesa e
    socialmente descabida. Não obstante, tudo isto dá uma ideia de como um tipo
    específico de liberdade corresponde diretamente a formas e estágios
    particulares de interdependência social entre seres humanos.

    As formas linguísticas não dinâmicas, às quais continuamos presos, opõem
    liberdade a coerção como se fossem céu e inferno. A curto prazo, esse
    raciocínio em opostos absolutos muitas vezes se mostra razoavelmente adequado.
    Para quem está na prisão, o mundo do outro lado das grades é um mundo de
    liberdade. Mas, examinando o assunto com mais cuidado, não há, ao contrário do
    que sugerem antíteses como essas, uma suposta liberdade “absoluta”, se por ela
    entendemos total independência e ausência de qualquer coação social. O que há é
    libertação, de uma forma de restrição opressiva ou intolerável para outra,
    menos pesada. Dessa maneira, o processo civilizador, a despeito da
    transformação e aumento das limitações que impõe às emoções, é acompanhado
    permanentemente por tipos de libertação dos mais diversos. A forma de casamento
    nas cortes absolutistas, simbolizada pela igual disposição de salas de estar e
    quartos de dormir para homens e mulheres nas mansões da aristocracia de corte,
    constitui um dos muitos exemplos desta situação. A mulher era mais livre de
    restrições externas do que na sociedade feudal. Mas a coação interior que ela
    era obrigada a impor a si mesma de acordo com a forma de integração e com o
    código de comportamento em vigor na sociedade de corte, que se originavam ambos
    dos mesmos aspectos estruturais dessa sociedade que engendraram sua
    “liberação”, havia aumentado para ela e para os homens em confronto com a
    sociedade cavaleirosa.

    [...]

    Burgueses e burguesas libertaram-se das limitações externas a que estiveram
    sujeitos como pessoas de segunda classe, na hierarquia dos estamentos. Aumentou
    o entrelaçamento de comércio e dinheiro, cujo crescimento lhes deu o poder
    social necessário para se libertarem. Mas, neste aspecto, as limitações sociais
    do indivíduo também são mais fortes do que antes. [...] O motivo por que o
    trabalho como ocupação, que com a ascensão da burguesia se tornou estilo geral
    de vida, deveria exigir uma disciplina particularmente rigorosa da sexualidade
    é uma questão independente; as ligações entre a estrutura da personalidade e a
    social no século XIX não cabem aqui. Não obstante, para os padrões da sociedade
    burguesa, o controle da sexualidade e a forma de casamento vigentes na
    sociedade de corte eram extremamente débeis. [...] Mas ambas as quebras de
    padrão têm, nessa época, de ser inteiramente excluídas da vida social oficial.
    Ao contrário do que acontece na corte, devem ser rigorosamente confinadas atrás
    da cena, banidas para o reino do segredo. Este é apenas um dos muitos exemplos
    do aumento da reserva e do autocontrole que o indivíduo então se impõe.

    10. O processo civilizador não segue uma linha reta. A tendência geral da
    mudança pode ser identificada, como aqui fizemos. Em escala menor, observamos
    os mais diversos movimentos que se entrecruzam, mudanças e surtos nesta ou
    naquela direção. Mas se estudamos o movimento da perspectiva de grandes
    períodos de tempo, vemos claramente que diminuem as compulsões originadas
    diretamente na ameaça do uso das armas e da força física, e que as formas de
    dependência que levam à regulação dos efeitos, sob a forma de autocontrole,
    gradualmente aumentam. Esta mudança desponta em seu aspecto mais retilíneo se
    observamos os homens da classe alta do tempo — isto é, a classe composta
    inicialmente de guerreiros ou cavaleiros, em seguida de cortesãos, e finalmente
    de profissionais burgueses. Se analisamos o tecido de muitas camadas do
    desenvolvimento histórico, contudo, verificamos que o movimento é infinitamente
    mais complexo. Em todas as fases ocorrem numerosas flutuações, frequentes
    avanços ou recuos dos controles internos e externos. O estudo dessas
    flutuações, particularmente das mais próximas de nós no tempo, pode facilmente
    obscurecer a tendência geral. Uma delas está presente ainda hoje na memória de
    todos: no período que se seguiu à Primeira Guerra Mundial, em comparação com o
    período anterior à guerra, parece ter ocorrido uma “relaxação da moral”. Certo
    número de limitações impostas ao comportamento antes da guerra debilitou-se ou
    desapareceu inteiramente. Muitas coisas antes proibidas passaram a ser
    permitidas. Visto bem de perto, o movimento parece estar ocorrendo na direção
    oposta à demonstrada aqui, a levar a uma relação dos controles impostos ao
    indivíduo pela vida social. Apurando-se o exame, porém, não é difícil notar que
    isto é apenas uma recessão muito ligeira, uma das flutuações que constantemente
    ocorrem na complexidade do movimento histórico, em cada fase do processo total.

    Um dos exemplos no particular é o das roupas de banho. No século XIX, cairia no
    ostracismo social a mulher que usasse em público os costumes de banho ora
    comuns. Mas essa mudança, e com ela toda a difusão do esporte entre ambos os
    sexos, pressupõe um padrão muito elevado de controle de impulsos. Só numa
    sociedade na qual um alto grau de controle é esperado como normal, e na qual as
    mulheres estão, da mesma forma que os homens, absolutamente seguras de que cada
    indivíduo é limitado pelo autocontrole e por um rigoroso código de etiqueta,
    podiam surgir trajos de banho e esporte com esse relativo grau de liberdade. É
    uma relaxação que ocorre dentro do contexto de um padrão “civilizado”
    particular de comportamento, envolvendo um alto grau de limitação automática e
    de transformação de emoções, condicionados para se tornarem hábitos.

    [...]

    O mecanismo de condicionamento, contudo, pouco difere do usado em épocas
    anteriores. Isto porque não implica uma supervisão mais rigorosa da tarefa, ou
    planejamento mais exato que leve em conta as circunstâncias especiais da
    criança, mas é efetuado, principalmente e por meios automáticos e, até certo
    ponto, por reflexos.

    [...]

    12. A tendência do processo civilizador a tornar mais íntimas todas as funções
    corporais, a encerrá-las em enclaves particulares, a colocá-las “atrás de
    portas fechadas”, produz diversas consequências. Uma das mais importantes, já
    observada em conexão com várias outras formas de impulsos, notamos com especial
    clareza no desenvolvimento de limitações civilizadoras à sexualidade. É a
    peculiar divisão dentro do homem, que se acentua na mesma medida em que os
    aspectos da vida humana que podem ser exibidos na vida social são separados dos
    que não podem, e que devem permanecer “privados” ou “secretos”. [...]
    Em outras palavras, com o avanço da civilização a vida dos seres humanos fica
    cada vez mais dividida entre uma esfera íntima e uma pública, entre
    comportamento secreto e público. E esta divisão é aceita como tão natural,
    torna-se um hábito tão compulsivo, que mal é percebida pela consciência.

    [...]

    Impulsos que prometem e tabus e proibições que negam prazeres, sentimentos
    socialmente gerados de vergonha e repugnância, entram em luta no interior do
    indivíduo. Este, conforme já apontamos, é o estado de coisas que Freud tenta
    descrever através de conceitos como “superego” e “inconsciente” ou, como se diz
    não sem razões na fala diária, como “subconsciente”. Mas, como quer que seja
    expresso, o código social de conduta grava-se de tal forma no ser humano, desta
    ou daquela forma, que se torna elemento constituinte do indivíduo. E este
    elemento, o superego, tal como a estrutura da personalidade do indivíduo como
    um todo, necessária e constantemente muda com o código social de comportamento
    e a estrutura da sociedade. A acentuada divisão do “ego”, ou consciência,
    característica do homem em nossa fase de civilização, que encontra expressão em
    termos como “superego” e “inconsciente”, corresponde à cisão específica no
    comportamento que a sociedade civilizada exige de seus membros. É igual ao grau
    de regulamentação e restrição imposto à expressão de necessidades profundas e
    impulsos. Tendências nessa direção podem se desenvolver sob qualquer forma na
    sociedade humana, mesmo naquelas que chamamos de “primitivas”. Mas a força
    adquirida em sociedades como a nossa por essa diferenciação, e a forma como ela
    aparece, são reflexo de um desenvolvimento histórico particular, são resultado
    de um processo civilizador.

    É isso o que temos em mente quando nos referimos aqui à constante
    correspondência entre a estrutura social e a estrutura da personalidade, do ser
    individual.

### A "forma socialmente impressa" da agressividade

    Como todos os demais instintos, ela é condicionada, mesmo em ações visivelmente
    militares, pelo estado adiantado da divisão de funções, e pelo decorrente
    aumento na dependência dos indivíduos entre si e face ao aparato técnico. É
    confinada e domada por inumeráveis regras e proibições, que se transformaram em
    autolimitações. Foi tão transformada, “refinada”, “civilizada” como todas as
    outras forma de prazer, e sua violência imediata e descontrolada aparece apenas
    em sonhos ou em explosões isoladas que explicamos como patológicas.

    [...]

    Deixando de lado uma pequena elite, o saque, a rapinagem, e o assassinato eram
    práticas comuns da sociedade guerreira dessa época, conforme anotou Luchaire, o
    historiador da sociedade francesa do século XIII. Há pouca evidência de que as
    coisas fossem diferentes em outros países ou nos séculos que se seguiram.
    Explosões de crueldade não excluíam ninguém da vida social. Seus autores não
    eram banidos. O prazer de matar e torturar era socialmente permitido. Até certo
    ponto, a própria estrutura social impelia seus membros nessa direção, fazendo
    com que parecesse necessário e praticamente vantajoso comportar-se dessa
    maneira.

    O que, por exemplo, devia ser feito com prisioneiros? Era pouco o dinheiro
    nessa sociedade. Se os prisioneiros podiam pagar e, além disso, eram membros da
    mesma classe do vitorioso, exercia-se certo grau de contenção. Mas, os outros?
    Conservá-los vivos significava alimentá-los. Devolvê-los significava aumentar a
    riqueza e o poder de luta do inimigo. Isto porque os súditos (isto é, os que
    trabalhavam, serviam e lutavam) faziam parte da riqueza da classe governante
    daquele tempo. De modo que os prisioneiros eram mortos ou devolvidos tão
    mutilados que não prestavam mais para serviço de guerra ou trabalho. O mesmo se
    aplicava à destruição de campos plantados, entupimento de poços e abate de
    árvores. Em uma sociedade predominantemente agrária, na qual as posses fixas
    representavam a maior parte da propriedade, isto também servia para enfraquecer
    o inimigo. A emotividade mais forte do comportamento era até certo ponto
    socialmente necessária. As pessoas se comportavam de maneira socialmente útil e
    tinham prazer nisso. 

    [...]

    Não que as pessoas andassem sempre de cara feia, arcos retesados e postura
    marcial como símbolo claro e visível de sua perícia belicosa. Muito pelo
    contrário, em um momento estão pilheriando, no outro trocam zombarias, uma
    palavra leva a outra e, de repente, emergindo do riso se veem no meio de uma
    rixa feroz. Grande parte do que nos parece contraditório — a intensidade da
    religiosidade, o grande medo do inferno, o sentimento de culpa, as penitências,
    as explosões desmedidas de alegria e divertimento, a súbita explosão de força
    incontrolável do ódio e da beligerância — tudo isso, tal como a rápida mudança
    de estados de ânimo, é na realidade sintoma da mesma estrutura social e de
    personalidade. Os instintos, as emoções, eram liberados de forma mais livre,
    mais direta, mais aberta, do que mais tarde. Só para nós, para quem tudo é mais
    controlado, moderado, calculado, em quem tabus sociais mergulham muito mais
    fundamente no tecido da vida instintiva como forma de autocontrole, é que esta
    visível intensidade de religiosidade, beligerância ou crueldade parece
    contraditória. A religião, a crença na onipotência punitiva ou premiadora de
    Deus nunca teve em si um efeito “civilizador” ou de controle de emoções. Muito
    ao contrário, a religião é sempre exatamente tão “civilizada” como a sociedade
    ou classe que a sustenta. E porque as emoções são expressas nessa época de uma
    maneira que, em nosso mundo, é geralmente observada em crianças, chamamos de
    “infantis” essas manifestações e formas de comportamento.

    [...]

    Quem quer que não amasse ou odiasse ao máximo nessa sociedade, quem quer que
    não soubesse defender sua posição no jogo das paixões, podia entrar para um
    mosteiro, para todos os efeitos. Na vida mundana ele estava tão perdido como,
    inversamente, estaria numa sociedade posterior, e particularmente na corte, o
    homem que não pudesse controlá-las, não pudesse esconder e “civilizar” suas
    emoções.

    [...]

    Nessa sociedade não havia poder central suficientemente forte para obrigar as
    pessoas a se controlarem.  [...] Conforme veremos no detalhe mais adiante, a
    reserva e a “consideração mútua” entre as pessoas aumentavam, inicialmente na
    vida social diária comum. E a descarga de emoções em ataque físico se limitava
    a certos enclaves temporais e espaciais. Uma vez tivesse o monopólio da força
    física passado a autoridades centrais, nem todos os homens fortes podiam se dar
    ao prazer do ataque físico. Isto passava nesse instante a ser reservado àqueles
    poucos legitimados pela autoridade central (como, por exemplo, a polícia contra
    criminosos) e a números maiores apenas em tempos excepcionais de guerra ou
    revolução, na luta socialmente legitimada contra inimigos internos ou externos.

    Mas até mesmo esses enclaves temporais ou espaciais na sociedade civilizada,
    nos quais se deu maior liberdade à beligerância — acima de tudo, nas guerras
    entre nações — tornaram-se mais impessoais e levavam cada vez menos a uma
    descarga emocional marcada pelo imediatismo e intensidade da fase medieval.
    [...] Ainda assim, isso poderia acontecer com maior rapidez do que poderíamos supor,
    não tivesse o combate físico direto entre um homem e seu odiado adversário
    cedido lugar a uma luta mecanizada que exige rigoroso controle dos afetos.
    Mesmo nas guerras do mundo civilizado, o indivíduo não pode mais dar rédea
    livre ao prazer provocado pela vista do inimigo, mas lutar, pouco importando
    como se sinta, obedecendo ao comando de chefes invisíveis, ou apenas
    indiretamente visíveis, e contra inimigos frequentemente invisíveis ou só
    indiretamente visíveis. E foi preciso uma imensa perturbação social, aguçada
    por propaganda habilmente concertada, para reacender e legitimar em grandes
    massas de pessoas os instintos socialmente proibidos, o prazer de matar e a
    destruição, que foram eliminados do cotidiano da vida civilizada.

    [...]

    Para dar um exemplo, a beligerância e a agressão encontram expressão
    socialmente permitida nos jogos esportivos. [...] Esse aspecto determina em
    parte a maneira como se escrevem livros e peças de teatro e influencia
    decisivamente o papel do cinema em nosso mundo.
    Essa transformação do que, inicialmente, se exprimia em uma manifestação ativa
    e frequentemente agressiva, no prazer passivo e mais controlado de assistir
    (isto é, em mero prazer do olho), já é iniciada na educação e nas regras de
    condicionamento dos jovens.

    Na edição de 1774 da Civilité, de La Salle, lemos (p.23): “Crianças gostam de
    tocar em roupas e em outras coisas que lhes agradam as mãos. Esta ânsia deve
    ser corrigida e devem ser ensinadas a tocar o que veem apenas com os olhos.”

    Hoje essa regra é aceita quase como natural. É altamente característico do
    homem civilizado que seja proibido por autocontrole socialmente inculcado de,
    espontaneamente, tocar naquilo que deseja, ama, ou odeia. Toda a modelação de
    seus gestos — pouco importando como o padrão possa diferir entre as nações
    ocidentais no tocante a detalhes — é decisivamente influenciada por essa
    necessidade. Já mostramos páginas atrás como o emprego do sentido do olfato, a
    tendência de cheirar o alimento ou outras coisas, veio a ser restringido como
    algo animal. Aqui temos uma das interconexões através da qual um diferente
    órgão dos sentidos, o olho, assume importância muito específica na sociedade
    civilizada. De maneira semelhante à da orelha, e talvez ainda mais, o olho se
    torna um mediador do prazer precisamente porque a satisfação direta do desejo
    pelo prazer foi circunscrita por grande número de barreiras e proibições.

    [...]

    Todos os que caírem fora dos limites desse padrão social são considerados
    “anormais”. Por conseguinte, alguém que desejasse gratificar seu prazer à
    maneira do século XVI, queimando gatos, seria hoje considerado “anormal”
    simplesmente porque o condicionamento normal em nosso estágio de civilização
    restringe a manifestação de prazer nesses atos mediante uma ansiedade instilada
    sob a forma de autocontrole.
    Neste caso, obviamente, opera o mesmo tipo de mecanismo psicológico com base no
    qual ocorreu a mudança a longo prazo da personalidade: manifestações
    socialmente indesejáveis de instintos e prazer são ameaçadas e punidas com
    medidas que geram e reforçam desagrado e ansiedade. Na repetição constante do
    desagrado despertado pelas ameaças, e na habituação a esse ritmo, o desagrado
    dominante é compulsoriamente associado até mesmo a comportamentos que, na sua
    origem, possam ser agradáveis. Dessa maneira, o desagrado e a ansiedade
    socialmente despertados — hoje representados, embora nem sempre nem
    exclusivamente, pelos pais — lutam com desejos ocultos. O que foi mostrado
    aqui, de diferentes ângulos, como um avanço nas fronteiras da vergonha, do
    patamar da repugnância, dos padrões das emoções, provavelmente foi posto em
    movimento por mecanismos como esses.

### Em tempos medievais

    A forca, o símbolo do poder judiciário de cavaleiro, é parte do ambiente de sua
    vida. Talvez não seja muito importante, mas, de qualquer maneira, não é um
    espetáculo particularmente doloroso. Sentença, execução, morte tudo isto é uma
    presença constante nessa vida. Elas, também, não foram ainda removidas para
    trás da cena.

    [...]

    por exemplo, na obra de Breughel, um padrão de repugnância que lhe permite
    trazer para as telas aleijados, camponeses, cadafalsos, ou pessoas que se
    aliviam de necessidades corporais. Mas o padrão visto neles está vinculado a
    sentimentos sociais muito diferentes dos que vemos nesses quadros em que
    aparece a classe alta de fins do período medieval.

    [...]

    Essas cenas de amor são tudo, menos “obscenas”. O amor é apresentado nelas como
    tudo o mais na vida do cavaleiro fidalgo, torneios, caçadas, campanhas,
    pilhagens. As cenas não são especialmente ressaltadas. Não sentimos em sua
    representação coisa alguma da violência, da tendência para excitar ou
    gratificar a satisfação de desejos negados na vida, característica de tudo o
    que é “obsceno”. Esse desenho não nasce de uma alma oprimida, nem revela nada
    de “secreto” mediante a violação de tabus.

    [...]

    Todas essas cenas são o retrato de uma sociedade na qual as pessoas davam vazão
    a impulsos e sentimentos de forma incomparavelmente mais fácil, rápida,
    espontânea e aberta do que hoje, na qual as emoções eram menos controladas e,
    em consequência, menos reguladas e passíveis de oscilar mais violentamente
    entre extremos.

    Mas tais restrições e controles se faziam em uma direção diferente e em grau
    menor que em períodos posteriores, e não assumiam a forma de autocontrole
    constante, quase automático. O tipo de integração e interdependência em que
    viviam essas pessoas não as compelia a abster-se de funções corporais uma na
    frente da outra ou a controlar seus impulsos agressivos na mesma extensão que
    na fase seguinte. Isto se aplica a todos. Mas, claro, para os camponeses, a
    margem para agressão era mais restrita do que para o cavaleiro — isto é,
    restrita a seus pares. [...] Uma restrição de ordem social às vezes se
    impunha ao camponês pelo simples fato de que não tinha o suficiente para comer.
    Isto certamente representa um controle de impulsos do mais alto grau e que
    afeta todo o comportamento do ser humano. Mas ninguém prestava atenção a isso e
    esta situação social dificilmente lhe tornava necessário impor controles a si
    mesmo quando assoava o nariz, escarrava, ou pegava vorazmente comida na mesa.

    [...]

    A manifestação de sentimentos na sociedade medieval é, de maneira geral, mais
    espontânea e solta do que no período seguinte. Mas não é livre ou sem modelagem
    social em qualquer sentido absoluto. O homem sem restrições é um fantasma.
    Reconhecidamente, a natureza, a força, o detalhamento de proibições, controles
    e dependências mudam de centenas de maneiras e, com elas, a tensão e o
    equilíbrio das emoções e, de idêntica maneira, o grau e tipo de satisfação que
    o indivíduo procura e consegue.

### Autocontrole como resposta ao avanço da interdependência

    Quanto mais avançam a interdependência e a divisão de trabalho na sociedade,
    mais dependente a classe alta se torna das outras e maior, por conseguinte, a
    força social destas, pelo menos potencialmente. Mesmo quando ela ainda é
    principalmente uma classe guerreira, quando mantém as outras classes
    dependentes sobretudo pela espada e o monopólio das armas, algum grau de
    dependência dessas outras classes não está de todo ausente. Mas é
    incomparavelmente menor, e menor também é, conforme veremos em mais detalhes
    adiante, a pressão vinda de baixo. Em consequência, o senso de domínio da
    classe alta, seu desprezo pelas demais, é muito mais franco, e muito menos
    forte a pressão sobre ela para praticar moderação e controlar seus impulsos.

    [...]

    Um novo comedimento, um controle e regulação novo e mais extenso do
    comportamento que a velha vida cavaleirosa fazia necessário ou possível, são
    agora exigidos do nobre. São resultado da nova e maior dependência em que foi
    colocado o nobre. Ele não é mais um homem relativamente livre, senhor de seu
    castelo, do castelo que é sua pátria.

### Para uma teoria sobre civilizações

    como e por que, no curso de transformações gerais da sociedade, que ocorrem em
    longos períodos de tempo e em determinada direção — e para as quais foi adotado
    o termo “desenvolvimento” —, a afetividade do comportamento e experiência
    humanos, o controle de emoções individuais por limitações externas e internas,
    e, neste sentido, a estrutura de todas as formas de expressão, são alterados em
    uma direção particular? Essas mudanças são indicadas na fala diária quando
    dizemos que pessoas de nossa própria sociedade são mais “civilizadas” do que
    antes, ou que as de outras sociedades são mais “incivis” ou menos “civilizadas”
    (ou mesmo mais “bárbaras”) que as da nossa. São óbvios os juízos de valor
    contidos nessas palavras, embora sejam menos óbvios os fatos a que se referem.
    Isto acontece em parte porque os estudos empíricos de transformações a longo
    prazo de estruturas de personalidade, e em especial de controle de emoções, dão
    origem a grandes dificuldades no estágio atual das pesquisas sociológicas. À
    frente do interesse sociológico no presente, encontramos processos de prazo
    relativamente curto e, em geral, apenas problemas relativos a um dado estado da
    sociedade. As transformações a longo prazo das estruturas sociais e, por
    conseguinte, também, das estruturas da personalidade, perderam-se de vista na
    maioria dos casos.

    O presente estudo diz respeito a esses processos de longo prazo. A sua
    compreensão pode ser facilitada por uma curta indicação dos vários tipos que
    esses processos assumem. Para começar, podemos distinguir duas direções
    principais nas mudanças estruturais das sociedades: as que tendem para maior
    diferenciação e integração, e as que tendem para menos. Além disso, há um
    terceiro tipo de processo social, no curso do qual é mudada a estrutura de uma
    sociedade, ou de alguns de seus aspectos particulares, mas sem haver tendência
    de aumento ou diminuição no nível de diferenciação e integração. Por último,
    são incontáveis as mudanças na sociedade que não implicam mudança em sua
    estrutura. Este trabalho não faz justiça a toda a complexidade dessas mudanças,
    porquanto numerosas formas híbridas, e não raro vários tipos de mudança, mesmo
    em direções opostas, podem ser observadas simultaneamente na mesma sociedade.
    Mas, por ora, este curto esboço dos diferentes tipos de mudança deve bastar
    para indicar os problemas de que trata este estudo.

    O primeiro volume concentra-se, acima de tudo, na questão de saber se a
    suposição, baseada em observações dispersas, de que há mudanças a longo prazo
    nas emoções e estruturas de controle das pessoas em sociedades particulares —
    mudanças que se desenvolvem ao longo de uma única e mesma direção durante
    grande número de gerações — pode ser confirmada por evidência fidedigna e
    encontrar comprovação factual.

    [...]

    A demonstração de uma mudança em emoções e estruturas de controle humanas que
    ocorre ao longo de muitas gerações, e na mesma direção ou, em curtas palavras,
    o aumento do reforço e diferenciação dos controles — gera outra questão: é
    possível relacionar essa mudança a longo prazo nas estruturas da personalidade
    com mudanças a longo prazo na sociedade como um todo, que de igual maneira
    tendem a uma direção particular, a um nível mais alto de diferenciação e
    integração social? O segundo volume trata desses problemas.

    No tocante a essas mudanças estruturais a longo prazo da sociedade, falta
    também prova empírica. Tornou-se, por conseguinte, necessário no segundo volume
    dedicar parte do mesmo à descoberta e elucidação das ligações factuais nesta
    segunda área. A questão é se uma mudança estrutural da sociedade como um todo,
    tendendo a um nível mais alto de diferenciação e integração, pode ser
    demonstrada com ajuda de evidência empírica confiável. Isto se revelou
    possível. O processo de formação dos Estados nacionais, discutido no segundo
    volume, constitui um exemplo desse tipo de mudança estrutural.  Finalmente, em
    um esboço provisório de uma teoria de civilização, elabora-se um modelo, a fim
    de demonstrar possíveis ligações entre a mudança a longo prazo nas estruturas
    da personalidade no rumo da consolidação e diferenciação dos controles
    emocionais, e a mudança a longo prazo na estrutura social com vistas a um nível
    mais alto de diferenciação e integração como, por exemplo, visando a uma
    diferenciação e prolongamento das cadeias de interdependência e à consolidação
    dos “controles estatais”.

    [...]

    Facilmente se pode compreender que ao adotar uma metodologia voltada para
    ligações factuais e suas explicações (isto é, um enfoque empírico e teórico
    preocupado com mudanças estruturais de longo prazo de um tipo específico, ou
    “desenvolvimento”), abandonamos as ideias metafísicas que vinculam o conceito
    de desenvolvimento à noção ou de uma necessidade mecânica ou de uma finalidade
    teleológica.

    [...]

    este estudo nem era de uma “evolução”, no sentido do século XIX, de um
    progresso automático, nem de uma “mudança social” inespecífica no sentido do
    século XX. Naquele tempo isto me pareceu tão óbvio que deixei de mencionar
    explicitamente essas implicações teóricas. A introdução à segunda edição me dá
    a oportunidade de corrigir essa omissão.

    [...]

    A situação é semelhante no tocante a grande número de outros problemas aqui
    estudados. Quando, após vários estudos preparatórios que me permitiram
    investigar a evidência documentária e explorar os problemas teóricos
    gradualmente emergentes, o caminho para uma possível solução pareceu mais
    claro, tornei-me consciente de que este estudo ajuda a solucionar o renitente
    problema da ligação entre estruturas psicológicas individuais (as assim
    chamadas estruturas de personalidade) e as formas criadas por grandes números
    de indivíduos interdependentes (as estruturas sociais). E o faz porque aborda
    ambos os tipos de estruturas não como fixos, como em geral acontece, mas como
    mutáveis, como aspectos interdependentes do mesmo desenvolvimento de longo
    prazo.

### Os limites de conceber as coisas como um mero jogo de cartas

    A fim de exemplificar este fato, bastará discutir a maneira como o homem que é
    atualmente considerado o principal teórico da sociologia, Talcott Parsons,
    tenta colocar e solucionar alguns dos problemas aqui estudados. É
    característico do enfoque teórico de Parsons tentar dissecar analiticamente, em
    seus componentes elementares, como disse ele certa vez,1 os diferentes tipos de
    sociedades em seu campo de observação. A um tipo particular de componente
    elementar ele chamou de “variáveis de padrão”. Essas variáveis de padrão
    incluem a dicotomia entre “afetividade” e “neutralidade afetiva”. Sua concepção
    pode ser mais bem-entendida comparando-se a sociedade com um jogo de cartas:
    cada tipo de sociedade, na opinião de Parsons, representa uma “mão” diferente.
    As cartas, porém, são sempre as mesmas e seu número é pequeno, por mais
    diversas que sejam suas faces. Uma das cartas com que o jogo é disputado é a
    polaridade entre afetividade e neutralidade afetiva. Parsons concebeu
    originariamente essa ideia, segundo nos diz, analisando os tipos de sociedade
    de Tönnies, a Gemeinschaft (comunidade) e Gesellschaft (sociedade). A
    “comunidade”, parece acreditar ele, caracteriza-se pela afetividade, e a
    “sociedade”, pela neutralidade afetiva. Mas, ao determinar as diferenças entre
    diferentes tipos de sociedade, e diferentes tipos de relacionamentos dentro da
    mesma, ele atribui a essa “variável de padrão” no jogo de cartas, como a
    outras, um significado inteiramente geral. No mesmo contexto, Parsons trata do
    problema da relação entre estrutura social e personalidade.2 Indica que
    conquanto antes os tivesse interpretado simplesmente como “sistemas de ação
    humana” estreitamente vinculados e interatuantes, agora pode declarar com
    certeza que, em um sentido teórico, eles são fases, ou aspectos, diferentes de
    um único e mesmo sistema de ação fundamental. Ilustra isto com um exemplo,
    explicando que o que no plano sociológico pode ser considerado como uma
    institucionalização da neutralidade afetiva é, essencialmente, o mesmo que no
    nível da personalidade pode ser considerado como “na imposição da renúncia à
    gratificação imediata, no interesse da organização disciplinada e dos objetivos
    a longo prazo da personalidade”.

    [...]

    [1] in Talcott Parsons, Essays in Sociological Theory (Glencoe, 1963), p.359 e segs.

    [...]

    O que, neste livro, com ajuda de extensa documentação empírica se mostra que é
    um processo, Parsons, pela natureza estática de seus conceitos, reduz
    retrospectivamente, e em minha opinião sem nenhuma necessidade, a estados. Em
    vez de um processo relativamente complexo, mediante o qual a vida afetiva das
    pessoas é gradualmente levada a um maior e mais uniforme controle de emoções —
    mas certamente não a um estado de total neutralidade afetiva —, Parsons sugere
    uma simples oposição entre dois estados, afetividade e neutralidade afetiva,
    que supostamente estariam presentes em graus diferentes em diferentes tipos de
    sociedade, tal como quantidades diferentes de substâncias químicas.

    [...]

    Os fenômenos sociais, na verdade, só podem ser observados como evoluindo e
    tendo evoluído. Sua dissecação por meio de pares de conceitos, que restringem a
    análise a dois estados antitéticos, representa um desnecessário empobrecimento
    da percepção sociológica tanto a nível empírico como teórico.

    [...]

    As categorias básicas selecionadas por Parsons, no entanto, parecem-me
    arbitrárias no mais alto grau. Subjacentes a elas há a noção tácita, não
    comprovada e supostamente axiomática, de que o objetivo de toda teoria
    científica é o de reduzir tudo o que é variável a algo invariável, e
    simplificar todos os fenômenos complexos dissecando-os em seus componentes
    individuais.

Construções auxiliares desnecessariamente complicadas:

    O exemplo da teoria de Parsons, no entanto, sugere que a teorização no campo da
    sociologia é mais complicada, do que simplificada, por uma sistemática redução
    dos processos sociais a estados sociais, e de fenômenos complexos,
    heterogêneos, a componentes mais simples e só aparentemente homogêneos. Este
    tipo de redução e abstração poderia justificar-se como método de teorização
    apenas se levasse, inequivocamente, a uma compreensão mais clara e profunda
    pelos homens de si mesmos como sociedades e como indivíduos. Em vez disso,
    descobrimos que as teorias formuladas por esses métodos, tal como a teoria do
    epiciclo de Ptolomeu, exigem construções auxiliares desnecessariamente
    complicadas, a fim de fazer com que concordem com os fatos observáveis. Não
    raro elas parecem nuvens escuras das quais, daqui e dali, caem uns poucos raios
    de luz que tocam a terra.

Processos:

    Na verdade, é indispensável que o conceito de processo seja incluído em teorias
    sociológicas ou de outra natureza que tratem de seres humanos. Conforme
    demonstrado neste estudo, a relação entre o indivíduo e as estruturas sociais
    só pode ser esclarecida se ambos forem investigados como entidades em mutação e
    evolução. Só então será possível construir modelos de seus relacionamentos,
    como é feito aqui, que concordam com fatos demonstráveis.

    [...]

    Sem jamais dizer isto clara e abertamente, Parsons e todos os sociólogos da
    mesma inclinação imaginam que existam separadamente essas coisas a que se
    referem os conceitos de “indivíduo” e “sociedade”. Assim — para dar apenas um
    exemplo —, Parsons adota a noção, já desenvolvida por Durkheim, de que a
    relação entre “indivíduo” e “sociedade” é uma “interpenetração” do indivíduo e
    do sistema social. Como quer que essa “interpenetração” seja concebida, o que
    mais pode essa metáfora significar, senão que estamos tratando de duas
    entidades diferentes que, primeiro, existem separadamente e que depois se
    “interpenetram”?3

Segue uma ótima crítica à sociologia estrutural/estática que enxerga sociedades
como sistemas em repouso e que apenas buscam o repouso quando ocorre algum acidente
perturbando seu equilíbrio homeostático.

### Humildar: sacar mais qual é a real e não se iludir

    Muitos dos artigos de fé sociológicos pioneiros não foram mais aceitos pelos
    sociólogos do século XX. Eles incluíam, acima de tudo, a crença em que o
    desenvolvimento da sociedade é necessariamente uma evolução para o melhor, um
    movimento na direção do progresso. Esta crença foi categoricamente rejeitada
    por muitos sociólogos posteriores, de acordo com sua própria experiência
    social.

    [...]

    Se, no século XIX, concepções específicas do que devia ser ou do que se
    desejava que fosse — concepções ideológicas específicas — levaram a um
    interesse fundamental pelo desenvolvimento da sociedade, no século XX outras
    concepções do que devia ser ou era desejável — outras concepções ideológicas —
    geraram pronunciado interesse entre os principais teóricos pelo estado da
    sociedade como ela se encontra, negligenciando os problemas da dinâmica das
    formações sociais, e sua falta de interesse por problemas de processo de longa
    duração e por todas as oportunidades de explicação que o estudo desses
    problemas proporciona.

    [...]

    Nos países em fase de industrialização do século XIX, onde foram escritos os
    primeiros grandes trabalhos pioneiros de sociologia, as vozes que expressavam
    as crenças, ideais, esperanças e objetivos a longo prazo das nascentes classes
    industriais ganharam, aos poucos, vantagens sobre as que procuravam preservar a
    ordem social existente no interesse das tradicionais elites de poder dinásticas
    de corte, aristocráticas ou patrícias. Foram as primeiras, em conformidade com
    sua situação de classes emergentes, que alimentaram altas expectativas de um
    futuro melhor. E como seus ideais se concentravam não no presente, mas no
    futuro, sentiram um interesse todo especial pela dinâmica, pelo desenvolvimento
    da sociedade. Juntamente com uma ou outra dessas classes industriais
    emergentes, os sociólogos da época procuraram obter confirmação de que o
    desenvolvimento da humanidade se daria na direção de seus desejos e esperanças.
    Fizeram isso estudando a direção e as forças motivadoras do desenvolvimento
    social até então. É muito difícil, porém, distinguir em retrospecto entre
    doutrinas heterônomas, repletas de ideais de curto prazo condicionados pela
    época em que apareceram, e os modelos conceituais que se revestem de
    importância, independentemente desses ideais, e exclusivamente no que tange a
    fatos verificáveis.

    Por outro lado, no mesmo século seriam ouvidas vozes que, por uma ou outra
    razão, opunham-se à transformação da sociedade pela via da industrialização,
    cuja fé social era orientada para a conservação da herança existente, e que
    ofereciam — aos que viam no presente um estado de deterioração — o ideal de um
    passado melhor. Representavam elas não só as elites pré-industriais dos Estados
    dinásticos, mas também grupos mais amplos de trabalhadores — acima de tudo os
    que se ocupavam na agricultura e ofícios artesanais, cujo meio de vida
    tradicional estava sendo corroído pelo avanço da industrialização. Eram os
    adversários de todos aqueles que falavam do ponto de vista das crescentes
    classes trabalhadoras industriais e que, de conformidade com a situação
    ascendente dessas classes, buscavam inspiração na crença em um futuro melhor,
    no progresso da humanidade. Desta maneira, no século XIX, o coro de vozes
    dividia-se entre os que exaltavam um passado melhor e os que cantavam um futuro
    mais risonho. Entre os sociólogos cuja imagem de sociedade se orientava para o
    progresso e um futuro melhor eram encontrados, conforme sabemos, porta-vozes
    das duas classes industriais. Incluíam eles homens como Marx e Engels, que se
    identificavam com a classe operária industrial, e também sociólogos burgueses
    como Comte, no início do século XIX, e Hobhouse, no fim.

    [...]

    O objetivo não é atacar outros ideais em nome dos ideais que temos, mas
    procurar compreender melhor a estrutura desses processos em si e emancipar o
    arcabouço teórico da pesquisa sociológica da primazia de ideais e doutrinas
    sociais. Isto porque só poderemos reunir conhecimentos sociológicos adequados o
    suficiente para serem usados na solução dos agudos problemas da sociedade se,
    quando equacionamos e resolvemos problemas sociológicos, deixamos de subordinar
    a investigação do que é a ideias preconcebidas a respeito do que as soluções
    devem ser.

A discussão que segue é muito porreta e vale a pena ser lida na íntegra. Em especial:

    Se o presente estudo tem alguma importância em absoluto, isto resulta de sua
    oposição a esta mistura do que é com o que devia ser, de análise científica com
    ideal. Indica a possibilidade de libertar o estudo da sociedade da servidão a
    ideologias sociais. Isto não quer dizer que um estudo de problemas sociais que
    exclua ideias políticas e filosóficas implique renunciar à possibilidade de
    influenciar o curso dos fatos políticos através dos resultados da pesquisa. O
    que ocorre é o oposto. A utilidade da pesquisa sociológica, como instrumento da
    prática social, só aumenta se o pesquisador não se engana projetando aquilo que
    deseja, aquilo que acredita que deve ser, em sua investigação do que é e foi.

    [...]

    Essa cisão nos ideais, essa contradição no ethos no qual são educadas as
    pessoas, encontra expressão em teorias sociológicas. Algumas delas tomam como
    ponto de partida o indivíduo independente, autossuficiente, como a “verdadeira”
    realidade e, por conseguinte, como o objeto autêntico da ciência social; outras
    começam com a totalidade social independente. Algumas tentam harmonizar as duas
    concepções, geralmente sem indicar como é possível reconciliar a ideia de um
    indivíduo inteiramente independente e livre com a de uma “totalidade social”
    igualmente independente e livre, e não raro sem perceber por inteiro a natureza
    do problema.

### Indivíduo como sujeito aberto

    No curso deste processo, as estruturas do ser humano individual são mudadas em
    uma dada direção. É isto o que o conceito de “civilização”, no sentido factual
    usado aqui, realmente significa.

    [...]

    Na filosofia clássica, essa figura entra em cena como sujeito epistemológico.
    Neste papel, como homo philosophicus, o indivíduo obtém conhecimento do mundo
    “externo” de uma forma inteiramente autônoma. Não precisa aprender, receber
    seus conhecimentos de outros. O fato de que chegou ao mundo como criança, o
    processo inteiro de seu desenvolvimento até a vida adulta e como adulto, são
    ignorados como irrelevantes por essa imagem do homem. No desenvolvimento da
    humanidade, foram precisos milhares de anos para que o homem começasse a
    compreender as relações entre os fenômenos naturais, o curso das estrelas, a
    chuva e o Sol, o trovão e o raio, como manifestações de uma sequência de
    conexões causais cegas, impessoais, inteiramente mecânicas e regulares. Mas a
    “personalidade fechada” do homo philosophicus aparentemente percebe essa cadeia
    causal mecânica e regular, quando adulto, simplesmente abrindo os olhos, sem
    precisar aprender coisa alguma sobre ela com seus semelhantes, e de modo
    inteiramente independente do estágio de conhecimentos alcançado pela sociedade.
    [...] Vimos aqui um exemplo da força com que a incapacidade de conceber
    processos a longo prazo (isto é, mudanças estruturadas nas configurações
    formadas por grande número de seres humanos interdependentes) ou de compreender
    os seres humanos que formam essas configurações, está ligada a um certo tipo de
    imagem do homem e da sua percepção de si mesmo.
    [...] Pessoas para quem parece axiomático que seu próprio ser (ou ego, ou o
    que mais possa ser chamado) existe, por assim dizer, “dentro” delas, isolado de
    todas as demais pessoas e coisas “externas”, têm dificuldade em atribuir
    importância a esses fatos que indicam que os indivíduos, desde o início de sua
    vida, existem em interdependência dos outros. Têm dificuldade em conceber as
    pessoas como relativa, mas não absolutamente, autônomas e interdependentes,
    formando configurações mutáveis entre si.

    [...]

    A armadilha conceitual na qual estamos sendo continuamente colhidos por ideias
    estáticas, como “indivíduo” e “sociedade”, só pode ser aberta se, como é feito
    aqui, elas são desenvolvidas ainda mais, em conjunto com estudos empíricos,
    isto de maneira tal que os dois conceitos sejam levados a se referirem a
    processos. Essa tentativa, porém, é inicialmente bloqueada pela extraordinária
    convicção implantada nas sociedades europeias, desde aproximadamente os dias da
    Renascença, pela autopercepção de seres humanos em termos de seu próprio
    isolamento, da completa separação entre seu “interior” e tudo o que é
    “exterior”.

    [...]

    Esta autopercepção também se encontra, em forma menos racionalizada, na
    literatura de ficção — como, por exemplo, no lamento de Virginia Woolf sobre a
    incomunicabilidade da experiência como causa da solidão humana. [...]
    Estaremos nós acaso tratando, como tantas vezes parece ser, de uma experiência
    eterna, fundamental, de todos os seres humanos, que não admite qualquer outra
    explicação, ou apenas do tipo de autopercepção que caracteriza um certo estágio
    no desenvolvimento de configurações formadas por pessoas, e das pessoas que as
    formam?

    [...]

    Não foram simplesmente as novas descobertas, o aumento cumulativo dos
    conhecimentos sobre os objetos da reflexão humana, que se fizeram necessários
    para tornar possível a transição de uma visão do mundo, de geocêntrica para
    heliocêntrica. O que foi necessário, acima de tudo, foi um aumento na
    capacidade do homem para se distanciar, mentalmente, de si mesmo. Modos
    científicos de pensamento não podem ser desenvolvidos, nem se tornar geralmente
    aceitos, a menos que as pessoas renunciem à sua inclinação primária,
    irrefletida e espontânea a compreender todas as suas experiências em termos de
    seu propósito e significado para si mesmas. O desenvolvimento que levou a um
    conhecimento mais profundo e ao crescente controle da natureza foi, por
    conseguinte, se considerado neste aspecto, também o desenvolvimento no sentido
    de maior autocontrole pelo homem.

    [...]

    Não é possível entrar em mais detalhes aqui sobre as ligações entre o
    desenvolvimento do método científico pelo qual se adquire conhecimento de
    objetos, por um lado, e o desenvolvimento de novas atitudes do homem para
    consigo mesmo, novas estruturas de personalidade e, especialmente, mudanças
    rumo a um maior controle das emoções e autodistanciamento, por outro. Talvez
    contribua para a compreensão destes problemas lembrar o egocentrismo
    espontâneo, irrefletido, de pensamento que podemos observar a qualquer momento
    nas crianças de nossa própria sociedade. Um controle mais rigoroso das emoções,
    desenvolvido em sociedade e aprendido pelo indivíduo, e acima de tudo um grau
    mais alto de controle emocional autônomo, foi necessário para que a visão do
    mundo centralizada na Terra e nas pessoas que nela vivem fosse superada por
    outra que, como a visão heliocêntrica, concorda melhor com os fatos
    observáveis, mas que era de início menos gratificante emocionalmente, porquanto
    tirava o homem de sua posição no centro do universo e o colocava em um dos
    muitos planetas que revolvem em torno do centro. A passagem da compreensão da
    natureza legitimada pela fé tradicional para outra, baseada na pesquisa
    científica, e a mudança rumo a maior controle emocional que essa passagem
    acarretou, é um aspecto do processo civilizador, que no estudo que ora
    republico examino a partir de outros aspectos.

    [...]

    O desenvolvimento da ideia de que a Terra gira em torno do Sol de uma maneira
    puramente mecânica, de acordo com leis naturais — isto é, de uma maneira que de
    forma alguma é determinada por qualquer finalidade referida à humanidade e, por
    conseguinte, não mais possui qualquer notável importância emocional para o
    homem — pressupunha e exigia ao mesmo tempo um desenvolvimento nos próprios
    seres humanos, no sentido de aumento do controle emocional, maior contenção da
    sensação espontânea de que tudo o que experimentassem, e tudo que lhes dissesse
    respeito, expressava uma intenção, um destino, uma finalidade que se
    relacionava com eles próprios. Agora, na época que chamamos de “moderna”, os
    homens chegaram a um estágio de autodistanciamento que lhes permite conceber os
    processos naturais como uma esfera autônoma que opera sem intenção, finalidade
    ou destino, em uma forma puramente mecânica ou causal, e que tem significação
    ou finalidade para eles apenas se estiverem em condições, através do
    conhecimento objetivo, de controlá-los e, desta maneira, dar-lhes significado e
    finalidade. Mas, nesse estágio, ainda não são capazes de se distanciarem o
    suficiente de si mesmos para tornar seu próprio autodistanciamento, sua própria
    contenção de emoções — em suma, as condições de seu próprio papel como o
    sujeito da compreensão científica da natureza — objeto do conhecimento e da
    indagação científica.

    [...]

    Os autocontroles individuais autônomos criados dessa maneira na vida social,
    tais como o “pensamento racional” e a “consciência moral”, nesse momento se
    interpõem mais severamente do que nunca entre os impulsos espontâneos e
    emocionais, por um lado, e os músculos do esqueleto, por outro, impedindo mais
    eficazmente os primeiros de comandar os segundos (isto é, de pô-los em ação)
    sem a permissão desses mecanismos de controle.

    [...]

    Todos eles mostram as marcas da transição para um estágio ulterior de
    autoconsciência, no qual o autocontrole embutido das emoções torna-se mais
    forte e maior o distanciamento reflexivo, enquanto a espontaneidade da ação
    afetiva diminui, e no qual as pessoas sentem essas suas peculiaridades mas
    ainda não se distanciam o suficiente delas em pensamento para fazerem de si
    mesmas um objeto de investigação.

    [...]

    Chegamos assim um pouco mais perto do centro da estrutura da personalidade
    individual subjacente à experiência de si mesmo do homo clausus. Se
    perguntamos, mais uma vez, o que realmente deu origem a esse conceito de
    indivíduo como encapsulado “dentro” de si mesmo, separado de tudo o que existe
    fora dele, e o que a cápsula e o encapsulado realmente significam em termos
    humanos, podemos ver agora a direção em que deve ser procurada a resposta. O
    controle mais firme, mais geral e uniforme das emoções, característico dessa
    mudança civilizadora, juntamente com o aumento de compulsões internas que, mais
    implacavelmente do que antes, impedem que todos os impulsos espontâneos se
    manifestem direta e motoramente em ação, sem a intervenção de mecanismos de
    controle — são o que é experimentado como a cápsula, a parede invisível que
    separa o “mundo interno” do indivíduo do “mundo externo” ou, em diferentes
    versões, o sujeito de cognição de seu objeto, o “ego” do outro, o “indivíduo”
    da “sociedade”. O que está encapsulado são os impulsos instintivos e
    emocionais, aos quais é negado acesso direto ao aparelho motor. Eles surgem na
    autopercepção como o que é ocultado de todos os demais, e, não raro, como o
    verdadeiro ser, o núcleo da individualidade. A expressão “o homem interior” é
    uma metáfora conveniente, mas que induz em erro.

### O conceito de configuração

    A imagem do homem como “personalidade fechada” é substituída aqui pela de
    “personalidade aberta”, que possui um maior ou menor grau (mas nunca absoluto
    ou total) de autonomia face a de outras pessoas e que, na realidade, durante
    toda a vida é fundamentalmente orientada para outras pessoas e dependente
    delas. A rede de interdependências entre os seres humanos é o que os liga. Elas
    formam o nexo do que é aqui chamado configuração, ou seja, uma estrutura de
    pessoas mutuamente orientadas e dependentes. Uma vez que as pessoas são mais ou
    menos dependentes entre si, inicialmente por ação da natureza e mais tarde
    através da aprendizagem social, da educação, socialização e necessidades
    recíprocas socialmente geradas, elas existem, poderíamos nos arriscar a dizer,
    apenas como pluralidades, apenas como configurações. Este o motivo por que,
    conforme afirmado antes, não é particularmente frutífero conceber os homens à
    imagem do homem individual. Muito mais apropriado será conjecturar a imagem de
    numerosas pessoas interdependentes formando configurações (isto é, grupos ou
    sociedades de tipos diferentes) entre si. Vista deste ponto de vista básico,
    desaparece a cisão na visão tradicional do homem.

### Dança, mu-dança

    O que temos em mente com o conceito de configuração pode ser convenientemente
    explicado com referência às danças de salão. Elas são na verdade, o exemplo
    mais simples que poderíamos escolher. Pensemos na mazurca, no minueto, na
    polonaise, no tango, ou no rock’n’roll. A imagem de configurações móveis de
    pessoas interdependentes na pista de dança talvez torne mais fácil imaginar
    Estados, cidades, famílias, e também sistemas capitalistas, comunistas e
    feudais como configurações. Usando este conceito, podemos eliminar as
    antíteses, chegando finalmente a valores e ideais diferentes, implicados hoje
    no uso das palavras “indivíduo” e “sociedade”. Certamente podemos falar na
    dança em termos gerais, mas ninguém a imaginará como uma estrutura fora do
    indivíduo ou como uma mera abstração. As mesmas configurações podem certamente
    ser dançadas por diferentes pessoas, mas, sem uma pluralidade de indivíduos
    reciprocamente orientados e dependentes, não há dança. Tal como todas as demais
    configurações sociais, a da dança é relativamente independente dos indivíduos
    específicos que a formam aqui e agora, mas não de indivíduos como tais. Seria
    absurdo dizer que as danças são construções mentais abstraídas de observações
    de indivíduos considerados separadamente. O mesmo se aplica a todas as demais
    configurações. Da mesma maneira que as pequenas configurações da dança mudam —
    tornando-se ora mais lentas, ora mais rápidas — também assim, gradualmente ou
    com maior subitaneidade, acontece com as configurações maiores que chamamos de
    sociedades. O estudo que se segue diz respeito a essas mudanças.

### Dinâmica criadora de monopólios

    Desta maneira, o ponto de partida para o estudo do processo de formação do
    Estado é uma configuração constituída de numerosas unidades sociais
    relativamente pequenas, em livre competição umas com as outras. A investigação
    mostra como e por que essa configuração muda. Demonstra simultaneamente que há
    explicações que não revestem o caráter de explicações causais. Isto porque uma
    mudança na configuração é explicada em parte pela dinâmica endógena dela mesma,
    a tendência a formar monopólios que é imanente a uma configuração de unidades
    livremente competitivas entre si. O estudo mostra por conseguinte como no curso
    dos séculos a configuração inicial se transforma em outra, na qual essas
    grandes oportunidades de poder monopolístico são ligadas a uma única posição
    social — a monarquia —, e nenhum ocupante de qualquer outra posição social na
    rede de interdependências pode competir com o monarca. Ao mesmo tempo, indica
    como as estruturas de personalidade dos seres humanos mudam também em conjunto
    com essas mudanças de configuração.
