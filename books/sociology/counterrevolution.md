[[!meta title="The Counterrevolution"]]

* [The Counterrevolution](http://bernardharcourt.com/the-counterrevolution/).
* By Bernard E. Harcourt.

## Index

[[!toc startlevel=2 levels=4]]

## Genealogy

### Mass-scale warfare

* MAD
* Massive retaliation
* Game theory
* Systems analisys
* Nuclear war

### Counterinsurgency

* Modern warfare
* Unconventional, counter-guerrila
* Special Ops
* Surgical operations

### Mao's “Eight Points of Attention” plus two principles

1. Talk to people politely.
2. Observe fair dealing in all business transactions.
3. Return everything borrowed from the people.
4. Pay for anything damaged.
5. Do not beat or scold the people.
6. Do not damage crops.
7. Do not molest women.
8. Do not ill-treat prisoners-of-war.

      Two other principles were central to Mao’s revolutionary doctrine: first, the
      importance of having a unified political and military power structure that
      consolidated, in the same hands, political and military considerations; and
      second, the importance of psychological warfare. More specifically, as Paret
      explained, “proper psychological measures could create and maintain ideological
      cohesion among fighters and their civilian supporters.” 7

### Paret's (1960) tasks of “counterguerrilla action”

1. The military defeat of the guerrilla forces.
2. The separation of the guerrilla from the population.
3. The reestablishment of governmental authority and the development of a viable social order.

### Petraeus: 3 key pillars

1. "The first is that the most important struggle is over the population."

2. "Allegiance of the masses can only be secured
    by separating the small revolutionary minority from the passive majority, and by
    isolating, containing, and ultimately eliminating the active minority. In his
    accompanying guidelines,"

3. "Success turns on collecting information on
   everyone in the population. Total information is essential to properly distinguish
   friend from foe and then extract the revolutionary minority. It is intelligence—
   total information awareness—that renders the counterinsurgency possible."

## Excerpts

### Torture

    In Modern Warfare, Trinquier quietly but resolutely condoned torture. The
    interrogations and related tasks were considered police work, as opposed to
    military operations, but they had the exact same mission: the complete
    destruction of the insurgent group. Discussing the typical interrogation of a
    detainee, captured and suspected of belonging to a terrorist organization,
    Trinquier wrote: “No lawyer is present for such an interrogation. If the prisoner
    gives the information requested, the examination is quickly terminated; if not,
    specialists must force his secret from him. Then, as a soldier, he must face the
    suffering, and perhaps the death, he has heretofore managed to avoid.” Trinquier
    described specialists forcing secrets out of suspects using scientific methods that
    did not injure the “integrity of individuals,” but it was clear what those
    “scientific” methods entailed. 4 As the war correspondent Bernard Fall suggests,
    the political situation in Algeria offered Trinquier the opportunity to develop “a
    Cartesian rationale” to justify the use of torture in modern warfare. 5
    Similarly minded commanders championed the use of torture, indefinite
    detention, and summary executions. They made no bones about it.

    [...]

    In his autobiographical account published in 2001, Services Spéciaux. Algérie
    1955–1957, General Paul Aussaresses admits to the brutal methods that were the
    cornerstone of his military strategy. 6 He makes clear that his approach to
    counterinsurgency rested on a three-pronged strategy, which included first,
    intelligence work; second, torture; and third, summary executions. The
    intelligence function was primordial because the insurgents’ strategy in Algeria
    was to infiltrate and integrate the population, to blend in perfectly, and then
    gradually to involve the population in the struggle. To combat this insurgent
    strategy required intelligence—the only way to sort the dangerous
    revolutionaries from the passive masses—and then, violent repression. “The first
    step was to dispatch the clean-up teams, of which I was a part,” Aussaresses
    writes. “Rebel leaders had to be identified, neutralized, and eliminated discreetly.
    By seeking information on FLN leaders I would automatically be able to capture
    the rebels and make them talk.” 7
    The rebels were made to talk by means of torture. Aussaresses firmly
    believed that torture was the best way to extract information. It also served to
    terrorize the radical minority and, in the process, to reduce it. The practice of
    torture was “widely used in Algeria,” Aussaresses acknowledges. Not on every
    prisoner, though; many spoke freely. “It was only when a prisoner refused to talk
    or denied the obvious that torture was used.” 8
    Aussaresses claims he was introduced to torture in Algeria by the policemen
    there, who used it regularly. But it quickly became routine to him. “Without any
    hesitation,” he writes, “the policemen showed me the technique used for
    ‘extreme’ interrogations: first, a beating, which in most cases was enough; then
    other means, such as electric shocks, known as the famous ‘gégène’; and finally
    water.” Aussaresses explains: “Torture by electric shock was made possible by
    generators used to power field radio transmitters, which were extremely
    common in Algeria. Electrodes were attached to the prisoner’s ears or testicles,
    then electric charges of varying intensity were turned on. This was apparently a
    well-known procedure and I assumed that the policemen at Philippeville [in
    Algeria] had not invented it.” 9 (Similar methods had, in fact, been used earlier in
    Indochina.)

    Aussaresses could not have been more clear:

        The methods I used were always the same: beatings, electric shocks, and, in particular, water
        torture, which was the most dangerous technique for the prisoner. It never lasted for more than one
        hour and the suspects would speak in the hope of saving their own lives. They would therefore
        either talk quickly or never.

    The French historian Benjamin Stora confirms the generalized use of torture.
    He reports that in the Battle of Algiers, under the commanding officer, General
    Jacques Massu, the paratroopers conducted massive arrests and “practiced
    torture” using “electrodes […] dunking in bathtubs, beatings.” General Massu
    himself would later acknowledge the use of torture. In a rebuttal he wrote in
    1971 to the film The Battle of Algiers, Massu described torture as “a cruel
    necessity.” 10 According to Aussaresses, torture was condoned at the highest
    levels of the French government. “Regarding the use of torture,” Aussaresses

    [...]

    For Aussaresses, as for Roger Trinquier, torture and disappearances were
    simply an inevitable byproduct of an insurgency—inevitable on both sides of the
    struggle. Because terrorism was inscribed in revolutionary strategy, it had to be
    used in its repression as well. In a fascinating televised debate in 1970 with the
    FLN leader and producer of The Battle of Algiers, Saadi Yacef, Trinquier
    confidently asserted that torture was simply a necessary and inevitable part of
    modern warfare. Torture will take place. Insurgents know it. In fact, they
    anticipate it. The passage is striking:

        I have to tell you. Whether you’re for or against torture, it makes no difference. Torture is a
        weapon that will be used in every insurgent war. One has to know that… One has to know that in
        an insurgency, you are going to be tortured.
        And you have to mount a subversive organization in light of that and in function of torture. It is
        not a question of being for or against torture. You have to know that all arrested prisoners in an
        insurgency will speak—unless they commit suicide. Their confession will always be obtained. So a
        subversive organization must be mounted in function of that, so that a prisoner who speaks does
        not give away the whole organization.16

    “Torture?” asks the lieutenant aide de camp in Henri Alleg’s 1958 exposé
    The Question. “You don’t make war with choirboys.” 18 Alleg, a French
    journalist and director of the Alger républicain newspaper, was himself detained
    and tortured by French paratroopers in Algiers. His book describes the
    experience in detail, and in his account, torture was the inevitable product of
    colonization and the anticolonial struggle. As Jean-Paul Sartre writes in his

    [...]

    In an arresting part of The Battle of Algiers it becomes clear that many of the
    French officers who tortured suspected FLN members had themselves, as
    members of the French Resistance, been victims of torture at the hands of the
    Gestapo. It is a shocking moment. We know, of course, that abuse often begets
    abuse; but nevertheless, one would have hoped that a victim of torture would
    recoil from administering it to others. Instead, as Trinquier suggests, torture
    became normalized in Algeria. This is, as Sartre describes it, the “terrible truth”:
    “If fifteen years are enough to transform victims into executioners, then this
    behavior is not more than a matter of opportunity and occasion. Anybody, at any
    time, may equally find himself victim or executioner.” 20

### Misc

    The central tenet of counterinsurgency theory is that populations—originally
    colonial populations, but now all populations, including our own—are made up
    of a small active minority of insurgents, a small group of those opposed to the
    insurgency, and a large passive majority that can be swayed one way or the other.
    The principal objective of counterinsurgency is to gain the allegiance of that
    passive majority. And its defining feature is that counterinsurgency is not just a
    military strategy, but more importantly a political technique. Warfare, it turns
    out, is political.

    On the basis of these tenets, counterinsurgency theorists developed and
    refined over several decades three core strategies. First, obtain total information:
    every communication, all personal data, all metadata of everyone in the
    population must be collected and analyzed. Not just the active minority, but
    everyone in the population. Total information awareness is necessary to
    distinguish between friend and foe, and then to cull the dangerous minority from
    the docile majority. Second, eradicate the active minority: once the dangerous
    minority has been identified, it must be separated from the general population

    [...]

    and eliminated by any means possible—it must be isolated, contained, and
    ultimately eradicated. Third, gain the allegiance of the general population:
    everything must be done to win the hearts and minds of the passive majority. It is
    their allegiance and loyalty, and passivity in the end, that matter most.
    Counterinsurgency warfare has become our new governing paradigm in the

    [...]

    imagination. It drives our foreign affairs and now our domestic policy as well.
    But it was not always that way. For most of the twentieth century, we
    governed ourselves differently in the United States: our political imagination
    was dominated by the massive battlefields of the Marne, of Verdun, by the
    Blitzkrieg and the fire-bombing of Dresden—and by the use of the atomic bomb.

    [...]

    warfare.
    Yet the transition from large-scale battlefield warfare to anticolonial struggles
    and the Cold War in the 1950s, and to the war against terrorism since 9/11, has
    brought about a historic transformation in our political imagination and in the
    way that we govern ourselves. In contrast to the earlier sweeping military
    paradigm, we now engage in surgical microstrategies of counterinsurgency
    abroad and at home. This style of warfare—the very opposite of large-scale
    battlefield wars like World War I or II—involves total surveillance, surgical
    operations, targeted strikes to eliminate small enclaves, psychological tactics,
    and political techniques to gain the trust of the people. The primary target is no
    longer a regular army, so much as it is the entire population. It involves a new

    [...]

    The result is radical. We are now witnessing the triumph of a counterinsurgency
    model of government on American soil in the absence of an insurgency, or
    uprising, or revolution. The perfected logic of counterinsurgency now applies
    regardless of whether there is a domestic insurrection. We now face a
    counterinsurgency without insurgency. A counterrevolution without revolution.
    The pure form of counterrevolution, without a revolution, as a simple modality
    of governing at home—what could be called “The Counterrevolution.”
    Counterinsurgency practices were already being deployed domestically in the

    [...]

    new internal enemies. It is vital that we come to grips with this new mode of
    governing and recognize its unique dangers, that we see the increasingly
    widespread domestication of counterinsurgency strategies and the new
    technologies of digital surveillance, drones, and hypermilitarized police for what
    they are: a counterrevolution without a revolution. We are facing something
    radical, new, and dangerous. It has been long in the making, historically. It is
    time to identify and expose it.


    [...]

    so on. I argued that we have become an “expository society” where we
    increasingly exhibit ourselves online, and in the process, freely give away our
    most personal and private data. No longer an Orwellian or a panoptic society
    characterized by a powerful central government forcibly surveilling its citizens
    from on high, ours is fueled by our own pleasures, proclivities, joys, and
    narcissism. And even when we try to resist these temptations, we have
    practically no choice but to use the Internet and shed our digital traces.
    I had not fully grasped, though, the relation of our new expository society to

    [...]

    strikes, indefinite detention, or our new hypermilitarized police force at home.
    But as the fog lifts from 9/11, the full picture becomes clear. The expository
    society is merely the first prong of The Counterrevolution. And only by tying
    together our digital exposure with our new mode of counterinsurgency
    governance can we begin to grasp the whole architecture of our contemporary
    political condition. And only by grasping the full implications of this new mode
    of governing—The Counterrevolution—will we be able to effectively resist it
    and overcome.

    [...]

    approach targeting small revolutionary insurgencies and what were mostly
    Communist uprisings. Variously called “unconventional,” “antiguerrilla” or
    “counterguerrilla,” “irregular,” “sublimited,” “counterrevolutionary,” or simply
    “modern” warfare, this burgeoning domain of military strategy flourished during
    France’s wars in Indochina and Algeria, Britain’s wars in Malaya and Palestine,
    and America’s war in Vietnam. It too was nourished by the RAND Corporation,
    which was one of the first to see the potential of what the French commander
    Roger Trinquier called “modern warfare” or the “French view of
    counterinsurgency.” It offered, in the words of one of its leading students, the
    historian Peter Paret, a vital counterweight “at the opposite end of the spectrum
    from rockets and the hydrogen bomb.” 2
    Like nuclear-weapon strategy, the counterinsurgency model grew out of a


    [...]

    from rockets and the hydrogen bomb.” 2
    Like nuclear-weapon strategy, the counterinsurgency model grew out of a
    combination of strategic game theory and systems theorizing; but unlike nuclear
    strategy, which was primarily a response to the Soviet Union, it developed more
    in response to another formidable game theorist, Mao Zedong. The formative
    moment for counterinsurgency theory was not the nuclear confrontation that
    characterized the Cuban Missile Crisis, but the earlier Chinese Civil War that led
    to Mao’s victory in 1949—essentially, when Mao turned guerrilla tactics into a
    revolutionary war that overthrew a political regime. The central methods and
    practices of counterinsurgency warfare were honed in response to Mao’s
    strategies and the ensuing anticolonial struggles in Southeast Asia, the Middle
    East, and North Africa that imitated Mao’s approach. 3 Those struggles for
    independence were the breeding soil for the development and perfection of
    unconventional warfare.
    By the turn of the twentieth century, when President George W. Bush would

    [...]

    T HE COUNTERINSURGENCY MODEL CAN BE TRACED BACK through several different
    genealogies. One leads to British colonial rule in India and Southeast Asia, to the
    insurgencies there, and to the eventual British redeployment and modernization
    of counterinsurgency strategies in Northern Ireland and Britain at the height of
    the Irish Republican Army’s independence struggles. This first genealogy draws
    heavily on the writings of the British counterinsurgency theorist Sir Robert
    Thompson, the chief architect of Great Britain’s antiguerrilla strategies in
    Malaya from 1948 to 1959. Another genealogy traces back to the American
    colonial experience in the Philippines at the beginning of the twentieth century.
    Others lead back to Trotsky and Lenin in Russia, to Lawrence of Arabia during
    the Arab Revolt, or even to the Spanish uprising against Napoleon—all
    mentioned, at least briefly, in General Petraeus’s counterinsurgency field
    manual. Alternative genealogies reach back to the political theories of
    Montesquieu or John Stuart Mill, while some go even further to antiquity and to
    the works of Polybius, Herodotus, and Tacitus. 1
    But the most direct antecedent of counterinsurgency warfare as embraced by
    the United States after 9/11 was the French military response in the late 1950s
    and 1960s to the anticolonial wars in Indochina and Algeria. This genealogy
    passes through three important figures—the historian Peter Paret and the French
    commanders David Galula and Roger Trinquier—and, through them, it traces
    back to Mao Zedong. It is Mao’s idea of the political nature of
    counterinsurgency that would prove so influential in the United States. Mao
    politicized warfare in a manner that would come back to haunt us today. The
    French connection also laid the seeds of a tension between brutality and legality
    that would plague counterinsurgency practices to the present—at least, until the
    United States discovered, or rediscovered, a way to resolve the tension by
    legalizing the brutality.


    [...]

    A founding principle of revolutionary insurgency—what Paret referred to as
    “the principal lesson” that Mao taught—was that “an inferior force could
    outpoint a modern army so long as it succeeded in gaining at least the tacit
    support of the population in the contested area.” 4 The core idea was that the
    military battle was less decisive than the political struggle over the loyalty and
    allegiance of the masses: the war is fought over the population or, in Mao’s
    words, “The army cannot exist without the people.” 5
    As a result of this interdependence, the insurgents had to treat the general
    population well to gain its support. On this basis Mao formulated early on, in
    1928, his “Eight Points of Attention” for army personnel:

    1. Talk to people politely.
    2. Observe fair dealing in all business transactions.
    3. Return everything borrowed from the people.
    4. Pay for anything damaged.
    5. Do not beat or scold the people.
    6. Do not damage crops.
    7. Do not molest women.
    8. Do not ill-treat prisoners-of-war. 6

    Two other principles were central to Mao’s revolutionary doctrine: first, the
    importance of having a unified political and military power structure that
    consolidated, in the same hands, political and military considerations; and
    second, the importance of psychological warfare. More specifically, as Paret
    explained, “proper psychological measures could create and maintain ideological
    cohesion among fighters and their civilian supporters.” 7
    Revolutionary warfare, in Paret’s view, boiled down to a simple equation:

    [...]

    the population.” 10
    Of course, neither Paret nor other strategists were so naïve as to think that
    Mao invented guerrilla warfare. Paret spent much of his research tracing the
    antecedents and earlier experiments with insurgent and counterinsurgency
    warfare. “Civilians taking up arms and fighting as irregulars are as old as war,”
    Paret emphasized. Caesar had to deal with them in Gaul and Germania, the
    British in the American colonies or in South Africa with the Boers, Napoleon in
    Spain, and on and on. In fact, as Paret stressed, the very term “guerrilla”
    originated in the Spanish peasant resistance to Napoleon after the Spanish
    monarchy had fallen between 1808 and 1813. Paret developed case studies of the

    [...]

    But for purposes of describing the “guerre révolutionnaire” of the 1960s, the
    most pertinent and timely objects of study were Mao Zedong and the Chinese
    revolution. And on the basis of that particular conception of revolutionary war,
    Paret set forth a model of counterrevolutionary warfare. Drawing principally on
    French military practitioners and theorists, Paret delineated a three-pronged
    strategy focused on a mixture of intelligence gathering, psychological warfare on
    both the population and the subversives, and severe treatment of the rebels. In
    Guerrillas in the 1960’s, Paret reduced the tasks of “counterguerrilla action” to
    the following:
    1. The military defeat of the guerrilla forces.
    2. The separation of the guerrilla from the population.
    3. The reestablishment of governmental authority and the development of a
    viable social order. 12

    [...]

    interact.” 13
    So the central task, according to Paret, was to attack the rebel’s popular
    support so that he would “lose his hold over the people, and be isolated from
    them.” There were different ways to accomplish this, from widely publicized
    military defeats and sophisticated psychological warfare to the resettlement of
    populations—in addition to other more coercive measures. But one rose above
    the others for Paret: to encourage the people to form progovernment militias and
    fight against the guerrillas. This approach had the most potential, Paret observes:
    “Once a substantial number of members of a community commit violence on


    [...]

    In sum, the French model of
    counterrevolutionary warfare, in Paret’s view, had to be understood as the
    inverse of revolutionary warfare.


    [...]

    The main sources for Paret’s synthesis were the writings and practices of French
    commanders on the ground, especially Roger Trinquier and David Galula,
    though there were others as well. 15 Trinquier, one of the first French
    commanders to theorize modern warfare based on his firsthand experience, had a


    [...]

    persisting in repeating its efforts.” Trinquier argues that this new form of modern
    warfare called for “an interlocking system of actions—political, economic,
    psychological, military,” grounded on “Countrywide Intelligence.” As Trinquier
    emphasizes, “since modern warfare asserts its presence on the totality of the
    population, we have to be everywhere informed.” Informed, in order to know and
    target the population and wipe out the insurgency. 17
    The other leading counterinsurgency theorist, also with deep firsthand


    [...]

    time.’” 19
    From Mao, Galula drew the central lesson that societies were divided into
    three groups and that the key to victory was to isolate and eradicate the active
    minority in order to gain the allegiance of the masses. Galula emphasizes in
    Counterinsurgency Warfare that the central strategy of counterinsurgency theory
    “simply expresses the basic tenet of the exercise of political power”:
    In any situation, whatever the cause, there will be an active minority for the cause, a neutral
    majority, and an active minority against the cause.

    [...]

    time.’” 19
    From Mao, Galula drew the central lesson that societies were divided into
    three groups and that the key to victory was to isolate and eradicate the active
    minority in order to gain the allegiance of the masses. Galula emphasizes in
    Counterinsurgency Warfare that the central strategy of counterinsurgency theory
    “simply expresses the basic tenet of the exercise of political power”:
    In any situation, whatever the cause, there will be an active minority for the cause, a neutral
    majority, and an active minority against the cause.

    The technique of power consists in relying on the favorable minority in order to rally the neutral
    majority and to neutralize or eliminate the hostile minority.20
    The battle was over the general population, Galula emphasized in his
    Counterinsurgency Warfare, and this tenet represented the key political
    dimension of a new warfare strategy.

    [...]

    US general David Petraeus picked up right where David Galula and Peter Paret
    left off. Widely recognized as the leading American thinker and practitioner of
    counterinsurgency theory—eventually responsible for all coalition troops in Iraq
    and the architect of the troop surge of 2007—General Petraeus would refine

    [...]

    On this political foundation, General Petraeus’s manual establishes three key
    pillars—what might be called counterinsurgency’s core principles.
    The first is that the most important struggle is over the population. In a short
    set of guidelines that accompanies his field manual, General Petraeus
    emphasizes: “The decisive terrain is the human terrain. The people are the center

    [...]

    The main battle, then, is over the populace.
    The second principle is that the allegiance of the masses can only be secured
    by separating the small revolutionary minority from the passive majority, and by
    isolating, containing, and ultimately eliminating the active minority. In his
    accompanying guidelines, General Petraeus emphasizes: “Seek out and eliminate
    those who threaten the population. Don’t let them intimidate the innocent. Target
    the whole network, not just individuals.” 25
    The third core principle is that success turns on collecting information on
    everyone in the population. Total information is essential to properly distinguish
    friend from foe and then extract the revolutionary minority. It is intelligence—
    total information awareness—that renders the counterinsurgency possible. It is

    [...]

    paraphrasing the French commander, underscores the primacy of political factors
    in counterinsurgency. “General Chang Ting-chen of Mao Zedong’s central
    committee once stated that revolutionary war was 80 percent political action and
    only 20 percent military,” the manual reads. Then it warns: “At the beginning of
    a COIN operation, military actions may appear predominant as security forces
    conduct operations to secure the populace and kill or capture insurgents;
    however, political objectives must guide the military’s approach.” 27
    Chapter Two opens with an epigraph from David Galula’s book: “Essential

    [...]

    General David Petraeus learned, but more importantly popularized, Mao
    Zedong’s central lesson: counterinsurgency warfare is political. It is a strategy
    for winning over the people. It is a strategy for governing. And it is quite telling
    that a work so indebted to Mao and midcentury French colonial thinkers would
    become so influential post-9/11. Petraeus’s manual contained a roadmap for a
    new paradigm of governing. As the fog lifts from 9/11, it is becoming
    increasingly clear what lasting impact Mao had on our government of self and
    others today.

    [...]

    D eveloped by military commanders and strategists over decades of anticolonial
    wars, counterinsurgency warfare was refined, deployed, and tested in the years
    following 9/11. Since then, the modern warfare paradigm has been distilled into
    a concise three-pronged strategy:
    1. Bulk-collect all intelligence about everyone in the population—every
    piece of data and metadata available. (total information awareness)

    [...]

    2. Identify and eradicate the revolutionary minority. Total information about
    everyone makes it possible to discriminate between friend and foe. Once
    suspicion attaches, individuals must be treated severely to extract all
    possible information, with enhanced interrogation techniques if
    necessary; and if they are revealed to belong to the active minority, they
    must be disposed of through detention, rendition, deportation, or drone
    strike—in other words, targeted assassination. Unlike conventional
    soldiers from the past, these insurgents are dangerous because of their

    [...]

    3. Pacify the masses. The population must be distracted, entertained,
    satisfied, occupied, and most importantly, neutralized, or deradicalized if
    necessary, in order to ensure that the vast preponderance of ordinary
    individuals remain just that—ordinary. This third prong reflects the
    “population-centric” dimension of counterinsurgency theory. Remember,
    in this new way of seeing, the population is the battlefield. Its hearts and
    minds must be assured. In the digital age, this can be achieved, first, by
    targeting enhanced content (such as sermons by moderate imams) to
    deradicalize susceptible persons—in other words, by deploying new
    digital techniques of psychological warfare and propaganda. Second, by
    providing just the bare minimum in terms of welfare and humanitarian
    assistance—like rebuilding schools, distributing some cash, and
    bolstering certain government institutions. As General Petraeus’s field

### Torture and surveilance

    T HE ATTACK ON THE W ORLD T RADE C ENTER SHOWED THE weakness of American
    intelligence gathering. Top secret information obtained by one agency was
    silo’ed from others, making it impossible to aggregate intelligence and obtain a
    full picture of the security threats. The CIA knew that two of the 9/11 hijackers
    were on American soil in San Diego, but didn’t share the information with the
    FBI, who were actively trying to track them down. 1 September 11 was a
    crippling intelligence failure, and in the immediacy of that failure many in
    President George W. Bush’s administration felt the need to do something radical.
    Greater sharing of intelligence, naturally. But much more as well. Two main
    solutions were devised, or revived: total surveillance and tortured interrogations.
    They represent the first prong of the counterinsurgency approach.
    In effect, 9/11 set the stage both for total NSA surveillance and torture as
    forms of total information awareness. The former functioned at the most virtual
    or ethereal, or “digital” level, by creating the material for data-mining and
    analysis. The latter operated at the most bodily or physical, or “analog” level,
    obtaining information directly from suspects and detainees in Iraq, Pakistan,
    Afghanistan, and elsewhere. But both satisfied the same goal: total information
    awareness, the first tactic of counterinsurgency warfare.

Census

    What is clear, though—as I document in Exposed—is that the myriad NSA,
    FBI, CIA, and allied intelligence agencies produce total information, the first
    and most important prong of the counterinsurgency paradigm. Most important,
    because both of the other prongs depend on it. As the RAND Corporation notes
    in its lengthy 519-page report on the current state of counterinsurgency theory
    and practice, “Effective governance depends on knowing the population,
    demographically and individually.” The RAND report reminds us that this
    insight is not novel or new. The report then returns, pointedly for us, to Algeria
    and the French commander, David Galula: “Galula, in Counterinsurgency
    Warfare, argued that ‘control of the population begins with a thorough census.
    Every inhabitant must be registered and given a foolproof identity card.’” 5

    [...]

    Today, that identity card is an IP address, a mobile phone, a digital device, facial
    recognition, and all our digital stamps. These new digital technologies have
    made everyone virtually transparent. And with our new ethos of selfies, tweets,
    Facebook, and Internet surfing, everyone is now exposed.

Enhanced interrogation:

    Second, tortured interrogation. The dual personality of counterinsurgency
    warfare is nowhere more evident than in the intensive use of torture for
    information gathering by the United States immediately after 9/11. Fulfilling the
    first task of counterinsurgency theory—total surveillance—this practice married
    the most extreme form of brutality associated with modern warfare to the
    formality of legal process and the rule of law. The combination of inhumanity
    and legality was spectacular.
    In the days following 9/11, many in the Bush administration felt there was
    only one immediate way to address the information shortfall, namely, to engage
    in “enhanced interrogation” of captured suspected terrorists—another
    euphemism for torture. Of course, torture of captured suspects would not fix the
    problem of silo’ed information, but they thought it would at least provide
    immediate information of any pending attacks. One could say that the United
    States turned to torture because many in the administration believed the country
    did not have adequate intelligence capabilities, lacking the spy network or even
    the language abilities to infiltrate and conduct regular espionage on
    organizations like Al Qaeda. 6
    The tortured interrogations combined the extremes of brutality with the

Getting information or "truth" was not the only, perhaps not the main point
of torture sessions, and maybe not as well the main point for mass surveillance:

    Even the more ordinary instances of “enhanced interrogation” were
    harrowing—and so often administered, according to the Senate report, after the
    interrogators believed there was no more information to be had, sometimes even
    before the detainee had the opportunity to speak.

Torture template:

    Ramzi bin al-Shibh was subjected to this type of treatment immediately upon
    arrival in detention, even before being interrogated or given an opportunity to
    cooperate—in what would become a “template” for other detainees. Bin al-
    Shibh was subjected first to “sensory dislocation” including “shaving bin al-
    Shibh’s head and face, exposing him to loud noise in a white room with white
    lights, keeping him ‘unclothed and subjected to uncomfortably cool
    temperatures,’ and shackling him ‘hand and foot with arms outstretched over his
    head (with his feet firmly on the floor and not allowed to support his weight with
    his arms).’” Following that, the interrogation would include “attention grasp,
    walling, the facial hold, the facial slap… the abdominal slap, cramped
    confinement, wall standing, stress positions, sleep deprivation beyond 72 hours,
    and the waterboard, as appropriate to [bin al-Shibh’s] level of resistance.” 8 This
    template would be used on others—and served as a warning to all.
    The more extreme forms of torture were also accompanied by the promise of

"Bigeard shrimp":

    The more extreme forms of torture were also accompanied by the promise of
    life-long solitary confinement or, in the case of death, cremation.
    Counterinsurgency torture in the past had often been linked to summary
    disappearances and executions. Under the Bush administration, it was tied to
    what one might call virtual disappearances.
    During the Algerian war, as noted already, the widespread use of brutal
    interrogation techniques meant that those who had been victimized—both the
    guilty and innocent—became dangerous in the eyes of the French military
    leadership. FLN members needed to be silenced, forever; but so did others who
    might be radicalized by the waterboarding or gégène. In Algeria, a simple
    solution was devised: the tortured would be thrown out from helicopters into the
    Mediterranean. They became les crevettes de Bigeard, after the notorious French
    general in Algeria, Marcel Bigeard: “Bigeard’s shrimp,” dumped into the sea,
    their feet in poured concrete—a technique the French military had apparently
    experimented with earlier in Indochina.

    [...]

    The CIA would devise a different solution in 2002: either torture the suspect
    accidentally to death and then cremate his body to avoid detection, or torture the
    suspect to the extreme and then ensure that he would never again talk to another
    human being. Abu Zubaydah received the latter treatment. Zubaydah had first
    been seized and interrogated at length by the FBI, had provided useful
    information, and was placed in isolation for forty-seven days, the FBI believing
    that he had no more valuable information. Then the CIA took over, believing he
    might still be a source. 10 The CIA turned to its more extreme forms of torture—
    utilizing all ten of its most brutal techniques—but, as a CIA cable from the
    interrogation team, dated July 15, 2002, records, they realized beforehand that it
    would either have to cover up the torture if death ensued or ensure that
    Zubaydah would never talk to another human being again in his lifetime.
    According to the Senate report, “the cable stated that if Abu Zubaydah were to
    die during the interrogation, he would be cremated. The interrogation team
    closed the cable by stating: ‘regardless which [disposition] option we follow
    however, and especially in light of the planned psychological pressure
    techniques to be implemented, we need to get reasonable assurances that [Abu
    Zubaydah] will remain in isolation and incommunicado for the remainder of his
    life.’” 11 In response to this request for assurance, a cable from the CIA station
    gave the interrogation team those assurances, noting that “it was correct in its
    ‘understanding that the interrogation process takes precedence over preventative
    medical procedures,’” and then adding in the cable:

KUBARK

    routines were approved at the uppermost level of the US government, by the
    president of the United States and his closest advisers. These practices were put
    in place, designed carefully and legally—very legalistically, in fact—to be used
    on suspected enemies. They were not an aberration. There are, to be sure, long
    histories written of rogue intelligence services using unauthorized techniques;
    there is a lengthy record, as well, of CIA ingenuity and creativity in this domain,
    including, among other examples, the 1963 KUBARK Counterintelligence
    Interrogation manual. 13 But after 9/11, the blueprint was drawn at the White
    House and the Pentagon, and it became official US policy—deliberate, debated,
    well-thought-out, and adopted as legal measures.


    [...]

    The Janus face of torture was its formal legality amidst its shocking brutality.
    Many of the country’s best lawyers and legal scholars, professors at top-ranked
    law schools, top government attorneys, and later federal judges would pore over
    statutes and case law to find legal maneuvers to permit torture. The felt need to
    legitimate and legalize the brutality—and of course, to protect the officials and
    operatives from later litigation—was remarkable.
    The documents known collectively as the “torture memos” fell into two
    categories: first, those legal memos regarding whether the Guantánamo detainees
    were entitled to POW status under the Geneva Conventions (GPW), written
    between September 25, 2001, and August 1, 2002; and second, starting in
    August 2002, the legal memos regarding whether the “enhanced interrogation
    techniques” envisaged by the CIA amounted to torture prohibited under
    international law.

How torture was defined to allow torture to happen:

    As Jay Bybee, then at the Office of Legal Counsel and now a
    federal judge, wrote in his August 1, 2002, memo:

        We conclude that torture as defined in and proscribed by [18 US Code] Sections 2340-2340A,
        covers only extreme acts. Severe pain is generally of the kind difficult for the victim to endure.
        Where the pain is physical, it must be of an intensity akin to that which accompanies serious
        physical injury such as death or organ failure. Severe mental pain requires suffering not just at the
        moment of infliction but also requires lasting psychological harm, such as seen in mental disorders
        like post-traumatic stress disorder. […] Because the acts inflicting torture are extreme, there is
        significant range of acts that though they might constitute cruel, inhuman, or degrading treatment
        or punishment fail to rise to the level of torture.22

    This definition of torture was so demanding that it excluded the brutal
    practices that the United States was using. It set the federal legal standard,
    essentially, at death or organ failure.

    [...]

    them 26 —and then, effectively, judicial opinions. The executive branch became a
    minijudiciary, with no effective oversight or judicial review. And in the end, it
    worked. The men who wrote these memos have never been prosecuted nor
    seriously taken to task, as a legal matter, for their actions. The American people
    allowed a quasi-judiciary to function autonomously, during and after. These self-
    appointed judges wrote the legal briefs, rendered judgment, and wrote the
    judicial opinions that legitimized these brutal counterinsurgency practices. In the
    process, they rendered the counterinsurgency fully legal. They inscribed torture
    within the fabric of law.

    One could go further. The torture memos accomplished a new resolution of
    the tension between brutality and legality, one that we had not witnessed
    previously in history. It was an audacious quasi-judicial legality that had rarely
    been seen before. And by legalizing torture in that way, the Bush administration
    provided a legal infrastructure for counterinsurgency-as-governance more
    broadly.

    [...]

    And through this process of legalization, these broader torturous practices
    spilled over into the second prong of counterinsurgency: the eradication of an
    active minority. Torture began to function as a way to isolate, punish, and
    eliminate those suspected of being insurgents.

Bare existence, indefinite detention, incommunication:

    The indefinite detention and brutal ordinary measures served as a way to
    eliminate these men—captured in the field or traded for reward monies, almost
    like slaves from yonder. The incommunicado confinement itself satisfied the
    second prong of counterinsurgency theory. 5 But somehow it also reached further
    than mere detention, approximating a form of disappearance or virtual death.
    The conditions these men found themselves in were so extreme, it is almost as if
    they were as good as dead.
    Reading Slahi’s numbing descriptions, one cannot help but agree with the
    philosopher Giorgio Agamben that these men at Guantánamo were, in his words,
    no more than “bare life.” 6 Agamben’s concept of bare existence captures well
    the dimensions of dehumanization and degradation that characterized their lives:
    the camp inmates were reduced to nothing more than bare animal existence.
    They were no longer human, but things that lived. The indefinite detention and
    torture at Guantánamo achieved an utter denial of their humanity.
    Every aspect of their treatment at black sites and detention facilities

### Drone strikes

    This debate between more population-centric proponents and more enemy-
    centric advocates of counterinsurgency should sound familiar. It replays the
    controversy over the use of torture or other contested methods within the
    counterinsurgency paradigm. It replicates the strategic debates between the
    ruthless and the more decent. It rehearses the tensions between Roger Trinquier
    and David Galula.

    Yet just as torture is central to certain versions of modern warfare, the drone
    strike too is just as important to certain variations of the counterinsurgency
    approach. Drone strikes, in effect, can serve practically all the functions of the
    second prong of counterinsurgency warfare. Drone strikes eliminate the
    identified active minority. They instill terror among everyone living near the
    active minority, dissuading them and anyone else who might contemplate joining
    the revolutionaries. They project power and infinite capability. They show who
    has technological superiority. As one Air Force officer says, “The real advantage
    of unmanned aerial systems is that they allow you to project power without
    projecting vulnerability.” 18 By terrifying and projecting power, drones dissuade
    the population from joining the insurgents.

    [...]

    Covered extensively by the news media,
    drone attacks are popularly believed to have caused even more civilian casualties
    than is actually the case. The persistence of these attacks on Pakistani territory
    offends people’s deepest sensibilities, alienates them from their government, and
    contributes to Pakistan’s instability.” 19
    In July 2016, the Obama administration released a report estimating the


    [...]

    Those in the affected countries typically receive far higher casualty reports.
    The Pakistan press, for instance, reported that there are about 50 civilians killed
    for every militant assassinated, resulting in a hit rate of about 2 percent. As
    Kilcullen and Exum argue, regardless of the exact number, “every one of these
    dead noncombatants represents an alienated family, a new desire for revenge,
    and more recruits for a militant movement that has grown exponentially even as
    drone strikes have increased.” 25
    To those living in Afghanistan, Iraq, Pakistan, Somalia, Yemen, and
    neighboring countries, the Predator drones are terrifying. But again—and this is
    precisely the central tension at the heart of counterinsurgency theory—the terror
    may be a productive tool for modern warfare. It may dissuade people from
    joining the active minority. It may convince some insurgents to abandon their
    efforts. Terror, as we have seen, is by no means antithetical to the
    counterinsurgency paradigm. Some would argue it is a necessary means.
    Drones are by no means a flawless weapons system even for their proponents.

    [...]

    Regarding the first question, a drone should be understood as a blended
    weapons system, one that ultimately functions at several levels. It shares
    characteristics of the German V-2 missile, to be sure, but also the French
    guillotine and American lethal injection. It combines safety for the attacker, with
    relatively precise but rapid death, and a certain anesthetizing effect—as well as,
    of course, utter terror. For the country administering the drone attack, it is
    perfectly secure. There is no risk of domestic casualties. In its rapid and
    apparently surgical death, it can be portrayed, like the guillotine, as almost
    humane. And drones have had a numbing effect on popular opinion precisely
    because of their purported precision and hygiene—like lethal injection has done,
    for the most part, in the death-penalty context. Plus, drones are practically
    invisible and out of sight—again, for the country using them—though, again,
    terrifying for the targeted communities.

    [...]

    Chamayou’s second question is, perhaps, the most important. This new
    weapons system has changed the US government’s relationship to its own
    citizens. There is no better evidence of this than the deliberate, targeted drone
    killing of US and allied nation citizens abroad—as we will see. 32

    [...]

    An analogy from the death penalty may be helpful. There too, the means
    employed affect the ethical dimensions of the practice itself. The gas chamber
    and the electric chair—both used in the United States even after the Holocaust—
    became fraught with meaning. Their symbolism soured public opinion on the
    death penalty. By contrast, the clinical or medical nature of lethal injection at
    first reduced the political controversy surrounding executions. Only over time,
    with botched lethal injections and questions surrounding the drug cocktails and
    their true effects, have there been more questions raised. But it has taken time for
    the negative publicity to catch up with lethal injection. Drones, at this point,
    remain far less fraught than conventional targeted assassinations.

### Winning hearts and minds

    THE THIRD PRONG OF COUNTERINSURGENCY THEORY CONSISTS in winning the
    hearts and minds of the general population to stem the flow of new recruits to
    the active minority and to seize the upper hand in the struggle. This goal can be
    achieved by actively winning the allegiance of the population, or by pacifying an
    already passive population, or even simply by distracting the masses. The bar,
    ultimately, is low since, on the counterinsurgency view, the people are mostly
    passive. As Roger Trinquier noted in 1961, “Experience has demonstrated that it
    is by no means necessary to enjoy the sympathy of the majority of the people to
    obtain their backing; most are amorphous, indifferent.” Or, as General Petraeus’s
    manual states, the vast majority is “neutral” and “passive”; it represents an
    “uncommitted middle” with “passive supporters of both sides.” 1 The third prong,
    then, is aimed mostly at assuaging, pacifying perhaps, or merely distracting the
    indifferent masses.
    
    [...] the third prong has translated, principally, into three tactics: investments in
    infrastructure, new forms of digital propaganda, and generalized terror. [...]
    Undergirding them both, though, is the third tactic, the threat of
    generalized terror, that serves as a foundational method and looming constant.

    [...]

    In How Everything Became War and the Military Became Everything, Rosa
    Brooks writes that since 9/11 we have witnessed the expansion of the military
    and its encroachment on civilian affairs. “We’ve seen,” in her words, “the steady
    militarization of US foreign policy as our military has been assigned many of the
    tasks once given to civilian institutions.” Brooks warns us of a new world where
    “the boundaries between war and nonwar, military and nonmilitary have
    eroded.”

    [...]

    We are indeed facing, as Brooks powerfully demonstrates, a new world of an
    ever-encroaching military. But what this reveals, more than anything, is the rise
    of the counterinsurgency paradigm of government. It is the model of
    counterinsurgency warfare—of Galula’s early turn to building schools and health
    facilities, to focusing on the hearts and minds of the general population—that
    has pushed the military into these traditionally civilian domains, including total
    surveillance, rule-of-law projects, artificial intelligence, entertainment, etc. In
    effect, it is the counterinsurgency paradigm of government that has become
    everything, and everything that has become counterinsurgency. The blurring of
    boundaries between war and peaceful governance is not merely the contingent
    result of 9/11, it is instead the culmination of a long and deliberate process of
    modernizing warfare.

Providing the basic needs:

    Providing basic necessities, labeled “essential services” in the field manual, is
    a key counterinsurgency practice. It consists primarily of ensuring that there is
    “food, water, clothing, shelter, and medical treatment” for the general
    population. General Petraeus’s manual explains the rationale in very simple
    terms: “People pursue essential needs until they are met, at any cost and from
    any source. People support the source that meets their needs. If it is an insurgent
    source, the population is likely to support the insurgency. If the [host nation]
    government provides reliable essential services, the population is more likely to
    support it. Commanders therefore identify who provides essential services to
    each group within the population.” 5

That, in most cases, involve funneling american taxpayer's money to enrich corporations
with "insane profit margins" for rebuilding countries along with US guidelines. See
Naomi Klein's Shock Doctrine for more details.

    A second approach to securing the neutrality of the majority is more
    psychological. In the early days of modern warfare, examples of this approach
    included measures such as the resettlement of populations, in the words of
    counterinsurgency experts, “to control them better and to block the insurgents’
    support.” This is what the British did in Malaya, and the French in Algeria.
    Other examples included basic propaganda campaigns. 16

    As time has gone by, new digital technologies have enabled new forms of
    psychological counterinsurgency warfare. One of the newest involves digital
    propaganda, reflected most recently in the Center for Global Engagement set up
    under the Obama administration in early 2016. Created with the objective to
    prevent the radicalization of vulnerable youth, the center adopted strategies
    pioneered by the giants of Silicon Valley—Google, Amazon, Netflix—and was
    originally funded at the level of about $20 million. It targeted susceptible
    persons suspected of easier radicalization and sent them enhanced and improved
    third-party content in order to try to dissuade them, subliminally, from
    radicalizing or joining ISIS. In the words of an investigative journalist, “The
    Obama administration is launching a stealth anti-Islamic State messaging
    campaign, delivered by proxies and targeted to individual would-be extremists,
    the same way Amazon or Google sends you shopping suggestions based on your
    online browsing history.” 17

Terror e tortura:

    The third set of measures was even more basic: terror. The most formidable way
    to win hearts and minds is to terrorize the local population to make sure they do
    not sympathize with or aid the active minority.

    [...]

    The brutality of counterinsurgency serves, of course, to gather information
    and eradicate the revolutionary minority. But it also aims higher and reaches
    further: its ambition, as General Aussaresses recognized well, is to terrorize the
    insurgents, to scare them to death, and to frighten the local population in order to
    prevent them from joining the insurgent faction. Today, the use of unusually
    brutal torture, the targeted drone assassination of high-value suspects, and the
    indefinite detention under solitary conditions aim not only to eviscerate the
    enemy, but also to warn others, strike fear, and win their submission and
    obedience. Drones and indefinite detention crush those they touch, and strike
    [...] Terror, in the end, is a key component of the third core strategy of
    counterinsurgency.

Torture and civilization:

    Since antiquity, terror has served to demarcate the civilized from the
    barbarian, to distinguish the free citizen from the enslaved. The free male in
    ancient Greece had the privilege of swearing an oath to the gods, of testifying on
    his word. The slave, by contrast, could only give testimony under torture.
    Torture, in this sense, defined freedom and citizenship by demeaning and
    marking—by imposing stigmata—on those who could be tortured. It served to
    demarcate the weak. It marked the vulnerable. And it also, paradoxically, served
    to delineate the “more civilized.” This is perhaps the greatest paradox of the
    brutality of counterinsurgency: to be civilized is to torture judiciously. This
    paradox was born in antiquity, but it journeys on.

    [...]

    The judicious administration of terror is the hallmark of civilization. To be
    civilized is to terrorize properly, judiciously, with restraint, according to the
    rules. Only the barbarians tortured savagely, viciously, unrestrainedly. The
    civilized, by contrast, knew how and when to tame torture, how to rein it terror,
    to apply it with judgment and discretion. Compared to the barbarians—the
    beheadings of ISIS is a modern case on point—we are tame and judicious, even
    when we torture, not like those barbarians. And since 9/11, the judicious use of
    terror has been a key US strategy. In the end, terror functions in myriad ways to
    win the hearts and minds of the masses under the counterinsurgency paradigm of
    governing.

Terror in many levels of governing:

    Now, terror is not an unprecedented component of governing, even if its role
    in the counterinsurgency model may be uniquely constitutive. It has been with us
    since slavery in antiquity, through the many inquisitions, to the internment and
    concentration camps of modern history. And there too, in each of its
    manifestations, it functioned at multiple levels to bolster different modes of
    governing. Looking back through history, terror has done a lot of work. Today as
    well. And to see all that terror achieves today—above and beyond the three
    prongs of counterinsurgency theory—it is useful to look back through history
    and recall its different functions and the work it has done. The reflections today
    are stunning.

Torture and truth:

    The first episode reaches back to antiquity, but represents a recurring theme
    throughout history: terror has often served to manufacture its own truth—
    especially in terms of its efficacy. “They all talk.” [...]

    [...]

    Trying to convince a suspect that he will talk, telling him that he will—this is,
    of course, a psychological technique, but it is more than that. It is also a firm
    belief of counterinsurgency theorists outside the interrogation room. Roger

    [...]

    Manufacturing truth: that is, perhaps, the first major function of terror. It is
    the power of terror, especially in the face of ordinary men and women, of
    humans, all too human. It has been that way since the inquisitions of the Middle
    Ages, and before, since antiquity. On this score, little has changed.
    In her book on slavery in Greek antiquity, Torture and Truth, Page duBois
    argues that the idea of truth dominant today in Western thought is indissolubly
    tied to the practice of torture, while torture itself is deeply connected to the will
    to discover something that is always beyond our grasp. As a result, society after
    society returns to torture, in almost an eternal recurrence, to seek out the truth
    that is always beyond our reach. In ancient times, duBois shows, torture
    functioned as the metaphorical touchstone of truth and as a means to establish a
    social hierarchy. In duBois’s words, “the desire to create an other and the desire
    to extract truth are inseparable, in that the other, because she or he is an other, is
    constituted as a source of truth.” Truth, in sum, is always “inextricably linked
    with the practice of torture.” 4

    [...]

    Truth, duBois argues, “resides in the slave body.” 5

    [...]

    Even more, terror produced social difference and hierarchy. The limits on
    torture in ancient societies served to define what it meant to be among those who
    could be tortured—what it meant to be a slave or to be free. In ancient times, the
    testimony of a slave could only be elicited, and only became admissible in
    litigation, under torture. Only free male citizens could take an oath or resolve a
    controversy by sermon. The rules about who could be tortured in ancient times
    did not just regulate the victims of torture, the rules themselves were constitutive
    of what it meant to be a slave. The laws demarcated and defined freedom itself
    —what it looked like, what it entailed.
    Sophocles’s tragedy Oedipus Rex has captured our imagination for centuries
    on questions of fate and power. But it is perhaps on the question of terror and
    truth that the play turns. At the climax of Sophocles’s tragedy—at the pivotal
    moment when truth finally emerges for all to see and to recognize—there is a
    scene of terror. The shepherd slave who held the knowledge of Oedipus’s
    ancestry is threatened with torture. And that threat of torture alone—at the
    culmination of a whole series of unsuccessful inquiries—produces the truth:
    torture provokes the shepherd’s confession and that allows Oedipus to recognize
    his fate. But more than that, torture reaffirms the social order in Thebes—a
    social order where gods rule, oracles tell truth, prophets divine, fateful kings
    govern, and slaves serve. It is, ultimately, the right to terrorize that reveals
    Oedipus’s power and the shepherd’s place in society.

    [...]

    In a similar way, terror today produces its own truth—about the effectiveness
    of torture in eliciting truth, about its effectiveness in subjugating the insurgents,
    about the justness of counterinsurgency.

    [...]

    Second, terror—or more specifically the regulatory framework that surrounds
    terror—legitimizes the practices of terror itself. This may sound paradoxical or
    circular—but it has often been true in history. The structures that frame and
    regulate the administration of terrorizing practices have the effect, unexpectedly,
    [...] The extreme nature of torture, once brought within the fabric of the law,
    concentrated power in the hands of those
    who had the knowledge and skill, the techne, to master the brutality. The
    Justinian codification served as a model to later codifications during the early
    Middle Ages and to the practices of the Inquisition.
    Extreme practices call for expert oversight and enable a concentration of

    [...]

    Torture was brought into the fabric of the law and rarified at the
    same time. The rarefication in the Medieval Period served a political end: to
    make torture even more foreboding. Had torture become too generalized or too
    frequent, it might have lost its exceptionality and terrorizing effect.
    Torture was rarely applied, and, as one historian notes, inflicted with “the

    [...]

    The rarity achieved by the limited use and legal regulation of torture in the
    Medieval Period served to ensure its persistence and role as a social
    epistemological device—as a producer of truths, especially truth about itself.
    Centuries later, the Bush administration and its top lawyers re-created a legal
    architecture surrounding the use of torture.

    [...]

    Third, the legal regulation of terror also legitimizes the larger political regime.

    [...]

    It may seem surprising or paradoxical that the antebellum courts would
    protect a slave accused of poisoning her master. But there is an explanation: the
    intricate legal framework surrounding the criminalization and punishment of
    errant slaves during the antebellum period served to maintain and stabilize
    chattel slavery in the South—it served to equilibrate the political economy of
    slavery. It served to balance interests in such a way that neither the slave owners
    nor the slaves would push the whole system of slavery into disarray. And the
    courts and politicians carefully handled this delicate balance.

    [...]

    In fact, the financial loss associated with the execution of a slave was viewed
    as the only way to guarantee that owners made sure their slaves received a fair
    trial. During the 1842–1843 legislative session, the general assembly passed a

    [...]

    These complex negotiations over the criminal rules accompanied the
    practices of slavery in Alabama—a form of terror—and served to legitimize the
    larger political economy of chattel slavery. They offered stability to the slave
    economy by making the different participants in the criminal process and in
    slavery—the slave owners, the foremen, the magistrates, and the public at large
    —more confident in the whole enterprise. The extensive legal regulation of the
    torture of slaves was not about justifying torture, nor about resolving
    philosophical or ethical questions. Instead, it served to strike a balance and
    stabilize the institution of slavery.

    [...]

    Fourth, the ability to terrorize—and to get away with it—has a powerful effect
    on others. The audacity and the mastery impress the general masses. Something
    about winning or beating others seduces the population. People like winners, and
    winning is inscribed in terrorizing others.

Masculinity:

    Fifth, and relatedly, terror is gendered, which also tends to reinforce the power
    and appeal of the more brutal counterinsurgency practices. Brutality is most
    often associated with the dominant half of the couple, the one who controls, and
    however much we might protest, this tends to strengthen the attraction.

Horrorism:

    Terror works in other ways as well, and many other historical episodes could
    shed light on the complex functioning of terror today—of what Adriana
    Cavarero refers to as “horrorism.” 45 Terror, for instance, operates to control and
    manage one’s comrades. It can serve to keep the counterrevolutionary minority
    in check. The willingness to engage in extreme forms of brutality, in senseless
    violence, in irrational excess signals one’s own ruthlessness to one’s peers or
    inferiors. It can frighten and discipline both inferiors and superiors. It
    demonstrates one’s willingness to be cruel—which can be productive, in fact
    necessary, to a counterinsurgency.

Counterinsurgency goes domestic:

    The operations of COINTELPRO—the Counter Intelligence Program
    developed by the FBI in the 1950s to disrupt the American Communist Party,
    and extended into the 1960s to eradicate the Black Panthers—took precisely the
    form of counterinsurgency warfare. The notorious August 1967 directive of FBI
    director J. Edgar Hoover to “expose, disrupt, misdirect, discredit, or otherwise
    neutralize the activities of black nationalist, hate-type organizations and
    groupings, their leadership, spokesmen, membership, and supporters”; 16 the
    police raids on Black Panther headquarters in 1968 and 1969; the summary
    execution of the charismatic chairman of the Chicago Black Panther Party, Fred
    Hampton; the first SWAT operations carried out against the Panthers in Los
    Angeles—these all had the trappings of modern warfare.

    Hoover’s FBI targeted the Panthers in a manner that drew on the foundational
    principles of counterinsurgency: first, to collect as much intelligence on the
    Black Panther Party as possible through the use of FBI informants and total
    surveillance; second, to isolate the Panthers from their communities by making
    their lives individually so burdened with surveillance and so difficult that they
    were forced to separate themselves from their friends and family members; third,
    to turn the Panther movement into one that was perceived, by the general
    population, as a radicalized extremist organization, as a way to delegitimize the
    Panthers and reduce their appeal and influence; and ultimately, to eliminate and
    eradicate them, initially through police arrests, then through criminal
    prosecutions (for instance, of the New York 21) and justified homicides [...]
    and ultimately by fomenting conflict and divisiveness within the party

    [...]

    The linchpin of a domesticated counterinsurgency is to bring total
    information awareness home. Just as it was developed abroad, it is total
    surveillance alone that makes it possible to distinguish the active minority on
    domestic soil from the passive masses of Americans. A fully transparent
    population is the first requisite of the counterinsurgency method. In General
    Petraeus’s field manual, it received a full chapter early on, “Intelligence in
    Counterinsurgency,” with a pithy and poignant epigraph: “Everything good that
    happens seems to come from good intelligence.” And as the manual began, so it
    ended, with the following simple mantra: “The ultimate success or failure of the
    [counterinsurgency] mission depends on the effectiveness of the intelligence effort."

    [...]

    American is a potential insurgent.
    Constant vigilance of the American population is necessary—hand in hand
    with the appearance of trust. Appearances are vital. A domesticated
    counterinsurgency must suspect everyone in the population, but not let it be
    known. This posture, developed in counterinsurgency theory decades ago, was at
    the core of the paradigm. David Galula had refined it to a witty statement he
    would tell his soldiers in Algeria: “One cannot catch a fly with vinegar. My rules
    are: outwardly you must treat every civilian as a friend; inwardly you must
    consider him as a rebel ally until you have positive proof to the contrary.” 2 This
    mantra has become the rule today—at home.

    [...]

    In Exposed, I proposed a new way to understand how power circulates in the
    digital age and, especially, a new way to comprehend our willingness to expose
    ourselves to private corporations and the government alike. The metaphors
    commonly used to describe our digital condition, such as the “surveillance
    state,” Michel Foucault’s panopticon prison, or even George Orwell’s Big
    Brother, are inadequate, I argued there. In the new digital age we are not forcibly
    imprisoned in panoptic cells. There is no “telescreen” anchored to the wall of our
    apartments by the state. No one is trying to crush our passions, or wear us down
    into submission with the smell of boiled cabbage and old rag mats, coarse soap,
    and blunt razors. The goal is not to displace our pleasures with hatred—with
    “hate” sessions, “hate songs,” “hate weeks.” Today, instead, we interact by
    means of “likes,” “shares,” “favorites,” “friending,” and “following.” We
    gleefully hang smart TVs on the wall that record everything we say and all our
    preferences. The drab uniforms and grim grayness of Orwell’s 1984 have been
    replaced by the iPhone 5c in its radiant pink, yellow, blue, and green. “Colorful
    through and through,” its marketing slogan promises, and the desire for color-
    filled objects—for the sensual swoosh of a sent e-mail, the seductive click of the
    iPhone camera “shutter,” and the “likes,” clicks, and hearts that can be earned by
    sharing—seduce us into delivering ourselves to the surveillance technologies.
    And as the monitoring and marketing of our private lives changes who we
    sharing—seduce us into delivering ourselves to the surveillance technologies.

    And as the monitoring and marketing of our private lives changes who we
    are, power circulates in a new way. Orwell depicted the perfect totalitarian
    society. Guy Debord described ours rather as a society of the spectacle, in which
    the image makers shape how we understand the world and ourselves. Michel
    Foucault spoke instead of “the punitive society” or what he called
    “panopticism,” drawing on Jeremy Bentham’s design of the panoptic prison.
    Gilles Deleuze went somewhat further and described what he called “societies of
    control.” But in our digital age, total surveillance has become inextricably linked
    with pleasure. We live in a society of exposure and exhibition, an expository
    society.

    [...]

    And that’s what happened: taxpayers would pay the telecoms to hold the data
    for the government. So, before, AT&T surreptitiously provided our private
    personal digital data to the intelligence services free of charge. Now, American
    taxpayers will pay them to collect and hold on to the data for when the
    intelligence services need them. A neoliberal win-win solution for everyone—
    except, of course, the ordinary, tax-paying citizen who wants a modicum of
    privacy or protection from the counterinsurgency.

    [...]

    In my previous book, however, I failed to fully grasp how our expository
    society fits with the other features of our contemporary political condition—
    from torture, to Guantánamo, to drone strikes, to digital propaganda. In part, I
    could not get past the sharp contrast between the fluidity of our digital surfing
    and surveillance on the one hand, and the physicality of our military
    interventions and use of torture on the other. To be sure, I recognized the deadly
    reach of metadata and reiterated those ominous words of General Michael
    Hayden, former director of both the NSA and the CIA: “We kill people based on
    metadata.” 20 And I traced the haunting convergence of our digital existence and
    of correctional supervision: the way in which the Apple Watch begins to
    function like an electronic bracelet, seamlessly caging us into a steel mesh of
    digital traces. But I was incapable then of fully understanding the bond between
    digital exposure and analog torture.

    It is now clear, though, that the expository society fits seamlessly within our
    new paradigm of governing. The expository society is precisely what allows the
    counterinsurgency strategies to be applied so impeccably “at home” to the very
    people who invented modern warfare. The advent of the expository society, as
    well as the specific NSA surveillance programs, makes domestic total
    information awareness possible, and in turn lays the groundwork for the other
    two prongs of counterinsurgency in the domestic context.

    [...]

    This idea of an occupied territory, of a colony within a nation, resonates
    perfectly with what we have witnessed in terms of the domestication of the
    counterinsurgency. I would just push the logic further: we have not simply
    created an internal colony, we have turned the nation itself into a colony. We
    govern ourselves through modern counterinsurgency warfare as if the entire
    United States was now a colonial dominion like Algeria, Malaya, or Vietnam.

    [...]

    These incidents—large and small, but all devastating for those targeted—also
    serve another objective of the domesticated counterinsurgency: to make the rest
    of us feel safe and secure, to allow us to continue our lives unaffected, to avoid
    disrupting our consumption and enjoyment. They serve to reassure, and also, in
    demonizing a phantom minority, to bring us all together against the specter of
    the frightening and dangerous other. It makes us believe that there would be,
    lurking in the quiet suburbs of Dallas or Miami, dangerous insurgents—were it
    not for our government. And these effects feed into the third prong of a

    [...]

    We had seen earlier, within counterinsurgency theory, similar debates
    between population-centric and enemy-centric theorists. The enemy-centric
    approach tended to be the more brutal, but more focused. The population-centric
    favored the more legal and social-investment approaches. I argued then that they
    were just two facets of the same paradigm.

    Here the debate is between population-and/or-enemy-centric theories versus
    individual-centric theory. But here too, I would argue, this is a false dichotomy.
    Again, these are just two facets of the same thing: a counterinsurgency paradigm
    of warfare with three core strategies. Like the population-and/or-enemy-centric
    theories, individual-centric theory naturally entails both incapacitating the
    individual terrorist or insurgent—eliminating him and all of the active minority
    —and preventing or deterring his substitution or replacement.

    [...]

    But rather than buy into this dichotomy of counterinsurgency and leaner
    antiterrorism, what history shows instead is a growing convergence of the two
    models in the United States since the 1960s. Counterinsurgency and domestic
    antiterrorism efforts, entwined from the start, have converged over time. The
    individual incapacitation strategy meshes perfectly into the counterinsurgency
    approach. And it leads seamlessly from the domestication of the second prong of
    counterinsurgency to the domestication of the third.

### Distraction and diversion

    MANY OF US WILL NOT RECOGNIZE OURSELVES, OR A MERICA for that matter, in
    these dreadful episodes—in the waterboarding and targeted assassinations
    abroad or in the militarization of our police forces, in the infiltration of Muslim
    mosques and student groups or in the constant collection of our personal data at
    home. Many of us have no firsthand experience of these terrifying practices. Few
    of us actually read the full Senate torture report, and even fewer track drone
    strikes. Some of us do not even want to know of their existence. Most of us are
    blissfully ignorant—at least most of the time—of these counterinsurgency
    practices at home or abroad, and are consumed instead by the seductive
    distractions of our digital age.

    And that’s the way it is supposed to be. As counterinsurgency is
    domesticated, it is our hearts and minds that are daily being assuaged, numbed,
    pacified—and blissfully satisfied. We, the vast majority of us, are reassured
    daily: there are threats everywhere and color-coded terror alerts, but
    counterinsurgency strategies are protecting us. We are made to feel that
    everything’s under control, that the threat is exterior, that we can continue with
    our daily existence. Even more, that these counterinsurgency strategies will
    prevail. That our government is stronger and better equipped, prepared to do
    everything necessary to win, and will win. That the guardians are protecting us.
    The effort to win the hearts and minds of the passive American majority is
    the third aspect of the domestication of counterinsurgency practices—perhaps
    the most crucial component of all. And it is accomplished through a remarkable
    mixture of distraction, entertainment, pleasure, propaganda, and advertising—
    now rendered all so much more effective thanks to our rich digital world. In
    Rome, after the Republic, this was known as “bread and circus” for the masses.
    Today, it’s more like Facebook and Pokémon GO.

    We saw earlier how the expository society entices us to share all our personal
    data and how this feeds into the first prong of counterinsurgency—total
    information awareness. There is a flip side to this phenomenon: keeping us
    distracted. The exposure is so pleasurable and engaging that we are mostly kept
    content, with little need for a coordinated top-down effort to do so. We are
    entranced—absorbed in a fantastic world of digitally enhanced reality that is
    totally consuming, engrossing, and captivating. We are no longer being rendered
    docile in a disciplinarian way, as Michel Foucault argued in Discipline and
    Punish. We are past notions of docility. We are actively entranced—not
    passively, not in a docile way. We are actively clicking and swiping, jumping
    from one screen to another, checking one platform then another to find the next
    fix—Facebook, Instagram, Twitter, Google, YouTube, and on and on.
    Winning over and assuaging the passive majority might be accomplished—
    indeed, has been accomplished in the past—through traditional propaganda, such
    as broadcast misinformation about the insurgent minority, and through the top-
    down provision of entertainment to keep us from thinking about politics. The
    new digital world we live in has rendered these older strategies obsolete. As the
    counterinsurgency’s mandate to pacify the masses has been turned on the
    American people, the third prong of modern warfare looks and works differently
    than it did in previous times and in other places.
    Things have changed. Just a few years ago, our politicians still had to tell us

    [...]

    Pokémon GO has already run its course, but that is to be expected. Another
    digital obsession will follow. These platforms are supposed to capture all of our
    attention for a while, to captivate us, to distract us—and simultaneously to make
    us expose ourselves and everything around us. This is the symbiosis between the
    third and first prongs of the domesticated counterinsurgency: while it pacifies us,
    a game like Pokémon GO taps into all our personal information and captures all
    our data. At first, the game required that players share all their personal contacts.
    Although that was eventually dropped, the game collects all our GPS locations,
    captures all the video of our surroundings in perfectly GPS-coded data, and
    tracks us wherever we are. Plus, even though it is free, many players are buying
    add-ons and in the process sharing their consumption and financial data. The
    more we play, the more we are distracted and pacified, and the more we reveal
    about ourselves.

    [...]

    The distractions are everywhere: e-mail notifications, texts, bings and pings,
    new snapchats and instagrams. The entertainment is everywhere as well: free
    Wi-Fi at Starbucks and McDonald’s, and now on New York City streets, that
    allow us to stream music videos and watch YouTube videos. And of course, the
    advertising is everywhere, trying to make us consume more, buy online,
    subscribe, and believe. Believe not only that we need to buy the recommended
    book or watch the suggested Netflix, but also believe that we are secure and safe,
    protected by the most powerful intelligence agencies and most tenacious military
    force. Believe that we can continue to mind our own business—and remain
    distracted and absorbed in the digital world—because our government is
    watching out for us.

    The fact is, the domestication of counterinsurgency has coincided with the
    explosion of this digital world and its distractions. There is a real qualitative
    difference between the immediate post–9/11 period and today. One that is
    feeding directly into the third strategy of modern warfare.
    Meanwhile, for the more vulnerable—those who are more likely to veer
    astray and perhaps sympathize with the purported internal enemy—the same
    digital technologies target them for enhanced propaganda. The Global
    Engagement Center, or its equivalents, will profile them and send improved
    content from more moderate voices. The very same methods developed by the
    most tech-savvy retailers and digital advertisers—by Google and Amazon—are
    deployed to predict, identify, enhance, and target our own citizens.
    were before or that we are experiencing a waning of civil and political
    engagement. While I agree that the growing capacity of the state and
    corporations to monitor citizens may well threaten the private sphere, I am not
    convinced that this is producing new apathy or passivity or docility among
    citizens, so much as a new form of entrancement. The point is, we were once
    kept apathetic through other means, but are now kept apathetic through digital
    distractions.

Voting turnout and Trump election:

    The voting patterns of American registered voters has remained constant—
    and apathetic—for at least fifty years. Even in the most important presidential
    elections, voter turnout in this country over the past fifty years or more has
    pretty much fluctuated between 50 percent and 63 percent. By any measure,
    American democracy has been pretty docile for a long time. In fact, if you look
    over the longer term, turnout has been essentially constant since the 1920s and
    the extension of the suffrage to women. Of course, turnout to vote is not the only
    measure of democratic participation, but it is one quantifiable measure. And
    electoral voting is one of the more reliable longitudinal measures of civic
    participation. But our record, in the United States, is not impressive.

    [...]

    Despite all this, over 62 million people voted for Donald Trump, resulting in
    his Electoral College victory. And it was by no means an unusual election. Voter
    turnout in 2016 was typical for this country. About 60.2 percent of the
    approximately 231 million eligible voters turned out to vote, representing about
    139 million votes case. That number is consonant with historical turnout in this
    country, almost squarely between voter turnout in 2012 (58.6 percent) and in
    2008 (61.6 percent), but still above most presidential election year turnouts since
    1972. 16 In all categories of white voters, Trump prevailed.

    [...]

    The cable news network CNN captured this best in a pithy lead to a story titled
    “Trump: The Social Media President?”: “FDR was the first ‘radio’ president.
    JFK emerged as the first ‘television’ president. Barack Obama broke through as
    the first ‘Internet’ president. Next up? Prepare to meet Donald Trump, possibly
    the first ‘social media’ and ‘reality TV’ president.” 10

    [...]

    This new mode of existence and digital consumption pleases and distracts the
    majority of Americans. The old-fashioned TV has now been enhanced and
    augmented, displaced by social media on digital devices of all sorts and sizes—
    from the Apple Watch and tablet, through the MacBook Air and Mac Pro, to the
    giant screen TV and even the Jumbotron. And all of it serves to pacify the
    masses and ensure that they do not have the time or attention span to question
    the domestication of the counterinsurgency.

    And, then, it all feeds back into total information awareness. Hand in hand,
    government agencies, social media, Silicon Valley, and large retailers and
    corporations have created a mesmerizing new digital age that simultaneously
    makes us expose ourselves and everything we do to government surveillance and
    that serves to distract and entertain us. All kinds of social media and reality TV
    consume and divert our attention, making us give our data away for free. A
    profusion of addictive digital platforms—from Gmail, Facebook, and Twitter, to
    YouTube and Netflix, Amazon Prime, Instagram, and Snapchat, and now
    Pokémon GO—distract us into exposing all our most private information, in
    order to feed the new algorithms of commerce and intelligence services: to
    profile us for both watch lists and commercial advertising.

This is compatible with Shoshana Zuboff's Dispossession Cycle:

    This third aspect of counterinsurgency’s domestication is perhaps the most
    important, because it targets the most prized military and political objective: the
    general masses. And today, in the expository society, the new algorithms and
    digital-advertising methods have propelled the manipulation and propaganda to
    new heights. We are being encouraged by government and enticed by
    multination corporations and social media to expose and express ourselves as
    much as possible, leaving digital traces that permit both government and
    corporations to profile us and then try to shape us accordingly. To make model
    citizens out of us all—which means docile, entranced consumers. The governing
    paradigm here is to frenetically encourage digital activity—which in one sense is
    the opposite of docility—in order to then channel that activity in the right
    direction: consumption, political passivity, and avoiding the radical extremes.

    What we are witnessing is a new form of digital entrancement that shapes us
    as subjects, blunts our criticality, distracts us, and pacifies us. We spend so much
    time on our phones and devices, we barely have any time left for school or work,
    let alone political activism. In the end, the proper way to think about this all is
    not through the lens of docility, but through the framework of entrancement. It is
    crucial to understand this in the proper way, because breaking this very
    entrancement is key to seeing how counterinsurgency governance operates more
    broadly. Also, because the focus on docility—along an older register of
    discipline—is likely to lead us into an outdated focus on top-down propaganda.

### Counterrevolution

    The paradigm was refined
    and systematized, and has now reached a new stage: the complete and systematic
    domestication of counterinsurgency against a home population where there is no
    real insurgency or active minority. This new stage is what I call “The
    Counterrevolution.”

    The Counterrevolution is a new paradigm of governing our own citizens at
    home, modeled on colonial counterinsurgency warfare, despite the absence of
    any domestic uprising. It is aimed not against a rebel minority—since none
    really exists in the United States—but instead it creates the illusion of an active
    minority which it can then deploy to target particular groups and communities,
    and govern the entire American population on the basis of a counterinsurgency
    warfare model. It operates through the three main strategies at the heart of
    modern warfare, which, as applied to the American people, can be recapitulated
    as follows:

    1. Total information awareness of the entire American population…: [by the]
    [...] “counterrevolutionary minority.”

    [...]

    2. … in order to extract an active minority at home…

Shock and Awe:

    3. … and win the hearts and minds of Americans: Meanwhile, the
    counterrevolutionary minority works to pacify and assuage the general
    population in order to ensure that the vast majority of Americans remain
    just that: ordinary consuming Americans. They encourage and promote a
    rich new digital environment filled with YouTube, Netflix, Amazon
    Prime, tweets, Facebook posts, instagrams, snapchats, and reality TV that
    consume attention while digitally gathering personal data—and at times,
    pushing enhanced content. They direct digital propaganda to susceptible
    users. And they shock and awe the masses with their willingness to
    torture suspected terrorists or kill their own citizens abroad. In the end,
    entertaining, distracting, entrancing, and assuaging the general population
    is the key to success—our new form of bread and circus.

The "new shape" of the State (and it's partners), as a "loose network":

    These three key strategies now guide governance at home, as they do military
    and foreign affairs abroad. What has emerged today is a new and different art of
    governing. It forms a coherent whole with, at its center, a security apparatus
    composed of White House, Pentagon, and intelligence officials, high-ranking
    congressional members, FISC judges, security and Internet leaders, police
    intelligence divisions, social-media companies, Silicon Valley executives, and
    multinational corporations. This loose network, which collaborates at times and
    competes at others, exerts control by collecting and mining our digital data. Data
    control has become the primary battlefield, and data, the primary resource—
    perhaps the most important primary resource in the United States today.

    [...]

    This new mode of governing has no time horizon. It has no sunset provision. And it is
    marked by a tyrannous logic of violence. [...] It is part and parcel of the new
    paradigm of governing that reconciles brutality with legality.

The unprecedented, self-fulfilling profecy:

    We govern ourselves
    differently in the United States now: no longer through sweeping social
    programs like the New Deal or the War on Poverty, but through surgical
    counterinsurgency strategies against a phantom opponent. The intensity of the
    domestication now is unprecedented.

    [...]

    Counterinsurgency, with its tripartite scheme (active minority, passive masses,
    counterrevolutionary minority) and its tripartite strategy (total awareness,
    eliminate the active minority, pacify the masses) is a deeply counterproductive
    self-fulfilling prophecy that radicalizes individuals against the United States.

    [...]

    “The Islamic State has called it ‘the blessed ban’ because it
    supports the Islamic State’s position that America hates Islam. The clause in the
    order that gives Christians preferential treatment will be seen as confirming the
    Islamic State’s apocalyptic narrative that Islam is in a fight to the death against
    the Christian crusaders. The images of Muslim visitors being turned away at
    American airports will only inflame those who seek to do us harm.” 6

    [...]

    We are headed not, as Kant would have it, toward perpetual peace, but
    instead, sounding the refrain of Nietzsche’s eternal return, toward an endless
    state of counterinsurgency warfare.

### Not exactly a state of exception, but of legality

    MANY COMMENTATORS ARGUE THAT WE NOW LIVE, IN THE United States and in the
    West more broadly, in a “state of exception” characterized by suspended legality.
    In this view, our political leaders have placed a temporary hold on the rule of
    law, with the tacit understanding that they will resume their adherence to liberal
    legal values when the political situation stabilizes. Some commentators go
    further, arguing that we have now entered a “permanent state of exception.”

    This view, however, misperceives one particular tactic of counterinsurgency
    —namely, the state of emergency—for the broader rationality of our new
    political regime. It fails to capture the larger ambition of our new mode of
    governing. The fact is, our government does everything possible to legalize its
    counterinsurgency measures and to place them solidly within the rule of law—
    through endless consultations with government lawyers, hypertechnical legal
    arguments, and lengthy legal memos. The idea is not to put law on hold, not
    even temporarily. It is not to create an exception, literally or figuratively. On the
    contrary, the central animating idea is to turn the counterinsurgency model into a
    fully legal strategy. So, the governing paradigm is not one of exceptionality, but
    of counterinsurgency and legality.

    [...]

    The logic today is based on a model of
    counterinsurgency warfare with, at its heart, the resolution of that central tension
    between brutality and legality. The counterrevolutionary model has resolved the
    inherited tension and legalized the brutality.

    [...]

    Agamben’s idea of a permanent state of exception pushes this
    further, but simultaneously undermines the defining element of the exception,
    since it becomes the rule. For the most part, though, the state of exception is
    presented as aberrational but temporary.

    [...]

    The problem with the state-of-exception view is that it mistakes tactics for
    the overarching logic of our new paradigm of governing and, in the process, fails
    to see the broader framework of The Counterrevolution. The state-of-exception
    framework rests on an illusory dichotomy between rule and exception, a myth
    that idealizes and reifies the rule of law. The point is, the use of torture at CIA
    black sites and the bulk collection of American telephony metadata were not
    exceptions to the rule of law, but were rendered fully legalized and regulated
    practices—firmly embedded in a web of legal memos, preauthorized formalities,
    and judicial or quasi-judicial oversight. In this sense, hardly anything that
    occurred was outside or exceptional to the law, or could not be brought back in.

    The Counterrevolution, unlike the state of exception, does not function on a
    binary logic of rule and exception, but on a fully coherent systematic logic of
    counterinsurgency that is pervasive, expansive, and permanent. It does not have
    limits or boundaries. It does not exist in a space outside the rule of law. It is all
    encompassing, systematic, and legalized.

    Of course, the rhetoric of “exception” is extremely useful to The
    Counterrevolution. “States of emergency” are often deployed to seize control
    over a crisis and to accelerate the three prongs of counterinsurgency.

    [...]

    The ultimate exercise of power, Foucault argued, is precisely to transform
    ambiguities about illegalisms into conduct that is “illegal.”

    [...]

    During the ancien régime, Foucault argues, the popular and the privileged
    classes worked together to evade royal regulations, fees, and impositions.
    Illegalisms were widespread throughout the eighteenth century and well
    distributed across the different strata of society

    [...]

    As wealth became increasingly mobile after the French Revolution, new
    forms of wealth accumulation—of moveable goods, stocks, and supplies as
    opposed to landed wealth—exposed massive amounts of chattel property to the
    workers who came in direct contact with this new commercial wealth. The
    accumulation of wealth began to make popular illegalisms less useful—even
    dangerous—to the interests of the privileged. The commercial class seized the
    mechanisms of criminal justice to put an end to these popular illegalisms
    [...] The privileged seized the administrative and
    police apparatus of the late eighteenth century to crack down on popular
    illegalisms.

    [...]

    They effectively turned popular illegalisms into
    illegalities, and, in the process, created the notion of the criminal as social
    enemy—Foucault even talks here of creating an “internal enemy.”

    [...]

    In The Counterrevolution—by contrast to the bourgeois revolutions of the
    early nineteenth century—the process is turned on its head. Illegalisms and
    illegalities are inverted. Rather than the privileged turning popular illegalisms
    into illegalities, the guardians are turning their own illegalisms into legalities.
    [...] The strategy here is to paper one’s way into the legal realm through elaborate
    memorandums and advice letters that justify the use of enhanced interrogation or the
    assassination of American citizens abroad.

    [...]

    On the one hand, there is a strict division of responsibilities: the intelligence
    agencies and the military determine all the facts outside the scope of the legal
    memorandum. [...] Everything is compartmentalized.

    [...]

    On the other hand, the memo authorizes: it allows the political authority to
    function within the bounds of the law. It sanitizes the political decision. It cleans
    the hands of the military and political leaders. It produces legalities.

A circular, feedback loop:

    None of this violates the rule of law or transgresses the boundaries of legal
    liberalism. Instead, the change was rendered “legal.” If this feels circular, it is
    because it is: there is a constant feedback effect in play here. The
    counterinsurgency practices were rendered legal, and simultaneously justice was
    made to conform to the counterinsurgency paradigm. The result of the feedback
    loop was constantly new and evolving meanings of due process. And however
    rogue they may feel, they had gone through the correct procedural steps of due
    process to render them fully lawful and fully compliant with the rule of law.

    [...]

    “Abnormal,” in 1975, Foucault explored how the clash between the juridical
    power to punish and the psychiatric thirst for knowledge produced new medical
    diagnoses that then did work.

    [...]

    In his 1978 lecture on the invention of the notion of dangerousness in French
    psychiatry, Foucault showed how the idea of future dangerousness emerged from
    the gaps and tensions in nineteenth-century law. 37

    [...]

    There are surely gaps here too in The Counterrevolution—tensions between
    rule-boundedness on the one hand and a violent warfare model on the other.
    Those tensions give momentum to the pendulum swings of brutality that are then
    resolved by bureaucratic legal memos.

### System analysis and Operations Research

    The RAND Corporation played a seminal role in the development of
    counterinsurgency practices in the United States and championed for decades—
    and still does—a systems-analytic approach that has come to dominate military
    strategy. Under its influence, The Counterrevolution has evolved into a logical
    and coherent system that regulates and adjusts itself, a fully reasoned and
    comprehensive approach.

    [...]

    Systems analysis was often confused with OR, but it was distinct in several
    regards. OR tended to have more elaborate mathematical models and solved
    lower-level problems; in systems analysis, by contrast, the pure mathematical
    computation was generally applied only to subparts of the overall problem.
    Moreover, SA took on larger strategic questions that implicated choices between
    major policy options. In this sense, SA was, from its inception, in the words of
    one study, “less quantitative in method and more oriented toward the analysis of
    broad strategic and policy questions, […] particularly […] seeking to clarify
    choice under conditions of great uncertainty.” 5

    [...]

    As this definition made clear, there were two meanings of the term system in
    systems analysis: first, there was the idea that the world is made up of systems,
    with internal objectives, that need to be analyzed separately from each other in
    order to maximize their efficiency. Along this first meaning, the analysis would
    focus on a particular figurative or metaphorical system—such as a weapons
    system, a social system, or, in the case of early counterinsurgency, a colonial
    system. Second, there was the notion of systematicity that involved a particular
    type of method—one that began by collecting a set of promising alternatives,
    constructing a model, and using a defined criterion.

    [...]

    This method of systems analysis became influential in government and
    eventually began to dominate governmental logics starting in 1961 when Robert
    McNamara acceded to the Pentagon under President John F. Kennedy.

    [...]

    According to its proponents, systems analysis
    would allow policy makers to put aside partisan politics, personal preferences,
    and subjective values. It would pave the way to objectivity and truth. As RAND
    expert and future secretary of defense James R. Schlesinger explained:
    “[Systems analysis] eliminates the purely subjective approach on the part of
    devotees of a program and forces them to change their lines of argument. They
    must talk about reality rather than morality.” 13 With systems analysis,
    Schlesinger argued, there was no longer any need for politics or value
    judgments. The right answer would emerge from the machine-model that
    independently evaluated cost and effectiveness. All that was needed was a
    narrow and precise objective and good criteria. The model would then spit out
    the most effective strategy.

    [...]

    Counterinsurgency theory blossomed at precisely the moment that systems
    analysis was, with RAND’s backing, gaining influence in the Pentagon and at
    the White House. The historian Peter Paret pinpoints this moment, in fact, to the
    very first year of the Kennedy administration: “In 1961, the Cuban revolution
    combined with the deteriorating Western position in Southeast Asia

    [...]

    It convened, as mentioned earlier, the seminal
    counterinsurgency symposium in April 1962, where RAND analysts discovered
    David Galula and commissioned him to write his memoirs. RAND would
    publish his memoirs as a confidential classified report in 1963 under the title

    [...]

    Counterinsurgency theory blossomed at precisely the moment that systems
    analysis was, with RAND’s backing, gaining influence in the Pentagon and at
    the White House. The historian Peter Paret pinpoints this moment, in fact, to the
    very first year of the Kennedy administration: “In 1961, the Cuban revolution
    combined with the deteriorating Western position in Southeast Asia to shift

    [...]

    One recent episode regarding interrogation
    methods is telling. It involved the evaluation of different tactics to obtain
    information from informants, ranging from truth serums to sensory overload to
    torture. These alternatives were apparently compared and evaluated using a SA
    approach at a workshop convened by RAND, the CIA, and the American
    Psychological Association (APA). Again, the details are difficult to ascertain
    fully, but the approach seemed highly systems-analytic.

    [...] a series of workshops on “The Science of Deception”

    [...]

    More specifically, according to this source, the workshops probed and
    compared different strategies to elicit information. The systems-analytic
    approach is reflected by the set of questions that the participants addressed: How
    important are differential power and status between witness and officer? What
    pharmacological agents are known to affect apparent truth-telling behavior?
    What are sensory overloads on the maintenance of deceptive behaviors? How
    might we overload the system or overwhelm the senses and see how it affects
    deceptive behaviors? These questions were approached from a range of
    disciplines. The workshops were attended by “research psychologists,
    psychiatrists, neurologists who study various aspects of deception and
    representatives from the CIA, FBI and Department of Defense with interests in
    intelligence operations. In addition, representatives from the White House Office
    of Science and Technology Policy and the Science and Technology Directorate
    of the Department of Homeland Security were present.” 31

    [...]

    And in effect, from a counterinsurgency perspective, these various tactics—
    truth serums, sensory overloads, torture—are simply promising alternatives that
    need to be studied, modeled, and compared to determine which ones are superior
    at achieving the objective of the security system. Nothing is off limits.
    Everything is fungible. The only question is systematic effectiveness. This is the
    systems-analytic approach: not piecemeal, but systematic.
    Incidentally, a few years later, Gerwehr apparently went to Guantánamo, but
    refused to participate in any interrogation because the CIA was not using video
    cameras to record the interrogations. Following that, in the fall of 2006 and in
    2007, Gerwehr made several calls to human-rights advocacy groups and
    reporters to discuss what he knew. A few months later, in 2008, Gerwehr died of
    a motorcycle accident on Sunset Boulevard. 32 He was forty years old.

    [...]

    Sometimes, depending on the practitioner, the analysis favored torture or summary
    execution; at other times, it leaned toward more “decent” tactics. But these
    variations must now be understood as internal to the system. Under President
    Bush’s administration, the emphasis was on torture, indefinite detention, and
    illicit eavesdropping; under President Obama’s, it was on drone strikes and total
    surveillance; in the first months of the Trump presidency, on special operations,
    drones, the Muslim ban, and building the wall. What unites these different
    strategies is counterinsurgency’s coherence as a system—a system in which
    brutal violence is heart and center. That violence is not aberrational or rogue. It
    is to be expected. It is internal to the system. Even torture and assassination are
    merely variations of the counterinsurgency logic.

    Counterinsurgency abroad and at home has been legalized and systematized. It
    has become our governing paradigm “in any situation,” and today “simply
    expresses the basic tenet of the exercise of political power.” It has no sunset
    provision. It is ruthless, game theoretic, systematic—and legal. And with all of
    the possible tactics at the government’s disposal—from total surveillance to
    indefinite detention and solitary confinement, to drones and robot-bombs, even
    to states of exception and emergency powers—this new mode of governing has
    never been more dangerous.

    In sum, The Counterrevolution is our new form of tyranny.
