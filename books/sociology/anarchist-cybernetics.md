[[!meta title="Anarchist Cybernetics"]]

* https://lra.le.ac.uk/bitstream/2381/37597/1/2016swanntrphd.pdf
* https://www.lboro.ac.uk/departments/phir/staff/thomas-swann/
* https://ethos.bl.uk/OrderDetails.do?uin=uk.bl.ethos.686573
* https://lra.le.ac.uk/handle/2381/37597
* https://www.dailykos.com/stories/2017/3/12/1642612/-Anti-Capitalist-Meetup-Swann-s-Way-anarchist-cybernetics-amp-organizational-dynamics-in-politics

## Excerpts

    goals. The metaphor of a dance with complexity echoes the way Ashby describes the
    process of control as being similar to a fencer facing an opponent. ‘(I)f a fencer faces an
    opponent who has various modes of attack available,’ he writes, ‘the fencer must be
    provided with at least an equal number of modes of defence’ ([1958] 2003, p. 356; see
    also Ashby, 1962)). Control and regulation are processes of responding to the
    unpredictable moves of another dancer (again, the complexity can be threatening or, in
    the case of a dance or in fencing, it can be more of a friendly game or a negotiation). 11
    Control is not something enacted by an entity, be it an individual or a group, over an

    [...]

    Self-organisation is a sticky concept for cybernetics. Ashby, for example, argues that it
    cannot exist. He bases his argument on the second-order cybernetic position that
    viability or effectiveness is always determined as such by an observer of a system. Self-
    organisation suggests that a system responds effectively by itself to complexity. Along
    these lines, self-organisation is not a property of the system itself but of the observation
    (1962; see also von Foerster, [1960] 2003; Duda, 2012, p. 89). While this may apply to
    mathematical, biological and engineering applications of cybernetics, it is
    fundamentally at odds with organisational cybernetics and the cybernetics of social and
    political organisation more generally. A more fitting account of self-organisation, one
    that is applicable to both the cybernetic need for control and the specific nature of social
    and political organisations, is the more common one of a group of people deciding
    amongst themselves, to achieve some goal(s). Stewart Umpleby (1987) highlights this
    simple definition by contrasting two situations:
    A teacher can organize a class into groups by assigning each child to a specific group and

    [...]

    One of the core ideas in cybernetic thought when it comes to communication is that of
    feedback. Beer in fact highlights feedback as ‘the most important concept of all’ ([1981]
    1994, p. 32). Feedback explains how information, 12 from the environment but also the
    internal workings of the organisation itself, plays a role in how the various parts of the
    organisation operate autonomously. Beer is keen to stress that feedback does not mean
    what it is commonly thought to mean, i.e. a response to something. Instead, feedback
    refers to the way in which information about the changes a part of an organisation or
    system faces are used to help that part maintain an agreed level of operation or to work
    towards an agreed goal. Information coming into an operating unit of an organisation or
    system about what is happening, both internally and externally, allows it to direct its

    [...]

    account of the free market as a tool for allowing order to emerge from chaos (Cooper,
    2011; see also Gilbert, 2005). Hayek was of course one of the key architects of the
    theories that supported neoliberalism and, in a sad irony, was involved in advising the
    dictatorship of Augusto Pinochet that had toppled the government of Salvador Allende
    in Chile that Beer had become so invested in (Harvey, 2005). While complexity has
    been discussed thus far in relation to the potential for self-organisation in a way that
    may well run parallel to radically political accounts (see also Maeckelbergh, 2009, pp.
    203-210; Purkis, 2004, pp. 51-52), it is important to note that there is a competing
    narrative around complexity theory, one that takes it in a dramatically different
    direction.
    Autonomy and ideas of self-organisation and horizontality, too, have been subject to

    [...]

    This initial affinity between anarchism and organisational cybernetics comes through in
    how Kropotkin characterises centralised, top-down forms of government as being not
    only politically and morally objectionable but at the same time ineffectual. Kropotkin
    writes (1927, pp. 76-7) that ‘in all production there arise daily thousands of difficulties
    which no government can solve or foresee.’ Against the plans of those socialists who
    wish to use the state to manage this complexity, ‘the governmentalists’ to use
    McEwan’s term, he argues that ‘production and exchange represented an undertaking so
    complicated that the plans of the state socialists, which lead inevitably to a party
    directorship, would prove to be absolutely ineffective as soon as they were applied to
    life.’ 21 As an alternative to centralised attempts at attenuating variety in society (which
    would lead to an oppression of individuals’ right to live their lives as they see fit) and
    amplifying variety in the state as an organising body (resulting in a massive
    bureaucracy), Kropotkin proposes that the workers themselves and their unions
    administer production in an autonomous manner. 22 As political science scholar Marius

    [...]

    22
    An alternative approach to anarchism and cybernetics that focuses on feedback loops can be found in
    Roel van Duijn’s Message of a Wise Kabouter (1972). In a conversation I had with van Duijn in 2013 he
    suggested that he had come to cybernetics as a result of discussions between himself and Murray
    Bookchin in the 1960s. Bookchin does use the term ‘cybernetics’ but does so to refer to high-technology
    and links it to a centralised, authoritarian corporate state (e.g. 1985), so it seems that he had not engaged
    with the cybernetics of Wiener and Beer. Van Duijn may have been put onto cybernetics through reading

    [...]

    John Duda (2013, p. 64) describes the approach to anarchism of McEwan as ‘a shift
    away from a moral vision of anarchism, outraged at the scandal of domination’ towards
    a paradigm focussed on the ‘superior productivity of anarchist organisational
    methodology’, but I would suggest that it in fact tries to show that anarchism trumps
    top-down government on both counts, without prioritising one over the other. Indeed,
    the fact that it is present in Kropotkin’s work supports the view that it is not a shift that
    took place in light of anarchist engagements with cybernetics but is a dual-perspective
    that is present in at least some forms of anarchism from relatively early in the canon.
    This could be thought of as ‘the two sides of the anarchist coin’. On the one hand there
    is the ethical and political concern for autonomy, while on the other there is the
    functional concern for effective organisation.
    The connections between anarchism and cybernetics were also picked up on by one of

    [...]

    While this is framed within a typically-hierarchical organisational structure with an
    executive and a receptionist, the point Pask is getting at is that the hierarchy is also one
    of levels of language. The highest level in the hierarchy involves a metalanguage that is
    used to talk about lower levels, which too have a metalanguage to talk about levels
    lower than them. Although this is clearly a hierarchy such that a level thrice removed
    from the top, for example, would have difficulty communicating directly with the top
    and vice versa given the difference in languages, Pask is very clear that this describes a

    [...]

    She goes so far as to point out that System Five in the VSM, the part of the organisation
    involved in defining the identity and overall goals of the organisation, can be ‘just a
    routine or an activity’ and ‘does not necessarily need to be an extra set of people’ (2013,
    p. 12; see also 1999).
    McEwan follows Pask’s account by making explicit the distinction between the two

    [...]

    command such that each level is subordinate to the levels above it and where the top
    level has overall control over decision making in the organisation. Functional
    hierarchy, however, applies to an organisation where ‘there are two or more levels of
    information structure operating in the system’ (McEwan, [1963] 1987, p. 44).

    [...]

    the working groups consider their activities and adjust them if necessary in line with the
    decided-upon goals of the organisation. Crucially, for an anarchist cybernetics and
    VSM, everyone involved in the working groups can, potentially, be involved in the
    General Assemblies and so in these System Three discussions. The same individuals
    step out of their functional role as working group members and into that of reflecting on
    their practice within working groups. System Four involves the same individuals again,
    and also in the General Assemblies, reflecting on the activities of the working groups
    and the organisation as a whole as well as its overall strategy in relation to events in the
    outside world. Adjustments to both tactics and strategy can be made in light of changes

    [...]

    Elsewhere, Bakunin similarly argues that ‘all organizations must proceed by way of
    federation from the base to the summit, from the commune to the coordinating
    association of the country or nation’ (1971b: 82-83, italics in original). Here we have a
    picture of a federated form of organisation in which smaller local organisations, free
    associations or cooperatives, link up with one another at the level of the commune.
    Communes then link up in a regional council and so on to the level of an international
    council. This too is reflective of the recursivity that is essential to the VSM: any viable
    system is itself a part of another viable system and has within it multiple viable systems.
    Not only does the decision-making structure of an anarchist federation relate favourably
    to that of organisational cybernetics, but so too does the very principle of federated
    organisation, something Wiener in fact hints at (1961, p. 155) and of which Beer is
    sometimes quite explicit in his support (see Medina, 2011, pp. 159-160).
    McEwan, in his Anarchy article, makes this link between this syndicalist model of

    [...]

    Importantly, this is not enough for an anarchist version of cybernetics. A more explicitly
    ethical and political autonomy, which values autonomy as an individual and collective
    good over-and-above its role within organisational structure, needs to be introduced.
    This brings into play a crucial distinction between the firm, as Beer conceives of it, and
    radical left and anarchist organisation. While the firm may be viable on cybernetic
    terms by including a functional autonomy, whereby individual operating units have
    some scope for self-organisation and independent decision making, it will be shown that
    radical left and anarchist organisation must combine this cybernetic demand with the
    ethical and political demand that values individual and collective autonomy in and of
    itself. The two sides of the anarchist coin, as I have described them above, must be
    brought together. For now it is enough to note that while Beer’s work focusses on the
    firm and, one can argue, takes that as a model of social organisation in general, an
    anarchist cybernetics highlights a distinction that must be made between the firm, which
    takes as a necessary condition autonomy as a function, and radical left and anarchist

    [...]

    Thinking about cybernetics and organisation along these lines, the work of science and
    technology studies scholar Andrew Pickering (2010) becomes extremely important.
    Rather than being focussed on representing an external reality with accuracy,
    Pickering’s cybernetics is instead involved in performance. Performance is understood
    as the actions we undertake in the world that demand a pragmatic and constructed
    knowledge as opposed to a detailed representation of reality. 24 He describes this as a
    ‘performative epistemology’: ‘a vision of knowledge as part of performance rather than
    24
    This owes something of a debt to the American Pragmatism of Pierce, Dewey and others, although this

    [...]

    as an external control of it’ (ibid., p. 25, italics in original). The cybernetician, therefore,
    is not engaged in unpacking and describing a reality (be it a machine, an animal, a
    human being or a social phenomenon) but in facilitating performances or practices that
    he or she is a part of. Cybernetics, in this regard, is committed to action and not simply
    theorising about the nature of knowledge. This is evident in the work of Beer who, as I
    have shown in the second chapter, was throughout his life heavily involved in practicing
    cybernetics. Pickering highlights the fact that the very definition of cybernetics as
    steering shows this connection with performance and practice (ibid., p. 30). It is a
    science not of representing the objective mechanisms of control and communication but
    of doing control and communication.
    Interestingly, this turn towards science as performance in fact brings to the fore another


    [...]

    He also speaks of the importance of ‘radical transformation and listening’ (2010, pp. 42-
    43). While many more could be added, these seven goals (mutual respect, cooperation,
    egalitarian decision-making, promotion of radical democratic vision, deconstruction of
    borders, radical transformation and listening) will serve to demonstrate how a virtuous
    anarchist mode of research can be conceived. In order to act virtuously as an anarchist, a
    researcher must act so as to embody these goals. The researcher should aim to: (1) be
    respectful of participants in research; (2) encourage cooperation in the research on the
    part of the participants; (3) engage in egalitarian relationships with the participants; (4)
    promote the radical democratic ideal of anarchism; (5) conduct the research in a
    borderless fashion; (6) be radically transformative (i.e. live as radically transformed);
    and (7) listen to participants. It should be noted that according to the prefigurative virtue
    ethics outlined by Franks, these virtues and goals are negotiable and specific to

    [...]

    While it is not the focus of cybernetics, such an ethical approach can also be seen in the
    work of von Foerster. In line with the second-order cybernetic concern for the
    researcher as ‘a person who considers oneself to be a participant actor in the drama of
    mutual interaction of the give and take in the circularity of human relations’ ([1991]
    2003, p. 289), von Foerster frames ethics as an understanding of the norms that govern
    the practices we engage in. Rather than seeing scientific research as a practice involving
    truth, von Foerster recasts it as involving trust, understanding, responsibility, reaching
    out for the other and ‘a conspiracy, whose customs, rules, and regulations we are now
    inventing’ (ibid., p. 294). 27 On this understanding, an ethics of research is
    fundamentally an ethics of co-producing knowledge and von Foerster’s account of
    second-order cybernetics links up well with the anarchist research ethics defined by

    [...]

    who wish to hinder and frustrate the movement with invaluable information about how
    practices are organised. Franks (1992) warns that ‘the social sciences are the third
    section of the intelligence gathering services. […] The state's liberal surveillance wing,
    sociology, informs on what working class people are thinking and doing.’ He goes on to
    say that sociologists aim to show ‘when working class people's actions and attitudes are
    showing signs of becoming a threat to the stability of [the ruling] class’s dominant
    position.’ This is a rather dramatic and perhaps unfair characterisation, but in general
    concerns related to research as surveillance are warranted. There is a serious risk

    [...]

    associated with anarchist research that detailed information about successful anarchist
    organising would be at the same time a guide to countering such organising. This is
    perhaps especially true in the case of the research carried out here as this could provide
    detailed information on the organisational dynamics of radical left groups and their
    communication practices, and would allow security services to disrupt organisation and
    communications at key points. In writing this thesis, care has therefore been taken to
    reduce the risk of it being useful to those wishing to disrupt the movement. I have
    allowed activists to review the transcripts of their interviews and highlight any details
    that they would prefer to not be included or that they would prefer not attributed to
    them.
    This anarchist, prefigurative research ethics has much in common with ethics of care in

    [...]

    levels of an organisation is through the notions of tactics and strategy. Beer does just
    this when he suggests that Systems One and Two are involved in tactics while System
    Three is involved in strategy (ibid., p. 360; see also, e.g. Pickering, 2010, p. 245). This
    is, I would argue, misleading as for Beer Systems Three and Four operate along similar
    lines but with System Three focused on the internal environment and what happens at
    Systems One and Two while System Four is focused on the external environment and
    the potential future of the organisation. I want to think, therefore, of Systems Three and

    [...]

    important tactically to what radical left groups and activists do. In a social centre I used
    to frequent there was a small model of a kitchen sink with written on it: ‘First the
    washing up, then the revolution’. 41 The tactical repertoire of radical left groups includes
    all this and more.
    How does this distinction between tactics and strategy operate in anarchist organisation,

    [...]

    As an extension of the tactics-strategy dichotomy presented above, this suggests the role
    of a third element of the politics of radical left groups and movements, what I want here
    to refer to as ‘grand strategy’. The term ‘grand strategy’ was coined by American
    military theorist John Boyd during the Cold War. 45 Boyd defines grand strategy as
    pursuing ‘the national goal’ and amplifying ‘our spirit and strength (while undermining
    and isolating our adversaries)’ (2005, slide 140). The notion of a ‘national goal’ is of
    course very specific to a state-centred geo-political project such as a war and is certainly
    at extreme odds with anarchism’s anti-militarism and anti-nationalism. Indeed, even the
    idea of competition contained in Boyd’s definition of grand strategy is antithetical to the
    relationships of mutual aid and cooperation. While ideas such as these are common in
    some business and management accounts of strategy (see, e.g. Carter, Clegg and

    [...]

    Hopefully, I have shown that the answer to both of these has to be negative. For Beer,
    organisational cybernetics is about defining the necessary and sufficient conditions,
    based on the need to handle complexity, for viable organisation. This does not need to
    result in a centralised, bureaucratic and authoritarian structure but can be grounded in
    one that relies on autonomy. By formulating an anarchist cybernetics, I want to show
    that while Beer maintains the basic structure of capitalist enterprise, with a middle
    management layer and a senior executive level at the top, these are not necessary for
    viability. This is the core difference between Beer’s cybernetics and the anarchist
    cybernetics I am arguing for here. To give Beer the credit he is due, his account of how
    organisations should determine goals does touch on non-hierarchical processes, as
    Pickering argues (2010: 272). Anarchist cybernetics shows how tactics, strategy and
