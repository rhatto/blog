[[!meta title="Ruptura"]]

## Trechos

    Os únicos que se sustentam são aqueles que não fingem mais governar nada, mas
    que usam o poder simplesmente para fornecer a parcelas da população o gosto
    drogado da autorização da violência contra os vulneráveis.
    
    -- 11

    Só governos fracos são violentos.  Eles têm de vigiar todos os poros, pois
    sabem que seu fim pode vir de qualquer lugar. Governos fortes são magnânimos,
    porque vislumbram tranquila- mente sua perpetuação. O que se contrapõe a nós é
    fraco e desesperado. Ele cairá. É hora de fazê-lo cair.

    -- 13

        Generais e almirantes tomam o poder. Eles exterminam seus predecessores de
        esquerda, exilam os opositores, aprisionam os intelectuais dissidentes, sufocam
        os sindicatos, controlam a imprensa e paralisam toda atividade política. Mas,
        nesta variante do fascismo de mercado, os che- fes militares tomam distância
        das decisões eco- nômicas. Eles não planificam a economia nem aceitam suborno.
        Eles confiam toda a economia a fanáticos religiosos – fanáticos cuja religião é
        o laissez-faire do mercado (...) Então o relógio da história anda para trás. O
        mercado é liberado e a massa monetária estritamente controlada. Os créditos de
        ajuda social são cortados, os trabalha- dores devem aceitar qualquer coisa ou
        morrer de fome (...) A inflação baixa reduz-se a quase nada (...) A liberdade
        política estando fora de circula- ção, as desigualdades de rendimentos, consumo
        e riqueza tendem a crescer.
    
    É evidente que as elucubrações de Samuelson a respeito do “fascismo de mercado”
    se inspiravam no Chile da ditadura de Augusto Pinochet (1973- 1990). Esse
    regime sucedeu um governo que ten- tava construir o socialismo pela via
    eleitoral e foi derrubado por uma articulação envolvendo a social- -democracia
    cristã, grupos terroristas neofascistas, entidades patronais e Washington, num
    processo que culminou no bombardeio do Palácio de La Moneda em 11 de setembro
    de 1973. A partir daí, Pinochet aniquilou a oposição com uma brutali- dade
    poucas vezes vista. O Estado chileno torturou cerca de 30 mil opositores, em
    centros espalhados por todo o território nacional, e assassinou milhares
    de pessoas. Apenas assim foi possível impor à população as políticas dos
    fanáticos do laissez-faire.  Iniciava-se o experimento neoliberal imposto pelos
    Chicago boys. Após o golpe, esse grupo de econo- mistas, ligados ao teórico e
    guru Milton Friedman, ocuparia todos os espaços do Estado ditatorial, dos
    ministérios à presidência do Banco Central.

    No mesmo momento em que o neoliberalismo aparecia como modelo de gestão social
    nas democracias liberais do Reino Unido de Margaret Thatcher e dos EUA de
    Ronald Reagan, a ditadura chilena explicitava a linha de fuga para a qual o
    capitalismo mundial se encaminhava. Essa junção de brutalidade política e
    neoliberalismo econômico, aplicada inicialmente no Chile, agora se mostra como
    a tendência generalizada do capitalismo atual e tem no Brasil seu mais recente
    laboratório. Tal processo ocorre precisamente no momento em que a farsa da
    livre concorrência foi definitivamente rasgada pelo retorno a práticas de
    acumulação primitiva, fazendo com que até mesmo a democracia
    liberal-parlamentar tenda a ser descartada – especialmente aqui, na periferia
    do capitalismo.

    [...]

    Por isso, segundo Hayek, o único regime totalitário que a América do Sul
    conheceu até os anos 1980 não teria sido o Brasil dos militares, a Argentina de
    Videla ou o Chile de Pinochet, mas o governo da Unidade Popular de Allende. A
    tese implícita era de que um modo de vida e de produção não baseado na
    propriedade privada dos meios de produção seria a definição mesma de
    totalitarismo.  Mas esse conceito liberal de liberdade só poderia se impor à
    base de choques. Afinal, as sociedades não aceitam sem resistência limitar seus
    desejos e sua inquietude à liberdade de empreender (reservada para alguns). A
    experiência histórica das lutas por liberdade revela justamente a insistência
    em livrar a atividade da submissão à forma do trabalho, da ânsia pela igualdade
    radical e pelo fim da naturalização da exploração, da vontade de liberação do
    mundo das coisas dos contratos de propriedade. Sendo assim, apenas uma fina
    engenharia social, que envolveria todas as instâncias do governo e do capital e
    que mobilizaria tanto o soldado de baixa patente como o burocrata do primeiro
    escalão, seria capaz de neutralizar esses desejos, criando uma homofonia
    social.  Embora paradoxal, a liberdade de empreender exige “mais” e não “menos”
    Estado, que se impõe na forma de repressão sanguinária e vigilância constante.

    [...]

    A aproximação entre Hayek e o principal jurista do Terceiro Reich, Carl
    Schmitt, não deixa dúvida sobre sua concepção de democracia.

    [...]

    O autoritarismo, portanto, não é um acidente do capitalismo e não é a antítese
    da democracia burguesa. Ele é parte constitutiva desse modo de gestão de
    populações. Afinal, foi no esteio da belle époque das grandes potências
    ocidentais que se consumou o holocausto dos povos coloniais, primeiro
    laboratório do caos.

    -- 17-25
