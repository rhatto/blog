[[!meta title="A Ilha"]]

A Ilha, de Fernando Morais, 30a edição.

## Depoimento de Julio Martinez Paes

    Santiago de Cuba tinha coisas curiosas. Frank País, por exemplo,
    estava sendo procurado, não podia andar pelas ruas. Mas podia falar
    pelo telefone sem medo de censura, porque todas as telefonistas da
    cidade eram militantes do Movimento 26 de Julho. Não só podíamos
    falar à vontade como até tínhamos acesso a conversações oficiais.
    Quando uma telefonista recebia uma ligação oficial, dava um jeito
    de colocar um de nós na extensão, para que soubéssemos com antecedência
    de muitos planos da ditadura.

-- pág. 209
