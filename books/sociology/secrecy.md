[[!meta title="The Sociology of Secrecy and of Secret Societies"]]

By Georg Simmel:

* [Original article](http://doi.org/10.1086/211418).
* [Full text](https://brocku.ca/MeadProject/Simmel/Simmel_1906.html).
* [Comments and references](https://en.wikipedia.org/wiki/Sociological_aspects_of_secrecy).

## Excerpts

    [...]

    All relationships of people to each other rest, as a matter of
    course, upon the precondition that they know something about
    each other. The merchant knows that his correspondent wants

    [...]

    rough and ready way, to the degree necessary in order that the
    needed kinds of intercourse may proceed. That we shall know
    with whom we have to do, is the first precondition of having
    anything to do with another. The customary reciprocal ptresenta-

    [...]

    reciprocally recognized. Their necessity is usually observed only
    when they happen to be wanted. It would be a profitable
    scientific labor to investigate the sort and degree of reciprocal
    apprehension which is needed for the various relationships
    between human beings. It would be worth while to know
    how the general psychological presumptions with which each
    approaches each are interwoven with the special experiences
    with reference to the individual who is in juxtaposition with us;
    how in many ranges of association the reciprocal apprehension
    does or does not need to be equal, or may or may not be permitted
    to be equal; how conventional relationships are determined in
    their development only through that reciprocal or unilateral
    knowledge developing with reference to the other party. The
    investigation should finally proceed in the opposite direction;

    [...]

    given by the total relationship of the knower to the known.
    Since one never can absolutely know another, as this would mean
    knowledge of every particular thought and feeling; since we
    must rather form a conception of a personal unity out of the
    fragments of another person in which alone he is accessible to
    us, the unity so formed necessarily depends upon that portion of
    the other which our standpoint toward him permits us to see.

    [...]

    on the other hand the actual reciprocity of the individuals is based
    tupon the picture which they derive of each other. Here we have
    one of the deep circuits of the intellectual life, inasmuch as one
    element presupposes a second, but the second presupposes the
    first. While this is a fallacy within narrow ranges, and thus

    [...]

    or by dissimulation he may deceive us as to the truth. No other
    object of knowledge can thus of its own initiative, either
    enlighten us with reference to itself or conceal itself, as a human
    being can. No other knowable object modifies its conduct from
    consideration of its being understood or misunderstood. 'Tlhis

    [...]

    in misconception about the true intention of the person who
    tells the lie. Veracity and mendacity are thus of the most far-
    reaching significance for the relations of persons with each
    other. Sociological structures are most characteristically dif-
    ferentiated by the measure of mendacity that is operative in
    them. To begin with, in very simple relationships a lie is
    much more harmless foir the persistence of the group than
    in complex associations. Primitive man, living in communities
    of restricted extent, providing for his needs by his own produc-
    tion or by direct co-operation, limiting his spiritual interests to
    personal experience or to simple tradition, surveys and controls
    the material of his existence more easily and completely than the
    man of higher culture. In the latter case life rests upon a thou-
    sand presuppositions which the individual can never trace back
    to their origins, and verify; but which he must accept upon faith
    and belief. In a much wider degree than people are accustomed
    the economic system
    to realize, modern civilized life -from
    which is constantly becoming more and more a credit-economy,

    [...]

    to the pursuit of science, in which the majority of investigators
    must use countless results obtained by others, and not directly
    subject to verification- depends upon faith in the honor of
    others. We rest our most serious decisions upon a complicated
    system of conceptions, the majority of which presuppose con-
    fidence that we have nlot been deceived. Hence prevarication in
    modern circumstances becomes something much more devasta-
    ting, something placing the foundations of life much more in
    jeopardy, than was earlier the case. If lying appeared today
    among us as a sin as permissible as among the Greek divinities,
    the Hebrew patriarchs, or the South Sea Islanders; if the
    extremne severity of the moral law did not veto it, the progressive
    upbuilding of modern life would be simply impossible, since
    modern life is, in a much wider than the economic sense, a
    "credit-economy." This relationship of the times recurs in the
    case of differences of other dimensions. The farther third per-
    sons are located from the center of our personality, the easier can
    we adjust ourselves practically, but also subjectively, to their lack
    of integrity. On the other hand, if the few persons in our imme-
    dia<te environment lie to us, life becomes intolerable. This


    [...]

    in the majority as compared with the liar who gets his advantage
    from the lie. Consequently that enlightenment which aims at
    elimination of the element of deception from social life is always
    of a democratic character.
    Human intercourse rests normally upon the condition that

    [...]

    development may gain vitality by alternate concession and resist-
    ance. Relationships of an intimate character, the formal vehicle
    of which is psycho-physical proximity, lose the charm, and even
    the content, of their intimacy, unless the proximity includes, at
    the same time and alternately, distance and intermission. Finally
    -and
    this is the matter with which we are now concerned -the
    reciprocal knowledge, which is the positive condition of social
    relationships, is not the sole condition. On the contrary, such as
    those relationships are, they actually presuppose also a certain

    [...]

    By virtue of the situation just noticed, that antecedent or
    consequent form of knowledge with reference to an individual-
    viz., confidence in him, evidently one of the most important syn-
    thetic forces within society -gains
    a peculiar evolution. Confi-
    dence, as the hypothesis of future conduct, which is sure enough
    to become the basis of practical action, is, as hypothesis, a mediate
    condition between knowing and not knowing another person.
    The possession of full knowledge does away with the need o,f
    trusting, while complete absence of knowledge makes trust evi-
    dentlv impossible.' Whatever quantities of knowing and not
    knowing must comnimingle, in order to make possible the detailed
    practical decision based upon confidence, will be determined by
    the historic epoch, the ranges of interests, and the individuals.

    [...]

    what is not forbidden is permitted, and, what is not permitted is
    forbidden. Accordingly, the relationships of men are differen-
    tiated by the question of knowledge with reference to each other:
    what is not concealed may be known, and what is not revealed
    may yet not be known. The last determination corresponds to the
    otherwise effective consciousness that an ideal sphere surrounds
    every human being, different in various directionsi and toward
    different persons; a sphere varying in extent, into which one may
    not venture to penetrate without disturbing the personal value of
    the individual. Honor locates such an area. Language indi-
    cates very nicely an invasion of this sort by such phrases as
    "coming too near" (zu nahe treten). The radius of that sphere,
    so to speak, marks the distance which a stranger may not cross
    without infringing up,on another's honor. Another sphere of
    like form corresponds to that which we designate as the "signifi-
    cance" (Bedeutung) of another personality. Towards the
    "significant" man there exists an inner compulsion to keep one's

    [...]

    signifies violation of the ego, at its center. Discretion is nothing
    other than the sense of justice with respect to the sphere of the
    intimate contents of life. Of co-urse, this sense is various in its


    [...]

    voluntarily reveal to us-must
    necessity. But in finer and less simple form, in fragmentary
    passages of association and in unuttered revelations, all commerce
    of men with each other rests upon the condition that each knows
    something more of the other than the latter voluntarily reveals
    to him; and in many respects this is of a sort the knowledge of
    which, if possible, would have been prevented by the party so
    revealed. While this, judged as an individual affair, may count
    as indiscretion, although in the social sense it is necessary as a

    [...]

    voluntarily reveal to us-must
    necessity. But in finer and less simple form, in fragmentary
    passages of association and in unuttered revelations, all commerce
    of men with each other rests upon the condition that each knows
    something more of the other than the latter voluntarily reveals
    to him; and in many respects this is of a sort the knowledge of
    which, if possible, would have been prevented by the party so
    revealed. While this, judged as an individual affair, may count
    as indiscretion, although in the social sense it is necessary as a
    condition for the existing closeness and vitality of the inter-
    change, yet the legal boundary of this invasion upon the spiritual
    private property of another is extremely difficult to draw. In
    general, men credit themselves with the right to know everything
    which, without application of external illegal means, through
    purely psychological observation and reflection, it is possible to
    ascertain. In point of fact, however, indiscretion exercised in
    this way may be quite as violent, and morally quite as unjusti-
    fiable, as listening at keyholes and prying into the letters of

    [...]

    strangers. To anyone with fine psychological perceptions, men
    betray themselves and their inmost thoughts and characteristics
    in countless fashions, not only in spite of efforts not to' do so, but
    often for the very reason that they anxiously attempt to guard
    themselves. The greedy spying upon every unguarded word;
    the boring persistence of inquiry as to the meaning of every slight
    action, or tone of voice; what may be inferred from. such and
    such expressions; what the blush at the mention of a given name
    may betray-all this does, not overstep the boundary o'f external
    discretion; it is entirely the labor of one's own mind, and there-
    fore apparently within the unquestionable rights of the agent.
    This is all the more the case, since such misuse of psychological
    superiority oiften occurs as a purely involuntary procedure. Very
    often it is impossible for us to, restrain our interpretation of
    another, our theory of his subjective characteristics and inten-
    tions. However positively an honorable person may forbid him-

    [...]

    so unavoidable, the division line between the permitted and the
    non-permitted is the more indefinite. To what extent discretion
    must restrain itself from mental handling " of all that which is its
    own," to what extent the interests of intercourse, the reciprocal
    interdependence of the members of the same group, limits this
    duty of discretion - this is a question for the answer to, which
    neither moral tact, nor survey of the o'bj ective relationships and
    their demands, can alone be sufficient, since both factors must
    rather always work together. The nicety and complexity of this
    question throw it back in a much higher degree upon the respon-
    sibility of the individual for decision, without final recourse to
    any authoritative general norm, than is the case in connection
    with a question of private property in the material sense.
    In contrast with this preliminary form, or this attachment of

    [...]

    quently friendship, in which this intensity, but also this
    inequality of devotion, is lacking, may more easily attach the
    whole person to the whole person, may more easily break up
    the reserves of the soul, not indeed by so impulsive a process,
    but throoughout a wider area and during a longer succession.
    This complete intimacy of confidence probably becomes, with
    the changing differentiation of men, more and more difficult.
    Perhaps the modern man has too much to conceal to make a
    friendship in the ancient sense possible; perhaps personalities
    also, except in very early years, are too peculiarly individualized
    for the complete reciprocality of understanding, to which
    always so much divination and productive phantasy are essen-
    tial. It appears that, for this reason, the mo,dern type of
    feeling inclines more to differentiated friendships; that is, to
    those which have their territory only upon one side of the person-
    ality at a time, and in which the rest of the personality plays no
    part. Thus a quite special type of friendship emerges. For our
    problem, namely, the degree of intrusion or of reserve within the
    friendly relationship, this type is of the highest significance.

    [...]

    must come sooner or later.
    In marriage, as in free relationships of analogous types, the
    temptation is very natural to open oneself to the other at the
    outset without limit; to abandon the last reserve of the soul
    equally with those of the body, and thus to. lose oneself completely
    in another. This, however, usually threatens the future of the
    relationship. Only those people can without danger give them-
    selves entirely to each other who canntot possibly give themselves
    entirely, because the wealth of their soul rests in constant pro-
    gressive development, which follows every devotion immediately
    with the growth of new treasures. Complete devotion is safe
    only in the case of those people who, have an inexhaustible fund
    of latent spiritual riches, and therefore can no more alienate them
    in a single confidence than a tree can give up the fruits of next
    year by letting go what it produces at the present moment. The
    case is quite different, however, with those people who, so to
    speak, draw from their capital all their betrayals of feeling and

    [...]

    intensity so soon as it is confronted by a purpose of discovery.
    Thereupon follows that purposeful concealment, that aggressive
    defense, so to speak, against the other party, which we call
    secrecy in the most real sense. Secrecy in this sense- i. e., whichi
    is effective through negative or positive means of concealment
    is one of the greatest accomplishments of humanity. In contrast
    with the juvenile condition in which every mental picture is at

    [...]

    by the fact that what was formerly putblic passes under the pro-
    tection of secrecy, and that, on the contrary, what was formerly
    secret ceases to require such protection and proclaims itself. This
    is analogous with that other evolution o,f mind in which move-
    ments at first executed consciously become unconsciously me-
    chanical, and, on the other hand, what was unconscious and
    instinctive rises into the light of consciousness.
    How this
    development is distributed over the various formations of private

    [...]

    essential and significant. The natural impulse to idealization, and
    the natural timidity of men, operate to one and the samne end in
    the presence of secrecy; viz., to heighten it by phantasy, and to
    distinguish it by a degree of attention that published reality could
    not command.
    Singularly enough, these attractions of secrecy enter into

    [...]

    not command.
    Singularly enough, these attractions of secrecy enter into
    combination with those of its logical opposite; viz., treason or
    betrayal of secrets, which are evidently no less sociological in
    their nature. Secrecy involves a tension which, at the moment of
    revelation, finds its release. This constitutes the climax in the
    development of the secret; in it the whole charm of secrecy con-
    centrates and rises to its highest pitch - just as the moment of the
    disappearance of an object brings out the feeling of its value in
    the most intense degree. The sense of power connected with
    possession of money is most comnpletely and greedily concentrated
    for the soul of the spendthrift at the moment at which this power
    slips from his hands. Secrecy also is sustained by the conscious-

    [...]

    466
    THE AMERICAN JOURNAL OF SOCIOLOGY
    ness that it might be exploited, and therefore confers power to
    modify fo,rtunes, to produce surprises, joys, and calamities, even
    if the latter be only misfortunes to ourselves. Hence the possi-
    bility and the temptation of treachery plays around the secret, and
    the external danger off being discovered is interwoven with the
    internal danger of self-discovery, which has the fascination of the
    brink o,f a precipice. Secrecy sets barriers between men, but at
    the same time offers the seductive temptation to break through the
    barriers by gossip or confession. This temptation accompanies
    the psychical life of the secret like an overtone. Hence the socio-
    logical significance of the secret, its practical measure, and the
    mode o,f its workings must be found in the capacity or the inclina-
    tion of the initiated to, keep the secret to' himself, or in his resist-
    ance or weakness relative to the temptation to, betrayal. From the
    play of these two interests, in concealment and in revelation,
    spring shadings and fortunes of human reciprocities throughout
    their whole range. If, according to our previous analysis, every
    human relationship has, as one of its traits, the degree of secrecy
    within or around it, it follows that the further development of the
    relationship in this respect depends on the combining proportions
    of the retentive and the communicative energies -the
    former
    sustained by the practical interest and the formal attractiveness
    of secrecy as such, the latter by inability to, endture longer the
    tension of reticence, and by the superiority which is latent, so to
    speak, in secrecy, but which is actualized for the feelings only at
    the moment o'f revelation, and o'ften also, on the other hand, by
    the joy of confession, which may contain that s,ense o,f power in
    negative and perverted form, as self-abasement and contrition.
    All these factors, which determine the sociological role of


    [...]

    too great temptation to disclose what might otherwise be hidden.
    But in this case there is no need of secrecy in a high degree,
    because this social formation usually tends to level its members,
    and every peculiarity of being, acting, or possessing the persist-
    ence of which requires secrecy is abhorrent to it. That all this
    changes to its opposite in case of large widening of the circle is
    a matter-of-course. In this connection, as in so many other par-
    ticulars, the facts of monetary relationships reveal most distinctly
    the specific traits of the large circle. Since transfers of economic
    values have occurred principally by means of money, an otherwise
    unattainable secrecy is possible in such transactions. Three pecu-
    liarities of the money form of values are here important: first,
    its compressibility, by virtue of which it is possible to, make a man
    rich by slipping into his hand a check without attracting attention;
    second, its abstractness and absence of qualitative character, in
    consequence of which numberless sorts of acquisitions and trans-
    fers of possessions may be covered up and guarded from publicity
    in a fashion impossible so long as values could be possessed only
    as extended, tangible objects; third, its long-distance effective-
    ness, by virtue of which we may invest it in the most widely
    removed and constantly changing values, and thus withdraw it
    utterly from the view of our nearest neighbors. These facilities
    of dissimulation which inhere in the degree of extension in the
    use of money, and which disclose their dangers particularly in
    dealings with foreign money, have called forth, as protective pro-
    visions, publicity of the financial operations of corporations.
    This points to a closer definition of the formula of evolution dis-
    cussed above; viz., that throughout the form of secrecy there
    occurs a permanent in- and out-flow of content, in which what is
    originally open becomes secret, and what was originally concealed
    throws off its mystery. Thus we might arrive at the paradoxical
    idea that, under otherwise like circumstances, human associations
    require a definite ratio of secrecy which merely changes its


    [...]

    this exchange it keeps its quantum unvaried. We may even fill
    out this general scheme somewhat more exactly. It appears that
    with increasing telic characteristics of culture the affairs of
    people at large become more and more public, those of individuals
    more and more secret. In less developed conditions, as observed
    above, the circumstances of individual persons cannot protect
    themselves in the same degree from reciprocal prying and inter-
    fering as within modern types of life, particularly those that have
    developed in large cities, where we find a quite new degree of
    reserve and discretion. On the other hand, the public function-
    aries in undeveloped states envelop themselves in a mystical
    authority, while in maturer and wider relations, through exten-
    sion of the range of their prerogatives, through the objectivity of
    their technique, through the distance that separates them from
    most of the individuals, a security and a dignity accrue to them
    which are compatible with publicity of their behavior. That
    earlier secrecy of public functions, however, betrayed its essential

    [...]

    Footnote 2 This counter-movement occurs also in the reverse direction.
    It has been
    observed, in connection with the history of the English court, that the actual
    court cabals, the secret whisperings, the organized intrigues, do not spring up
    under despotism, but only after the king has constitutional advisers, when the
    government is to that extent a system open to view. After that time-
    and this
    applies especially since Edward II-the
    king begins to form an unofficial, and
    at the same time subterranean, circle of advisers, in contrast with the ministers
    somehow forced upon him. This body brings into existence, within itself, and
    through endeavors to join it, a chain of concealments and conspiracies.


    [...]

    have thought possible. Accordingly, politics, administration,
    justice, have lost their secrecy and inaccessibility in precisely the
    degree in which the individual has gained possibility of more com-
    plete privacy, since modern- life has elaborated a technique for
    isolation of the affairs of individuals, within the crowded condi-
    tions of great cities, possible in former times only by means of
    spatial separation.

    To what extent this development is to be regarded as advan-
    tageous depends upon social standards of value. Democracies are
    bound to regard publicity as the condition desirable in itself.
    This follows from the fundamental idea that each should be
    informed about all the relationships and occurrences with which
    he is concerned, since this is a condition of his doing his part with
    reference to them, and every community of knowledge contains
    also the psychological stimulation to community of action. It is
    immaterial whether this conclusion is entirely binding. If an
    objective controlling structure has been built up, beyond the
    individual interests, but nevertheless to their advantage, such
    a structure may very well, by virtue of its formal inde-
    pendence, have a rightful claim to carry on a certain amount
    of secret functioning without prejudice to its public char-
    acter, so far as real consideration of the interests of all is con-
    cerned. A logical connection, therefore, which would necessitate
    the judgment of superior worth in favor of the condition of pub-
    licity, does not exist. On the other hand, the universal scheme of
    cultural differentiation puts in an appearance here: that which
    pertains to the public becomes more public, that which belongs to
    the individual becomes more private. Moreover, this historical
    development brings o-ut the deeper real significance: that which
    in its nature is public, wvhich in its content concerns all, becomes
    also externally, in its sociological form, more and more public;
    while that which in its inmost nature refers to the self alone-
    also, gain
    that is, the centripetal affairs of the individual -must
    in so-ciological position a more and more private character, a
    more decisive possibility of remaining secret.
    While secrecy, therefore, is a sociological ordination which

    [...]

    As a general proposition, the secret society
    emerges everywhere as correlate of despotism and of police con-
    trol. It acts as protection alike of defense and of offense against
    the violent pressure of central powers. This is true, not alone in
    political relations, but in the same way within the church, the
    school, and the family.

    [...]

    Thus the secret society
    cotinterbalances the separatistic factor which is peculiar to, every
    secret by the very fact that it is society.

    [...]

    lating will; for growth from within, constructive purposefulness.
    This rationalistic factor in their upbuilding cannot express itself
    more distinctly than in their carefully considered and clear-cut
    architecture. I cite as example the structure of the Czechic secret
    order, Omlaidina, which was organized on the model of a group
    of the Carbonari, and became known in consequence of a judicial
    process in I893. The leaders of the Omladina are divided into
    "thumbs" and "fingers." In secret session a "thumb" is chosen
    by the members. He selects four "fingers." The latter then
    choose another " thumb," and this second " thumb " presents himn-
    self to the first "thumb." The second "thumb" proceeds to
    choose four more "fingers"; these, another "thumb;" and so
    the articulation continues. The first " thumb " knows all the
    other " thumbs," but the remaining " thumbs " do not know each
    other. Of the "fingers" only those four know each other who
    are subordinate to one and the same "thumb." All transactions

    [...]

    of the Omladina are conducted by the first "thumb," the " dicta-
    tor." He informs the other "thumbs" of all proposed under-
    takings. The "thumbs" then issue orders to their respective
    subordinates, the "fingers." The latter in turn instruct the mem-
    bers of the Omnladina assigned to each. The circumstance that
    the secret society must be built up, from its base by calculation and
    conscious volition evidently affords free play for the peculiar
    passion which is the natural accompaniment of such arbitrary
    processes of construction, such foreordaining programs. All
    schematology - of science, of conduct, of society - contains a
    reserved power of compulsion. It subjects a material which is
    outside of thought to a form which thought has cast. If this is
    true of all attempts to organize groups according to a priori prin-
    ciples, it is true in the highest degree of the secret society, which
    does not grow, which is built by design, which has to reckon with
    a smaller quantum of ready-made building material than any
    despotic or socialistic scheme. Joined to the interest in making

    [...]

    The secret society must seek to create among the cate-
    gories peculiar to itself, a species of life-totality. Around the
    nucleus of purposes which the society strongly emphasizes, it
    therefore builds a structure of formulas, like a body around a
    soul, and places both alike under the protection of secrecy, because
    only so can a harmonious whole come into, being, in which one
    part supports the other. That in this scheme secrecy of the
    external is strongly accentuated, is necessary, because secrecy is
    not so much a matter of course with reference to these super-
    ficialities, and not so directly demanded as in the case of the real
    interests of the society. This is not greatly different from the
    situation in military organizations and religious communities.
    The reason why, in both, schematism, the body of forms, the fixa-
    tion of behavior, occupies so large space, is that, 'as a general pro-
    position, both the military and the religious career demand the
    wvhole man; that is, each of them projects the whole life upon a
    special plane; each composes a variety of energies and interests,
    from a particular point of view, into a correlated unity. The
    secret society usually tries to do the same.


    [...]

    The secret society must seek to create among the cate-
    gories peculiar to itself, a species of life-totality. Around the
    nucleus of purposes which the society strongly emphasizes, it
    therefore builds a structure of formulas, like a body around a
    soul, and places both alike under the protection of secrecy, because
    only so can a harmonious whole come into, being, in which one
    part supports the other. That in this scheme secrecy of the
    external is strongly accentuated, is necessary, because secrecy is
    not so much a matter of course with reference to these super-
    ficialities, and not so directly demanded as in the case of the real
    interests of the society. This is not greatly different from the
    situation in military organizations and religious communities.
    The reason why, in both, schematism, the body of forms, the fixa-
    tion of behavior, occupies so large space, is that, 'as a general pro-
    position, both the military and the religious career demand the
    wvhole man; that is, each of them projects the whole life upon a
    special plane; each composes a variety of energies and interests,
    from a particular point of view, into a correlated unity. The
    secret society usually tries to do the same. One of its essential
    characteristics is that, even when it takes hold of individuals only

    [...]

Counterpart of the official world, detachment from larger structures in
which it's contained (the next level of recursion):

    Moreover, through such formalism,
    just as through the hierarchical structure above discussed, the
    secret society constitutes itself a sort of counterpart of the official
    world with which it places itself in antithesis. Here we have a
    case of the universally emerging sociological norm; viz., struc-
    tures, which place themselves in opposition to and detachment
    from larger structures in which they are actually contained,
    nevertheless repeat in themselves the forms of the greater struc-
    tures. Only a structure that in some way can count as a whole
    is in a situation to hold its elements firmly together. It borrows
    the sort of organic completeness, by virtue of which its members
    are actually the channels of a unifying life-stream, from that
    greater whole to which its individual members were already
    adapted, and to which it can most easily offer a parallel by means
    of this very imitation.

    -- 482

Freedom and law from the inside:

    In exercise of this freedom a territory is occupied to which the norms of the
    surrounding society do not apply. The nature of the secret
    society as such is autonomy. It is, however, of a sort which
    approaches anarchy. Withdrawal from the bonds of unity which
    procure general coh,erence very easily has as consequences for the
    secret society a condition of being without roots, an absence of
    firm touch with life (Lebensgefiihl), and of restraining reserva-
    tions. The fixedness and detail of the ritual serve in part to
    counterbalance this deficit. Here also is manifest how much men
    need a settled proportion between freedom and law; and, further-
    more, in case the relative quantities of the two are not prescribed
    for him from a single source, how he attempts to reinforce the
    given quantum of the one by a quantum of the other derived from
    any source whatsoever, until such settled proportion is reached.

    -- 482

Existem a partir de sociedes públicas e de forma exclusiva::

    The secret society, on the other hand, is a secondary structure;
    i. e., it arises always only within an already complete society.

    [...]

    That they can build them selves up with such characteristics is possible, however, only
    under the presupposition of an already existing society. The
    secret society sets itself as a special society in antithesis with the
    wider association included within the greater society. This anti-
    thesis, whatever its purpose, is at all events intended in the spirit
    of exclusion. Even the secret society which proposes only to
    render the whole community a definite service in a completely
    unselfish spirit, and to dissolve itself after performing the service,
    obviously regards its temporary detachment from that totality as
    the unavoidable technique for its purpose. Accordingly, none of
    the narrower groups which are circumscribed by larger groiups
    are compelled by their sociological constellation to insist so
    strongly as the secret society upon their formal self-sufficiency.
    Their secret encircles them like a boundary, beyond which there is
    nothing but the materially, o,r at least formally, antithetic, which
    therefore shuts up the society within itself as a complete unity.
    In the groupings of every other sort, the content of the group-

Aristocracy:

    This significance of secret associations, as intensification of
    sociological exclusiveness in general, appears in a very striking
    way in political aristocracies. Among the requisites of aristo-
    cratic control secrecy has always had a place. It makes use of
    the psychological fact that the unknown as such appears terrible,
    powerful, and threatening. In the first place, it employs this fact
    in seeking to conceal the numerical insignificance of the govern-
    ing class. In Sparta the number of warriors was kept so, far as

    [...]

    On the other hand, the democratic principle is
    bound up with the principle of publicity, and, to the same end, the
    tendency toward general and fundamental laws. The latter relate
    to an unlimited number of subjects, and are thus in their nature
    public. Conversely, the employment of secrecy within the aristo-
    cratic regime is only the extreme exaggeration of that social
    exclusion and exemption for the sake of which aristocracies are
    wont to oppose general, fundamentally sanctioned laws.
    In case the notion of the aristocratic passes over from the

Freedom, obedience and centralization:

    To this result not merely the correlation of demand
    from freedom and for union contributes, as we have observed it
    in case of the severity of the ritual, and in the present instance it
    binds together the extremes of the two tendencies. The excess of
    freedom, which such societies possessed with reference to all
    otherwise valid norms, had to be offset, for the sake of the
    equilibrium of interests, by a similar excess olf submissiveness
    and resigning of the individual will. More essential, however.
    was probably the necessity of centralization, which is the con-
    dition of existence for the secret society, and especially when,
    like the criminal band, it lives off the surrounding society,
    when it mingles with this society in many radiations and
    actions, and when it is seriously threatened with treachery
    and diversion of interests the moment the most invariable
    attachment to one center ceases to prevail. It is conseqeuntly
    typical that the secret society is exposed to peculiar dangers,
    especially when, for any reasons whatever, it does not develop
    a powerfully unifying authority. The Waldenses were in
    nature not a secret society. They became a secret society in
    the thirteenth century only, in consequence of the external pres-
    sure, which made it necessary to keep themselves from view. It
    became impossible, for that reason, to hold regular assemblages,
    and this in turn caused loss of unity in doctrine. There arose a
    number of branches, with isolated life and development, fre-
    quently in a hostile attitude toward each other. They went into
    decline because they lacked the necessary and reinforcing attri-
    bute of the secret society, viz., constantly efficient centralization.

Responsibility:

    Nevertheless, responsibility
    is quite as immediately joined with the ego - philosophically, too,
    the whole responsibility problem is merely a detail of the problem
    of the ego - in the fact that removing the marks of identity of
    the person has, for the naive understanding in question, the effect
    of abolishing responsibility. Political finesse makes no less use of
    this correlation. In the American House of Representatives the
    real conclusions are reached in the standing,committees, and they
    are almost always ratified by the House. The transactions of
    these committies, however, are secret, and the most important
    portion of legislative activity is thus concealed from public view.
    This being the case, the political responsibility of the repre-
    sentatives seems to be largely wiped out, since no one can be
    made responsible for proceedings that cannot be observed. Since
    the shares of the individual persons in the transactions remain
    hidden, the acts of committees and of the House seem to be those
    of a super-individual authority. The irresponsibility is here also
    the consequence or the symbol of the same intensified sociological
    de-individualization which goes with the secrecy of group-action.
    In all directorates, faculties, committees, boards of trustees, etc.,
    whose transactions are secret, the same thing holds. The indi-
    vidual disappears as a person in the anonymous member of the
    ring, so to speak, and with him the responsibility, which has no
    hold upon him. in his intangible special character.
    Finally, this one-sided intensification of universal sociological

    -- 496-497

    [...]

Danger for the rest of society and the existing oficial and central power:

    Wherever there is an attempt to realize
    strong centralization, especially of a political type, special organi-
    zations of the elements are abhorred, purely as such, entirely apart
    from their content and purposes. As mere unities, so to speak,
    they engage in competition with the central principle.

    [...]

    Accordingly, the secret society seems to be dangerous simply
    because it is secret. Since it cannot be surely known that any
    special organization whatever may not some day turn its legally
    accumulated powers to some undesired end, and since on that
    account there is suspicion in principle on the part of central
    powers toward organizations of subjects, it follows that, in the
    case of organizations which are secret in principle, the suspicion
    that their secrecy conceals dangers is all the more natural.

    [...]

    Thus the secret society, purely on the ground of its secrecy, appears
    dangerously related to conspiracy against existing powers.

    [...]

    The secret association is in such bad repute as enemy of central powers that,
    conversely, every politically disapproved association must be
    accused of such hostility!

    -- 497-498
