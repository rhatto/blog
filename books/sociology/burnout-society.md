[[!meta title="The Burnout Society"]]

* Author: Byung-Chul Han

## Nano-resenha

Muito interessante. No entando, tomando emprestado a prática do autor de citar
para contradizer, é muito complicado definir a vigência de paradigmas de forma
estaque. Paradigmas se sobrepõem, coexistem.

## Excerpts

### The immunological age

    The past century was an immunological age. The epoch sought to distinguish
    clearly between inside and outside, friend and foe, self and other. The Cold
    War also followed an immunological pattern. Indeed, the immunological paradigm
    of the last century was commanded by the vocabulary of the Cold War, an
    altogether military dispositive. Attack and defense determine immunological
    action. The immunological dispositive, which extends beyond the strictly social
    and onto the whole of communal life, harbors a blind spot: everything foreign
    is simply combated and warded off. The object of immune defense is the foreign
    as such. Even if it has no hostile intentions, even if it poses no danger, it
    is eliminated on the basis of its Otherness.

### Multitasking, hyperactivity and boredom

    Excessive positivity also expresses itself as an excess of stimuli,
    information, and impulses. It radically changes the structure and economy of
    attention. Perception becomes fragmented and scattered. Moreover, the mounting
    burden of work makes it necessary to adopt particular dispositions toward time
    and attention [Zeit-und Aufmerksamkeitstechnik]; this in turn affects the
    structure of attention and cognition. The attitude toward time and environment
    known as “multitasking” does not represent civilizational progress. Human
    beings in the late-modern society of work and information are not the only ones
    capable of multitasking. Rather, such an aptitude amounts to regression.
    Multitasking is commonplace among wild animals. It is an attentive technique
    indispensable for survival in the wilderness.

    An animal busy with eating must also attend to other tasks. For example, it
    must hold rivals away from its prey. It must constantly be on the lookout, lest
    it be eaten while eating. At the same time, it must guard its young and keep an
    eye on its sexual partner. In the wild, the animal is forced to divide its
    attention between various activities. That is why animals are incapable of
    contemplative immersion—either they are eating or they are copulating. The
    animal cannot immerse itself contemplatively in what it is facing [Gegenüber]
    because it must also process background events. Not just multitasking but also
    activities such as video games produce a broad but flat mode of attention,
    which is similar to the vigilance of a wild animal. Recent social developments
    and the structural change of wakefulness are bringing human society deeper and
    deeper into the wilderness. For example, bullying has achieved pandemic
    dimensions. Concern for the good life, which also includes life as a member of
    the community, is yielding more and more to the simple concern for survival.

    We owe the cultural achievements of humanity—which include philosophy—to deep,
    contemplative attention. Culture presumes an environment in which deep
    attention is possible. Increasingly, such immersive reflection is being
    displaced by an entirely different form of attention: hyperattention. A rash
    change of focus between different tasks, sources of information, and processes
    characterizes this scattered mode of awareness. Since it also has a low
    tolerance for boredom, it does not admit the profound idleness that benefits
    the creative process.

### Rage

    Rage is the capacity to interrupt a given state and make a new state begin.

### Positivity

    The computer calculates more quickly than the human brain and takes on
    inordinate quantities of data without difficulty because it is free of all
    Otherness. It is a machine of positivity [Positivmaschine]. Because of autistic
    self-referentiality, because negativity is absent, an idiot savant can perform
    what otherwise only a calculator can do. The general positivization of the
    world means that both human beings and society are transforming into autistic
    performance-machines.

### Tiredness

    Tiredness in achievement society is solitary tiredness; it has a separating and
    isolating effect.

### Psyche

    The psyche of today’s achievement-subject differs from the psyche of the
    disciplinary subject. The ego, as Freud defines it, is a well-known
    disciplinary subject. Freud’s psychic apparatus is a repressive apparatus with
    commandments and prohibitions that subjugate and repress. Like disciplinary
    society, the psychic apparatus sets up walls, thresholds, borders, and guards.
    For this reason, Freudian psychoanalysis is only possible in repressive
    societies that found their organization on the negativity of prohibitions and
    commandments. Contemporary society, however, is a society of achievement;
    increasingly, it is shedding the negativity of prohibitions and commandments
    and presenting itself as a society of freedom. The modal verb that determines
    achievement society is not the Freudian Should, but Can. This social
    transformation entails intrapsychic restructuring. The late-modern
    achievement-subject possesses an entirely different psyche than the
    obedience-subject for whom Freud conceived psychoanalysis. Freud’s psychic
    apparatus is dominated by negation [Verneinung], repression, and fear of
    transgression. The ego is a “seat of anxiety” [Angststätte].3 In contrast, the
    late-modern achievement-subject is poor in negation. It is a subject of
    affirmation. Were the unconscious necessarily connected to the negativity of
    negation and repression [Verdrängung], then the late-modern achievement-subject
    would no longer have an unconscious. It would be a post-Freudian ego. The
    Freudian unconscious is not a formation that exists outside of time. It is a
    product of the disciplinary society, dominated by the negativity of
    prohibitions and repression, that we have long since left behind.
    
    The work performed by the Freudian ego involves the fulfillment of duty, above
    all. On this score, it shares a feature with the Kantian obedience-subject. For
    Kant, the conscience occupies the position of the superego. Kant’s moral
    subject is subject to “power” [Gewalt], too: Every man has a conscience and
    finds himself observed, threatened, and, in general, kept in awe (respect
    coupled with fear) by an internal judge; and this authority watching over the
    law in him is not something that he himself (voluntarily) makes, but something
    incorporated into his being.4 The Kantian subject, like the Freudian subject,
    is internally divided. It acts at the behest of Another; however, this Other is
    also part of itself: Now, this original intellectual and (since it is the
    thought of duty) moral predisposition called conscience is peculiar in that,
    although its business is a business of man with himself, one constrained by his
    reason sees himself constrained to carry it on as at the bidding of another
    person.5
    
    On the basis of this split, Kant speaks of a “doubled self,” or “dual
    personality.”6 The moral subject is simultaneously defendant and judge.  The
    obedience-subject is not a subject of desire or pleasure, but a subject of
    duty. Thus, the Kantian subject pursues the work of duty and represses its
    “inclinations.” Hereby, God—that “omnipotent moral being”—does not appear only
    as the instance of punishment and condemnation, but also (and this is a very
    important fact, which seldom receives due attention) as the instance of
    gratification. As the subject of duty, the moral subject represses all
    pleasurable inclinations in favor of virtue; God, who epitomizes morality,
    rewards such painfully performed labors with happiness [Glückseligkeit].
    Happiness is “distributed in exact proportion to morality [Sittlichkeit].”7 The
    moral subject, which accepts pain for morality, may be entirely certain of
    gratification. There is no threat of a crisis of gratification occurring, for
    God does not deceive: He is trustworthy.
    
    The late-modern achievement-subject does not pursue works of duty. Its maxims
    are not obedience, law, and the fulfillment of obligation, but rather freedom,
    pleasure, and inclination. Above all, it expects the profits of enjoyment from
    work. It works for pleasure and does not act at the behest of the Other.
    Instead, it hearkens mainly to itself. After all, it must be a self-starting
    entrepreneur [Unternehmer seiner selbst]. In this way, it rids itself of the
    negativity of the “commanding [gebietender] Other.” However, such freedom from
    the Other is not just emancipating and liberating. The dialectic of freedom
    means developing new constraints. Freedom from the Other switches into
    narcissistic self-relation, which occasions many of the psychic disturbances
    afflicting today’s achievement-subject.
    
    The absence of relation to the Other causes a crisis of gratification. As
    recognition, gratification presupposes the instance of the Other (or the “Third
    Party”). It is impossible to reward oneself or to acknowledge oneself. For
    Kant, God represents the instance of gratification: He rewards and acknowledges
    moral accomplishment. Because the structure of gratification has been
    disturbed, the achievement-subject feels compelled to perform more and more.
    The absence of relation to the Other, then, represents the transcendental
    condition for the crisis of gratification to arise in the first place. However,
    contemporary relations of production are also responsible. A definitive work
    [Werk], as the result of completed labor [Arbeit], is no longer possible today.
    Contemporary relations of production stand in the way of conclusion. Instead,
    one works into the open. Conclusive forms [Abschlußformen] with a beginning and
    an end prove wanting.

    [...]

    Hysteria is a typical psychic malady of the disciplinary society that witnessed
    the founding of psychoanalysis. It presumes the negativity of repression,
    prohibition, and negation, which lead to the formation of the unconscious.
    Drive-representations [Triebrepräsentanzen] that have been pushed off into the
    unconscious manifest themselves, by means of “conversion,” as bodily symptoms
    that mark a person unambiguously. Hysterics exhibit a characteristic morphe.
    Therefore, hysteria admits morphology; this fact distinguishes it from
    depression.
    
    According to Freud, “character” is a phenomenon of negativity, for it does not
    achieve form without the censorship that occurs in the psychic apparatus.
    Accordingly, he defines it as “a precipitate of abandoned object-cathexes.”10
    When the ego becomes aware of object-cathexes taking place in the id, it either
    lets them be or fights them off through the process of repression. Character
    contains the history of repression within itself. It represents a determinate
    relation of the ego to the id and to the superego. Whereas the hysteric shows a
    characteristic morphe, the depressive is formless; indeed, he is amorphous. He
    is a man without character. One might generalize the observation and declare
    that the late-modern ego has no character. Carl Schmitt says it is a “sign of
    inner conflict to have more than one real enemy.”11 The same holds for friends.
    Following Schmitt, having more than one true friend would betoken a lack of
    character and definition. One’s many friends on Facebook would offer further
    proof of the late-modern ego’s lack of character and definition. In positive
    terms, such a human being without character is flexible, able to assume any
    form, play any role, or perform any function. This shapelessness—or,
    alternately, flexibility—creates a high degree of economic efficiency.

    Psychoanalysis presupposes the negativity of repression and negation. The
    unconscious and repression, Freud stresses, are “correlative” to the greatest
    extent. In contrast, the process of repression or negation plays no role in
    contemporary psychic maladies such as depression, burnout, and ADHD. Instead,
    they indicate an excess of positivity, that is, not negation so much as the
    inability to say no; they do not point to not-being-allowed-to-do-anything
    [Nicht-Dürfen], but to being-able-to-do-everything [Alles-Können]. Therefore,
    psychoanalysis offers no way of approaching these phenomena. Depression is not
    a consequence of repression that stems from instances of domination such as the
    superego. Nor does depression permit “transference,” which offers indirect
    signs of what has been repressed.
    
    With its idea of freedom and deregulation, contemporary achievement society is
    massively dismantling the barriers and prohibitions that constituted
    disciplinary society. The dismantling of negativity serves to enhance
    achievement. Matters reach a general state of dissolution and
    boundlessness—indeed, a state of general promiscuity—from which no energy of
    repression issues. Where restrictive sexual morality does not prevent the
    impulses of drives from being discharged, paranoid delusions do not emerge—such
    as those of Daniel Paul Schreber, which Freud traced back to repressed
    homosexuality. The “Schreber Case” typifies nineteenth-century disciplinary
    society, where the strict prohibition of homosexuality—indeed, of pleasure and
    desire as a whole—predominated.
    
    The unconscious plays no part in depression. It no longer governs the psychic
    apparatus of the depressive achievement-subject.
    
    [...]
    
    Freud understands melancholy as a destructive relationship to the Other that
    has been made part of the self through narcissistic identification. In this
    process, the originary conflicts with the Other are internalized and
    transformed into a conflicted self-relationship that leads to
    ego-impoverishment and auto-aggression. However, the depressive disorder of the
    contemporary achievement-subject does not follow upon a conflicted, ambivalent
    relation to the Other that now has gone missing. No dimension of alterity is
    involved. Depression—which often culminates in burnout—follows from
    overexcited, overdriven, excessive self-reference that has assumed destructive
    traits. The exhausted, depressive achievement-subject grinds itself down, so to
    speak. It is tired, exhausted by itself, and at war with itself. Entirely
    incapable of stepping outward, of standing outside itself, of relying on the
    Other, on the world, it locks its jaws on itself; paradoxically, this leads the
    self to hollow and empty out. It wears out in a rat race it runs against
    itself.

    New media and communications technology are also diluting being-for-otherness
    [Sein zum Anderen]. The virtual world is poor in alterity and the resistance
    [Widerständlichkeit] it displays. In virtual spaces, the ego can practically
    move independent of the “reality principle,” which would provide a principle of
    alterity and resistance. In all the imaginary spaces of virtuality, the
    narcissistic ego encounters itself first and foremost. Increasingly,
    virtualization and digitalization are making the real disappear, which makes
    itself known above all through its resistance. The real is a stay in the double
    meaning of the word. It not only offers interruption and resistance, but also
    affords stopping and support.
    
    The late-modern achievement-subject, with a surplus of options at its disposal,
    proves incapable of intensive bonding. Depression severs all attachments.
    Mourning differs from depression above all through its strong libidinal
    attachment to an object. In contrast, depression is objectless and therefore
    undirected. It is important to distinguish depression from melancholy.
    Melancholy is preceded by the experience of loss. Therefore it still stands in
    a relation—namely, negative relation—to the absent thing or party. In contrast,
    depression is cut off from all relation and attachment. It utterly lacks
    gravity [Schwerkraft].
    
    Mourning occurs when an object with a strong libidinal cathexis goes missing.
    One who mourns is entirely with the beloved Other. The late-modern ego devotes
    the majority of libidinal energy to itself. The remaining libido is distributed
    and scattered among continually multiplying contacts and fleeting
    relationships. It proves quite easy to withdraw the weakened libido from the
    Other and to use it to cathect new objects. There is no need for drawn-out,
    pain-filled “dream work.” In social networks, the function of “friends” is
    primarily to heighten narcissism by granting attention, as consumers, to the
    ego exhibited as a commodity.
    
    [...]
    
    Seen in this light, depression no longer represents the “lost relation to
    conflict,” but rather the absent relation to an objective instance of decision
    that would produce conclusive forms and thereby assure an instance of
    gratification.

### Burnout

    Burnout, which often precedes depression, does not point to a sovereign
    individual who has come to lack the power to be the “master of himself.”
    Rather, burnout represents the pathological consequence of voluntary
    self-exploitation. The imperative of expansion, transformation, and
    self-reinvention—of which depression is the flipside—presumes an array of
    products tied to identity. The more often one changes one’s identity, the more
    production is dynamized. Industrial disciplinary society relied on unchanging
    identity, whereas postindustrial achievement society requires a flexible person
    to heighten production.

    [...]

    The late-modern achievement-subject is subject to no one. In fact, it is no
    longer a subject in the etymological sense (subject to, sujet à). It
    positivizes itself; indeed, it liberates itself into a project. However, the
    change from subject to project does not make power or violence disappear.
    Auto-compulsion, which presents itself as freedom, takes the place of
    allo-compulsion. This development is closely connected to capitalist relations
    of production. Starting at a certain level of production, auto-exploitation is
    significantly more efficient and brings much greater returns [leistungsstärker]
    than allo-exploitation, because the feeling of freedom attends it. Achievement
    society is the society of self-exploitation. The achievement-subject exploits
    itself until it burns out. In the process, it develops auto-aggression that
    often enough escalates into the violence of self-destruction. The project turns
    out to be a projectile that the achievement-subject is aiming at itself.

    [...]

    In view of the ego ideal, the real ego appears as a loser buried in
    self-reproach. The ego wages war with itself. The society of positivity, which
    thinks itself free of all foreign constraints, becomes entangled in destructive
    self-constraints. Psychic maladies such as burnout and depression, the
    exemplary maladies of the twenty-first century, all display auto-aggressive
    traits. Exogenous violence is replaced by self-generated violence, which is
    more fatal than its counterpart inasmuch as the victim of such violence
    considers itself free.
    
    [...]
    
    The capitalist system is switching from allo-exploitation to auto-exploitation
    in order to accelerate. On the basis of the paradoxical freedom it holds, the
    achievement-subject is simultaneously perpetrator and victim, master and slave.
    Freedom and violence now coincide.
    
    [...]
    
    The life of homo sacer in achievement society is holy and bare for another
    reason entirely. It is bare because, stripped of all transcendent value, it has
    been reduced to the immanency of vital functions and capacities, which are to
    be maximized by any and all means. The inner logic of achievement society
    dictates its evolution into a doping society. Life reduced to bare, vital
    functioning is life to be kept healthy unconditionally. Health is the new
    goddess.31 That is why bare life is holy.
    
    The homines sacri of achievement society also differ from those of the society
    of sovereignty on another score. They cannot be killed at all. Their life
    equals that of the undead. They are too alive to die, and too dead to live.
