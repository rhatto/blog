[[!meta title="Torture and Truth"]]

* [Torture and Truth](https://www.worldcat.org/title/torture-and-truth/oclc/20823386).
* By Page duBois.

## About

    First published in 1991, this book — through the examination of ancient
    Greek literary, philosophical and legal texts — analyses how the Athenian
    torture of slaves emerged from and reinforced the concept of truth as something
    hidden in the human body. It discusses the tradition of understanding truth as
    something that is generally concealed and the ideas of ‘secret space’ in both
    the female body and the Greek temple. This philosophy and practice is related
    to Greek views of the ‘Other’ (women and outsiders) and considers the role of
    torture in distinguishing slave and free in ancient Athens. A wide range of
    perspectives — from Plato to Sartre — are employed to examine the subject.

## Topics

* Example of actual tortures that took place: see defense of Andokides. Footnote 6.

## Excerpts

### Machine atroci

    Inside sat other devices. The iron maiden of Frankfurt, a
    larger-than-life-size female body, cast in iron, strangely reminiscent of one
    of those Russian dolls, a rounded maternal peasant body that opens horizontally
    to reveal another identical doll inside, that opens again and again until one
    reaches a baby, perhaps, I can’t recall, in its deepest inside. This body,
    propped open, had been cast with a vertical split, its interior consisting of
    two sets of sharp iron spikes that, when the maiden was closed on a captive
    human body, penetrated that body, trapping it upright as it killed in a
    grotesque parody of pregnancy made a coffin.

    [...]

    For me, the pear was not the most compelling “machine” on display. There
    sat, on one of the tables inside the Quirinale Palace, a simple modern device,
    looking something like a microphone, with electrodes dangling from it. The
    catalogue acknowledged that critics had objected to the inclusion of this
    instrument in the exhibit.

    [...]

    As I recognized what it must be, pieced together an idea of its functions from
    recently read accounts of refugees from the Argentinian junta and from Central
    America, recalled films of the Algerian war, news stories of the reign of the
    colonels in Greece, this instrument composed of the only too familiar elements
    of modern technology defamiliarized the devices on exhibit; removing them from
    the universe of the museum, it identified them with the calculated infliction
    of human agony. It recontextualized all the other objects, prevented them from
    being an aesthetic series, snatched them from the realm of the commodified
    antique, recalled suffering.

    The ancient Greeks and Romans routinely tortured slaves as part of their legal
    systems. So what? Is the recollection of this fact merely a curiosity, a memory
    of the “antique” which allows us to marvel at our progress from the past of
    Western culture, our abolition of slavery? Some of us congratulate ourselves on
    our evolution from a barbaric pagan past, from the world of slave galleys and
    crucifixions, of vomitoria and gladiatorial contests, of pederasty and
    polytheism. But there is another, supplementary or contestatory narrative told
    about ancient Greek culture—a narrative about the noble origins of Western
    civilization. This narrative has analogies with the Quirinale exhibit—it
    represents the past as a set of detached objects, redolent with antique
    atmosphere. This alternate and prejudicially selective gaze at the high culture
    of antiquity, the achievement of those ancient Greeks and Romans to whom we
    point when we discuss our golden age, produces an ideological text for the
    whole world now, mythologies about democracy versus communist totalitarianism,
    about progress, civilized values, human rights. Because we are descended from
    this noble ancient culture, from the inventors of philosophy and democracy, we
    see ourselves as privileged, as nobly obliged to guide the whole benighted
    world toward Western culture’s version of democracy and enlightenment. But even
    as we gaze at high culture, at its origins in antiquity, at its present
    manifestations in the developed nations, the “base” practices of torturers
    throughout the world, many of them trained by North Americans, support this
    narrative by forcing it on others, by making it the hegemonic discourse about
    history. So-called high culture—philosophical, forensic, civic discourses and
    practices—is of a piece from the very beginning, from classical antiquity, with
    the deliberate infliction of human suffering. It is my argument in this book
    that more is at stake in our recognition of this history than antiquarianism,
    than complacency about our advances from barbarism to civilization. That truth
    is unitary, that truth may finally be extracted by torture, is part of our
    legacy from the Greeks and, therefore, part of our idea of “truth.”

### Sartre

    "Torture is senseless violence, born in fear. The purpose of it is to force
    from one tongue, amid its screams and its vomiting up of blood, the secret of
    everything. Senseless violence: whether the victim talks or whether he dies
    under his agony, the secret that he cannot tell is always somewhere else and
    out of reach. It is the executioner who becomes Sisyphus. If he puts the
    question at all, he will have to continue forever."

### Tradition, secrets, truth and torture

    The Gestapo taught the French, who taught the Americans in Indo-China, and
    they passed on some of their expertise to the Argentinian, Chilean, El
    Salvadoran torturers. But this essay is not meant to be a genealogy of modern
    torture. Rather I am concerned with what Sartre calls “the secret of
    everything” with the relationship between torture and the truth, which “is
    always somewhere else and out of reach.”

A crucial point (an "crucial" also in the sense of the crucified, tortured body):

    I want to show how the logic of our philosophical tradition, of some of our
    inherited beliefs about truth, leads almost inevitably to conceiving of the
    body of the other as the site from which truth can be produced, and to using
    violence if necessary to extract that truth.

    [...]

    I want to work out how the Greek philosophical idea of truth was produced
    in history and what role the social practice of judicial torture played in its
    production.

    I don’t want to suggest that the ancient Greeks invented torture, or that it
    belongs exclusively to the Western philosophical tradition, or that abhorrence
    of torture is not also part of that tradition. But I also refuse to adopt the
    moral stance of those who pretend that torture is the work of “others,” that it
    belongs to the third world, that we can condemn it from afar. To stand thus is
    to eradicate history, to participate both in the exportation of torture as a
    product of Western civilization, and in the concealment of its ancient and
    perhaps necessary coexistence with much that we hold dear. The very idea of
    truth we receive from the Greeks, those ancestors whom Allan Bloom names for
    us,3 is inextricably linked with the practice of torture, which has almost
    always been the ultimate attempt to discover a secret “always out of reach.”

    [...]

    The ancient Greek word for torture is basanos. It means first of all the
    touchstone used to test gold for purity; the Greeks extended its meaning to
    denote a test or trial to determine whether something or someone is real or
    genuine. It then comes to mean also inquiry by torture, “the question,”
    torture.4 In the following pages I will discuss the semantic field of the word
    basanos, its uses in various contexts, both literal and metaphorical. [...]
    This analysis will lead me to consideration of the idea of truth as secret in
    ancient Greek thought, in literary, ritual, and philosophical practices

    [...]

    The desire for a reliable test to determine the fidelity of a suspect
    intimate recurs in Greek poetry, and later poets often employ the metaphor of
    the testing of metal to describe the necessity and unreliability of testing for
    the fidelity of friends.

    The Lydians of Asia Minor had invented the use of metal currency, of money, in
    the seventh century B.C.E. The polis or city-state of Aegina was reputed to be
    the first Greek city to establish a silver coinage; in the classical period
    several different coinages circulated. By the fifth century B.C.E. coins of
    small enough denominations existed to enter into the economic transactions of
    daily life. In Euripides’ Hippolytus the Athenian king Theseus, bewildered by
    contradictory accounts of an alleged seduction attempt by his son against his
    wife, uses monetary language to convey his confusion about the mysteries of
    domestic intimacy:

    [...]

    Theseus employs the language of the banker, of the money-lender, to suggest
    that one of his friends, that is, of those dear to him, either his son or his
    wife, is false and counterfeit.

    [...]

    pollution is a religious term, connected with the impurity of blood shed,
    of unclean sacrificial practices or murder.10
    In the archaic period, the state of freedom from pollution is sometimes
    connected with notions of inherited purity, of uncontaminated descent from the
    generations of heroes, from the gods, ideas of inherited excellence through
    which the aristocrats justified their dominance in the archaic cities.

### Governing, the steering of a ship in the hands of the aristocracy

    The very last lines of the poem echo the concerns of Theognis, a recognition of
    the political disturbances of the ancient city, and the desire of the
    aristocratic poet for a steady hand, sanctioned by blood and tradition, at the
    city’s helm: 

        Let us praise
        his brave brothers too, because
        they bear on high the ways of Thessaly
        and bring them glory.
        In their hands
        belongs the piloting of cities, their fathers’ heritage.
        (68-72) [poet Pindar, in Pythian 10]

    This last phrase might be rendered: “in the care of the good men [Theognis’s
    agathoi, a political term] lie the inherited, paternal pilotings, governings
    [following the metaphor of the ship of state] of cities.” The city is a ship
    that must be guided by those who are capable by birth of piloting it, that is
    to say, the agathoi, the good, the aristocrats. The basanos reveals the good,
    separates base metal from pure gold, aristocrat from commoner.

### A change of meaning

    The Sophoclean language, and its ambiguity, reveal the gradual transition of
    the meaning of the word basanos from “test” to “torture.” The literal meaning,
    “touchstone,” gives way to a figurative meaning, “test,” then over time changes
    to “torture,” as the analogy is extended to the testing of human bodies in
    juridical procedures for the Athenian courts. Is the history of basanos itself
    in ancient Athens a process of refiguration, the alienation of the test from a
    metal to the slave, the other? Such a transfer is literally catachresis, the
    improper use of words, the application of a term to a thing which it does not
    properly denote, abuse or perversion of a trope or metaphor (OED); George
    Puttenham, the Elizabethan rhetorician, calls catachresis “the figure of
    abuse.” The modern English word touchstone is similarly employed by people who
    have no idea of the archaic reference to the lapis Lydius, also called in
    English basanite . The figurative use of the word touchstone has taken the
    place of the literal meaning.

    [...]

    The forensic language of Oedipus Rex fuses heroic legend with the poetic
    representation of the city’s institutions. The mythic narrative of Oedipus’s
    encounter with the Sphinx, set in the most remote past, and the struggle
    between Kreon and Oedipus over the investigation of an ancient and mythic
    homicide, here meet the daily life of the democratic polis. The language of
    Sophokles’ tragedy might be said to exemplify not only the contradictions
    between the tyranny of the fictional past and the “secret ballots,” alluded to
    disparagingly by Pindar, of the audience’s present, but also to represent
    dramatically, in an almost Utopian manner, a synthesis of the Greeks’ legendary
    origins and their political processes. The chorus’s attempt to judge Oedipus
    resembles the aristocratic reveler’s testing of his fellow symposiasts, using
    as it does archaic, lyric language; it is also like the democratic jury’s
    testing of a citizen on trial, alluding obliquely at the same time to the
    juridical torture of slaves in the Athenian legal system, a process which by
    this time was referred to as the basanos.
    
    Some of the semantic processes that transformed basanos as touchstone into the
    term for legal torture can be seen in the use of the term in the Oedipus
    Coloneus. This tragedy is only obliquely concerned with the process of
    democracy, with the new institutions of the mid-fifth century which mediated
    between the city’s aristocratic past and its democratic present. It speaks
    instead of the exhaustion of the political, of disillusionment with parties and
    with war, of metaphysical solutions to problems too bitter to be resolved in
    mortal agones. 

    [...]

    The basanos, no longer an autonomous, inert, inanimate tool for assaying
    metal, has become a struggle between two forces, a contest that assumes
    physical violence, a reconcretizing of the “touchstone,” which is neither the
    literal stone nor a metaphorical ordeal. Hands, pitted one against the other,
    rematerialize the test. The touchstone sets stone against metal; the test of
    friends sets one against another; here the agon, the contest implicit in the
    notion of basanos, takes on a new connotation, one of combat between enemies.
    
    Some historians of the ancient city believe that the word basanos refers not to
    physical torture, but to a legal interrogation that does not involve violence.
    Others claim that the threat of torture may have been present in the word, but
    that there is no evidence that torture was actually ever practiced.16 It seems
    to me very unlikely , even though the ancient evidence does not describe
    directly any single case of torture, that the frequent mentions of basanos in
    contexts of physical intimidation can refer to anything but the practice of
    torture. The accidents of survival of ancient material may mean that we have no
    single documented instance of torture having been applied, but the many uses of
    the term, not only in the context of the law court, suggest a cultural
    acceptance of the meaning “torture” for basanos, and an assumption that torture
    occurred. For example, the historian Herodotos, in recounting an incident that
    took place during the Persian Wars, in the early years of the fifth century
    B.C.E., describes the Athenian hero Themistokles’ secret negotiations with the
    Persian emperor Xerxes after the battle of Salamis: 
    
    Themistocles lost no time in getting a message through to Xerxes. The men he
    chose for this purpose were all people he could trust to keep his instructions
    secret [sigan], even under torture [es pasan basanon].

    [...]

    This passage suggests not only that basanos was not merely interrogation, but
    that with the meaning torture it formed part of the vocabulary of daily life,
    and that torture figured in the relations between ancient states as well as in
    the legal processes of the democratic city. Themistokles had to take into
    account the ability of his emissaries to resist physical torture, pasan
    basanon, “any, all torture,” when deciding whom to send to the Persian emperor.
    He required silence under extreme interrogation, since he was claiming falsely
    to have protected Xerxes from the pursuit of the Greeks after the Persians’
    defeat. And Herodotos uses the word basanos as if the meaning “torture” were
    common currency.
    
    As do Sophokles’ Oedipus plays, Herodotos’s text offers a double vision,
    providing further evidence about the place of torture in the democracy and in
    its prehis-tory. Sophokles writes from within the democracy about episodes from
    the archaic, legendary past of the city. Herodotos writes from the world of the
    mid-fifth century, the time of Sophokles and the great imperial age of Athens,
    looking back half a century. The victories of Athens in the Persian Wars had
    enabled and produced the great flowering of Athenian culture and ambition in
    the middle of the fifth century. Herodotos’s retrospective gaze at the origins
    of the democracy and its empire paints the portrait of Themistokles, one of the
    great aristocrats whose power and vision shaped the evolution of the democratic
    city. His encouragement, for example, of the policy of spending the city’s
    mining wealth on its fleet, rather than distributing of monies to the citizens,
    meant that the poorest citizens in Athens, who manned the fleet, participated
    actively and powerfully in the political and military decisions of the
    following years. In the incident Herodotos describes, Themistokles takes care
    to ensure that his self-interested machinations not be known by the Athenians
    he led. Themistokles, like Oedipus, has become a creature of legend.

    [...]

    Silence under torture may be coded as an aristocratic virtue. [...] it
    indicates the degree to which silence under pain is ideologically associated
    with nobility. The slave has no resources through which to resist submitting to
    pain and telling all. In contrast the aristocratic soldier, noble by both birth
    and training, maintains laconic silence in the face of physical abuse.

### Comedy and inversion

    We find a comic parody of the use of basanos for the courtroom in Aristophanes’
    Frogs.20 The comedy is devoted to themes of judgment, discrimination, and
    evaluation . Dionysos and his slave Xanthias have set off on a journey to Hades
    to retrieve the tragic poet Euripides, but end by choosing Aeschylus to bring
    back with them, claiming him over Euripides as the superior poet. Dionysos had
    dressed for the trip as Herakles, who had once successfully entered and, more
    importantly, departed the realm of the dead, but when he learns that Herakles
    is persona non grata in Hades, he forces his slave to trade costumes with him.
    When Xanthias is mistaken for Herakles and about to be arrested as a
    dog-napper, for the stealing of Kerberos, the slave offers to give up his own
    supposed slave, really the god Dionysos, to torture

    [...]

    The result is a beating contest in which Xanthias seems sure to win, accustomed
    as he, a real slave, is to such beatings. The beatings constitute not
    punishment but torture, and the language of the comedy reflects this fact. The
    torture will reveal the truth, show which of the two is a god, which a slave.
    The comedy works on the reversal of slave and god; Xanthias claims a god would
    not be hurt by a beating, but the slave, the lowest of mortal beings, might in
    fact be thought, because of experience, most easily to endure a whipping.
    Dionysos begins to weep under the beating, but claims it’s due to onions.
    Aiakos is finally unable to decide which of the two is divine.
    
    In the parabasis, the address to the audience, that follows, the theme of noble
    and base currency emerges once again, as if connected by free association with
    this scene of torture, of the basanos or touchstone. The chorus appeals to the
    Athenian populace, complaining that Athenians who had committed one fault in a
    battle at sea were to be put to death, while slaves who had fought alongside
    their masters had been given their freedom. This seems to the chorus to be a
    perversion of traditional hierarchical thinking:

    [...]

    The comic beating is quite hilarious, of course. But it does not put into
    question the reality of torture. The exchange has a carnival quality, Dionysos
    masquerading as slave, slave masquerading as Dionysos masquerading as Herakles,
    the god beaten like a common slave. The slave remains uppity and insolent, the
    god cowardly and ridiculous. Comedy permits this representation of the
    quotidian reality of the polis, the exposure of what cannot be alluded to
    directly in tragedy, the violence and domination implicit in the situation of
    bondage. Comedy allows the fictional depiction of the unspeakable, the
    representation of the lowly slave, the allusion to ordinary cruelty, a
    commentary on the difficulty of perceiving the essential difference between
    divine and enslaved beings.
    [...] If Aristophanes is so iconoclastic as to mock the gods, and to treat
    the slave as if he were a human character, he does not go so far as to question
    the institution of the basanos. 

### Testing

    The slave on the rack waits like the metal, pure or alloyed, to be tested.
    [...] The test assumes that its result will be truth; [...] The truth is
    generated by torture from the speech of the slave; the sounds of the slave on
    the rack must by definition contain truth, which the torture produces. And when
    set against other testimony in a court case, that necessary truth, like a
    touchstone itself, will show up the truth or falsity of the testimony. The
    process of testing has been spun out from the simple metallurgist’s experiment,
    to a new figuration of the work of interrogating matter. It is the slave’s
    body, not metal, which receives the test; but how can that body be demonstrated
    to be true or false, pure or alloyed, loyal or disloyal? The basanos assumes
    first that the slave always lies, then that torture makes him or her always
    tell the truth, then that the truth produced through torture will always expose
    the truth or falsehood of the free man’s evidence.

### Athenian democracy and torture

    Yet the Athenian democracy was at best a sort of oligarchy, one that denied
    legal and political rights to all women, even daughters of citizens, and to
    foreigners and slaves residing in Attica. The practice of slave torture is
    consistent with the democracy’s policies of exclusion, scapegoating, ostracism,
    and physical cruelty and violence; to overlook or justify torture is to
    misrecognize and idealize the Athenian state.

    MacDowell describes the place of torture in the Athenian legal system: 

        A special rule governed the testimony of slaves: they could not appear in
        court, but a statement which a slave, male or female, had made under torture
        (basanos) could be produced in court as evidence.3

        The party in a trial who wished a slave to be tortured would put his questions
        in writing, specifying which slaves he wished to have tortured and the
        questions they were to be asked, and also agreeing to pay the slave’s owner for
        any permanent damage inflicted on the slave. Athenian citizens could not be
        tortured.

    MacDowell reasons as follows about the rule that slave’s testimony could
    be received in the courtroom only if the slave had been tortured: 

        The reason for this rule must have been that a slave who knew anything material
        would frequently belong to one of the litigants, and so would be afraid to say
        anything contrary to his owner’s interests, unless the pressure put on him to
        reveal the truth was even greater than the punishment for revealing it which he
        could expect from his master.4

    A. R. W. Harrison believes that the right to testify freely in court may have
    been seen as a privilege, perhaps because witnesses who appeared in court were
    once thought of as “compurgators,” witnesses who swore to the credibility of a
    party in a law suit. “Torture must therefore be applied to the slave as a mark
    of the fact that he was not in himself a free agent entitled to support one
    side or the other.”5 Since the slave was a valuable piece of property, liable
    to damage from torture, she or he could not be tortured without permission of
    the owner.6 If that permission were denied, the opponent often claimed that the
    evidence which would have been obtained under torture would of certainty have
    been damning to the slave’s owner.

### Slavery and freedom

    Jean-Paul Sartre’s [...] says: “Algeria cannot contain two human species, but
    requires a choice between them”.1 The soldiers who practiced torture on
    Algerian revolutionaries attempted to reduce their opponents to pure
    materiality, to the status of animals.

    [...]

    Free men and women could be enslaved at any time, although in Athens the
    Solonian reforms of the sixth century B.C.E.

    [...]

    In the Politics Aristotle claims that some people are slaves “by nature”

    [...]

    The discourse on the use of torture in ancient Athenian law forms part of an
    attempt to manage the opposition between slave and free, and it betrays both
    need and anxiety: need to have a clear boundary between servile and free,
    anxiety about the impossibility of maintaining this difference.

### Tendency of suspend democracy protections (coup) during crisis

    [The] incident of the mutilation of the herms shook the stability of the
    Athenian state, but also points to the future tendencies among the aristocratic
    and oligarchic parties to suspend democratic protections in a moment of crisis.
    The logical tendency would seem to be either to extend torture to all who could
    give evidence, or to forbid torture of any human being. The instability of the
    distinctions between slave and free, citizen and noncitizen, Greek and
    foreigner, becomes apparent in these debates on the possibility of state
    torture of citizens. Athenian citizens treasured the freedom from torture as a
    privilege of their elevated status; Peisander’s eagerness to abrogate this
    right is a premonition of the violence and illegality of the oligarchic coup of
    411 and of the bloody rule of the Thirty after the defeat of the Athenians by
    the Spartans in 404.5

### Truth-making and the secret of everything

    Another kind of truth, what Sartre calls “the secret of everything,” is named
    by many Greek writers as the explicit aim of judicial torture. [,,,] 
    In the Greek legal system, the torture of slaves figured as a guarantor of
    truth, as a process of truth-making.

### Secret ballots and lawyers

    Jurors received pay for their service in the courts [...] A case was required
    to be completed within a day at most; several private cases would be tried
    within a single day. The time alloted for arguments by the opponents in a trial
    was measured by a water-clock; the amount of water allowed for each side was
    determined by the seriousness of the charges. After each of the litigants
    spoke, brought forward witnesses, and had evidence read, the jurors placed
    disks or pebbles into urns to determine the winner of the suit. 
    [...] At the end of the fifth century it became customary to employ professional
    writers to compose one’s speech for the court.

    Thus the scene in the court resembled the great assemblies of the democratic
    city, with up to six thousand men adjudicating disputes. [...]
    In this context, the evidence from the torture of slaves is evidence from
    elsewhere, from another place, another body. It is evidence from outside the
    community of citizens, of free men. Produced by the basanistês, the torturer,
    or by the litigant in another scene, at the time of torture, such evidence
    differs radically from the testimony of free witnesses in the court. It is
    temporally estranged, institutionally, conventionally marked as evidence of
    another order; what is curious is that speakers again and again privilege it as
    belonging to a higher order of truth than that evidence freely offered in the
    presence of the jurors by present witnesses.

    [...]

    There are many such passages. The slave body has become, in the democratic
    city, the site of torture and of the production of truth.

    The argument concerning the greater value of slave evidence frequently occurs
    in accusations against an opponent who has refused to allow his slaves to be
    tortured. The speaker claims then that this failure to produce slave witnesses
    proves indirectly that their testimony would condemn their owner.

### Slaves are bodies; citizens possess logos

    Lykourgos argues against Leokrates [...]:

        Every one of you knows that in matters of dispute it is considered by far the
        just and most democratic [dêmotikôtatori] course, when there are male or female
        slaves, who possess the necessary information, to examine these by torture and
        so have facts to go upon instead of hearsay, particularly when the case
        concerns the public and is of vital interest to the state.

    He argues that by nature the tortured slaves would have told the truth; does
    this mean that any human being, when tortured, will produce the truth, or that
    it is the nature of slaves to tell the truth under torture? Free citizen men
    will be deceived by clever arguments; slaves by nature will not be misled
    because they think with their bodies. Slaves are bodies; citizens possess
    logos, reason. [...] This appeal to the practice of torture as an integral
    and valued part of the legal machinery of the democracy points up the
    contradictory nature of Athenian democracy, and the ways in which the
    application of the democratic reforms of Athens were carefully limited to the
    lives of male citizens, and intrinsic to the production and justification of
    this notion of male citizenship.

### Notions of truth and the real of the untorturable, the dead

    In another speech attributed to Antiphon, this one part of a case presumably
    actually tried, torture again plays an important role in the defense of a man
    accused of murder, Euxitheos [...]

        Probably both of these considerations induced him to make the false charges
        against me which he did; he hoped to gain his freedom, and his one immediate
        wish was to end the torture. I need not remind you, I think, that witnesses
        under torture are biased in favour of those who do most of the torturing; they
        will say anything likely to gratify them. It is their one chance of salvation,
        especially when the victims of their lies happen not to be present. Had I
        myself proceeded to give orders that the slave should be racked [strebloun] for
        not telling the truth, that step in itself would doubtless have been enough to
        make him stop incriminating me falsely. (5.31-32)

    The speaker, because for once forced to confront the evidence of a tortured
    slave, rather than bemoaning the lack of slave evidence, here points out the
    absolute unreliability of slave evidence, based as it is on the will of the
    torturer. Bizarrely, however, he ends by claiming that the true truth would
    have emerged, another truth truer than the first, if he himself had been the
    torturer. If so, it is interesting that the defendant distinguishes between
    an essentialist notion of truth and a pragmatic notion of truth in the case of
    the slave, but not in the case of the free foreigner, where a reversion to the
    essentialist notion appears to occur. And the logic he exposes, that the slave
    will say anything to gratify his torturer, is dropped as soon as he himself
    becomes the torturer. We can imagine the body of the slave ripped apart in a
    tug-of-war between two litigants, in a law case in which he was implicated only
    by proximity.

    This very slave, who had been purchased by the prosecution, in fact later
    changed his testimony, according to the speaker because he recognized his
    imminent doom. Nonetheless the prosecution put him to death. The defendant
    continues: 
    
        Clearly, it was not his person, but his evidence, which they required; had the
        man remained alive, he would have been tortured by me in the same way, and the
        prosecution would be confronted with their plot: but once he was dead, not only
        did the loss of his person mean that I was deprived of my opportunity of
        establishing the truth, but his false statements are assumed to be true.
        (5.35)
    
    All of the prosecution’s case rests on the testimony of the tortured and now
    dead slave; the defendant claims to be completely frustrated, since now the
    truth lies in a realm inaccessible to him. He cannot torture the dead man and
    discover the “real” truth. Even though the slave had at first insisted on the
    defendant’s innocence, he had under torture called him guilty: 
    
        At the start, before being placed on the wheel [trokhon], in fact, until
        extreme pressure was brought to bear, the man adhered to the truth [alêtheia]
        and declared me innocent. It was only when on the wheel, and when driven to it,
        that he falsely incriminated me, in order to put an end to the torture.
        (5.40-41)
    
    The persistence of the defendant’s desire himself to torture this slave claims
    our attention; even after the inevitability of false testimony under torture
    stands exposed, he bemoans the retreat of the slave into the realm of the
    untorturable, of the dead.

        I repeat, let no one cause you to forget that the prosecution put the informer
        to death, that they used every effort to prevent his appearance in court and to
        make it impossible for me to take him and examine him under torture on my
        return.… Instead, they bought the slave and put him to death, entirely on their
        own initiative [idia], (5.46-47)

### Reasoning attributed to a free man, but foreigner

    The free man knew that the torture would end; he also could not be bribed by
    promises of freedom for giving the answers the torturers desired to hear. In
    this case, the defendant gives priority to the free man’s unfree testimony;
    unlike the free testimony of an Athenian in a courtroom, this evidence was
    derived from torture, but the defendant seeks to give it the added authority of
    the free man in spite of its origin in this procedure tainted with unfreedom,
    because it supports his view of the case.

### Truth even if it cost lives

        You do not need to be reminded, gentlemen, that the one occasion when
        compulsion [anagkai] is as absolute and as effective as is humanly possible,
        and when the rights of a case are ascertained thereby most surely and most
        certainly, arises when there is an abundance of witnesses, both slave and free,
        and it is possible to put pressure [anagkazein] upon the free men by exacting
        an oath or word of honour, the most solemn and the most awful form of
        compulsion known to free men, and upon the slaves by other devices [heterais
        anagkais], which will force them to tell the truth even if their revelations
        are bound to cost them their lives, as the compulsion of the moment [he gar
        parousa anagkê] has a stronger influence over each than the fate which he will
        suffer by compulsion afterwards. (6.25)

    That is, the free man is compelled by oaths; he might lose his rights as a
    citizen if he lied under oath. The slave, even though he will certainly be put
    to death as a consequence of what he reveals under torture, will nonetheless,
    under torture, reveal the truth. The two kinds of compulsion are equated, one
    appropriate for the free man, one for the slave.

### Torturability

    Torture serves not only to exact a truth, some truth or other, which will
    benefit one side of the case or the other. It also functions as a gambit in the
    exchange between defendant and prosecution; if for any reason one of them
    refuses to give up slaves to torture, the other can claim that the missing
    testimony would of a certainty support his view of things. And as I argued
    earlier, torture also serves to mark the boundary between slave and free
    beings. Torture can be enacted against free, non-Greek beings as well as
    slaves; all “barbarians” are assimilated to slaves. Slaves are barbarians,
    barbarians are slaves; all are susceptible to torture. Torturability creates a
    difference which is naturalized. And even the sophistry of the First Tetralogy,
    which wants to create a category of virtually free in the case of the slave who
    would have been freed had he lived, seeks to support this division of human
    beings into free, truth-telling creatures, and torturable slave/barbarians, who
    will only produce truth on the wheel.

### The Slave's Truth

    Torture performs at least two functions in the Athenian state. As an instrument
    of demarcation, it delineates the boundary between slave and free, between the
    untouchable bodies of free citizens and the torturable bodies of slaves. The
    ambiguity of slave status, the difficulty of sustaining an absolute sense of
    differences, is addressed through this practice of the state, which carves the
    line between slave and free on the bodies of the unfree. In the work of the
    wheel, the rack, and the whip, the torturer carries out the work of the polis;
    citizen is made distinct from noncitizen, Greek from barbarian, slave from
    free. The practice of basanos administers to the anxiety about enslavement,
    hauntingly evoked in the texts of Athenian tragedy that recall the fall of
    cities, particularly the fall of Troy, evoked as well in the histories that
    recount Athenian destruction of subject allies.

    [...]

    But the desire to clarify the respective status of slave and free is not the
    motive, never the explicit motive, of torture. Rather, again and again, even in
    the face of arguments discounting evidence derived from torture, speakers in
    the courts describe the basanos as a search for truth. How is this possible?
    And how are the two desires related? The claim is made that truth resides in
    the slave body.

    [...]

    That is, the master possesses reason, logos. When giving evidence in court, he
    knows the difference between truth and falsehood, he can reason and produce
    true speech, logos, and he can reason about the consequences of falsehood , the
    deprivation of his rights as a citizen. The slave, on the other hand,
    possessing not reason, but rather a body strong for service (iskhura pros ten
    anagkaian khrêsin), must be forced to utter the truth, which he can apprehend,
    although not possessing reason as such. Unlike an animal, a being that
    possesses only feelings, and therefore can neither apprehend reason, logos, nor
    speak, legein, the slave can testify when his body is tortured because he
    recognizes reason without possessing it himself.

    [...]

    Thus, according to Aristotle’s logic, representative or not, the slave’s truth
    is the master’s truth; it is in the body of the slave that the master’s truth
    lies, and it is in torture that his truth is revealed. The torturer reaches
    through the master to the slave’s body, and extracts the truth from it. The
    master can conceal the truth, since he possesses reason and can choose between
    truth and lie, can choose the penalty associated with false testimony. His own
    point of vulnerability is the body of his slave, which can be compelled not to
    lie, can be forced to produce the truth. If he decides to deny the body of his
    slave to the torturer, assumptions Will be made that condemn him.

    [...]

    Aristotle advocates the pragmatic approach; one can argue either side concerning
    the truth of torture.

    [...]

    As Gernet says, “Proof is institutional.” Proof, and therefore truth, are
    constituted by the Greeks as best found in the evidence derived from torture.
    Truth, alêtheia, comes from elsewhere, from another place, from the place of
    the other.

### Torture and Writing

    The tortured body retains scars, marks that recall the violence inflicted upon
    it by the torturer. In part because slaves were often tattooed in the ancient
    world, such marks of torture resonate in the Greek mind with tattoos, and with
    other forms of metaphorical inscription, in Greek thinking considered analogous
    to writing on the body.1 I have discussed the topos of corporeal inscription
    elsewhere. The woman’s body was in ancient Greece sometimes likened to a
    writing tablet, a surface to be ”ploughed,” inscribed by the hand, the plough,
    the penis of her husband and master.2

The famous case of the tatooed head:
    
    One especially intriguing mention of slave tattooing occurs in Herodotos’s
    Histories, in a narrative in which the possibility of torture remains implicit.
    Although I have discussed this episode elsewhere, I want here to draw out its
    implications for a consideration of the relationship between torture and truth.
    Histiaios of Miletus sends a message urging revolt to a distant ally by shaving
    the head of his most trusted slave, tattooing the message on the slave’s head,
    then waiting for the slave’s hair to grow back. He sends the slave on his
    journey, ordering him to say at the journey’s end only that the “destinataire,”
    the receiver of the message, should shave off his hair and look at his head.
    The message reaches its goal, and Aristagoras the receiver revolts (Herodotos,
    Histories 5.35).
    
    The tattooed head is a protection against torture. If the slave were captured
    and tortured, he would not himself know the message of revolt. He could not
    betray his master if questioned and interrogated specifically about his
    master’s intentions to rise up against those who have enslaved him. He did not
    know the content of Histiaios’ communication with Aristagoras. But he did know
    the instructions he bore to Aristagoras, to shave his head and read the message
    inscribed there. The ruse only displaces the discovery of the message’s truth
    by a single step, but in this case it succeeds in protecting the message. Here
    the tattooing, the inscription on the slave’s body, subverts the intention of
    torture to expose the truth.

"Branding":

    In other contexts in ancient Greece, slave tattooing serves as a sort of label.
    It is as if writing on the slave body indicated the contents of that body. Such
    a function of writing recalls the work of Denise Schmandt-Besserat, who argues
    that writing originates in the markings on the outside of packages recording
    their contents.3 Aristotle points out in the Politics, as we have seen, that
    the slave body ought to reveal its truth, ought to be immediately perceptible
    as a servile body to the eye, but in fact sometimes it is not. A tattoo on a
    slave reveals his or her true status. In Aristophanes’ Babylonians, of which
    only fragments remain, we learn that prisoners of war were sometimes branded or
    inscribed with a mark indicating the city they served.4

    [...]

        though he is a human being, he does not know himself, soon he will know, having
        this inscription on his forehead. (74_79)6

    Herodes invokes the inscription at Delphi, also cited by Plato: gnôthi seauton,
    “Know yourself.”7

    [...]

    This placement of the “epigram,” whatever it is, if it is that, on the metope,
    the forehead of the slave, makes the inscription a sign. The message of
    Herodotos’s slave was concealed by his hair, directed to a specified other, the
    recipient who received the slave as a vehicle for his master’s words. The
    communication was not directed to the slave himself. In the case of Herodes’
    slave, the man named “Belly” would bear a sign meant to remind him of his
    humble status.

### Buried Truth

    If torture helped to manage the troublesome differentiation between slave and
    free in the ancient city, it also served as a redundant practice reinforcing
    the dominant notion of the Greeks that truth was an inaccessible, buried
    secret. In his valuable book Les maîtres de vérité dans la Grèce archaïque,
    Marcel Detienne describes a historical shift in the Greeks’ ideas about truth
    that corresponds to the historical shift from mythic to rational thought.1
    According to Detienne, Alêtheia is at first conceived of by the Greeks in an
    ambiguous relationship with Lethe, forgetting; truth is the possession of the
    poet and the just king, who has access to this truth through memory. Alêtheia
    is caught up in a relationship of ambiguity with Lêthê because, for example,
    the poet who speaks truth by using memory also confers truth’s other,
    forgetfulness, oblivion of pain and sorrow, on his listeners. His
    “magico-religious” speech, as Detienne calls it, which exists in an ambiguous
    relationship with truth, persists as the dominant form in the Greek world until
    the speech of warriors, the citizens who form the city’s phalanxes, a speech of
    dialogue, comes to dominate the social world in the time of the polis. Detienne
    associates a resultant secularization of poetic Alêtheia with the name of the
    poet Simonides. Doxa, seeming, becomes the rival province of sophistic and
    rhetorical speech, while Alêtheia comes to belong to an unambiguously
    “philosophico-religious” domain. In this field of discourse the logic of
    ambiguity typical of the Alêtheia-Lêthê relationship is replaced by a logic of
    contradiction, in which Alêtheia is opposed to Apatê, deception, as its other.
    The common use of memory provides a link between these two stages of thinking
    truth; the secularization of speech marks a break between a mythic and a
    rationalist semantic field in which the term Alêtheia persists.

### The modern word Tortura

    A word used in addition to alêthês in the Odyssey is atrekês, real, genuine,
    with a connotation perhaps of that which does not distort or deviate. The Latin
    word torqueo means “to twist tightly, to wind or wrap, to subject to torture,
    especially by the use of the rack.” This word may come from the root trek-,
    also occurring in Greek, which may give us atraktos, “spindle,” and also
    “arrow.”6 (Tortor is used as a cult title of Apollo, “perhaps”, according to
    the Oxford Latin Dictionary, “from the quarter at Rome occupied by the
    torturers.”)7 Our English word “torture” is taken from this Latin root. The
    Oxford English Dictionary defines “torture,” an adaptation of the Latin
    tortura, in the following way: 
    
        The infliction of excruciating pain, as practised by cruel tyrants, savages,
        brigands, etc., from the delight in watching the agony of a victim, in hatred
        or revenge, or as a means of extortion; spec. judicial torture, inflicted by a
        judicial or quasi-judicial authority, for the purpose of forcing an accused or
        suspected person to confess, or an unwilling witness to give evidence or
        information.8 Although the writers of the dictionary list first tyrants,
        savages, and brigands as the agents of torture, the first entry in their
        citations of the use of the word in English refers to the Acts of the Privy
        Council of 1551. This set of connotations, to return to the point, links the
        English word torture with the twisted, the distorted, and suggests that the
        truth gained as a confession is in English not conceived of as a straight line,
        but is rather bent, extorted from time on the rack.
    
    In Greek, however, not passing through Latin into English for our etymology
    (etumos is another word used for “true” in Greek), we have, parallel to
    atrekês, the word alêtheia with its suggestion of hiddenness and forgetting.
    The connotations of the alternative words nêmertês and atrekês are respectively
    “not missing the mark”, and “not deviating from an existing model”; the weight
    of alêthês rests instead on the trace of something not forgotten, not slipping
    by unnoticed.9

    [...]

    Even in the texts of the Hippocratic tradition, the body is seen to contain
    secrets that must be interpreted, elicited by signs that emerge onto the body’s
    surface, as the emanation from the earth arises to possess the Pythia.

    [...]

    Each of these sites of meaning in ancient culture—the epic, oracles, sacred
    buildings, the medicalized body—lay out a pattern of obscure, hidden truth that
    must be interpreted.

### Heraclitian truth differs

    The works of the pre-Socratic philosophers (even to presume to call them
    philosophers may be to presume too much) present problems of reading for the
    historian of philosophy, for the literary and cultural critic. Even the problem
    of who is disciplinarily responsible for these texts is insoluble. And the
    incompleteness of many pre-Socratic texts causes unease. How can one speak of
    philosophical development when only one line, one metaphor, one aphorism
    remains, torn out of context, lines repeated to illustrate a well-known point?
    The ellipses in the published pre-Socratic fragments recall stopped mouths,
    messages gone astray, the utter failure of communication across a distance of
    centuries.

    [...]

    The very search for integrity and indivisibility in all things has been called
    into question by the heirs of Nietzsche, among them those feminists who see the
    emphasis on wholeness and integrity, on the full body, as a strategy of
    scholarship that has traditionally excluded the female, who has been identified
    as different, heterogeneous, disturbing the integrity of the scholarly body,
    incomplete in herself. Aristotle describes the female as a “deformed”
    (pepêrômenon) male (Generation of Animals 737a), and argues further that her
    contribution to reproduction lacks a crucial ingredient, the principle of soul
    (psukhe). The project of scientific textual studies has been to supply the
    text’s lack, to reduce the fragmented, partial quality of embodied, material
    texts, to reject the defective text as it rejects the defective female. Like
    the slave body that needs the supplement of the basanos to produce truth, the
    female body and the fragmentary text are both constructed as lacking.

    [...]

    Elsewhere Herakleitos seems to argue against an innate hierarchy of mortal
    beings: “War is father of all, and king of all. He renders some gods, others
    men; he makes some slaves, others free” (fr. 53). Mortal and immortal status
    depends on human history, on events. There is no essence, no absolute truth in
    the differences among beings. Circumstances, history, time affect relations of
    difference and power.2 This relativism establishes a ground for the vision of
    equality among citizens in ancient democratic ideology, and even further, a
    point from which to examine the commonly held view that some human beings are
    slaves by nature.
    
    Herakleitos represents an alternative to the essentializing concept of truth as
    a buried, hidden substance; he offers a temporal notion of truth, that the
    basanos of the physician is good at one time, at another time bad, that war
    creates slaves and free, a relative notion of truth. [...]
    Herakleitos’s relationship to time, change and process prefigures values of the
    democracy and of the pre-Socratic sophists whom the aristocratic philosophical
    tradition despised: “As they step into the same rivers, different and (still)
    different waters flow upon them” (fr. 12); “we step and do not step into the
    same rivers; we are and are not” (fr. 49a). His is not a doctrine of
    superficial appearance and deep truth, but rather a celebration of the
    mutability and interdependence of all things. The Heraclitean truth, read
    within his words, fragmentary as they are, celebrating flux, time, difference,
    allows for an alternative model to a hidden truth. [...]
    for him truth is process and becoming, obtained through
    observation, rather than a fixed, divine and immutable truth of eternity.

### Truth and memory

    The word anagkê, “constraint,” is associated with the yoke of slavery.10 All of
    human experience suffers from its subjection to necessity; the slave offers an
    extreme example of the general human condition. In one of her many forms the
    goddess who instructs the youth, the Kouros, is mistress of “brute force,” or
    of the bonds associated with enslavement, and is therefore binding the
    “what-is,” the “true,” in captivity. Like the slave who yields the truth to the
    torturer, the “what-is” is bound in domination, and delivers up its truth under
    necessity.

    [...]

    Does truth as eternally located elsewhere, either hidden in the body, or hidden
    in the earth, or hidden inside or beyond human existence, in some realm
    inaccessible to ordinary consciousness, lead by some tortuous path to the
    necessity for torture? Can we posit a truth of process and becoming, and
    another truth of eternity? If so, the word a-lêtheia seems to carry buried
    within it support for the view of hidden truth, of truth brought up from the
    depths. The possibility of forgetting leads to the imagination of a buried
    realm, the realm of forgetting, of Lethe, which can be represented either
    positively or negatively. It is good to forget suffering and pain, regrettable
    to forget a message, to forget crucial information that must be transmitted to
    a listener; in either case Lethe—or, to coin a word, “letheia”—remains a domain
    beyond consciousness.

    [...]

    The dominance of a notion of truth as alêtheia, not forgetting, he attributes
    in part to the gradual shift to literacy taking place in the fifth and fourth
    centuries.14 The legal corpus reflects the state of the problem of truth in the
    fifth and fourth centuries B.C.E. Charles Segal has discussed eloquently the
    ways in which growing literacy affects concepts of the self and truth in Greek
    tragedy.15

    [...]

    In the dominant literary and philosophical paradigm, the truth is seen to be
    forgettable, slipping away from notice, buried, inaccessible, then retrieved
    through an effort of memory, through the invocation of divine possession,
    through the interrogation by a privileged seeker of some enlightened source;
    seeking the truth may involve a journey, a passage through a spatial narrative
    of some sort, a request, a sinking down into the past, into the interiority of
    memory. This model of truth seeking is consistent with other such paradigms
    already suggested earlier, in the law courts, where, as we saw, the violence of
    the torturer is thought to be necessary to enforce the production of truth from
    the slave, either to force him or her to recall the truth, or to force him or
    her to speak the truth for the benefit of the court.
    
    The slave’s body is thus construed as one of these sites of truth, like the
    adyton, the underworld, the interiority of the woman’s body, the elsewhere
    toward which truth is always slipping, a Utopian space allowing a less
    mediated, more direct access to truth, where the truth is no longer forgotten,
    slipping away. The basanos gives the torturer the power to exact from the
    other, seen as like an oracular space, like the woman’s hystera, like the
    inside of the earth, the realm of Hades, as other and as therefore in
    possession of the truth. The truth is thus always elsewhere, always outside the
    realm of ordinary human experience, of everyday life, secreted in the earth, in
    the gods, in the woman, in the slave. To recall it from this other place
    sometimes requires patience, sometimes payment of gifts, sometimes seduction,
    sometimes violence.

### Torture and commodification

    Does it have to do with the invention of coinage, with the idea of abstract
    exchange value, and the slave as an exchangeable body, a thing to be tested
    like a coin, like a marker for exchange? In the Laws the Athenian says men are
    like puppets with strings, and that they should follow the soft, golden string,
    the “golden and hallowed drawing of judgment which goes by the name of the
    public law of the city” (644-45).
