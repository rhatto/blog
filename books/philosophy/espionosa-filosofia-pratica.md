[[!meta title="Espinosa: Filosofia Prática"]]

    Não é apenas uma questão de música, mas de maneira de viver: é pela velocidade e lentidão
    que a gente desliza entre as coisas, que a gente se conjuga com outra coisa: a gente nunca
    começa, nunca se recomeça tudo novamente, a gente desliza por entre, se introduz no meio,
    abraça-se ou se impõe ritmos.

    -- 128
