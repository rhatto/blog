[[!meta title="A Cidade Perversa"]]
[[!meta date="2018-10-07 12:00:00-0300"]]
[[!toc startlevel=2 levels=4]]

## Sobre

* Título: [A Cidade Perversa - Liberalismo e Pornografia](https://outrapolitica.wordpress.com/2010/05/27/a-cidade-perversa-liberalismo-e-pornografia/).
* Autor: [Dany-Robert Dufour](https://fr.wikipedia.org/wiki/Dany-Robert_Dufour).

## Impressões

* Uau! Como ressoa com as leituras de Elias, Marcuse e Hans Sachs.
* Marquês de Sade versus [Sady Baby](https://pt.wikipedia.org/wiki/Sady_Baby)?

## Resumo

O livro articula a mudança do pensamento ocidental, a partir de Pascal, do
paradigma agostiniano da Cidade Deus -- que seria caracterizada pelo amor a
Deus e ao próximo junto com o máximo desprezo de si (paradigma ultra-altruísta)
-- para a Cidade Perversa -- de característica oposta: egoísmo absoluto e
desprezo ao outro -- cuja melhor exemplificação até hoje seria dada pela obra
de Sade.

A noção então de que o capitalismo seria dada apenas pela orientação
egóico-puritana cujo expoente clássico é Adam Smith -- e de onde a harmonia da
Cidade dos Homens seria uma característica emergente, a "Mão Invisível" --
precisa ser modificada para incluir também o traço perverso. Daí que o
capitalismo contemporâneo seria associado a um comportamento simultaneamente
puritano e perverso.

Uma Cidade dos Homens operando como Cidade de Deus, a moral de cada cidadão
seria guiada pelo princípio do máximo altruísmo: cuidar de tudo e todos, se
preocupar com tudo e todos ao ponto de jamais cuidar de si. Num sistema
hipotético deste tipo, haveria uma tendência ao surgimento de uma harmonia
entre as pessoas pois umas cuidariam das outras onde o valor social seria dado
por uma espécie de "endividamento perpétuo", [conforme demonstrei para uma
sociedade hipotética caracterizada pela ajuda múltipla](/economics/valor-social/).

O capitalismo traria uma gradual, porém acelerada, mudança onde o egoísmo se torna
aceitável até a inversão da lógica da Cidade de Deus. Surge então a Cidade Perversa,
onde os cidadão seriam então estimulados a adotar a postura egoísta: se não adotarem,
acabam como presas fáceis de quem o adota. Surge então o valor anti-social e
a desajuda múltipla, ao contrário da harmonia prevista por Smith. Pascal é o
grande exemplo tanto por sua vida oscilando entre o extremo puritanismo e
arroubos de vida mundana, como especialmente pelas suas iniciativas empresariais.
Pascal seria o símbolo do início da permissão do ego para a satisfação das paixões
em vida:

    47

    No fragmento 458 dos Pensamentos, Pascal enumera três concupiscências (“três
    rios de fogo que abrasam a terra”), resultando da chegada do amor de si ao
    primeiro plano, em detrimento do amor de Deus: a paixão de ver e saber, a
    paixão dos sentidos e da carne e a paixão de dominar (libido sciendi, libido
    sentiendi, libido dominandi).

As implicações dessa mudança de valor é mascarada por um puritanismo hipócrita até
serem levadas às últimas consequências pela obra de Sade.

A relação maquínica de produção e consumo em escala industrial, articulando Marx
e Sade (vide seleção de trechos) é crucial.

## Análise

### Estudo de caso: Eleições Brasil Hostil 2018

Tive um sonho muito doido, talvez um dia depois de ter terminado o livro e já ter engrenado
na leitura da [Psicologia de Massas do Fascismo](/books/psychology/mass-psychology-of-fascism)
do Reich.

Nele, ocorre um debate político em que havia três candidatos no segundo turno
(uma tríade!), Haddad, Bolsonaro e uma terceira pessoa. Na pergunta sobre
ética, havia apenas duas possibilidades na mesa, a de Haddad e a do terceiro/a
candidato, já que Bolsonaro se apresentava sem sistema ético definido; Haddad
se apresentava como seguindo a ética de Morin, mas que se parecia com uma
versão cristã/satânica, perverso/puritana e formulada nos finais do período
medieval, mas ainda não renascentista.

Há um momento em que Bolsonaro é instado a assumir uma posição.  Ele, num
estado hesitante, talvez meio confuso, afirma que não sabe explicar o porquê,
como chegou à conclusão, mas julgava que sua ética era na linha moriniana...
fico estupefato, neste momento consto como expectador, a platéia aplaude porque
isso significa a virada para Haddad e a definição das eleições; o perverso fica
confuso; eu fico confuso - "peraí, a ética moriniana não tem nada disso"; vou
consultar uma "bula", onde está o resumo da ética, leio, é uma rabulagem na
linha do amor Dei (amor a Deus, ao próximo) / amor sui (amor próprio).

Seria o pai castrador capitulando? Ou o sádico "mítico" (e muitos o chamam
de mito) entendeu que Haddad pode representar um tipo de sadismo mais sutil
e talvez assim mais perverso por ser um neoliberal disfarçado de "trabalhista"
/ "social democrata"?

Não exatamente. Talvez Bolsonaro capitulava para Haddad acreditando que este
seria ainda mais perverso, por estar disfarçado de social-democrata disfarçado
de trabalhista mas por ser um neoliberal. Capitulava, como um sádico passando
a se sujeitar.

Será que isso não revelaria um pouco do balanço de economia psíquica
inconsciente rolando por aí?  Talvez haja uma grande parcela do eleitorado que
prefira a perversidade declarada, na qual é permitido que o indivíduo puritano
também pratique a perversão, ao invés de um perverso enrustido que não permita
a perversão generalizada, mas só a dele?

É o que a minha psicanálise de linha bacaniana (de Baco!) mostra da psicologia
de massas orientada ao fascismo.

A massa quer botar pra foder e descontar a raiva nos setores identitários pela
promessa não cumprida do antigo Pai Barbudo de que haveria consumo
indefinido... as pessoas foram incitadas a consumir, ainda são, mas não
conseguem mais tanto por motivos materiais quanto psíquicos. O Brasil atingiu
um certo limite do gozo possível nesta época.

Isso está de acordo com [esta análise econômica](https://www.brasil247.com/pt/247/mundo/371189/Pepe-Escobar-o-futuro-da-humanidade-está-sendo-jogado-no-Brasil.htm):

     O jornalista Pepe Escobar expõe sua análise quinzenal à TV 247, com
     perplexidade, dizendo que não previa uma ameaça fascista no Brasil,
     alertando que a vitória do candidato Jair Bolsonaro (PSL) pode jogar o
     Brasil num limbo econômico de subserviência aos EUA, afastando mercados
     estratégicos como o europeu e o asiático; "O futuro da humanidade está
     sendo jogado no Brasil", alerta o jornalista; assista a íntegra da
     análise do jornalista

     [...]

     Pepe considera que o grande capital prefere Haddad a Bolsonaro, porém,
     o candidato do PT teria que assinar a cartilha neoliberal. "O capital
     internacional vai cobrar seu preço altíssimo", avalia.

Aí recebo este voto de bom voto, esta bomba semiológica:

[[!img voto-prisao.jpg link="no"]]

Tou falando?! Pura coprofagia sado-masoquista no dia de eleger o próximo Marquês de Sade!

Agora, umas palavrinhas sobre sujeição:

    Urna eletrônica

Que equipamento curioso neste contexto, hein?

    Urna
    Deposite seu voto na urna
    BURP! PUM!

Por que não "saco preto"?

    Deposite seu canditado no saco preto
    Embrulhe o peixe no jornal e entregue aos correligionários

Botar o voto na urna como um ritual de morte do indivíduo, de sujeição, e a
urna é o caixão da sua própria autonomia, e talvez até da consciência.

Escolha o futuro próximo: governo demento-fascista-ultraliberal ou neoliberal
disfarçado de social-democrata disfarçado de trabalhista em risco de virar
golpe militar.

Das opções dadas, o antifascismo fala mais alto do que NONONON pra sair do
dilema da urna.

As eleições são importantes, mas não definem totalmente o cenário futuro. A
batalha decisiva ocorre nos corações e mentes...

### O loop estranho da subjetivação

Ok, chega de eleições e voltemos aos cadáveres mais frios do sadismo literário.

O livro segue a linha da lógica cartesiana para chegar à linha dos loops estranhos, passando,
usando, criticando e ultrapassando Lacan. Daí uma linha evolutiva do cartesianismo, para o lacanismo
e em seguida para o hofstadterismo!

Num primeiro momento, a leitura "estourou" minha cabeça -- para usar uma expressão do próprio autor
quando explica uma transição de uma perversa-puritana do "modo neurótico" para o "modo perverso" de
operação -- me deixou [acéfalo](https://en.wikipedia.org/wiki/Ac%C3%A9phale) -- e a referência à
_Acéphale_ no livro talvez tenha passado desapercebida pelo autor e indique o próprio limite da
sua obra, afora a constante referência a dualismos como physis/nomos e natureza/cultura (leis da natureza
versus leis dos humanos) que poderiam ser dialogicamente articulados [morinianamente](/books/epistemology/metodo),
pois ele esbarrou sem querer com a própria complexidade!

Hofstadter utiliza os loops estranhos para mostrar como símbolos, "estruturas" ou "formas" ditas "irracionais"
podem se enrolar, se emaranhar em configurações que apresentam padrões mais "inteligentes" -- ou seja,
o racional surgindo a partir do irracional --, Dufour permanece apenas no nível lógico e aí me parece
o limite de sua análise do liberalismo -- e por quê também não dizer do fascismo? -- como uma articulação
puritano-perversa, pornográfica por trazer em cena o que era até então -- na Cidade dos Homens aspirando
a ser Cidade de Deus -- obsceno.

Utiliza o seguinte loop estranho de "enunciação e estrutura de subjetivação" a partir da sentença
"eu falo a ti a propósito dele":

       .--- Ele --.    (grande Sujeito)
      /            \
     /              \
    |   .-> eu -.   /
    `-->|       |->´   (sujeito s barrado)
        \_ tu <-'

Tal estrutura seria dada por conta da neotenia humana: uma resposta ao nosso desamparo neonatal e fundamental,
do nosso nascimento prematuro:

    220

    Não se é nada disso por natureza porque a natureza é aquilo que a nós, homens,
    mais falta. Com efeito, nascemos prematuros. Para os que não acreditam, eis
    aqui algumas provas da prematuração do homem ao nascer: paredes cardíacas não
    fechadas, imaturidade pós-natal do sistema nervoso piramidal, insuficiência dos
    alvéolos pulmonares, caixa craniana não fechada (o que explica a moleira),
    circunvoluções cerebrais mal desenvolvidas, ausência de polegar posterior
    opositor, ausência de sistema piloso, ausência de dentição de leite ao nascer —
    para não falar, nos homens, da ausência extremamente lamentável de osso peniano
    ao nascer, o que poderá… tornar-se mais tarde uma experiência dolorosa. O ser
    humano é, portanto, um ser de nascimento prematuro, sujeito a uma longuíssima
    maternagem, incapaz de atingir seu desenvolvimento germinal completo e, no
    entanto, capaz de se reproduzir e de transmitir seus caracteres de
    juvenilidade, normalmente transitórios nos outros animais. Resulta disso que
    esse estranho animal, inacabado, ao contrário dos outros animais, deve
    completar-se em outro lugar que não a primeira natureza, ou seja, numa segunda
    natureza, geralmente chamada cultura.

    Não foi apenas a nossa época que se deu conta dessa fraqueza do homem. Tentei
    demonstrar num trabalho anterior285 que existe uma grande narrativa,
    percorrendo toda a civilização ocidental, baseada nesse inacabamento originário
    do homem. Mais ainda, sabe-se hoje que toda a metafísica ocidental, seja ela
    proveniente de Atenas ou de Jerusalém, fez do desamparo do homem ao nascer e na
    primeira infância o ponto de partida da aventura humana. Acontece que essa
    narrativa das origens baseia-se numa razão no real: desde o início do século
    XX, os antropólogos falam, para se referir a essa prematuração, da neotenia do
    homem.286

    221

    É, portanto, aí que tudo começa, com um ser incapaz e incompleto, incapaz de se
    virar — vou chamá-lo aqui de o baixíssimo. Freud, por sua vez, propõe aqui o
    termo da Hilflosigkeit humana, que remete ao desamparo originário do homem. É
    digno de nota que esse conceito cintile ao longo de toda a extensa elaboração
    freudiana. É bem verdade que não se trata de um conceito-estrela como o Édipo,
    como o ego/id/superego ou como o recalque, mas é um conceito de fundo, sem o
    qual os outros não teriam surgido. Ele teria sucessivas definições, cada vez
    mais precisas, mas nunca seria abandonado. Indica que existe apenas uma solução
    para a sobrevivência do homem: que ele supra essa falta de primeira natureza
    com uma segunda natureza, a cultura. A cultura é, de certa maneira, o remédio
    para a Hilflosigkeit humana. O único remédio possível. Aquele que permite ao
    homem sair de seu estado marcado não só por esse inacabamento originário, mas
    também por sua finitude no tempo (eu não sou para sempre; um dia, isto vai
    acabar) e por seu fechamento no espaço (eu não estou em toda parte, mas aqui,
    numa espécie de prisão domiciliar).

    Haverá remediação se eu, ser tão mal acabado no tempo e no espaço, conseguir
    supor um ser infinito em relação ao qual eu me coloque em posição de tudo
    dever. Ora, supor esse ser é algo que eu posso, já que falo, e falar é fabular.
    Nada, portanto, impede-me de inventar o que não existe, mas de que eu preciso
    para viver. Pois se o suponho, a Ele, o Altíssimo, ou seja, o grande Sujeito,
    poderei então me “sub-por” como seu sujeito — e é exatamente o que significa a
    palavra “sujeito”: o subjectum em latim significa o “submisso”, ou seja, aquele
    que é “posto sob”, colocado debaixo. É, portanto, necessário e suficiente que
    eu conjeture um grande sujeito que supostamente tudo sabe, tudo pode e tudo vê
    para que finalmente encontre o meu lugar, como sujeito desse ser. O grande
    Sujeito atende então a essa definição de Aristóteles: “Ele é aquele pelo qual
    tudo mais se ordena”, dizia ele na Metafísica.

    Em outras palavras, a sobrevivência do homem, animal neotênico, por isso
    carente de natureza, passa pela criação de seres de sobrenatureza, vale dizer,
    seres de cultura que, apesar de não existirem, revelam-se dotados de uma
    poderosa eficácia simbólica.

    [...]

    Muito bem. O único problema é que um círculo remetendo indefinidamente de s a S
    e de S a s corre o risco de muito rapidamente tornar-se cansativo, pois
    vicioso. Impossível, com efeito, sair dele, a menos que… A menos que a praxis
    (que, vale lembrar, significa “ação” entre os gregos), resultando como tal de
    uma decisão, venha a provocar o engatamento temporal, forçando as coisas. Não é
    possível, com efeito, ficar dando voltas por toda uma eternidade como um
    hamster na estrutura circular da subjetivação. Será necessário começar por uma
    das pontas. O que implica romper a circularidade e reintroduzir uma
    causalidade, que pode então ter início de duas maneiras diferentes:

    — partindo de S, o grande Sujeito — será essa a escolha do crente. É uma
    escolha frequente, pois tranquiliza o ser desamparado que é o homem, assim
    reconfortado por se imaginar procedendo de algum deus;

    — ou partindo de s — e será essa a escolha do ateu. É uma escolha mais rara,
    pois recusa o consolo tão buscado e prolonga a inquietação. Por isso é que o
    ateu com frequência range — ao mesmo tempo tentando dar uma forma aceitável a
    esse rangido: o humor, por exemplo. Cioran — e eis aí alguém que rangia muito
    —, que sabia conferir ao seu desespero essa forma polida que vem a ser o humor,
    dizia: “Se existe alguém que tudo deve a Bach, é Deus.”288 Uma forma elegante
    de afirmar que Deus, em Sua própria perfeição, foi criado pelos homens…
    essencialmente para acalmá-los em suas angústias.  Poderíamos aqui
    perguntar-nos se, em última análise, existem verdadeiros ateus. O que, de fato,
    não é certo. Muito simplesmente porque a estrutura funcionará tanto melhor na
    medida em que o sujeito ignorar que foi ele que inventou o grande Sujeito (ou
    seu substituto). Em qualquer dos casos, ele deverá dar mostra de ignorância, e
    é precisamente esse não saber que necessariamente fará dele um ser sujeito ao
    inconsciente.289

    224

    É evidente, contudo, que essas duas maneiras de começar são igualmente ruins,
    na medida em que pretendem impor uma decisão no lugar do que é rigorosamente
    impossível de decidir. Em suma, o homem é um ser beckettiano: finito, mal
    acabado e, sobretudo, sempre necessariamente começando mal. Em tais condições,
    cabe supor que o remédio para o desamparo humano venha a ser bem pior que o
    mal.

    Esse remédio simbólico para o desamparo real do homem tem a ver com o que
    Platão chamava de pharmakon: um remédio e um veneno.290 Em suma, o Outro, esse
    grande Sujeito que não existe, é de grande ajuda… até que se torne extremamente
    embaraçoso.

    Por isso é que estamos constantemente matando nosso salvador. Entretanto, como
    o sujeito é esperto, um belo dia tomou a frente, dizendo que havia morrido por
    nós — e isso se chama cristianismo. Com isso, ficou difícil matá-lo… pois ele
    já está morto — e, no entanto, Nietzsche bem que tentou, e sabemos o que lhe
    custou.

    225

    Outro traço característico dessa estrutura estranha: ela permite afirmar que,
    falando estritamente, não existe sujeito. Na verdade, existe apenas um
    infrassujeito (que falta a ele próprio) e um sobressujeito. Os dois, o
    baixíssimo e o Altíssimo, mantêm uma relação de implicação recíproca. Se
    realmente se quisesse que houvesse um sujeito, seria necessário imaginá-lo como
    o que resulta da interação dessas duas instâncias.

    226

    Essa estrutura s/S permite, ao que me parece, dar uma nova forma, indo além da
    clínica individual, ao que Freud havia denominado, num texto tão breve quanto
    decisivo, um de seus últimos, divisão subjetiva (a Spaltung).291 A divisão
    subjetiva é o que faz de nós seres cindidos, incapazes de jamais nos
    encontrarmos, pois no exato momento em que poderíamos nos encontrar,
    perdemo-nos no Outro.  O psicanalista Alain Didier-Weill encontrou as palavras
    mais simples e precisas para dizer essa cisão originária: “Assim que o sujeito
    fala, significando-se numa fala que decide e distingue, uma parte dele,
    insignificável pela fala, retira-se daquilo que foi significado e cai como que
    velada.”292

    [...]

    229

Três respostas básicas seriam possíveis: do neurótico, do perverso e do psicótico:

    Dessa estrutura circular em que o um (s) supõe o Outro (S) que “sub-põe” o um,
    é possível sair de três maneiras: pela neurose, pela perversão ou pela psicose.
    O que retoma em novas condições a intuição de Freud, que havia distinguido três
    patologias fundamentais.

1. Neurose: "dívida simbólica contraída em relação ao Outro", lembando que "sujeito"
   vem de "sujeição", de se sujeitar:

      Se a histeria constitui o protótipo da neurose, é porque o(a) histérico(a) é
      aquele(a) que venera o Outro por lhe ter tudo dado e ao mesmo tempo o detesta
      por tê-lo(a) posto na situação de tanto e tudo lhe dever. Ele/ela amará o Outro
      detestando-o ou o detestará amando-o. É o lugar de um nó psíquico importante,
      no qual constantemente se remotiva o conflito neurótico em todas as suas formas
      possíveis. Por exemplo, esta, que faz as delícias do histérico: seduzir o Outro
      — sob a figura de Deus, de um mestre, de um grande homem, etc. — ao mesmo tempo
      escapando-lhe.

2. Psicose: o caso-limite, "mais onerosa. Ela diz que se Deus é, então eu não sou":

      Um combate que pode assumir duas formas opostas e complementares. Uma forma
      paranoica, como tal perseguida: existe um Deus que está constantemente querendo
      roubar meu ser, que me espiona e me persegue. E uma forma esquizofrênica e
      triunfante: Deus, na verdade, sou eu. Nos dois casos, essa potência
      manifesta-se como sobrenatural, o mais das vezes através de uma voz imperiosa
      que ocupa o sujeito, no sentido de tomar posse dele, de se apoderar dele.

3. Perversão:

      Quanto à enunciação perversa, ela se esclarece nesse esquema. Ela permite
      entender que o que está em jogo no grande circuito enunciativo (com o “Ele”)
      vem a atuar no pequeno, de tal maneira que o “eu” ocupe, diante do “tu”, a
      posição eminente que o “Ele” ocupa em relação a todo sujeito falante (“eu” e
      “tu”). Em suma, o perverso coloca-se, diante de todo outro, na posição do
      Outro. A definição poderá ser estranhada. Mas seria um equívoco, pois ela
      encontra e confere sentido à maneira como Lacan definia o perverso: “O perverso
      imagina ser o Outro para garantir seu gozo.”302 De fato, essa proposição só
      pode ser realmente entendida mobilizando-se as teorias da enunciação baseadas
      na análise da relação de lugar entre as três pessoas verbais: “eu” (o um), “tu”
      (o outro) e “Ele” (o Outro). A perversão surge então como uma negação da grande
      estrutura, compensada por um inchaço da pequena, como se essa estrutura
      secundária pudesse e devesse suportar sozinha o que está em jogo na grande.
      Poderíamos falar aqui de uma translação do que está em jogo na estrutura
      principal para a estrutura secundária. O que, provavelmente, explica a
      seriedade com que o perverso maquina suas encenações, às vezes deploráveis,
      como se ele ocupasse diante de seu alter ego o lugar do Outro.

Os modos de operação individuais variariam de acordo com a ênfase dos caminhos
do circuito de enunciação subjetiva.

O atual turbo-neoliberalismo é sustentado por um par operativo oscilatório
neurótico-perverso.

O caso limite da psicose não é tão útil pois raramente articula com sucesso a
produção e o consumo capitalistas.

Resumiria o livro com o trocadilho: "Sade, Smith e Lacan: um laço realmente estranho, mas não eterno".

E poderíamos pensar em outros tipos de diagramas e máquinas possíveis para a constituição
da relação sujeito/objeto/outro, com Sujeito-Deus, Sujeito-Leviatã, e até de Sujeito como composto
por redes de `eu <-> tu`, incluindo também outros seres. Teríamos assim a possibilidade de
inúmeras montagens e configurações de redes relacionais, hierárquicas, anárquicas, poliárquicas...
uma modelagem desse tipo poderia ajudar na análise de dinâmicas sociais.

### O dilema do prisioneiro

Aqui novamente esbarramos com um limite: altruísmo está situado no lugar da dívida com
o outro (neurose, culpa) e não no abandono-de-si. Será mesmo que a única orientação
("drive") dos seres é a autopreservação? [Marcuse parece mais apropriado neste ponto
ao dar mais ênfase ao loop estranho das pulsões básicas Eros/Thanatos](/books/psychology/eros-civilization),
que podem tanto ser entendidas como criação/destruição quanto tendências oscilantes de autopreservação
ou reintegraçao/dissolução no ambiente.

Se a opção for pela cisão physis/nomos, é preciso ter muito cuidado ao tomar
por naturais as "leis" inventadas pelos humanos, especialmente no campo dos
jogos. Pode-se entender a teoria dos jogos enquanto melhor estratégia possível
sem questionar a valoração que está por trás dela, e portanto sua
arbitráriedade.  Mas entendo que um liberalismo oriundo de uma cultura
ocidental da cisão natureza/cultura, esta é a teoria que vem da cultura e quer
se fazer natural, e este querer-fazer que a naturaliza no sentido de que a
torna normal, a difunde e a impõe a tal ponto que parece imediata, se é que me entendem
dada a dificuldade de formular a ideia.

Seria então dupla mesquinharia acreditar tanto na naturalidade (no sentido de
não ser arbitrário, não haver outra possibilidade) da teoria quando na sua
aplicação (esta não é uma crítica ao autor, mas ao liberalismo)?

Eis o trecho:

    212

    A intervenção de Lacan é muito importante, pois tira a filosofia moral da
    esfera da psicologia — extremamente duvidosa, do ponto de vista científico, já
    que pressupõe indivíduos a priori bons (como Rousseau) ou maus (como Hobbes) na
    sua essência — para transformá-la num autêntico problema lógico.  E, por sinal,
    se Lacan tivesse ido um pouco mais longe nesse terreno, teria podido valer-se
    de suas ruminações sobre a lógica, aquelas mesmas que despertavam o seu
    interesse nessa época, para colocá-la a serviço de sua reflexão sobre os
    eternos impasses da ética e as possíveis superações que a reflexão
    psicanalítica acaso permitiria. A coisa vai do “dilema dos prisioneiros”, que
    ele havia comentado, já em 1945,270 a seu interesse pela cibernética, a partir
    da década de 1950.271

    Um caminho extremamente inovador é aberto aqui, já que enriquece a discussão
    sobre a ética e a escolha das máximas (egoísta ou altruísta) com as
    contribuições da teoria dos jogos.272

    Kant abriu o caminho nesse terreno, ao considerar que a escolha das máximas
    depende de um “você deve” que só pode ser incondicional, porque é lógico. Lacan
    propôs a primeira articulação possível entre as duas máximas, desenvolvendo
    seus aspectos lógicos. Vieram em seguida as discussões sobre a escolha das
    máximas a partir de uma reflexão sobre o famoso dilema dos prisioneiros, tal
    como expresso não na versão complexa de Lacan, mas numa versão simplificada,
    que costuma ser enunciada da seguinte maneira:

    Suponhamos dois prisioneiros, A e B, cúmplices de um crime, detidos em celas
    separadas, sem possível comunicação. O juiz propõe a cada um deles a seguinte
    barganha: denunciar o outro em troca da suspensão da pena. Haveria, assim, três
    possibilidades:

    1º Ambos se denunciam. Neste caso, cada um deles será condenado a cinco anos de
    prisão.

    2º Nenhum dos dois denuncia o outro. Neste caso, cada um será condenado a dois
    anos.

    3º Apenas um dos dois denuncia o outro. Neste caso, aquele que denuncia será
    libertado e outro será condenado a dez anos.273

    Cabe lembrar que esse problema foi enunciado pela primeira vez dessa forma, na
    década de 1950, por pesquisadores da RAND Corporation.274 Este problema logo
    provocou inúmeras discussões científicas, tendo sido estudado de forma
    sistemática na década de 1980 por Robert Axelrod, especialista americano em
    ciências políticas, que introduziu uma variante suplementar, o tempo: o jogo é
    repetido, de tal maneira que os participantes guardam na memória os encontros
    anteriores.275

    É esse problema, precisamente, que vamos encontrar no cerne dos estudos que
    permitem avaliar a pertinência da escolha da máxima egoísta nas e pelas
    sociedades liberais. Ou seja, esta máxima derivada da reviravolta da metafísica
    ocidental, que aos poucos se impôs, como tentamos demonstrar, de Pascal a Sade.
    Se fosse necessária uma confirmação da pertinência da orientação de nossa
    investigação, poderíamos encontrá-la no fato de que precisamente essa máxima
    está em discussão há trinta anos num dos mais importantes think tanks
    americanos.276

    Farei aqui como o professor Mascomo, indo diretamente aos resultados. A solução
    ideal (assim considerada quando beneficia o maior número possível de
    indivíduos), alcançada depois de uma série de cálculos teóricos, experiências
    práticas e simulações em computador, é obtida quando o jogador adota
    inicialmente a estratégia altruísta (chamada tit for tat, ou seja,
    “toma-lá-dá-cá”), o que significa propô-la ao outro, para ver, sabendo que, em
    seguida, deverá estar preparado para um recuo imediato a uma máxima egoísta,
    que, portanto, deve estar pronta, ainda que ele não a use, necessariamente, em
    função do que o outro fará.

    Aqui poderíamos nos perguntar se uma dedução transcendental extremamente
    complexa seria necessária para chegar a essa posição e nela se manter na ação
    prática. Creio que não. É possível chegar a essa posição instantaneamente. Em
    outras palavras, essa dedução transcendental pode ser feita inconscientemente:
    ela surge então como a posição espontânea que permite a regulação ideal da
    relação com o outro, advertindo o sujeito, antes mesmo que ele pense a
    respeito, de que não deve infligir nem se sujeitar.277

    Assim é que a dedução transcendental, consciente ou não, revela que a máxima
    altruísta deve ser completada por uma máxima egoísta — o que poderia ser dito
    de outra forma: a minha máxima kantiana deve, portanto, ser completada por uma
    máxima sadeana, suscetível de ser usada não como estratégia primeira, mas como
    recurso.

    Lacan, portanto, tem razão. O único problema é que ele nem desconfia em que
    medida pode ter razão. Não vê em que medida sua solução permite entender os
    problemas contemporâneos nas sociedades liberais, cada vez mais presas da
    máxima sadeana.

### Eros versus Perversão

No que tange a Marcuse, concordo com o autor de que "Eros e Civilização" não
assume que pode haver uma solução capitalista para o problema da mais-repressão
e que os desejo pode ser infinitamente explorado via consumo.

Mas no meu entender isso não invalida a possibilidade de um arranjo social nos
moldes defendidos pro Marcuse.

São duas formas possíveis de canalizar o desejo: uma aprisionadora, outra que
liberta.

## Trechos

Compilação parcial da seleção de trechos feita do livro todo... a ser completada um dia...

### Zanga

    58A zanga é provavelmente o primeiro jogo de cartas feito para levar a melhor
    (tipo de jogo no qual os jogadores mostram alternadamente uma carta na mesa, e
    aquele que jogou a carta mais forte, segundo as regras do jogo, se apodera de
    tudo, abrindo e fechando as cartas). Foi muito jogado na França no século XVII
    e no início do século XVIII, e continua em uso com regras muito semelhantes com
    o nome de tresillo na Espanha, hombre [como na França] na Dinamarca e tridge na
    Inglaterra.

### Misc

    103
    A leitura de Mandeville permite entender o que muitos estudos econômicos
    não conseguem explicar. Não teria sido possível o desenvolvimento do
    capitalismo sem a liberação das paixões. Aí é que se encontra, em minha
    opinião, a resposta a essa pergunta, até hoje sem resposta, concludente e
    constantemente reiterada desde Marx. Por que exatamente o capitalismo, tendo
    amadurecido desde a Idade Média, finalmente nasceu na Europa por volta de
    1700, nas Províncias Unidas impregnadas de calvinismo, e depois na
    Inglaterra? Por que, se em tantos lugares existiam poderosos mercados
    tradicionais, nenhum se transformou em mercado liberal capitalista? Por que
    essa transformação ocorreu na Europa por volta de 1700, e não nos séculos de
    ouro do Império Romano, sob a dinastia dos Antoninos, tanto mais que a
    primeira máquina a vapor, a eolípila, acabava de ser inventada por Héron de
    Alexandria? Ou ainda na China, por exemplo, no apogeu da dinastia Qing, nos
    séculos XVII e XVIII? Ou ainda no apogeu do Império Otomano, no século XVI,
    por exemplo, sob Solimão, o Magnífico? Ou ainda na Índia, na época da
    dinastia Maurya, no século IV antes de Jesus Cristo, durante a qual foi
    escrito o primeiro tratado de economia política, intitulado Arthaçastra —
    Instrução sobre a prosperidade material? E ainda poderíamos mencionar muitos
    outros lugares. A única resposta possível parece-me a seguinte: as condições
    materiais identificadas por Marx provavelmente estavam reunidas nesses
    diferentes lugares (acumulação primitiva, tendo por um lado uma mão de obra
    desenraizada e, por outro, fluxos de dinheiro), mas a condição moral, ou
    antes, amoral, não estava. Quero dizer que nesses lugares as paixões eram
    contidas em sistemas simbólicos poderosos, ao passo que aqui foram liberadas.
    Essa liberação das paixões ao longo dos séculos XVII e XVIII é que permitiu a
    entrada no capitalismo.

    104

    Nessa condição amoral reside certamente o segredo da irresistível penetração
    do capitalismo em muitos sistemas tradicionais em todo o mundo: o capitalismo
    pareceu libertador a muitos dos povos ainda presos a severas cláusulas
    morais. E, de fato, ele o era — ao mesmo tempo trazendo consigo formas
    absolutamente inéditas de alienação.

    [...]

    Mas, sobretudo, a colmeia é uma ilustração perfeita do gênio do Criador da
    natureza, que consegue construir uma organização extremamente complexa,
    implicando a divisão do trabalho entre os homens, a partir de uma única causa
    muito simples: o amor próprio (chamado de self-liking por Mandeville).
    Utilizando da melhor maneira possível este simples e mesmo estúpido amor
    próprio, gerando todas as libidos possíveis, podemos chegar, sem precisar
    intervir com leis jurídicas ou regras morais, a uma metáfora “admirável”, tão
    perfeita quanto a da colmeia. Existe aí uma espécie de astúcia do Criador,
    que utiliza os defeitos dos homens para criar, apesar deles próprios, uma
    ordem perfeita que os transcende. É pura e simplesmente o projeto
    cibernético, tal como viria a ser desenvolvido por Norbert Wiener, que já
    está contido na ideia de colmeia, já que ela é organizada de acordo com um
    programa perfeito de grande complexidade, que resulta de subprogramas muito
    simples (comportando apenas algumas instruções) seguidos por cada um dos
    habitantes.101

    [...]

    O que me parece analisar mais radicalmente a colmeia mandevilliana, no que
    ela tem de extremamente inquietante para a liberdade humana, com esses homens
    incapazes de sair de uma total alienação aos seus vícios, é o castelo
    sadeano, que também se organiza a partir de uma exploração sistemática de
    todas as paixões imagináveis e mesmo inimagináveis.

    [...]

    145

    Se Marx tivesse lido Sade, não teria cometido um grave erro: não ter
    visto que toda a economia também é uma enorme questão passional e pulsional.
    Se Marx tivesse lido Sade, o mundo seria outro. Teríamos evitado a criação
    desses monstros frios que foram as economias socialistas suspeitando de toda
    paixão, exceto a paixão pelo chefe. Não teríamos tido essa divisão altamente
    nociva entre Marx, por um lado, na economia dos bens, e Freud por outro, na
    economia libidinal — cisão equivocada desde o início, que nenhum
    freudo-marxista, nem mesmo da escola de Frankfurt, jamais foi capaz de
    resolver. Se Marx tivesse lido Sade, poderíamos dispor de uma economia geral
    das paixões. O mundo poderia ter sido reformado de outra maneira. Teríamos
    evitado a captação e o desvio dos espíritos resistentes à teodiceia smithiana
    nas falsas alternativas ao capitalismo representadas pelas economias
    socialistas, que só poderiam levar ao mais lamentável dos fiascos.

    Ao passo que, para Kant, era absolutamente necessário regular — a moral deve
    ser baseada no imperativo categórico consistindo em se impor a si mesmo uma
    lei na vida prática —, para Smith cabia, sobretudo, deixar fazer [laisser
    faire], vale dizer, desregular — o que conduz logicamente a Sade.

    [...]

    Postulada essa distinção entre os dois Iluminismos, fica mais fácil indicar o
    que distingue a modernidade da pós-modernidade. A modernidade é o equilíbrio
    instável entre essas duas correntes opostas. Terá durado um século e meio. A
    pós-modernidade é o recuo cada vez mais acentuado da zona transcendental que
    remete ao que “não tem preço, mas uma dignidade” (Kant), em proveito do
    princípio liberal, segundo o qual tudo tem um preço (Smith).

    Com efeito, podemos conceber Sade como aquele que, no fim do século, se
    apropriou dessas teses liberais, por sinal de maneira extremamente sadeana,
    levando-as a suas últimas consequências e mostrando, de uma forma que tendo a
    considerar irretocável, aonde conduz, do ponto de vista do ser-si-mesmo e do
    ser-junto, a sua escolha, constantemente reiterada ao longo de seus textos,
    da moral egoísta contra a moral altruísta.

    Fazer de Sade um homem-chave do seu século, o século XVIII, é, portanto,
    afastar-se das interpretações tão frequentes quanto anacrônicas que pretendem
    considerá-lo arauto perfeito dos sistemas fascistas. Basta pensar, por
    exemplo, na posição assumida por um autor tão estimável quanto Pasolini no
    filme intitulado Salò ou Os cento e vinte dias de Sodoma, lançado no fim de
    1975. É provável que Pasolini, irritado com as visões, cada vez mais
    frequentes depois de 1968, de um Sade simpático, bon vivant, alvo de
    perseguições dos obscurantistas de sua época, tenha pretendido reagir a esse
    absoluto contrassenso. Com isso, situou a ação de Os cento e vinte dias… em
    Salò, a cidade do norte da Itália onde Mussolini se refugiara no fim da
    Segunda Guerra Mundial para fundar uma república fascista. Como sabemos, foi
    o último filme de Pasolini: ele seria assassinado após o lançamento.

    A morte trágica de Pasolini nos faz pensar. Não podemos, com efeito, passar por
    cima de uma questão pungente: será que as circunstâncias de sua morte… não
    desmentiriam sua tese? Pois ele foi morto, de maneira extremamente sadeana, não
    por fascistas, mas por jovens extremamente liberados, tão liberados que não
    tinham controle de suas paixões e pulsões, já que, pelo que sabemos, foi um
    jovem prostituto romano de dezessete anos, um dos que eram frequentados por
    Pasolini, que o matou a cacetadas no dia 1º de novembro de 1975, para em
    seguida esmagá-lo várias vezes com seu próprio carro na praia de Ostia, perto
    de Roma.

    [...]

    Situar Sade dessa maneira permite adiantar que o liberalismo tem duas faces:
    uma face puritana, representada pelo “primeiro filho” de Mandeville, vale
    dizer, Adam Smith, e uma face perversa, indissociavelmente ligada, representada
    pelo “segundo filho” de Mandeville, Sade. Em outras palavras, devemos entender
    o liberalismo como um sistema bifronte, à Janus, vale dizer, como um conjunto
    perverso-puritano.

    O que poderia ser dito assim: o liberalismo é Smith com Sade.

    153

    A tese que defendo, portanto, é a seguinte: Sade diz a verdade do liberalismo e
    por esse motivo é que foi necessário aprisioná-lo durante toda a vida e
    atirá-lo no inferno após a morte, enquanto o conto da harmonização dos
    interesses privados pela mão invisível, prometido pela teodiceia puritana de
    Adam Smith, se espalhava pelo mundo. A esse inferno das bibliotecas, exposto
    apenas à crítica devoradora dos ratos e camundongos, é que Sade foi recolhido e
    escondido por alguns eruditos durante dois séculos.

### Marx e Sade

    140

    Essa entrada do capital, no negócio sadeano, é marcada pela referência
    constante, no texto, a um outro lugar além do convento e do castelo. É a
    fábrica, então nascendo, que verdadeiramente pode transfigurar o convento e o
    castelo, permitindo-lhes alcançar a dimensão industrial. E, com efeito, o texto
    sadeano se empenha em mostrar a possibilidade da industrialização do gozo.

    Ora, para industrializar o gozo, é necessário:

    1) Um aporte de capital. Acabamos de falar a respeito, a propósito do castelo
    de Silling e do banqueiro Durcet. Silling, assim como outra mansão, o castelo
    da Sociedade dos Amigos do Crime, da história de Juliette, são empreendimentos
    baseados na iniciativa de membros muito afortunados da alta nobreza ou da
    grande burguesia. Trata-se, portanto, de um investimento que envolve modos de
    gestão muito precisos e, por sinal, indicados já na introdução de Os cento e
    vinte dias…:

    A sociedade havia criado um fundo comum alternadamente administrado por cada um
    de seus membros durante seis meses; mas os recursos desse fundo, devendo servir
    apenas aos prazeres, eram imensos. Sua enorme fortuna permitia-lhes coisas
    muito singulares a esse respeito, e o leitor não deve espantar-se quando lhe é
    dito que anualmente eram destinados dois milhões exclusivamente aos prazeres da
    boa mesa e da lubricidade.

    2) Uma provisão de matérias-primas, vale dizer, corpos prontos para serem
    reduzidos a órgãos para o gozo, em outras palavras, prontos para serem
    des-organizados, desmembrados em órgãos, em condições de serem integrados
    pedaço a pedaço à indústria do gozo.

    3) Um pessoal de organização e intendência, além de capatazes, capazes de
    explorar da melhor forma essa matéria-prima e fazer funcionar uma tal máquina.
    Entre outras coisas, esse pessoal serve para cuidar das cadências, como aqui,
    em Juliette: “Sob nossas bocas, as bocetas, os paus, os cus se sucediam tão
    rapidamente quanto o desejo; por outro lado, mal os aparelhos que masturbávamos
    haviam descarregado, outros surgiam” (Juliette, 5ª parte). Ou aqui, em Os cento
    e vinte dias…: “É preciso que a coisa ande muito depressa; cada moça deve dar
    vinte e cinco chicotadas, e no intervalo desses vinte e cinco golpes é que a
    primeira chupa e a terceira caga.”

    O modelo antecipado por Sade é claramente o da cadeia de montagem industrial.

    Sessenta anos antes de Marx, Sade entendeu que a produtividade está diretamente
    ligada ao fator tempo (O Capital, 1ª seção, 1º capítulo).

    141

    O que vemos começar a funcionar é uma manufatura de um tipo especial, na qual
    os corpos são integrados em uma grande máquina de produção de gozo. A ironia de
    Sade em relação aos templos smithianos (vale dizer, as empresas da primeira
    revolução industrial), em que se realiza o maravilhoso plano secreto da
    natureza, é feroz. Reconhecemos aí a atitude assassina do descendente de uma
    velha família nobre que considera com desprezo as realizações de que tanto se
    orgulha a burguesia conquistadora.

    142

    É notável que Sade, grande anunciador da Cidade perversa, tenha pensado que o
    gozo podia industrializar-se, graças, entre outras coisas, à inserção de
    algumas máquinas-ferramentas, antepassados dos atuais sextoys. Por exemplo,
    máquinas masturbadoras ou chupadoras feitas de roldanas, molas e engrenagens,
    que são engatadas no senhor e funcionam como substitutos de órgãos, de tal
    maneira que, quando os agentes humanos estão presentes, é necessário que seja
    visível apenas a parte do corpo que fornece o gesto útil ao gozo (a mão, o
    pênis, a boca, o ânus):

    Então Francaville retirou um tecido de cetim rosa que recobria o otomano… Oh!
    Que assento se encontrava sob o tecido! […] [uma mulher podia ajoelhar-se],
    suas mãos […] iam pousar no baixo ventre de dois homens que assim colocavam nas
    mãos da mulher uma máquina monstruosa que era a única coisa que se via: o resto
    do corpo, oculto por baixo de panos negros, não era visto. Uma nova mecânica
    muito mais singular era operada sob o ventre da mulher […]. De toda essa
    mecânica resultava que a mulher, sobre o sofá movido pelas molas adaptadas,
    nele era a princípio molemente estendida sobre o ventre, penetrada por um
    consolo, chupada por uma jovem, masturbando um pau com cada uma das mãos,
    oferecendo o cu ao pau bem real que vinha sodomizá-la e alternadamente
    chupando, conforme o gosto, um pau, uma boceta e mesmo um cu.164

    143

    Cabe notar que a sociedade-fábrica de produção/consumo do gozo de Sade é uma
    sociedade sem restos, onde tudo pode ser explorado:

    “Vamos, minha criança”, diz ele, “mãos à obra; a merda está pronta, eu a senti,
    lembre-se de cagar aos poucos e sempre esperar que eu tenha devorado um pedaço
    antes de expelir outro. Minha operação é longa, mas não a apresse. Um tapinha
    nas nádegas servirá de aviso para expelir, mas que seja sempre aos poucos.”
    Tendo-se então colocado o mais confortavelmente possível em relação ao objeto
    de seu culto, ele cola sua boca e eu lhe entrego quase imediatamente um pedaço
    de bosta do tamanho de um pequeno ovo. Ele o chupa, virando-o e revirando-o mil
    vezes na boca, mastiga-o, saboreia-o e, ao fim de dois ou três minutos, vejo
    claramente que o engole.165

    Estamos lidando com um sistema perfeito, sem restos, já que os dejetos são
    reciclados. E, por sinal, é exatamente onde os comentadores mais entusiásticos
    de Sade, como Maurice Heine, não aguentam mais. Mas estão errados, pois é aí
    que a sociedade-fábrica da produção/consumo encontra seu regime ideal, seu
    regime ecológico perfeito, pela acoplagem da máquina-boca, para falar como
    Deleuze, à máquina-cu.

    144

    Marx fez uma análise impecável do processo de produção. O capitalista não paga
    ao proletário o produto de seu trabalho, mas apenas a soma necessária para a
    reprodução de sua força de trabalho, de tal maneira que capta a diferença (a
    mais-valia) que, com o tempo, permite a constituição do capital. Mas Marx não
    se aventurou na análise do processo de consumo. Sade é o único que articulou a
    produção (pelos proletários) e o consumo (pelo senhor). Em outras palavras, a
    mais-valia extraída também é uma reserva de fundos que podem ser
    incessantemente convertidos em gozos de toda natureza — o que Lacan muito
    justificadamente chamaria, em seu seminário de 1968-1969 intitulado De um Outro
    ao outro (livro XVI, Le Seuil, Paris, 2006), de “o mais-gozar”.166 Graças a
    Sade, ficamos então sabendo algo essencial: o consumo é um gozo. Um gozo
    proibido ao proletário produtor.

    Marx e Sade

    145

    Se Marx tivesse lido Sade, não teria cometido um grave erro: não ter visto que
    toda a economia também é uma enorme questão passional e pulsional. Se Marx
    tivesse lido Sade, o mundo seria outro. Teríamos evitado a criação desses
    monstros frios que foram as economias socialistas suspeitando de toda paixão,
    exceto a paixão pelo chefe. Não teríamos tido essa divisão altamente nociva
    entre Marx, por um lado, na economia dos bens, e Freud por outro, na economia
    libidinal — cisão equivocada desde o início, que nenhum freudo-marxista, nem
    mesmo da escola de Frankfurt, jamais foi capaz de resolver. Se Marx tivesse
    lido Sade, poderíamos dispor de uma economia geral das paixões. O mundo poderia
    ter sido reformado de outra maneira. Teríamos evitado a captação e o desvio dos
    espíritos resistentes à teodiceia smithiana nas falsas alternativas ao
    capitalismo representadas pelas economias socialistas, que só poderiam levar ao
    mais lamentável dos fiascos.

    146

    Poderíamos responder que Marx não teve a menor necessidade de ler Sade, pois
    desenvolvera um conceito que permite a análise do processo de consumo, o
    “fetichismo da mercadoria”. Esse conceito é apresentado num curto texto
    (algumas páginas) que constitui a quarta e última parte do primeiro capítulo do
    livro I do Capital, intitulada “O caráter fetiche da mercadoria e seu segredo”.
    E, de fato, ele permite entender por que, no regime capitalista, o homem encara
    a mercadoria como o “selvagem” vê um ídolo: ela possui uma qualidade mágica, a
    de poder ser trocada por qualquer outra mercadoria — e podemos ver aonde isso
    conduz, ao fetichismo do dinheiro. Se a mercadoria é fetichizada, é por ocultar
    aquilo que é na realidade, e que a “ciência” (marxista) enuncia assim: a
    mercadoria é apenas o tempo de trabalho socialmente necessário para produzi-la,
    ou seja, trabalho abstrato, e, portanto, remete apenas a relações sociais. Esse
    trabalho é considerado abstrato porque, em oposição ao trabalho concreto, torna
    abstrata toda qualidade sensível e todo valor de uso — e é precisamente isso
    que leva a mercadoria a funcionar como fetiche.

    [...]

    Há os que pensam (os marxistas ortodoxos) que Marx tem razão de destacar o
    domínio do trabalho abstrato sobre o trabalho concreto e os que consideram
    (toda uma corrente crítica do marxismo ortodoxo) que Marx deplora e denuncia o
    trabalho abstrato, daí extraindo uma consequência radical: é necessário pôr fim
    ao trabalho assalariado.

    [...]

    O texto de Marx é, na verdade, fundamentalmente ambíguo. E todo o mérito dos
    trabalhos críticos está em decidir no lugar de Marx e propor, contra as
    habituais interpretações marxistas, uma leitura radical do “fetichismo da
    mercadoria”. Entretanto, essas críticas, permanecendo escoradas em Marx, e
    mesmo num Marx corrigido, deparam-se com um considerável obstáculo. Elas não
    abordam uma questão decisiva. É verdade que a mercadoria é apenas tempo de
    trabalho socialmente necessário para produzi-la, mas é precisamente a isso que
    não se dá a menor importância quando se desfruta de um objeto. E mais:
    desfrutar de um objeto também é subitamente abolir o trabalho socialmente
    necessário para sua produção. Pois nesse caso está em ação uma outra lei. A lei
    que, no consumidor, joga com a dinâmica que vai da pulsão a sua satisfação.
    Ora, esta segunda lei é ignorada por Marx. Foi precisamente o que permitiu as
    interpretações dos marxistas ortodoxos. Pois sendo o consumo sempre frio e
    puramente utilitário em Marx, era fatal que o marxismo real engendrasse apenas
    monstros frios gerados pela “ciência”, enunciando incansavelmente essa “lei” do
    trabalho socialmente necessário e esquecendo a outra. Creio assim que é
    necessário não só conduzir o texto de Marx na direção de uma crítica radical do
    “fetichismo da mercadoria”, como ler nessas ausências o que permitiu o
    desenvolvimento, com as trágicas consequências que sabemos, do erro e do horror
    dos marxismos reais.

[[!tag philosophy psychology sociology]]
