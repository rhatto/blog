[[!meta title="Estoicismo"]]

## Crítica

* É uma filosofia aceitável para diagnóstico: aceitar os fatos, nossos
  limites e a inutilidade das expectativas.

* No entanto, pode ser conformista: o escravo se acostumar a aceitar
  ser escravo, senhor a ser senhor.

* Também pode ser egocêntrica, uma vez que leva à conclusão que uma pessoa
  só pode contar consigo mesma ou, no limite, com seu próprio pensamento.
  Na verdade somos completamente dependentes e nada pode ser assumido de
  antemão. Mas todos e todas estamos nessa, então alianças são fundamentais!

* Parece, ao mesmo tempo, uma vida medrosa e mesquinha, porque para evitar
  sofrimentos ela prefere se abster de possíveis alegrias. Somos assim tão
  frágeis?

* O estoicismo é útil como parte da bagagem de uma vida simples mas que
  luta por melhor condições dentro de um meio social. Ela ajuda a lidar com
  as situações difícieis.

## The Enchiridion

                                THE ENCHIRIDION


                                       I

    There are things which are within our power, and there are things which
    are beyond our power. Within our power are opinion, aim, desire,
    aversion, and, in one word, whatever affairs are our own. Beyond our
    power are body, property, reputation, office, and, in one word, whatever
    are not properly our own affairs.

    Now the things within our power are by nature free, unrestricted,
    unhindered; but those beyond our power are weak, dependent, restricted,
    alien. Remember, then, that if you attribute freedom to things by nature
    dependent and take what belongs to others for your own, you will be
    hindered, you will lament, you will be disturbed, you will find fault
    both with gods and men. But if you take for your own only that which is
    your own and view what belongs to others just as it really is, then no
    one will ever compel you, no one will restrict you; you will find fault
    with no one, you will accuse no one, you will do nothing against your
    will; no one will hurt you, you will not have an enemy, nor will you
    suffer any harm.

    Aiming, therefore, at such great things, remember that you must not allow
    yourself any inclination, however slight, toward the attainment of the
    others; but that you must entirely quit some of them, and for the present
    postpone the rest. But if you would have these, and possess power and
    wealth likewise, you may miss the latter in seeking the former; and you
    will certainly fail of that by which alone happiness and freedom are
    procured.

    Seek at once, therefore, to be able to say to every unpleasing semblance,
    “You are but a semblance and by no means the real thing.” And then
    examine it by those rules which you have; and first and chiefly by this:
    whether it concerns the things which are within our own power or those
    which are not; and if it concerns anything beyond our power, be prepared
    to say that it is nothing to you.

                                      XII

    If you would improve, lay aside such reasonings as these: “If I neglect
    my affairs, I shall not have a maintenance; if I do not punish my
    servant, he will be good for nothing.” For it were better to die of
    hunger, exempt from grief and fear, than to live in affluence with
    perturbation; and it is better that your servant should be bad than you
    unhappy.

    Begin therefore with little things. Is a little oil spilled or a little
    wine stolen? Say to yourself, “This is the price paid for peace and
    tranquillity; and nothing is to be had for nothing.” And when you call
    your servant, consider that it is possible he may not come at your call;
    or, if he does, that he may not do what you wish. But it is not at all
    desirable for him, and very undesirable for you, that it should be in his
    power to cause you any disturbance.
