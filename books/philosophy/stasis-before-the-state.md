[[!meta title="Stasis Before the State: Nine Theses on Agonistic Democracy"]]

* Athor: Dimitris Vardoulakis
* References:
  * https://www.worldcat.org/title/stasis-before-the-state-nine-theses-on-agonistic-democracy/oclc/1000452218
  * https://muse.jhu.edu/chapter/2009359
  * https://www.jstor.org/stable/j.ctt1xhr6vd
  * https://www.academia.edu/35908382/Vardoulakis_Stasis_Before_the_State_--_Introduction
  * https://www.fordhampress.com/9780823277414/stasis-before-the-state/
* Topics:
  * Ruse of sovereignty.
  * Diference between justification and judgement.

## Excerpts

    This question would be trivial if sovereignty is under­
    stood simply as the sovereignty of specific states. The
    question is pertinent when we consider the vio­lence
    functioning as the structural princi­ple of sovereignty.
    Sovereignty can only persist and the state that it sup­
    ports can only ever reproduce its structures—­political,
    economic, ­legal, and so on—­through recourse to certain
    forms of vio­lence. Such vio­lence is at its most effective
    the less vis­i­ble and hence the less bloody it is. This in­
    sight has been developed brilliantly by thinkers such
    as Gramsci, u
    ­ nder the rubric of hegemony; Althusser,
    through the concept of ideology; and Foucault, as the
    notion of power. It is in this context that we should also
    consider Carl Schmitt’s definition of the po­liti­cal as the
    identification of the e ­ nemy. They all agree on the essen­
    tial or structural vio­lence defining sovereignty—­their
    divergent accounts of that vio­lence notwithstanding.
    The prob­lem of a space outside sovereignty is com­

    [...]

    Posing the question of an outside to sovereignty
    within the context of the mechanism of exclusion turns
    the spotlight to what I call the ruse of sovereignty. This
    essentially consists in the paradox that the assertion of
    a space outside sovereignty is nothing other than the as­
    sertion of an excluded space and consequently signals
    the mobilization of the logic of sovereignty.

    [...]

    To put this in the vocabulary used h
    ­ ere, the at-
    tempt to exclude exclusion is itself exclusory and thus
    reproduces the logic of exclusion.

    [...]

    Turning to Solon’s first demo­cratic constitution,
    I ­will suggest in this book that it is pos­si­ble by identify­
    ing the conflictual nature of democracy—or what the
    ancient Greeks called stasis. Agonistic monism holds
    that stasis is the definitional characteristic of democ­
    racy and of any other pos­si­ble constitutional form. Sta­
    sis or conflict as the basis of all po­liti­cal arrangements
    then becomes another way of saying that democracy is
    the form of e ­ very constitution. Hence, stasis comes be-
    fore any conception of the state that relies on the ruse of
    sovereignty.

    The obvious objection to this position would be about
    the nature of this conflict. Hobbes makes the state of
    nature — which he explic­itly identifies with democracy —­
    also the precondition of the commonwealth. Schmitt
    defines the po­liti­cal as the identification of the enemy.

    [...]
    
    ent power. Is t ­ here a way out of this entangled knot?
    Antonio Negri’s most significant contribution to po­
    liti­cal philosophy is, in my opinion, precisely at this
    juncture. His intervention starts with his book on Spi­
    noza, The Savage Anomaly, in which he distinguishes
    between potentia (constituent power) and potestas (con­
    stituted power). 20 It is most explic­itly treated in Insur-
    gencies, which provides an account of the development
    of constituent power in philosophical texts from early
    modernity onward and examines the function of con­
    stituent power in significant historical events. 21 The
    starting premise of this investigation is the rejection of
    ent power. Is t ­ here a way out of this entangled knot?
    Antonio Negri’s most significant contribution to po­
    liti­cal philosophy is, in my opinion, precisely at this
    juncture. His intervention starts with his book on Spi­
    noza, The Savage Anomaly, in which he distinguishes
    between potentia (constituent power) and potestas (con­
    stituted power). 20 It is most explic­itly treated in Insur-
    gencies, which provides an account of the development
    of constituent power in philosophical texts from early
    modernity onward and examines the function of con­
    stituent power in significant historical events. 21 The
    starting premise of this investigation is the rejection of
    and avoiding the ruse of sovereignty. 23
    The appeal to constituent power gives Negri the means
    to provide an account of democracy as creative activity.
    This has a wide spectrum of aspects and implications
    that I can only gesture t ­ oward ­here. For instance, this
    approach shows how democracy requires a convergence
    of the ontological, the ethical, and the political—­which
    is also a position central to my own proj­ect (see Thesis
    6). Consequently, democracy is not reducible to a con­
    stituted form, and thus Negri can provide a nonrepre­
    sen­ta­tional account of democracy. This is impor­tant
    because it enables Marx’s own distaste for representative
    democracy to resonate with con­temporary sociology
    and po­liti­cal economy—­a proj­ect that starts with Negri’s
    involvement in Italian workerism and culminates in his
    collaborations with Michael Hardt. Besides the details,
    which Negri has been developing for four de­cades, the
    impor­tant point is that this description of democracy
    and constituent power is consistently juxtaposed to the
    po­liti­cal tradition that privileges constituted power and
    sovereignty. 24
    
    There is, however, a significant drawback in Negri’s
    approach. It concerns the lack of a consistent account of
    vio­lence in his work.
    
    [...]
    
    Without a
    consideration of vio­lence, radical democracy ­w ill never
    discover its agonistic aspect, namely, that conflict or
    stasis is the precondition of the po­liti­cal and that, as
    such, all po­liti­cal forms are effects of the demo­cratic. In
    other words, Negri’s obfuscation of the question of vio­
    lence can never lead to agonistic monism.

Production of the real:

    Second, the state of emergency leading to justification
    does not have to be “real”—it simply needs to be credi­
    ble. Truth or falsity are not properties of power—as Fou­
    cault very well recognized—­and the reason for this, I
    would add, is that power’s justifications are rhetorical
    strategies and hence unconcerned with validity. This is
    the point where my account significantly diverges from
    
    [...]
    
    If we are to understand better sovereign vio­lence, we
    need to investigate further the ways in which vio­lence is
    justified. Sovereignty uses justification rhetorically. In­
    stead of being concerned with w
    ­ hether the justifications
    of actions are true or false, sovereignty is concerned
    with ­whether its justifications are believed by ­those it af­
    fects.

Torture:

    Greek po­liti­cal philosophy. 4 Hannah Arendt also pays
    par­tic­u­lar attention to this meta­phor. According to Ar­
    endt, Plato needs the meta­phor of the politician as a
    craftsman in order to compensate for the lack of the no­
    tion of authority in Greek thought. ­These Platonic meta­
    phorics include the meta­phor of the statesman as a
    physician who heals an ailing polis. 5 The meta­phor of
    craftsmanship is used as a justification of po­liti­cal power.
    craftsmanship is used as a justification of po­liti­cal power.
    The meta­phor persists in modernity, and we can find
    examples much closer to home. Mao Zedong justifies
    the purges of the Cultural Revolution on the following
    grounds: “Our object in exposing errors and criticizing
    shortcomings is like that of curing a disease. The entire
    purpose is to save the person.” 6 Whoever does not con­
    form to the Maoist ideal is “ill” and needs to be “cured.”
    Similarly, George Papadopoulos, the col­o­nel who headed
    the Greek junta from 1967 to 1974, repeatedly described
    Greece as an ill patient requiring an operation. The dic­
    tatorial regime justified its vio­lence by drawing an anal­
    ogy of its exceptional powers to the powers of the head
    surgeon in a hospital emergency room. Th
    ­ ese operations
    on “patients” took place not in hospitals but in dark po­
    lice cells or in vari­ous forms of prisons or concentration
    camps. And the instruments of the “operations” ­were
    not t ­ hose of the surgeon but rather of the torturer and
    in many cases also of the executioner. The analogy be­
    tween the surgeon and the torturer is mobilized to pro­
    vide reasons for the exercise of vio­lence. An emergency
    mobilizes rhetorical strategies that justify vio­lence, ir­
    respective of the fact that such a justification may be
    completely fabulatory.

    -- 32-33

Razão instrumental:

    Let us return to consider more carefully how sover­
    eign vio­lence always strives for justification. This means
    that we can characterize the acts of sovereignty as con­
    forming to a rationalized instrumentalism. Sovereign
    vio­lence is instrumental in the sense that it always aims
    toward something—it is not vio­
    ­
    lence for vio­
    lence’s
    sake. This means that the desired outcome of sover­
    eign vio­lence is calculated with the help of reason. The
    extrapolation of vio­lence in instrumental terms is noth­
    ing new. For instance, Hannah Arendt pres­ents instru­
    mentalism as the defining feature of vio­lence. 7 Yet the
    instrumentalism of sovereign vio­lence is not as self-­
    evident as it may at first appear. For instance, as Fran­
    çois Jullien shows, the conception of an instrumental
    thinking as appropriate to the po­liti­cal arises in ancient
    Greece, and it does not characterize the Chinese cul­
    ture, including even the ways in which warfare is con­
    ceived. 8 The impor­tant point, then, is to remember that
    the instrumentality of reason in the ser­v ice of a justifi­
    cation of vio­lence is a characteristic of sovereignty as it
    is developed in the Western po­liti­cal and philosophical
    tradition.
    The “invention” of the instrumentality of reason is
    an impor­tant moment in the history of thought, and
    its “inventors,” the ancient Greeks, amply recognized its
    importance. In fact, their tragedies are concerned pre­
    cisely with the clash between the older forms of thinking
    and new forms exemplified by instrumental reason. The
    best example of this is perhaps the Oresteia. In the first
    play of the trilogy, Agamemnon is murdered by his wife,
    Clytemnestra. In the second play, Orestes, Agamem­
    non’s son, responds by killing his ­mother. In the third
    play, the Eumenides, the court of Athens is called to de­
    cide w
    ­ hether Orestes’s murder was justified. The alter­
    natives are that he is e ­ ither guilty of matricide pure and
    simple or that his act was a po­liti­cal one aiming to ­free
    Argos of a tyrant. Th
    ­ ere is, then, a standstill or stasis—­
    and I draw again attention to this word, to which I w
    ­ ill
    return ­later—­between the two dif­fer­ent l ­ egal frame­
    works: one legality privileging kinship, the other privi­
    leging instrumental rationality whereby the murder of
    Clytemnestra is justified by the end of saving the city
    from a tyrant. The judges’ vote is a tie, at which point
    the goddess Athena, who presides over the proceedings,
    casts the vote to f ­ ree Orestes of the charge of matricide.
    Calculative reason prevails as the mode of the po­liti­cal.
    But at the same time, it should not be forgotten that the
    vote was equally split. For the ancient Athenians, it is
    impossible to reconcile the two dif­fer­ent legalities—­the
    politics of kinship and the politics of instrumental
    reason. Justice persists in this irreconcilability, despite
    its tragic consequences.

    -- 33-35

Soberania como persuasão e interpretação:

    In other words, the absoluteness of
    sovereignty has nothing to do with the power of sover­
    eignty as it is exercised through its institutions—­the
    police, the army, the judiciary, and so on. Rather, the
    absoluteness of sovereignty is an expression of the rhe­
    torical and logical mechanisms whereby sovereignty
    uses the justification of vio­lence to dominate public de­
    bate and to persuade the citizens. The exercise of sover­
    eignty is the effect of an interpretative pro­cess. Differently
    put, this entails that the justification of vio­lence is more
    primary than the legitimate forms assumed by constituded power.
    Without an effective justification, any government loses its
    mandate to govern, even though its
    decisions and po­liti­cal actions, its policies, and its legis­
    lative agenda may perfectly conform to the law of the
    state.

    -- 52-53

Democracia:

    How can democracy as the other of sovereignty be
    mobilized to respond to sovereignty’s justification of
    vio­lence? This final question is, I believe, the most fun­
    damental po­liti­cal question. It essentially asks about
    the relation of sovereignty and democracy. What is re­
    quired at this juncture in order to broach the relation
    between democracy and sovereignty further is a better
    determination of democracy.

    -- 53

> The first ever democracy was instituted through the Solonian reforms that were
> introduced to counteract a chronical political no less than social crisis in
> Athens. The crisis was the result of a protracted animosity between the rich
> and the poor parties. The confrontation was largely because of material
> inequalities, such as the requirement to hold property in order to be a
> citizen, and the economic inequalities that were threatening to turn into
> slaves a large portion of the poor population who had defaulted on their
> payments. Unsurprisingly, given the sensitivity of these issues, tensions
> ran high, and the city often found itself in conflict or stasis, with the two
> sides taking arms against each other. The situation had reached an acute
> crisis, at which point the Athenians re­ solved that they had to take decisive
> action. They turned to Solon, who was largely viewed as impartial and wise, to
> write a new constitution for the city. He responded by compiling the first ever
> democratic constitution.
>
> [...]
>
> The crisis is the condition of citizenship and residency within
> Athens and even of the possibility of the operation of the state. Solon's law
> does not describe mea­sures whereby the crisis can be avoided. Instead, it
> describes how everyone is required to participate in it -- as if the aim is to
> accentuate the crisis. Those who avoid conflict will be punished. The
> democratic overcoming of crisis consists in the institutionalization of
> crisis within the constitution. According to Solon, his fellow Athenians need
> to recognize the illusion that the implementation of measures can always
> prevent crisis. According to Solon, democracy consists in the dispelling of
> that illusion. This does not mean that certain measures or policies cannot and
> should not be devised to ameliorate or evade predictable crises. Rather, it
> highlights that such mea­sures are never adequate. Or, to put it the other
> way around, Solon sees crisis as a way of being, as a condition of existence,
> and he is determined that his democratic constitution aknowledges this.
>
> -- 57-58

> Democracy does not seek to be charitable to the other but instead affords the
> other the respect to give them a voice to express their opinions as well as to
> debate and rebuke these opinions.
>
> -- 73

> These insights amount to saying that a democratic being is conflictual
> -- which is to say that it cannot find certainty in any political regime
> promising unity or in a state characterized by order, peace, and stability.
> Rather, democracy in this sense is a regime that is inherently open to the
> possibility of conflict without any underlying structure to regulate this
> conflict or to resolve it to some­ thing posited as higher.
>
> -- 76
