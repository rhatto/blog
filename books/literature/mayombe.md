[[!meta title="Mayombe"]]

## Trechos

### Prometeu africano

    A comida faltava e a mata criou as «comunas», frutos secos, grandes amêndoas,
    cujo caroço era partido à faca e se comia natural ou assado. As «comunas» eram
    alimentícias, tinham óleo e proteínas, davam energia, por isso se chamavam
    «comunas». E o sítio onde os frutos eram armazenados e assados recebeu o nome
    de «Casa do Partido». O «comunismo» fez engordar os homens, fê-los restabelecer
    dos sete dias de marchas forçadas e de emoções. O Mayombe tinha criado o fruto,
    mas não se dignou mostrá-lo aos homens: en carregou os gorilas de o fazer, que
    deixaram os caroços partidos perto da Base, misturados com as suas pegadas. E
    os guerrilheiros perceberam então que o deus-Mayombe lhes indicava assim que
    ali estava o seu tributo à coragem dos que o desafiavam: Zeus vergado a
    Prometeu, Zeus preocupado com a salvaguarda de Prometeu, arrependido de o ter
    agrilhoado, enviando agora a águia, não para lhe furar o fígado, mas para o
    socorrer. (Terá sido Zeus que agrilhoou Prometeu, ou o contrário?)

    A mata criou cordas nos pés dos homens, criou cobras à frente dos homens, a
    mata gerou montanhas intransponíveis, feras, aguaceiros, rios caudalosos, lama,
    escuridão, Medo. A mata abriu valas camufladas de folhas sob os pés dos homens,
    barulhos imensos no silêncio da noite, derrubou árvores sobre os homens. E os
    homens avançaram. E os homens tornaram-se verdes, e dos seus braços folhas
    brotaram, e flores, e a mata curvou-se em abóbada, e a mata estendeu-lhes a
    sombra protectora, e os frutos. Zeus ajoelhado diante de Prometeu. E Prometeu
    dava impunemente o fogo aos homens, e a inteligência. E os homens compreendiam
    que Zeus, afinal, não era invencível, que Zeus se vergava à coragem, graças a
    Prometeu que lhes dá a inteligência e a força de se afirmarem homens em
    oposição aos deuses. Tal é o atributo do herói, o de levar os homens a
    desafiarem os deuses.
     
    Assim é Ogun, o Prometeu africano.

### Comandante Sem Medo

    – Há coisas que uma pessoa esconde, esconde, e que é difícil contar. Mas,
    quando se conta, pronto, tudo nos aparece mais claro e sentimo-nos livres. É
    bom conversar. Esse é dos tais problemas que pode destruir um indivíduo, se ele
    o guarda para si. Mas podes ter a certeza de que todos têm medo, o problema é
    que os intelectuais o exageram, dando-lhe demasiada importância. É realmente
    aqui uma origem de classe social... Todos pensamos ter duas personalidades, a
    que é covarde e a outra, que não chamamos corajosa, mas inconsciente. O medo...
    o medo não é problema. A questão é conseguir dominar o medo e ultrapassá-lo.
    Dizes que o ultrapassas quando os outros te observam, ou quando pensas que te
    observam, que é o mais verídico... mas que, se estiveres sozinho, não és capaz.
    Talvez. Dás demasiada importância ao que os outros pensam de ti. Hoje, tu já
    não tens cor, pelo menos no nosso grupo de guerrilha estás aceite,
    completamente aceite. Não é dum dia para o outro que te vais libertar desse
    complexo de cor, não. Mas tens de começar a pensar que já não é um problema
    para ti. Talvez sejas o único que tem as simpatias e o respeito de todos os
    guerrilheiros, isso já o notei várias vezes. Não podes viver nessa angústia
    constante, senão os nervos dão de si. E hoje já não há razão.
     
    – Os meus nervos já estoiraram tantas vezes...
     
    – Ainda não. Foram só ameaços! É bom falar, é bom conversar com um amigo, a
    quem se abre o coração. Sempre que estiveres atrapalhado, vem ter comigo. A
    gente papeia. Guardar para si não dá, só quando se é escritor. Aí um tipo põe
    tudo num papel, na boca dos outros. Mas, quando se não é escritor, é preciso
    desabafar, falando. A acção é outra espécie de desabafo, muitos de nós utilizam
    esse método, outros batem na mulher ou embebedam-se. Mas a acção como desabafo
    perde para mim todo o seu valor, torna-se selvática, irracional. As outras
    formas são uma covardia. Só há a conversa franca que me parece o melhor, a mim
    que não sou escritor. Não foi por acaso que os padres inventaram a confissão,
    ela corresponde a uma necessidade humana de desabafo. A religião soube desde o
    princípio servir-se de certas necessidades subjectivas, nasceu mesmo dessas
    necessidades. Por isso o cristianismo foi tão aceite. Há certas seitas
    protestantes, não sei se todas, em que a confissão é pública. Isso corresponde
    a um maior grau de sociabilidade, embora leve talvez as pessoas a serem menos
    profundas, menos francas, na confissão. Corresponde melhor à hipocrisia
    burguesa... E daí não sei, pois eu nunca fui muito franco nas minhas confissões
    individuais de católico...

    [...]

    – Ora! Vamos tomar o poder e que vamos dizer ao povo? Vamos construir o
    socialismo. E afinal essa construção levará 30 ou 50 anos. Ao fim de cinco
    anos, o povo começará a dizer: mas esse tal socialismo não resolveu este
    problema e aquele. E será verdade, pois é impossível resolver tais problemas,
    num país atrasado, em cinco anos. E como reagirão vocês? O povo está a ser
    agitado por elementos contra-revolucionários! O que também será verdade, pois
    qualquer regime cria os seus elementos de oposição, há que prender os
    cabecilhas, há que fazer atenção às manobras do imperialismo, há que reforçar a
    polícia secreta, etc., etc. O dramático é que vocês terão razão.
    Objectivamente, será necessário apertar-se a vigilância no interior do Partido,
    aumentar a disciplina, fazer limpezas. Objectivamente é assim. Mas essas
    limpezas servirão de pretexto para que homens ambiciosos misturem
    contra-revolucionários com aqueles que criticam a sua ambição e os seus erros.
    Da vigilância necessária no seio do Partido passar-se-á ao ambiente policial
    dentro do Partido e toda a crítica será abafada no seu seio. O centralismo
    reforça-se, a democracia desaparece. O dramático é que não se pode escapar a
    isso...  – Depende dos homens, depende dos homens...
     
    – Os homens? – Sem Medo sorriu tristemente. – Os homens serão prisioneiros das
    estruturas que terão criado. Todo organismo vivo tende a cristalizar, se é
    obrigado a fechar-se sobre si próprio, se o meio ambiente é hostil: a pele
    endurece e dá origem a picos defensivos, a coesão interna torna-se maior e,
    portanto, a comunicação interna diminui. Um organismo social, como é um
    Partido, ou se encontra num estado excepcional que exige uma confrontação
    constante dos homens na prática – tal uma guerra permanente – ou tende para a
    cristalização. Homens que trabalham há muito tempo juntos cada vez têm menos
    necessidade de falar, de comunicar, portanto de se defrontar. Cada um conhece o
    outro e os argumentos do outro, criou-se um compromisso tácito entre eles. A
    contestação desaparecerá, pois. Onde vai aparecer contestação? Os
    contestatários serão confundidos com os contra-revolucionários, a burocracia
    será dona e senhora, com ela o conformismo, o trabalho ordenado mas sem paixão,
    a incapacidade de tudo se pôr em causa e reformular de novo. O organismo vivo,
    verdadeiramente vivo, é aquele que é capaz de se negar para renascer de forma
    diferente, ou melhor, para dar origem a outro.

    – Depende dos homens – disse o Comissário. – Se são indivíduos revolucionários
    e, por isso, capazes de ver quais são as necessidades do povo, poderão corrigir
    todos os erros, poderão mudar as estruturas...
     
    – E a idade? E o assento que conquistaram? Quererão perdê-lo? Quem gosta de
    perder um cargo? Sobretudo quando atingem a idade do comodismo, da poltrona
    confortável com os chinelos e os charutos que nessa altura poderão comprar? É
    preciso ser excepcional! 

    – Há homens excepcionais...
     
    – Sim, há. Uma vez todas as décadas. Um só homem excepcional poderá mudar tudo?
    Então tudo repousará nele e cair-se-á no culto da personalidade, no
    endeusamento, que entra dentro da tradição dos povos subdesenvolvidos,
    religiosos tradicionalmente. O problema é esse. É que, nos nossos países, tudo
    repousa num núcleo restrito, porque há falta de quadros, por vezes num só
    homem. Como contestar no interior dum grupo restrito? Porque é demagogia dizer
    que o proletariado tomará o poder. Quem toma o poder é um pequeno grupo de
    homens, na melhor das hipóteses, representando o proletariado ou querendo
    representá-lo. A mentira começa quando se diz que o proletariado tomou o poder.
    Para fazer parte da equipa dirigente, é preciso ter uma razoável formação
    política e cultural. O operário que a isso acede passou muitos anos ou na
    organização ou estudando. Deixa de ser proletário, é um intelectual. Mas nós
    todos temos medo de chamar as coisas pelos seus nomes e, sobretudo, esse nome
    de intelectual. Tu, Comissário, és um camponês? Porque o teu pai foi camponês,
    tu és camponês? Estudaste um pouco, leste muito, há anos que fazes um trabalho
    político, és um camponês? Não, és um intelectual. Negá-lo é demagogia, é
    populismo.

    [...]

    – Compreendi, em primeiro lugar, que o verdadeiro homem, aquele que não pode
    ser dominado, é o que pode calar a paixão para seguir friamente um plano. Todo
    o sentimento irracionaliza e, por isso, incapacita para a acção. Que todo o
    dominador é em parte dominado, é essa a relação dialéctica entre o escravo e o
    senhor de escravos. Oue as relações humanas são sempre contraditórias e que as
    não há perfeitas. Que a sorte sorri a quem a procura, arriscando. Que não há
    actos gratuitos e que não existe coragem gratuita, ela deve estar sempre ligada
    à procura dum objectivo. E que, quando alguém quer fazer uma asneira, deves
    deixá-lo fazer a asneira. Cada um parte a cabeça como quiser! Depois de ter a
    cabeça partida, aceitará melhor um conselho. Só se pode provar que um plano é
    mau, quando ele não atingir o objectivo proposto.
     
    – Dir-se-ia que toda a tua vida te levou para a estratégia militar, Sem Medo. O
    seminário, o amor... 

    – Sim. A vida modelou-me para a guerra. A vida ou eu próprio? Difícil de saber.

    [...]

    – Não temos as mesmas ideias – disse Sem Medo. – Tu és o tipo do aparelho, um
    dos que vai instalar o Partido único e omnipotente em Angola. Eu sou o tipo que
    nunca poderia pertencer ao aparelho. Eu sou o tipo cujo papel histórico termina
    quando ganharmos a guerra. Mas o meu objectivo é o mesmo que o teu. E sei que,
    para atingir o meu objectivo, é necessária uma fase intermédia. Tipos como tu
    são os que preencherão essa fase intermédia. Por isso, acho que fiz bem em
    apoiar o teu nome. Um dia, em Angola, já não haverá necessidade de aparelhos
    rígidos, é esse o meu objectivo. Mas não chegarei até lá.

    [...]

    – Eu? Eu sou, na tua terminologia, um aventureiro. Eu quereria que na guerra a
    disciplina fosse estabelecida em função do homem e não do objectivo político.
    Os meus guerrilheiros não são um grupo de homens manejados para destruir o
    inimigo, mas um conjunto de seres diferentes, individuais, cada um com as suas
    razões subjectivas de lutar e que, aliás, se comportam como tal.
     
    – Não te percebo.
     
    – Não me podes perceber. Nem te sei explicar, é tudo ainda tão confuso. Por
    exemplo, eu fico contente quando um jovem decide construir-se uma
    personalidade, mesmo que isso politicamente signifique um individualismo. Mas é
    um homem novo que está a nascer, contra tudo e contra todos, um homem livre de
    baixezas e preconceitos, e eu fico satisfeito. Mesmo que para isso ele infrinja
    a disciplina e a moral geralmente aceite. É um exemplo, enfim... Sei apenas,
    que a tua posição é a mais justa, pois a mais conforme ao momento actual. Tu
    serves-te dos homens, neste momento é necessário. Eu não posso manipular os
    homens, respeito-os demasiado como indivíduos. Por isso, não posso pertencer a
    um aparelho. A culpa é minha. Culpa! A culpa não é de ninguém.

    – Estás desmoralizado, Sem Medo.
     
    – Não – disse ele, olhando Ondina. – Estou angustiado, porque luto entre a
    razão e o sentimento.

    [...]

    – O que conta é a acção. Os problemas do Movimento resolvem-se, fazendo a acção
    armada. A mobilização do povo de Cabinda faz-se desenvolvendo a acção. Os
    problemas pessoais resolvem-se na acção. Não uma acção à toa, uma acção por si.
    Mas a acção revolucionária. O que interessa é fazer a Revolução, mesmo que ela
    venha a ser traída.

### Estudo

    – Tu, Lutamos, és um burro! – disse Sem Medo. – Quem não quer estudar é um
    burro e, por isso, o Comissário tem razão. Queres continuar a ser um tapado,
    enganado por todos... As pessoas devem estudar, pois é a única maneira de
    poderem pensar sobre tudo com a sua cabeça e não com a cabeça dos outros. O
    homem tem de saber muito, sempre mais e mais, para poder conquistar a sua
    liberdade, para saber julgar. Se não percebes as palavras que eu pronuncio,
    como podes saber se estou a falar bem ou não? Terás de perguntar a outro.
    Dependes sempre de outro, não és livre. Por isso toda a gente deve estudar, o
    objectivo principal duma verdadeira Revolução é fazer toda a gente estudar. Mas
    aqui o camarada Mundo Novo é um ingénuo, pois que acredita que há quem estuda
    só para o bem do povo. É essa cegueira, esse idealismo, que faz cometer os
    maiores erros. Nada é desinteressado.

    [...]

    – Mas não acreditas, Comandante, que haverá homens totalmente desinteressados?
     
    – Jesus Cristo?... Acho que sim, existem alguns raros. Mas não o são sempre. O
    Comissário, por exemplo, é em certa medida um desinteressado. Penso que pode
    corresponder, nalguns eleitos, a um período determinado. Mas é temporário.
    Ninguém é perpetuamente desinteressado.

### Caminho

    – Penso que é como a religião – disse Sem Medo. – Há uns que necessitam dela.
    Há uns que precisam crer na generosidade abstracta da humanidade abstracta,
    para poderem prosseguir um caminho duro como é o caminho revolucionário.
    Considero que ou são fracos ou são espíritos jovens, que ainda não viram
    verdadeiramente a vida. Os fracos abandonam só porque o seu ideal cai por
    terra, ao verem um dirigente enganar um militante. Os outros temperam-se,
    tornando-se mais relativos, menos exigentes. Ou então mantêm a fé acesa. Estes
    morrem felizes embora talvez inúteis. Mas há homens que não precisam de ter uma
    fé para suportarem os sacrifícios; são aqueles que, racionalmente, em perfeita
    independência, escolheram esse caminho, sabendo bem que o objectivo só será
    atingido em metade, mas que isso já significa um progresso imenso. É evidente
    que estes têm também um ideal, todos o têm, mas nestes o ideal não é abstracto
    nem irreal. Eu sei, por exemplo, que todos temos bem no fundo de nós um lado
    egoísta que pretendemos esconder. Assim é o homem, pelo menos o homem actual.
    Para que serviram séculos ou milénios de economia individual, senão para
    construir homens egoístas? Negá-lo é fugir à verdade dura, mas real. Enfim, sei
    que o homem actual é egoísta. Por isso, é necessário mostrar-lhe sempre que o
    pouco conquistado não chega e que se deve prosseguir. Isso impedir-me-á de
    continuar? Porquê? Se eu sei isso, a frio, e mesmo assim me decido a lutar, se
    pretendo ajudar esses pequenos egoístas contra os grandes egoístas que tudo
    açambarcaram, então não vejo porquê haveria de desistir quando outros
    continuam. Só pararei, e aí racionalmente, quando vir que a minha acção é
    inútil, que é gratuita, isto é, se a Revolução for desviada dos seus objectivos
    fundamentais.

### Criatividade

    – Já te disse que uma mulher deve ser conquistada permanentemente – disse Sem
    Medo. – Não te podes convencer que ela ficou conquistada no momento em que te
    aceitou, isso era só o prelúdio. O concerto vem depois e é aí que se vê a raça,
    o talento, do maestro. O amor é uma dialéctica cerrada de aproximação-repúdio,
    de ternura e imposição. Senão cai-se na rotina, na mornez das relações e,
    portanto, na mediocridade. Detesto a mediocridade! Não há nada pior no homem
    que a falta de imaginação. É o mesmo no casal, é o mesmo na política. A vida é
    criação constante, morte e recriação, a rotina é exactamente o contrário da
    vida, é a hibernação. Por vezes, o homem é como o réptil, precisa de hibernar
    para mudar de pele. Mas nesse caso a hibernação é uma fase intensa de
    auto-escalpelização, é pois dinâmica, é criadora. Não a rotina. Evita a rotina
    no amor, as discussões mesquinhas sobre os problemas do dia-a-dia, procura o
    fundamental da coisa. Para ti, o fundamental é a diferença cultural entre os
    dois. Ainda não te livraste desse complexo. Ao falar dela, há uma admiração
    latente pela sua maneira de se exprimir, uma procura das suas frases, da sua
    pronúncia mesmo. No entanto, tu és mais culto que ela. Os teus estudos foram
    menos avançados, mas tens uma compreensão da vida muito superior. Ela conhece
    mais Física ou Química, mas é incapaz de compreender a natureza profunda da
    oposição entre os dois pólos do eléctrodo e da sua ligação essencial. Tu pouco
    conheces de Física, mas és capaz de a compreender melhor, porque conheceste a
    dialéctica na vida. A tua acção na luta, em que estás a contribuir para
    transformar a sociedade, é um facto cultural muito mais profundo que todos os
    conhecimentos literários que ela tem. Vocês os dois podem completar-se, pois
    têm muito para ensinar um ao outro. Mas tu fechas-te no teu complexo, na
    consciência da tua incultura que, afinal, é só aparente; ela sente isso e
    considera-se intelectualmente superior, daí até ao desprezo só vai um passo. És
    tu que a levas a dar esse passo.

    [...]

    – Sempre achei ridículo o indivíduo que pega no Mao e passa uma noite a lê-lo,
    para estabelecer o plano duma emboscada. O Mao dá lições de estratégia, não a
    táctica precisa para cada momento. O indivíduo tem de ter imaginação, estudar o
    terreno, e recriar a sua táctica. Posso dar-te uma orientação, mas não os
    detalhes do procedimento. Há mulheres que amam a violência, que amam ser
    violadas, outras preferem a violação psíquica, outras a ternura, outras a
    técnica. Tens de estudar a Ondina, saber qual é o seu género e então traçar o
    teu plano. Ao meter em execução o plano, tens de ser lúcido, mas, ao mesmo
    tempo, apaixonado, intuitivo, para o poderes mudar se for necessário. A lucidez
    não significa frieza no amor. Podes ser espontâneo e lúcido.

### Prisioneiros

    – Parece-me que há três tipos de indivíduos perante a prisão – disse Sem Medo.

    – Há em primeiro lugar os que se conformam; são os desesperados, que se deixam
    destruir, que se queixam constantemente mas que aceitam, no fundo, a desgraça.
    Por isso se queixam. Formalmente, aparentemente, são os mais inconformistas,
    porque gritam, protestam, choram. Mas isso afinal é uma forma de aceitação. O
    inconformismo é uma atitude racional e coerente. Esses são apenas tipos sem
    personalidade, para quem as lágrimas ou os gritos não passam de um meio
    exterior de se crerem ainda revoltados. 

    – Porreiro! – disse Teoria. – Continua.
     
    O Comandante olhou o Comissário, que procurava manter os olhos fechados. Uma
    ruga cavou-se na testa de Sem Medo.
     
    – Há depois os inconformistas, que lutam para fugir, que preparam planos e
    criam novos logo que aqueles falharam, que vivem em oposição directa com os
    guardas, que levam pancada todo o tempo mas que se levantam em seguida.
     
    – E depois?
     
    – O terceiro tipo é o dos inconformistas serenos. Vendo que a fuga é
    impossível, organizam-se, fazem agitação junto dos outros presos, arranjam
    maneira de estudar, escrever, etc. Nunca se lamentam, porque sabem ser inútil.
    Não tentam uma fuga individual, porque é inútil. E eles detestam os gestos
    inúteis, que só desgastam a capacidade de revolta.

### Amor

    – O amor é assim. Se se torna igual, a paixão desaparece. É preciso reavivar a
    paixão constantemente. Eu não o sabia ainda, deixei-me convencer pela vida sem
    histórias que levávamos. Vês a vida dum empregado de escritório em Luanda? Está
    bem que tinha o trabalho clandestino, a Leli começava a interessar-se,
    estudávamos juntos o marxismo. Mas sentimentalmente tínhamos parado. Chegámos à
    estabilidade. A culpa foi minha que me acomodei à situação, que não me apercebi
    que a rotina é o pior inimigo do amor. Mesmo na cama nos tornámos rotineiros.

    [...]

    – Os primeiros tempos da vossa separação devem ter sido duros.
     
    – Sim. As coisas não se passaram linearmente. Tinha crises de angústia,
    misturadas a momentos de apatia. Todo o trabalho se ressentiu. À noite pensava
    que ela estava nos braços do outro. Esforçava-me então por adormecer, para me
    convencer de que era o mais forte, capaz de dominar todo o sentimento.
    Adormecia esgotado. Por vezes tinha vontade de lhe rogar que voltasse. Mas à
    sua frente mantinha um desinteresse de pedra, uma esfinge. Foi o nome que me
    dei, a Esfinge. Tornou-se o meu nome de guerra, até que me deram a alcunha de
    Sem Medo, nem sei porquê. A Esfinge ficava-me melhor.  O Comissário viu Sem
    Medo dominando o deserto, recebendo as chicotadas da areia sem mexer as
    pálpebras. Tudo se passava no interior, nas convulsões da pedra, nas correntes
    de ar percorrendo os túneis cavados pelo tempo, no lento borbulhar da matéria
    aparentemente parada.
     
    – O contrário da vida é o imobilismo – disse Sem Medo. – No amor é a mesma
    coisa. Se uma pessoa se mostra toda ao outro, o interesse da descoberta
    desaparece. O que conta no amor é a descoberta do outro, dos seus pecadilhos,
    das suas taras, dos seus vícios, das suas grandezas, os seus pontos sensíveis,
    tudo o que constitui o outro. O amante que se quer fazer amar deve dosear essa
    descoberta. Nem só querer tudo saber num momento, nem tudo querer revelar. Tem
    de ser ao conta-gotas. E a alma humana é tão rica, tão complexa, que essa
    descoberta pode levar uma vida. Conheci um tipo, um militante, que ao se juntar
    a uma mulher fez uma autocrítica sincera do que era. Passou uma noite a falar.
    Contou tudo tal qual se via. «Agora já me conheces, já estás prevenida.» Ao fim
    de um mês, a mulher abandonou-o. E ele era o melhor tipo do mundo. O seu mal
    foi aplicar à letra no amor o que aprendera no Partido sobre os benefícios da
    autocrítica.

### Moral e justiça

    – Não creio. A Direcção verá. Mas estes casos, no Movimento, implicam sempre um
    castigo. Nem que seja uma suspensão.
     
    – Sim, a eterna moral cristã! – disse Sem Medo.
     
    – Moral revolucionária, camarada.
     
    – Deixa-te disso! Moral revolucionária, nada. Seria moral revolucionária, se
    todos os casos fossem sancionados ou nenhum o fosse. Há uma série de casos
    similares que se passam, toda a gente sabe, e não se faz nada. Só quando
    provoca escândalo é que o Movimento se mete. Isso é moral cristã, que se
    interessa pelas aparências. Aliás, penso que um caso destes não é um crime
    contra o Movimento, é humano. No caso da Ondina. No do André já não, porque é
    responsável.

### Tabus

    – Ora. Que todos os homens deixam de ser estúpidos e começam a aceitar as
    ideias dos outros. Que se poderá andar nu nas ruas. Que se poderá rir à
    vontade, sem que ninguém se volte para ti e ponha um dedo na cabeça. Que se
    faça amor quando se quiser, sem pensar nas consequências. Etc., etc. Coisas
    impossíveis, como vês.
     
    – Pensas realmente isso?
     
    – Se te digo!
     
    Ondina sorriu. Apontou um bêbado que passava, cambaleando.
     
    – Também eu gostaria. No entanto, estou a apontar aquele bêbado. E na rua,
    seria capaz de me virar para trás e rir dele.
     
    – Também eu, Ondina. Isso é que me enraivece. Queremos transformar o mundo e
    somos incapazes de nos transformar a nós próprios. Queremos ser livres, fazer a
    nossa vontade, e a todo o momento arranjamos desculpas para reprimir os nossos
    desejos. E o pior é que nos convencemos com as nossas próprias desculpas,
    deixamos de ser lúcidos. Só covardia. É medo de nos enfrentarmos, é um medo que
    nos ficou dos tempos em que temíamos Deus, ou o pai ou o professor, é sempre o
    mesmo agente repressivo. Somos uns alienados. O escravo era totalmente
    alienado. Nós somos piores, porque nos alienamos a nós próprios. Há correntes
    que já se quebraram mas continuamos a transportá-las connosco, por medo de as
    deitarmos fora e depois nos sentirmos nus.

    [...]

    – Estamos a falar de coisas diferentes. No aspecto sexual, por exemplo, a tua
    moral por vezes impede-te de satisfazer os teus desejos?
     
    – Mas era isso o que eu dizia! Uma pessoa é levada a pensar nas consequências e
    trava os desejos.
     
    – Tu?
     
    – Pensas então que sou um tarado sexual...
     
    – Não. Um libertino.

    – Nem isso. Conheci um libertino. Conheci um monte de pessoas, devia ser
    escritor para as descrever. Foi em Praga, nas férias. Um verdadeiro libertino.
    Mulher que lhe agradasse não lhe escapava, mesmo se fosse a sua irmã.
     
    – Que lhe aconteceu?
     
    – Nada. Não sei, deve ter continuado assim. Eu não sou um libertino. Fui
    demasiado marcado pelos tabus para o poder ser. A um momento dado, pensei ser
    essa a solução, fiz tudo para me criar uma filosofia libertina. Mas não
    consegui, desconsegui mesmo, apareceram sempre problemas morais a estragar
    tudo.

### Guerra e jogo

    «Na Europa tive ocasião de jogar em máquinas, onde uma bolinha de metal vai
    contando pontos. O jogador só tem de fazer funcionar os flippers, quando a bola
    vai sair, ou encaminhar, com gestos doces, a bola para o sítio mais
    conveniente. O prazer do jogo não é o de vencer. É o de se atingir o êxtase, o
    esquecimento do corpo e do espírito pela concentração total na bolinha que
    salta dum lado para o outro e vai somando pontos. Havia momentos em que sabia
    que ia ganhar, atingia o estado de graça. Dominava de tal modo a máquina, pela
    força da minha tranquilidade, que, de facto, os reflexos eram perfeitos: uma
    confiança absoluta nos meus dedos que levemente tocavam os flippers, nas mãos
    que orientavam, por movimentos suaves, a bolinha para o sítio desejado. Atingia
    o estado de possessão da máquina, era sem dúvida um prazer sensual.
     
    «No jogo, o homem que se domina e ao mesmo tempo se entrega não pode ser
    escravo. Escravos são os que se entregam ao jogo sem se dominarem ou o inverso:
    é a dialéctica da dominação-submissão que distingue o homem feito para senhor,
    o dominador, e o escravo. Também no amor.

    «Há homens que vencem no póquer, embora percam dinheiro. Têm tal domínio dos
    nervos, sendo simultaneamente ousados, que os adversários são subjugados, não
    têm a iniciativa, ficam à espera das suas reacções, dos seus desejos. São os
    senhores que podem, numa cartada, arriscar tudo o que ganharam, só pelo prazer
    de arriscar. Os adversários podem ganhar, no sentido em que saem com mais
    dinheiro que o capital inicial; mas o verdadeiro vencedor foi aquele que os fez
    empalidecer, apertar os lábios, roer as unhas, tremer, ter vontade de urinar, e
    se arrepender num instante de jogar. O verdadeiro senhor, o conquistador, não
    se aborrece por ter perdido: essa é a sua ocasião de dominar e, se de facto
    impôs a sua lei, con tenta-se com a derrota. São os homens de temperamento
    mesquinho que sofrem por perder. 

    «Na guerra, também há os senhores, os que decidem. Não são fatalmente os
    chefes, embora essas características só se possam manifestar totalmente em
    situação de chefia. São os dominadores, finalmente, os mais magnânimos para os
    adversários. Fazem a guerra, em parte, como quem joga à roleta: é um meio de se
    confrontarem com o outro eu. São uns torturados. Lúcidos, compreendem que o
    inimigo em face, tomado individualmente, é um homem como eles; mas está a
    defender o lado injusto e deve ser aniquilado. A guerra revolucionária é nisso
    mais dura que as clássicas. Outrora, o combatente estava convicto que o
    estrangeiro que defrontava era o somatório de todos os vícios, de todas as
    baixezas. Era fácil odiar pessoalmente o soldado que avançava contra ele, não o
    inimigo em abstracto, mas aquele mesmo Frank, Schulz, Ahmed ou Ngonga que se
    metia à sua frente. Hoje, quem é o combatente consciente que nisso acredita? Só
    existe o ódio ao inimigo em abstracto, o ódio ao sistema que os indivíduos
    defendem. O soldado inimigo pode mesmo estar em contradição com a causa que é
    forçado a defender. O combatente revolucionário sabe disso; pode mesmo pensar
    que aquele inimigo é um bom camponês ou um são operário, útil e combativo
    noutras circunstâncias, mas que está aqui envenenado por preconceitos,
    supercondicionado pela classe dirigente para matar. O revolucionário tem de
    fazer um compromisso entre o ódio abstracto ao inimigo e a simpatia que o
    inimigo-indivíduo lhe possa inspirar.
     
    «Por isso esta guerra é mais dura, pois mais humana (e, portanto, mais
    desumana).
     
    «O dominador, o senhor, nunca procurará matar por matar, antes pelo contrário,
    evitará matar. Ele vê a guerra como o jogo ou o amor. E seu momento de perda de
    lucidez é quando o ódio abstracto se concretiza no indivíduo e avança,
    raivosamente lúcido, contra os soldados que procuram impedi-lo de avançar, não
    porque são inimigos, mas porque o impedem de avançar, são obstáculos que têm de
    ser afastados do caminho. Nesse momento, o equilíbrio está vencido e a
    necessidade psíquica – sentida físiologicamente – de fazer a acção leva ao ódio
    frio e calculado, implacável. Um dominador com ódio não gesticula, não ofende;
    ele poupa o esforço, os gestos, o ódio; é a sua acção, mais que os símbolos,
    que exprime a sua determinação.

    «Tal gostaria de ser hoje, mas este é um herói de romance. Há os camaradas
    mortos ou em perigo de morte e não consigo dominar as emoções, não consigo
    atingir o êxtase sensual de dominar, arriscando friamente, lucidamente. Há o
    João no meio, deixo de ser lúcido. E, mais do que nunca, Leli.»

### Rádio

    – Já sei – gritou Sem Medo. – És o mecânico. Que fazes aqui?
     
    – Vim ter com vocês. Quero trabalhar no Movimento. Saí do kimbo ontem de manhã,
    cheguei ao Congo sem problemas. Venho apresentar-me.
     
    O Comandante ficou um segundo hesitante, depois, num ímpeto, abraçou-o.
     
    – És bem-vindo, camarada. Como te decidiste?
     
    – Bem, aquela conversa que os camaradas tiveram connosco começou a
    convencer-me. Realmente nós somos explorados e devemos lutar. Mas o que me
    convenceu mesmo foi quando os camaradas se arriscaram tanto para me devolver o
    dinheiro. Aí, sim, eu compreendi tudo. Os camaradas eram mesmo para defender o
    povo. Comecei a ouvir a rádio, Angola Combatente. Aí aprendi umas coisas.
    Depois falei com os meus amigos, começámos a discutir da situação e do MPLA.
    Achámos que podíamos trabalhar para o Movimento mesmo lá, sem ninguém saber.
    Mas os camaradas não apareciam mais lá. Então eu vim fazer contacto.
