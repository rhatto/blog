[[!meta title="Enamoramento e Amor"]]

* Autor: Franceso Alberoni.
* Editora: Rocco.
* Ano: 1988. 
* [Versão inglesa](http://www.fallinginlovecenter.it/falling_inlove_center_download.asp).

## Trechos

    Em todos os períodos históricos que antecedem um movimento social,
    em todas as histórias pessoais que antecedem um enamoramento, há
    sempre uma grande preparação em consequência de uma mutação, de uma
    deterioração nas relações com as coisas amadas. Nesses períodos, os
    velhos mecanismos, o de depressão e o de perseguição, continuam
    funcionando. Protegemos com toda a força o nosso ideal, escondendo
    o problema. A consequência é que o movimento coletivo (o enamoramento)
    golpeia sempre de improviso. Era uma pessoa tão gentil e afetuosa, diz
    o marido (ou a mulher) abandonado. Era tão feliz comigo, pensa. Na
    realidade, ela já estava procurando uma alternativa, só que a rejeitava
    obsessivamente.

    -- págs. 16-17

    As pessoas enamoradas (e muitas vezes ambas conjuntamente) revêem o passado
    e se dão conta de que o que aconteceu foi assim porque, naquele momento,
    fizeram opções, que elas quiseram e agora não querem mais. O passado
    não é negado nem oculto, é privado de valor. É verdade que amei e odiei meu marido,
    mas não o odeio mais; enganei-me, mas posso mudar. Então o passado se configura
    como pré-história, e a verdadeira história começa agora. Desse modo terminam
    o ressentimento, o rancor e o desejo de vingança. [...] Seu passado adquiriu
    outro significado à luz de seu novo amor. No fundo, pode até continuar gostando
    do marido ou da mulher justamente por estar apaixonada. A alegria desse amor a torna
    dócil, meiga, boa. É geralmente a outra pessoa enamorada que não aceita esse
    fato, que não acredita nele.

    -- pág. 19

    Por exemplo, ele diz que me ama, mas não me leva com ele na sua vida,
    coloca-me à parte do seu trabalho; quando ele viaja, não viaja comigo;
    quer confinar-me à figura da amante que se encontra que se encontra de
    vez em quando, da amante silenciosa que ama à sombra. Ele continua a ser
    o mesmo, não pôe em risco suas relações, mantendo-as todas. Eu tenho de
    ser somente seu refúgio secreto, devo reduzir minha vida a um esperar
    que venha quando bem quiser, de acordo com as regras que se atribui.
    Não, não posso aceitar isso; para mim, isso é um não-viver. Para outra
    mulher, poderia ser, teria sido também para mim no passado, mas agora não.
    Agora quero uma vida plena. Agora peço-lhe coisas: ''Posso ir com você?''
    Minha pergunta é uma prova. Se responde que não, quer dizer que me afasta
    para onde eu não posso existir.
    
    -- pág. 61

    No início, vai procurar lutar, conquistá-lo com o fascínio, com todos os
    cuidados e dedicação, mudando sua própria maneira de ser, mas quando
    compreender que o ser amado não o ama mais, não lhe resta mais nada a
    fazer senão empunhar a espada da separação. A força que lhe resta permite-lhe
    cortar as mãos que se estendem até esse ser querido, cegar os olhos que o
    procuram por toda a parte. Pouco a pouco, para não mais desejar a quem amou,
    deverá encontrar nele razões para se desenamorar; deverá procurar refazer
    o que viveu, cobrindo de ódio tudo aquilo que foi. O ódio será sua tentativa de
    destruir o passado, mas é um ódio impotente
    
    -- pág. 67
