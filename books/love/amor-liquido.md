[[!meta title="Amor Líquido"]]

* Autor: Zygmunt Bauman.
* Editora: Zahar.
* Ano: 2008.

## Trechos

* Ratos, 20.
* Amor e criatividade, 21.
* Amor, doação, alteridade, 24.
* Economia moral, criatividade, rotina, anarquia, 93-96:

    São essas as capacidades que constituem os esteios da "economia moral" -
    cuidado e auxílio mútuos, viver _para_ os outros, urdir o tecido dos
    compromissos humanos, estreitar e manter os vínculos inter-humanos, traduzir
    direitos em obrigações, compartir a responsabilidade pela sorte e o bem-estar
    de todos - indispensável para tapar os buracos escavados e conter os fluxos
    liberados pelo empreendimento, eternamente inconcluso, da estruturação.

    [...]

    Os principais alvos do ataque do mercado são os seres humanos _produtores_.
    Numa terra totalmente conquistada e colonizada, somente _consumidores_
    humanos poderiam obter permissão de residência. [...] O Estado obcecado
    com a ordem combateu (correndo riscos) a anarquia, aquela marca registrada
    da _communitas_, em função da ameaça à rotina imposta pelo poder. O mercado
    consumidor obcecado pelos lucros combate essa anarquia devido à turbulenta
    capacidade produtiva que ela apresenta, assim como apo potencial para a
    autossuficiência que, ao que se suspeita, crescerá a partir dela. É porque
    a economia moral tem pouca necessidade do mercado que as forças deste se
    levantam contra ela.

    -- 96

* Jogo, descartabilidade do humano, 111.
* David Harvey, política local/global, 124.
* Filantropia, verdade, Hannah Arendt, 179.
* Diálogo, jogo, discussão 181.
* Finale:

    Na era da globalização, a causa e a política da humanidade compartilhada
    enfrentam a mais decisiva de todas as fases que já atravessaram em sua longa
    história.

    -- 185
