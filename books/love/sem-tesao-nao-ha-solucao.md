[[!meta title="Sem tesão não há solução"]]

* Autor: Roberto Freire.
* Editora: Guanabara.
* Ano: 1987.

## Assuntos

* Tesão: versão expandida do conceito de `awareness`, com alegria, prazer e beleza.
* Em geral é bem interessante, apesar de alguns conceitos que considero desnecessários:
  * Uso de argumentos baseados na genética biológica (por exemplo na página 36).
  * Inconsciente Coletivo (pág. 23).
  * Que a "posse e domínio sobre os outros homens" passou a ser preferido com a agricultura e domesticação de animais (pág. 32).

## Trecho

    A vida, para mim, não tem qualquer sentido. Ter nascido, estar vivo, ser eu
    mesmo o centro do Universo em termos de consciência, tudo isso eu sinto apenas
    como um fascinante, incompreensível e indesvendável mistério.
    
    Não pedi e não escolhi de quem, por que, onde e quando nascer. Da mesma forma
    não posso decidir quando, como, onde, de que e por que morrer. Essas coisas me
    produzem a sensação de um imenso e fatal desamparo, uma insegurança existencial
    permanente.
    
    O suicídio poderia ser uma contestação ao que estou afirmando, supondo-se que
    através dele posso decidir se quero ou não viver, que tenho algum poder sobre a
    vida. Mas quem me garante que o suicídio é realmente um ato voluntário?
    Acredito

    -- 19

    Poder, hierarquia, perversão, crueldade, sadomasoquismo, paranóia, dor-prazer de mandar.

    -- 30

    Parece, à primeira vista, sobretudo para os que ainda o tem enrustido e
    reprimido, não ser fácil disciplinar o tesão. Sem dúvida, mas para entender
    isso direito faz-se necessário primeiro investigar os conceitos de disciplina,
    responsabilidade e imaturi- dade para os protomutantes. Penso que não existe
    nada tão rigidamente disciplinado quanto as forças da Natureza que produzem o
    dia e a noite, as marés, as ondas do mar, as chuvas, os ventos, a vida vegetal
    e animal, as estações. Tudo no Universo obedece a um tipo de disciplina rígida,
    mas contraditória também.
    
    [...]
    
    Isto quer dizer que na Natureza, fazendo uma comparação com a criação musical,
    a vida se organiza num modelo rítmico mais ou menos rígido de tempo e de
    espaço, mas é todo livre o seu tesão, que varia segundo as circunstâncias
    produzidas por variáveis para nós imperceptíveis e que geram sempre novas e
    originais melodias e harmonias. O sol se põe diariamente, mas nunca o
    crepúsculo se apresenta igual, no mesmo horário e no mesmo lugar. Nem sua

    -- 38

Será que a oposição a seguir é necessária?:

    A atitude relativista — a busca do tesão e o comportamento lúdico dos
    protomutantes em seu cotidiano libertário — pode ser encarada como imaturidade
    e irresponsabilidade pelos tradicionalistas. As pessoas sérias e que vivem sob
    e no controle de valores absolutos, colocam o dever antes do prazer ou o dever
    em lugar do prazer. Talvez o único prazer possível para eles seja a sensação
    aliviada e orgulhosa do dever cumprido a qualquer custo. Essa atitude perante a
    vida convencionou-se chamar de comportamento responsável, que é exatamente o
    contrário, na teoria e na prática, do comportamento tesudo.

    -- 39

    Tudo isso foi dito para afirmar que o uso da palavra responsabilidade é algo
    indevido e impróprio, porque ela não significa qualquer ato moral, atestando
    sua capacidade de cumprir obrigações e deveres, como também não designa pessoa
    confiável e disciplinada.  A origem latina da palavra responsabilidade
    significa apenas a capacidade de dar resposta e, supõe-se, capacidade de dar
    respostas adequadas, próprias, originais e satisfatórias a estímulos que se
    recebe. Claro que cada resposta reproduz nossa ética, nossa ideologia, nossa
    psicologia.  Em síntese, quero dizer que ser mais ou menos responsável
    corresponde à relação dialética entre o nosso medo e o nosso tesão no viver
    cotidiano.

    -- 40

    Já se viu alguma forma de vida e de trabalho escravo à qual o homem foi
    submetido, na qual lhe tenha sido negado o direito a uma companhia para o afeto
    e para o sexo? Um mínimo de afeto, qualquer possibilidade de satisfação sexual,
    além de procriação garantida, mesmo nas condições mais miseráveis e sórdidas,
    parecem suficientes para garantir a circulação da energia vital necessária no
    corpo das pessoas e para alimentar a sua ilusão de felicidade. Ilusão que
    justificaria o fato do homem se submeter à escravidão. Essa capacidade de
    resistir vivo, mesmo como escravo, não pode ser explicada sem se recorrer ao
    poder da fé, do qual o poder do amor seria apenas uma subsidiária ou uma
    eventual encarnação.

    -- 46

    Arte, beleza, estética.

    -- 57-74

    Eu gosto muito da geração hippie, e sei distinguir o joio do trigo. Sei onde
    está a neurose e a sacanagem do hippie, e a verdade dele. Pra mim o hippie
    certo é o sujeito que se nega a participar da sociedade de consumo, sem deixar
    com isso de participar da história da humanidade. Conheço vários hippies, em
    todos os campos: letras, ciências, artes, filosofia, etc., que se negam a
    participar da sociedade de consumo mas não param de criar, de lutar. O que não
    faz nada, só está na dele, esse é o sacana, porque termina sendo um parasita
    dos consumistas.

    -- 99-100

    Conheço casos de pais que batiam nos filhos dizendo que faziam isso para evitar
    que os filhos apanhassem da polícia.

    -- 109

    RF. — Nos anos 60 houve o movimento hippie, as manifestações de 68 na França,
    mas todas as questões colocadas na época foram absorvidas pelo consumo. Houve
    então uma grande frustração. Não vejo hoje um processo que determine que
    devemos mudar nossa maneira de vestir, nossos cabelos, pois isso é muito
    superficial. Mesmo com nossas calças jeans, dentro desse sistema, podemos ser
    revolucionários.

    Outro fator foram as falências, tanto dos modelos capitalistas quanto dos
    socialistas autoritários. Nenhum deles ousou romper com a estrutura da família,
    conservando-a como célula autoritária, o que permitiu a manutenção de governos
    autoritários. Bakunin, um anarquista, foi exilado logo após a Revolução Russa.
    Mesmo os revolucionários temiam a visão anarquista.  Conheci nos tempos da luta
    armada diversos companheiros que davam a vida pelo socialismo na luta, mas eram
    tiranos com suas mulheres e seus filhos. Se tomassem o poder, que sociedade
    iriam propor? Hoje a juventude tem consciência da falência dessas soluções.

    -- 110

    Por segurança financeira quero dizer também uma certa estabilidade social; toda
    vez que eu mudei de profissão eu tive de, duramente, iniciar do zero.
    Entretanto, agindo assim, chega um certo período da sua vida em que se você
    tentou várias coisas, você terá três ou quatro possibilidades de trabalho.

    -- 126

    P. — Como fica a pessoa que incorpora o senso comum, isto é, a pessoa que se
    limita em favor de uma segurança financeira e de uma aceitação social, mesmo
    adivinhando outras possibilidades?

    RF. — Este é o ponto mais grave. Eu sou terapeuta e posso dizer que 80% dos
    meus clientes têm problemas psicológicos por não estarem fazendo o que
    gostariam de fazer. As pessoas fazem, convencidas pelas suas famílias, o que o
    meio social prefere; isto de fazer o que é imposto provoca nessas pessoas um
    grande sofrimento, que muitas vezes estoura fora do trabalho, estoura em sexo,
    em agressividade, em equilíbrio mental.

    [...]

    Numa sociedade como a nossa, com esta família autoritária e cumpridora das
    normas do Estado, as pessoas sensíveis, cujo projeto de vida não está dentro do
    que espera o meio social, sofrerão muita repressão; e esta é uma repressão
    muito danosa, pois é castrativa. Uma pessoa que não faz o que precisa fazer,
    tende a adoecer, perde, no mínimo, a identidade e o auto-respeito.

    -- 127

    Uma pessoa livre é uma pessoa altamente solidária, à medida que fica mais livre
    fica menos egoísta, necessita mais dos outros. Enquanto egoísta, não percebe
    nem os outros e nem a sociedade. Ela vive realmente muito voltada para si, em
    busca de soluções para o seu sofrimento ou apenas amarga, impotente.

    -- 143

    É preciso nunca esquecer que uma pessoa neurótica e que procura terapia ainda
    está viva e ainda pode ser salva. Seus sintomas são seus gritos de socorro,
    porque ainda querem ser elas mesmas e estão dispostas a lutar contra o que as
    reprime. As demais, são as que já se submeteram.  As outras são as que
    enlouqueceram e vivem internadas ou são as que se matam.

    -- 147

## Amor

    Em síntese, é o seguinte: pros jovens terem o amor de que necessitam pra amar e
    criar, têm de enfrentar toda a estrutura em que vivem, a familiar, a social, os
    preconceitos e tradições, etc.

    [...]

    As colocações tradicionais do tipo amor-pra- sempre,
    até-que-a-morte-nos-separe, etc., são as grandes fórmulas de escravização de
    uma pessoa às outras. Pra ter grana de sustentação, o casal tem de fazer tudo o
    que a estrutura quer.  Acho que temos de educar os filhos e alunos a não
    aceitar nenhuma dessas regras. Romper com as coisas significa fazer o que se
    tem necessidade e não o que é imposto. Deixar o cabelo crescer, deixar a escola
    se ela for quadrada. Devemos chuchar a leitura neles, para se complementarem.
    Não se pode forçá-los a nada. Deve-se dizer a eles que rompam com o que não
    estiver dando. Não está dando? Deixe de lado a amizade, o trabalho, a mulher, a
    família. Não fique com nada que não esteja dando, pois senão você se estrepa.
    Sinto na pele como, pra nossa geração, isso é difícil.

    -- 99

    Dentro da minha visão, acho o casamento um absurdo. Eu gostaria de falar sobre
    o acasalamento, porque quanto ao casamento eu só quero que acabe, que não
    exista mais, porque o casamento é uma das grandes fontes de destruição do amor.
    Mas o acasalamento é indispensável. Acho que as novas formas de acasalamento
    que hoje estão sendo pesquisadas pela juventude no mundo são a medida das
    transformações atuais na história do homem. Porque não acredito que haverá
    modificação alguma nesta fase da história do homem se não se modificarem as
    regras de funcionamento e os objetivos da família.

    -- 115-116

    O que acho mais bonito e vejo no amor dos casais revolucionários é como eles o
    vivem de forma lúdica, brincando e jogando sempre. A ludicidade é a mãe do
    tesão e, ao mesmo tempo, o pai da criatividade. É o processo de criação, no
    amor, que garante a sua sobrevivência.

    - 118

    Acho interessantes as relações de suplementação entre os casais porque nós
    estamos acostumados a buscar no outro algo que nos complete. Isso para mim é um
    grande erro, porque a grande novidade que eu estou vendo é as pessoas não
    precisarem nada uma das outras para que o amor seja realmente uma troca de
    emoções, de delícias, de encantamentos e não apenas um quebra-galho onde um dá
    força para a fraqueza do outro. As pessoas se encontram para brincar, para ler
    juntas, para fazer

    -- 119

## Felicidade e alegria

    A grande decepção dos amantes que buscam a felicidade (estado de prazer
    permanente, institucionalizado) através do amor é produzida pela sua
    incapacidade em aceitar que, como todas as coisas vivas, o amor também tem um
    começo, um meio e um fim.  Nem seu tempo, nem seu espaço e nem o seu tesão
    podem ser determinados e controlados por razão da vontade e pela força de
    poder. Assim, às vezes somos obrigados a perder um amor precocemente, ou a ter
    de suportá-lo desnaturado, moribundo ou mesmo morto. Só a liberdade e a
    autonomia nos ensinam a aceitar biológica e humanamente o tempo, o espaço e o
    tesão das coisas vivas.

    Creio já ter conseguido expor todas as razões que me levam a concluir ser a
    felicidade algo impossível de se atingir sem a deformação biológica e
    psicológica do ser humano e, mesmo quando isso é realizado, não se trata de
    prazer saudável e libertário o que se conseguiu, mas, exatamente, a sua
    contrafação: o poder autoritário e patológico. Logo, definitivamente, a
    felicidade é uma coisa impossível de ser alcançada. Em verdade, acho que ela
    não existe e nem é, sequer, necessária. Penso mesmo que sua ideologia deve ser
    fortemente combatida tanto como estratégia política ou como delírio religioso e
    patológico, produzidos ora pela fome e ora pelo desespero, tanto pela miséria
    quanto pela carência amorosa, mas sempre, em quaisquer situações, manipulados
    pela paranóia autoritária do poder.

    -- 48

    Mas, felizmente, apesar de tudo, estamos com a vida do nosso lado e eles, os
    profetas da Felicidade, algum dia também compreenderão que sem tesão não há
    mesmo solução. Mas, ainda alucinados e insaciáveis, estão muito longe de se
    convencer, como nós, modesta e espertamente, que em lugar de qualquer tipo de
    felicidade aqui e no além, buscamos simples e naturalmente a alegria. Alegria,
    coisa tão incerta como o vento, que tem dia que vem, dia que não vem, que às
    vezes é tão forte que vira tufão, outras parece apenas brisa suave, que pode
    vir do sul ou do norte, do leste ou do oeste, mas que vem, queiramos ou não, na
    hora ou no jeito que bem entender.

    -- 50

    Falo de alegria entendida como a expressão gostosa, saborosa, orgástica e
    encantada do funcionamento satisfatório e equilibrado de nossa energia vital no
    plano físico, emocional, psicológico, afetivo, sensual, ético, ideológico e
    espiritual. Tudo isso significa também metabolismo, num sentido amplo.

    -- 51

## Psicanálise

    Nascida profundamente do pensamento pessimista, de triste, um homem amargo e
    genial, mas ressentido, a Psicanálise reflete as características da história da
    religião e da raça judaica no mundo. Sigmund Freud era um homem que, segundo
    seu discípulo Wilhelm Reich, permitia-se pouca convivência com o prazer, com a
    beleza e com a alegria lúdicos da existência comum, o que provam suas obras
    principais. Afirmar a existência, no homem, de um instinto de morte, é supor
    ser a morte um mal, algo destrutivo e não simplesmente a ausência, o fim da
    vida. A sua afirmação de que a morte é uma entidade equivalente à da vida,
    parece coisa mais de sua formação religiosa que da científica, pois ela sujeita
    o homem aos poderes políticos (via religião ou via ciência) para ser exorcizado
    do mal inerente à sua natureza imperfeita. Isso os torna incompetentes e
    perigosos para a vida social, sem proteção, tutela e controle permanentes.
    Enfim, dependência, resultante da suposição de sua incapacidade natural para a
    autonomia e a autodeterminação.

    Vivendo em sistemas políticos autoritários, aos quais tanto religião como
    ciência estão ligados, associados e dependentes, a visão trágica da existência
    é um dos suportes ideológicos mais poderosos e úteis para a sua manutenção.
    Assim, por exemplo, a análise infinita e inútil do complexo de Édipo. Para mim,
    não passa de um exercício de poder, tentativa de sujeição ao poder do pai, bem
    como ao do analista. Édipo, personagem da tragédia grega, que arranca os
    próprios olhos como punição para aliviar o sentimento de culpa por ter transado
    sexualmente com uma mulher que desconhecia ser sua mãe, mostra claramente como
    Freud, ao adotá-lo para simbolizar o complexo por ele descoberto, necessitava
    do sentimento trágico da existência para justificar a crença de que a vida
    lúdica dos homens estará inexoravelmente sujeita ao controle e punição por
    parte dos poderes autoritários e deuses antropomórficos, criados e utilizados
    por seus representantes na Terra.

    -- 26-27

    Meu analista fez um esforço total para eu poder projetar meu pai nele e
    conseguiu, era um grande analista vienense. Mas o que me incomodava mais não
    eram os problemas teóricos, era a impossibilidade de viver a vida. Você ficar
    trancado das oito da manhã às sete da noite num consultório fechado, ouvindo as
    mesmas pessoas todos os dias, é um negócio muito doloroso.

    -- 149

## Individual e coletivo

    E dentro da consciência de cada pessoa e a cada instante, a realidade estará
    sendo vista e percebida através de movimentos contraditórios como na dialética
    tradicional, mas os protomutantes não vão se satisfazer com os estágios
    provisórios que forem sendo atingidos. Eles vão querer superá-los sempre: negar
    ao mesmo tempo o que afirmam e combater o que acabam de provar. Sua consciência
    será polar. Mas polar no sentido de que num extremo está a sua tendência
    natural social, que impele a pessoa a se integrar no meio ambiente em que vive
    e que forma a extensão externa mais imediata de seu universo. E, no outro
    extremo, está a sua outra tendência, também primária e natural, de se afirmar
    como individualidade, com realidade e identidade próprias. A sua sabedoria não
    consiste em colocar o barco ideológico e moral no rumo do meio-termo, mas saber
    quando é a hora de se associar aos outros e quando é o momento de se
    distanciar, quando é o tempo de pensar em si mesmo e quando é a hora de
    esquecer de si.

    Esse “quando é a hora”, que constitui o núcleo da sabedoria do protomutante,
    está sempre ligado (no sentido de atento) e atuando de modo permanente. Ele só
    pode ser explicado como produto do tesão regulando e harmonizando a relação do
    espaço e do tempo na vida das pessoas livres. Assim, constatando que o
    protomutante se orienta fundamentalmente por seu tesão, concluímos que terá de
    ser lúdico o seu viver cotidiano, que vai combater por pura vocação todas as
    formas de autoritarismo, que seu prazer serve mesmo é para amar e para criar,
    que sempre vai encontrar um jeito potente e competente para se associar e
    conviver, para ser amigo, para ser amante e companheiro, para ser cúmplice e
    solidário com as pessoas que seleciona ao sabor das ondas e marés do seu
    encantado tesão cotidiano.

    -- 35

## Organização

    Refiro-me à relação misteriosa e dinâmica entre o todo e as partes, existente
    na essência e na estrutura das coisas vivas. O seu todo é muito mais e outra
    coisa que a simples soma e a superposição das suas partes. Cada parte viva,
    resumida até o limite da unidade, contém em si, de modo sintético, cifrado e em
    potencial, o que é e significa o todo a que pertence. Da relação dinâmica entre
    o todo e as partes, surge a energia que sintetiza e cifra o potencial da vida
    nas partes e que se revela, se expande e se integraliza na organização do todo.
    
    Estou falando, por exemplo, do mistério que produz nas sementes vegetais a
    presença dos potenciais da árvore adulta e, inclusive, o potencial desta
    produzir novas sementes da mesma espécie que, quando germinadas, produzirão
    árvores adultas, mas originais e diversas das que as geraram.

    -- 22

    Além disso, o relativismo explica também a naturalidade existente na atividade
    lúdica do homem novo em sua vida cotidiana. O jogar e o brincar adultos
    traduzem nossa vocação para criar e amar, bem como a de lutar, também
    ludicamente, para nos garantir a liberdade relativa de amar criativamente e de
    criar amorosamente.

    Logo, o relativismo assim compreendido e assumido afasta qualquer necessidade
    de poder hierarquizado e autoritário na organização das relações interpessoais,
    familiares e sociais, sobretudo de poderes que pretendem ser absolutos.

    -- 29-30

    Como se poderia entender o significado da liderança em associações nas quais
    não estaria existindo nenhuma forma de autoritarismo? Para isso vamos nos
    socorrer da genética que explica o conceito protomutante de “diferença”, um dos
    pilares de sua ideologia anarquista: a natureza necessita que os homens não
    sejam iguais uns aos outros, a fim de poder ampliar a amostragem e as
    alternativas na dinâmica da seleção natural.

    [...]

    Acredito que inteligência e criatividade possuem algo em
    comum, mas nenhuma delas é apenas isto: capacidade de
    solucionar o mais rápida e satisfatoriamente possível problemas
    novos e inéditos.

    -- 36

Interessante, exceto pela repartição não-igualitária de "benefícios e lucros",
o que pode ser entendido como uma falta de generosidade (modelo da regulação
por trocas versus desregulação da doação):

    Considero, pois, a liderança algo que é, além de emergente, também
    circunstancial, temporária, descartável e recorrível. Até as lideranças
    emergentes se tornam autoritárias se se estratificam, se se impõem e se tornam
    estáveis, estáticas, únicas e permanentes.  Sendo assim, na organização da vida
    social dos

    Sendo assim, na organização da vida social dos protomutantes até os exercícios
    do prazer e do poder de liderança se enquadram no relativismo lúdico geral da
    vida, tornando-se assim um grande tesão ser líder e ser liderado dessa forma.
    Como os protomutantes só podem se associar de forma anárquica, tanto para
    conviver quanto para produzir, a sua dinâmica organizativa, além de funcionar
    através de uma dialética ecológica, irá também descobrir maneiras originais e
    específicas de funcionamento em autogestão. Isto quer dizer que a sua produção
    é projetada, organizada, financiada, gerida, controlada e utilizada por quem
    produz, recaindo seus benefícios e seus lucros de modo proporcional à qualidade
    e à quantidade de trabalho criativo realizado por cada produtor. A decisão a
    esse respeito é tomada por todos aqueles que produzem.

    -- 37

## Autoritarismo

    Então, foi justamente convivendo com essas famílias que pude descobrir o quanto
    éramos autoritários, apesar de estarmos arriscando nossas vidas no combate ao
    autoritarismo da direita brasileira. Falo do autoritarismo na relação com a
    companheira ou com o companheiro, na relação com os filhos, na relação com os
    demais companheiros fora do campo de luta. Eu, nessa época, já me escandalizara
    com meu machismo disfarçado e me submetia à Somaterapia. Alguns desses
    companheiros, como eu, acabaram por sacar essa contradição absurda e,
    inclusive, submeteram-se também à Soma, em sessões individuais ou em grupos,
    nestes arriscando-se enormemente, pois trabalhávamos de madrugada, em lugares
    variados e sob severo sistema de segurança, que nem sempre funcionava. Eu
    ficava pensando que a violência autoritária da direita sobre a gente era a
    contrapartida do medo que tinham da violência do nosso autoritarismo de
    esquerda. Nós queríamos o poder, nós e eles, para exercer a mesma atitude
    política de posse, domínio, controle e mando na individualidade do outro,
    sobretudo através da prioridade e prevalência de um social abstrato sobre um
    pessoal concreto, assim como se quem tivesse o poder era o dono e o
    representante do social, ao que todas as pessoas tinham de se submeter. O
    abstrato do social tornava-se coisa concreta nas pessoas que encarnavam o
    poder. E o poder assim instituído só pode ser de cima para baixo e arbitrário.

    -- 134
