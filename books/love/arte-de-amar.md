[[!meta title="A Arte de Amar"]]

* Autor: [Erich Fromm](https://pt.wikipedia.org/wiki/Erich_Fromm).
* Ano: 1956.
* [Íntegra](http://estudioterraforte.com.br/wp-content/uploads/2013/07/arte-de-amar.pdf).

## Trechos

    O homem é dotado de razão; é a vida consciente de si mesma; tem,
    consciência de si, de seus semelhantes, de seu passado e das possibilidades de
    seu futuro. Essa consciência de si mesmo como entidade separada, a
    consciência de seu próprio e curto período de vida, do fato de haver nascido
    sem ser por vontade própria e de ter de morrer contra sua vontade, de ter de
    morrer antes daqueles que ama, ou estes antes dele, a consciência de sua
    solidão e separação, de sua impotência ante as forças da natureza e da
    sociedade, tudo isso faz de sua existência apartada e desunida uma prisão
    insuportável. Ele ficaria louco se não pudesse libertar-se de tal prisão e
    alcançar os homens, unir-se de uma forma ou de outra com eles, com o
    mundo exterior.

    -- 15

    a união com o grupo é o modo predominante de superar a
    separação, É uma união em que o ser individual desaparece em ampla escala,
    em que o alvo é pertencer ao rebanho. Se sou como todos os mais, se não
    tenho sentimentos ou pensamentos que me façam diferentes, se estou em
    conformidade com os costumes, idéias, vestes, padrões do grupo, estou salvo;
    salvei-me da terrível experiência da solidão. 

    -- 18

    não importando que esse uso fosse cruel ou “humano”.
    Na sociedade capitalista contemporânea, o significado de igualdade
    transformou-se. Por igualdade, faz-se referência à igualdade dos autômatos,
    dos homens que perderam sua individualidade. Igualdade, hoje significa
    “mesmice”, em vez de “unidade”. É a mesmice das abstrações, dos homens
    que trabalham nos mesmos serviços, têm as mesmas diversões, lêem os
    mesmos jornais, experimentam os mesmos sentimentos e as mesmas idéias.

    [...]

    A sociedade contemporânea advoga esse ideal de igualdade não individualizada,
    porque necessita de átomos humanos, cada qual o mesmo, a fim de fazê-los
    funcionar numa agregação de massa, suavemente, sem fricções, obedecendo todos
    ao mesmo comando e, contudo, convencido cada qual de estar seguindo seus
    próprios desejos. Assim como a moderna produção em massa exige a padronização
    dos artigos, também o processo social requer a padronização do homem, e tal
    padronização é chamada “igualdade”.

    -- 20

    Mesmo seu funeral, que ele antevê como o último de seus grandes eventos
    sociais, está em estreita conformidade com os padrões.  Além da conformidade
    como meio de aliviar a ansiedade que nasce da

    -- 21

    Quase não é necessário acentuar o fato de que a capacidade de dar
    depende do desenvolvimento do caráter da pessoa. Pressupõe o alcançamento
    de uma orientação predominantemente produtiva; nessa orientação a pessoa
    superou a dependência, a onipotência narcisista, o desejo de explorar os
    outros, ou de amealhar, e adquiriu fé em seus próprios poderes humanos,
    coragem de confiar em suas forças para atingir seus alvos. No mesmo grau em
    que faltarem essas qualidades é ela temerosa de dar-se — e, portanto, de amar.

    -- 27

    Cuidado, responsabilidade, respeito e conhecimento são mutuamente
    interdependentes. Constituem uma síndrome de atitudes que vamos encontrar
    na pessoa amadurecida, isto é, na pessoa que desenvolve produtivamente seus
    próprios poderes, que só quer ter aquilo por que trabalhou, que abandonou os
    sonhos narcisistas de onisciência e onipotência, que adquiriu humildade
    alicerçada na força íntima somente dada pela genuína atividade produtiva.

    -- 31

    Problema: considera o homossexualismo como desvio e fracasso.

    -- 32

    Segurança, prisão.

    -- 93
