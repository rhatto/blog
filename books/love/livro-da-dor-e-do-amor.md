[[!meta title="O Livro da Dor e do Amor"]]

* Autor: J. D. Nasio.
* Ano: 210.
* Editora: Zahar.

## Trechos

    A imagem do ser perdido não deve se apagar; pelo
    contrário, ela deve dominar até o momento em que — graças ao luto
    — a pessoa enlutada consiga fazer com que coexistam o amor pelo
    desaparecido e um mesmo amor por um novo eleito. Quando essa
    coexistência do antigo e do novo se instala no inconsciente, podemos
    estar seguros de que o essencial do luto começou.

    -- 13

    Eu também estava surpreso de ter
    expresso espontaneamente, em tão poucas palavras, o essencial da
    minha concepção de luto, segundo a qual a dor se acalma se a pessoa
    enlutada admitir enfim que o amor por um novo eleito vivo nunca
    abolirá o amor pelo desaparecido.

    -- 14

    dar um sentido à dor do outro significa, para o psicanalista,
    afinar-se com a dor, tentar vibrar com ela, e, nesse estado de resso-
    nância, esperar que o tempo e as palavras se gastem.

    -- 16

    Ao longo destas páginas, gostaria de transmitir o que eu próprio aprendi,
    isto é, que a dor mental não é necessariamente patológica; ela baliza
    a nossa vida como se amadurecêssemos a golpes de dores sucessivas.

    -- 17-18

    Para quem pratica a psicanálise, revela-se com toda a evidência —
    graças à notável lente da transferência analítica — que a dor, no coração
    do nosso ser, é o sinal incontestável da passagem de uma prova. Quando
    uma dor aparece, podemos acreditar, estamos atravessando um limiar,
    passamos por uma prova decisiva. Que prova? A prova de uma
    separação, da singular separação de um objeto que, deixando-nos súbita
    e definitivamente, nos transtorna e nos obriga a reconstruir-nos.

    -- 18

    O luto
    do amado é, de fato, a prova mais exemplar para compreender a natureza
    e os mecanismos da dor mental. Entretanto, seria falso acreditar que a
    dor psíquica é um sentimento exclusivamente provocado pela perda de
    um ser amado. Ela também pode ser dor de abandono, quando o amado
    nos retira subitamente o seu amor; de humilhação quando somos
    profundamente feridos no nosso amor-próprio; e dor de mutilação
    quando perdemos uma parte do nosso corpo. Todas essas dores são,
    em diversos graus, dores de amputação brutal de um objeto amado, ao
    qual estávamos tão intensa e permanentemente ligados que ele regulava
    a harmonia do nosso psiquismo. A dor só existe sobre um fundo de amor.

    -- 18

    Antes de tudo, a dor é um afeto, o derradeiro
    afeto, a última muralha antes da loucura e da morte. Ela é como que
    um estremecimento final que comprova a vida e o nosso poder de nos
    recuperarmos. Não se morre de dor. Enquanto há dor, também temos
    as forças disponíveis para combatê-la e continuar a viver. É essa noção
    de dor-afeto que vamos estudar nos primeiros capítulos.

    -- 19-20

    Quer se trate de uma dor corporal provocada por uma lesão dos
    tecidos ou de uma dor psíquica provocada pela ruptura súbita do laço
    íntimo com um ser amado, a dor se forma no espaço de um instante.
    Entretanto, veremos que a sua geração, embora instantânea, segue um
    processo complexo. Esse processo pode ser decomposto em três tem-
    pos: começa com uma ruptura, continua com a comoção psíquica que
    a ruptura desencadeia, e culmina com uma reação defensiva do eu para
    proteger-se da comoção. Em cada uma dessas etapas, domina um
    aspecto particular da dor.

    -- 20

    Como diferencia ele cada um desses afetos? Propõe o
    seguinte paralelo: enquanto a dor é a reação à perda
    efetiva da pessoa amada, a angústia é a reação à
    ameaça de uma perda eventual.

    -- 27

    Mas qual é essa reação? Diante do transtorno pul-
    sional introduzido pela perda do objeto amado, o eu
    se ergue: apela para todas as suas forças vivas —
    mesmo com o risco de esgotar-se — e as concentra
    em um único ponto, o da representação psíquica do
    amado perdido. A partir de então, o eu fica inteira-
    mente ocupado em manter viva a imagem mental do
    desaparecido. Como se ele se obstinasse em querer
    compensar a ausência real do outro perdido, magnifi-
    cando a sua imagem. O eu se confunde então quase
    totalmente com essa imagem soberana, e só vive
    amando, e por vezes odiando a efígie de um outro
    desaparecido.

    -- 28
