[[!meta title="A ajuda múltipla e o valor social"]]

Esta é uma versão antiga:

* [Versão atual](https://ensaios.fluxo.info/valor.html) disponível
  nos [Ensaios Vertiginosos](https://ensaios.fluxo.info).
* [Versão original](valor-social.pdf) ([fonte](valor-social.tex)).

Procurando resolver um problema prático, este texto sistematiza uma
forma de promover a ajuda múltipla através de acordos sucessivos e
virais. Para auxiliar na sua compreensão, é definida uma forma de
cálculo do valor social e suas consequências são avaliadas.

Motivação
=========

Em geral, quando ajudamos alguém (principalmente quando ensinamos algo),
não há muita garantia que a pessoa ajudada passará a idéia pra frente,
seja ajudando outrem ou passando o conhecimento adiante. Mesmo em
coletivos horizontais, não-hierárquicos e baseados na ajuda mútua, não
há necessariamente uma cultura de passar para frente a ajuda recebida.
Por isso, estabelecemos neste texto uma sugestão de acordos de ajuda
múltipla tanto como proposta de prática e sobretudo como reflexão da
distância que os grupos sociais se encontram com relação a um regime de
dádiva e não-escassez.

O acordo de ajuda múltipla
==========================

Para fomentar o aumento da ajuda entre as pessoas, criaremos o conceito
de *ajuda múltipla* e proporemos um pequeno acordo padrão para o seu
estabelecimento. Pois bem: *ajuda múltipla é a forma de colaboração onde
uma ou mais pessoas -- grupo A -- auxiliam outras -- grupo B -- com a condição
de que estas últimas efetuem ajuda múltipla auxiliando outras pessoas
-- grupo C.* Atente para o fato de que definição é *recursiva* (isto é, a
definição necessita de sua própria definição): uma ajuda múltipla seria,
por exemplo, Maria ajudar Lopes com a condição de que este ajude alguém
no futuro. Note que o grupo C pode ser composto pelas mesmas pessoas do
grupo A, mas não necessariamente: Lopes deve ajudar alguém, mas não
necessariamente Maria[1].

Viralidade (ou potência) do acordo
----------------------------------

Estamos interessados/as na possibilidade da multiplicação da ajuda e,
para tanto, devemos melhorar nossa definição de ajuda múltipla: ajuda
múltipla é a forma de colaboração onde uma ou mais pessoas -- grupo A --
auxiliam outras -- grupo B -- com a condição de que estas últimas efetuem
*pelo menos _v_ ajudas múltiplas* (onde _v_ é um número inteiro
positivo) auxiliando outras pessoas -- grupo C, D, E, etc -- com a condição
de que as próximas pessoas também pratiquem ajuda múltipla e assim por
diante.

Nesta segunda definição, introduzimos o que chamaremos de *viralidade:*
não apenas a pessoa ajudada precisa participar de pelo menos mais _v_
acordos de ajuda como as pessoas ajudadas por esses próximos _v_ acordos
precisam, após serem ajudadas, participarem como ajudantes em pelo menos
mais _v_ acordos[2].

A idéia principal da viralidade é que ela representa o custo social de
uma ajuda: se recebo uma ajuda, devo retribuir não exatamente a quem me
ajuda mas a todo o grupo social, participando como ajudante em pelo
menos _v_ outros acordos.

Por isso, os acordos não devem ser entendidos como moedas de troca: a
moeda abstrai e aliena as relações sociais – já que pode ser trocada –
enquanto que o acordo reforça e encoraja relações sociais. A moeda
conserva valor (uma vez que ela é criada, basta que circule)[3]. Os
acordos, ao contrário, geram valor o tempo todo por causa de sua
viralidade. Eles criam valor social sem precisarem ser trocados, já que
eles se reproduzem. Assim, devem ser entendidos mais na lógica da dádiva
do que do contrato social.

Modelo de acordo viral
----------------------

Na prática, convém termos um modelo de acordo para facilitar o
dia-a-dia: pessoas nos pedem ajuda e em geral precisamos dar uma
resposta rápida. Um modelo de acordo – onde o/a proponente pode ser
qualquer uma das partes envolvidas e os acordos podem ser de múltiplas
partes – deve ser simples e eficaz e por isso o texto do modelo de
acordo abaixo serve para criar pequenos acordos entre pessoas:

    Acordo de ajuda múltipla
    ------------------------

    O/a proponente/a deste acordo tem como objetivo multiplicar seus esforços de
    ajuda. Para tal, é utilizado o princípio da reprodução viral de atividades
    culturais.

    Neste acordo, as pessoas ajudantes concordam a ajudar as pessoas, doravante
    denominadas como ajudadas, desde que as ajudadas concordem em participar como
    ajudantes em pelo menos X próximos acordos deste mesmo tipo (nos quais, por sua
    vez, as pessoas ajudadas deverão participar como ajudantes em pelo menos X
    acordos deste mesmo tipo e assim sucessivamente).

    A contrapartida não precisa ser necessariamente no mesmo teor da ajuda
    prestada.

Esse modelo de acordo não pretende apenas incentivar a iniciativa e o
protagonismo como também encorajar quem não ajuda ou não pede ajuda por
conta de algum receio. Não podemos também deixar de mencionar que estes
tipos de acordo só fazem sentido e apenas serão necessários enquanto a
ajuda mútua/múltipla não for uma prática cultural comum e generalizada,
quando então a prática descartará a necessidade de microacordos.

O modelo acima é apenas uma sugestão: muitos outros podem ser feitos e
inclusive é possível ainda tornar tais acordos acopláveis em licenças de
manipulação de conteúdo. Desde que os acordos funcionem para criarem
valor no grupo social, tão melhor. Sugestões de melhoria desse modelo
seriam abrir margem para uma melhor definição de contrapartidas e
estipular um prazo para que o acordo seja cumprido. Sugerimos que ao
menos a simplicidade, a clareza e o tamanho reduzido do acordo sejam
preservados.

O valor social
==============

Como se comportaria um grupo social onde tal prática de acordos se
iniciasse ou fosse já endêmica? Para nos auxiliar nesta e noutras
perguntas, podemos recorrer a um mínimo de sistematização. Considerando
um grupo social de _m_ pessoas, podemos definir a função *valor social*
como sendo

$$S = \displaystyle\sum_{p=1}^{m}\frac{\left(p\ n_p\right)^{v}}{mr}$$

onde $n_p$ é a quantidade de acordos existentes envolvendo $p$
pessoas[4], cada acordo com viralidade[5] $v$ e $r < m$ é o número de
pessoas que *poderiam* [6] ter efetuado acordos mas que ficaram de fora
(isto é, não fizeram acordo nenhum). O valor social assim definido exibe
uma série de propriedades interessantes sob o ponto de vista das
interações sociais, que pode ser revelado pela simples análise das
componentes da somatória.

Primeiramente, esse valor é uma propriedade do sistema social como um
todo e não de um ou outro indivíduo. Em segundo lugar, quanto mais
acordos envolvendo múltiplas partes, maior será o valor social: muitos
acordos entre poucas partes podem ter um peso menor do que poucos
acordos entre múltiplas partes. Um grupo social com muitos acordos de
múltiplas partes possui maior ação coletiva (maior participação
coletiva, maior coletividade) do que uma sociedade com acordos entre
apenas poucas partes.

Já a quantidade $m$ de pessoas do grupo e o total $r$ de pessoas que não
participaram de nenhum tipo de acordo contribuem na diminuição do valor
social: se poucas pessoas (em relação ao total $m$) fazem acordo, temos
uma sociedade com pouca ajuda múltipla e, portanto, para que $S$ atinja
valores significativos, é preciso que $m$ se torne quantitativamente
menor em relação aos valores dos componentes $\left(p\ n_p\right)^{v}$.
O mesmo vale para $r$: os componentes devem ser mais significativos do
que a quantidade de pessoas que poderiam estar em acordos mas que
ficaram de fora, ou seja, $S$ leva em conta a inclusão ou exclusão
social da ação coletiva[7].

Por fim, a viralidade potencializa a multiplicação de acordos: quanto
maior for a viralidade, maior é o valor dos acordos, pois cada acordo é
um acordo de ajuda futura e portanto de investimento na potencialidade
das ações coletivas.

Poderíamos ter definido um valor social de outra forma, mas sabemos que
não há definição de valor que não haja um propósito e muito menos há uma
definição sob a qual todas as outras se reduzem: o valor é uma
propriedade definida pelo grupo social e deve servir a este: devemos
buscar definições e convenções de valor (ou também suas indefinições)
que nos sirvam. Não só acreditamos que esta teoria do valor sirva para
mostrar como a ajuda múltipla implica numa maior ação coletiva como
ainda exibe propriedades interessantíssimas do ponto de vista de
sistemas dinâmicos.

Por simplificação, podemos reescrever a equação anterior como

$$S = k\displaystyle\sum_{p=1}^{m}\left(p\ n_p\right)^{v}$$

onde $k = \frac{1}{mr}$. É claro que o valor de $k$
pode mudar num dado grupo social – por exemplo: mais pessoas ingressando
ou saindo do grupo ou então com um aumento ou diminuição de protagonistas
de acordos – mas podemos considerá-lo como constante num dado momemto, ou seja,
$k = k(t)$ e independente de outras variáveis.

O que realmente nos interessa agora, no entanto, é que chega um momento
em que o grupo social está com tantos acordos que, da forma como
definimos na equação [eq:simples], $S$ começa a crescer absurdamente e
já não passa a representar o valor efetivo de um corpo social onde a
ajuda múltipla se faz presente. Em outras palavras: chega um momento em
que as pessoas já estão tão endividadas de acordos a cumprir que mais
dívidas não afetarão consideravelmente no seu comportamento de ajuda
múltipla. Para refrear o crescimento indiscriminado de $S$,
redefiniremos nossa função como

$$S = k\ ln\displaystyle\sum_{p=1}^{m}\left(p\ n_p\right)^{v}$$

onde _ln_ cumpre um amortecimento no crescimento da somatória, mostrando
que o valor efetivo do grupo cresce logaritmicamente: temos um rápido
crescimento do valor conforme os acordos se iniciam e se multiplicam e,
conforme o endividamento social cresce, a sociedade atinge patamares de
valor altos demais para que um maior acréscimo se torne significativo.

Temos que, pela própria definição, $S$ é uma função de estado, uma vez
que, definido um grupo social e suas interações a partir das variáveis
$n$, $m$, $v$, $r$, etc, temos que $S$ é um indicativo do estado do
sistema – indicando, por exemplo, se ele possui mais ou menos acordos (e
qual a potência e alcance dos acordos) do que outro grupo social
igualmente caracterizado. Além disso, obedece a

$$\frac{dS}{dt} \geq 0$$

Portanto, chamaremos nossa última definição de $S$ (equação [eq:valor])
como *entropia econômica do grupo social*. Tal entropia mede,
inicialmente, *o grau de endividamento do corpo social.* O endividamento
é então a única forma de acúmulo possível: uma vez que alguém ajuda
outrem, não é essa pessoa que detém um crédito: muito pelo contrário, as
pessoas ajudadas contraem uma dívida com todo o corpo social, já que os
acordos estipulam que a pessoa ajudada deve ajudar qualquer outra pessoa
e não necessariamente quem a ajudou.

A entropia tem sido fonte de controversias e mal-entendidos quanto à sua
interpretação. Pela nossa definição, temos que uma entropia maior se
deve exclusivamente a um aumento da complexidade do sistema social,
complexidade que medimos utilizando um conjunto de variáveis que
consideramos como características do sistema[8] que de algum modo
representam o seu estado. Aqui, utilizamos número de acordos, viralidade
dos acordos, etc, o que caracteriza uma abordagem de *granulação
grosseira,* ou seja, de baixa resolução. Um cálculo de valor com maior
resolução deveria levar em consideração, por exemplo, os acordos
separadamente ao invés de agrupá-los por partes envolvidas.

Descontrole social
==================

Esta se torna então uma teoria do descontrole social: o aumento da
entropia é, aqui, não só benéfica como desejável, já que ela indica um
aumento do número de interações. Se nas teorias do controle a entropia
tem um aumento indesejável, aqui se torna o comportamento almejado.

Sendo os acordos diretos, isto é, não mediados, temos ainda mais
descontrole: é importantíssimo que tais acordos não sejam mediados por
bancos de dados. Por banco de dados entendemos qualquer iniciativa de
tentar *efetivamente* calcular $S$ para um dado grupo social (e não o
registro pessoal que cada indivíduo mantiver a respeito dos acordos que
participou). A mera existência de um banco de dados centralizado capaz
de calcular a cada instante o valor social tem os seguintes riscos:

- Dá margens para o estabelecimento de controles sociais com a
  identificação das pessoas mais protagonistas (que participam de mais
  acordos), das pessoas mais prestativas (as que mais ajudam), as que
  mais são ajudadas e as que menos contribuem com ações coletivas,
  possibilitando assim represálias, etc.

- Se, por um lado, o banco de dados “facilita” a busca de pessoas que
  querem ajuda e que podem ajudar, por outro diminuem a necessidade
  das pessoas de travarem contato pessoal para iniciarem seus acordos,
  já que o banco de dados detecta e aproxima as pessoas
  automaticamente.

- Acredita-se que seja de interesse do grupo social que a prática da
  ajuda múltipla faça parte da sua cultura e não uma dependência do
  banco de dados (o que seria um culto ao banco de dados).

É com esse sentido de oposição aos bancos de dados que estabelecemos o
conceito de valor social: não nos interessa calcular efetivamente o
valor de $S$ para um dado grupo social e muito menos caracterizar cada
grupo em função desses parâmetros, o que além de policialesco não
representa o real valor social do grupo (afinal, nem discutimos as
diferenças qualitativas de cada acordo). Queremos, ao contrário, mostrar
*como se comporta* um grupo social adepto de acordos virais de ajuda
múltipla. Podemos resumir isso com a seguinte expressão: *criamos um
cálculo para auxiliar na compreensão o valor social mas jamais queremos
que ele seja usado para quaintificá-lo,* mesmo porque muitos valores
escapam da fórmula que estabelecemos. Não necessitamos de um banco
porque, na ajuda múltipla, o sistema bancário já emerge do próprio
tecido social.

Desdobramentos
==============

Não sabemos os desdobramentos desta teoria do valor e desta prática de
acordos aqui sugeridas. Num primeiro momento, podemos vislumbrar que, no
limite desta teoria, o endividamento excessivo devido a acordos deve
produzir uma prática social indistinguível de uma economia de dádivas
onde não há expectativa de retribuição direta ou o uso da dádiva como
demonstração de poder[9]. No caso da pedagogia também podemos
vislumbrar um ótimo uso da ajuda múltipla: pessoas que aprenderam algo
podem ensinar para outras, multiplicando o conhecimento ao invés de
sempre recorrerem aos luminares do saber.

Por outro lado, a existência e a propagação dos acordos pressupõem um
grupo social pertencente a redes de relacionamentos afins, o que em
certo sentido limita a aplicação da ajuda múltipla: e quem não participa
da rede? E no caso de grupos em conflito interno?

Estas são apenas sugestões de desdobramentos possíveis: convidamos todas
as pessoas que queiram contribuir para a análise de regimes econômicos
fora do mercado para que pensem conjuntamente no que aqui foi meramente
delineado. A experimentação também é encorajada: sem ela, toda esta
discussão não passa de uma teoria descolada dos grupos sociais.

Referências
===========

1. Notar que esta definição de ajuda múltipla não é necessariamente
   equivalente à de ajuda mútua utilizada em muitos estudos sobre
   economia da dádiva: em alguns deles, a ajuda mútua ocorre quando
   cada uma das partes envolvidas no acordo deve se ajudar
   reciprocamente, enquanto que na ajuda múltipla isso não é
   necessário. Não pretendemos neste texto sugerir a suposta
   superioridade do conceito de ajuda múltipla sobre a ajuda mútua.
   Muito pelo contrário: na falta de um devido estudo sobre a
   literatura existente, preferimos utilizar um termo distinto da ajuda
   ou apoio mútuo (mas que eventualmente possa ter o mesmo
   significado).

2. É claro que o valores de $v$ podem ser estipulados em cada acordo.

3. Por *conservar valor* não queremos dizer que a moeda não sofre
   valorização e desvalorização, mas sim que a moeda “congela”
   trabalho.

4. Começamos nossa somatória com $p = 1$ pois, apesar de ser um caso
   em princípio bizarro (uma pessoa fazendo acordo consigo mesmo), não
   deixa de ser uma possibilidade: posso, por exemplo, fazer um acordo
   comigo mesmo e, caso o cumpra, ajudarei mais pessoas, sendo caso
   clássico disso é a solidariedade de ex-viciados, por exemplo. Outro
   argumento para manter $p = 1$ é a simplicidade.

5. Poderíamos, é claro, supor um sistema onde cada acordo tivesse uma
   viralidade $v$ própria, mas a complexidade do cálculo seria
   desnecessária para esta primeira exposição do assunto.

6. Que fique claro: $r$ não inclui pessoas que não podem ajudar, mas
   apenas as que podem mas que ficaram de fora dos acordos.

7. Alternativamente, poderíamos definir o divisor como $m^r$ ao invés
   de $mr$, o que faria com que $S$ fosse muito mais sensível à
   inclusão ou exclusão social. Optamos, no entanto, por uma abordagem
   em que $m$ e $r$ contribuem com igual teor.

8. Num sistema mais próximo da realidade teríamos trocentas outras
   variáveis.

9. O uso da dádiva como demonstração de poder seria, por exemplo uma
   pessoa com mais recursos dar um presente a outra com menos recursos
   de forma que seja causado um vínculo de relação seja paternalista,
   humilhante, etc.
