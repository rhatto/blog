[[!meta title="Economics"]]
[[!tag economics]]

An inquiry on economies and values.

## Subpages

[[!inline pages="page(research/economics*)" archive="yes"]]
