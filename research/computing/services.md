[[!meta title="Free and Open Source Services"]]
[[!meta date="2013-05-20 21:54:48-0300"]]
[[!meta updated="2023-07-18 21:03:14 -0300"]]

An awesome list of FOSS "services".

This is a curated FOSS listing for service hosting. Staying on this list doesn't mean
that a given software is recommended or audited in the spirit of the [Franklin
Street Statement on Freedom and Network
Services](http://wiki.p2pfoundation.net/Franklin_Street_Statement_on_Freedom_and_Network_Services).

What to look for when choosing:

1. Portability: how easy is to export and import data into a service?
2. Maintenance: how easy is to install, upgrade, migrate and restore the service?
3. Security: how secure the service look like? How upstream handles security? What's the service security track record?
4. Stability: does the system is stable or is full of bugs?
5. Maturity: does the architecture and data model of the service changes very often? Are there a community and many developers?

Sometimes is better not run a service than to do it poorly.

## Messaging

Misc:

* [Actor - Open-Source messaging with mobile apps](https://actor.im/).
* [Minds · GitHub](https://github.com/Minds).
* [WhisperSystems/TextSecure-Browser](https://github.com/WhisperSystems/TextSecure-Browser).
* [Chat simples](https://drupal.org/project/chatroom).
* [Let's Chat](https://sdelements.github.io/lets-chat/).
* [Crypptocat](https://crypto.cat/): not recommended due to a problematic architecture, review in the future.
* [Mumble](http://mumble.sourceforge.net).
* Asterisk/SIP: puppet-asterisk: [1](https://github.com/camptocamp/puppet-asterisk) e [2](https://github.com/AhmedElGamil/puppet-asterisk).
* [RTC quick start](http://www.rtcquickstart.org).
* [Secushare](http://secushare.org).
* [Retroshare](http://retroshare.sourceforge.net).
* [simpleWebRTC](http://simplewebrtc.com).
* [Kaiwa](http://getkaiwa.com/).
* [Tox](https://tox.im/).
* [CaliOpen](https://caliopen.org/).
* [ricochet-im - Anonymous peer-to-peer instant messaging](https://github.com/ricochet-im/ricochet).
* [BigBlue Button](http://www.bigbluebutton.org).
* [Rocket.Chat](https://rocket.chat/#!).
* [Mattermost](https://about.mattermost.com/).
* [Riot. Break through](https://riot.im/).
* [Signal Server](https://github.com/whispersystems/signal-server).
* [Wire Server](https://github.com/wireapp/wire-server).
* [Katzenpost](https://katzenpost.mixnetworks.org/).
* [Zulip](https://zulipchat.com/).

Jabber:

* [ejabberd](http://packages.debian.org/stable/ejabberd) ([puppet-ejabberd](https://git.fluxo.info/?p=puppet-ejabberd.git)).
* [prosody](http://packages.debian.org/stable/prosody).
* [puppet-openfire](https://github.com/ghoneycutt/puppet-openfire).
* [jappix](https://jappix.com/).
* [jsxc - Real-time xmpp chat application with video calls, file transfer and encrypted communication](https://github.com/jsxc/jsxc/).

IRC gateway:

* [The Lounge](https://thelounge.chat).
* [qwebirc](http://www.qwebirc.org) ([puppet-qwebirc](https://git.fluxo.info/?p=puppet-qwebirc.git)).
* [iris](http://www.atheme.net/iris.html).
* [cgiirc](http://cgiirc.org).

Bots (IRC, Jabber e StatusNET/Twitter):

* Gateways with proprietary services:
  * Twitter
    * [twitterircbot](https://code.google.com/p/twitterircbot/).
    * [python-twitter-bot](https://code.google.com/p/python-twitter-bot/).
    * [Twitter IRC Bot](http://wiki.projectnerdhaus.com/Twitter_IRC_Bot).
    * [TTYtter](http://www.floodgap.com/software/ttytter).
    * [Rainbowstream](http://www.rainbowstream.org/).
  * WhatsApp:
    * [yowsup](https://github.com/tgalal/yowsup).
* [IRC](http://packages.debian.org/search?keywords=irc+bot&searchon=all&suite=stable&section=all)
  * [supybot](http://packages.debian.org/squeeze/supybot) ([handbook](http://supybook.fealdia.org/), [plugin doc index](http://ubottu.com/stdin/supydocs/plugins.html), [plugins interessantes](http://www.kreativekorp.com/software/supybot/), [default plugins](http://whatnwhat.blogspot.com.br/2011/06/list-of-default-supybot-plugins-along.html)). Ver também [documentação dos plugins](http://supybot.fr.cr/doc/use/plugins/index.html), [documentação do Limnoria](http://supybot.fr.cr/doc/index.html) e [artigo no IRC Wiki](http://www.irc-wiki.org/Supybot).
    * [MeetBot](http://wiki.debian.org/MeetBot) ([manual](http://meetbot.debian.net/Manual.html#administrators), [instalação](https://altinukshini.wordpress.com/2011/09/10/how-to-setup-meetbot/)).
    * [supybot-git](http://mueller.panopticdev.com/2011/08/supybot-git-ircbot-plugin-for-git.html) ([código](https://github.com/mmueller/supybot-git)), dependências: [python-mock](http://packages.debian.org/stable/python-mock) e [python-git](http://packages.debian.org/stable/python-git).
    * [supybot-twitter](https://github.com/rubinlinux/supybot-twitter) (requer [python-twitter](http://packages.debian.org/jessie/python-twitter)).
  * [gozerbot](http://packages.debian.org/squeeze/gozerbot).
  * [rss2irc](http://packages.debian.org/wheezy/rss2irc).
  * [blootbot](http://packages.debian.org/sid/blootbot).
  * [kgb-bot](http://packages.debian.org/squeeze/kgb-bot) / [kgb-client-git](http://packages.debian.org/squeeze/kgb-client-git).
  * [phenny](http://inamidst.com/phenny/).
  * [Sopel](https://sopel.chat/).
  * [Errbot](http://errbot.io/).

## Infrastructure

* [DNSCrypt](https://www.dnscrypt.org/).
* DNS-over-TLS like [hddemux](https://0xacab.org/dkg/hddemux).
* Dynamic DNS:
  * [heartbeat](https://www.mirbsd.org/cvs.cgi/contrib/code/heartbeat/) ([sobre](https://www.mirbsd.org/permalinks/wlog-10_e20130520-tg.htm#e20130520-tg_wlog-10)).
  * With nsupdate: [1](http://linux.yyz.us/nsupdate/), [2](http://linux.yyz.us/dns/ddns-server.html), [3](http://caunter.ca/nsupdate.txt), [4](http://www.rtfm-sarl.ch/articles/using-nsupdate.html), [5](https://github.com/skx/dhcp.io/).
* Keyserver: [puppet-onak](http://git.puppet.immerda.ch/?p=module-onak.git).
* OpenID Provider:
  * [Run your own identity provider](http://wiki.openid.net/w/page/12995226/Run%20your%20own%20identity%20server).
  * [php-openid](https://github.com/openid/php-openid).
  * [simpleid](http://simpleid.koinic.net/).
  * [OpenID URL](https://drupal.org/project/openidurl).
  * [simplesamlphp](http://packages.debian.org/stable/web/simplesamlphp).
* Proxies and load balancing like [Deflect](http://wiki.deflect.ca).
* [Ethereum Frontier](https://www.ethereum.org/).
* Software repositories (apt, f-droid, etc).
* Censorship circunvention:
  * [shadowsocks](https://github.com/shadowsocks/shadowsocks).
  * [lantern](https://github.com/madeye/lantern).
  * [meek](https://trac.torproject.org/projects/tor/wiki/doc/meek).

## Social networking

* [Fediverse](https://fediverse.party/) ecosystem.
* [Agorakit, a groupware for citizens](https://philippejadin.github.io/agorakit/).
* [Crabgrass](https://labs.riseup.net/code/projects/show/crabgrass).
* [Diaspora](https://github.com/diaspora/diaspora).
* [buddycloud](http://buddycloud.com/) ([desenvolvimento](https://buddycloud.org) / [pacote](http://packages.debian.org/sid/buddycloud-server)).
* [BuddyPress](http://wp-brasil.org/buddypress).
* [GNU social and GNU FM](https://gnu.io/) ([public instances](http://www.fediverse.org/)).
* [qvitter · GitHub](https://github.com/hannesmannerheim/qvitter).
* [kune.cc](http://kune.cc/).
* [Tent](http://tent.io).
* [Newebe](http://newebe.org).
* [Shapado](http://shapado.com) ([codigo](https://github.com/ricodigo/shapado), [mirror](https://gitorious.org/shapado)), like [other stackoverflow clones](http://meta.stackoverflow.com/questions/2267/stack-overflow-clones).
* [pump.io](http://pump.io).
* [Friendica](http://friendica.com).
* [StatusNet](http://status.net).
* [Incoma](https://github.com/Incoma/Incoma).
* [Sneer](https://github.com/klauswuestefeld/sneer).
* [Noosfero](http://noosfero.org/).
* [Mastodon](https://mastodon.social) ([public instances](https://instances.mastodon.xyz/)).
* Democracy: [Liquid Democracy](https://wiki.piratenpartei.de/Liquid_Democracy) / [Liquid Feedback](http://liquidfeedback.org).
* [Briar](http://briar.sourceforge.net).
* [Twister](http://twister.net.co/).
* [RedMatrix](https://redmatrix.me/).
* [Hubzilla](http://hubzilla.org/).
* [commento: A lightweight, open source, tracking-free comment engine alternative to Disqus](https://github.com/adtac/commento)
* Calendar:
  * [Demosphere](https://demosphere.net/en/content/download) with [browser extension](https://demosphere.net/en/content/browser-extension).
  * [Gancio](https://gancio.org/): a shared agenda for local communities.

## Timebanking

* [GitHub - wadobo/timebank: Timebank is a web application in which users can exchange services using time as currency.](https://github.com/wadobo/timebank).
* [GitHub - hourbank/timebank](https://github.com/hourbank/timebank).
* [Timebanking Software Platforms - P2P Foundation](https://wiki.p2pfoundation.net/Timebanking_Software_Platforms).

## Mapping

* [Ushahidi](http://www.ushahidi.com).

## Torrent

* [vivatorrent](https://svn.sarava.org/viewvc/vivatorrent/).

## Security and privacy

* Tor (relay, bridge and exit nodes).
* VPN (encrypted proxies and tunnels).
* [ngrok - secure introspectable tunnels to localhost](https://ngrok.com/) ([client](https://packages.debian.org/stable/ngrok-client) / [server](https://packages.debian.org/stable/ngrok-server)).
* [Convergence Notary](https://github.com/moxie0/Convergence/wiki/Running-a-Notary).
* Webproxy for anonymous navigation with [anon-proxy](http://packages.debian.org/stable/anon-proxy).
* [I2P](http://www.i2p2.de/).
* [Freenet](https://mirror4.freenetproject.org).
* [Gnunet](http://packages.debian.org/stable/gnunet).
* [Cryptomail](cryptomail).
* [One-time secret](https://github.com/onetimesecret/onetimesecret).
* [sshuttle](https://github.com/apenwarr/sshuttle).
* [ssh-chat](https://github.com/shazow/ssh-chat).

## Access

* [iodine](https://wiki.koumbit.net/DnsTunnel) ([pacote](http://packages.debian.org/stable/iodine) / [puppet-iodine](https://labs.riseup.net/code/projects/shared-iodine)).
* [icmptx](http://packages.debian.org/stable/icmptx).
* [ptunnel](http://packages.debian.org/stable/ptunnel).
* [PageKite](https://pagekite.net) ([instruções](https://pagekite.net/wiki/Howto/GNULinux/ConfigureYourSystem/) e [pacote](https://packages.debian.org/stable/pagekite)).

## Office

* [Bloom](https://bloom.sh/): [a free and open source Google](https://www.kerkour.fr/blog/bloom-a-free-and-open-source-google/) ([code](https://gitlab.com/bloom42)).
* [Davros: Personal file storage server](https://github.com/mnutt/davros).
* [Wekan — open-source kanban](https://wekan.github.io/).
* [OpenPaaS - An open source Entreprise Social Platform](http://open-paas.org/).
* [ONLYOFFICE](http://onlyoffice.org/).
* [WebODF](http://www.webodf.org/).
* [eXo](http://www.exoplatform.com/).
* [Owncloud](http://owncloud.org).
* [Nextcloud](https://nextcloud.com/).
* [Seafile](https://github.com/haiwen/seafile).
* [Taiga](https://taiga.io).
* [Kolab](http://kolab.org/content/overview).
* [Open webOS](http://openwebosproject.org).
* [Feng Office](http://www.fengoffice.com/).
* [SocialCalc](https://www.socialtext.net/open/socialcalc) ([código](https://github.com/audreyt/socialcalc)).
* [OBM - Open Business Management](http://obm.org).
* [Etherpad](http://etherpad.org/) ([puppet-etherpad](https://git.fluxo.info/?p=puppet-etherpad.git); [vim-etherpad](https://github.com/guyzmo/vim-etherpad) as a proof-of-concept).
* [HedgeDoc - Ideas grow better together](https://hedgedoc.org/): (formerly
  known as CodiMD) is an open-source, web-based, self-hosted, collaborative
  markdown editor. You can use it to easily collaborate on notes, graphs and even
  presentations in real-time. All you need to do is to share your note-link to
  your co-workers and they’re ready to go.
* [Ethercalc](http://www.ethercalc.org) ([código](https://github.com/audreyt/ethercalc)).
* [HedgeDoc](https://docs.hedgedoc.org/): create real-time collaborative markdown notes.
* [HackMD - Collaborative Markdown Knowledge Base](https://hackmd.io/#).
* [LastCalc Is Open Sourced](http://science.slashdot.org/story/12/03/10/186201/lastcalc-is-open-sourced).
* [COMT](http://www.co-ment.org/).
* [UNG Project](http://www.ung-project.org).
* [Libre Docs](http://libredocs.org).
* [Gitlab](http://gitlabhq.com) / [Gitorious](http://getgitorious.com) / [Gogs](http://gogs.io) / [klaus: the first Git web viewer that Just Works™](https://github.com/jonashaag/klaus) / [Gitea](https://gitea.io) / [sr.ht](https://meta.sr.ht/).
* [Flyspray](http://www.flyspray.org/): lightweight, web-based bug tracking system.
* [Teambox](http://teambox.com/) ([código](https://github.com/teambox/teambox)).
* Sobby / Infinote (gobby).
* [Pleft](https://github.com/sander/pleft) / [RdvZ](http://gpl.univ-avignon.fr/rdvz/) / [Nuages](http://nuages.domainepublic.net/nuages/) / [OpenSondage](https://github.com/leblanc-simon/OpenSondage).
* [Open-Xchange](http://news.slashdot.org/story/13/03/20/1433239/open-xchange-launches-open-source-browser-based-office-suite).
* [Apertium](http://www.apertium.org/): open source translation tool.
* [Weblate - web-based translation](https://weblate.org/en/) ([code](https://github.com/WeblateOrg/weblate), [cli](https://packages.debian.org/sid/wlc)).
* [Amara - pculture/unisubs](https://github.com/pculture/unisubs): subtitling platform.
* [Seafile](http://seafile.com).
* [GONG](http://gong.es/).
* [ASCIIFlow Infinity](http://asciiflow.com/) ([code](https://github.com/lewish/asciiflow2).
* [eConvenor | Activists, get organised](https://econvenor.org/).
* [MindMup](https://github.com/mindmup).
* [Cryptpad](https://beta.cryptpad.fr/).
* [GNUKhata - Fast Feature rich Free accounting software](http://gnukhata.in/).
* [draw.io](https://github.com/jgraph/drawio).
* [helpy](https://github.com/helpyio/helpy/): helpdesk customer support application.
* [Zammad](https://zammad.org/): web-based, open source user support/ticketing solution.
* [Discourse](https://www.discourse.org/): "civilized discussion for your community".
* [mat2 web](https://dustri.org/b/mat2-for-the-web.html).

## Finance

* [Timestrap: time tracking and invoicing](https://github.com/overshard/timestrap).
* [ihatemoney - Account manager](https://ihatemoney.org/) ([code](https://github.com/spiral-project/ihatemoney)).
* [pretix](https://pretix.eu/about/en/): ticketing software that cares about your event—all the way.

## Conferences

* [pretalx — CfP and scheduling for conferences](https://pretalx.com): From Call for Papers to schedule – build your conference!
* [frab - conference management system](https://frab.github.io/frab/) with [ANGELSYSTEM - online tool for coordinating helpers and work shifts on large events](https://engelsystem.de/index_en.html).
* [EasyChair Smart CFP](https://easychair.org/cfp/).

## URL shorteners

* [ShURLy](https://drupal.org/project/shurly).
* [nanourl](http://nanourl.sourceforge.net/) ([pacote](http://packages.debian.org/squeeze/nanourl)).
* [naofo.de](https://github.com/pedromoraes/naofo.de).

## Pastebin

* [EZCrypt](https://github.com/novaking/ezcrypt).
* [pastebinit](http://packages.debian.org/lenny/pastebinit).
* [pythin-pastebin](http://packages.debian.org/squeeze/python-pastebin).
* [rebbin](http://rebbin.berlios.de).
* [pastebin](http://github.com/weierophinney/pastebin).
* [rapaste](http://github.com/manveru/rapaste).
* [Perl-6-Pastebin](http://github.com/wolfman2000/Perl-6-Pastebin).
* [patchbin](http://github.com/ninjagod/patchbin).
* [spaste](https://spaste.com/source/).
* [pastebin.ca](http://github.com/slepp/pastebin.ca).
* [rapaste](https://github.com/manveru/rapaste).
* [giturl](https://github.com/whee/giturl).
* [pnopaste](http://sourceforge.net/projects/pnopaste) ([repositório](https://github.com/tpruvot/pnopaste), [pacote](http://packages.debian.org/squeeze/pnopaste)).
* [bpaste](http://sourceforge.net/projects/bpaste).
* [yanopaste](http://sourceforge.net/projects/yanopaste).
* [drupalbin](https://drupal.org/project/drupalbin).
* [zeropaste](https://github.com/edogawaconan/zeropaste).
* [ZeroBin](https://github.com/sebsauvage/ZeroBin).
* [d-note](https://pthree.org/2014/01/13/announcing-d-note-a-self-destructing-notes-application/).
* [PassLok Privacy](http://passlok.weebly.com).
* [0bin - encrypted pastebin](http://0bin.net/).
* [Client-side encrypted image/text/etc host web server](https://github.com/Upload/Up1) ([cli](https://gitlab.com/riseup/up1-cli-client-nodejs)).
* [Lutim - Let's Upload That Image](https://lut.im/) coupled with [Goblim, a mobile app to store and share pictures](http://www.gobl.im/).
* [PrivateBin](https://privatebin.info/) and [PrivateBin Directory](https://github.com/PrivateBin/PrivateBin/wiki/PrivateBin-Directory).

## Downloaders

* [Lufi](https://framagit.org/fiat-tux/hat-softwares/lufi): E2E with one-time download option!
* [Jirafeau](https://gitlab.com/mojo42/Jirafeau).
* [coquelicot](https://coquelicot.potager.org/).
* [filetea](http://packages.debian.org/wheezy/filetea).
* [jyraphe](http://home.gna.org/jyraphe/).
* [fipes](https://github.com/tOkeshu/fipes).
* [noattach](https://github.com/astro/noattach).
* [img.bi](https://img.bi/) ([código](https://github.com/imgbi)).
* [MediaCrush](https://github.com/MediaCrush/MediaCrush).
* [BurnBox](https://github.com/thingssimple/burnbox).
* [Up1](https://github.com/Upload/Up1).
* [FilePizza](https://github.com/kern/filepizza).

## RSS

* [Tiny Tiny RSS](http://tt-rss.org/redmine/projects/tt-rss/wiki).
* [Managin News](http://managingnews.com/).
* [selfoss](http://selfoss.aditu.de/).
* [NewsBlur](https://github.com/samuelclay/NewsBlur).

## Media managers

* [ResourceSpace](http://www.resourcespace.com/).
* [MediaGoblin](http://mediagoblin.org).
* [Zend.To](http://zend.to).
* [Pix](http://pix.toile-libre.org).
* [SparkleShare](http://sparkleshare.org).
* [StackSync](http://stacksync.org).
* [Baobáxia](https://github.com/RedeMocambos/baobaxia).
* [Syncany](https://www.syncany.org/).
* [PeerTube](https://github.com/Chocobozzz/PeerTube).
* [DTube](https://github.com/dtube).

## Asset manages

* [Snipe-IT - Free open source IT asset management](https://snipeitapp.com) ([repository](https://github.com/snipe/snipe-it)).

## Image galeries

Dynamic:

* [Grid, Guardian’s image management service](https://www.theguardian.com/info/developer-blog/2015/aug/12/open-sourcing-grid-image-service).
* [Comparison](https://en.wikipedia.org/wiki/Comparison_of_photo_gallery_software)
* [Gallery](http://galleryproject.org) (hibernating).
* [Piwigo](http://piwigo.org).
* [Coppermine](http://coppermine-gallery.net).
* [Zenphoto](http://www.zenphoto.org).
* [Plogger](http://www.plogger.org).
* [thumbor](https://www.thumbor.org/): open-source smart on-demand image cropping, resizing and filters.

Static:

* [fgallery: a modern, minimalist javascript photo gallery](http://www.thregr.org/~wavexx/software/fgallery/) ([pacote](https://packages.debian.org/sid/fgallery)).
* [Sigal - Simple Static Gallery Generator — Sigal 1.1.0-dev documentation](http://sigal.saimon.org/en/latest/).
* [Static image gallery generator comparison (FOSS)](http://www.nico.schottelius.org/docs/static-image-gallery-generator-comparison/).
* [images - How to generate web picture gallery offline? (no php on server) - Super User](https://superuser.com/questions/190134/how-to-generate-web-picture-gallery-offline-no-php-on-server/309079#309079).

## Other

* [Blockstack, building the decentralized internet](https://blockstack.org/).
* [MonitoringScape](https://bigpanda.io/monitoringscape/): guide to the new, exciting world of modern monitoring.
* [Derefer Script PHP](http://www.naden.de/blog/derefer-script).
* [idno/Known · GitHub](https://github.com/idno/Known).
* [perma](https://github.com/harvard-lil/perma).
* [Koken](http://koken.me).
* [hotglue](http://hotglue.me) ([puppet-hotglue](https://git.fluxo.info/?p=puppet-hotglue.git)).
* [git-annex assistant](http://git-annex.branchable.com/assistant/).
* [Gopher](http://packages.debian.org/stable/pygopherd).
* [Fidonet](https://en.wikipedia.org/wiki/Fidonet).
* [Usenet](https://en.wikipedia.org/wiki/Usenet) ([Howto](http://tldp.org/HOWTO/Usenet-News-HOWTO/index.html), [papercut](http://packages.debian.org/stable/papercut)).
* [Namecoin](http://dot-bit.org) and other [DNS's alternatives](https://en.wikipedia.org/wiki/Alternative_DNS_root).
* [dename](https://github.com/andres-erbsen/dename).
* [BOINC](http://packages.debian.org/stable/boinc-client).
* [Aegir](http://www.aegirproject.org).
* [Huginn](http://www.wired.com/wiredenterprise/2013/03/hugin/) ([código](https://github.com/cantino/huginn)).
* [OpenPhoto](http://theopenphotoproject.org/).
* [Photographer.io](https://github.com/afternoonrobot/photographer-io).
* [Weblate](http://weblate.org/en/).
* [YaCy](http://yacy.net/).
* [searchx](https://github.com/asciimoo/searx) ([firefox search addon](https://addons.mozilla.org/en-US/firefox/addon/searx-0-9-0/)).
* [Bitcoin](http://packages.debian.org/sid/bitcoind).
* [Tahoe-LAFS](https://we.riseup.net/debian/tahoe).
* [Dark Market](https://github.com/darkwallet/darkmarket).
* [Mozilla Sync Server](https://docs.services.mozilla.com/howtos/run-sync-1.5.html).
* [AMBER](http://amberlink.org/).
* [Transparency Toolkit | Watching the watchers](https://transparencytoolkit.org).
* [TANIA - A free and open source farm management system for everyone.](http://gettania.org/).
* [Webrecorder pywb - Web Archiving Tools for All](https://github.com/webrecorder/pywb/) ([docs](https://pywb.readthedocs.io/en/latest/index.html)).
* [Lobsters - Computing-focused community centered around link aggregation and discussion](https://github.com/lobsters/lobsters).

## Streaming

* [Rhinobird.tv](https://github.com/rhinobird).
* [Libresonic: Media streaming software](https://github.com/Libresonic/libresonic).
* [Airsonic](https://airsonic.github.io/).
* [Icecast](http://icecast.org/).
* [CherryMusic](https://github.com/devsnd/cherrymusic).
* [Koel](https://github.com/phanan/koel).
* [mStream](https://github.com/IrosTheBeggar/mStream).

## Mobilization

* [Zylum](https://github.com/peacenews/ecosystem).
* Crowdfunding: [Catarse](https://github.com/danielweinmann/catarse.git).
* Mobilization, campaigns and petitions tool.
* Anonymous publishing platform like ([GlobaLeaks](http://globaleaks.org), [DeaDrop](http://deaddrop.github.io/) or similar).
* Poll system similar to [lembrador](https://lembrador.sarava.org): [dudle](http://dudle.mister-muffin.de/about.cgi), [croodle](https://github.com/jelhan/croodle).
* Scheduler: [Framadate](http://framadate.org/) and [Makemeeting](https://drupal.org/project/makemeeting).
* [LimeSurvey](http://www.limesurvey.org).
* [Loomio](https://github.com/loomio/loomio).

## Decision-making

* [Decidim](https://decidim.org/): digital platform for citizen participation.
  Free/libre, open and safe technology. With all democratic guarantees.
  Reprogramming democracy is now possible with Decidim.
* [Helios Voting / Helios Election System](https://vote.heliosvoting.org/): "verifiable online
  elections" ([code](https://github.com/benadida/helios-server)).

## Email

* Virtual accounts:
  * [Postfix Admin](http://sourceforge.net/projects/postfixadmin).
  * [Ratuus](http://www.ratuus.org).
  * [VBoxAdm](http://www.vboxadm.net).
  * [Modoboa](http://modoboa.org/en).
  * [ViMbAdmin](https://github.com/opensolutions/ViMbAdmin).
* Web clients:
  * [RainLoop](http://www.rainloop.net/).
  * [Roundcube](http://www.roundcube.net).
  * [Mailpile](http://www.mailpile.is/).
  * [Petmail](https://github.com/warner/petmail).
  * [Pixelated](https://pixelated-project.org).
  * [Whiteout](https://github.com/whiteout-io/mail-html5).
  * [N1](https://github.com/nylas/N1).
* Migration:
  * [Gmvault](http://gmvault.org).

## Bookmarks

* [SemanticScuttle](http://semanticscuttle.sourceforge.net/).
* [QStode](https://github.com/piger/qstode).
* [Bookie](https://github.com/bookieio/Bookie).
* [wallabag: a self hostable application for saving web pages](https://wallabag.org/en) with [wallabag-cli](https://github.com/Nepochal/wallabag-cli).
* [prismo](https://gitlab.com/mbajur/prismo).
* [linkding: Self-hosted bookmark service](https://github.com/sissbruecker/linkding).
* [Lemmy - A link aggregator for the fediverse](https://join-lemmy.org/):
  selfhosted social link aggregation and discussion platform. It is completely
  free and open, and not controlled by any company. This means that there is no
  advertising, tracking, or secret algorithms. Content is organized into
  communities, so it is easy to subscribe to topics that you are interested in,
  and ignore others. Voting is used to bring the most interesting items to the
  top.

## Libraries

* [Evergreen ILS | Evergreen – Open Source Library Software](http://evergreen-ils.org/).
* [Koha - Open Source ILS - Integrated Library System](http://www.koha.org/).
* [Library Simplified](http://www.librarysimplified.org/).

## Turn-key appliances

* [Sandstorm](https://sandstorm.io/).
* [FreedomBox](https://freedomboxfoundation.org).
* [Mail-in-a-Box - Firefox (Private Browsing)](https://mailinabox.email/).
* [LEAP](https://leap.se).
* [Enigmabox](http://wiki.enigmabox.net).
* [Streisand](https://github.com/jlund/streisand).

## P2P / Serverless

* [Bitmessage](https://bitmessage.org) (apparently with [problems](http://www.chronicles.no/2013/08/bitmessage-crackdown.html)).
* [Torchat](https://code.google.com/p/torchat/).
* [Cryptosphere](http://cryptosphere.org/).
* [WebTorrent - Streaming browser torrent client](https://webtorrent.io/).

## CMS

* [Droplets](http://dropplets.com/).
* [StrongLink](https://github.com/btrask/stronglink).

## Geo

* [QGIS](http://qgis.org/en/site/).
* [GeoServer](http://geoserver.org/).
* [GeoNode](https://geonode.org/).
* [Mapeo](https://www.digital-democracy.org/mapeo/) ([docs](https://docs.mapeo.app/)).

## Radio

* [OpenWebRX](https://sdr.hu/openwebrx) ([código](https://github.com/simonyiszk/openwebrx)).

## Education

* [Openki](https://gitlab.com/Openki/Openki/): Course-Organization-Platform: A
  tool to build up and organize local communities – Open education for real.
* [Kiwix lets you access free knowledge – even offline](https://www.kiwix.org/) with
  [kiwix-serve](https://www.kiwix.org/en/downloads/kiwix-serve/).

## References

* [Servers - Platforms - PRISM Break](https://prism-break.org/en/categories/servers/).
* [Indie Web Projects](http://indiewebcamp.com/projects).
* [Alternative Internet](https://github.com/rossjones/alternative-internet).
* [Overview of projects working on next-generation secure email](https://github.com/OpenTechFund/secure-email).
* [alternativeTo](https://alternativeto.net).
* [Social Media Alternatives Project (S-MAP)](http://www.socialmediaalternatives.org/).
* [Awesome-Selfhosted](https://github.com/Kickball/awesome-selfhosted).
