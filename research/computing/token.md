[[!meta title="Tokens USB"]]

Sobre tokens criptográficos USB.

## Intro

* [ImperialViolet - Security Keys](https://www.imperialviolet.org/2017/08/13/securitykeys.html).

## FST-01: Gnuk and Neug

### FST-01

* [FST-01](https://www.gniibe.org/FST-01/fst-01.html "FST-01") (Gniibe)
* [FST-01](https://seeeddoc.github.io/FST-01/ "FST-01") (Seedstudio)
* [unixjazz / DIYNuk · GitLab](https://gitlab.com/unixjazz/DIYNuk "unixjazz / DIYNuk · GitLab")
* [» Reading and Writing Firmware on an STM32 using SWD](https://cybergibbons.com/hardware-hacking/reading-and-writing-firmware-on-an-stm32-using-swd/ "» Reading and Writing Firmware on an STM32 using SWD")
  * [Serial Wire Debug (SWD) - Silicon Labs](https://community.silabs.com/s/article/serial-wire-debug-swd-x?language=en_US "Serial Wire Debug (SWD) - Silicon Labs")
  * [ST-LINK/V2 - ST-LINK/V2 in-circuit debugger/programmer for STM8 and STM32 - STMicroelectronics](https://www.st.com/en/development-tools/st-link-v2.html "ST-LINK/V2 - ST-LINK/V2 in-circuit debugger/programmer for STM8 and STM32 - STMicroelectronics")
  * [STM8S-DISCOVERY - Discovery kit with STM8S105C6 MCU - STMicroelectronics](https://www.st.com/en/evaluation-tools/stm8s-discovery.html "STM8S-DISCOVERY - Discovery kit with STM8S105C6 MCU - STMicroelectronics")
  * [FST-01 gnuk firmware update via USB - Raymii.org](https://raymii.org/s/tutorials/FST-01_firmware_upgrade_via_usb.html "FST-01 gnuk firmware update via USB - Raymii.org")
  * [FST-01 - Seeed Wiki](http://wiki.seeed.cc/FST-01/).
  * [Questions - FST-01 Q&A Forum](http://no-passwd.net/askbot/questions/).
  * [Programming the FST-01 (gnuk) with a Bus Pirate + OpenOCD](https://www.earth.li/~noodles/blog/2015/08/program-fst01-with-buspirate.html).

### Gnuk

* [Free Software Initiative of Japan - gnuk](http://www.fsij.org/category/gnuk.html)
* [Gnuk Documentation — Gnuk Documentation 1.0 documentation](http://www.fsij.org/doc-gnuk/)
* [Gnuk - Noisebridge](https://noisebridge.net/wiki/Gnuk).
* [Gnuk Token and GnuPG scdaemon](https://fosdem.org/2018/schedule/event/hwenablement_gnuk_token_and_gnupg_scdaemon/).
* [Gnuk source code](https://salsa.debian.org/gnuk-team/gnuk/gnuk).

### Neug

* [NeuG, a True Random Number Generator Implementation](https://www.gniibe.org/memo/development/gnuk/rng/neug.html "NeuG, a True Random Number Generator Implementation")
* [Gnuk / gnuk / neug · GitLab](https://salsa.debian.org/gnuk-team/gnuk/neug "Gnuk / gnuk / neug · GitLab")
  * [NeuG USB True Random Number Generator | Hacker News](https://news.ycombinator.com/item?id=16080019 "NeuG USB True Random Number Generator | Hacker News")
* [Gnuk, NeuG, FST-01](https://incenp.org/dvlpt/docs/fsij-gnuk-neug/index.html "Gnuk, NeuG, FST-01")
  * [How can I install Gnuk on FST-01 with NeuG 1.0.5?](http://www.gniibe.org/FST-01/q_and_a/gnuk_install_over_neug.html "How can I install Gnuk on FST-01 with NeuG 1.0.5?")
  * [udev-rules for my FST-01 gnuk security token](https://lists.gnu.org/archive/html/help-guix/2018-07/msg00051.html "udev-rules for my FST-01 gnuk security token")
    * [Device Configuration for Gnuk Token with libusb — Gnuk Documentation 1.0 documentation](http://www.fsij.org/doc-gnuk/udev-rules.html "Device Configuration for Gnuk Token with libusb — Gnuk Documentation 1.0 documentation")
  * [How to install or update NeuG firmware with STLink/v2 debugger on FST-01](https://demsh.org/post/neug-memo/ "How to install or update NeuG firmware with STLink/v2 debugger on FST-01")
  * [How can I use NeuG standalone device on my Debian box?](https://www.gniibe.org/FST-01/q_and_a/neug-standalone-device.html "How can I use NeuG standalone device on my Debian box?")
  * [linux - Using the NeuG TRNG with /dev/random? - Unix & Linux Stack Exchange](https://unix.stackexchange.com/questions/354188/using-the-neug-trng-with-dev-random#433397 "linux - Using the NeuG TRNG with /dev/random? - Unix & Linux Stack Exchange")

### Threat modeling

* [How safe is Gnuk against side channel attacks, USB sniffer, or electron/tunneling microscope? - FST-01 Q&A Forum](http://no-passwd.net/askbot/question/33/how-safe-is-gnuk-against-side-channel-attacks-usb/).
* [How does Gnuk protect against attacks to extract private keys? - FST-01 Q&A Forum](http://no-passwd.net/askbot/question/32/how-does-gnuk-protect-against-attacks-to-extract/).
* [What types of risk are more likely? What's "best practice" against that? - FST-01 Q&A Forum](http://no-passwd.net/askbot/question/68/what-types-of-risk-are-more-likely-whats-best/).

### Programando o FST-01

Agradecimento ao `unixjazz` do projeto [DIYNuk](https://gitlab.com/unixjazz/DIYNuk) por fornecer estas instruções!

Roteiro baseado no programador (STLink) ligado a um STM8 com os pinos soldados:

1. Ligar ambas as pacas conforme [este diagrama dos
   pinos](https://www.gniibe.org/memo/development/gnuk/hardware/stlinkv2-stm8s-discovery.html).
2. Configurar o ST-Link (programador) no PC. Instruções
   [aqui](https://www.gniibe.org/FST-01/q_and_a/swd-debugger.html).
3. Compilar (mesmo procedimento para Gnuk e Neug (se diz Noisy com sotaque
   japones)) conforme [estas instruções](https://www.gniibe.org/memo/development/gnuk/gnuk-building-for-stm32-part-of-stm8s-discovery-kit.html).

Em geral, o procedimento e' o seguinte:

1. Instalar o [ultimo NeuG do repo do Debian](https://salsa.debian.org/gnuk-team/gnuk/neug).
2. Instalar o GNU Toolchan for ARM (4.5 ou maior).
3. Instalar o OpenOCD (pacote do Debian).
4. Compilar o NeuG.
5. Configurar o ST-Link com as seguintes regras do udev (por exemplo em `/etc/udev/rules.d/10-stlink.rules`):

        ACTION=="add", SUBSYSTEM=="usb", ATTR{idVendor}=="0483", \
        ATTR{idProduct}=="3748", GROUP="tape", MODE="664", SYMLINK+="stlink"

6. Plugar o ST-Link, rodar o OpenOCD e escrever o binario na flash do STM8
   conforme [este procedimento](https://www.gniibe.org/memo/development/gnuk/gnuk-installation-to-stm32-part-of-stm8s-discovery-kit.html)
7. Pronto! Agora basta [ler a serial](https://www.gniibe.org/FST-01/q_and_a/neug-standalone-device.html).
