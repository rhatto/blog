[[!meta title="Libreboot"]]

Also check the [additional references](https://links.fluxo.info/tags/libreboot).

## Important note

From [Libreboot – Installation instructions](https://libreboot.org/docs/install/index.html):

> NOTE: if running flashrom -p internal for software based flashing, and you get
> an error related to /dev/mem access, you should reboot with iomem=relaxed
> kernel parameter before running flashrom, or use a kernel that has
> CONFIG_STRICT_DEVMEM not enabled.

Or you might get errors like this:

    user@x60s:~/code/libreboot/libreboot_r20160907_util/flashrom/i686$ sudo ./flashrom_lenovobios_sst -p internal -r factory.bin
    flashrom v0.9.9-unknown on Linux 4.9.0-2-686-pae (i686)
    flashrom is free software, get the source code at https://flashrom.org

    Calibrating delay loop... OK.
    Found chipset "Intel ICH7M".
    Enabling flash write... Error accessing ICH RCRB, 0x4000 bytes at 0xfed1c000
    /dev/mem mmap failed: Operation not permitted
    FAILED!
    FATAL ERROR!
    Error: Programmer initialization failed.
    user@x60s:~/code/libreboot/libreboot_r20160907_util/flashrom/i686$ sudo ./flashrom_lenovobios_macronix -p internal -r factory.bin

## Pre-compiled binaries

[Get the latest stable release](https://libreboot.org/download.html), check integrity, etc.

In my case I only needed these, which were the latest releases at the time of writing:

* https://mirrors.peers.community/mirrors/libreboot/stable/20160907/SHA512SUMS
* https://mirrors.peers.community/mirrors/libreboot/stable/20160907/SHA512SUMS.sig
* https://mirrors.peers.community/mirrors/libreboot/stable/20160907/libreboot_r20160907_util.tar.xz
* https://mirrors.peers.community/mirrors/libreboot/stable/20160907/rom/grub/libreboot_r20160907_grub_x60.tar.xz

Download and unpack bot libreboot util and rom tarballs. In this example I used the following paths:

* `~/code/libreboot/libreboot_r20160907_util`
* `~/code/libreboot/roms/libreboot_r20160907_grub_x60`

## Building from source

Just if you need to:

    git clone https://notabug.org/libreboot/libreboot.git && cd libreboot
    sudo ./oldbuild dependencies trisquel7
    ./download all
    ./oldbuild module all

## x60s

This is how I've done. The actual procedure might change without notice :P

### Overview

* Backup the original firmware [like said](https://www.coreboot.org/Board:lenovo/x60/Installation#Back_up_the_original_proprietary_firmware).
* Then flash the new ROM as [said here](https://libreboot.org/docs/install/#flashrom_lenovobios).

Note this [funny note](https://en.wikibooks.org/wiki/Libreboot/Install/ThinkPad_X60_and_T60)
on customized and solitary proprietary software:

> While backing up the proprietary BIOS image might be construed as "endorsing"
> proprietary software: This BIOS image is unique to every motherboard. It will
> be impossible to restore the original BIOS once it is lost. Back it up now or
> you will lose it forever. Do not take this decision lightly.

### BIOS Backup

    user@x60s:~/code/libreboot/libreboot_r20160907_util/flashrom/i686$ sudo ./flashrom_lenovobios_sst -p internal -r factory.bin
    user@x60s:~/code/libreboot/libreboot_r20160907_util/flashrom/i686$ sudo ./flashrom_lenovobios_macronix -p internal -r factory.bin

### Flashing

    user@x60s:~/code/libreboot/libreboot_r20160907_util$ sudo ./flash i945lenovo\_firstflash ../roms/libreboot_r20160907_grub_x60/x60_usqwerty_vesafb.rom
    Mode selected: i945lenovo_firstflash
    bucts utility version 'withoutgit'
    Using LPC bridge 8086:27b9 at 0000:1f.00
    Current BUC.TS=0 - 128kb address range 0xFFFE0000-0xFFFFFFFF is untranslated
    Updated BUC.TS=1 - 64kb address ranges at 0xFFFE0000 and 0xFFFF0000 are swapped
    flashrom v0.9.9-unknown on Linux 4.9.0-2-686-pae (i686)
    flashrom is free software, get the source code at https://flashrom.org

    Calibrating delay loop... OK.
    Found chipset "Intel ICH7M".
    Enabling flash write... WARNING: SPI Configuration Lockdown activated.
    OK.
    Found SST flash chip "SST25VF016B" (2048 kB, SPI) mapped at physical address 0xffe00000.
    Reading old flash chip contents... done.
    Erasing and writing flash chip... spi_block_erase_20 failed during command execution at address 0x0
    Reading current flash chip contents... done. Looking for another erase function.
    spi_block_erase_52 failed during command execution at address 0x0
    Reading current flash chip contents... done. Looking for another erase function.
    Transaction error!
    spi_block_erase_d8 failed during command execution at address 0x1f0000
    Reading current flash chip contents... done. Looking for another erase function.
    spi_chip_erase_60 failed during command execution
    Reading current flash chip contents... done. Looking for another erase function.
    spi_chip_erase_c7 failed during command execution
    Looking for another erase function.
    No usable erase functions left.
    FAILED!
    Uh oh. Erase/write failed. Checking if anything has changed.
    Reading current flash chip contents... done.
    Apparently at least some data has changed.
    Your flash chip is in an unknown state.
    Get help on IRC at chat.freenode.net (channel #flashrom) or
    mail flashrom@flashrom.org with the subject "FAILED: <your board name>"!
    -------------------------------------------------------------------------------
    DO NOT REBOOT OR POWEROFF!
    flashrom v0.9.9-unknown on Linux 4.9.0-2-686-pae (i686)
    flashrom is free software, get the source code at https://flashrom.org

    Calibrating delay loop... OK.
    Found chipset "Intel ICH7M".
    Enabling flash write... WARNING: SPI Configuration Lockdown activated.
    OK.
    No EEPROM/flash device found.
    Note: flashrom can never write if the flash chip isn't found automatically.
    user@x60s:~/code/libreboot/libreboot_r20160907_util$

Then reboot the machine, passing "iomem=relaxed" into the kernel command line. Then run the
second flash:

    user@x60s:~/code/libreboot/libreboot_r20160907_util$ sudo ./flash i945lenovo\_secondflash ../roms/libreboot_r20160907_grub_x60/x60_usqwerty_vesafb.rom
    Mode selected: i945lenovo_secondflash
    flashrom v0.9.9-unknown on Linux 4.9.0-2-686-pae (i686)
    flashrom is free software, get the source code at https://flashrom.org

    Calibrating delay loop... OK.
    coreboot table found at 0x7be9f000.
    Found chipset "Intel ICH7M".
    Enabling flash write... OK.
    Found SST flash chip "SST25VF016B" (2048 kB, SPI) mapped at physical address 0xffe00000.
    Reading old flash chip contents... done.
    Erasing and writing flash chip... Erase/write done.
    Verifying flash... VERIFIED.
    bucts utility version 'withoutgit'
    Using LPC bridge 8086:27b9 at 0000:1f.00
    Current BUC.TS=1 - 64kb address ranges at 0xFFFE0000 and 0xFFFF0000 are swapped
    Updated BUC.TS=0 - 128kb address range 0xFFFE0000-0xFFFFFFFF is untranslated
    user@x60s:~/code/libreboot/libreboot_r20160907_util$

## Issues

* [#870006 - xserver-xorg: X60 libreboot Xorg hangs reproducibly after upgrade to stretch - Debian Bug report logs](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=870006).
