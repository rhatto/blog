[[!meta title="The suckless agenda"]]

    Laws of Computer Programming:
            1. Any given program, when running, is obsolete.
            2. Any given program costs more and takes longer.
            3. If a program is useful, it will have to be changed.
            4. If a program is useless, it will have to be documented.
            5. Any given program will expand to fill all available memory.
            6. The value of a program is proportional the weight of its output.
            7. Program complexity grows until it exceeds the capability of
               the programmer who must maintain it.

    -- fortune(6)

# Subpages

[[!inline pages="page(suckless*)" archive="yes"]]

# Guiding principles

Suckless: future-proof, present-friendly.

* [The Critical Engineering Manifesto](https://criticalengineering.org).
* [Software rot - Wikipedia, the free encyclopedia](https://en.wikipedia.org/wiki/Software_rot).
* [The Mutt E-Mail Client](http://www.mutt.org/): "All mail clients suck. This one just sucks less".
* [The Future Programming Manifesto](http://alarmingdevelopment.org/?p=893).
* [Unix philosophy - Wikipedia, the free encyclopedia](https://en.wikipedia.org/wiki/Unix_philosophy).
* [KISS principle - Wikipedia, the free encyclopedia](https://en.wikipedia.org/wiki/KISS_principle).
* [suckless.org software that sucks less](http://suckless.org/).
* [principles - IndieWebCamp](http://indiewebcamp.com/principles).
* [Reduce, reuse, recycle](https://en.wikipedia.org/wiki/Waste_hierarchy):
    * Reduce: favor the simplest solutions and focus attention on specific problems;
    * Reuse: work from experience and favor examples of current practice;
    * Recycle: encourage modularity and the ability to embed. [reference](https://en.wikipedia.org/wiki/Microformat#Design_principles).

## Contributions to the agenda

    Though a program be but three lines long, someday it will have to
    be maintained.

    -- The Tao of Programming

* [Security Specs](https://manual.fluxo.info/specs).
* [Static site generation](/static).
* [Metadot](https://metadot.fluxo.info) to manage dotfiles along with [a locally-installable applications repository](https://inception.fluxo.info).

## Stuff currently being observed

* [Neovim](http://neovim.io).

## I should not install a software if

I can solve my problem using:

* A terminal and the related userland.
* A text editor like `vim`.
* A version control system like `git`.
* A clean data format like markdown, yaml, dot or CSV.
