[[!meta title="Git"]]

Git research and development.

Barebones shared git repositories
---------------------------------

No gitolite, gitosis, gitlab or whatever involded. Only OpenSSH and git is needed.

### Basic config

If needed, create a host instance for your git server at your `~/.ssh/config`:

    Host git.project.org gitserver
      HostName git.project.org

Now make sure you can log into the server using key-based auth.

### Server config

    sudo apt install git
    sudo adduser git --home /var/git
    sudo mkdir              /var/git/repositories
    sudo chown git.         /var/git/repositories
    sudo chmod 775          /var/git/repositories
    sudo usermod -a -G git `whoami` # add yourself into the git group

### Creating a repository

At your computer:

    repo="name-your-project-here"
    mkdir $repo
    cd $repo
    git init
    git remote add origin ssh://gitserver/var/git/repositories/$repo.git

Then do your regular stuff: create files, commit stuff, etc:

    touch test
    git add .
    git commit -m "Initial import"

### Copy a bare git repo to the server

    cd ..
    git clone --bare $repo $repo.git
    scp -r $repo.git gitserver:/var/git/repositories/$repo.git

### Making the repository shareable

In the server:

    sudo chgrp -R git /var/git/repositories/$repo.git
    sudo chmod 775    /var/git/repositories/$repo.git

    find /var/git/repositories/$repo.git/ -type f -exec sudo chmod 664 {} \;
    find /var/git/repositories/$repo.git/ -type d -exec sudo chmod 775 {} \;

Now make sure that the repository configuration has the following option at the `core` section:

    sharedRepository = group

You can edit `/var/git/repositories/$repo.git/config` to add this config or just run
the following commands:

    git -C /var/git/repositories/$repo.git config core.sharedRepository group

### Daily workflow

From now on, you can work at your computer's local `$repo` as usual:

    cd $repo
    git pull
    touch another-test
    git add .
    git commit -m "Adds another-test"
    git push # this sends changes back to your git server

### Adding more users into the game

You can add existing users to edit the repository given that:

* They have accounts in the system.
* They are added into the `git` group.

If they also use key-based auth they can seamlessly contribute to your repository
as if you were using a more complex repository manager like gitolite or a service
like gitlab.

You can even try to implement some more complex access control by using different
groups for each project so you're not bound to the `git` group.

### References

- [How to make bare git repository group-writable after pushing?](http://stackoverflow.com/questions/15881104/how-to-make-bare-git-repository-group-writable-after-pushing).
- [Create Git bare / shared remote repository](http://pietervogelaar.nl/create-git-bare-shared-remote-repository/).
- [How to clone and share a Git repository over SSH](http://linuxaria.com/pills/how-to-clone-and-share-a-git-repository-over-ssh?lang=en)
- [Git - Getting Git on a Server](https://git-scm.com/book/en/v2/Git-on-the-Server-Getting-Git-on-a-Server).
- [Git - Setting Up the Server](https://git-scm.com/book/en/v2/Git-on-the-Server-Setting-Up-the-Server).
- The [utils-git](https://git.fluxo.info/utils-git/about/) repository for useful scripts and plugins.
