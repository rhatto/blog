[[!meta title="ThinkPad"]]
[[!meta date="2021-01-17 15:26:04 -0300"]]

Esta é uma pesquisa em andamento sobre [ThinkPads](https://en.wikipedia.org/wiki/ThinkPad).

## Índice

[[!toc startlevel=2 levels=4]]

## Política

É sempre bom lembrarmos das origens das linhagens técnicas. Como o Fusca, que
foi [concebido pelo Porsche para ser um carro resistente e de fácil
manutenção](https://hackaday.com/2016/05/03/volkswagen-beetle-the-most-hackable-car/).

Aqui segue um brainstorm (ou thinkstorm?) que pode ser aprimorado com mais
propriedade.

### Think

[De onde vem o slogan THINK](/books/history/ibm-holocaust):

    NCR salesmen wore dark suits, the corporation innovated a One Hun-
    dred Point Club for agents who met their quota, and The Cash stressed "clean
    living" as a virtue for commercial success. One day during a pep rally to the
    troops, Watson scrawled the word THINK on a piece of paper. Patterson saw
    the note and ordered THINK signs distributed throughout the company.
    Watson embraced many of Patterson's regimenting techniques as indispens-
    able doctrine for good sales. What he learned at NCR would stay with him
    forever.

    -- do livro IBM and The Holocaust, pág. 42

    Large pictures of Watson in the weekly company publication, Business
    Machines, regularly sported headlines proclaiming even his ordinary accom-
    plishments, such as "Thomas J. Watson Opens New Orleans Office." The ever-
    present equating of his name with the word THINK was more than an
    Orwellian exercise, it was a true-life indoctrination. The Watson mystique
    was never confined to the four walls of IBM. His aura was only magnified by
    his autocratic style

    -- do livro IBM and The Holocaust, pág. 47

Esse aí é o DNA da IBM. Não é de se estranhar que tenha sido tão fácil para a
Apple capitalizar em cima dos hobbystas da computação pessoal e se colocar como
uma alternativa humanizadora ao "Grande Irmão" de gigantes da computação
monolítica como a IBM.

Não há como operacionalizar um império se nao houver uma classe mínima de escribas,
sejam corporativos ou estatais e que sejam encorajados a pensar dentro dos limites
e ditames do regime.

Mais um ponto pra tese de que nazismo não foi só baseado no arcaísmo da "terra e
sangue" mas também numa racionalidade técnica. Havia pensamento, inteligência e
estratégia, mas para todos os propósitos irracionais, boçais, torpes e banais,
mostrando que mesmo na sapiência pode haver a demência.

### A Classe dos Escribas

O ThinkPad é um equipamento de produtividade feito para ajudar a pensar
estrategicamente. Talvez seja o melhor computador pra isso pelo seu design
potencializador da simbiose humano-máquina.

É um ápice dos PCs conhecidos como laptops ou notebooks, funcionando muito bem
para o processamento da linguagem escrita. Daí que é muito confortável para o
público letrado que produz código.

### A nova ideologia

> O lance é...
> Na nova ideologia, o slogan mudou
> Nao eh mais **THINK**
> Mas sim **INTERACT**
> Daí que as recentes linhagens técnicas tem outro design
> Pra facilitar uma interação instantânea, não para pensar

Não que o slogan anterior tenha sido abolido, mas ele é minoritário numa indústria
da computação onde a maior parte dos consumidores não está sendo tratada como
classe pensadora, mas como meros terminais de interação entre o mundo e as
"nuvens" computacionais (o antigo "mainframe" do paradigma totalitário
clássico).

O Pad está para o THINK assim como o Pod está para o INTERACT.

Não é à toa que a IBM é uma empresa do século passado apesar de ainda ter uma
certa preponderância em IA e Big Data. Mas sua identidade de uma empresa de
pesquisadores ligados às ciências duras tem sido suplantada pouco a pouco pela
lógica das neo doctons como Google e Apple.

Não é à toa que a IBM vendeu a linha ThinkPad para a chinesa Lenovo, que
prontamente alterou diversos aspectos desses PCs para que sejam mais
interativos.

Como será que a atual classe dos escribas se portará? Permanecerá como estamento
THINK ou será absorvido pela massa INTERACT conforme funções de escrita sejam
absorvidas por agentes como "IAs"? Haverá necessidade de uma vasta classe de
escribas no novo império que está a se desenhar?

### Libertando o design

Assim como o Fusca, o ThinkPad é de fácil manutenção ("serviceable"). Suas
peças são de fácil instalação e existem manuais detalhados para conserto.

Decorre que é um hardware favorito de boa parte da comunidade hacker que
entendeu que, de certo modo, já passamos pela época de esplendor dos PCs e e
que o melhor que fizeram até hoje foram ThinkPads.

Possivelmente, a comunidade do hardware livre fará a engenharia reversa de todo
esse rolê. E talvez aí haja uma semente para a difusão de uma computação que
incentive tanto o pensamento quanto a interação de forma emancipatória. Uma máquina
de pensar popular e acessível como alternativa às tecnotoxinas totalitárias
do turbocapitalismo.

Texto escrito a partir de um ThinkPad, máquina surgida nas contradições.

## Pontos altos

Alguns dos principais pontos altos dos (antigos) ThinkPads:

0. Resistência e durabilidade.
1. Facilidade de manutenção ("reparabilidade").
2. Trackpoint.
3. Facilidade de acesso aos discos por uma portinhola lateral (recentemente
   isso foi retirado por conta da transição dos discos S/ATA para as memórias
   M.2).

## Critérios de escolha

Ok, chega de groselha. Bora falar de hardware.

Bons critérios para escolha de modelos:

1. Facilidade no acesso a componentes, especialmente unidade de armazenamento.
2. Conector de energia comum.
3. Bom teclado e trackpoint.

Exemplos: T430, x200, x201, x230.

## Modelos

Esta seção inclui modelos "homologados" e também em pesquisa.

### X62 - 51NB

* [ThinkPad X62](https://geoff.greer.fm/2017/07/16/thinkpad-x62/).
* [X62 Laptop Review](https://www.notebookcheck.net/X62-Laptop-Review.211598.0.html).
* [Accept the fourth batch of X62 motherboard, New orders](https://m.facebook.com/notes/lcdfans/-accept-the-fourth-batch-of-x62-motherboard-new-orders/1877823262484777/?__tn__=H-R).

### X201 e X201i

* [Detailed specifications - ThinkPad X201, X201s](https://support.lenovo.com/br/en/solutions/pd010141).
* [Category:X201 - ThinkWiki](http://www.thinkwiki.org/wiki/Category:X201)

### X210

* [ThinkPad X210](https://geoff.greer.fm/2019/03/04/thinkpad-x210/).

### X220

* [X220 no ThinkWiki](https://www.thinkwiki.org/wiki/Category:X220).
* [Lenovo Thinkpad X220 - iFixit](https://www.ifixit.com/Device/Lenovo_Thinkpad_X220).
* [Downloads](https://pcsupport.lenovo.com/br/en/products/laptops-and-netbooks/thinkpad-x-series-laptops/thinkpad-x220/downloads).
* Problemas:
  * Não consegui iniciar um sistema com GRUB em modo FDE (Full Disk Encryption). Notas do ThinkWiki sobre "On booting":
    * The X220 cannot/will not boot GPT disks using Legacy BIOS, you must setup UEFI.
    * The X220 will not boot /efi/*/*.efi unless "signed"(?) into BIOS, you have to copy it to /efi/boot/bootx64.efi.
    * Disabling the BIOS setting "USB UEFI BIOS Support" disables *all* USB booting, ie, both UEFI and legacy BIOS.

### X230

* [x230 no ThinkWiki](https://www.thinkwiki.org/wiki/Category:X230).
* [Lenovo ThinkPad x230 - iFixit](https://www.ifixit.com/Device/Lenovo_Thinkpad_x230).
* [Downloads](https://pcsupport.lenovo.com/br/en/products/laptops-and-netbooks/thinkpad-x-series-laptops/thinkpad-x230/downloads).
* [Battery calibration issues (and tips for fixing): when battery starts to suddenly drop its level](https://www.reddit.com/r/thinkpad/comments/2fworf/battery_calibration_issues_and_tips_for_fixing/).
  * Exemplo: deixar por uns dias rodando a cada inicialização: `tlp setcharge 99 100`.

### X250

* [ThinkPad X250 Ultrabook Laptop](https://www3.lenovo.com/us/en/laptops/thinkpad/x-series/x250/).
* [X250 no ThinkWiki](https://www.thinkwiki.org/wiki/Category:X250).
* [Manual de Serviço](https://download.lenovo.com/pccbbs/mobiles_pdf/x250_hmm_en_sp40f30022.pdf).
* Problemas:
  * Oficialmente só suporta até 16GB de RAM.
  * "Any internal component replacement or upgrade (including hard disk and expansion cards) requires opening the back cover. (ThinkWiki)"

### T430 e T430i

* [T430 no ThinkWiki](https://www.thinkwiki.org/wiki/Category:T430).
* [Detailed Specifications - ThinkPad T430, T430i](https://support.lenovo.com/br/en/solutions/pd024705).
* Bateria do Thinkpad T430i - FRU P/N 45N1001, ASM P/N 45N1000.

### T440

* [T440 no ThinkWiki](https://www.thinkwiki.org/wiki/Category:T440).
* Problemas:
  * [Buttonless Touchpad](https://www.thinkwiki.org/wiki/Buttonless_Touchpad).

### T480

* [Category:T480 - ThinkWiki](https://www.thinkwiki.org/wiki/Category:T480)
* [ThinkPad T480 - ThinkPad_T480_datasheet_EN.pdf](https://psref.lenovo.com/syspool/Sys/PDF/datasheet/ThinkPad_T480_datasheet_EN.pdf)
* [Lenovo ThinkPad T480 | 14" Business Laptop with 8th Generation Intel® Core™ i7 | Lenovo US | Lenovo US](https://www.lenovo.com/us/en/p/laptops/thinkpad/thinkpadt/thinkpad-t480/22tp2tt4800)

### X280

* [Category:X280 - ThinkWiki](https://www.thinkwiki.org/wiki/Category:X280)
* [ThinkPad_X280_Spec.PDF](https://psref.lenovo.com/syspool/Sys/PDF/ThinkPad/ThinkPad_X280/ThinkPad_X280_Spec.PDF)
* [PSREF ThinkPad ThinkPad X280](https://psref.lenovo.com/Product/ThinkPad/ThinkPad_X280)
* Drawback: memory is soldered: [How to upgrade x280 Memory-English Community](https://forums.lenovo.com/t5/ThinkPad-X-Series-Laptops/How-to-upgrade-x280-Memory/m-p/4601913)

### T14

* [Category:T14 - ThinkWiki](https://www.thinkwiki.org/wiki/Category:T14)
* [ThinkPad T14 Gen 1 (Intel) - ThinkPad_T14_Gen_1_Intel_datasheet_EN.pdf](https://psref.lenovo.com/syspool/Sys/PDF/datasheet/ThinkPad_T14_Gen_1_Intel_datasheet_EN.pdf)
* [T14 Gen 1 and P14s Gen 1 Hardware Maintenance Manual - t14_gen1_p14s_gen1_hmm_en.pdf](https://download.lenovo.com/pccbbs/mobiles_pdf/t14_gen1_p14s_gen1_hmm_en.pdf)
* [laptops and netbooks :: thinkpad t series laptops :: thinkpad t14 gen 2 type 20w0 20w1contentdetail - Lenovo Support AU](https://pcsupport.lenovo.com/au/en/products/laptops-and-netbooks/thinkpad-t-series-laptops/thinkpad-t14-gen-2-type-20w0-20w1)
* [Lenovo ThinkPad T14 Gen1 User Guide - Manuals+ - thinkpad-t14-gen1-manual.pdf](https://manuals.plus/lenovo/thinkpad-t14-gen1-manual.pdf)
* [(English) User Guide (HTML) - ThinkPad T14, T15, P14s, P15s - Lenovo Support AU](https://pcsupport.lenovo.com/au/en/products/laptops-and-netbooks/thinkpad-p-series-laptops/thinkpad-p15s-type-20t4-20t5/manuals/um923720-english-user-guide-html-thinkpad-t14-t15-p14s-p15s)
* [Removal and Replacement Videos - ThinkPad T14 Gen 1 (20S0, 20S1, 20S2, 20S3, 20UD, 20UE), P14s Gen1 (20S4, 20S5), T14 AMD Gen 1 (20UD, 20UE ), P14s AMD Gen 1 (20Y1, 20Y2), T14 Gen 2 (20W0, 20W1, 20XK, 20XL), P14s Gen 2 (20VX, 20VY, 21A0, 21A1) - Lenovo Support US](https://pcsupport.lenovo.com/us/en/solutions/ht510512-removal-and-replacement-videos-thinkpad-t14-gen-1-thinkpad-p14s-gen-1-20s0-20s1-20s2-20s3-20s4-20s5).

## Memória

* [Memory Compatibility - Notebooks](https://support.lenovo.com/br/en/solutions/pd012623#x).

## Firmware livre

* [Libreboot – Hardware compatibility list](https://libreboot.org/docs/hcl/).

## Firmware oficial

* http://www.thinkwiki.org/wiki/Installing_Gentoo_on_a_ThinkPad_X220
* https://bbs.archlinux.org/viewtopic.php?id=122352
* http://www.thinkwiki.org/wiki/UEFI_Firmware
* https://raw.githubusercontent.com/ksergey/thinkpad/master/geteltorito.pl

Exemplo:

    perl ../../geteltorito.pl g2uj32us.iso > g2uj32us.hybrid.iso

## Modificações

* [GitHub - Evv1L/thinkpad-x230-upgrades: List of upgrades for ThinkPad x230](https://github.com/Evv1L/thinkpad-x230-upgrades)
* [ThinkMods store](https://thinkmods.store/)
  * [ExpressCard to NVMe Adapter – ThinkMods](https://thinkmods.store/products/expresscard-to-nvme-adapter)
  * [X230 - what SSD will best fit in the WWAN slot? - Thinkpads Forum](https://forum.thinkpads.com/viewtopic.php?t=135801)
  * [New ThinkPad Mod: ExpressCard to NVMe SSD Adapter - Thinkpads Forum](https://forum.thinkpads.com/viewtopic.php?t=130008)
  * [ThinkMods: ExpressCard NVMe Adapter | Indiegogo](https://www.indiegogo.com/projects/thinkmods-expresscard-nvme-adapter#/)
* [GitHub - n4ru/1vyrain: LiveUSB Bootable exploit chain to unlock all features of xx30 ThinkPad machines. WiFi Whitelist, Advanced Menu, Overclocking.](https://github.com/n4ru/1vyrain)
  * [Unlocking Hidden Potential In IvyBridge ThinkPads | Hackaday](https://hackaday.com/2020/02/03/unlocking-hidden-potential-in-ivybridge-thinkpads/)
  * ["1vyrain" - xx30 ThinkPad Jailbreak - Overclocking, Advanced Menu, ME Disable, Whitelist automated unlock with software! - Thinkpads Forum](https://forum.thinkpads.com/viewtopic.php?t=129880)

## Referências

* [Thinkpads Forum - Index page](https://forum.thinkpads.com/).
* [ThinkWiki](https://www.thinkwiki.org/wiki/ThinkWiki).
* [ThinkPad - Reddit](https://www.reddit.com/r/thinkpad/).
* [fluxo de links » Tags (Rótulos): thinkpad](https://links.fluxo.info/tags/thinkpad).
* [Lenovo: retro thinkpad](http://blog.lenovo.com/tag/retro+thinkpad).
* [Lenovo Laptop Repair - iFixit](https://www.ifixit.com/Device/Lenovo_Laptop).
* [IBM Laptop Repair - iFixit](https://www.ifixit.com/Device/IBM_Laptop).
* [CPU-Upgrade](http://www.cpu-upgrade.com).
* [Welcome to ThinkPads.org! - ThinkPads.org](https://libthinkpad.github.io/projects/)
* [ThinkPad FRU parts catalog - ThinkPads.org](https://libthinkpad.github.io/fru/)
