[[!meta title="Virtualized GUI environments"]]

Can't or don't want to use [Qubes OS](https://www.qubes-os.org/)? Here comes a straightforward sollution
that, while not offering the same level of security, is practical enough to be implemented in the confort
of your current FOSS OS!

A picture
---------

Could you spot the difference between the Tor Browser running in the host for the one inside the virtual machine? That's what we want to achieve!

![Screenshot](screenshot.png)

First things first
------------------

What you can do:

1. Create a virtual machine image of the operating system of your choice like [this example](https://padrao.fluxo.info/boxes/).
2. Setup basic X11 environment with automatic login and startup programs.
3. Configure your hypervisor to hide icons and additional decorations around the virtual machine.
4. Setup key bindings on your window manager to start/resume and stop/suspend the virtual machine.

Debian desktop
--------------

When using a debian virtual machine as a virtual desktop, consider the following:

    apt-get install lightdm ratpoison

Make sure to configure `/etc/lightdm/lightdm.conf` with something like

    autologin-user=vagrant
    autologin-user-timeout=0

If using VirtualBox, you might also want to try [virtualbox-guest-x11](https://packages.debian.org/stable/virtualbox-guest-x11).

Features
--------

* Good security through isolation.
* Improved start/stop of your application by using virtual machine suspend/resume.
* Minor performance penalties while running the virtual machine.

Limitations
-----------

* Memory and disk consumption.
* Clipboard might still be available to the virtual environment, see [this discussion](http://theinvisiblethings.blogspot.com.br/2011/04/linux-security-circus-on-gui-isolation.html).

Future
------

* This should be better documented!
* Automated expendable snapshots for one-time-use virtual machines.
* Automated recipes (puppet/ansible).
* Vagrant integration for fast provisioning of golden images.
* Alternatives to the VirtualBox hypervisor.

References
----------

Applications:

* [kvmx script](https://kvmx.fluxo.info).
* [vbox script](https://git.fluxo.info/vbox).
* [plymouth - Debian Wiki](https://wiki.debian.org/plymouth).
* [SPICE Project](http://www.spice-space.org/).

Other implementations:

* [Marco Carnut: Ambiente "Auto-Limpante" via Virtualização Ultra-Leve Descartável - Tempest Blog](http://blog.tempest.com.br/marco-carnut/ambiente-auto-limpante-via-virtualizacao-ultra-leve-descartavel.html).
* [Subgraph OS and Mail](https://subgraph.com/sgos/index.en.html).

Tips:

* If using Firefox, try to disable hardware graphics acceleration as it might impact performance and produce graphics artifacts. This behavior was seem on VMs running with qemu-kvm with SPICE.

Spice and KVM:

* http://www.linux-kvm.org/page/SPICE
* https://www.spice-space.org/spice-user-manual.html
* https://kuther.net/content/convert-virtualbox-kvmqemu
* http://www.ubuntugeek.com/how-change-display-resolution-settings-using-xrandr.html
* https://bugzilla.redhat.com/show_bug.cgi?id=1020393
* https://people.freedesktop.org/~teuf/spice-doc/html/ch03.html
* http://askubuntu.com/questions/107228/how-to-resize-virtual-machine-disk#481887
* http://wiki.qemu.org/Documentation/9psetup
* https://ask.fedoraproject.org/en/question/8080/shared-folder-with-qemu-kvm/

Mounting guest images:

* [Mounting raw and qcow2 VM disk images](https://alexeytorkhov.blogspot.com.br/2009/09/mounting-raw-and-qcow2-vm-disk-images.html) using `losetup` or `qemu-nbd`.
* [guestmount](http://libguestfs.org/guestmount.1.html) from [libguestfs, a library for accessing and modifying VM disk images](http://libguestfs.org/).

Image optimization:

* [Tip: Making a disk image sparse | Richard WM Jones](https://rwmj.wordpress.com/2010/10/19/tip-making-a-disk-image-sparse/).
* [How to convert a non-sparse image to a sparse image – Patrick's Blog(2)](https://blog.laimbock.com/2013/10/31/how-to-convert-a-non-sparse-image-to-sparse/).

    ionice -c 3 nice -n 19 virt-sparsify --tmp /tmp --convert qcow2 --compress box.img box.new && mv box.new box.img
