[[!meta title="Mensageria Suckless"]]

Quem não comunica se trumbica. Mas quem comunica também. Porque tá cheio de
sistema por aí que te afaga e te apedreja ao mesmo tempo. Oferece serviço de
vigilância gratuita que possui funcionalidade de comunicação.

Aqui queremos a boa e velha mensageria suckless, que também é uma merda, mas é
uma merda menor do que o enlatado baseado em soluções proprietárias oferecido
pelas startups turbocapitalistas.

Isto aqui é um esboço! Patches são bem vindos :)

Requisitos
----------

* Seja independente de plataforma e independente de um computador móvel como
  smartphones.

* Sessão persistente: podem te enviar mensagem mesmo que você não esteja online.
  Melhor ainda, que você conste como online mesmo que não esteja acessando a
  mensageria.

* Um mínimo de privacidade: conexão cifrada com o servidor, criptografia
  ponta-a-ponta com negação plausível pra falar com os/as amiguinhos, sem logs.

Ingredientes
------------

1. Conta shell num servidor com bom uptime e acesso SSH.

2. Mutiplexador de terminal.

   É uma espécie de "gerenciador de janelas" para
   a linha de comando que permite manter programas
   rodando em background mesmo quando o terminal
   é fechado.

   Ele permite que malandros deixem a mensageria
   IRC rodando no servidor mesmo que não estejam
   conectados via SSH.

   Exemplos: tmux e screen

3. Cliente de IRC

   Exemplos: irssi e weechat

Howto
-----

Logando e instalando:

    ssh servidor-remoto
    sudo apt install tmux irssi irssi-plugin-otr

Criando uma sessão para o IRC:

    tmux

Abrindo o cliente e se conectando no rolê:

    irssi

Para sair da sessão sem encerrar a mensageria, digite Ctrl B D
(control sequence do tmux mais o comando "detach"). Depois é
só encerrar a shell do servidor remoto.

Para se reconectar, basta

    ssh servidor-remoto tmux attach

Básico do IRC
-------------

    /network add -nick <nick> -realname <realname> freenode
    /server add -auto -ssl_verify -ssl_capath /etc/ssl/certs -network freenode chat.freenode.net 7000
    /save
    /connect freenode
    /join #canal

Bônus
-----

* Tor.
* Bitlbee.

Privacidade
-----------

Note que uma sessão persistente implica no cliente rodando num
servidor. Isso pode degradar sua privacidade, uma vez que o servidor
pode ser comprometido de várias formas. É importante ter consciência
disso.

Da mesma forma que seu celular pode ser invadido, roubado e ter dados extraídos,
um servidor também é um ponto vulnerável na sua comunicação, especialmente
se for nele que as chaves criptográficas estiverem armazenadas.

Assim, a sessão persistente de mensageria oferece um nível de segurança apenas
intermediário, servindo para a comunicação do dia-a-dia que não for sensível.

Para comunicação sensível, o melhor é rodar o cliente de mensageria diretamente
a partir do seu [Console Físico Confiável](https://opsec.fluxo.info/specs/tpc).
