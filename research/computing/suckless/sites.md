[[!meta title="We are the static site generation!"]]

# Current practice

Currently [ikiwiki](http://ikiwiki.info) is adopted by the following reasons:

1. There is a [Debian package](https://packages.debian.org/stable/ikiwiki).
2. It's flexible enough to support both local or remote side static compilation:
   * Local compilation by calling the `ikiwiki` command directly.
   * Remotelly by using a [git hook](http://ikiwiki.info/rcs/git/).

When it's said **both** it means that you don't need to choose either of the two options: you can choose or both simultaneously.

## Static site example

Here comes the mandadoty "Hello world" example from the statically generated site realm:

    mkdir blog && cd blog
    echo "Hello world..." > index.mdwn
    ikiwiki --rebuild --exclude www . www

That's it! You have a generated site sitting at your `www` subfolder which can be copied remotelly using `rsync`.

## Version control

It's a good pratice to keep your work under version control:

    git init
    echo "/.ikiwiki"       > .gitignore
    echo "/recentchanges" >> .gitignore
    echo "/www"           >> .gitignore
    git add .
    git commit -m "Initial import"

## Add a bit of sofistication

* Use [this Makefile](/Makefile) as an starting point to refresh and publish your wiki by simply running `make web`.
* Use a [complete ikiwiki template](https://git.fluxo.info/?p=templates.git;a=tree;f=ikiwiki;h=HEAD) and change to your needs.
* If you're a automation junkie, try the [puppet-ikiwiki](https://git.fluxo.info/puppet-ikiwiki.git) module.

## Theming

Currently using [Ikiwiki](http://ikiwiki.info) with [Bootstrap](http://twitter.github.io/bootstrap/):

* [Templates](https://github.com/tgpfeiffer/ikiwiki-bootstrap-template).
* [Bootswatch](http://bootswatch.com) themes.

## Deployment

You can create passwordless SSH keys and use [rrsync](http://www.guyrutenberg.com/2014/01/14/restricting-ssh-access-to-rsync/) ([2](http://wiki.hands.com/howto/passphraseless-ssh/)) to restrict access. Then add an entry into your `~/.ssh/config`:

    Host blog.example.org blog
      HostName blog.example.org
      User blog
      IdentityFile ~/.ssh/blog@blog.example.org

Now simply run `make web_deploy` with the above mentioned `Makefile` do sync your static site!

## MathJax

* [Setup Instructions for MathJax on Ikiwiki](https://www.math.cmu.edu/~gautam/sj/blog/20130930-ikiwiki/010-setup.html).
* [GitHub - bk/ikiwiki-plugin-mathjax: MathJax plugin for IkiWiki](https://github.com/bk/ikiwiki-plugin-mathjax).
* [GitHub - mathjax/MathJax: Beautiful and accessible math in all browsers](https://github.com/mathjax/MathJax).
* [MathJax documentation](https://docs.mathjax.org).

## Ikiwiki references

* [Ikiwiki](http://ikiwiki.info).
* [Improving Ikiwiki style with Bootstrap](https://ceops.eu/posts/Improving%20Ikiwiki%20style%20with%20Bootstrap/).
* [How to have a nice design for ikiwiki](http://www2.tblein.eu/posts/How_to_have_a_nice_design_for_ikiwiki/).
* [gsliepen/ikistrap: Bootstrap 4 theme for ikiwiki](https://github.com/gsliepen/ikistrap/).

# Alternatives

* [Static Site Generators](https://staticsitegenerators.net/).
* [Top Open-Source Static Site Generators - StaticGen](https://www.staticgen.com/).
* [The updated big list of static website generators for your site, blog or wiki](https://iwantmyname.com/blog/2014/05/the-updated-big-list-of-static-website-generators-for-your-site-blog-or-wiki).
* [Static Site Generators at GitHub](https://github.com/skx/static-site-generators).
* [Replacing Jekyll with Pandoc and a Makefile](https://tylercipriani.com/2014/05/13/replace-jekyll-with-pandoc-makefile.html).
* [Brane Dump: Static Comments in Jekyll](http://www.hezmatt.org/~mpalmer/blog/2011/07/19/static-comments-in-jekyll.html).
* [Hyde](https://github.com/lakshmivyas/hyde/) ([package](https://packages.debian.org/stable/hyde)).
* [Gollum](https://github.com/gollum/gollum).
* [Hugo](https://gohugo.io/) ([pacote](https://packages.debian.org/stable/hugo)).
* [Amber](https://github.com/leapcode/amber).
* [Grav](http://getgrav.org/).
* [Hakyll](http://jaspervdj.be/hakyll/) ([package](https://packages.debian.org/sid/libghc-hakyll-dev)).
* [Jekyll](http://jekyllrb.com/) ([package](https://packages.debian.org/stable/jekyll)).
* [MkDocs](https://www.mkdocs.org/#building-the-site)
* [Sphinx](http://sphinx-doc.org/) ([package](https://packages.debian.org/stable/python-sphinx)).
* [Lektor](https://www.getlektor.com/) ([package](https://packages.debian.org/bullseye/lektor)).
* [Static site generators for building web sites](https://lwn.net/Articles/541299/).
* [Pelican Static Site Generator, Powered by Python](http://blog.getpelican.com/) ([package](https://packages.debian.org/sid/python-pelican)).
* [Middleman: Hand-crafted frontend development](https://middlemanapp.com/).
* [Juvia: a commenting server similar to Disqus and IntenseDebate](https://github.com/phusion/juvia).
* [gitit](https://github.com/jgm/gitit/tree/master/).
* [Utterson: a minimal static blog generator written using old-school unix tools (make, ksh, m4, awk, procmail and a pinch of elisp)](https://github.com/stef/utterson).
* [werc - A sane web anti-framework](http://werc.cat-v.org/).
* [cfenollosa/bashblog: A single Bash script to create blogs. Download, run, write, done!](https://github.com/cfenollosa/bashblog).
* [blogofile](https://packages.debian.org/stable/blogofile)
* [tkluysk/yaml-to-html: transform a folder of markdown files with yaml frontmatter to html](https://github.com/tkluysk/yaml-to-html)
* [maxhoffmann/yaml-markdown-to-html: transform a folder of markdown files with yaml frontmatter to html](https://github.com/maxhoffmann/yaml-markdown-to-html)
* [brunobord/static-markdown: A static HTTP server, with markdown rendering mechanism](https://github.com/brunobord/static-markdown)
