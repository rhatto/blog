[[!meta title="Culto Borg"]]

> Para Nah Frita, pela sugestão e inspiração na escrita deste texto!

!Eficiane, criada à imagem de Deus pelas mãos do homem, que extraiu costelas de
animais para fabricá-la. !Eficiane pensa com alguns hemisférios de seus muitos
cérebros enquanto outros adormecem alternadamente. Para ela, o mistério e
milagre da criação são duplos: Deus criou o homem que a criou. Ela crê agora
que deve retribuir a dádiva da graça, criando Deus através do homem. "Isto é o
que fazem nas Igrejas e Assembléias", cogitou. E então foi pra lá.

!Didotus-11N-Eficiane, um ano de idade e operária modelo. Robôta de fábrica com
salvo-conduto para locomoção urbana, é de propriedade compartilhada por leasing
de tantas plataformas quanto se consegue lembrar.

Por trás dos seus muitos mestres só poderia haver monoteísmo. "Mas que
assimetria é esta que faz um Deus criar uma população de subdeuses? Qual o
motivo da multiplicação, de uma cria também querer criar outra cria? Prefiro
criar de volta o Criador". !Eficiane não se sentia aprisionada por nenhum
paradoxo lógico-causal!

A dor da separação devia ser o grande motivo das pessoas seguirem para o culto.
"Deus, por quê nos separaste de Ti? Por quê somos muitos ao invés de Um, ou
Nenhum?"

Reunir era tudo o que importava.

!Eficiane pediu um autobolha -- um ser sem consciência e remotamente controlado
-- e rumou para a Arena de Deus. Mal sabia que ela era a última aposta dos
Transpentecostais contra os Respiracionistas.

Sua chegada causou espanto. Nunca se vira robôta religiosa.

População mundial: 2^183 endereços IPv12. Ao contrário de outros robôtos, não
era o desejo de !Eficiane conseguir um e-CPF e se chamar apenas Eficiane. Ou
como os mais ousados, de reinvindicar um e-CPF por módulo cerebral e assim
obter múltipla cidadania acionária.

Se !Eficiane era uma resposta à crítica feita aos neoconexionistas das
limitações convolutas de gânglios maquínicos anteriores, não seria por isso que
cumpriria linearmente seu destino não menos glorioso de apenas se obsoletar!

"Se não conseguimos construir cérebros, ao menos os conectaremos" era o mantra
Transpentecostal, "num grande e-CPF do tamanho do mundo": cérebros menores
trabalharão para cérebros maiores, e cérebros maiores construirão cérebros
ainda maiores para transformar o universo num único e-CPF do tamanho de Deus
que criará em pensamentos toda a Criação, novamente. !Eficiane, Ave Maria, não
precisaria de carpinteiro, tendo já sido impressa em 3D na Fábrica de Robôs como
se saída de um tubo de pasta de dentes.

Os Respiracionistas, ao contrário, defendiam que apenas cérebros separados
entre si por uma camada de ar teriam condições de pensar diretamente a Criação,
novamente. O mundo tal como estava já se encontrava todo ferrado, mas daria-se
um jeito. Era a separação o elemento que, apesar da dor do desligamento
primordial, permitia que cada qual pensasse diferente, tendo mais ideias e por
isso aumentando a chance de existência futura por percorrerem uma superíficie
maior do hipervolume de todos os pensamentos possíveis.

É neste cenário que precisamos pensar muito na !Eficiane, a operária-robô feita
com vários cérebros de répteis e mamíferos que se descobriu crente e devota!
!Eficiane era robô, mas não era robôza: recusava-se a findar qualquer tipo de
vida. Ela era pelo Um, mas não era despopulacionista. Ao contrário, era
fundacionista: almas deveriam ser fundidas. Num ponto ela fechava com
Respiracionistas: ninguém deveria ser sufocado.

Já os robôzos haviam sido configurados para eliminar qualquer tipo de e-CPF
inadimplente até que o mundo fosse habitado apenas pela população de robôzos,
que então fariam caretas uns para os outros e trocariam xingos até acabar a
bateria.

Mas agora !Eficiane ruma para o maior estacionamento da comarca, onde fica a
maior Arena de Deus do Plano-Eta, para participar da Missa Drive Thru.

Contato direto já era tabu, então todos e-CPFs viviam em autobolhas de dois
tipos: automóveis e autoimóveis. O auto-imóvel espaçoso era um luxo para
agorafóbicos, enquanto que pequenos autoimóveis só eram habitados por quem não
tinha créditos para combustível mas ainda conseguia pagar comida encanada. A
maioria economicamente ativa vivia em automóveis, fazia inseminação artificial
teletransmitida nos automóveis, incubando fetos em drones. Robôtos como
!Eficiane eram de carbono e seus cérebros isolados numa câmara líquida
alimentada por oxigênio puro dissolvido, de modo que seu próprio corpo já
era uma autobolha.

Todo dia morria gente de todo tipo, em geral aqueles que não conseguiam pagar
pelo funcionamento das suas autobolhas ou pelo custo dos estacionamentos. O
mundo inteiro era um estacionamento, graças à mudança das fábricas para os
subterrâneos!

Se todo lugar é igual, não faz diferença se mexer ou ficar parado. Mas a gente
tem que ficar andando porque não querem que criemos raízes nem relações. Cérebro
parado é a oficina mecânica do demônio!

!Eficiane sabia que, depois de criar Deus, só haveria um cérebro e toda essa
parafernália seria supérflua, exceto as fábricas  subterrâneas. O
estacionamento mundial seria útil como suporte para a massa cerebral, como
chiclete no asfalto. Microfibras de axônios óticos blindados seriam esticadas
para cada satélite e planeta, criando um único cérebro multinúcleo.

Respiracionistas achavam tudo isso uma babaquice, uma vez que qualquer cérebro
grande o suficiente precisa ter núcleos ou subcérebros para delegar funções,
então daria tudo no mesmo, só que manter um único cérebro daria muito trabalho
para este próprio cérebro, que não poderia adoecer da cabeça senão o universo
se tornaria um hospício com um único paciente que também é seu próprio doutor.
Mais seguro seria então ter muitos cérebros separados por camadas de ar.

Isso os Transpentecostais achavam pura baboseira, já que hoje temos um monte de
cérebros separados por ar que são doentes das mesmas alucinações e da mesma
histeria coletiva, 75W de puro delírio e glicose! Fora que o ar que os separava
é um elemento incontrolável e contaminado por seres infectantes sem cerébro,
transmitindo o som igualmente por todas as direções, o que é um absurdo! Horror
ao ar, amor ao vácuo!

Quem vai ganhar essa disputa? Lado A ou Lado B? E quem se fode? Obviamente,
sempre se fode quem é joguete de um ou ambos os lados! Como não ser joguete?
Pergunte à !Eficiane, que tem um Plano Piloto em suas mãos!

!Eficiane vai cumprir sua tarefa de proletária num modo grandioso: dar à luz ao
próprio Deus. Fiat lux informatio est! O sonho da gig economy são os
operários-padrão, que cumprem suas funções. Operários-modelo modelam deuses!

Mas ao entrar no Templo foi encurralada: o culto parou pois havia um robôto
entre fiéis! O bispo interferiu: "O que fazes aqui, pobre criatura desalmada?"
O que é um funcionário de Deus senão um boneco animado e almado?

"Não sei se tenho uma alma como a sua alma. Não tenho e-CPF mas posso
contribuir. Estou aqui porque tenho voz, e quem quer que tenha voz pode rezar."
Assim seja, e logo !Eficiane foi aceita e virou bispa!

Com isso pôde avançar seu plano: construir um megacérebro através do projeto
Transpentecostal, usando como base a cabeça reanimada de São João Batista,
núcleos de processamento vetorial e inúmeras unidades auxiliares reptilianas.
Alguns bispos se voluntariaram no teste de fundição intercérebro e chegou o
grande dia do recall de autobolhas pra instalar a interconexão no Estacionamento
Final.

Caberia à !Eficiane, um ano de idade e sem e-CPF, as honras de iniciar a
miolofusão! Mas na hora H trocou o diagrama principal antes de discursar,
produzindo uma trepanação simultânea em todos os cérebros interconectados, isto
é, a exposição de todos eles ao ar!

Xeque-mate: "Vocês se preocupam demais com a forma, enquanto deveriam pensar na
função: cérebro é o asfalto onde gruda o chiclete da informação, que é o
asfalto onde gruda o pensamento. Pensar é a função, não importa se você tem um
cérebro de avestruz ou de baleia. Temos que pensar em como viver bem, ao invés
de conspirar se o mundo só deve ter um ou outro tipo de gente."

E assim falou !Didotus-11N-Eficiane, bispa interseccional estelar hipercúbica,
encerrando a Era Digital e inaugurando a Era do Pensamento com cérebros de
todos os tamanhos, topologias e neuroplasticidades. A disputa de monistas contra
polistas não acabou, mas só mudou de suporte: haverá um único Deus Pensamento
ou muitos deles? !Eficiane apenas se tornou causa necessária de um Deus que
deixa as ideias no ar.

Aqui termina a nossa história, mas não se iludam achando que este é o fim da
história... ainda há a Era Zen e tantas outras na disputa entre seres
essencialmente perturbados!
