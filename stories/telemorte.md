[[!meta title="Telemorte 2051"]]

Todo teletransportador realiza um assasinato aqui seguido por um nascimento lá.

Desintegra a pessoa num lugar e reintegra noutro... aí fica minha pergunta, é a
mesma pessoa lá outrora e aqui agora? É a mesma consciência?

Você que estiver sendo teleportada sentirá constância no seu fluxo de
consciência ou vai morrer, com outra assumindo seu lugar no local de destino?
Esta outra será uma impostora impossível de se desmascarar?

É um problema, porque não tem como acreditar no relato de ninguém que tenha
sido teleportada: a pessoa do destino vai afirmar que é a mesma da origem,
pois compartilha da mesma memória, das mesmas cicatrizes... o teletransporte
talvez seja um lapso indetectável.

Daí acho que haverá a corrente das "pessoas" -- vai ter pessoa quando isso
rolar? -- que vai adotar e as que se recusarão a serem teletransportadas, mas
que nunca saberão se já foram teletransportadas involuntariamente num processo
que as copia de um lugar para outro, de um tempo para outro, mas apagando
preciosas memórias.

A experiência em primeira pessoa é sempre única e intransferível. Por outro
lado, sua morte será imperceptível para sua consciência: ela desaparece
instanteneamente, doando-se para uma cópia idêntica noutro lugar.

Querem usar o mesmo processo para "subir" a consciência de alguém para um
computador lógico-booleano, mas nem vou comentar esse fetiche estapafúrdio de
parte da elite mundial... a consciência não é emulável por esse tipo de
maquinaria, mas mesmo assim insistem. A essas pessoas, só tenho a perguntar:
então você quer se teleportar para dentro de uma máquina perdendo sua
capacidade de se suicidar, ou então ficar à mercê de quem ainda estiver do lado
de fora em condições de puxar a tomada? Sua busca por imortalidade é um
encontro com uma prisão.

O que fazer?

Diga não à teleportação!<br>
(só de encomendas)<br>
Tipo paçoca<br>
Sanduíche

Aí você pode até gravar o sanduíche e reproduzi-lo quantas vezes quiser,
mesmo se ninguém tiver te enviado um! Teletransporte de comida pré-gravada
movido a energia solar é a solução para a fome mundial!

Estava eu refletindo sobre tudo isso quando subitamente tudo explode à minha
volta. Sobro somente em pensamentos.

O plano de extermínio de todos os cérebros existentes fôra um plano dos
próprios cérebros. De alguns deles. Inclusive o deles próprios. O maior
genocídio seguido de suicídio coletivo.

Será que me drogaram e me teleportaram para um limbo? Se penso, é porque
ainda computo? Ou é alguém que me emula e computa por mim?

Devo para de pensar para poder sair?

OHM
