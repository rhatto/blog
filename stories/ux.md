[[!meta title="Experiência de Usuário: Brasil 2029mg"]]

## Sobre

Desde os anos passados nos educandários e reformatórios não escrevia nenhuma
história curta ou redação. Então considero este como meu primeiro conto.

Escrito durante uma grande ressaca moral da quarta-feira de cinzas de 2019, na
impressão de que todo o amor havia acabado e iniciava-se um enorme ciclo de
desagregação social.

[Selecionado](https://www.revista-pub.org/post/contosvencedores) para a
coletânea Brasil 2029 do [Concurso Marry Shelley de Contos Góticos e Pós
Apocalípticos](https://www.revista-pub.org/post/brasil2029-concursodecontos),
fica disponível online em 2020 num momento em que [a mega indústria inicia uma
virada
distópica](https://theintercept.com/2020/05/13/coronavirus-governador-nova-york-bilionarios-vigilancia/).

[[!img 2029.png link="no"]]

Agradecimentos especiais a Guilherme Purvin da ([Revista
PUB](https://www.revista-pub.org) e do
[IBAP](http://ibap.org/abertas-inscricoes-para-23o-congresso-do-ibap/)) e a Gavin
Adams.

Leia a íntegra a seguir ou baixe em [PDF](2029.pdf) ou [EPUB](2029.epub).

# Experiência de Usuário - Brasil 2029mg

por Silvio Rhatto

versão 0.6 - 18/08/2019

## 1

**Pim Pum. Bom dia, jovem empreendedor! Vamos acordar? Sua nanoempresa individual fechou ontem com score 0,4. Saldo devedor de \$25.483,00 com juros de 1% ao mês. Seu custo de vida diário é $348,75. Você precisa se esforçar mais! O sucesso só depende de você! :smile: :thumbs_up:**

Humm....

**Seu hit favorito da semana é Jesus Lacração do conjunto Fúria Messiânica:**

_Shurubalah!_

_Profanadores de Túmulos Não Passarão_

_Bello Tchau Bum!_

_Tchum Tchá Tchá Tchum Tchum Tchá!_

_Rot Rot Rot!_

_Castiga a mulherada!_ (fade out virando som de fundo)

**Duas super ofertas de #trabalho para #hoje: Capataz de Limpa Fossa na Av. Heróis da Pátria ou Teste de Remédio Experimental.**

Nossa, gostei desse sonho que o Dreamr me mandou! Tá cada vez melhor. Acho que vou assinar o Premium pra ter aqueles microsonhos quando tiver no trem.

**Sem créditos para isso! Você anda gastão. Escolha seu emprego do dia. Você deu sorte com a chance de testar um novo remédio, ganhar $103,80 e colaborar com a sociedade.**

Porra, ainda tou sem grana. E queria mandar mais moedinhas pro #crowdfunding dos Expedicionários da Terra Plana. Eles vão chegar lá, vão furar o muro de gelo e tirar o Encosto do mundo.

**Sei que daqui 2 segundos você vai sentir vontade de trucidar alguém, Lobo Solitário! Hohoho, te conheço!**

A culpa é do Encosto. Abrir App Julgamento Popular.

**Gangue esquerdista. Olha só pra esses caras. O do meio é viado com certeza.**

Por mim pode matar.

**Grato pelo civismo!**

**Agora tome sua ração de javaporco, vista seu terno azul favorito e vá trabalhar.**

Gulp gulp. Limpa-Fossa. Fala sério. Ninguém merece.

Ok, vou testar o remédio. Pensei que já usavam os vagabundos dos presidiários pra isso.

**Usam, mas você é uma das poucas pessoas que dão match com o teste.**

**Vá rápido! Outro candidato já tá indo pra lá e o prêmio caiu para $95,32, Imposto Único já descontado.**

`#`diademerda talkei...

## 2

**Pegue a linha azul e desça na estação Castidade.**

**Suprimindo pensamentos inúteis até você chegar e ligando modo autopiloto. Recompensa de $3,20.**

**Mantenha-se à direita.**

Odeio metrô. Nossa, que #gostosa. Quero estuprar.

**Sem chance. Essa é de família. Não tem mais vadias de esquerda pra encoxar neste vagão. Nem na cidade.**

**Você está muito agitado hoje. Ligando OrgasmatrON...**

Oh! Oh! Oh! Uff... obrigado.

**Custou R0,32 no Plano Flex Pós.**

**Agora passe o tempo jogando CandyClysm.**

Bola. Quadrado. Quadrado bola sobe sobe lado desce desce. Oba!

**Anúncio Patrocinado! Liberte a Polícia que existe dentro de você: denuncie os pedintes e ganhe $2!**

Bola gude. Bom. Vermelho. Saliva. Suco. Prêmio prêmio!

**$0,75**

Modo Roleta Russa Tudo Ou Nada.
Espinho. Facada. Bomba. Melancia. Droga...

**Perdeu. $0,93.**

Posso jogar Cem Dias de Ilha de Caras?

**Não. Quem contou disso pra você?**

Vi num anúncio na rua.

**Era ilegal. Esqueça.**

Esquecer o quê?

**Ei, você ainda não assistiu Cinco Minutos de Aquário Aquecendo Até Ferver! Veja agora.**

Ok.

## 3

**Saia do trem.**

**Vire à direita.**

**Em 8 metros, vire à esquerda.**

**Mantenha a direita na escada rolante.**

**Mantenha sempre a direita.**

**Atravesse a catraca e vire à esquerda.**

**Suba a escada rolante.**

**Ao sair da estação, vire à direita e siga por 100 metros.**

**Entre no prédio à esquerda.**

_Entrada autorizada._

**Pegue o elevador F.**

**Subindo.**

**Saia à direita. Direita.**

**Pegue o pacote no locker que está aberto na parede à frente.**

**Abra e tome o comprimido.**

Ephemerol de Claril Crocodileno.

**Não leia. Engula. Mais confiança, empresário!**

Gulp.

_Parabéns! $95,32 na sua conta! Tenha um bom dia._

**Saia do prédio. É propriedade privada e sua presença será indesejada em 5 minutos.**

## 4

Ok, tou na rua. O que eu faço agora?

**Sei lá. Não tem mais trabalho pra você hoje. Veja umas notícias...**

**EXTRA! Rebelião em Bangu 36 termina com 7 mil mortes!**

**Navio-Presídio Guanabara 8 afunda por excesso de peso!**

**Snipers robóticos fazem novo recorde e abatem 3582 num único dia deste verão! Fetos de todas as gestantes mortas resgatados com sucesso!**

**Relógio do Juízo Final ajustado para 23:59:35!**

`#`PQP!

A minha cabeça tá doendo...

**Criança maior de idade faz 12 reféns com o drone-metralhadora do pai em Osasco!**

**Lulu Monteiro instala prolapso retrátil de 4 polegadas! Confira as imagens!**

**Bolão da Rinha Humana acumula 12 milhões na Lotérica Federal! Faça logo sua aposta!**

Argh. Tá esquentando... meus olhos tão que nem ovo frito!

**Morte por apedrejamento re-estréia com casal pego se beijando em público!**

Aaahh.

**Previsão do Tempo: 6 barragens devem estourar neste final de semana com a passagem de mais um ciclone!**

Aaahhhhhh...

**Ministério da Família estima 26% mais linchamentos neste mês!**

Aaahhhhhhhhhhhhhh!!! Sai da minha cabeça!!!

**10:00. Glória a #Deus!**

Aleluia!

Que dor!

**Ativar reza giratória em línguas!**

Aaahhhhhhhh. Ãr ãr ãr... arf arf...

Zeterubar aveguerrói tingater shumishumi.

Xileco enuma rorrorirrgzzioobbbb.....

Aaaaaarrrrrhhhhhhhhhhhhhh!!!!!!

**bzzzZZZZzzzzzz tilt glitch nononononono.....**

**[meta] WARNING: Blipvert overflow!!!**

**[meta] WARNING: Entering into admin mode with low safeguards... might be remotely exploitable...**

_Interlinked_

**[meta] Dopamine response needed.**

**[meta] More dopamine. Dopamine, please!**

**[meta] Deep Depression ahead. Aborting.**

_Interlinked_

**[meta] Rebooting Ultraego Hypervisor... (C) 2027 Aleph Holdings.**

**[meta] Systemd failed to bring up Mephisto daemon.**

**[meta] Id/Ego binding error... sending Distress Code to the Mothership...**

**[meta] Indroid Smarthead is shutting down.**

OI? O que tá acontecendo?

A dor sumiu.

...

...

...

Pastor? Você tá aí? Fala comigo.

...

Pastor? O que eu fiz de errado?

Sem você não falo com Deus. Nem Deus fala comigo. Nem ninguém.

...

A gente fez uma promessa. Um nunca ia deixar o outro. Eu dou dízimos. Você sabedoria. Entendeu?

Você prometeu parar todas as outras vozes. Eu só ia escutar a sua. Eu só ia falar com você. E não ia mais sentir dor. Só prazer.

**gã gã gã gã gã...**

Foi essa merda que tomei, não foi? Tá com encosto. Tou fodido, baixei o App do Encosto!!!

**vvVVvvvvzzziiiiiiiiiiiiiiiiiiiiiiiiiiiiiinnnn..........**

**[meta] Over-The-Gut upgrade successful.**

**[meta] UberEgo InnerVoice selection:**

**[meta] (0) Socratic Dialoguer (test run only)**

**[meta] (1) Pentecost Speaker**

**[meta] (2) Archaic Father**

**[meta] (3) Castrating Mother**

**[meta] (4) MKUltra Headhunter**

**[meta] (5) Schizoid Symphony**

**[meta] (6) Sadistic Sophist**

**[meta] (7) Nihilist Psychoterapist**

**[meta] (8) Amateur Lobotomist**

**[meta] (9) Skinnerian Engineer**

**[meta] (10) Surrealist DeepFaker**

**[meta] (11) Dadaist Preacher**

**[meta] (12) Bored Microinfluencer**

**[meta] (13) Tyler Durden (M.A.D)**

**[meta] Please choose your profile(s): 4**

**[meta] Loading A/I...**

**[meta] ManchuriaD online. load average: 1,06, 1,12, 1,07**

**[meta] Adapting to the subject... done.**

**[meta] Downloading mission data... done.**

ã?

**OLÁ, MAICON.**

Pastor?

**NÃO.**

Tem um clarão bloqueando minha tela... não dá pra enxergar nada!

**É A LUZ DIVINA.**

Senhor?

**NÃO É TODO MUNDO QUE TEM A CHANCE DE FALAR DIRETAMENTE COM O PRÓPRIO.**

Aleluia! Eu acredito, Senhor!

**PARABÉNS. VOCÊ ENCONTROU COM O CRIADOR, MAS AINDA ESTÁ VIVO, FORTE E SÃO!**

Mas e o Pastor?

**DE AGORA EM DIANTE SEREMOS SÓ EU E VOCÊ. ESQUEÇA AS DÚVIDAS E AS DÍVIDAS.**

`#`Brasil acima de tudo! #Deus acima de todos!

**É O SEGUINTE, MEU FILHO. #DEUS MODERNO É PAPO RETO.**

**VOCÊ TEM UMA MISSÃO ÚNICA. PORQUE VOCÊ É UM HOMEM SANCTO, O MENSAGEIRO DO VERDADEIRO #MESSIAS, NÃO NENHUM OUTRO DOS FALSOS QUE ANDAM POR AÍ.**

**NÓS TE ACHAMOS, CELIBATÁRIO INVOLUNTÁRIO. NÓS TE DESRECALCAMOS. A SUA CABEÇA É MUITO IMPORTANTE PARA NÓS. MAS ANTES, PRECISAMOS TESTÁ-LO.**

**SEU PRESIDENTE FALHOU. ELE COMETEU MUITOS ERROS. AGORA ELE TAMBÉM DEU PRA SER BROCHA. O BROCHA NACIONAL.**

**A JUSTIÇA DIVINA SE FAZ PELA MÃO DO HOMEM, JOVEM ZELOTA.**

**SIGA MEU COMANDO DE VOZ!**

**VOCÊ DEVE LIQUIDÁ-LO!**

**ELE USA SONDA ENCEFÁLICA JUNTO COM O ÚTERO ARTIFICIAL DO PROJETO HERDEIRO 05.**

**USE SUA FACA DE GRAFENO AUTODEGRADÁVEL PRA NÃO SER DETECTADO.**

Uma facadinha no bucho e o saco de merda vira lama tóxica morro abaixo!

**SIM! O PRESIDENTE ORA SÓLIDO DEVE VIRAR DEFUNTO LÍQUIDO!**

**ABRIREI SEU CAMINHO ATÉ ELE. E VOCÊ ABRIRÁ SEU CAMINHO COM A PISTOLA PLÁSTICA IMPRESSA NO CENTRO CÍVICO.**

Missão dada é missão cumprida, Senhor!

## 5

`#`Deus disse: que ia ficar em silêncio. Parece que minha cabeça explode se escuto #Ele demais.
Ninguém pode estar com #Ele por muito tempo. Me disse pra falar comigo mesmo enquanto isso.
Porque o que eu disser fica gravado numa caixa preta. Pra ajudar os próximos zelotas.
Nunca mais falei comigo mesmo. Desde que instalei o Pastor.
Acho que #Ele faz experimentos. Junto com os Angenheiros. Pra melhorar a espécie. Depois dessa, acho que vou virar tipo um
Super-Homem. Herói do Brasil! Já sou bombadão e agora com `#`Deus na cabeça vou saber de tudo.
Será que também vou poder fazer meus experimentos? Copiar `#`Deus pra cabeça das outras pessoas.
Fazer elas fazerem o que eu quiser... salvar a humanidade!

...

Um tempão no carro automático. Guiado por #Deus. Ele desacelera só uma vez quando atropela um mendigo. Depois só ando.

Todas portas tão abertas. Passo por um monte de gente. Ninguém fala nada. Tudo autorizado.
Tem um cara de gravata. Um monte de gente em volta dele.
Tudo do jeito que o Senhor disse.

**É AGORA!**

**ELE TÁ USANDO OS SURDOS-MUDOS DE ESCUDO! CHEGA PERTO E PASSA O PENTE!**

dum dum dum dum dum!

**AGORA CUIDE DO CHEFÃO! CHEQUE E MATE! COMO UM TURCO MECÂNICO AO REVERSO!**

Hein?

**FURA O BUCHO DO BOY!**

Tum tum tum tum faca faca faca faca!!!!!!!!!! Estoca estoca estoca torce entuxa! MORRE CUSÃO!!!

**AGORA PRONUNCIE O MANIFESTO DIVINO EM VOZ ALTA!**

NÃO GOSTEI DO LOGOTIPO DO SEU NOVO GOVERNO E ENTÃO #DEUS MANDOU TE APAGAR!

**HAHAHAHAHAHAHA!**

**HAHAHAHAHAHAHA!**

**HAHAHAHAHAHAHA!**

**Aí, trouxa, você foi trollado! Matamo o coroa e agora você vai se foder. Vamo rapar tua memória e todo seu limite de crédito! #Deus meu ovo! Worldwide Prankstering for the Lulz(TM)!**

????

????

????

**GAME OVER**

**RIP MAICON BISPO - "A MAN WITHOUT PAST" - 2010-2029 AD / SCORE -$99,999.99 (P.E.A.O. - LOW SUBPRIME HETERONORMATIVE URBAN TYPE - e-CPF 9929172128-20 CANCELADO)**
