[[!meta title="Livros"]]

Resenhas, anotações, excertos, enxertos, fichamentos e divertimentos numa
divisão por tópicos nem sempre coerente, muitas vezes arbitrária ou fazendo
a difícil escolha de decidir qual é o assunto preponderante de cada livro
ou do momento em que foi lido, tendo esta tortuosa catalogação complementada
com o uso de [tags](https://ikiwiki.info/tags/).

[[!inline pages="page(books*)" archive="yes"]]
