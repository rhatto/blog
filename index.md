[[!meta title="cypherprank machine"]]

<!--

   the
     ▄      ▄       ▄  _, ▄   _, ▄
    ░  ░_█ ░_█ ░__ ░_ ░  ░_█ ░  ░_█ ░▄_ ░▄▀
     ▀  -▀ ▀   ▀ ▀ ▀- ▀  ▀   ▀  ▀ ▀ ▀ ▀ ▀ ▀
                                    machine
-->

[[!inline pages="page(*) and !ikiwiki/* and !index and !sandbox and !templates* and !smileys and !shortcuts and !ikiwiki and !RecentChanges and !*/Discussion" archive="yes" limit="0" feedlimit="0"]]
