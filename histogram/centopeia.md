[[!meta title="Hardware da Semana - A Centopéia"]]

[[!img histogram/centopeia/IMG_20160525_133427.jpg link="no" alt="Centopéia lado A"]]
[[!img histogram/centopeia/IMG_20160525_133630.jpg link="no" alt="Centopéia lado B"]]

Belíssimo gadget solucionando o problema para o acumulador sem-teto. Impossível de achar onde está o flagrante.

A cabeça da centopéia é o próprio mendigo, o que economiza muitos Watts e P&D em A/I, liberando tempo para a atividade-fim, que é acumular lixo.

Crackers do centrão agora se preparam para construir um aríete e estourar a Secretaria de Segurança Pública.

Foda-se a obsolescência programada! Abaixo o fascismo da elite pós-humana!
