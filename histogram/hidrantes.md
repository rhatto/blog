[[!meta title="Hidrantes Sentimentais"]]

Solidão
-------

[[!img histogram/hidrantes/IMG_20160128_200745273.jpg link="no" alt="Solidão"]]

Timidez
-------

[[!img histogram/hidrantes/IMG_20160128_200916924.jpg link="no" alt="Timidez"]]

Amizade
-------

[[!img histogram/hidrantes/IMG_20160128_200018250.jpg link="no" alt="Amizade"]]

Sede
----

[[!img histogram/hidrantes/IMG_20150912_222650381.jpg link="no" alt="Sede"]]

Afogamento
----------

[[!img histogram/hidrantes/IMG_20151001_195801567.jpg link="no" alt="Afogamento"]]

Na fila de atendimento
----------------------

[[!img histogram/hidrantes/IMG_20150914_191549601.jpg link="no" alt="Atendimento"]]
