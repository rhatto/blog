[[!meta title="SNUFFART: Bernhard Roskoff's Readymades"]]

Readymades articulam a tensão da irreprodutibilidade de um objeto
artístico que é justamente oriundo do mercado de massas.

Roskoff quer ir além disso: como devolver ao mercado de massas o caráter
irreprodutível de cada artigo?

Resgatando o conceito de Historicidade (Philip K. Dick), Roskoff acredita que a
sociedade pós-industrial está à beira de uma crise pelo design: qualquer objeto
técnico -- de parafusos a absorventes e dentaduras -- carrega uma marca
opressiva do excesso de projeto e gerenciamento.

Pelo contrário, a aura restante do objeto de arte está justamente nas condições
históricas -- nunca de todo conhecidas -- que o origina.

Roskoff vai então muito além dos mictórios para explorar a arte espontânea das
pilhas de lixo das grandes metrópoles, produtos da ausência de design, ausência
de políticas urbanas e ausência de sensibilidade do público.

É preciso transformar o espontâneo e efêmero em sólido eterno para que surja a
percepção da matéria.

O alto custo da arte de Roskoff é ainda um ótimo atrativo para investigar a
ambiguidade entre o mercado e a arte.

PROCURA-SE
==========

* Artista de fachada e alpinista social.
* Para representar conglomerado anônimo em esquema caixa 2.
* 01 VAGA.

Projeto
-------

Material necessário:

* Equipe de extração 24hs.
* Um caminhão pipa de substância fossilizante.
* Um guindaste do tipo munck.
* Um caminhão de cargas.
* Serra circular gigante.

Temática
--------

* ACMEzação da arte.
* Gentrificação e agressão urbana.
* A arte dos packrats e o tesouro dos sambaquis.
* Crítica à acumulação como doença, desapego como virtude.

Breve: Reality Show dos Acumuladores!
-------------------------------------

Um espaço no museu preenchido por acumuladores disputando recursos escassos
e rejeitos do mercado da arte!

Referências
-----------

* http://nycgarbage.com/about/
* https://en.wikipedia.org/wiki/Found_object
* http://www.ifobot.com/sale_2011_69.html
* http://www.moma.org/collection/theme.php?theme_id=10135
* http://www.artspace.com/magazine/art_101/the-history-of-the-found-object-in-art
* https://www.princeton.edu/~achaney/tmve/wiki100k/docs/Found_art.html

[[!img IMG_20150729_212248222.jpg link="no"]]

[[!img IMG_20150917_155056133.jpg link="no"]]

[[!img IMG_20151011_120805641.jpg link="no"]]

[[!img IMG_20151018_134323801.jpg link="no"]]

[[!img IMG_20151024_151444660.jpg link="no"]]

[[!img IMG_20151024_151448844.jpg link="no"]]

[[!img IMG_20151025_115806395.jpg link="no"]]

[[!img IMG_20151025_115826764.jpg link="no"]]

[[!img IMG_20151209_193714495.jpg link="no"]]
