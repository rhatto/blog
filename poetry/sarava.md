[[!meta title="Saravá!"]]

Saravenho dizer que saravei por anos e que agora é o momento de dizer Saravá,
Adeus, Farewell, vida longa e próspera e obrigado por todo o peixe.

Saraventos agora são ventos que para mim sopram noutras direções.

Agradeço pelos anos sonhados juntos, por desejos compartilhados e pelo caminho
trilhado.

Levo comigo lembranças dos bons e dos maus momentos, dos sorrisos e lamentos.

Levo comigo um pouco da força que move o mundo. Me fiz nela e nela quase me
consumi.

Não há como dizer o quanto a experiência saravana me mudou.  Profundamente.
Para toda a vida.

Aqui não preciso dizer mais nada, ensinar mais nada, repassar mais nada.  Está
tudo aí. Que seja dádiva. Se for dívida, melhor desligar tudo.
