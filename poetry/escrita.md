[[!meta title="Na escrita"]]

    Se penso demais,
    Não escrevo
    Duvido da forma
    De cada letra;
    Se consigo percorrê-la
    No caminho do lápis,
    No vazio do papel
