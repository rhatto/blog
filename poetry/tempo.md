[[!meta title="Tempo"]]

    Tento coletar
    Resumir
    Expandir
    Dilatar
    
    Tudo o que sei sobre o tempo
    Mas isso não vai dar tempo
    
    Porque o tempo
    É inexorável
    
    Porque o tempo
    É a desmedida dos acontecimentos
    
    Não é o tempo que muda a gente
    É o tempo que a gente passa com ou sem as pessoas
    
    Ao essencial
    A vida é longa e dá tempo
    
    Ao supérfluo
    Curta a vida
    Tempo é ilusão
