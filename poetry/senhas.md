[[!meta title="Senhas"]]

Publicado originalmente em [Abre-te Sésamo: as senhas da nossa vida digital | Oficina Antivigilância](https://antivigilancia.org/pt/2015/06/abre-te-sesamo-as-senhas-da-nossa-vida-digital-2/).

    Senha forte
    Senha fraca
    
    Arranca a matraca da anta
    Repete demais as vogais
    
    Mergulho do murro boçal
    Tá cheio de encontro consonantal
    
    DV%y-%!I! \C1Xg1#a=-~a1uR1R"W\lJPJIWH? nDyqtC!!
    É palavrão de poesia malcriada!
    
    Senha é poesia
    E pode ser carente de rima!
    
    É poesia secreta
    Conhecida só por quem autora
    
    Estes versos não mais são
    Pois difundidos já estão
    
    E senha compartilhada
    Em geral é uma grande roubada
    
    Nem todo segredo é senha!
    
    Desabafos
    Confidências e conspirações
    Não são senhas
    Mas segredos por elas protegidos
    
    Senha é poesia
    Mesmo sem significar
    
    De preferência dadaísta
    Com palavras tiradas no dado
    Ou sorteadas da sacola
    
    Métrica escalafobética
    Desnumerológica
    
    De palavras inventadas
    Lúdica, brincalhona
    Uma ode à entropia
    
    Piada interna
    Que só você entende
    Que nem você compreende
    Mas que só você sabe
    
    Riovém de finício algum
    Prapilépricas quipergísticas
    Vitrola esmola?
    Vodininfantes!!!
    
    Enrolou tua língua?
    Então atrasa o lado da trairagem!
    
    Sendo arte do sigilo
    A regra de poesia é o estilo
    
    Só que estilo demais
    É padrão demais
    E entropia de menos
    
    A força de uma senha
    Vem da criatividade de quem poeta
    De usar e abusar do espaço da poesia
    Que é um lugar composto de símbolos
    Caracteres
    Palavras
    Versos e aglomerações maiores
    
    Este aqui é um desafio e um manifesto
    Reconhecendo um novo gênero literário
    Tímido, complicado
    Mas inclusivo
    
    Junte-se a este movimento!
    Seja poeta, poetiza de senhas
    Mas não divulgue suas composições do gênero!
    
    Se decorares alguns destes versos
    Serás capaz de memorizar tuas próprias senhas
    Ó, criptopanque!
    
    E o oponente
    Será capaz de deduzir nosso subconsciente?
