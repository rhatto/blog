[[!meta title="The travel lite strategy"]]

When travelling, people don't wan't to be worried about luggage. But very often
people make a huge bag with tons of stuff that may round the world without
seeing the sunlight, resting useless in the suitcase. People do that mainly
because they feel insecure about their trip and pack everything they consider
that will be useful.

If that's your case, first we recommend you to rely your travel safety in other
aspect of your planning. Second, we recommend that you carry just the mininum.
For "minimum" we mean really the minimum. Here comes our tip...

Imagine you're a sort of

[Paul Erdős](https://en.wikipedia.org/wiki/Paul_Erd%C5%91s) or [Julian
Assange](https://secure.wikimedia.org/wikipedia/en/wiki/Assange%5E) in the
sense that you're always travelling, moving around carrying just the barely
necessary for your existence.

If that's you case, we would like to help you with the minimal checklist.

Equipment

* Your documents and money.
* Notebook and pencil.
* Smartphone, USB charger, USB cable and headphones.
* Laptop and charger.
* Micro toilet kit: tootbrush, toothpaste and mini-shampoo/soap.

Special clothes for all seasons

* 3 underwear
* 2 socks

Special clothes for winter

* 2 warm underwear sets
* A jacket
* Polar fleece
* Gloves
* Hat 

Special clothes for summer

* 2 dry-fit tshirts

Wash your clothes while taking shower or use vodca or another spirit to wash. What else you need to have a good time?

See also
--------

Check the [Travel Templates](https://templates.fluxo.info/travel).
