[[!meta title="Salvador"]]

## Rolês

* [Toxic Tour](http://midianinja.org/news/assassino-invisivel-lixo-industrial-na-ilha-de-mare-chega-a-niveis-mortais/).
* Feira de São Joaquim: mergulho na Bahia profunda.
* Sorvete na Ribeira.
* Cine Glauber na praça Castro Alves.
* Restaurante Porto do Moreira.
* Candomblé.
* Pôr-do-sol:
  * Porto da Barra.
  * MAM / Solar do Unhão (ou umas das praias do lado).

## Praias

* Porto da Barra.
* Unhão e Gamboa.
* Buracão no Rio Vermelho.
