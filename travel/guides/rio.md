[[!meta title="Rio de Janeiro"]]

## Visiting

* Midiateca do [Maison de France](http://www.maisondefrance.org.br/) - Avenida Presidente Antônio Carlos, 58.
* [Museu do Amanhã](http://museudoamanha.org.br/pt-br):
    * Ingresso R$20.
    * Terça a Domingo, das 10h às 18h (com a última entrada às 17h).
    * Entrada gratuita às terças-feiras (o voucher deve ser retirado somente na bilheteria do Museu).
* Real Gabinete Português de Leitura:
    * Rua Luís de Camões, 30.
    * Segunda a Sexta-feira, das 9 às 18 horas.
    * [Real Gabinete Português de Leitura](http://www.realgabinete.com.br/portalWeb/).
    * [The Royal Portuguese Cabinet of Reading – Rio de Janeiro, Brazil | Atlas Obscura](http://www.atlasobscura.com/places/the-royal-portuguese-cabinet-of-reading).
* [Largo do Boticário – Rio de Janeiro, Brazil | Atlas Obscura](http://www.atlasobscura.com/places/largo-do-boticario).
* [Rio de Janeiro Cathedral – Rio de Janeiro, Brazil | Atlas Obscura](http://www.atlasobscura.com/places/rio-de-janeiro-cathedral).
* [Horários e facilidades | Biblioteca Nacional](https://www.bn.gov.br/visite/horarios-facilidades).
    * Visitas orientadas: segunda a sexta das 10:00 às 17:00.
    * Acervo geral, segunda a sexta, das 9h às 19h e sábado, das 10h30 às 15h.
* [S Riachuelo (S-22) – Wikipédia, a enciclopédia livre](https://pt.wikipedia.org/wiki/S_Riachuelo_(S-22)).
* Ilha Fiscal:
    * (21) 2532-5992 / 2233-9165.
    * De quinta a domingo às 12h30, 14h e 15h30.
    * Os ingressos serão vendidos no período das 11h às 15h10.
    * [Ilha Fiscal | DPHDM](https://www1.mar.mil.br/dphdm/ilha-fiscal).
    * [Ilha Fiscal – Wikipédia, a enciclopédia livre](https://pt.wikipedia.org/wiki/Ilha_Fiscal).
    * [Ilha Fiscal – Rio de Janeiro, Brazil | Atlas Obscura](http://www.atlasobscura.com/places/ilha-fiscal).
    * [Ilha Fiscal (Rio de Janeiro, Brazil): Top Tips Before You Go - TripAdvisor](https://www.tripadvisor.com/Attraction_Review-g303506-d552454-Reviews-Ilha_Fiscal-Rio_de_Janeiro_State_of_Rio_de_Janeiro.html).
    * [Espaço Cultural da Marinha | DPHDM](https://www1.mar.mil.br/dphdm/espaco-cultural-da-marinha).
* Cemitério São João Batista:
    * [Cemitério São João Batista](http://cemiteriosjb.com.br/): R. Real Grandeza, S/N - Botafogo.
    * [Cemitério São João Batista – Rio de Janeiro, Brazil | Atlas Obscura](http://www.atlasobscura.com/places/cemiterio-sao-joao-batista).
* Trilha Transcarioca:
    * [Trilha Transcarioca](http://trilhatranscarioca.com.br/).
    * [Mosaico Carioca: Trilha Transcarioca](http://mosaico-carioca.blogspot.com.br/p/trilha-transcarioca.html).
* Horto Florestal:
    * [Pobre sai, rico fica | Pública](http://apublica.org/2017/03/pobre-sai-rico-fica/).
    * [No Rio, comunidade fundada nos tempos da escravidão luta para ficar | Pública](http://apublica.org/2017/03/no-rio-comunidade-fundada-nos-tempos-da-escravidao-luta-para-ficar/).
* [Fábrica Bhering | Mapa de Cultura RJ](http://mapadecultura.rj.gov.br/manchete/fabrica-bhering).
* Observatório Valongo no Morro da Conceição: Ladeira Pedro Antônio, 43 | Centro - +55 21 2263-0685.
    * [Observatório do Valongo - UFRJ – Observatório do Valongo - UFRJ](http://www.ov.ufrj.br/en).
    * [Observatório do Valongo – Wikipédia, a enciclopédia livre](https://pt.wikipedia.org/wiki/Observat%C3%B3rio_do_Valongo).
* [Livraria Leonardo da Vinci - Livrarias - Av. Rio Branco, 185, Centro, Rio de Janeiro - RJ - Número de Telefone - Yelp](https://www.yelp.com.br/biz/livraria-leonardo-da-vinci-rio-de-janeiro).
* Parque das Ruínas:
    * [Parque das Ruínas (Oficial) | Facebook](https://www.facebook.com/parquedasruinas).
    * [Centro Cultural Municipal Parque das Ruínas (Rio de Janeiro) - O que saber antes de ir - TripAdvisor](https://www.tripadvisor.com.br/Attraction_Review-g303506-d311256-Reviews-Centro_Cultural_Municipal_Parque_das_Ruinas-Rio_de_Janeiro_State_of_Rio_de_Janeiro.html).
    * [Centros Culturais - prefeitura.rio](http://www.rio.rj.gov.br/web/smc/centros-culturais).
* Casa de Benjamin Constant.

## Weird

* [Enchanted Land - Terra Encantada – Rio de Janeiro, Brazil | Atlas Obscura](http://www.atlasobscura.com/places/terra-encantada).

    Suffering financial setbacks and false starts, the park finally opened its
    doors in 1998, but was soon plagued with a myriad of troubles. The bad luck
    streak included an actress being injured within the opening month, a 61-person
    riot, a street fight between jiu-jitsu fighters, and an employee strike. In
    June of 2010, after only 12 years in operation, a woman was thrown from the
    Mount Aurora ride and died from her injuries. The park was deemed unsafe and
    liable, and the owners were charged with manslaughter.

* [Bangu Statue of Liberty – Rio de Janeiro, Brazil | Atlas Obscura](http://www.atlasobscura.com/places/bangu-statue-of-liberty).

## Food

* Bagdá Restaurante.
* Amir Restaurante.
* Gohan.
* Colombo.
* Casa Manon.
* Leiteria Mineira.
* [Delirio Tropical](http://www.delirio.com.br/).
* [Restaurante Tempeh](http://www.restaurantetempeh.com.br/).
* [Refeitório RJ](http://refeitoriorj.com.br).

## Art

* [Escola Portátil](https://escolaportati.com.br).
* [Vera Holtz, Guilherme Leme Garcia e Flávia Pucci criam monólogo/depoimento “O Olho de Vidro” - dica de teatro](http://dicadeteatro.com.br/vera-holtz-guilherme-leme-garcia-e-flavia-pucci-criam-monologodepoimento-o-olho-de-vidro/).

## Nightlife

* Pedra do Sal.
* Bar Arco-íris.
* Armazém Senado: bom samba toda à sexta no final da tarde até umas 21:00.
* [Bar do Nanam | Facebook](https://www.facebook.com/bardonanam/?rf=114066822389364).
* [Leviano Bar](http://www.levianobar.com.br/): rola salsa!
  * [Salsa & Forró Leviano](http://www.levianobar.com.br/evento.php?id=1505&t=SALSA%20&%20FORR%C3%93%20LEVIANO&data=2017-03-16)

## References

* [10 Unusual Things to Do in Rio de Janeiro | Atlas Obscura](http://www.atlasobscura.com/things-to-do/rio-de-janeiro-brazil).
