[[!meta title="New York City"]]

Small tips from the big apple.

[[!img travel/guides/nyc/IMG_20160213_241056469.jpg link="no" caption="Teenage Mutant Ninja Turtles HQ entrance, NYC"]]

General
-------

* [Tenement Museum New York City - NYC Museum](https://www.tenement.org): 103 Orchard Street.
* [Interference Archive](http://interferencearchive.org).
* [The Buckminster Fuller Institute](https://bfi.org).
* [Woodbine](http://woodbine.nyc) social centre in Queens.
* [Speakeasy](http://en.wikipedia.org/wiki/Speakeasy) / [Prohibition-era Bars](http://www.theguardian.com/travel/2013/may/16/best-speakeasy-style-bars-in-new-york).
* [America in the 1970s: New York City](http://www.theatlantic.com/infocus/2013/07/america-in-the-1970s-new-york-city/100557/).
* [Radical Walking Tours of New York City](https://www.goodreads.com/book/show/255847.Radical_Walking_Tours_of_New_York_City).
* [America Black Chamber](https://en.wikipedia.org/wiki/The_American_Black_Chamber).
* Warriors:
  * [The Warriors Film Locations - On the set of New York.com](http://onthesetofnewyork.com/thewarriors.html).
  * [The Warriors Trip From Coney Island to Dyre Avenue in the Bronx Revisited](http://www.stonegreasers.com/greaser/conclay.html).
  * [Tracking 'The Warriors' through old New York, in 1979 and 2013 | The Verge](http://www.theverge.com/2013/5/23/4358656/quick-read-tracking-the-warriors-through-old-new-york).
  * [Following the Warriors - New York City Forum - TripAdvisor](https://www.tripadvisor.com/ShowTopic-g60763-i5-k5335704-Following_the_Warriors-New_York_City_New_York.html).

Shopping
--------

* [Tinkersphere](http://tinkersphere.com/).

Party / Music
-------------

* [Salsa NY](http://salsanewyork.com/).
* [Timeout - New York City](http://www.timeout.com/newyork).

Bars
----

* Cocktail: [Freeman's](http://www.yelp.com/biz/freemans-new-york-3): Lower Eastish Drink at the back bar or upstairs.
* The Ear Inn: 326 Spring Street - New York, NY 10013-1322.

Bookstores
----------

* Strand.
* Greenlight in Ft Greene, Spoonbill in Williamsburg at North 6th and Bedford.
* Printed Matter for zines and art books in Chelsea by art galleries, 196 on Hudson.
* Bluestocking Books - radical bookstore with good events.

Food
----

* [Brooklynvegan.com](http://brooklynvegan.com).
* [Restaurant Week](http://www.nycgo.com/restaurantweek).
