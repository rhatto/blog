[[!meta title="When shit happens"]]

This short history has many things to do with Europe decadence: basically our
luggage got lost in Frankfurt airport in the last December and our conection
fligh was cancelled. The whole airport was crowded, a lot of flights were
cancelled and there were just a minimum staff to help and recommend people to
get a train to their final destination.

"Ok", we said, "we get the train but then give us our luggage". The staff just
replied: "No luggage, the system is overloaded. Fill this form and we'll send
your luggage home".

Hell! We were at the airport, our luggage was lying there and we couldn't get
it! Also, sending the luggage back home was no deal. The companies didn't
realized that actually there were people travelling to and not just returning
from their trips.

In short, we spent 15 days in an extreme european winter without luggage.
That's why we adopted the [Travel Lite Strategy](/travel/lite).

It looks like in normal conditions everything works in Europe, while during
emergency situations people can't handle anything and start to mess things
around. In Brazil things looks quite the opposite: there aren't "normal"
conditions in the sense of everything working seamleslly. Everything always
looks like in a special conditions, but at least people can manage the
situation.

So it looks like a symptom of the crisis and substantial cuts the airway
companies had in the past two years. They fired a big part of their staff, a
measure that save money without a "substantial" loss of quality in their
service (read: without reaching the consumer's claiming threshould) during
normal conditions. But a measure that definatelly has disastrous consequences
during emergencies.

So, when travelling, always remember: companies are there for the sole purpose
of profit, they give a shit for you or for their staff. If they could they
would fire everybody, except you that is their source of wealth.
