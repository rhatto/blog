[[!meta title="Meta"]]
[[!meta date="2010-01-01 12:00:00-0300"]]

## Subpages

[[!inline pages="page(meta*)" archive="yes"]]

## What you're gonna do with your freedom?

I'm going to bootstrap exuberance.

## About

This is a mixed language and an one-cyborg-effort based on the work of the
broader [FLOSS](https://en.wikipedia.org/wiki/FLOSS) community. All the content
is here because it haven't found any other place yet to be sent. It's my
brain's downstream wishing to be upstream somewhere or staged content to be
sent to existing upstreams.

<!--
Fork this site and [check it's integrity](https://opsec.fluxo.info/meta)!
-->

    git clone https://git.fluxo.info/blog
<!--
    git -C blog verify-commit HEAD

And send patches ;)
-->

## License

Check [LICENSE](/LICENSE) for details.

## Contact

See [keys](/keys).
