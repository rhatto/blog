[[!meta title="As 1001 Cartas do Baralho OPSEC"]]

Traga seu cobertor de mendigo, sua bebida favorita e uma comida calórica pra suportar
o frio implacável numa madrugada em que contaremos histórias para nos manter com vida e com calor.

Vamos compartilhar pão, vinho e causos num piquenique noturno à beira dos Grandes Expurgos.

ATENÇÃO PANACAS: vai ficar pequeno pra quem ministrar Boa Noite Cinderela na moçada. Estamos de olho na qualidade do grogue.
