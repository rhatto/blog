[[!meta title="CriptoSalsa"]]

Expressão corporal como processo de desrobotização.

## Sumário

Sessão prática para dançar salsa em estilo livre! Dance só ou em companhia!
Afugente o tédio e a angústia balançando sua carcaça!

Não é necessário nenhum conhecimento prévio sobre dança, muito menos a ideia de
que para dançar é preciso já "saber" dançar.

## Descrição

Seguindo o exemplo do projeto [Tour Delírio](https://tourdelirio.com), esta é
uma iniciativa de re-aproximação entre música, dança e tecnologias de expressão
e socialização no contexto do capitalismo de vigilância que, além de coletar
informações, também induz comportamentos, reconfigurando não apenas o corpo
como principalmente o seu movimento.

Gleick já apontara, em seu livro sobre Informação o fato que "os tambores
`[africanos]` transmitiam informações. Em suas próprias culturas, em certos casos
um tambor podia ser um instrumento de sinalização, bem como o clarim e o sino,
usados para transmitir um pequeno conjunto de mensagens: atacar; recuar; ir à
igreja. Mas eles `[europeus]` jamais poderiam imaginar que os tambores falassem."

Tambores codificam informação muito antes de Morse ou Shannon terem seus
insights. A dança é uma resposta ao som do tambor e, em muitos casos, também
retroalimenta a execução musical nas situações de contato direto entre quem
baila e quem toca a percussão.

A dança, longe de ser uma manifestação puramente "emocional' (para quem opera
este tipo de divisão), também utiliza diversos recursos cognitivos de noção
espacial e de poliritmia que podem ser entendidos dentro do esquema de
agrupamentos de Piaget: é a partir da experimentação do sistema motor que surge
o pensamento abstrato, que posteriormente pode incidir de volta nas execuções
motoras.

Mas chega de bláblablá e vamos dançar! Nesta prática ninguém falará nada:
ouviremos música enquanto coreografamos de acordo com nossas percepções e
afetos, passeando do broacast (alguém propondo passos) ao multicast (todo mundo
se influenciando). Dialogaremos apenas usando nossos corpos.

## Notes

Necessário um local com chão liso porém não escorregadio e onde seja possível
tocar música em volume razoável sem causar nas outras atividades.

## Roteiro

    || Música                                       || Movimentações                                        ||
    || Wendell Rivera - Manhattan                   || Alongamentos: ombros, braços, plexo solar, pernas    ||
    || Richard Bona & Mandekan Cubano - Jokoh Jokoh || Ouvir e bater a clave                                ||
    || Idem                                         || Posição de dança européia versus afrocubana          ||
    || Idem                                         || Pés marcando o tempo                                 ||
    || Idem                                         || Idem, andando                                        ||
    || Idem                                         || Idem, em dupla                                       ||
    || Idem                                         || Idem, fazendo figuras diversas, em dupla             ||
    || Idem                                         || Pés marcando os três tempos e a pausa da salsa       ||
    || Idem                                         || Idem, andando pra frente e pra trás                  ||
    || Idem                                         || idem, andando pra frente e pra trás alternadamente   ||
    || Idem                                         || Passo básico da salsa, no tempo                      ||
    || Idem                                         || Passo básico lateral                                 ||
    || DJ Alexio - Primera                          || Improvisação múltipla                                ||
    || La Maxima 79 - Fabry's Swing                 || Improvisação múltipla                                ||
    || Rio Salsa - Mezcla                           || Improvisação múltipla                                ||
    || Papucho & Manana - Tu Papi soy Jo            || Improvisação múltipla                                ||
    || JG el Dueño del Party                        || Improvisação múltipla                                ||
    || Los 4 - Arrebatate                           || Improvisação múltipla                                ||
    || Los 4 - Lo Que Tengo Yo                      || Improvisação múltipla                                ||
    || Calle Real - Rompiendo Murallas              || Improvisação múltipla                                ||
