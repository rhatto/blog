[[!meta title="CryptoRave 2019"]]

Atividades realizadas na [CryptoRave 2019](https://cryptorave.org):

* [As 1001 Cartas do Baralho OPSEC](mil-e-uma-cartas).
* [CriptoSalsa](criptosalsa).

## BOM

* [x] Baralho mais recente.
* [x] Caixa de som.
* [x] Cabo de áudio.
* [x] Dispositivo com playlists.
* [x] Playlist Baralho.
* [x] Playlist Salsa.
* [x] Roteiro salsa.
* [x] Velas pretas, vermelhas etc.
* [x] Isqueiro.
* [x] Goró.
* [x] Gorro.
* [x] ~~Castiçais~~.
* [x] ~~Capa~~.
* [x] ~~Cartola~~.
* [x] ~~Charutos~~.
* [x] ~~Clave~~.
* [x] ~~Lenço de rumbero~~.
