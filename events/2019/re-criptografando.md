[[!meta title="Re-criptografando o presente - Lidando com o Invisível"]]

* Evento: [Re-criptografando o presente](https://centrodepesquisaeformacao.sescsp.org.br/atividade/re-criptografando-o-presente) | [Slides](slides.pdf)

<!--
## Mesa 1: Segurança digital - lidando com o invisível

Nossa bagagem instintiva está muito preparada para ameaças que nossos
sentidos podem captar. Mas quando lidamos com a comunicação mediada,
somente nossa percepção sensorial não é suficiente para encontrar
ameaças à nossa segurança.

Quais são os conhecimentos e técnicas adicionais que precisamos adquirir
para aumentar nossa segurança digital?

Quais são as boas práticas básicas na área?

## Mesa 2: Porque criptografia é importante?

De tempos em tempos, surgem propostas para enfraquecer a privacidade na
comunicação para supostamente evitar ou combater crimes. No entanto, por
detrás dos discursos de criminalização da criptografia existe uma
tentativa de aumentar a vigilância sobre as pessoas.

Como é construído este discurso? Por que ele traz perigos para todos
os setores da sociedade?

Quais são as ferramentas mais importantes hoje para a comunicação
segura?
-->

# Devemos confiar no visível?

![Aviso do WhatsApp sobre "criptografia de ponta-a-ponta"](images/whatsapp-aviso-sobre-criptografia.png)

# O que esta pequena afirmação significa? Criptografia...

                   grampo ativo ou passivo
                              |
                              |
    Emissor/a ---meio-------meio----------meio--- Emissor/a
    Receptor/a       --  mensagem-1 -->          Receptor/a
        1            <-- mensagem-2 ---              2


* Regra geral prática: se uma comunicação *pode* ser grampeada, assuma que ela
  está sendo grampeada. Assim fica mais fácil se prevenir *sempre.*

* Proteja sempre que possível a comunicação com criptografia.

# ... de ponta-a-ponta

* Use preferencialmente criptografia de ponta-a-ponta.

* Operações de criptografar/descriptografar acontecem no dispositivo da pessoa.

* As pessoas tem como confirmar o **número de segurança** da conversa com outra pessoa.

![ ](images/signal-safetynumber.png)

<!--
* Imagem: [Cybersecurity for the people - how to keep chats truly private with Signal](https://theintercept.com/2017/05/01/cybersecurity-for-the-people-how-to-keep-your-chats-truly-private-with-signal/).
-->

# Em quais lugares esta afirmação está inscrita?

Nas redes digitais, o próprio meio é um conjunto enorme de emissores/receptores, cada qual passível de grampo.

Em qualquer lugar no caminho da comunicação, do dispositivo emissor até o
receptor, as mensagens podem ou não estar criptografadas. Podem ou não estar grampeadas.

![ ](images/motoe-motherboard.jpeg)

<!--
Imagem: https://www.ifixit.com/Teardown/Motorola+Moto+X+Teardown/16867
Mods: https://web.archive.org/web/20160402005909/https://people.torproject.org/~ioerror/skunkworks/moto_e/
-->

# Esta afirmação é mesmo verdade?

* Não temos como dizer quando o **o código do aplicativo estiver fechado.**

* Mesmo que os dados estejam criptografados:

    * Os metadados podem não estar. Provavelmente não estão. E eles dizem muito...

    * Seu dispositivo pode estar comprometido e a comunicação vazar quando estiver decifrada.

    * Autoridades podem ser incluídas na conversa como mais uma das pontas cifradas.

Metadados? Dispositivo comprometido? Autoridades???

# Metadados: dados de atividade

      ___________
     |           |
     |           | -> quem fala com quem, quando...
     | metadados |    diz ao meio como enviar a mensagem,
     |           |    trata do endereçamento, arquivamento,
     |           |    do ciclo de vida da comunicação.
     |-----------|
     |           |
     |  dados    | -> o que é dito, isto é, a mensagem.
     |___________|

Também pode ser informação de uso: quanto tempo a pessoa usou o aplicativo etc.

# Dispositivo comprometido

Falhas de segurança:

* Por conta da concepção/design do dispositivo, do sistema e do aplicativo.

* Durante o processo de construção.

* Falta de atualização.

* Problemas de configuração.

Cada dispositivo tem vários pontos onde pode ser atacado. Para quem pode, há muita documentação sobre como protegê-los melhor.

# Autoridades

* [WhatsApp apresentou a Moro iniciativas para colaborar com a Justiça](https://www1.folha.uol.com.br/colunas/monicabergamo/2019/02/whatsapp-apresentou-a-moro-iniciativas-para-colaborar-com-a-justica.shtml). Não sabemos quais exatamente. Podemos imaginar...

* [PL 9808/2018](https://www.camara.leg.br/proposicoesWeb/fichadetramitacao?idProposicao=2169629) -
 "Acrescenta os parágrafos 5° e 6º ao art. 10 da Lei nº 12.965, de 23 de
  abril de 2014, para dispor sobre o acesso a dados de comunicação por meio de
  aplicativos de internet para fins de persecução criminal, nos casos que
  especifica".

<!--
* WhatsApp Security - https://www.whatsapp.com/security/
* WhatsApp Privacy & Terms - https://www.whatsapp.com/legal/
* [Metadata: Story Of How Whatsapp And Other Chat Apps Collect Data](https://fossbytes.com/whatsapp-chats-collect-data-metadata/).
-->

# Como assim?

* É possível implementar chave de custódia, sem violar a criptografia de
  ponta-a-ponta do aplicativo bastando adicionar um "destinatário interno"
  para qualquer comunicação de interesse.

* É possível, ainda, que a plataforma forneça os metadados às autoridades,
  via Marco Civil (art. 15 da Lei 12965/2019) e/ou Lei do Grampo (Lei 9296/1998).

* O WhatsApp passa a ser, além de disseminador relâmpago de informações falsas,
  o paraíso dos espiões computadorizados.

# O que seria isso, então?

![Oocistos espurulados, com quatro esporozoítos cada!](images/cryptosporidium.jpg)

<!--
* Imagem: https://www.cdc.gov/parasites/crypto/index.html
          https://www.businessinsider.de/cryptosporidium-parasite-swimming-pools-2017-5?r=US&IR=T
          https://www.nbcnews.com/health/health-news/more-100-sickened-arizona-cryptosporidium-parasite-outbreak-n635091
* https://en.wikipedia.org/wiki/Cryptosporidium
* https://pt.wikipedia.org/wiki/Cryptosporidium
* https://pt.wikipedia.org/wiki/Ciclo_de_vida_do_filo_Apicomplexa
-->

# Cryptosporidium

    Cryptosporidium é um gênero de protozoário apicomplexo
    que pode causar criptosporidíase, um tipo de diarreia
    que afeta humanos e outros animais. A transmissão dos
    oocistos é fecal-oral.  Pode ser prevenido filtrando
    ou fervendo a água antes de beber e cozinhando bem
    os alimentos, não ingerindo-os crus. 

    -- https://pt.wikipedia.org/wiki/Cryptosporidium

# Ilusões de Segurança

* Aplicativos fechados, que se anunciam como criptografados de ponta-a-ponta mas
  que coletam e processam metadados e que podem fornecer sua comunicação quando
  pressionados são **Cryptosporidiuns Digitais,** ocasionando a diarréia da sua
  comunicação!

* A doença pode ser prevenida com software, hardware e serviços livres que tenham
  criptografia forte, de ponta-a-ponta, bem implementada, auditada e com cadeia
  de produção mais justa.

* Mas estamos vivendo uma epidemia e as medidas preventivas não são facilmente
  aplicáveis pra qualquer pessoa... :(

# Imagens de Insegurança

![Que delícia! Mas tá cheia de salmonela!](images/maionese.jpg)

<!--
* Imagem: [Maionese de legumes com batata amassada](https://guiadacozinha.com.br/maionese-de-legumes-com-batata-amassada/) (não, não devia estar estragada :P)
-->

# Como (não) fazer visível o invisível?

![Campanha da Gangrena do Ministério da Saúde](images/campanha-da-gangrena.jpeg)

<!--
* Campanha estilosa, mas não tem tanta efetividade. As pessoas compram cigarro com a imagem menos agressiva, ou nunca olham para ela.
-->

# Não se pode esconder a cadeia produtiva

![Perigo invisível do fósforo ainda não riscado...](images/cigarettes.jpg)

<!--
* Imagem: [Cancer Society blasts tobacco export](http://www.stuff.co.nz/dominion-post/6640492/Cancer-Society-blasts-tobacco-export)
* Filme: [The Insider](https://en.wikipedia.org/wiki/The_Insider_(film)).
-->

# Choque de Realidade

![Recordista fuma 153 em 1983 usando um ventilador](images/full-of-cigarettes.jpg)

<!--
* Imagem: [Jim Mouth uses a fan to blow smoke from 155 cigarettes as he breaks “The Guinness Book of World Records” record November 18, 1993 for smoking the most cigarettes at the same time](http://www.vancouversun.com/story.html?id=6820722).
-->

# Um cigarro nunca é só um cigarro

![Campanha Tochas da Liberdade, do sobrinho de Freud, 1929](images/torches-of-freedom.jpeg)

<!--
* Imagem: [Torches of Freedom: Women and Smoking Propaganda](https://thesocietypages.org/socimages/2012/02/27/torches-of-freedom-women-and-smoking-propaganda/).
* [Sometimes a Cigar Is Just a Cigar - Sigmund Freud? Apocryphal?](https://quoteinvestigator.com/2011/08/12/just-a-cigar/).
* [Torches of Freedom](https://en.wikipedia.org/wiki/Torches_of_Freedom).
* Do livro [A Cidade Perversa](https://blog.fluxo.info/books/philosophy/cidade-perversa/), de Dany-Robert Dufour:

    Se Sade obviamente não é o organizador em carne e osso desse desfile, o fato é
    que seu espírito certamente presidiu a sua concepção. De qualquer maneira, não
    resta dúvida de que Sade se teria regozijado tanto mais diante de um tal
    acontecimento pelo fato de se dar na capital espiritual do império puritano.
    
    Vejamos agora de que maneira a necessidade desse acontecimento perverso se
    impôs num contexto tão puritano. Por um lado, é certo que fumar em público
    expõe uma mulher ao opróbrio geral (é o puritano que fala). Por outro, é
    lamentável, quando se é fabricante de cigarros, que a metade da humanidade, as
    mulheres, não tenha direito de fumar em público. Perde-se com isso muito
    dinheiro. Mandeville já dizia: “Um enorme comércio é feito graças […] ao hábito
    […] de fumar, que faz muito mal aos que o cultivam” (cf. fragmento 132). O
    comércio poderia, portanto, ser duas vezes maior se a outra metade da
    humanidade tivesse acesso a ele. Para isso, é necessário acabar com a ideia de
    que as mulheres que fumam em público são umas desavergonhadas. Cabe assim
    permitir-lhes fazer o que os homens fazem, e mesmo exortá-las nesse sentido —
    no caso, a fumar. Não é bom para a economia, em geral, e para a riqueza da
    nação americana que as mulheres não fumem, havendo tanto tabaco por vender
    nesse ano de 1929, o que alimenta a crise em formação.
    
    Com base em tais considerações, é que George Washington Hill, presidente da
    American Tobacco Co. (proprietária das marcas Lucky Strike, Pall Mall…),
    decide, em 1929, conquistar esse novo mercado, considerável. Hill contrata um
    certo Edward Bernays, especialista do que os americanos chamam desde então de
    Spin — manipulação de notícias, dos meios de comunicação, da opinião pública,
    dos sentimentos, dos afetos e outros elementos da doxa.  Acontece que Bernays é
    duplamente sobrinho de Freud179 — o que não é propriamente indiferente. Ele
    gostaria de consultar o tio, que, no entanto, está muito distante, e decide
    então procurar um de seus discípulos, o psicanalista Abraham Arden Brill, um
    dos primeiros a exercer essa estranha profissão nos Estados Unidos, fundador da
    New York Psychoanalytic Society. Brill explica a Bernays que o cigarro é um
    símbolo fálico representando o poder sexual do macho: se fosse possível ligar o
    cigarro a uma forma de contestação desse poder, as mulheres, de posse dos seus
    próprios “pênis”, fumariam.
    
    É assim que, a 31 de março de 1929, Bernays decide organizar um desfile de um
    grupo de jovens manequins na famosa Quinta Avenida, durante a New York City
    Easter Parade (o desfile de Páscoa), tomando antes o cuidado de avisar à
    imprensa que as belas jovens haveriam de acender torches of freedom. Elas
    estariam, portanto, representando estátuas da liberdade — o que em Nova York
    não é qualquer coisa. Diante da multidão de fotógrafos, e obedecendo a um sinal
    de Bernays, elas acendem suas tochas da liberdade: cigarros. No dia seguinte, o
    acontecimento é comentado em todo o território dos Estados Unidos e no mundo
    inteiro. A associação ilusória do cigarro com a emancipação da mulher é um
    enorme sucesso, e as mulheres começam a fumar com tanto maior empenho, na
    medida em que acreditam ter conquistado sua liberdade ao subtrair aos homens o
    pequeno falo portátil que até então era sua marca exclusiva.  157
    
    Eis então as mulheres liberadas — e, por sinal, muitas acreditaram nisso, e
    algumas continuam acreditando. É verdade que com isso elas também se viciam
    numa droga — leve, segundo se diz. Mas isso é um detalhe: a liberação não tem
    preço.
    
    Todo o espírito do novo capitalismo do consumo pode ser lido nesse ato
    inaugural de Bernays. Constatamos que ele começa por uma oferta de liberação
    feita ao consumidor, apresentada como algo que atenderia a uma demanda,
    eventualmente inconsciente. Entretanto, como o industrial que propõe seu objeto
    não age por filantropia, mas por interesse, a oferta só será realmente
    interessante para ele se puder provocar uma autêntica dependência. Não
    surpreende, assim, que tudo tenha começado com o cigarro, produto adictivo por
    excelência. Seria interessante, portanto, estudar mais atentamente as
    estratégias utilizadas pela indústria do cigarro com vistas ao estabelecimento
    da dependência, tanto mais que sua dinâmica foi transposta para outros produtos
    a partir do momento em que se descobriu quais as outras adicções
    industrialmente passíveis de serem exploradas eles poderiam acarretar — o que
    resultou no estabelecimento de uma especialização adictiva em que a publicidade
    desempenhou um papel essencial.180
-->

# Tecnologias do Vício são vendidas como Plataformas de Prazer

* A coleta de dados e metadados e a manipulação de usuários(as) consistem nos
  principais processos do Capitalismo de Vigilância.

* Se focarmos o debate apenas no campo estritamente tecnológico (e isso é
  possível?) ou político (na visão estrita de "ativistas políticos" demandando
  privacidade), deixamos de lado os processos mais importantes.

* Há uma campanha em curso para que cada pessoa seja mais um terminal conectado
  constantemente ao aparato de dominação.

# O Texto-Sombra

* Conceito-chave Shoshana Zuboff em seu recente livro "The Age of Surveillance
  Capitalism" (2019).

* O "texto principal" é aquele que consta na comunicação entre as pessoas e
  serviços. São as mensagens que trocamos.

* O texto-sombra (shadow text) é aquele que é produzido através da análise dos
  dados extraídos das pessoas, sejam dados, sejam metadados.

* O texto-sombra é inacessível às pessoas, mas está disponível aos
  administradores(as) das plataformas. Essa assimetria cria
  a "divisão do aprendizado" e estabelece uma distinção dura: "Quem sabe? Quem
  decide? Quem decide quem decide?"

* Zuboff argumenta que apenas adotar criptografia é insuficiente, apenas um
  remendo para um sistema que está quebrado em seu núcleo, a assimetria de
  conhecimento gerando desigualdades sociais.

# Perguntas em aberto

* Estratégia de Conscientização: como mostrar a realidade nua e crua sem
  provocar aversão, alienação e escapismo?

* É importante fazer campanhas não só alertando problemas, mas sobretudo
  disputando a narrativa. Mas quais são as narrativas que podem ser instigantes
  a ponto de contestar o Admirável Mundo Novo?

* Mostrar alternativas possíveis de viver são suficientes? E se a servidão
  continuar sendo mais fácil?

# Então é isso aí

* Perguntas?
* Contato: rhatto@riseup.net
