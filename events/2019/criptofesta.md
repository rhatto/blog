[[!meta title="Salve quem puder - o apagão de dados no Brasil"]]

* [Slides](https://slides.fluxo.info/salve-quem-puder).

## Referências

* Brasil:
    * [Água de Queimada](https://www.aguadequeimada.org/)
    * [Análises confirmam presença de partículas de queimadas maior do que o normal em água de chuva preta de SP | São Paulo | G1](https://g1.globo.com/sp/sao-paulo/noticia/2019/08/20/analises-confirmam-presenca-de-particulas-de-queimadas-maior-do-que-o-normal-em-agua-de-chuva-preta-de-sp.ghtml)
    * [Na Amazônia, a floresta está à venda | ISA - Instituto Socioambiental](https://www.socioambiental.org/pt-br/noticias-socioambientais/na-amazonia-a-floresta-esta-a-venda)
    * [Desmatamento | ISA - Instituto Socioambiental](https://www.socioambiental.org/pt-br/tags/desmatamento)
    * [Efeito Bolsonaro promove maior aumento anual do desmatamento neste século | ISA - Instituto Socioambiental](https://www.socioambiental.org/pt-br/noticias-socioambientais/efeito-bolsonaro-promove-maior-aumento-anual-do-desmatamento-neste-seculo)
    * [Sob Salles, ministério deixa 8 em 10 jornalistas sem resposta - ((o))eco](https://www.oeco.org.br/reportagens/sob-salles-ministerio-deixa-8-em-10-jornalistas-sem-resposta/)
    * [Manifesto Arquivista](https://arquivista.info/)
    * [A Grande Simplificação - Manifesto Arquivista](https://arquivista.info/simplificacao/)
    * [Desserviços ao conhecimento - Manifesto Arquivista](https://arquivista.info/desservicos/)
    * [ArchiveBot/2018 Brazilian general elections - Archiveteam](https://www.archiveteam.org/index.php?title=ArchiveBot/2018_Brazilian_general_elections)
    * [Acumule dados! - Manifesto Arquivista](https://arquivista.info/acumule/)
    * [Referências - Manifesto Arquivista](https://arquivista.info/referencias/)
    * [Place overview - Global Open Data Index](https://index.okfn.org/place/)
    * [http://paineis.cgu.gov.br/dadosabertos/index.htm](http://paineis.cgu.gov.br/dadosabertos/index.htm)
    * [Pesquisa sobre o uso das Tecnologias de Informação e Comunicação - TIC Governo Eletrônico 2017](https://cetic.br/publicacao/pesquisa-sobre-o-uso-das-tecnologias-de-informacao-e-comunicacao-tic-governo-eletronico-2017/)
    * [Bem vindo - Portal Brasileiro de Dados Abertos](http://dados.gov.br/)
    * [5d35dfbc9118a_Dados_abertos_e_meio_ambiente.pdf](http://www.imaflora.org/downloads/biblioteca/5d35dfbc9118a_Dados_abertos_e_meio_ambiente.pdf)
    * [Domínios Gov.br - Conjuntos de dados - Portal Brasileiro de Dados Abertos](http://dados.gov.br/dataset/dominios-gov-br)
* [Stasi - Wikipedia](https://en.wikipedia.org/wiki/Stasi):
  * [Stasimuseum Berlin in der Zentrale des MfS](https://www.stasimuseum.de/en/enindex.htm)
  * [Computers piece together millions of shredded Stasi documents / Boing Boing](https://boingboing.net/2008/01/22/computers-piece-toge.html)
  * [Piecing Together the Dark Legacy of East Germany's Secret Police | WIRED](https://www.wired.com/2008/01/ff-stasi/?currentPage=all)
  * [Solving a Billion-Piece Puzzle | WIRED](https://www.wired.com/2008/02/ff-stasi-ss/)
  * [Stasi files: The world's biggest jigsaw puzzle - BBC News](https://www.bbc.com/news/magazine-19344978)
* Regulações:
    * [Declaração Universal | ONU Brasil](https://nacoesunidas.org/direitoshumanos/declaracao/)
    * [OHCHR |](https://www.ohchr.org/EN/UDHR/Pages/Language.aspx?LangID=por)
    * [European legislation on open data and the re-use of public sector information | Digital Single Market](https://ec.europa.eu/digital-single-market/en/european-legislation-reuse-public-sector-information)
    * [Open Data Gov](https://www.data.gov/open-gov/)
    * [Text - H.R.4174 - 115th Congress (2017-2018): Foundations for Evidence-Based Policymaking Act of 2018 | Congress.gov | Library of Congress](https://www.congress.gov/bill/115th-congress/house-bill/4174/text#toc-H8E449FBAEFA34E45A6F1F20EFB13ED95)
    * [Decreto nº 8777](http://www.planalto.gov.br/ccivil_03/_ato2015-2018/2016/decreto/d8777.htm)
    * [Resolução n º 3, de 13 de outubro de 2017](http://conarq.arquivonacional.gov.br/resolucoes/654-resolucao-n-3-de-13-de-outubro-de-2017.html)
    * [D9756](http://www.planalto.gov.br/ccivil_03/_ato2019-2022/2019/Decreto/D9756.htm)
* Projetos:
    * [Perma.cc](https://perma.cc/)
    * [Internet Archive: Digital Library of Free & Borrowable Books, Movies, Music & Wayback Machine](https://archive.org/)
    * [Archiveteam](https://archiveteam.org/)
    * [Webpage archive](https://archive.is/)
    * [Common Crawl](https://commoncrawl.org/)
    * [End of Term Web Archive: U.S. Government Websites](http://eotarchive.cdlib.org/)
    * [End of Term Archive: Search Results](http://eotarchive.cdlib.org/search?f1-administration=2008)
* Livros:
    * [Um Cântico para Leibowitz – Wikipédia, a enciclopédia livre](https://pt.wikipedia.org/wiki/Um_C%C3%A2ntico_para_Leibowitz)
    * [Fahrenheit 451](https://en.wikipedia.org/wiki/Fahrenheit_451)
