[[!meta title="CryptoRave 2017"]]

Atividades realizadas na [CryptoRave 2017](https://cpa.cryptorave.org/pt-BR/CR2017/public/events):

* [Jogatina e Depravação com o Baralho OPSEC](baralho).
* [Brasil Hostil: como você vai dançar?](hostil).
* [Pequenos e densos datacenters autônomos](datacenters).
* [Autodefesa Digital e Educação em Segurança](autodefesa).
