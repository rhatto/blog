[[!meta title="Brasil Hostil: como você vai dançar? OPSEC em tempos de cólera"]]

* [Página do evento](https://cpa.cryptorave.org/pt-BR/CR2017/public/events/101).
* [Slides](slides) | [PDF](slides.pdf).

Mostraremos o cardápio disponível pra parar no xilindró neste Brasil
contemporâneo. Como rodar e quais são as cadeias disponíveis! Como evitar rodar
e passar incólume por este período e de quebra ainda ajudar os parcerias que
tão na berlinda.

Réu primário? Superior completo? Não tem problema! Existem opções para todas as
condições sociais!

A urubuzada de toga, os engravatados do Regresso Nacional e os concurseiros do
Sinistério Público já deixaram tudo pronto! E a polícia? Nem me fale!
Um tour pela Brasilônia.

    O cemitério está cheio desses heróis

    -- Gilmar Mendes

## Parte I: como você vai dançar?

* Panfleto vagabundo de guia turístico.
* Estamos num estado de exceção fodido.

### Sopa de Letrinhas

Arcabouço, ou calabouço regulatório:

* [Política Nacional de Inteligência](https://baralho.fluxo.info/?card=bUW6Bi4Zwdgrq7jv4gimU6), "documento de mais
alto  nível  de  orientação  da  atividade  de  Inteligência  no  País".
* [LOC, ou Lei das Organizações Criminosas](https://baralho.fluxo.info/?card=Si9P7burevxtFivewxSpBC), feita pra infiltrar, enquadrar e enjaular; a lei não é DAS organizações, mas PARA ferrá-las.
* GLO, ou Garantia da Lei e da Ordem: feita pra infiltrar e reprimir.
* LSN, ou Lei de Segurança Nacional, pra deixar um gostinho retrô!
* O Velho Código Penal, pra quem não se enquadrar no resto!
* OEs, vulgo Operações Extra-Oficiais! Dão conta de quem sobrar com ajuda de jagunços, detetives e paramilitares.

### Atores envolvidos

* OBAN 2.0: Elite agrária, financista e industrial relativamente unificadas na agenda do retrocesso.
* Sindicatos do Crime se realinhando: PCC aplicando um choque de gestão no ramo.
* Grupos de Extrema Direita em ascenção.

### Rodando

Oi Amiguinho! Atenção para as várias formas de se dar mal hoje em dia!

* Fenômeno da periferização do centro: o Estado de Exceção virando regra também no centro
  e aí vemos membros da classe média ou até desentendidos da própria elite entrando pelo
  cano.
* Enquadro: fuçando no seu celular.
* Grampolândia.
* Calúnia e difamação.
* A barbárie é cotidiana.

Há um time de primeira linha querendo te detonar!

Na prática, você não precisa se esforçar muito se quiser fazer um tour pelas cadeias.

### Casos exemplares

* [Com o caso Eduardo Guimarães, Moro atravessa o Rubicão](http://jornalggn.com.br/noticia/com-o-caso-eduardo-guimaraes-moro-atravessa-o-rubicao): liberdade de expressão e sigilo de fonte ameaçadas: casa invadida, dispositivos apreendidos, condução coercitiva ([mais](http://jornalggn.com.br/noticia/xadrez-dos-abusos-no-caso-eduardo-guimaraes)).
* [No caso Rafael Braga, depoimento da polícia basta | Brasil | EL PAÍS Brasil](http://brasil.elpais.com/brasil/2016/01/14/politica/1452803872_078619.html).
* [Como a primeira-dama foi chanteageada por um telhadista - Gizmodo Brasil](http://gizmodo.uol.com.br/marcela-temer-telhadista-chantagem-iphone/): condenação relâmpago e draconiana com imprensa censurada.

## Parte II: como você pode dançar?

* Seu testamento?
* OPSEC: Segurança Operacional.
* Modelagem de ameaças.
* Preparação:
  * O que fazer em caso de incêndio?
  * Rotas de fuga.
  * Preparação jurídica.
* Círculos de Solidariedade e Fundos Coletivos: Protosindicato trans-{gênero,categoria,etc}.

## Referências

* Pra frente Brasil:
    * https://cinemahistoriaeducacao.files.wordpress.com/2011/04/pra-frente-brasil01.jpg
    * https://cinemahistoriaeducacao.files.wordpress.com/2011/04/pra-frente-brasil-poster011.jpg
    * http://www.bcc.org.br/cartazes/cartaz_fb/025194
    * https://www.youtube.com/watch?v=rzj1_bD3BDI
* Brazil, o filme.
* Umanizzare: a galera dos subúrbios já roda desde sempre e vive a regra.
* Referências
  * [Psiquiatra ensina como lidar com as crianças que têm os pais presos na Lava Jato | Poder | Glamurama](http://glamurama.uol.com.br/psiquiatra-ensina-como-lidar-com-as-criancas-que-tem-os-pais-presos-na-lava-jato/).
  * [Jornal do Brasil - País - 'The Guardian': Problema da elite brasileira é explicar porquê papai está na cadeia](http://www.jb.com.br/pais/noticias/2016/12/21/the-guardian-problema-da-elite-brasileira-e-explicar-porque-papai-esta-na-cadeia/).
  * [Introducing Guiding Hands - CONAN on TBS](https://www.youtube.com/watch?v=v6Wpc9s35ZY).
  * [Militante do MST é executado no Vale do Rio Doce, Minas Gerais - Movimento dos Trabalhadores Rurais Sem Terra](http://www.mst.org.br/2017/04/24/militante-do-mst-e-executado-no-vale-do-rio-doce-minas-gerais.html).
  * [MonstruáRIO 2016 - YouTube](https://www.youtube.com/watch?v=9u7fwUPjTew).
  * http://apublica.org/vigilancia/
  * [11 anos em uma sentença: por que Rafael está preso? | CMI Brasil](https://midiaindependente.org/?q=node/262).
  * Zumbis: [Sobre a “Baleia azul” e as tecnologias da morte — CartaCapital](https://www.cartacapital.com.br/sociedade/sobre-a-201cbaleia-azul201d-e-as-tecnologias-da-morte).
  * [Bandidos pedem 'dinheiro digital' para libertar refém de sequestro - 03/05/2017 - Cotidiano - Folha de S.Paulo](http://m.folha.uol.com.br/cotidiano/2017/05/1880569-bandidos-pedem-dinheiro-digital-para-libertar-refem-de-sequestro.shtml#).
  * [Facção criminosa tenta dominar presídios do país todo - 01: 1 - Clube do Crime | Folha](http://temas.folha.uol.com.br/clube-do-crime/introducao/faccao-criminosa-tenta-dominar-presidios-do-pais-todo.shtml).
  * [Opera Mundi - Um ano de golpe: 10 fatos que provam que a vida dos pobres virou um inferno](http://operamundi.uol.com.br/conteudo/samuel/46997/um+ano+de+golpe+10+fatos+que+provam+que+a+vida+dos+pobres+virou+um+inferno.shtml).
  * [Rio de Janeiro toma conhecimento da guerra na Cidade Alta após cinco meses de confronto](https://theintercept.com/2017/05/02/rio-de-janeiro-toma-conhecimento-de-guerra-na-cidade-alta-apos-cinco-meses-de-confronto/).
  * [Gilmar critica procuradores da Lava Jato: "cemitério está cheio desses heróis" - Agência Estado - UOL Notícias](https://noticias.uol.com.br/ultimas-noticias/agencia-estado/2016/08/23/o-cemiterio-esta-cheio-desses-herois-diz-gilmar-mendes.htm)
