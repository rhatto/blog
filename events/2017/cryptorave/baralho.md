[[!meta title="Jogatina e Depravação com o Baralho OPSEC"]]

[Página do evento](https://cpa.cryptorave.org/pt-BR/CR2017/public/events/1)!

24 horas de carteado

Libera aí uma sala com mesas de pôquer pra rapaziada cachaçar!

Um ano após o lançamento do Baralho OPSEC, queremos proporcionar mais
diversão este ano com uma nova edição do jogo, com mais cartas e mais
possibilidades de comunhão com um mundo arredio em estado de crise.

Só um recadinho pra galera do vício: não pode entrar com arma, não pode
dar garrafada. Não é pra dar porrada, animal! Vamos descarregar nossa
selvageria e civilidade NAS CARTAS, não na pessoa que tá do lado.
Recado dado, valeu!
