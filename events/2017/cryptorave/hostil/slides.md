[[!meta title="Brasil Hostil: OPSEC em tempos de cólera: slides"]]
% Brasil Hostil: OPSEC em tempos de cólera
% "O cemitério está cheio desses heróis" -- Gilmar Mendes, Sinistro na Justiça
% -

Parte I: como você vai dançar?
------------------------------

![](images/01-irrealidade.jpg)

A Nova República de Weimar
--------------------------

![](images/02-cigarro.jpg)

Rodando
-------

![](images/03-choque.jpg)

Hermenêutica
------------

* PNI, ou Política Nacional de Inteligência - 2017
* GLO, ou Garantia da Lei e da Ordem - 2013
* LOC, ou Lei das Organizações Criminosas - 2013
* LSN, ou Lei de Segurança Nacional - 1983
* O Velho Código Penal - 1940
* OEs, vulgo Operações Extra-Oficiais - desde sempre!

Na prática...
-------------

![](images/04-prafrente.jpg)

Casos recentes
--------------

* Eduardo Guimarães.
* Rafael Braga.
* Operação Hashtag.
* Silvonei.
* Al Janiah...

Parte II: como você pode dançar?
--------------------------------

![](images/05-selfie.jpg)

OPSEC: Segurança Operacional
----------------------------

* Checklist de Segurança.
* O que pode dar errado? Como evitar?
* Preparação pessoal e coletiva.
* Checagem periódica.
* Não há receita sob medida.
* O que fazer em caso de diversas emergências?
* Preparação jurídica, econômica, informática, física, mental...
* Círculos de Solidariedade e organização coletiva.

TRETA
-----

Estágio inicial: https://opsec.fluxo.info
