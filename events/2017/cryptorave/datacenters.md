[[!meta title="Pequenos e densos datacenters autônomos: monte o seu!"]]

[Página do evento](https://cpa.cryptorave.org/pt-BR/CR2017/public/events/100).

Criptografia forte e software livre são bases essenciais para a privacidade
e o domínio da nossas informações. Mas onde esse monte de software vai rodar?

Podemos focar no controle dos nossos dispositivos pessoais e usar criptografia
de ponta-a-ponta, mas ainda assim dependeremos de uma vasta infraestrutura
controlada por terceiros para o transporte dos nossos dados. Atores estatais e
corporativos podem não apenas interceptar mas também impedir a nossa comunicação,
seja por censura ou pela disponibilização de um leque de serviços que transforma
a internet numa mera televisão interativa.

A tendência por centralização da rede se revela não apenas nos grandes serviços
de conteúdo, mas especialmente na concentração dos centros de processamento de
dados. A própria imagem da "computação de nuvem" já é uma falácia reveladora de
como a sociedade está sendo apartada dos processos computacionais.

Nesta fala encorajaremos a montagem de pequenos datacenters como hobby,
diversão e atividade política importante para a diversificação e resiliência
dos nossos sistemas computacionais.

Parafraseando 1984 em 2017, quem computa o passado controla o futuro; quem controla
o presente computa o passado.

## Roteiro

* TL;DR:
    * O que é?
    * Por quê?
    * Para quê?
* Como fazer?
    * Hardware:
        * Do raspberry ao rack: crescendo aos poucos.
        * Energia: nobreaks e geradores.
    * Connectividade:
        * DNS dinâmicos.
        * Hidden services.
        * IP fixo, zona reversa.
    * Segurança:
        * Criptografia de disco e partida manual.
        * Evil Maid.
        * Firmwares e backdoors.
        * Takedown notices.
        * Guarda de logs.
    * Software:
        * Virtualização e containerização.
        * Backups e recuperação de desastres!
        * Monitoramento e qualidade de serviço.
        * Atualizações.
        * [Serviços](https://blog.fluxo.info/services). Dica: rodando apenas os serviços que você utilizar aumenta a garantia de qualidade de serviço: se o serviço parar, você vai notar e tomar uma atitude.
    * Sociedade:
        * Fazer só ou coletivamente?
        * Compartindo recursos entre grupos.
        * Como se sustentar? Horizontal versus vertical.
* Kits!
    * Raspberry Pi nos sabores NAS, onionpi e web; exemplos de outros hardwares.
    * Arranjo intermediário: torre com nobreak, switch e appliance firewall.
    * Escalando: montando um rack, múltiplos upstreams, BGP.
    * Interligando: fazendo seus enlaces, redes wireless abertas.
* Referências: documentação espalhada por aí a ser sistematizada e expandida:
    * [TRETA — ISO 1312 - OPSEC 0.1 documentation](https://opsec.fluxo.info/).
    * [Providers' Commitment for Privacy (PCP)](https://policy.fluxo.info/).
    * [Provedor de Serviços de Internet - ISP](https://protocolos.fluxo.info/provedor/).
    * [Resource Sharing Protocol](https://rsp.fluxo.info/).
    * [Padrão Fluxo](https://padrao.fluxo.info/).

Com um aumento considerável de capacidade, de repente seu datacenter fica assim, como
a [Petabox](https://archive.org/web/petabox.php).
