[[!meta title="Autodefesa Digital e Educação em Segurança"]]

[Página do evento](https://cpa.cryptorave.org/pt-BR/CR2017/public/events/103).

Lançamento do Curso Online de Autodefesa Digital e da nova versão do Manual de Segurança.

Em parceria com a plataforma TIM Tec, desenvolvi um curso básico sobre segurança e privacidade
em sistemas digitais para todos os níveis de conhecimento. Aproveitei para atualizar o Manual
de Segurança com mais conceitos essenciais.

Gostaria de apresentar rapidamente ambos os materiais e partir para uma reflexão sobre os
desafios da formação e educação tecnológica com enfoque em segurança e privacidade e na
seguinte questão: como podemos formar mais formadoras(es), oficineiros(as) e o público
em geral para que o conhecimento se multiplique?

## Histórico

* 1999 - [Indymedia](https://indymedia.org) - tecnologias de segurança da informação dos anos 90: SSH e OpenPGP.
* 2002 - [Manual de Criptografia do Indymedia](https://criptografia.fluxo.info).
* 2003 - Oficinas de Segurança para grupos e movimentos sociais. Personal GNU/Linux Installer.
* 2012 - [Manual de Segurança](https://manual.fluxo.info), futuro Guia de Autodefesa.
* 2014 - [ISO 1312 - Modelagem de Ameaças e Especificações](https://opsec.fluxo.info).
* 2015 - [Baralho OPSEC - introdução do lúdico](https://baralho.fluxo.info).
* 2017 - [Curso e Guia de Autodefesa Digital](https://autodefesa.fluxo.info).

Nesse intervalo, muitos outros projetos também surgem! Exemplos:

* [Oficina Antivigilância](https://antivigilancia.org/).
* [Tem Boi Na Linha](https://temboinalinha.org/).
* [Guia de Protestos](https://protestos.org/).

## Desafios

Em 15 anos, deu pra perceber o seguinte:

* Princípios mudam mais lentamente: é viável manter documentação teórica.
* Implementações práticas mudam muito: roteiros, screenshots e screencasts se desatualizam em pouco tempo.
* As referências vão sumindo: amnésia da internet.
* Problema clássico da documentação: ela sempre está em baixa prioridade.
* Profusão de tutoriais e screencasts, porém falta um esforço sistematizador para dar coesão.

## Visão do tema

* Abordagem hierárquica da educação: obediência a esquemas, conhecimento como poder, adestramento.
* O que funciona melhor: ensinar os princípios, porque assim você forma galera replicadora que inclusive depois vai te ensinar.
* O que fiz no Guia de Autodefesa já foi feito noutras áreas e pode ser feito para difundir a capacidade
  de cada pessoa desenvolver seu conhecimento em tecnologia da informação e em ciência em geral.
* Ferramentas: o que é simples tende a durar: HTML, PDF, EPUB... sites estáticos em formatos universais como Markdown e RST.

## Debate

Após essa introdução, abrir pra um debate sobre o tema.

## Referências

* [Curso Online de Autodefesa Digital](http://cursos.timtec.com.br/course/autodefesa/intro/).
* [Guia de Autodefesa Digital](https://autodefesa.fluxo.info).
