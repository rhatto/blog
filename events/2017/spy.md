[[!meta title="Oficina de carteado espião"]]

Intro
-----

* Não tenho como esgotar esse tema, apesar de a partir de um determinado
  momento a variação ser tornar sempre no mesmo tema.

* Estou aqui só para atiçar a curiosidade!

De Sun Tzu a Balta Nunes: um cronológio
---------------------------------------

Espionagem analógica, manual, tête-à-tête:

* Registro mais antigo conhecido: A Arte da Guerra: [Sun Tzu: Capítulo 13 - Sobre o uso de espiões](https://suntzu-artedaguerra.blogspot.com.br/2007/09/captulo-13-sobre-o-uso-de-espies.html):

    Se não se trata bem os espiões, podem converter-se em renegados e trabalhar para o inimigo.

* Grandes guerras mundiais impulsionaram a espionagem.
* 1957: [Martin and Mitchell defection](https://en.wikipedia.org/wiki/Martin_and_Mitchell_defection).
* 1963: [Kim Philby](https://en.wikipedia.org/wiki/Kim_Philby) e os [Cinco de Cambridge](https://en.wikipedia.org/wiki/Cambridge_Five).

Espionagem digital, automática, de massa:

* Novidade! Portaria 85/2017: uso do Terminal de Comunicação Segura (TCS) fornecido pela Abin.

Os paradigmas nem sempre são esgotados. Eles se sobrepõem, se superpõe, se imbricam.

Arquétipos
----------

* Ian Fleming, James Bond: alto escalão espião.
* John Le Carré: baixo clero, os peões espiões.
* Snowden et al: o nerdão espião!

Narrativa
---------

* Como explicar a sanha de algumas pessoas pelo jogo da espionagem?
* Política, poder, economia e ideologia explicam grande parte e já existe muita discussão nessas linhas.
* Aqui, neste momento, vou propor um viés adicional: espionagem enquanto arte cênica, performática.
* Encenar também é jogar. Jogo vicia. Do _homo sapiens demens faber ludens_ olharemos para o ludens.
* Baralho: embaralhar: complicação, complexidade, confusão.

Teorias da conspiração
----------------------

Erros comuns:

* Conspirações improváveis.
* Complicação desnecessária para explicação de fenômenos (Navalha de Occam).
* Paranoia: inação E excesso de importância para si próprio.

No mundo complexo existem infinitas conspirações, infinitos enredamentos e
planos secretos, falas pelas costas, planejamento. Assim:

    A espionagem compõe a essência da política além do parlamento

* Parlamento: local da fala pública; política pura no sentido da "pólis" grega: lógica e retórica.
* Entreato: local onde as ações são planejadas; surge como consequência da existência de disputas políticas no âmbito público: local para fazer alianças e ganhar massa crítica em ações que se publicizarão no futuro: lógica, tática e estratégia.
* Espião: trafega entreatos: quebra conspirações obtendo informações vantajosas.

Quebrando a conspiração:

* Uma vez que você entrou, você sai?
* [Julian Assange: Conspiracy as Governance](http://www.mara-stream.org/think-tank/julian-assange-conspiracy-as-governance/).
* Conspirações são naturais e podem ser co-inspirações de um tempo de maturação antes de tornar uma ideia pública a ser disputada entre diferentes ideias. Mas podem ser prejudiciais quando trabalham _contra_ o público.
* Assim, o âmbito público fortalecido se defende de conspirações danosas e encoraja as benéficas. Como isso pode ser feito?

    Alcaguete no xadrez para na fábrica de sabonete

Referências
-----------

* John Le Carre. O espião que veio do frio. (romance, 1963)
* Leitura complementar: [Dossiê da Agência Pública sobre Vigilância](http://apublica.org/vigilancia/).
* [Grampolândia - A República da Escuta](https://grampo.org).
* [Baralho OPSEC / OPSEC Cardgame](https://baralho.fluxo.info).
* [Autodefesa Digital](https://autodefesa.fluxo.info).
