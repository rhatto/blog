[[!meta title="Criptografia e anti-vigilância | Semana de Mídias Livres"]]

- Link: https://espectrolivre.milharal.org/criptografia-e-anti-vigilancia/

Discussão de caráter técnico e político sobre o aparato existente de
espionagem e monitoramento, com uma introdução sobre os conceitos básicos
em segurança da informação.

Mais do que recomendar práticas e ferramentas seguras, esta oficina pretende
introduzir o "mindset" de segurança, para que as próprias pessoas ganhem
condições de avaliar suas salvaguardas.

Conceitos que serão abordados de modo relâmpago:

- O uso e a qualidade de senhas.
- Dados, metadados, anonimato e proteção da informação.
- Criptografia básica: chaves, impressões digitais e assinaturas.
- O uso e os perigos das redes sociais.
- Conexões e armazenamento criptografado.
- Segurança e software livre.

Prática
-------

- Verificando conexões criptografadas e outras melhorias de navegação
  com plugins para o Mozilla Firefox.

- Usando o Tor com o Tails ou o Tor Browser Bundle.

- Introdução ao OTR e ao OpenPGP.
