[[!meta title="Desafios da democratização hoje"]]

Seminário Comunicação e Democracia: experiências e desafios no Brasil contemporâneo
20 anos da Associação Imagem Comunitária - AIC

AIC: *"Nosso trabalho está ancorado no entendimento de que
     a inserção dos grupos marginalizados no espaço midiático é fundamental
     para o fortalecimento democrático e da cidadania."*

Bio
---

  Silvio Rhatto contribuiu com o Rizoma das Rádios Livres e com o Centro de
  Mídia Independente. Atualmente participa do Grupo Saravá. Autodidata em
  computação e diletante em ciências sociais.

Resumo
------

Abordarei a questão da infraestrutura de comunicação: como os grupos e
movimentos ainda tem muito o que fazer para controlar efetivamente os meios,
pará além das reformas legislativas e regulamentações necessárias (marco civil
da internet, proteção de dados pessoais, plano nacional da banda larga, nova
lei das rádios comunitárias, lei dos meios de comunicação, definição do padrão
de rádio digital, etc).

A simples criação de marcos regulatórios é insuficiente para atacar o problema
da comunicação, sendo importantíssimo também a criação de modelos de
financiamento e a apropriação da tecnologia.

O finaciamento determinará não só o grau de independência de cada veículo de
comunicação como também sua ingerência por órgãos de controle (pauta, proteção
de dados, censura prévia e retirada de conteúdo).

Já o domínio da tecnologia é o mais fundamental dos pontos e incrivelmente o
mais negligenciado do debate. A sociedade precisa ter capacidade de escolha dos
padrões e das tecnologias de comunicação.

Assim, não podemos restringir o escopo do debate apenas aos meios tradicionais
(mídia impressa, rádio analógico e televisão) mas devemos incluir os novos
meios (internet, rádio digital) levando em conta a convergência e a função
social de cada um deles.

Pretendo mostrar como o Brasil se encontra num contexto úbico para encaminhar
essas questões.

Pressupostos
------------

- Comunicação como requisito para a participação política.

- Política, do ponto de vista da comunicação, é a disputa pela audiência e luta
  pelo convencimentos sobre pontos de vista que determinam o rumo da sociedade.

- Como a computação está se transformando na base da comunicação contemporânea, tratar de
  participação política implica em falar sobre **soberania computacional**

- Soberania não do ponto de vista nacional, mas pessoal e popular.

- Democratização da comunicação: estamos falando de democratizar **meios** para
  atingir participação política plena.

- Curiosamente, ao se falar de *meios* a infraestrutura tipicamente fica de lado.
  Esta é uma fala tecnopolítica sobre infraestrutura e soberania popular.

Control freaks
--------------

Governos e empresas se unindo para manter os recursos computacionais e canais
de comunicação limitados, consequentemente limitando a possibilidade de
participação política:

- Legislação como barreira para a comunicação comunitária.

- Tendência ao favorecimento econômico dos oligopólios.

- Censura e desproporcionalidade de audiência.

- Arquiteturas de comunicação cada vez mais fechadas: protocolos mais estritos, quebra de neutralidade
  das redes, espectro radioelétrico sendo leiloado, portais cativos com interações pré-determinadas,
  guerra psicológica, etc.

Elementos
---------

- [As máquinas brechtianas](https://wiki.sarava.org/Estudos/MaquinasBrechtianas).
- [As máquinas brechtianas e o Marco Civil da Internet](https://anotador.sarava.org/p/marco-civil-autonomo).

Tecnologia
----------

- O aspecto tecnológico principal para a democratização das comunicações
  desde já é o domínio da computação e das redes de dados.

- Não estamos falando apenas de desktops e da internet como também de dispositivos
  móveis e redes de dados sem fio.

- Não se trata apenas da capacidade de transmissão e recepção em banda larga, mas também
  em preservação da **memória**

- Não podemos deixar nas mão de terceiros a construção da arquitetura dos sistemas, pois
  os interesses deles nem sempre serão os nossos.

A agenda
--------

- A agenda de democratização das comunicações é global, porém apenas recentemente
  descobrimos que podemos nos coordenar globalmente.

- Precisamos agora descobrir *como* fazer isso.

- Envolve um planejamento *prático* definido com prioridades e prazos para a criação
  de *plataformas abertas*

- Lida com várias camadas e segmentos da cadeia de comunicação, das mais simples e imediatas
  às mais sonhadoras e complexas. Exemplos: sistemas de transmissão e recepção de curto, médio e longo
  alcance; sistemas de servidores de dados; plataformas de redes sociais e compartilhamento
  de mídias; sistemas coletivos de gestão financeira e laboral.

Modelos de financiamento
------------------------

- Polêmica: quem paga a conta? Qual é a agenda de quem paga a conta?

- Como possibilitar o autofinanciamento?

- Soberania econômia anda junto com a soberania computacional: as liberdades dos cypherpunks
  (mobilidade, comunicação e interação econômica).
