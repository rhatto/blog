[[!meta title="Regulamentação, uso e compartilhamento do espectro"]]

ESC 2 - Espectro, Sociedade e Comunicação 2: O Rádio Digital no Contexto Brasileiro

Contexto
--------

Necessidade de se democratizar o acesso aos meios de comunicação, da realocação
de canais de Rádio, TV e telefonia móvel, possibilidade do uso de 20kHz para os
canais digitais na faixa do AM e 100kHz para VHF, quais são os pontos críticos
de uma política que  permita uma nova canalização e compartilhamento do
espectro.

Pressupostos
------------

- Comunicação como requisito para a participação política.

- Política, do ponto de vista da comunicação, é a disputa pela audiência e luta
  pelo convencimentos sobre pontos de vista que determinam o rumo da sociedade.

- Como a computação está se transformando na base da comunicação contemporânea, tratar de
  participação política implica em falar sobre **soberania computacional**

- Rádio digital: comunicação eletromagnética definida por software!

Control freaks
--------------

Governos e empresas se unindo para manter os recursos computacionais e canais
de comunicação limitados, consequentemente limitando a possibilidade de
participação política:

- Legislação como barreira para a comunicação comunitária.
- Tendência ao favorecimento econômico dos oligopólios.
- Censura e desproporcionalidade de audiência.

Regulamentação
--------------

[Lei 9472/97 - Lei Geral das Telecomunicações](http://www.planalto.gov.br/ccivil_03/Leis/L9472.htm).

    Art. 127. A disciplina da exploração dos serviços no regime privado terá
              por objetivo viabilizar o cumprimento das leis, em especial das
              relativas às telecomunicações, à ordem econômica e aos direitos
              dos consumidores, destinando-se a garantir:

    VII - o uso eficiente do espectro de radiofreqüências;

    VIII - o cumprimento da função social do serviço de interesse coletivo, bem
           como dos encargos dela decorrentes;

    IX - o desenvolvimento tecnológico e industrial do setor;

    [...]

    Art. 157. O espectro de radiofreqüências é um recurso limitado, constituindo-se
    em bem público, administrado pela Agência.

    [...]

    Art. 159. Na destinação de faixas de radiofreqüência serão considerados o emprego
    racional e econômico do espectro, bem como as atribuições, distribuições e consignações
    existentes, objetivando evitar interferências prejudiciais.

    [...]

    Art. 160. A Agência regulará a utilização eficiente e adequada do espectro, podendo
    restringir o emprego de determinadas radiofreqüências ou faixas, considerado o interesse público.

Ou seja:

1. Leis.
2. **Ordem econômica**.
3. Direitos dos consumidores.
4. Uso eficiente do espectro.

SBRD
----

[Portaria nº 290, de 30 março de 2010 - Ministério das Comunicações - Institui o Sistema Brasileiro de Rádio Digital – SBRD](http://www.mc.gov.br/acoes-e-programas/redes-digitais-da-cidadania/273-lex/portarias/25477-portaria-n-290-de-marco-de-2010):

    Art. 2o Para o serviço de radiodifusão sonora em Onda Média (OM) e em
    Frequência Modulada (FM) deve ser adotado padrão que, além de contemplar os
    objetivos de que trata o art. 3o, possibilite a operação eficiente em ambas as
    modalidades do serviço.

    Art. 3o O SBRD tem por finalidade alcançar, entre outros, alcançar os seguintes
    objetivos:

    I - promover a inclusão social, a diversidade cultural do País e a língua
    pátria por meio do acesso à tecnologia digital, visando à democratização da
    informação;

    II - propiciar a expansão do setor, possibilitando o desenvolvimento de
    serviços decorrentes da tecnologia digital como forma de estimular a evolução
    das atuais exploradoras do serviço;

    III - possibilitar o desenvolvimento de novos modelos de negócio adequados à
    realidade do País;

    IV - propiciar a transferência de tecnologia para a indústria brasileira de
    transmissores e receptores, garantida, onde couber, a isenção de royalties;

    V - possibilitar a participação de instituições brasileiras de ensino e
    pesquisa no ajuste e melhoria do sistema de acordo com a necessidade do País;

    VI - incentivar a indústria regional e local na produção de instrumentos e
    serviços digitais;

    VII - propiciar a criação de rede de educação à distância;

    VIII - proporcionar a utilização eficiente do espectro de radiofreqüências;

    IX - possibilitar a emissão de simulcasting, com boa qualidade de áudio e com
    mínimas interferências em outras estações;

    X - possibilitar a cobertura do sinal digital em áreas igual ou maior do que as
    atuais, com menor potência de transmissão;

    XI - propiciar vários modos de configuração considerando as particularidades de
    propagação do sinal em cada região brasileira;

    XII - permitir a transmissão de dados auxiliares;

    XIII - viabilizar soluções para transmissões em baixa potência, com custos
    reduzidos; e

    XIV - propiciar a arquitetura de sistema de forma a possibilitar, ao mercado
    brasileiro, as evoluções necessárias.

Extinção do AM
--------------

[Decreto nº 8139 - Dispõe sobre as condições para extinção do serviço de radiodifusão sonora em ondas médias de caráter local, sobre a adaptação das outorgas vigentes para execução deste serviço e dá outras providências](http://www.planalto.gov.br/CCIVIL_03/_Ato2011-2014/2013/Decreto/D8139.htm).

Como deveria ser
----------------

- Os fatos sociais precedem a regulamentação.

- Quando ocorre o contrário, barreiras emancipatórias são estabelecidas.

- Quando o fato social existe, regulamentações no sentido de limitá-lo enfrentam mais resistência,
  já que a sociedade será avessa à perda de liberdades.

- Assim, devemos nos apropriar da tecnologia de comunicação e criar os fatos
  sociais antes que eles sejam regulados para que o status quo de escassez seja
  mantido.

Poder
-----

* Rede Globo: poder político: [Marco Civil da Internet muda para atender demanda da Rede Globo](http://ultimosegundo.ig.com.br/politica/2013-11-06/marco-civil-da-internet-muda-para-atender-demanda-da-rede-globo.html).
* Teles e empresas .com: poder financeiro.

Uso eficiente da escassez
-------------------------

* Decreto de migração (ou extinção?) do AM.
* [Uso de frequências radioelétricas para ampliação da banda larga](http://nupef.org.br/?q=node/104).
* Redes mesh.

O Brasil e a oportunidade única
-------------------------------

O Brasil, por ser emergente e ter adotado tardiamente muitas tecnologias, está curiosamente numa posição singular para influir nos marcos regulatórios internacionais:

- Na ITU.
- Na ICANN.
- Na legislação internacional.

Grupo de Trabalho: Implementações abertas de rádio digital
----------------------------------------------------------

Incluindo: rádio digital nas distintas bandas de radiodifusão (OM, OT, OC e
VHF), canal de retorno e o rádio como meio de difusão de conteúdo digital.

Conclusão
---------

- Os fatos sociais precedem a regulamentação.

- Quando ocorre o contrário, barreiras emancipatórias são estabelecidas.

- Quando o fato social existe, regulamentações no sentido de limitá-lo enfrentam mais resistência,
  já que a sociedade será avessa à perda de liberdades.

- Assim, devemos nos apropriar da tecnologia de comunicação e criar os fatos
  sociais antes que eles sejam regulados para que o status quo de escassez seja
  mantido.
