[[!meta title="Cypherpunks: Liberdade, privacidade e o futuro da internet"]]

- São Paulo, 17/09/2013
- https://calendario.cc/pt-br/evento/liberdade-privacidade-e-o-futuro-da-internet

Roteiro
-------

Esta é uma fala sobre tecnologia e política e foi muito difícil escolher o que _não_ falar!

- O que está acontecendo?
- Do que se trata e como acontece?
- PRISM e os SpyFiles.
- Como chegamos a este ponto?
- Monitoramento e espionagem no Brasil.
- Impactos na sociedade.
- Alternativas HOJE.
- Alternativas futuras.
- Referências e contato.

O que está acontecendo?
-----------------------

- É como se os jornais tivessem se transformado em e-zines hackers dos anos 80 e 90!
  Como se voltássemos às cryptowars dos anos 90 ou se estivéssemos num revival da guerra fria.

- Incrível que no Brasil o debate ainda não tenha se ampliado, já que envolve não apenas
  soberania mas principalmente direitos civis, liberdade política e dependência
  econômica. Temos uma cultura muito permissiva à espionagem?

  Problema: pouco material traduzido para o português.

- O que a comunidade de segurança sabia ser _possível_ foi revelado como sendo _real_ .

Do que se trata e como acontece?
--------------------------------

- A falta de segurança é inerente às tecnologias de comunicação amplamente utilizadas.

- Comunicação digital ubíqua barateia a vigilância de massa, que torna economicamente viável 
  o armazenamento _a priori_ de conteúdo e/ou metadados de comunicação para eventual análise
  posterior.

Teoria da comunicação hacker
----------------------------

                      grampo ativo ou passivo
                                 |
                                 |
    Emissor/a ---meio----------meio-------------meio------ Emissor/a
    Receptor/a         -----mensagem-1------>              Receptor/a
        1              <----mensagem-2-------                 2

Nas redes digitais, o próprio meio é um conjunto enorme de emissores/receptores,
cada qual passível de grampo.

Anatomia da mensagem
--------------------

      ___________
     |           |
     | metadados | -> quem fala com quem, quando, etc: diz ao meio como enviar a mensagem.
     |-----------|
     |           |
     |  dados    | -> o que é dito
     |___________|


- A coleta dos dados permite a catalogação do que é dito, porém os dados podem se protegidos
  através de criptografia se aplicada corretamente.

- A coleta dos metadados permite a criação do "grafo" social, que é o desenho da rede
  de relacionamento entre as partes envolvidas na comunicação e é tão valiosa quanto a
  coleta dos dados. Os metadados podem ser criptografados, oferecendo um certo nível de
  anonimato à comunicação (por exemplo usando o Tor).

Tipos de ataques
----------------

- Man-in-the-middle, físico (fibra óptica, grampo telefônico tradicional) ou
  lógico (interceptação telefônica digital, escuta de tráfego de rede).

- Backdoors ("portas dos fundos") plantados nos softwares (mesmo nos livres e
  abertos!) ou até mesmo nas especificações dos protocolos!

O nível do ataque depende da capacidade do ator envolvido, o que tem relação
direta com o poderio econômico.

Que tal US$52.6 bi apenas em 2013?

O escândalo do PRISM e afins
----------------------------

- Timeline: http://virostatiq.com/interactive-timeline-of-the-prism-scandal/
- Atinge a comunicação em todos os níveis (MITM físico e lógico, backdoors e descriptografia).
- Colaboracionismo com provedores de conteúdo.
- Infiltração no desenvolvimento de softwares e protocolos.
- Influência incluse em parâmetros criptográficos!
- FISA: Trata-se de uma corte secreta, com normas secretas!
- Impulsionado com a Doutrina Rumsfeld e o Patriot Act.

Os SpyFiles
-----------

- http://www.wikileaks.org/spyfiles
- Brochuras e releases de fabricantes de equipamentos de vigilância de massa.
- Evidencia a mútua independência entre empresas e governos na espionagem.

Como chegamos a este ponto?
---------------------------

- Os "Five Eyes" e as agências de três letras ocidentais remontam ao final da segunda guerra mundial.

- Softpower: influenciar e espionar mentes e corações como estratégia da Pax Americana.

- Faz parte do plano pelo Novo Século Americano o expansionismo das empresas de
  comunicação da web 2.0 e sua cooperação com os aparatos de vigilância.

- "Transcript of secret meeting between Julian Assange and Google CEO Eric Schmidt"

- "The Banality of ‘Don’t Be Evil’"

Monitoramento brasileiro
------------------------

Também temos know how brasileiro de espionagem!

- O caso Serasa / Receita Federal: https://manual.fluxo.info/casos/serasa/
- Dígitro.
- SpyFiles: Telesoft, HackingTeam, Gamma Group.
- Redes sociais, grandes eventos.
- Vale do Rio Doce, Abin e movimentos sociais...

Impactos na sociedade
---------------------

- A espionagem está se tornando um padrão comportamental?
- Todos/as somos/as hackers.
- Cypherpunks: quem são eles/as?

Alternativas HOJE
-----------------

Para serviços de comunicação "gratuitos", vale o verso de Augusto dos Anjos:

    A mão que afaga é a mesma que apedreja.

Em qualquer situação:

  - É importante ter um conhecimento básico sobre o funcionamento dos sistemas
    de comunicação e sua relação entre usabilidade, segurança e vulnerabilidade.

  - Escolha aplicativos e procedimentos compatíveis com sua rotina e seu modelo
    de ameaças.

  - Nossas escolhas atuais terão impacto futuro!

  - Esforço em andamento: https://manual.fluxo.info / https://questionario.fluxo.info

Nível pessoal
-------------

- Parar de fornecer seus dados gratuitamente para empresas: skype, facebook, microsoft, google, etc.
- Evitar o uso de telefones móveis se hover preocupação com um dispositivo que monitora _por design_ .
- Snowden: "criptografia ponto a ponto, se feita com cuidado", ainda oferece segurança.
- Panopticlick.
- Tor Browser Bundle, Tails e VPN.
- Firefox: HTTPS Everywhere, Certificate Patrol, Convergence.
- Use software livre!

Nível coletivo ou institucional
-------------------------------

- Provedores radicais inspiradores: https://we.riseup.net/yellowpages
- Seu próprio provedor! :)
- https://rhatto.fluxo.info/services/

Nível nacional
--------------

- Fomento governamental: custo da banda e desenvolvimento de software.
- Marco Civil, Proteção de Dados Pessoais e outros marcos regulatórios.
- Legislação: seguir o exemplo da Islândia?

Nível internacional
-------------------

- Pressão!
- Necessário e proporcional: https://pt.necessaryandproportionate.org/text 

Alternativas futuras
--------------------

Possibilidades como o https://leap.se, que tentam atacar os sete grandes problemas da comunicação segura:

1. Autenticidade.
2. Proteção de metadados.
3. Comunicação assíncrona e _forward secrecy_ .
4. Comunicação criptografada entre um grupo.
5. Problema do recurso: como compartilhar um recurso de forma segura?
6. Disponibilidade: como sincronizar conteúdo entre dispositivos de modo seguro?
7. Problema da atualização: como realizar atualizações de software com segurança?

Referências e contato
---------------------

- https://oblivia.vc/pt-br/clipping
- https://links.fluxo.info
- rhatto@riseup.net
