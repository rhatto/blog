[[!meta title="Você sabe o quanto é espionado? E como evitar?"]]

- Você sabe o quanto é espionado? E como evitar?
- Silvio Rhatto @ Campus Party Brasil 7 #CPBR7 2014.
- [Página do evento](http://campuse.ro/social/resource/38713/view.cp).
- [Slides em PDF](slides.pdf).

Você sabe o quanto é espionado?
===============================

**TL;DR** - Ninguém sabe **o quanto** - podemos descobrir brechas de privacidade, mas **dificilmente
saberemos se não estamos sendo monitorados**

- O quanto conseguimos auditar das nossas vidas? Existem modelos de ameaça traçados?
- A segurança digital ou a falta dela está em vários niveis, da BIOS e microcode até a comunicação instantânea.
- Visão sobre privacidade tipicamente dada no nível individual, porém os problemas atingem populações inteiras!

Mas o que sabemos então?
========================

0wned!
------

* Você usa celular? Este primeiro é um dispositivo de localização e só depois um comunicador!
* Você usa X? Muito material disponível sobre sistemas e suas vulnerabilidades **conhecidas**

Paranoia Mode, ativar?
----------------------

* Por padrão, dados de comunicação são armazenados!
* O que não implica que necessariamente você seja um alvo de espionagem ativa.
* No entanto, dados podem ser guardados indefinidamente.
* Haja com naturalidade: assuma que, independente de espionagem, você deve se proteger.

Feliz 2014!
===========

<!--
    "This is the last free generation [...] The coming together of the
    systems of government and the information apartheid is such that none of us
    will be able to escape it in just a decade."
-->

    "Esta é a última geração livre [...] A chegada conjunta
     de sistemas de governo e o apartheid informational são 
     tais que nenhum(a) de nós será capaz the escapar deles
     em apenas uma década"
    
    -- Julian Assange, "Sysadmins do mundo, uni-vos!"
       http://is.gd/ZceGDz, tradução livre
    
<!--
    "A child born today will grow up with no conception of privacy at all. They'll
    never know what it means to have a private moment to themselves - an
    unrecorded, unanalyzed thought. And that's a problem, because privacy matters.
    Privacy is what allows us to determine who we are and who we want to be."
-->
    
    "Uma criança nascida hoje crescerá sem nenhum conceito de
     privacidade. Ela jamais saberá o que significa ter um
     momento privado para si mesmas [..] E esse é um problema,
     porque privacidade é importante. Privacidade é o que nos
     permite determinar quem somos e quem queremos ser."

    -- Mensagem de Natal de Edward Snowden
       http://vimeo.com/82686097, tradução livre

Infraestrutura
==============

- Nossos computadores são pequenos, leves e portáteis apenas porque o grosso do processamento está
  disperso em elementos que não controlamos.

- **Temos computadores, mas não temos os servidores. Nos comunicamos, mas não temos segurança.**

- Será que não é o caso de buscar soluções coletivas para a comunicação segura?

Perpectiva: o lado do servidor OU, como embaralhar os lados?
============================================================

- Muito se diz sobre como usuários/as podem se defender contra a vigilância de massa.
- Pouco se diz como eles/as também podem ser tornar provedores de serviços!

Faça você mesmo/a
=================

Serviços
--------

- Email, Jabber, Web, armazenamento de arquivos, VPN, etc.
- [Free and Open Source Services](https://rhatto.fluxo.info/services/): rhatto.fluxo.info/services (coming soon).

Implementações
---------------

- [Virtual Appliances](https://en.wikipedia.org/wiki/Virtual_appliance): Wikipedia Virtual_appliance (en).
- [FreedomBox](https://www.freedomboxfoundation.org/): freedomboxfoundation.org.
- [GNU Consensus](https://gnu.org/consensus): gnu.org/consensus.
- [LEAP Encryption Access Project](https://leap.se): leap.se.
- [Processo Hydra](https://hydra.fluxo.info): hydra.fluxo.info (coming soon).

Esforço necessário
==================

Fácil:

- Hardware barato: Raspberry Pi, MiniITX, etc.
- Muita documentação disponível!
- Muita configuração já disponível!

Difícil:

- Gargalos: dedicação, energia e banda. IPv6, neutralidade da rede e acesso ubíquo podem ajudar!
- Nível de serviço, backups e qualidade: tenha um nobreak e um disco externo! Se o sistema der certo, invista em redundância :)

Dúvidas?
========

- rhatto @ riseup.net / https://oblivia.vc
- https://seguranca.fluxo.info (coming soon)
- https://rhatto.fluxo.info/campusparty (coming soon)
