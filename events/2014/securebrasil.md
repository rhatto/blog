[[!meta title="LEAP: stack completo de comunicação segura"]]

Bio
---

Silvio Rhatto é um cypherpunk brasileiro e mantenedor do keyringer.pw,
um software para compartilhamento de segredos. Participa do sarava.org
e está iniciando o provedor oblivia.vc.

Descritivo
----------

O LEAP Encryption Access Project (htps://leap.se) é uma solução de
software aberto completa e automatizada para provedores de comunicação.

Atualmente oferece serviços de Proxy de Internet Criptografado (EIP,
vulgo VPN) e email de próxima geração.

As funcionalidades do LEAP serão abordadas, porém o foco da palestra é o
conjunto de escolhas técnicas que facilitam o desenvolvimento e a
auditoria da plataforma, o que é crucial para a sua difusão.

Por que o tema é importante?
----------------------------

Precisamos de novas abordagens para comunicação segura se quisermos
massificar tecnologia com alto nível de privacidade.

O LEAP é uma delas, disputando padronização e atacando problemas como o
da usabilidade. Podemos usá-la como modelo para outras iniciativas?

LEAP: stack de comunicação segura
=================================

Como promover privacidade **de massa** em tempos de paranoia?

Comunicação Segura - Problemas
------------------------------

* Gestão de chaves.
* Proteção de metadados.
* Assincronicidade com sigilo futuro.
* Comunicação em grupo.
* Compartilhamento de recursos e disponibilidade.
* Atualização e autenticação seguras.
* Facilidade de uso.
* Etc!!

https://oblivia.vc/pt-br/content/problemas-difíceis-na-comunicação-segura

LEAP Encryption Access Project
------------------------------

* Código aberto - https://leap.se
* Provedores federados, friend-in-the-middle, criptografia ponta-a-ponta.
* Foco na **comunicação** - autenticidade, usabilidade e proteção de metadados.

LEAP - Serviços e Cronograma
----------------------------

* VPN/EIP: navegação por proxy criptografado (implementado).
* Email de próxima geração (fase beta).
* Chat seguro (2015+).
* Compartilhamento de arquivos (2016+).
* Conferência de voz segura (2018).

LEAP - Bitmask Application
--------------------------

* Cliente com interface mínima.
* Funciona como um proxy local.
* Pode ser distribuído num "pacote" com cliente de comunicação integrados.

LEAP - Provider Platform
------------------------

* UX para DevOps!
* Suíte automatizada suportanto múltiplos provedores simultâneos.
* Simplifica a gestão de uma infra complexa e com requisitos fortes.
* Ciclo de desenvolvimento, provisionamento e deploying *turn key*.
* Pode ser usada conjuntamente com OpenStack ou soluções similares.

Como esse modelo pode ser aproveitado?
--------------------------------------

Características para uma plataforma *padrão* e *ubíqua*:

* Federalismo e escalabilidade: diversos atores participando de uma
  mesma rede (email X web 2.0).

* Código aberto: desenvolvimento por equipe internacional, auditorias
  e certificações locais, regionais, nacionais.

* Retrocompatibilidade, migração transparente, upgrades incrementais!

* Usuários/as: facilidade de uso sem comprometer segurança e privacidade;
  liberdade de escolha de provedor; dados sincronizados entre dispisitivos.

* Sysadmins: suíte de gestão reduzindo a barreira de entrada para novos
  provedores.

Muito diferente do modelo de silo das grandes redes sociais!

Modelos de negócio
------------------

* Ad free, sem mineração de dados, cobrança pelo serviço.
* Provedores comerciais, empresariais e administração pública.
* Auditoria, especialmente nacional.
* Soberania computacional: o que importa é o bare metal!
* Brasil: muito potencial no mercado interno e externo, mas custo de datacenter tem que cair!

Como participar?
----------------

* Testando em https://bitmask.net
* Discutindo e desenvolvendo em https://leap.se

Backup
------

* Com o LEAP, vaza dado do Itamaraty?
