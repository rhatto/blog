[[!meta title="Pílulas de Privacidade"]]

* http://antivigilancia.tk/pilulas
* https://anotador.sarava.org/p/arenamundial__cryptoparty

Hardware & Soberania computacional
----------------------------------

* A segurança se dá por camadas.
* Insegurança numa camada compromete as camadas acima.
* Hardware: a camada mais baixa!
* Trusted Computing: moeda com dois lados? DRM também limita.
* O que é um backdoor?
* Onde é mais barato implantar backdoor? Hardware ou software?

Referências:

* [Advogato: Sovereign Computing](http://www.advogato.org/article/808.html).
* [Spy agencies ban Lenovo PCs on security concerns](https://arquivo.sarava.org/conteudo/links.sarava.org/assets/32fd7fe29b621ed824ce76efeeb87429ab8d67f4/www.afr.com/p/technology/spy_agencies_ban_lenovo_pcs_on_security_HVgcKTHp4bIA4ulCPqC7SL.html).

Celulares e smartphones
-----------------------

1. Introdução: Celular é um tracking device; citação de chelsea manning na conversa com adrian lamo
1.1. Quais dados são coletados (imei, imsi, localização e outros metadados);
1.2. Plataformas existentes de smartphone: iOS, Android/CyanogenMod, Firefox OS, Replicant, etc
1.3. Como ter um celular anônimo

2. Suíte de APPs para smartphones
2.1. Guardian Project - orbot, chatsecure, pixelknot, obscura cam
2.2. Whisper System - textsecure
2.3. Fdroid x google play

3. Hardening de sistema operacional
3.1. Cyanogenmod e seus limites (integrado com o google)
3.2. Full disk encryption
3.3. PIN x Passphrase
3.4. OpenPDroid
3.5. "CryptogenMod": in kernel enable XTS, disable NFC, dev/mem, dev/kmem, "android"; then add OpenPDroid, remove CMota + CMuser + SSH.

4. Vulnerabilidades
4.1. Documentos revelados pelo Snowden que afetam iphone/android

Projetos que vale a pena olhar:

* Dark Matter - https://github.com/grugq/darkmatter
* Mobile Opsec - http://www.slideshare.net/grugq/mobile-opsec

Projetos comerciais:

* BlackPhone
