[[!meta title="Espionagem e liberdade nos meios digitais"]]

* Silvio Rhatto - Oficinas de Criatividade - SESC Pompéia 2014
* [Espionagem e liberdade nos meios digitais - Oficinas de criatividade - SESC Pompeia](http://oficinas.sescsp.org.br/curso/espionagem-e-liberdade-nos-meios-digitais).
* [PDF](apresentacao.pdf) ([Makefile](Makefile)).

<!--
## Chamada

Internet, cartões de crédito, telefones celulares, videogames, carros e TVs
inteligentes, câmeras de vigilância, catracas eletrônicas e identificação
biométrica. Todos esses elementos são dispositivos que guardam informações
pessoais digitalizadas, ou seja, rastros digitais.

Quem puder reunir e analisar esse conjunto de dados obterá informações sobre os
hábitos das pessoas e informações sigilosas, como nas espionagens de e-mails de
presidentes de vários países. Essa situação afeta diretamente a privacidade e a
liberdade, seja para o controle da vida privada ou para o monitoramento de
ações políticas.

Neste bate-papo será possível conhecer e entender os meandros da luta secreta
entre liberdade e controle no ambiente virtual e, com isso, vislumbrar
possíveis desafios e respostas que indivíduos ou grupos organizados podem dar a
essa situação.

Perpectiva: o lado do servidor OU, como embaralhar os lados?
------------------------------------------------------------

- Muito se diz sobre como usuários/as podem se defender contra a vigilância de massa.
- Pouco se diz como eles/as também podem ser tornar provedores de serviços!

-->

Pressupostos
============

Comunicação
-----------

- Requisito para a participação política e social.

Política
--------

- Disputa por audiência.
- Luta pelo convencimento para mudar o rumo da sociedade.

Computação
----------

- Base da comunicação contemporânea.
- Cypherpunks: aplicação da computação para problemas políticos.
- Participação política implica em **soberania computacional**

Política e Comunicação
======================

- Disputa por recursos computacionais: banda, armazenamento, processamento.
- O acesso a esses recursos facilita ou dificulta a invenção de novas formas de associação e comunicação.

Tensão
------

- Esses recursos estão se tornando ilimitados!!!
- Mas sistemas de controle tentam limitá-los a todo custo!!!

Control freaks
==============

Governos e empresas se unindo para manter os recursos computacionais e canais
de comunicação limitados, consequentemente limitando a possibilidade de
participação política:

- Combatendo com o P2P.

- Tornando alguma ponta fechada: arquitetura de sistemas, SaaS, PaaS, IaaS, quebra
  da neutralidade da rede, etc.

- Negociações fechadas (ACTA, TPP, TTIP), cortes e leis secretas (FISA).

- **"Se não conseguimos censurá-las, vamos espioná-las!"**

Feliz 2014!
===========

<!--
    "This is the last free generation [...] The coming together of the
    systems of government and the information apartheid is such that none of us
    will be able to escape it in just a decade."
-->

    "Esta é a última geração livre [...] A chegada conjunta
     de sistemas de governo e o apartheid informational é
     tal que nenhum(a) de nós será capaz the escapar deles
     em apenas uma década"
    
    -- Julian Assange, "Sysadmins do mundo, uni-vos!"
       http://is.gd/ZceGDz, tradução livre
    
<!--
    "A child born today will grow up with no conception of privacy at all. They'll
    never know what it means to have a private moment to themselves - an
    unrecorded, unanalyzed thought. And that's a problem, because privacy matters.
    Privacy is what allows us to determine who we are and who we want to be."
-->
    
    "Uma criança nascida hoje crescerá sem nenhum conceito de
     privacidade. Ela jamais saberá o que significa ter um
     momento privado para si mesmas [..] E esse é um problema,
     porque privacidade é importante. Privacidade é o que nos
     permite determinar quem somos e quem queremos ser."

    -- Mensagem de Natal de Edward Snowden
       http://vimeo.com/82686097, tradução livre

Paranoia Mode On?
=================

* Por padrão, dados de comunicação são armazenados!
* O que não implica que necessariamente você seja um alvo de espionagem ativa.
* No entanto, dados podem ser guardados indefinidamente.
* Haja com naturalidade: assuma que, independente de espionagem, você deve se proteger.

<!--
Infraestrutura
==============

- Nossos computadores são pequenos, leves e portáteis apenas porque o grosso do processamento está
  disperso em elementos que não controlamos.

- **Temos computadores, mas não temos os servidores. Nos comunicamos, mas não temos segurança.**

- Será que não é o caso de buscar soluções coletivas para a comunicação segura?

Arquitetura
===========

- Centralizada, descentralizada e distribuída.
- [A fancy table](https://leap.se/en/docs/tech/infosec).
- Por que não usar provedores comerciais?

Faça você mesmo/a
=================

Serviços
--------

- Email, Jabber, Web, armazenamento de arquivos, VPN, etc.
- [Free and Open Source Services](https://rhatto.sarava.org/services/): rhatto.sarava.org/services (coming soon).

## Implementações

- [Virtual Appliances](https://en.wikipedia.org/wiki/Virtual_appliance): Wikipedia Virtual_appliance (en).
- [FreedomBox](https://www.freedomboxfoundation.org/): freedomboxfoundation.org.
- [GNU Consensus](https://gnu.org/consensus): gnu.org/consensus.
- [LEAP Encryption Access Project](https://leap.se): leap.se.
- [Processo Hydra](https://hydra.sarava.org): hydra.sarava.org (coming soon).

## Esforço necessário

Fácil:

- Hardware barato: Raspberry Pi, MiniITX, etc.
- Muita documentação disponível!
- Muita configuração já disponível!

Difícil:

- Gargalos: dedicação, energia e banda. IPv6, neutralidade da rede e acesso ubíquo podem ajudar!
- Nível de serviço, backups e qualidade: tenha um nobreak e um disco externo! Se o sistema der certo, invista em redundância :)

-->

Soluções?
=========

* A espionagem é um problema comum.
* A solução precisa ser coletiva.
* Exemplo: documentação e educação: https://seguranca.sarava.org/chamado/

Dúvidas?
========

- rhatto @ sarava.org
- https://seguranca.sarava.org (coming soon)
- https://rhatto.sarava.org/campusparty (coming soon)
