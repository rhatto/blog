[[!meta title="Espectro livre, radio digital e rádio definido por software (SDR) - ESC 2011"]]

A grande sacada do SDR é justamente a separação entre o hardware e o software,
transformando o rádio num periférico de um sistema computacional.

Como segunda vantagem, é a capacidade de transmissão com menor potência.

Estamos falando apenas de renovar o rádio tradicional?
------------------------------------------------------

Não se trata apenas de emissão e recepção de música e notícias com "qualidade
digital".

  - Telefones móveis e outros computadores pessoais.
  - Sensoriamento remoto.
  - Veículos.
  - Infra-estrutura urbana.
  - Sensores neuronais!

A própria topologia de comunicação dos dispositivos pode ser repensada:

  - Uma espécie de "Internet das Coisas" radiofônica.
  - O modo de comunicação influencia e é influencida pela organização da sociedade.

Brasil
------

  - Proporções continentais
  - Resposta a emergencias além do rádio amador.
  - Handover / agnosticismo.
  - Mesh entre telefones moveis: Serval (BATMAN).
  - Prática da democracia direta sem represálias.
  - Situação da alocação de frequências:
    - 450MHz
    - 700MHz
    - 2,5 GHz e 3,5 GHz
    - Faixas pouco utilizadas

Quem vai financiar uma coisa dessas?
------------------------------------

Oportunidades de negócios: as novas indústrias sedimentam as anteriores, não
necessariamente matando-as mas impingindo grandes danos. A mídia impressa já
agoniza e tal fim pode ser similar para a indústria do broadcast.

Conglomerados com maior inércia (como a Velha Indústria Cultural) se o opõem
e forçam a barra o quanto for possível para frear mudanças sociais trazidas
por novas tecnologias. Algumas conseguem um movimento simultâneo de adequação
e conseguem se perpetuar.

  - Além dos negócios
  - Leilão do espectro
  - OpenSpectrum: abertura para inovação

O que perdemos com o rádio digital?

  - Simplicidade - http://wiki.radiolivre.org/Manuais/RadioDeTrincheira
  - Mas perdemos mesmo? O SDR pode receber e transmitir em FM e AM.

Mito do espectro
----------------

1. A limitação do espectro é um mito
2. Com o rádio digital, o espectro é ainda menos limitado

Alocação na gringa: https://en.wikipedia.org/wiki/Frequency_allocation

Regulamentação do SDR
---------------------

Regulamentação: por se tratar de um hardware padrão e produzido em massa, pode
ser avaliado e testado pelo órgão competente (como a Anatel) para evitar
interferências espectrais nas regiões do espectro em que os equipamentos podem
operar.

Assim, transceivers SDR de baixa e média potência podem ser disponibilizados no
mercado e o usuário final define sua aplicação.

Em menor medida, isso já ocorre hoje: ninguém pede concessão para usar Wifi e
não existem denúncias sobre interferência nessa porção do espetro.

Power to the people
-------------------

- [GRC - Página de Screenshots](http://www.joshknows.com/grc#screenshots)
- [GRC - Screenshot específico](http://www.joshknows.com/images/grc/grc_usrp_wbfm_recv.png)
- Repressão, ruído branco?

Topologias
----------

  - Broadcast
  - Mesh

Padrões
-------

  - AM, FM, DRM
  - Wifi, Wimax
  - BATMAN
  - GSM

SIGINT
------

SIGINT = Signal Intelligence

As mudanças são inevitáveis?
----------------------------

O discurso do inevitável se assemelha ao do fim da história. As mudanças podem surgir
da ação da sociedade.

Referências
-----------

- [Comunicação digital e a construção dos commons: Redes virais, espectro aberto e as novas possibilidades de regulação](http://www.fpabramo.org.br/o-que-fazemos/editora/livros/comunicacao-digital-e-construcao-dos-commons-redes-virais-espectro-ab-0)

- [OpenSpectrum](http://www.openspectrum.eu/drupal6/)

- [The Serval Project: Practical Wireless Ad-Hoc Mobile Telecommunications](http://developer.servalproject.org/site/docs/2011/Serval_Introduction.html)

- [Espectro no Brasil - Atribuição, Destinação e Distribuição de Faixas - Anatel](http://www.anatel.gov.br/Portal/exibirPortalRedireciona.do?codigoDocumento=247837&caminhoRel=Cidadao-Radiofreq%FC%EAncia-Atribui%E7%E3o%2C%20Destina%E7%E3o%20e%20Distribui%E7%E3o%20de%20Faixas)

- [Espectro no Brasil - Atribuição, Destinação e Distribuição de Faixas - Anatel](http://www.anatel.gov.br/Portal/exibirPortalRedireciona.do?codigoDocumento=98580&caminhoRel=Cidadao-Radiofreq%FC%EAncia-Atribui%E7%E3o%2C%20Destina%E7%E3o%20e%20Distribui%E7%E3o%20de%20Faixas)

- [Open Spectrum](https://links.sarava.org/tags/open%20spectrum)

- [Wireless](https://links.sarava.org/tags/wireless)

- [Espectro](https://links.sarava.org/tags/espectro)

- [Bionic Implants and Spectrum Clash](http://science.slashdot.org/story/11/11/23/2233247/bionic-implants-and-spectrum-clash)

- [Apresentação de Ara Minassian — Anatel](http://www.mc.gov.br/images/Documentos/SeminarioRadioDigital/32_AraMinassian_Anatel.pdf) no [Seminário de Rádio Digital - Brasília, 1º de setembro de 2011](http://www.mc.gov.br/apresentacoes-e-discursos/23730-seminario-de-radio-digital-1o-de-setembro-de-2011)
