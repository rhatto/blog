[[!meta title="CryptoRave 2018"]]
[[!meta date="2018-05-06 09:00:00-0300"]]

Atividades realizadas na [CryptoRave 2018](https://cryptorave.org):

* [Contos da Crypto - Capítulo Intervenção Militar](cardificina).
* [Tendências Brasil Hostil para a Década de 20](hostil), ou mensagem da ratazana para as nossas gerações.
