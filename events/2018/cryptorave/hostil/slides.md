[[!meta title="Tendências Brasil Hostil para a Década de 20 - Slides"]]
[[!meta date="2018-05-06 11:00:00-0300"]]

    "E essa agora que o General da intervenção disse que o
     Rio de Janeiro é um Laboratório para o Brasil?
     E nós somos as cobaias??? Absurdo!"

    -- Marielle Franco, em mensagem de 27/02/18

    "E eu sei quem são meus amigos eternos e quem são
     os eventuais. Os de gravatinha, que iam atrás de mim,
     agora desapareceram."

    -- Lula, em seu discurso de 07/04/18

# Recapitulando, mas sem capitular

<!--
qrencode https://blog.fluxo.info/events/2017/cryptorave/hostil/slides.pdf \
         -o ../images/link-ano-passado.png -s 4
 -->

![https://blog.fluxo.info/events/2017/cryptorave/hostil/slides.pdf](../images/link-ano-passado.png)

# Como estávamos?

<!-- Quem viu as palestras dos anos anteriores? -->

* 2016: Brasil Hostil - Circuito Grampolândia: a República da Escuta:
  vigilância generalizada - https://grampo.org.

* 2017: Brasil Hostil - Etapa República de Weimar: efervescência política e
  avanço do arcabouço jurídico da repressão.

<!-- PNI, GLO, LOC, LSN, Lei Anti-Terror, Código Penal, Operações Extra-Oficiais... -->

Falamos sobre OPSEC - Segurança Operacional, dando ênfase na preparação para
pessoas e grupos enfrentarem as dificuldades da vida e estarem à frente da
repressão e desestabilização social.

<!--
 Onde aquela palestra termina, esta começa: Círculos de Solidariedade o organização coletiva.
 -->

# Como estamos?

* 2018: Brasil Hostil - Edição Intervenção Foderal.

* Fim de muitos ciclos aqui e no mundo. Talvez até de grandes períodos sob a
  égide do Iluminismo ou do Renascimento.

* Mas chega de prosa. Bora dar um giro?

# Attention! Achtung! Attenzione! Atenção!

![Recadinho do General para turistas](../images/baderna.png)

# Mini-tour Brasil Hostil 2018 - Bordel

![Ritual dos Sádicos no clube hedonista "Bahamas"](../images/bahamas.png)

<!--
 Parece uma cena de enforcamento pré-Thermidoriana.
 -->

# Mini-tour Brasil Hostil 2018 - Personalidades

![Ministro da Defesa reverenciando o General](../images/beijo-chefao.jpg)

# Mini-tour Brasil Hostil 2018 - Emboscadas

![Marielle: morte sem ameaças prévias](../images/carro-marielle.jpeg)

<!--
 Essa cena me lembrou do fusca do Marighella.
 -->

# Mini-tour Brasil Hostil 2018 - Moda

![Trench Coat Militar](../images/trench-coat.jpg)

# Mini-tour Brasil Hostil 2018 - Polvo

![Kit Palhacitos](../images/tuiuti-pato-fantoche.jpg)

<!--
 Fantasia ou realidade?
 Vizú de carnaval tirando um côco da imbecilidade
 -->

# E o mundo em geral, a quantas desanda?

![Zombie Nation](../images/facebook-zombies.jpeg)

# Tendências para a Década de 20

Em bom protuguês:

<!-- * Um FEBEAPÁ pornográfico. -->
<!-- Tem gente muito mais informada do que eu pra dar um diagnóstico melhor. -->

* Elite local tentado dar o salto para se tornar elite global via
  privatização extrema e entreguismo; indícios de forte parceria entre o crime
  organizado do andar de cima e do de baixo (facções); e por aí vai.
  Um processo cheio de ruídos.

* Estado de exceção permanente, Terror, necropolítica, pós-realidade.
  Muita gente vai rodar.

* Fascismo de consumo em momento de recessão econômica.

* Falta de narrativa que dê sentido à existência frente ao abismo.

* Choque, luto, depressão, paralisia.

<!-- Momento pouco propício para que a mobilização se amplie. -->
<!-- Vivi isso antecipadamente. -->

# Então fodeu?

* A canoa emborcou. Pode ser que amarguemos alguns anos até a próxima iteração.

* Protestos e mobilizações passam por uma fase de esgotamento e
  insuficiência.

* A janela histórica pode se abrir havendo comoção massiva.

<!--
* Este século pode ser decisivo para o futuro da humanidade.
  Possibilidade de obsolescência incluindo o humano. Ainda não sabemos.
 -->

* Enquanto isso, estamos mais ou menos que por conta. Enquanto isso...
  bom... havendo vida há esperança.

# Por onde (re-)começar?

* O que a gente faz depois de sobreviver a uma avalanche?

* Momento de estudo, maturação, fortalecimento, solidariedade, organização e luta.

* A gente começa pequeno e cuida do básico essencial. Fazendo bem, a parada
  cresce.

* No passado falamos muito de _segurança._ Agora nossa ênfase será
  na ideia de _organização._ E falaremos das organizações _solidárias._

# Solidariedade: egoísmo e altruísmo

O fundamento da solidariedade é a **tensão** dinâmica entre egoísmo (cuidar de si,
receber cuidados) e altruísmo (cuidar de outrem, aceitar os cuidados de outrem).
A isto chamaremos de _ajuda mútua._

![Afresco egípcio sobre segurança aérea, séc. XX D.C.](../images/egiptian-safety-sheet.png)

<!--
Essa figura é curiosa. Note que o cuidado é associado a um papel feminino.
Ao mesmo tempo, o estado frágil, desprotegido e sem máscara, é caracterizado
pela roupa na cor rosa, enquanto que no estado de salvação a roupa passa a ser
azul, uma cor na época usualmente associada ao papel masculino. Essa civilização 
possivelmente tinha muitos problemas relativos à atribuição de papéis a gêneros.
-->

<!-- Sacos de vômito também são bem-vindos. -->

"Solidariedade" aqui não será entendida no termo clássico, de solidez e apenas "soma
do todo". Será mais *fluido:* a relação local/global.

<!--
O problema do termo "solidariedade": sólido, solidez, "soma do todo": visão
eminentemente globalista, totalizante.  Aqui iremos usar o termo solidariedade
mais no sentido da ajuda mútua: relações locais entre pessoas e grupos que
compõem um tecido social global.
-->

<!-- 
Então, primeiramente... cuidar da dor. Fazer o luto. Cicatrizar. Saber o quanto é
preciso estar bem para pode ajudar. Aceitar ajuda.

A via é de mão dupla e o puro antagonismo entre os termos não nos serve.
Os vínculos _sólidos_ podem ao mesmo tempo serem fluídos. Fluidariedade.

Diferenciar ajuda mútua do conceito liberal de "liberdade, igualdade e fraternidade".

Diferenciar ajuda mútua de Esquema Ponzi:
http://www.dinheiroganhar.net/ajuda-mutua-e-crime-saiba-a-verdade/
 -->

<!--
# Bem-estar

* Os afetos _tristes_ reduzem nossa capacidade de ação. Estar junto favorece
  os afetos _alegres,_ fundamentais para a mudança.

* É incrível como às vezes a gente consegue ajudar mesmo estando em frangalhos.
  Outras vezes não.

* O Duplo Movimento de renúncia e libertação: tirar o peso do mundo das costas
  para poder mudar o mundo. Poder respirar primeiro para assim poder mergulhar de novo.

* Os múltiplos movimentos: a dificuldade humana de relacionar escalas: micro-macro,
  temporalidades, local-global, aqui-e-agora etc.
 -->

# Os movimentos e os caminhos

<!--
* Esta é uma proposta para ajudar no caminho de hoje. Utopia vista como narrativa
  de onde podemos chegar que nos ajuda a andar. Não como ponto de chegada.

* Pode ser que o trampo deste geração seja preparar o terreno para a próxima...

* Pode ser que precisamos olhar para gerações (iterações) passadas e checar
  alguns detalhes que passaram batidos...
 -->

* A ajuda mútua pode ser a orientação básica para praticar e propagar
  formas de existência mais dignas.

* Mas apenas essa orientação é insuficiente. Apenas a prática espontânea
  da ajuda também é insuficiente.

* É necessária uma solidariedade além do meramente espontâneo e que gere algum
  acúmulo, um ganho organizativo que permita um salto, mesmo que lá na frente.

* Precisamos de solidariedade _organizada._

# O que fazer?

* Poderíamos buscar teorias progressistas para repensar no que fazer.

* Mas aqui será proposta uma rota alternativa: voltar para um período
  de resistência ao totalitarismo e descobrir no que ele pode nos ajudar.

* Começar dentro do possível para chegar ao impossível. Mas antes
  de falar da nossa década de 20, falemos sobre as décadas de 30
  e 40 do século passado.

<!--
* Mesmo porque a própria esquerda partidária tem falado sobre
  criar uma frente antifascista...
 -->

# Rebobinando...

    "Quando os nazistas levaram os comunistas, eu calei-me,
     porque, afinal, eu não era comunista.

     Quando eles prenderam os sociais-democratas, eu calei-me,
     porque, afinal, eu não era social-democrata.

     Quando eles levaram os sindicalistas, eu não protestei,
     porque, afinal, eu não era sindicalista.

     Quando levaram os judeus, eu não protestei, porque,
     afinal, eu não era judeu.

     Quando eles me levaram, não havia mais quem protestasse."

    -- Martin Niemöller

# Naquela época...

* O contexto tinha suas semelhanças.
* Estávamos muito na merda. Fascismo crescendo. Totalitarismo.

No meio da bagunça aparece um estranho personagem...

# Quem é este figura?

![Apenas mais um gravatinha?](../images/carmille.jpeg)

# A nossa cartada

![Agente secreto da Rede Marco Polo](../images/card.png)

# Naquele tempo, eram outros Cards

![Cartão Perfurado](../images/punchcard.jpg)

<!-- Este modelo de cartão perfurado aparentemente é de um período posterior. -->

# SIGSTOP, SIGKILL, HALT

Carmille operou em duas temporalidades:

1. Urgência, curto prazo: salvar quem estava na berlinda. E fez isso em larga escala
   apenas parando a maquinaria sem que os alemães soubessem. A ação
   passiva Zen, a não-ação, desobediência.

2. Organização, médio e longo prazo: tentar mobilizar o exército de reserva francês para
   ações futuras da Resistência.

Preso, torturado e depois enviado pro saco, não concluiu a segunda etapa do plano.

# Uma lição dos tempos

* Aproveitar a nossa condição, usando que temos disponível.
* Não precisamos de super-heróis mártires: há um pouco a fazer para cada pessoa.

Em especial, descobrimos que a solidariedade, e não apenas a dominação, também
é um empreendimento logístico-informacional!

# As máquinas sociais dominadoras e as solidárias

* Maquinário da Dominação: cartão perfurado, registro e rastreamento: mesquinharia
  contábil para controle, dominação e eliminação das pessoas. E dessa contagem que
  nasce a economia muquirana baseada na escassez. A questão de fundo é o medo egoísta
  da morte que exige extrair a vida de outrem a todo custo para tentar interromper o
  processo vital.

* Na maquinaria solidária, as vidas estão entrelaçadas, de modo que uma pessoa
  tem sua liberdade enquanto outra também tiver. Estará bem conforme a outra também
  estiver.

<!--
# Tensionando

Estas são conceituações de máquinas "puras", "perfeitas". Na prática, no caos da vida,
as máquinas sociais reais são uma mistura desses dois modelos arquetípicos, com maior
ou menor presença de solidariedade e dominação.

Existe uma tensão permanente entre egoísmo e altruísmo que se manifesta no
diagrama, ou máquina social.

Lutar por um mundo melhor implica em mudar o balanço social em direção a uma maquinaria
solidária.

Encontrar sua faixa de operação solidária consiste em achar a região de equilíbrio
entre cuidar de si e cuidar da outra pessoa.

# As máquinas da dominação

Sua violência pode ser distinguida entre

* Psicológica (enganação): vulgo "softpower", "me engana que eu gosto".
* Física: opera quando a enganação já não surte efeito.

Sempre operam em par, porque não é possível enganar nem bater em todo mundo ao
mesmo tempo; bate-se em uns enquanto diz-se pros outros que eram a raíz de
todos os males, que mereciam mesmo etc; mantém-se entre a violência e a
enganação pois, se as coisas ficam à mostra, o sistema não se sustenta.
 -->

# As máquinas solidárias

* O processo civilizador que vivemos tende a criar cada vez mais
  mediações/separações entre as pessoas.

* Associações solidárias entre pessoas resgatam o contato humano direto e
  o compartilhamento da vida.

* Organização: tensão entre o espontâneo e o planejado, entre o formal e o
  informal, entre a ordem e a desordem.

* As máquinas solidárias são máquinas antifascistas!

# Como fazer máquinas solidárias?

<!--
* Pergunte-me como! Quer dizer, também estou aprendendo!

* Carmille não precisou estar em contato direto com que ajudava. Na verdade o essencial
  era que ele não soubesse quem precisava de ajuda, pois isso implicaria em entregar
  essas pessoas para os algozes.
 -->

* Se o primeiro passo era estar bem para poder ajudar, o segundo passo é partir para
  a prototipagem e construção de relações entre as pessoas.

* Autodefesa: neutralização da ameaça, não necessariamente a sua aniquilação.

* Máquinas solidárias podem surgir espontaneamente: pessoas se ajudando mutuamente
  sem combinarem previamente.

Não tratarei do que é espontâneo. Isso vou assumir de imediato. Vou justamente
propor falar do que não é espontâneo, do que precisamos planejar e construir
para que talvez um dia possa ser espontâneo. Tudo o que será dito é básico, mas
por isso mesmo precisa ser dito.

<!--
Teoria do valor solidário: A Ajuda Múltipla e o Valor Social -
https://blog.fluxo.info/economics/valor-social/

Experimento mental pensando num outro tipo de economia para a sociedade.
 -->

<!--
* O termo organização acabou ficando associado apenas à menor parte das
  organizações possíveis.

* Precisamos trazer para o consciente detalhes de operação da sociedade.

* A ajuda é uma forma de interação. Interações entre pessoas compõem redes.

* Existem redes com escala (centralizadas, descentralizadas) e sem escala (distribuídas).

* Os diagramas de interação entre as redes dizem muito sobre a concentração de poder,
  riqueza, influência, indo das centralizadas para as distribuídas.

* Redes centralizadas são mais simples e possuem uma pragmática (prática) mais linear.
  Exigem menos habilidades e capacidades das periferias da rede e consequentemente
  remuneram menos a periferia.

* Com o aumento do número de pontos nodais (pontos de interação) aumenta o requisito
  de habilidades de cada integrante da rede.
 -->

# Estimando seu fator solidário

<!-- Máquinas solidárias também calculam, porém calculam para o bem-estar. -->

De maneira pragmática, incompleta mas que serve pra saber "o que tem pra hoje":

1. O quanto você precisa dedicar do seu tempo e energia para você?
2. O quanto você pode    dedicar do seu tempo e energia para outras pessoas?

A partir disso, alguém pode estimar sua relação solidária: horas semanais e
e intensidade de esforço, por exemplo.

<!--
Uma boa estimativa garante a ação solidária de longo prazo.
Investir demais em você pode produzir um futuro solitário.
Investir demais nas outras pessoas pode te esgotar ou até te martirizar.
Ajudar as pessoas não pode ser uma pena ou flagelo.

Existem pessoas com inclinação mais egoísta ou altruísta.
E cada pessoa tem uma necessidade diferente.
E tudo bem. Estamos tentando articular algo que opere dentro
dos limites e possibilidades das pessoas.
 -->

<!--
Esse tempo tende a ser menor do que em condições ideias. Por exemplo a expropriação
de mais-valia pela exploração do mundo do trabalho reduz drasticamente a quantidade
de riqueza que pode ser gerada por vias solidárias.

Mas pode ser um tempo maior se nos livrarmos de práticas nocivas como tempo
excessivo usando televisão e dispositivos móveis.
 -->

# Esferas de atuação

Onde e quanto você quer dispender seu tempo e energia disponíveis?

* Família.
* Amigos.
* Vizinhança.
* Colegas de trabalho.
* Grupos de Afinidade.
* Outros Movimentos Sociais Organizados.
* Quem estiver perto.

# Recursos disponíveis

<!-- Diferencias filantropia, caridade e ajuda mútua. -->
<!-- A ideia aqui é a autogestão, cada pessoa sendo capaz de ajudar sem precisar ser coordenada, mas capaz de integrar uma coordenação. -->

No que você pode ajudar? O que você pode oferecer?

* Calor humano?
* Espaço para alguém dormir?
* Artigos de dignidade básica? Comida, roupa, remédios, banheiro, local pra dormir...
* Assistência profissional: jurídica, médica, psicológica...

<!-- O trabalho solidário organizado e constante pode ser chamado como trabalho de base. -->

# Articulando a solidariedade

* A ajuda mútua compõe *redes* de pessoas, ou *tecido social.*
* As máquinas solidárias são *teares* orgânicos desse tecido.
* Daí que o desafio de construir redes ajuda mútua que garantam um fortalecimento do tecido.
* Eficiência, ganho de escala podem ser conceitos danosos e falaciosos.
* Cuidado com o hype das novas tecnologias!
* Simplexidade: a complexidade necessária.
* Romper separação entre teoria e prática. Pesquisa-ação, pesquisa-luta.

<!--
Sua tecnologia "de ponta" (blockchain, etc) pode ser instigante, mas também fetichista e errar feio o alvo. Precisa funcionar. Relação entre o que queremos fazer (o que dá tesão) e o que não queremos fazer mas que é necessário.
-->

# Parametrização

* Divisão de tarefas ou não? Revezar tarefas ou não?
* Especialização ou generalização?
* Horizontalidade ou verticalidade?
* Gerência e passividade ou autogestão e iniciativa?
* Tensão entre espontâneo e planejado.
* Tensão entre tesão e necessidade.
* O quanto planejar e o quanto deixar rolar? O quanto antecipar/preparar?
* Topologia da rede, redundância, tolerância a falhas e crescimento.
* Definição de princípios éticos pessoais e coletivos.
* Redes: "casamento de impedâncias" / compatibilização protocolar de princípios.

# Exemplos

<!-- Da seriedade ao engajamento. -->

* Sugestões e dicas por onde começar. Para onde iremos, não faço a mínima ideia.
* Tentando conceber formas de organização que funcionem na precariedade.
* Se organizar é um exercício prático: tente de uma forma, se não de certo, tente de outra.
* Use a dimensão lúdica das atividades!

# Perguntas balizadoras

* Quantas pessoas podem se abrigar na sua casa?
* Você tem contatos de emergência para várias situações?
* Tu tem comida pra quantos dias?
* Jah tirou passaporte? Vacinas em dia?
* Como anda seu preparo físico?
* Você tem ficha médica?
* Você já montou um procedimento padrão do que fazer em casos especiais (prisão, acidente, etc)?
* Uma ou mais pessoas do grupo tem treinamento em primeiros socorros?

<!--
* Quantas pessoas podem dormir na sua casa?
* Antitetanica e outras vacinas em dia?
* Você já tirou um molde da sua arcada dentária para facilitar eventual identificação?
* Você anda com alguma identificação? RG, placa metálica ou pulseira?
 -->

# Exemplo: Uma Rede de Solidariedade Protosindical

* Pessoas que vivem próximas.
* Reuniões periódicas: relatorias, discussões, decisões e responsabilizações.
* Realização de tarefas.
* Tarefas de exemplo: arrecadação de fundo de solidariedade, treinamento, mutirões, grupos de estudos.

# Exemplo: Rede de Solidariedade Criptosindical

<!--
qrencode  \
         https://opsec.fluxo.info/specs/criptosindicato.html \
         -o ../images/link-criptosindicato.png -s 4
 -->

![https://opsec.fluxo.info/specs/criptosindicato.html](../images/link-criptosindicato.png)

<!--
# Resumo

* Solidariedade: uma pessoa cuida da outra.
* Formações sociais possuem uma componente espontânea e outra que pode ser
  voluntariamente construída e experimentada.
-->
