[[!meta title="Contos da Crypto - Capítulo Intervenção Militar"]]
[[!meta date="2018-05-06 10:00:00-0300"]]

Cardificina OPSEC: histórias, tangos & tragédias num tecnodespacho
embaralhado.

Imagine que estamos refugiados no nosso próprio país, escondidos
num templo de religião proibida ou acampados no meio da terra
devastada.

Passaremos a noite contando histórias de várias cartas do
Baralho OPSEC - https://baralho.fluxo.info.

Para isso, precisamos de um canto isolado e silencioso durante
a madrugada onde possamos fumar charuto e beber rum sem sermos
importunados por agentes do sistema.

Material necessário pra criar um climão:

* Aparelho de som e músicas estilo Satanique Samba Trio.
* Velas e iluminação indireta.
* Mala de cartas.
* Charutos, rum, cigarrilhas, maços de cigarro cubano, fósforos.
* Almofadas pra sentar no chão.
* Cartazes indicativos da atividade.
* Taco de baseball cubano.
