[[!meta title="Tendências Brasil Hostil para a Década de 20"]]
[[!meta date="2018-05-06 12:00:00-0300"]]
[[!toc startlevel=2 levels=4]]

## Sobre

Estas são notas diversas de uma palestra realizada na [CryptoRave de 2018](/events/2018/cryptorave)
e contém um esboço inacabado sobre redes de solidariedade com modificações posteriores à palestra.

* [Slides](slides) | [PDF](slides.pdf).

## Convite

A primavera durou pouco e agora temos um longo e tenebroso
inverno pela frente.

Medievalismo judiciário, estado policial de exceção, boçalidade cognitiva e
zumbificação de massas compõem o repertório corrente de violência e bestialidade.

Mas você não vai suportar todo esse período sem se preocupar
em ficar longe da vala, com um look descolado e ainda causando impressões, certo?

Então vamos falar do que está IN e o que está OUT.

Moda: conselhos exclusivos para evitar o paletó de madeira da coleção passada.

Ostentação: quem hoje em dia consegue aquele habeas corpus malhado e tão cobiçado?
Como se virar sem ele e aproveitar a vida fora da cadeia.

Polêmica: precisamos falar sobre clandestinidade. O que é, quem entra e quem sai
mantendo o bronzeado.

E muito mais! Dicas de Segurança Operacional que ninguém quer dar pra você se organizar
na coletividade para resistir ao tratamento de choque social.

## Primeira parte: tour de force

    "E essa agora que o General da intervenção disse que o
     Rio de Janeiro é um Laboratório para o Brasil?
     E nós somos as cobaias??? Absurdo!"

    -- Marielle Franco, em mensagem de 27/02/18

    "E eu sei quem são meus amigos eternos e quem são
     os eventuais. Os de gravatinha, que iam atrás de mim,
     agora desapareceram."

    -- Lula, em seu discurso de 07/04/18

### Recapitulando, mas sem capitular

Como estávamos nos anos anteriores?

* [2016: Brasil Hostil - Circuito Grampolândia: a República da Escuta:
  vigilância generalizada](https://grampo.org).

* [2017: Brasil Hostil - Etapa República de
  Weimar](https://blog.fluxo.info/events/2017/cryptorave/hostil/):
  efervescência política e avanço do arcabouço jurídico da repressão.

O assunto era PNI, GLO, LOC, LSN, Lei Anti-Terror, Código Penal, Operações Extra-Oficiais...

Falamos sobre OPSEC - Segurança Operacional, dando ênfase na preparação para
pessoas e grupos enfrentarem as dificuldades da vida e estarem à frente da
repressão e desestabilização social.

Onde a última palestra termina, esta começa: Círculos de Solidariedade o organização coletiva.

### Como estamos?

* 2018: Brasil Hostil - Edição Intervenção Foderal / Ascensão do Fascismo.

* Fim de muitos ciclos aqui e no mundo. Talvez até de grandes períodos sob a
  égide do Iluminismo ou do Renascimento.

* Mas chega de prosa. Bora dar um giro?

### Attention! Achtung! Attenzione! Atenção!

![Recadinho do General para turistas](images/baderna.png)

### Mini-tour Brasil Hostil 2018 - Bordel

![Ritual dos Sádicos no clube hedonista "Bahamas"](images/bahamas.png)

Parece uma cena de enforcamento pré-Thermidoriana.

### Mini-tour Brasil Hostil 2018 - Personalidades

![Ministro da Defesa reverenciando o General](images/beijo-chefao.jpg)

### Mini-tour Brasil Hostil 2018 - Emboscadas

![Marielle: morte sem ameaças prévias](images/carro-marielle.jpeg)

Essa cena me lembrou do fusca do Marighella.

### Mini-tour Brasil Hostil 2018 - Moda

![Trench Coat Militar](images/trench-coat.jpg)

### Mini-tour Brasil Hostil 2018 - Polvo

![Kit Palhacitos](images/tuiuti-pato-fantoche.jpg)

Fantasia ou realidade?
Vizú de carnaval tirando um côco da imbecilidade

### E o mundo em geral, a quantas desanda?

![Zombie Nation](images/facebook-zombies.jpeg)

### Tendências para a Década de 20

Tem gente muito mais informada do que eu pra dar um diagnóstico melhor.
Mesmo assim, em bom protuguês:

* Um FEBEAPÁ pornográfico.

* Elite local tentado dar o salto para se tornar elite global via
  privatização extrema e entreguismo; indícios de forte parceria entre o crime
  organizado do andar de cima e do de baixo (facções); e por aí vai.
  Um processo cheio de ruídos.

* Estado de exceção permanente, Terror, necropolítica, pós-realidade.
  Muita gente vai rodar.

* Fascismo de consumo em momento de recessão econômica.

* Falta de narrativa que dê sentido à existência frente ao abismo.

* Choque, luto, depressão, paralisia.

Momento pouco propício para que a mobilização se amplie.
Vivi essa sensação antecipadamente.

### Então fodeu?

* A canoa emborcou. Pode ser que amarguemos alguns anos até a próxima iteração.

* Protestos e mobilizações passam por uma fase de esgotamento e
  insuficiência.

* A janela histórica pode se abrir havendo comoção massiva.

* Este século pode ser decisivo para o futuro da humanidade.
  Possibilidade de obsolescência incluindo o humano. Ainda não sabemos.

* Enquanto isso, estamos mais ou menos que por conta. Enquanto isso...
  bom... havendo vida há esperança.

### Por onde (re-)começar?

* O que a gente faz depois de sobreviver a uma avalanche?

* Momento de estudo, maturação, fortalecimento, solidariedade, organização e luta.

* A gente começa pequeno e cuida do básico essencial. Fazendo bem, a parada
  cresce.

* No passado falamos muito de _segurança._ Agora nossa ênfase será
  na ideia de _organização._ E falaremos das organizações _solidárias._

### Solidariedade: egoísmo e altruísmo

O fundamento da solidariedade é a **tensão** dinâmica entre egoísmo (cuidar de si,
receber cuidados) e altruísmo (cuidar de outrem, aceitar os cuidados de outrem).
A isto chamaremos de _ajuda mútua._

![Afresco egípcio sobre segurança aérea, séc. XX D.C.](images/egyptian-safety-sheet.png)

Nota: essa figura é curiosa. Repare que o cuidado é associado a um papel
usualmente considerado de feminino em tal sociedade. Ao mesmo tempo, o estado
frágil, desprotegido e sem máscara, é caracterizado pela roupa na cor rosa,
enquanto que no estado de salvação a roupa passa a ser azul, uma cor na época
usualmente associada ao papel masculino. Essa civilização possivelmente tinha
muitos problemas relativos à atribuição de papéis a gêneros.
Assim, sacos de vômito também são bem-vindos.

"Solidariedade" aqui não será entendida no termo clássico, de solidez e apenas "soma
do todo". Será mais *fluido:* a relação local/global.

O problema do termo "solidariedade": sólido, solidez, "soma do todo": visão
eminentemente globalista, totalizante.  Aqui iremos usar o termo solidariedade
mais no sentido da ajuda mútua: relações locais entre pessoas e grupos que
compõem um tecido social global.

Então, primeiramente... cuidar da dor. Fazer o luto. Cicatrizar. Saber o quanto é
preciso estar bem para pode ajudar. Aceitar ajuda.

A via é de mão dupla e o puro antagonismo entre os termos não nos serve.
Os vínculos _sólidos_ podem ao mesmo tempo serem fluídos. Fluidariedade.

Diferenciar ajuda mútua do conceito liberal de "liberdade, igualdade e fraternidade".

Diferenciar ajuda mútua de Esquema Ponzi:
http://www.dinheiroganhar.net/ajuda-mutua-e-crime-saiba-a-verdade/

### Bem-estar

* Os afetos _tristes_ reduzem nossa capacidade de ação. Estar junto favorece
  os afetos _alegres,_ fundamentais para a mudança.

* É incrível como às vezes a gente consegue ajudar mesmo estando em frangalhos.
  Outras vezes não.

* O Duplo Movimento de renúncia e libertação: tirar o peso do mundo das costas
  para poder mudar o mundo. Poder respirar primeiro para assim poder mergulhar de novo.

* Os múltiplos movimentos: a dificuldade humana de relacionar escalas: micro-macro,
  temporalidades, local-global, aqui-e-agora etc.

### Os movimentos e os caminhos

* Esta é uma proposta para ajudar no caminho de hoje. Utopia vista como narrativa
  de onde podemos chegar que nos ajuda a andar. Não como ponto de chegada.

* Pode ser que o trampo deste geração seja preparar o terreno para a próxima...

* Pode ser que precisamos olhar para gerações (iterações) passadas e checar
  alguns detalhes que passaram batidos...

* A ajuda mútua pode ser a orientação básica para praticar e propagar
  formas de existência mais dignas.

* Mas apenas essa orientação é insuficiente. Apenas a prática espontânea
  da ajuda também é insuficiente.

* É necessária uma solidariedade além do meramente espontâneo e que gere algum
  acúmulo, um ganho organizativo que permita um salto, mesmo que lá na frente.

* Precisamos de solidariedade _organizada._

### O que fazer?

* Poderíamos buscar teorias progressistas para repensar no que fazer.

* Mas aqui será proposta uma rota alternativa: voltar para um período
  de resistência ao totalitarismo e descobrir no que ele pode nos ajudar.

* Começar dentro do possível para chegar ao impossível. Mas antes
  de falar da nossa década de 20, falemos sobre as décadas de 30
  e 40 do século passado.

* Mesmo porque a [própria esquerda partidária tem falado sobre
  criar uma frente antifascista](http://midianinja.org/jeanwyllys/o-brasil-precisa-de-uma-frente-antifascista/).

### Rebobinando...

    "Quando os nazistas levaram os comunistas, eu calei-me,
     porque, afinal, eu não era comunista.

     Quando eles prenderam os sociais-democratas, eu calei-me,
     porque, afinal, eu não era social-democrata.

     Quando eles levaram os sindicalistas, eu não protestei,
     porque, afinal, eu não era sindicalista.

     Quando levaram os judeus, eu não protestei, porque,
     afinal, eu não era judeu.

     Quando eles me levaram, não havia mais quem protestasse."

    -- Martin Niemöller

### Naquela época...

* O contexto tinha suas semelhanças.
* Estávamos muito na merda. Fascismo crescendo. Totalitarismo.

No meio da bagunça aparece um estranho personagem...

### Quem é este figura?

![Apenas mais um gravatinha?](images/carmille.jpeg)

### A nossa cartada

![Agente secreto da Rede Marco Polo](images/card.png)

* [Links diversos sobre René Carmille](https://links.fluxo.info/tags/ren%C3%A9%20carmille).
* Saiba mais sobre Carmille [pelo resumo do livro IBM and the Holocaust](/books/history/ibm-holocaust/).

### Naquele tempo, eram outros Cards

![Cartão Perfurado](images/punchcard.jpg)

Nota: este modelo de cartão perfurado aparentemente é de um período posterior.

### SIGSTOP, SIGKILL, HALT

Carmille operou em duas temporalidades:

1. Urgência, curto prazo: salvar quem estava na berlinda. E fez isso em larga escala
   apenas parando a maquinaria sem que os alemães soubessem. A ação
   passiva Zen, a não-ação, desobediência.

2. Organização, médio e longo prazo: tentar mobilizar o exército de reserva francês para
   ações futuras da Resistência.

Preso, torturado e depois enviado pro saco, não concluiu a segunda etapa do plano.

### Uma lição dos tempos

* Aproveitar a nossa condição, usando que temos disponível.
* Não precisamos de super-heróis mártires: há um pouco a fazer para cada pessoa.

Em especial, descobrimos que a solidariedade, e não apenas a dominação, também
é um empreendimento logístico-informacional!

### As máquinas sociais dominadoras e as solidárias

* Maquinário da Dominação: cartão perfurado, registro e rastreamento: mesquinharia
  contábil para controle, dominação e eliminação das pessoas. E dessa contagem que
  nasce a economia muquirana baseada na escassez. A questão de fundo é o medo egoísta
  da morte que exige extrair a vida de outrem a todo custo para tentar interromper o
  processo vital.

* Na maquinaria solidária, as vidas estão entrelaçadas, de modo que uma pessoa
  tem sua liberdade enquanto outra também tiver. Estará bem conforme a outra também
  estiver.

### Tensionando

Estas são conceituações de máquinas "puras", "perfeitas". Na prática, no caos da vida,
as máquinas sociais reais são uma mistura desses dois modelos arquetípicos, com maior
ou menor presença de solidariedade e dominação.

Existe uma tensão permanente entre egoísmo e altruísmo que se manifesta no
diagrama, ou máquina social.

Lutar por um mundo melhor implica em mudar o balanço social em direção a uma maquinaria
solidária.

Encontrar sua faixa de operação solidária consiste em achar a região de equilíbrio
entre cuidar de si e cuidar da outra pessoa.

### As máquinas da dominação

Sua violência pode ser distinguida entre

* Psicológica (enganação): vulgo "softpower", "me engana que eu gosto".
* Física: opera quando a enganação já não surte efeito.

Sempre operam em par, porque não é possível enganar nem bater em todo mundo ao
mesmo tempo; bate-se em uns enquanto diz-se pros outros que eram a raíz de
todos os males, que mereciam mesmo etc; mantém-se entre a violência e a
enganação pois, se as coisas ficam à mostra, o sistema não se sustenta.

### As máquinas solidárias

* O processo civilizador que vivemos tende a criar cada vez mais
  mediações/separações entre as pessoas.

* Associações solidárias entre pessoas resgatam o contato humano direto e
  o compartilhamento da vida.

* Organização: tensão entre o espontâneo e o planejado, entre o formal e o
  informal, entre a ordem e a desordem.

* As máquinas solidárias são máquinas antifascistas!

### Como fazer máquinas solidárias?

* Pergunte-me como! Quer dizer, também estou aprendendo!

* Carmille não precisou estar em contato direto com que ajudava. Na verdade o essencial
  era que ele não soubesse quem precisava de ajuda, pois isso implicaria em entregar
  essas pessoas para os algozes.

* Se o primeiro passo era estar bem para poder ajudar, o segundo passo é partir para
  a prototipagem e construção de relações entre as pessoas.

* Autodefesa: neutralização da ameaça, não necessariamente a sua aniquilação.

* Máquinas solidárias podem surgir espontaneamente: pessoas se ajudando mutuamente
  sem combinarem previamente.

Não tratarei do que é espontâneo. Isso vou assumir de imediato. Vou justamente
propor falar do que não é espontâneo, do que precisamos planejar e construir
para que talvez um dia possa ser espontâneo. Tudo o que será dito é básico, mas
por isso mesmo precisa ser dito.

Exemplo: [Teoria do valor solidário: A Ajuda Múltipla e o Valor Social](/economics/valor-social),
um experimento mental pensando num outro tipo de economia para a sociedade.

* O termo organização acabou ficando associado apenas à menor parte das
  organizações possíveis.

* Precisamos trazer para o consciente detalhes de operação da sociedade.

* A ajuda é uma forma de interação. Interações entre pessoas compõem redes.

* Existem redes com escala (centralizadas, descentralizadas) e sem escala (distribuídas).

* Os diagramas de interação entre as redes dizem muito sobre a concentração de poder,
  riqueza, influência, indo das centralizadas para as distribuídas.

* Redes centralizadas são mais simples e possuem uma pragmática (prática) mais linear.
  Exigem menos habilidades e capacidades das periferias da rede e consequentemente
  remuneram menos a periferia.

* Com o aumento do número de pontos nodais (pontos de interação) aumenta o requisito
  de habilidades de cada integrante da rede.

### Estimando seu fator solidário

Máquinas solidárias também calculam, porém calculam para o bem-estar.

De maneira pragmática, incompleta mas que serve pra saber "o que tem pra hoje":

1. O quanto você precisa dedicar do seu tempo e energia para você?
2. O quanto você pode    dedicar do seu tempo e energia para outras pessoas?

A partir disso, alguém pode estimar sua relação solidária: horas semanais e
e intensidade de esforço, por exemplo.

* Uma boa estimativa garante a ação solidária de longo prazo.
* Investir demais em você pode produzir um futuro solitário.
* Investir demais nas outras pessoas pode te esgotar ou até te martirizar.
* Ajudar as pessoas não pode ser uma pena ou flagelo.
* Existem pessoas com inclinação mais egoísta ou altruísta.
* E cada pessoa tem uma necessidade diferente.
* E tudo bem. Estamos tentando articular algo que opere dentro
* dos limites e possibilidades das pessoas.

Esse tempo tende a ser menor do que em condições ideias. Por exemplo a expropriação
de mais-valia pela exploração do mundo do trabalho reduz drasticamente a quantidade
de riqueza que pode ser gerada por vias solidárias.

Mas pode ser um tempo maior se nos livrarmos de práticas nocivas como tempo
excessivo usando televisão e dispositivos móveis.

### Esferas de atuação

Onde e quanto você quer dispender seu tempo e energia disponíveis?

* Família.
* Amigos.
* Vizinhança.
* Colegas de trabalho.
* Grupos de Afinidade.
* Outros Movimentos Sociais Organizados.
* Quem estiver perto.

### Recursos disponíveis

* Diferencias entre filantropia, caridade e ajuda mútua.
* A ideia aqui é a autogestão, cada pessoa sendo capaz de ajudar sem precisar ser coordenada, mas capaz de integrar uma coordenação.

No que você pode ajudar? O que você pode oferecer?

* Calor humano?
* Espaço para alguém dormir?
* Artigos de dignidade básica? Comida, roupa, remédios, banheiro, local pra dormir...
* Assistência profissional: jurídica, médica, psicológica...

O trabalho solidário organizado e constante pode ser chamado como trabalho de base.

### Articulando a solidariedade

* A ajuda mútua compõe *redes* de pessoas, ou *tecido social.*
* As máquinas solidárias são *teares* orgânicos desse tecido.
* Daí que o desafio de construir redes ajuda mútua que garantam um fortalecimento do tecido.
* Eficiência, ganho de escala podem ser conceitos danosos e falaciosos.
* Cuidado com o hype das novas tecnologias!
* Simplexidade: a complexidade necessária.
* Romper separação entre teoria e prática. Pesquisa-ação, pesquisa-luta.

Sua tecnologia "de ponta" (blockchain, etc) pode ser instigante, mas também
fetichista e errar feio o alvo. Precisa funcionar. Relação entre o que queremos
fazer (o que dá tesão) e o que não queremos fazer mas que é necessário.

### Parametrização

* Divisão de tarefas ou não? Revezar tarefas ou não?
* Especialização ou generalização?
* Horizontalidade ou verticalidade?
* Gerência e passividade ou autogestão e iniciativa?
* Tensão entre espontâneo e planejado.
* Tensão entre tesão e necessidade.
* O quanto planejar e o quanto deixar rolar? O quanto antecipar/preparar?
* Topologia da rede, redundância, tolerância a falhas e crescimento.
* Definição de princípios éticos pessoais e coletivos.
* Redes: "casamento de impedâncias" / compatibilização protocolar de princípios.

### Exemplos

* Sobre a palavra que não é seriedade, nem engajamento; envolvimento com desapego, talvez.
* Sugestões e dicas por onde começar. Para onde iremos, não faço a mínima ideia.
* Tentando conceber formas de organização que funcionem na precariedade.
* Se organizar é um exercício prático: tente de uma forma, se não de certo, tente de outra.
* Use a dimensão lúdica das atividades!

### Perguntas balizadoras

* Quantas pessoas podem se abrigar na sua casa?
* Você tem contatos de emergência para várias situações?
* Tu tem comida pra quantos dias?
* Jah tirou passaporte? Vacinas em dia?
* Como anda seu preparo físico?
* Você tem ficha médica?
* Você já montou um procedimento padrão do que fazer em casos especiais (prisão, acidente, etc)?
* Uma ou mais pessoas do grupo tem treinamento em primeiros socorros?

Nível saco preto:

* Você já tirou um molde da sua arcada dentária para facilitar eventual identificação?
* Você anda com alguma identificação? RG, placa metálica ou pulseira?

### Exemplo: Uma Rede de Solidariedade Protosindical

* Pessoas que vivem próximas.
* Reuniões periódicas: relatorias, discussões, decisões e responsabilizações.
* Realização de tarefas.
* Tarefas de exemplo: arrecadação de fundo de solidariedade, treinamento, mutirões, grupos de estudos.

### Exemplo: Rede de Solidariedade Criptosindical

<!--
qrencode  \
         https://opsec.fluxo.info/specs/criptosindicato.html \
         -o images/link-criptosindicato.png -s 4
 -->

Vide [Especificação: Rede de Solidariedade Criptosindical](https://opsec.fluxo.info/specs/criptosindicato.html)

### Resumo

* Solidariedade: uma pessoa cuida da outra.
* Formações sociais possuem uma componente espontânea e outra que pode ser
  voluntariamente construída e experimentada.

Por enquanto é isso! Aguardem mais insucessos da franquia Brasil Hostil.

## Segunda parte: saindo da lama

### O Baseline

* Partiremos do pressuposto básico, o critério de *dignidade universal e irrestrita.*
* A dignidade hoje é, infelizmente, um privilégio restritíssimo.
* Quando deveria ser uma condição básica de existência.
* Vivemos no contexto contrário, de redução de dignidade e concentração de privilégios.
* Falemos então de possibilidades solidárias para reverter este quadro.
* Falaremos de distopia, mas também de utopia.

### Vamo lá

Truta, se ligue: a falta de garantias implica que qualquer pessoa pode se ferrar.
Isso vale mesmo para quem defende o sistema. Não há garantias. Esta é a característica
dos períodos de Terror.

Ressaltemos alguns aspectos:

1. A solidariedade, ou a falta dela.
2. A preparação pra quando sua hora chegar, ou a falta dela.
3. O protesto como agitação política não é suficiente para barrar retrocessos.

Se pensarmos individualmente, estaremos totalmente na merda. Porque hoje, focar
na proteção de uma pessoa de forma satisfatória às ameaças vigentes se tornou
uma tarefa impossível. Apesar de ainda ser uma tarefa importante e fundamental.

Crucial agora é dar o salto de ênfase, pensando também e muito na segurança
de grupos sociais.

Temos então dois movimentos simultâneos:

1. Solidariedade -- sólida e flexível: está na manutenção da segurança e privacidade comuns.
2. Segurança pessoal: a nossa higiene pessoal contra ameaças.

### Conjuntura

* Os fatores da dominação: violência e enganação.
  * Violência psicológica (enganação): vulgo "softpower", "me engana que eu gosto".
  * Violência física: opera quando a enganação já não surte efeito.
  * Sempre operam em par, porque não é possível enganar nem bater em todo mundo
    ao mesmo tempo; bate-se em uns enquanto diz-se pros outros que eram a raíz
    de todos os males, que mereciam mesmo etc; mantém-se entre a violência e
    a enganação pois, se as coisas ficam à mostra, o sistema não se sustenta.
* Repetindo: a canoa emborcou. Vamos amargar alguns anos até a próxima iteração.
* Momento de estudo, maturação, fortalecimento, solidariedade, organização e luta.
* A janela histórica se abrirá apenas caso haja condições que comovam massivamente.
* Enquanto isso, estamos mais ou menos que por conta.

Em geral os prognósticos de luta precisam se basear na crença da possibilidade
de vencer.

A proposta a seguir, no âmbito da Segurança Operacional - OPSEC, assume que
potencialmente tudo vai dar errado. Mas que mesmo assim vamos tentar minimizar
os estragos.

### Realpolitik

É sempre importante saber qualé. Para não cair na enganação. A ilusão e o equívoco
existem para qualquer pessoa. Mesmo a esquerda tende muitas vezes a se iludir e
inclusive diversos de seus setores também são praticantes da violência e da enganação.

Por isso, abra a cabeça, camarada, para não fugir da dominação da direita e cair
na de alguns setores da esquerda. É um pré-requisito da esquerda emancipatória ter
uma ética da não-violência e da não-enganação. Os meios são importantes. O modo de
fazer é pré-figurativo.

Tentam nos colocar numa sinuca de bico chamando de idealismo qualquer iniciativa ética
que não busque a mudança a todo custo. Opõem-na à chamada *realpolitik,* erroneamente
conhecida como política real, feita de conchavos, acordos prejudiciais e corrupções.

O que seduz na realpolitik é o seu pragmatismo a todo custo, que toma atalhos mas
que nunca, na prática, produz a mudança real. A um ganho de curtíssimo prazo se
troca toda uma possibilidade de mudança duradoura.

Mas a *política real* efetiva é aquela que pré-figura a todo tempo a sociedade em que
queremos viver, produzindo-a de momento em momento mesmo que seja em microescala.
Inicialmente ela não parece ser prática, porque pode ser de difícil aplicação. Mas
ela é justamente prática porque requer que seja praticada diariamente, fazendo com
que quem a pratique se aperfeiçoe cada vez mais.

Nesse contexto, a atual luta moralista contra a corrupção perde completamente o
sentido por diversos motivos. Principalmente, porque ela se exime de ter qualquer
responsabilidade sobre fazer política. Querem matar a política e quem a pratica,
mas não sugerem nenhuma outra forma de gerir a vida comum. A dominação adora
esse tipo de gente, que pode ser facilmente delegar seu poder político para
ser governada e que também facilmente deixa-se enganar quando convém.

Tentam nos colocar numa sinuca de bico dizendo que a prática solidária horizontal
não cresce a ponto de produzir uma mudança em grande escala. Porém, se esquecem
que a prática pré-figurativa pode inspirar e se espalhar.

### Autodefesa versus violência

Confunde-se não-violência com a proibição de se autodefender e de imobilizar o oponente.
Ao contrário da violência, a autodefesa busca a neutralização do inimigo enquanto inimigo
e não a sua aniquilação.

![Nazi punch card](images/nazi-punch-card.png)

### OPSEC

* Sendo ou não sendo alvo.
* Foco individual ou coletivo.
* Repressão (curta duração, alta intensidade, localizada) e Opressão.
* Choque/trator: ação de impacto que provoca a neutralização de qualquer resposta social.

### Alvos

* Alvo difuso: quando uma ameaça é dirigia não só a você, mas a todo um grupo social; por exemplo
  a vigilância das ruas usando câmeras.

* Alvo definido: quando a ameaça é dirigida especificalmente a você ou a um grupo social pequeno
  e bem definido; por exemplo escutas telefônicas.

* Seleção de alvos: é um processo que, a partir de sistemas de vigilância difusos, pode selecionar
  manual ou automaticamente alvos mais específicos a partir de critérios de seleção.

* É melhor não ser um alvo definido. Do contrário, o custo pessoal da segurança fica impraticável.
  Para isso, uma das melhores estratégias é não demonstrar relevância, ter um perfil low profile
  ou se possível deixar sua identidade com pouca evidência.

* Nem sempre é possível se manter na indefinição. Isso vai depender muito dos nossos papéis dentro
  da sociedade e especialmente da nossa atuação. Muitos perfis precisam ser públicos. E conforme
  esses perfis se tornam influentes, passam a ser alvos definidos. E aí o custo de se manter em
  segurança passa a ser muito, muito alto, a ponto de talvez não ser mais possível a manutenção
  da segurança por conta própria, usando apenas recursos pessoais.

  Mas, os poderes do status quo tem um monopólio de uso da violência e uma capacidade de vigilância
  que são limitados, e não conseguem colocar muitas pessoas ao mesmo tempo como alvos definidos.
  Nesses casos, se conseguirmos mobilizar nossa capacidade de solidariedade e comoção, teremos
  condições de ajudar as vítimas e/ou mudar a situação após quadros políticos consumados.

Então o diagrama fica assim:

      "big data"
      alvos difusos
      escala de massa
      vigilância de baixo custo                 -> autodefesa mútua
      violência aleatória (bala perdida etc)

      "little data"
      alvos definidos                           -> defesa coletiva e solidária
      baixa escala
      vigilância com alto custo
      alta violência, direcionada

Esse tipo de situação pode ser interpretada a partir desta frase (cuja atribuição a Stálin
[não foi confirmada](https://quoteinvestigator.com/2010/05/21/death-statistic/),
[2](https://en.wikiquote.org/wiki/Joseph_Stalin#Misattributed>)):

    A morte de uma pessoa é uma tragédia; a de milhões, estatística.

### Solidariedade e Organização

Togetherness e outridade: estar junto, de acordo com a narração do livro O Jogo de Amarelinha:

![Togetherness e outridade: estar junto](images/togetherness.jpg)

A solidariedade não é algo sempre automático. Não basta que nos comovamos com uma situação para
que automaticamente consigamos agir de forma solidária e efetiva. A comoção, sem uma canalização
das nossas energias, pode ser apenas mera catarse e dispersão de iniciativas individualizadas.

É importante um diálogo do individual com o coletivo. Que não seja de sujeição, controle,
gerenciamento verticalizado. Mas que seja *organizativo.*

É a organização que permite nossa autodefesa mútua, em escala individual, e a defesa coletiva
e solidária em escala coletiva.

O termo *Organização* está em mal uso. Aqui, não estamos falando do que muita gente automaticamente
pensa quando essa palavra é proferida. Organização aqui, não se restringe a instituições, empresas,
partidos políticos, família ou governo.

Organização aqui é entendida como qualquer possibilidade de convivência entre pessoas de forma
horizontal, consensual, não-violenta, não-discriminatória, emancipatória, voluntária,
igualitária/equivalente.

Existem infinitas formas de organização possíveis. A humanidade experimentou apenas uma pequena
parte do que é possível.

### Organização e Esforço

As organizações não se criam de huma hora para outra. Muitas delas são emergentes, isto é, aparecem
com espontaneidade de acordo com algumas condições ambientais e socias.

* No entanto, algumas organizações importantes precisam ser criadas a partir de muito esforço e não
  necessariamente apenas na espontaneidade. Se fiar apenas no espontâneo é como que jogar tudo pro
  alto e deixar rolar; ao contrário, focar em organizações que não deixem espaço para a espontaneidade
  é uma receita para o engessamento e para a perda de tesão.

* Saber quando o mar está pra peixe: o que vai dar certo terá uma mistura de esforço, preparação,
  antecipação, planejamento com o que tendencialmente é propício de acontecer. Não basta que tenhamos
  ideias brilhantes e instigantes: é preciso que haja uma potência latente e iminente para que
  um modo de organização se multiplique.

### Autogestão e ganho de escala

* O gerenciamento centralizado permite ganhos de escala por padronização, uniformização e pelo
  estabelecimento de cadeias de comando, criando assimetrias, opressão, supressão de individualidades
  e desperdiçando o potencial mental coletivo.

* Por outro lado, a autogestão opera em baixa escala permitindo a realização de potencialidades
  individuais de forma solidária. Porém, para que a autogestão ganhe escala é preciso que ela seja
  replicada. Isso demanda que usemos nossa mente em todo o nosso potencial.

### Política e Gestão

A ideologia corrente criminaliza a política e enaltece a gestão centralizada.
Ao contrário, podermos resgatar a política com a gestão descentralizada e distribuída.

Mas antes algumas definições:

* Política: usualmente debate livre e sem violência de ideias para o convencimento de
  pessoas do que precisa ser feito e os requisitos gerais de como fazer. Ela é normalmente
  composta de acordos, compromissos e inclusive de sigilo durante algumas de suas fases,
  mas a tomada de decisão precisa ser transparente.

* Economia.

* Tecnologia: a arte da vida prática: como fazer mais usando menos, desperdiçando menos e
  dentro de uma ética do que é válido e do que não é, por exemplo conservação do ambiente
  e não explorar ninguém.

* Gestão: usualmente, a administração da técnica para realizar aquilo que foi
  decidido. Mesmo nesta visão usual já é possível perceber que não há gestão sem
  uma orientação política.

E agora, uma redefinição: política, economia, tecnologia e gestão são conceitos entrelaçados.
Entendidos como conceitos separados, distintos, pode servir para entendermos diferentes formas
de se agir no mundo. Simultaneamente podem ser usados para dividir e conquistar, para afastar
a pessoas de esferas distintas artificialmente.

O pragmatismo tem distintas facetas. Atividades podem ser separadas por questões práticas,
para facilitar a ação, mas podem criar abstrações, indireções, mediações e complexidades
que tornam as atividades inclusive pouco práticas, abrindo a brecha para, na complexidade,
usar e abusar da violência e da enganação.

### As muitas seguranças

* Afetiva, fisiológica, cognitiva, financeira, alimentar.
* Resistência e resiliência.
* Pontos de falha: centralização, descentralização e distribuição: redundância é fundamental.

Dialógica da parada: havendo uma centralização de informação qualquer, aumenta
a importância de tal ponto nodal; torna-se um ponto de falha, um lugar de risco
e de interesse; é importante leva isso em conta ao criar sistemas de informação
para mobilização; fundamental saber o limite de armazenamento de um ponto nodal
para equilibrar o quanto ele pode articular organizacionalmente minimizando os
riscos de vazamento.

### O que é o que é

* OUT: fatalismo - fodeu, não há o que fazer!
* IN: mesmo que não dê certo, vamos tentar.

* OUT: agir apenas no espontaneísmo.
* IN: planejar, respirar, se organizar mas também deixar espaço para o inesperado.

* OUT: depressão e solidão.
* IN: elas baterão na sua porta, talvez você já ande com elas, mas cultivá-la
  não é a ferramenta correta para superarmos o estado de coisas.

* OUT: agir apenas por conta própria.
* IN: você não vai se garantir completamente.

* OUT: martirização, levar o mundo nas costas.
* IN: pra funcionar, é importante que cada pessoa contribua o quanto pode;
  a gente não vai dar conta de tudo; isso é ótimo, porque isso quebra a
  expectativa de que salvemos o mundo por conta própria, sendo um convite
  para que mais pessoas participem e contribuam onde não conseguimos incidir.

* OUT: heroísmos e culto à personalidade.
* IN: somos pessoas de carne e osso; todo mundo tem limitações, mas sobre
  essas limitações trabalhamos com sobreposição e complementaridade.

* OUT: viver apenas das emergências, urgências e apagação de fogo.
  IN: se preocupar também com a preparação e a antecipação.

* OUT: protesto xoxo.
* IN: cadê o carnaval anticapitalista dos anos 90 e 2000?

* OUT: achar que ativismo é só fazer manifestação.
* IN: a manifestação é uma flexão muscular social, mas para que ela exista é necessário
  que haja um músculo social sadio em primeiro lugar; e ele é composto de muito trabalho
  de base, constância e planejamento.

* OUT: Guerrilha dos anos 70. O foquismo é um método de aplicação e resultados restritos,
  além do alto custo vital.
* IN: Resistência Antifascista dos anos 30 e 40.

* OUT: ser enquadrado nas LSN, na Lei Antiterror e na LOC.
  IN: jogar de novas formas; se adotarmos os mesmos métodos que o status quo, seremos
  varridos facilmente.

### O mundo é um emaranhado de NÓS

Tudo isto aqui é básico. E forma uma espécie de protocolo de organização pessoal e coletiva.
E galera, isso aqui é tudo exemplo. Não tou querendo dizer que as coisas devem ser assim ou
assado. Tou dando ideias pra vocês adotarem o que quiserem.

* Cálculo de pessoas/hora disponíveis.
* 1h/semana por exemplo; com avanços incrementais.
* Uma cerva a menos, R$10 por semana como fundo de caixa. E você vai cair na bebedeira igualmente.
* Montar uma checklist pra você e pro seu grupo daquilo que for mais importante pra vocês
  se prepararem.
* Mas sem plataforma de organização da precariedade como um "Uber do Ativismo". Smartcontracts
  é o ultraneoliberalismo degradando ainda mais as relações sociais; nosso movimento precisa ser
  inverso, de acordos e protocolos que aproximem ao invés de afastar pessoas.
* É importante que saibamos nos organizar sem plataformas computacionais e sem aplicativos.
* Existem organizações que se mantém sem nada disso.
* Um grupo coeso funciona bem pra isso. Antigamente chamávamos de "grupo de afinidade".
* Idealmente, faça parte também de grupos com a possibilidade de convívio presencial.
* Mas também nutra relações remotas.
* A solidariedade pode percorrer escalas de tempo e espaço maiores do que nós!

### Etapia: para além da utopia e da distopia

Uma fala caótica. Pesquisa em construção. Somente pontos de partida, não de chegada.
Este texto apenas toca num problema de fundo, que é a relação da utopia com a distopia.

Podemos esboçar uma articulação possível de um caminhar tendo a utopia como direção e
sentido:

    UTOPIA como a próxima ETAPA
    ETAPIA, utopia em passos incrementais, discretos

* Distopia: lugar de merda, "tamos fodidos", onde não queremos estar
* Utopia: Morus, lugar nenhum; Huxley: aqui e agora.
* Atopia? Etapia?

Topos vira tapas! Do luto à luta!

### Reforma e revolução

Etapia e o conflito Reformismo X Revolução: de certa maneira este conflito é
falso. Ao atingir uma etapa suficiente de organização e mobilização, o
movimento social está em condição de promover as mudanças necessárias para
acabar com o sistema de dominação. Daí que reforma e revolução, no sentido
estrito, são diferenças de escala de tempo e espaço. O que não invalida a
oposição real entre reformismo (preferir concorrer à eleições etc) e
revolucionarismo (optar por trabalho de base).

A diferença começa a se acentuar também quando ambas escolhas se revelam
antagônicas e um dos grupos tenta suprimir o outro. Aí, nesse sentido, reforma
e revolução passam a ser duas abordagens muito distintas e concorrentes:
disputa da etapa atual. Ou seja, por outro lado o conflito Reformismo X
Revolução é muito real.

É possível mostrar, também, que a ancestralidade, de onde alguém vem, por ser
formulada como uma cadeia de eventos, uma sequência de ETAPIAS.

### Resumo

A tecnologia de hoje é muitas ordens de grandeza mais poderosa do que nos tempos de Carmille.

Não mais cartões perfurados senão a própria biometria, por exemplo
reconhecimento facial, em que o próprio rosto da pessoa é convertido em sua
marca da besta particular, não sendo mais necessária a tatuagem da colônia
penal que agora é reduzida apenas à sua dimensão torturante e traumática.

A figura da máscara de ar é uma alegoria e partida, não de chegada. Enquanto
máscara nos dá ar, mas não podemos nos mascarar -- no sentido de falsear-nos --
para praticar solidariedade ou perderemos nossa autenticidade. A ajuda pode ser
mútua e não apenas unidirecional.

### Sugestões para desenvolvimentos futuros

Finalizar redação inicial:

* Rene Carmille e a Rede Marco Polo da Resistência Antifascista Francesa. Uso de TICs pra resistir.
* Disclaimer básico para os agentes do sistema?
* Rolê em Lins, vazando pra Passárgada: clandestinidade e exílio.
* Estado de exceção (Agamben) e Terror (França contra-revolucionária).
* Incluir legislação, inclusive projetos de lei piorantes (antiterror, rastreamento etc).
* Modelos: Cruz Negra Anarquista, Operações Especiais, etc.
* Criptosindicato: como nos primeiros sindicatos, organização básica para custeio de funerais, pensões, greves.
* Hierarquia, heterarquia, poliarquia, anarquia.
* Duração no tempo, institucionalização, transição geracional.
* O par egoísmo-servidão, ou a servidão voluntária.

### Referências

Recursos:

* [Brasil Hostil](https://brasilhostil.org).
* [OPSEC - ISO 1312](https://opsec.fluxo.info).
* [Guia de Autodefesa Digital](https://manual.fluxo.info).
* [Clipping "Brasil Hostil"](https://links.fluxo.info/tags/brasil%20hostil).

Misc:

* [Villas Bôas é o centro das atenções em evento em Brasília](https://oglobo.globo.com/brasil/villas-boas-o-centro-das-atencoes-em-evento-em-brasilia-22560720).
* [Nada mais representativo do que o beijo do Jungmann na testa do Comandante Villas Boas, um dia depois da sessão do habeas corpus de Lula no STF](https://twitter.com/pedromachadoba/status/982000461409550336).
* [General Paulo Chagas sobre "baderna"](https://twitter.com/GenPauloChagas/status/982061528345083904?s=19).

Fotos:

* [Munição utilizada em assassinato de Marielle Franco é de lotes vendidos à Polícia Federal](http://diariogaucho.clicrbs.com.br/rs/policia/noticia/2018/03/municao-utilizada-em-assassinato-de-marielle-franco-e-de-lotes-vendidos-a-policia-federal-10191622.html).
* [Interventor federal diz que 'Rio é um laboratório para o Brasil'](https://g1.globo.com/rj/rio-de-janeiro/noticia/autoridades-detalham-medidas-da-intervencao-federal-o-rio-de-janeiro.ghtml#).
* [Punch positions in card](http://www.columbia.edu/cu/computinghistory/punch.html).
