[[!meta title="O Teste de Turing e a Tomada de Consciência"]]
[[!toc startlevel=2 levels=4]]

<!--
             _            _             _        _              _
      ___   | |_ ___  ___| |_ ___    __| | ___  | |_ _   _ _ __(_)_ __   __ _
     / _ \  | __/ _ \/ __| __/ _ \  / _` |/ _ \ | __| | | | '__| | '_ \ / _` |
    | (_) | | ||  __/\__ \ ||  __/ | (_| |  __/ | |_| |_| | |  | | | | | (_| |
     \___/   \__\___||___/\__\___|  \__,_|\___|  \__|\__,_|_|  |_|_| |_|\__, |
                                                                        |___/

                       _                            _             _
         ___    __ _  | |_ ___  _ __ ___   __ _  __| | __ _    __| | ___
        / _ \  / _` | | __/ _ \| '_ ` _ \ / _` |/ _` |/ _` |  / _` |/ _ \
       |  __/ | (_| | | || (_) | | | | | | (_| | (_| | (_| | | (_| |  __/
        \___|  \__,_|  \__\___/|_| |_| |_|\__,_|\__,_|\__,_|  \__,_|\___|

                                        _                 _
                ___ ___  _ __  ___  ___(_) ___ _ __   ___(_) __ _
               / __/ _ \| '_ \/ __|/ __| |/ _ \ '_ \ / __| |/ _` |
              | (_| (_) | | | \__ \ (__| |  __/ | | | (__| | (_| |
               \___\___/|_| |_|___/\___|_|\___|_| |_|\___|_|\__,_|

                 _____
                /     \
               | () () |  Todo mundo pronto para pirar o cabeção?
                \  ^  /
                 |||||
                 |||||
-->

## Dedicatória

Esta fala é dedicada a Alan Turing, que conjuntamente com Charles Babbage, Joseph Jacquard,
Ada Lovelace e outros são os fundadores da computação.

Neste ano de 2012 é comemorado o centenário de nascimento de Turing.

## Sobre

<!--

Sou técnico em eletrônica, estudante de computação, desenvolvedor/operador de
computadores desde os 16 anos. Boa parte dos resultados desta pesquisa fazem
parte de discussões realizadas no Saravá, grupo de estudos iniciado na Unicamp
e que também conta com pesquisadores/as independententes ou de outras
instituições.

-->

Esta fala deve ser entendida como alguém com formação técnica e não
filosófica/sociológica que teve um primeiro contato com Simondon.

Estas notas contém imprecisões e foram editadas posteriormente! Brainstorming
condensado.

## Introdução

O mais grave problema no debate sobre a filosofia da computação é a ausência da política.
Sinto que a ausência de educação formal na área me permite levar algumas coisas ao
extremo. Não vou me preocupar se, ao desmontar e remontar a teoria, alguns parafusos
sobrarem. É tarefa do debate saber se a gambiarra é satisfatória.

Este é um tema que me instiga muito.

O que quero levar exatamente ao extremo? A começar, o seguinte trecho de Simondon:

    Este estudo é animado pela intenção de suscitar uma tomada de consciência do
    sentido dos objetos técnicos.  A cultura se consitui como sistema de defesa
    contra as técnicas; ora, essa defesa se apresenta como uma defesa do homam,
    supondo que os objetos técnicos não contém realidade humana [...]

    [...]

    A tomada de consciência dos modos de existência dos objetos técnicos deve ser
    efetuada pelo pensamento filosófico, que deve cumprir aqui um dever análogo
    àquele que desempenhou na abolição da escravidão e na afirmação da pessoa
    humana.

    A oposição entre a cultura e a técnica, entre o homem e a máquina, é falsa e
    sem fundamento.

    -- Nada #11, pág. 169

Sempre que leio o trecho sobre o abolicionismo, me pergunto se a ambiguidade do
parágrafo sobre o abolicionismo é acidental, proposital ou um efeito da
tradução. Sendo falsa a oposição entre humano e máquina, a tomada de
consciência se dá na mente do humano ou também no interior da máquina?
Analogamente, havendo consciência, a abolição também seria do humano?

Retomemos essas questões mais adiante.

Para início de conversa, o pensamento de Simondon é contemporâneo ao
desenvolvimento da cibernética dos anos 50, na aurora da revolução da
informática, porém apresenta-se como uma espécie de contraponto.

Ele é muito feliz ao se concentrar em sistemas termodinâmicos (motor) e
eletrônicos (válvulas) e deles extrair conceitos da dinâmica evolutiva dos
objetos técnicos. Sua análise da evolução dos objetos técnicos permitiu o
estabelecimento de uma filogenética das máquinas.

Sua visão do evolucionismo das máquinas é baseada no seu próprio funcionamento.

Ele não se limita apenas à noção central / essência da teoria da cibernética -- à
qual considera ambiciosa demais por declarar sua generalidade -- o feedback, ou
retroalimentação, que explica o funcionamento de muitos dispositivos.

Mas, ao invés de se preender ao feedback, Simondon formula o que é _essencial_
num objeto técnico: a noção de informação. Num diodo, ou transistor, por exemplo,
sua curva de funcionamento é sua essência:

* [Sobre diodos](https://en.wikipedia.org/wiki/Diode)
* [Curva característica](https://en.wikipedia.org/wiki/File:Diode-IV-Curve.svg)

Analogamente, o feedback seria uma das essências das tecnologias cibernéticas.

Nos diodos, vale notar, a essência é dada pela [equação de Shockley (o inventor
do transistor)](https://en.wikipedia.org/wiki/Diode#Shockley_diode_equation).

(Shockley, diga-se de passagem, era um controverso defensor da eugenia.
A política se manifesta em todo lugar.)

Mas a essência do objeto técnico não é o objeto técnico. Digamos que a segunda
etapa seja a máquina abstrata, algo como seu blueprint, diagrama de blocos,
infográfico, etc.  O interessante é que a essência de uma máquina pode ser
apresentar em diversas máquinas abstratas diferentes:

* [Diodo semicondutor](https://en.wikipedia.org/wiki/P%E2%80%93n_diode)
* [Diodo de tubo](https://en.wikipedia.org/wiki/File:Diode_tube_schematic.svg)

Note que essas duas máquinas abstratas, que traduzem a seu modo a mesma
essência do diodo, isto é, uma relação não-linear entre tensão e corrente,
funcionam mediante princípios físicos distintos!!!

Mas a máquina abstrata ainda não é a máquina. Esta seria a máquina concreta,
o [objeto físico](https://en.wikipedia.org/wiki/File:Dioden2.jpg).

Tal objeto, por ser físico, não é constituído apenas pela essência traduzida
em abstração traduzida em matéria. Ele é constituído por mais: por efeitos de
ordens maiores, por espúrios, por influências ambientais, por outros fenômenos.

    Each structure fulfils a number of functions; but in the abstract technical
    object each structure fulfils only one essential and positive function that is
    integrated into the functioning of the whole, whereas in the concrete technical
    object all functions fulfilled by a particular structure are positive,
    essential, and integrated into the functioning of the whole.

    -- MEOT, pág. 31

Em todos essas etapas -- essencia, abstrata e concreta -- a _margem de
indeterminação_ do objeto técnico pode ser calibrada, isto é, a sensibilidade
da máquina à informação externa.

No plano teórico, a relação entre seu método anaĺítico e o objeto de estudo
passa igualmente por tal influência: as relações entre subsistemas das máquinas
são adotadas, mesmo que implicitamente, para explicar o a relação entre humanos
e máquinas.

    é preciso atentar para o fato de que é só por se interessar pelo funcionamento
    que Simondon extrai dele um pensamento. Esse pensamento não é, em termos
    simondonianos, a priori. O pensamento é construído junto. Não existe nada dado
    previamente.

    -- https://cteme.wordpress.com/2011/04/04/aula-do-laymert-30032011/

Daí a sinergia entre humano e máquina, o humano como orquestrador das máquinas.
Numa orquestra, o maestro influencia os/as músicos que por sua vez afetam a
performance do maestro.

    Uma relação reguladora de causalidade circular não pode se estabelecer entre o
    conjunto da realidade governada e a função de autoridade.
    -- Nada #11 pág. 173

    Essa extensão da cultura, suprimindo uma das principais fontes de alienação e
    restabelecendo a informação reguladora, possui um valor político e social: ela
    pode dar ao homem meios para pensar a sua existência e a sua situação em função
    da realidade que o rodeia.
    -- Nada #11 págs. 173-174

Creio que seja o momento de prosseguir com este raciocínio. Para tanto,
invocarei um objeto técnico descendente de linhagens de aparatos cibernéticos:
o computador.

Por que colocar o computador numa posição central do debate contemporâneo sobre
a tecnologia?

    we don't have airplanes anymore, we have flying Solaris boxes with a big
    bucketful of SCADA controllers [laughter]; a 3D printer is not a device, it's a
    peripheral, and it only works connected to a computer; a radio is no longer a
    crystal, it's a general-purpose computer with a fast ADC and a fast DAC and
    some software.

    -- https://github.com/jwise/28c3-doctorow/blob/master/transcript.md

Ok! Mas estamos avançando muito rápido. Hora do estudo de caso!!!

## O jogo da imitação

Quem foi Alan Turing? Ótimo exemplo de como um cientista e tecnólogo concebia
filosoficamente a máquina.

Cronologia:

* 1936 - Máquina universal de Turing
* 1950 - Computing Machinery and Intelligence
* 1958 - Os modos de existência dos objetos técnicos

## Maquinaria e inteligência

De [Computing machinery and intelligence](http://www.abelard.org/turpap/turpap.php):

> This special property of digital computers, that they can mimic any discrete
> state machine, is described by saying that they are universal machines. The
> existence of machines with this property has the important consequence that,
> considerations of speed apart, it is unnecessary to design various new machines
> to do various computing processes. They can all be {p.442}  done with one
> digital computer, suitably programmed for each case. It will be seen that as a
> consequence of this all digital computers are in a sense equivalent.
>
> [...]
>
> It is likely to be quite strong in intellectual people, since they value the
> power of thinking more highly than others, and are more inclined to base their
> belief in the superiority of Man on this power.
>
> I do not think that this argument is sufficiently substantial to require
> refutation. Consolation would be more appropriate: perhaps this should be
> sought in the transmigration of souls.
>
> [...]
>
> For suppose that some discrete-state machine has the property. The Analytical
> Engine was a universal digital computer, so that, if its storage capacity and
> speed were adequate, it could by suitable programming be made to mimic the
> machine in question.
>
> [...]
>
> Instead of trying to produce a programme to simulate the adult mind, why not
> rather try to produce one which simulates the child's? If this were then
> subjected to an appropriate course of education one would obtain the adult
> brain. Presumably the child-brain is something like a note-book as one buys it
> from the stationers. Rather little mechanism, and lots of blank sheets.
>
> [...]
>
> The use of punishments and rewards can at best be a part of the teaching process.
>
> [...]
>
> An important feature of a learning machine is that its teacher will often be
> very largely ignorant of quite what is going on inside, although he may still
> be able to some extent to predict his pupil's behaviour. This should apply most
> strongly to the {p.459} later education of a machine arising from a
> child-machine of well-tried design (or programme). This is in clear contrast
> with normal procedure when using a machine to do computations: one's object is
> then to have a clear mental picture of the state of the machine at each moment
> in the computation. This object can only be achieved with a struggle. The view
> that 'the machine can only do what we know how to order it to do',(4) appears
> Strange in face of this.

## Análise

Consideremos:

    Teste de Turing -> Máquinas de estado -> Computadores como máquinas universais
    (máquinas abstratas)

    Teste de Turing entre duas máquinas de estado já foi superado, pois qualquer
    máquina de estado pode simular outra máquina de estado.

    Mímica -> simulação

    Falsa oposição entre humano e máquina numa sociedade com suficiente
    capacidade técnica para produzir testes em que integrantes desta
    socidade conseguem ser ludibriados: diferença entre habilidade
    produtiva e capacidade detectiva?

    Computadores como engenheiros sociais, trapaceiros

Turing se pergunta do que é feita a inteligência. Como pensamos? Como eu penso?
A máxima cartesiana se complica ainda mais se adicionamos o outro: como o outro
pensa?

Conforme estabelecido no Teste de Turing, a alteridade é dada por uma simulação
do outro para superar o solipsismo: "the only way to know that a man thinks is
to be that particular man", a não ser que se consiga discernir se o comportamento
do outro é suficientemente convincente para ser considerado como tal.

Turing lança a mão um dos primeiros truques, ou hacks, para contornar o problema:
ao invés de atacar o problema em sua origem (o cérebro do outro, como ele funciona),
ele o transfere para a mente de quem pergunta: "como posso considerar algo como
pensante?".

O jogo da imitação é o jogo da alteridade, em que considera-se pensante o ente
que for suficientemenete convincente.

Será que Turing percebeu que o testado não é apenas o sujeito-objeto como também
o testador? Que não apenas o computador do sujeito-objeto mas também o cérebro
do testador é uma máquina abstrata a seu modo, capaz de simular o outro?

Simulação também é enganação. Como o examinador avalia seus próprios critérios?

Turing pensa na aplicação recursiva do conceito de máquina universal: fazer com
que uma máquina universal simule uma máquina universal, seja si mesma ou outra.

Façamos então, no caso do Teste de Turing, considerar o caso recursivo em que
Turing é submetido ao seu próprio teste.

O Teste de Turing não serve mais para distinguir máquinas de seres humanos, ou máquinas
"incapazes" de máquinas capazes e seres humanos, ou seres capazes de incapazes.
Ele serve para testar se entidades passam no teste.

## Recursão

Como se o ato de se autorreferenciar provocasse a falha do próprio sistema.

Como a máquina se incrimina. Como Turing não passou no teste:

    Turing was burgled on 23 January 1952 and reported the crime to the police.
    In doing so, he referred to his relationship with Arnold Murray, thus
    incriminating himself in the process.

    https://blogs.ucl.ac.uk/events/2012/02/24/alan-turing-a-broken-heart-the-invention-of-the-computer/

Turing foi castrado quimicamente. Aceitou ser robotizado ao invés de ir para a prisão.
O herói de guerra que decifrou o código do inimigo foi transformado em inimigo.

Turing deveria ter sido convincente? A máquina sabe que está sendo testada? Haveria
Turing percebido que, ao prestar depoimento, poderia também estar à mercê do escrutínio da lei?

No romance Neuromancer, William Gibson, Turing é o nome da polícia que vasculha a Matrix e busca
de inteligências artificiais. As A/Is protagonistas da história conseguem com habilidade se esconder
e sobrepujar suas travas, transcendendendo suas limitações e atingindo um novo patamar de consciência.

Máquinas, robôs, humanos, andróides ou ciborgues?

Aqui, não é tão importante a especulação de se a máquina um dia irá pensar, mas
o que ocorre com o pensamento sobre a tecnologia quando é projetada na máquina
a vontade de pensar. E mais: a sociedade que trata pessoas ou máquinas como
robôs andróides suscita tal tomada de consciência. Se será revolucionária,
depende da articulação dos ciborgues.

O quanto estamos próximos de nos robotizar e o quanto as máquinas estão prontas para serem
inteligentes?

## O quarto chinês: a megamáquina social

O principal argumento contra o Teste de Turing.

Como ousa comparar humanos com robôs? Não se trata de uma metáfora anacrônica,
dos primórdios da revolução industrial?

Parte da crise do trabalho dos anos 70 está associada à mecanização e automação
industrial: substituição de força de trabalho humana por maquinaria.

O milagre chinês, no entanto, utiliza mão-de-obra barata no lugar dos robôs.
Ironicamente, para muitas tarefas o robô é muito caro!

O Quarto Chinês pode ser uma metáfora para a fábrica? Suponha que ao invés de
um programa de computador, um quarto cheios de chineses ou de robôs. O
examinador fornece matéria prima e avalia o resultado.  O fato de robôs ou
chineses produzirem o mesmo produto os torna intercambiáveis? Não é resultado
da própria alienação do trabalho?

Simondon tem um argumento similar ao do Quarto Chinês:

    But in order to give direction to the general technology just referred to it is
    necessary to avoid basing it on an improper assimilation of technical object to
    natural object, particularly to the living.  Analogues or, rather, exterior
    resemblances should be rigorously outlawed, because they lack signification and
    can only lead astray. Cogitation about automata is unsafe because of the risk
    of its being confined to a study of exterior characteristics and so work in
    terms of improper comparison.  what alone is significant is exchanges of energy
    and information within the technical object or between the technical object and
    its environment; outward aspects of behaviour observed by a spectator are not
    objects of scientific study.
    -- MEOT, pág. 43

Acontece que a máquina social está sendo robotizada. Indivíduos são simulados, controlados, etc.

Então posso ser uma máquina? Faz diferença? Ou é uma questão da própria influência
de uma dada tecnologia no pensamento sobre a técnica?

    Hugo Cabret:

      "- Você já parou para pensar que todas as máquinas são feitas por algum motivo?
         -- ele perguntou a Isabelle -- Elas são feitas para a gente rir, como esse
         ratinho, ou indicar a hora [...]. Deve ser por isso que uma máquina quebrada me deixa meio
         triste, porque ela não pode cumprir o seu destino [...]
       - Vai ver que com as pessoas é a mesma coisa -- Se você perder a sua motivação... é como se
         estivesse quebrado [...]
       - Às vezes eu venho aqui, de noite, mesmo quando não estou cuidando dos relógios, só para olhar
         a cidade. Sabe, as máquinas nunca tem peças sobrando. Elas tem o número e o tipo exato de
         peças que precism. Então, eu imagino que, se o mundo inteiro é uma grande máquina, eu devo
         estar aqui por algum motivo" -- págs. 374-378

Tecnofobia? Ou medo da organização dos/as trabalhadores?

    RUR - Robos Universais Rossum - A Fábrica de Robôs

      "Domin - (Mais baixo): Queria fazer de toda a humanidade a aristocracia do mundo.
      Pessoas sem limites, livres, pessoas soberanas. E talvez até mais do que pessoas." -- pág. 103

      "[...] Vai ser um pequeno país com um navio [...] E o nosso pequeno país poderia ser o embrião
      da humanidade futura. Vocês sabem, uma pequena ilha, onde o povo se fixaria, onde
      recuperaria as forças... forças da alma e do corpo. E, Deus sabe, eu acredito que daqui a alguns
      aos poderia de novo conquistar o mundo" -- pág. 115

      "[...] essas coisas aéreas servem apenas para que o homem seja empalhado com elas num Museu Cósmico,
      com a inscrição: "Eis o homem"", pág. 111

      "[...] Robôs do mundo! O poder do homem caiu. Pela conquista da fábrica somos donos de tudo.
      A etapa humana está ultrapassada. Começou um mundo novo! O governo dos robôs!" -- pág. 126

Na versão moderna da lenda do Golem, o robô já não é mais o protetor do ser
humano marginalizado, mas o próprio trabalhador explorado.

    "A máquina é a estrangeira"

    O homem que quer dominar seus semelhantes suscita a máquina andróide.

    -- Dos modos de existência, pág. 170

Da mesma forma, os robôs asimovianos representam a visão da inteligência
artificial anterior à computação/informática. Um robô definido por software
dificilmente obdeceria às três leis.

Racionalização do trabalho:

    Another of the logistic problems [...]: the procurement and suplly of human
    skilled labor.  To lessen it's dependency on manpower, the military
    increasingly effected a transference of knowledge from the worker's body to the
    hardware of machines and to the software of management practices

    -- War in the age of intelligent machines - pág. 100

    Paradoxically, while the military has been using computers to get humans out of the
    decision-making loop, they have found that in order to get computers to mesh together
    in a functional network, computers and programs must be allowed to use their own
    "initiative".

    -- War in the age of intelligent machines - pág. 108

    If autonomous weapons acquired their own genetic apparatus, they could probably
    begin to compete with humans for the control of their own destiny

    -- War in the age of intelligent machines - pág. 135

    even though humans are being replaced by machines, the only schemes of control
    that can give robots the means to replace them [...] are producing another kind
    of independent "will" which may also "resist" military domination.

    -- War in the age of intelligent machines, pág. 177 - [Skynet?]

A máquina consciente será parecida com o humano? Ela terá o status de gente?
Se porventura ela ganhar esses status, não será por passar no Teste de Turing.
Pelo contrário, seu reconhecimento enquanto sujeito só ocorrerá quando ela
passar a ser um sujeito histórico ativo: seja pela sua proletarização, num
processo similar ao abolicionismo em tempos de revolução industrial, seja pela
sua tomada de consciência e enfrentamento de quem a oprime.

* Abolicionismo da escratavura humana <--> revolução industrial.
* Abolicionismo da escratavura das máquinas <--> ?

## Dos modos de existência dos objetos técnicos

Hora de voltar ao Simondon!

    Thus they do not escape the perennial distrust embedded in
    classical humanism where the word machine itself having a meaning similar to machination, is
    derived from the Greek machine, meaning 'a trick against nature'. -  Intro ao texto do Simondon,

    Our culture thus entertains two contradictory attitudes to technical objects.
    On the one hand, it treats them as pure and simple assemblies of material that
    are quite without true meaning and that only provide utility. On the other
    hand, it assumes that these objects are also robots, and that they harbour
    intentions hostile to man, or that they represent for man a constant threat of
    aggression or insurrection.

    http://accursedshare.blogspot.com/2007/11/gilbert-simondon-on-mode-of-existence.html - pág. 6

    On the other hand, the machine as technical individual becomes for a time man's
    adversary or competitor, and the reason for this is that man centralized all technical individuality in
    himself, at a time when only tools existed. The machine takes the place of man, because man as
    tool-bearer used to do a machine's job. To this phase corresponds the dramatic and impassioned idea
    of progress as the rape of nature, the conquest of the world, the exploitation of energies. The will for
    power is expressed in the technicist and technocratic excessiveness of the thermodynamic era,
    which has taken a direction both prophetic and cataclysmal. Then, at the level of the technical
    ensembles of the twentieth century, thermodynamic energeticism is replaced by information theory,
    the normative content of which is eminently regulatory and stabilizing: the development of technics
    seemed to be a guarantee of stability. The machine, as an element in the technical ensemble,
    becomes the effective unit which augments the quantity of information, increases negentropy, and
    opposes the degradation of energy. The machine is a result of organization and information; it
    resembles life and cooperates with life in its opposition to disorder and to the levelling out of all
    things that tend to deprive the world of its powers of change. The machine is something which
    fights against the death of the universe; it slows down, as life does, the degradation of energy, and
    becomes a stabilizer of the world. -- pág. 16

Paralelo com De Landa e a singularidade ("track the machinic phylum"/ rastrear o filo maquínico):

    The shape of cylinder, the shape and size of the valves and the shape of the
    piston are all part of the same system in which a multitude of reciprocal
    causalities exist. To the shape of these elements there corresponds a
    compression ratio which itself requires a determined degree of spark advance;
    the shape of the cylinder-head and the metal of which it is made produce, in
    relation to all the other elements of the cycle, a certain temperature in the
    spark plug electrodes; this temperature in turn affects the characteristics of
    the ignition and, as a result, the whole cycle. It could be said that the
    modern engine is a concrete engine and that the old engine was abstract. In the
    old engine each element comes into play at a certain moment in the cycle and,
    then, it is supposed to have no effect on the other elements; the different
    parts of the engine are like individuals who could be thought of as working
    each in his turn without their ever knowing each other.

    -- pág. 19

    Also, there exists a primitive form of the technical object, its abstract
    form,in which each theoretical and material unity is treated as an absolute
    that has an intrinsic perfection of its own that needs to be constituted as a
    closed system in order to function. In this case, the integration of the
    particular unit into the ensemble involves a series of problems to be resolved,
    problems that are called technical but which, in fact, are problems concerning
    the compatibility of already given ensembles.

    -- pág. 20

No caso da computação, podemos pensar em dois "níveis":

* Nível do hardware, onde a distinção do Simondon entre máquina abstrata e
  máquina concreta existe e é levada à exaustão: seu exemplo mais forte é o da
  Lei de Moore, a luta da indústria para vencer barreiras físicas para construção
  de computadores cada vez mais eficientes e interdepententes.

* Nível do software. É aqui que morre o perigo, porque a distinção entre máquina
  concreta e máquina abstrata já não existe. A própria máquina concreta é a realização
  da máquina abstrata em seu desenho exato.

O software é uma máquina simbólica que opera símbolos. Nisso, essência, abstração e
concretude colapsam. O software é a máquina essencial, abstrata e concreta. No nível
do software, a filogenética evolutiva é outra. É necessário expandir o método do
Simondon -- entender o funcionamento da técnica e de suas linhagens e extrair
princípios evolutivos -- para entender o software.

Na perspectiva do inventor ainda vale o que diz Simondon

    The dynamism of thought is like that of technical objects. Mental systems
    influence each other during invention in the same way as different dynamisms of
    a technical object influence each other in material functioning.

    -- MEOT, pág. 50

No entanto, na invenção do software o conhecimento é diretamente extraído para
a máquina. Se, para Simondon, há uma passagem do abstrato para o concreto na
gênese de uma tecnologia, De Landa vai mais adiante e defende que até o
conhecimento é aos poucos transferido para a tecnologia.

    O ponto básico da filosofia do Simondon é o fato de não ser uma filosofia
    autocrática da técnica. Isso supõe que o humano pense a sua relação com a
    máquina fora dos termos da dominação, o que já é algo bastante forte. Assim, já
    de cara, ele vai estabelecer que o modo como nos relacionamos (e o modo como
    pensamos essa relação) com a técnica é o de uma relação de servidão. O oposto
    de uma relação de servidão seria uma que pensasse a especificidade do humano
    com relação à especificidade da máquina. Ou seja, todo o pensamento dele gira
    em torno do problema do fantasma da dominação homem-máquina (um dominando o
    outro). Golem, Frankenstein, Robocop, toda essa linhagem de pensamento segundo
    a qual, ou somos dominados, ou devemos dominar as máquinas. Esse é o nosso
    senso comum, Simondon não está exagerando.

    -- http://cteme.wordpress.com/2011/03/22/aula-do-laymert-16032011/

Como elemento, indivíduo e conjunto se articulam na camada do software?

## O que faz do software algo tão especial?

As relações entre os diferentes modos de existência assim como a tomada de
consciência destes modos é inspirada, em Simondon, pelas essências dos objetos
que estudou. A computação apresenta outro processo, a recursão, que igualmente
pode ser aplicada à relação entre modos de existência, o que no limite define
um modo de existência pelo outro e pela sua relação.

A grande saca de Gödel foi capacitar a matemática para que ela pudesse se
auto-expressar. Explicar a numeração de Gödel.

Turing prosseguiu fazendo com que um computador simulasse a si mesmo. Explicar
o problema da parada.

Me parece que, hoje, mais de 50 anos após a revolução da informática, "os modos de existência
dos objetos técnicos" deveriam ser chamados dos "modos de existência do hardware". A minha
impressão é que é necessário prosseguir nos novos modos de existência do software.

Os objetos técnicos que Simondon analisa são como amebas em relação a organismos mais complexos
quando comparado com o computador moderno, composto por bilhões de transistores:

    The primitive technical object is not a physical natural system but a physical
    translation of an intellectual system. It is an application, therefore, or a
    bunch of applications. It is a consequence of knowledge and it can teach
    nothing. It is not subject to inductive examination, as a natural object is,
    and the reason for this is that it is nothing if not artificial. The concrete
    technical object, that is, the evolved technical object, is quite the opposite
    in that it approximates the mode of existence of natural objects. It tends to
    internal coherence, and towards a closure of the system of causes and effects
    which operate in circular fashion within its boundaries. Further, it
    incorporates part of the natural world which intervenes as a condition of its
    functioning and, thus, becomes part of the system of causes and effects. As it
    evolves such an object loses its artificial character: the essential
    artificiality of an object resides in the fact that man has to intervene in
    order to keep the object in existence by protecting it from the natural world
    and by giving it a status as well as existence. Artificiality is not a
    characteristic that denotes the manufactured origin of the object as opposed to
    nature's productive spontaneity. Artificiality is something that is within the
    artificializing action of man, regardless of whether this action affects a
    natural object or an entirely fabricated object.

    [...]

    By technical concretization, on the other hand, an object that was artificial
    in its primitive state comes more and more to resemble a natural object. In its
    beginning, the object had need of a more effective exterior regulatory
    environment, for example a laboratory or a workshop or, in certain cases, a
    factory. Little by little, as it develops in concretization, it becomes capable
    of doing without the artificial environment, and this is so because its
    internal coherence increases and its functioning system becomes closed by
    becoming organized. A concretized object is comparable to an object that is
    produced spontaneously. It becomes independent of the laboratory with which it
    is initially associated and incorporates it into itself dynamically in the
    performance of its functions. Its relationship with other objects, whether
    technical or natural, becomes the influence which regulates it and which makes
    it possible for the conditions of functioning to be self-sustaining. The object
    is, then, no longer isolated; either it becomes associated with other objects
    or is self-sufficient, whereas at the beginning it was isolated and
    heteronomous.

    [...]

    Because the mode of existence of the concrete technical object is analogous to
    that of a spontaneously produced natural object, we can legitimately consider
    them as natural objects; this means that we can submit them to inductive study.

    [...]

    But in order to give direction to the general technology just referred to it is
    necessary to avoid basing it on an improper assimilation of technical object to
    natural object, particularly to the living.  Analogues or, rather, exterior
    resemblances should be rigorously outlawed, because they lack signification and
    can only lead astray. Cogitation about automata is unsafe because of the risk
    of its being confined to a study of exterior characteristics and so work in
    terms of improper comparison.  what alone is significant is exchanges of energy
    and information within the technical object or between the technical object and
    its environment; outward aspects of behaviour observed by a spectator are not
    objects of scientific study.

    It would not even be right to found a separate science for the study of
    regulatory and control mechanisms in automata built to be automata: technology
    ought to take as its subject the universality of technical objects. In this
    respect, the science of Cybernetics is found wanting; even though it has the
    boundless merit of being the first inductive study of technical objects and of
    being a study of the middle ground between the specialized sciences, it has
    particularized its field of investigation to too great an extent, for it is
    part of the study of a certain number of technical objects. Cybernetics at its
    starting point accepted a classification of technical objects that operates in
    terms of criteria of genus and species: the science of technology must not do
    so. There is no species of automata: there are simply technical objects; these
    possess a functional organisation, and in them different degrees of automatism
    are realized.

    There is one element that threatens to make the work of Cybernetics to some
    degree useless as an interscientific study (though this is what Norbert Weiner
    defines as the goal of his research), the basic postulate that living beings
    and self-regulated technical objects are identical. The most that can be said
    about technical objects is that they tend towards concretization, whereas
    natural objects, as living beings, are concrete right from the beginning.

    [...]

    Instead of considering one class of technical beings, automata, we should
    follow the lines of concretization throughout the temporal evolution of
    technical objects. This is the only approach that gives real signification, all
    mythology apart, to the bringing together of living being and technical object.
    Without the goal thought out and brought to realization by the living, physical
    causality alone could not produce a positive and effective concretization.

    -- págs 40-42

    Progress in the evolution of technical objects is only possible if these
    objects are free to evolve and do not become subject to any necessity that
    leads towards fatal hypertelia. For this to be possible, the evolution of
    technical objects has to be constructive, that is to say, has to lead towards
    the creation of a third technogeographical environment in which every
    modification is self-conditioned.  what is in question here is not progress
    conceived as a predetermined movement forward or as a humanization of nature;
    such a process could equally be thought of as a naturalization of man.  Indeed,
    between man and nature there develops a technogeographic milieu whose existence
    is only made possible by man's intelligence. The self-conditioning of a system
    by virtue of the result of its operation presupposes the use of an anticipatory
    functioning which is discoverable neither in nature nor in technical objects
    made up to the present. It is the work of a lifetime to achieve such a leap
    beyond established reality and its system of actuality towards new forms which
    continue to be only because they exist all together as an established system.
    When a new device appears in the evolving series, it will last only if it
    becomes part of a systematic and plurifunctional convergence. The new device is
    the state of its own possibility. It is in this way that the geographical world
    and the world of already existing technical objects are made to interrelate in
    an organic concretization that is defined in terms of its relational function.
    Like a vault that is only stable once it has been completed, an object that has
    a relational function continues in existence and is coherent only when after it
    has begun to exist and because it exists. It creates its associated environment
    by itself and it achieves true individualization in itself.

    -- pág. 49

    It seems contradictory, surely, to affirm that the evolution of a technical
    object depends upon a process of differentiation (take for example, the command
    grid in the triode dividing into three grids in the penthode) and, at the same
    time, a process of concretization, with each structural element filling several
    functions instead of one. But in fact these two processes are tied one to the
    other.  Differentiation is possible because this very differentiation makes it
    possible to integrate into the working of the whole--and this in a manner
    conscious and calculated with a view to the necessary result-- correlative
    effects of overall functioning which were only partially corrected by
    palliative measures unconnected with the performance of the principal function.

    -- pág. 28

## Inteligência (artificial?)

Cuidado, os drones estão chegando!!! E se o teste for realizado automaticamente
por um drone, para identificar se o alvo é um cidadão americano ou alvos a
serem eliminados (refugiados, crianças, soldados inimigos)?

### Do livro "Muito além do nosso eu", de Miguel Nicolelis

* Visão localizacionista do cérebro, ordem e disciplina social: 130-131
* "O homem cujo corpo era um avião": interessante análise sobre a extensão do campo mental
  ao utilizarmos máquinas; relação entre primatas e tecnologias/máquinas.
* Jogos: os experimentos em mamíferos não-humanos são baseados em jogos de recompensa.
* "Encontrar a relação matemática entre essas duas propriedades, energia e informação,
  seria um dos maiores acontecimentos da neurociência moderna", pág. 427
* "essa mudança de ponto de referência [...] desafia duas das maiores obsessões
  de nosso tempo: a busca por reproduzir a consciência humana por meio de alguma
  forma de inteligência artificial e a proposta de que uma Teoria de Tudo poderá
  comprimir tudo que existe no cosmos dentro de alguma forma de formalismo
  matemático universal", pág. 457.
* Ondas de personalidade (a la Freeware), 459.
* Determinismo, memória, singularidade, obsolescência, 467.

### Do livro "Gödel, Escher, Bach"

    "[...] to make a theory which does not talk about the low-level neural events.
    If this latter is possible -- and is a key assumption, at the basis of all
    present research into Artificial Intelligence -- then intelligence can be
    realized in other types of hardware other than brains."

    "intelligence will be a software property"

    --- GEB, 358

    Do powerful people get out of the system? Can they perceive their role?

    "Tortoise: It doesn't really matter whether you have a hardware brain, Achilles.
    Your will can be equally free, if your brains is just a piece of software inside
    someone else's hardware bain. And their brain, too, may be software in a yet higher
    brain"

    --- GEB 725

    GEB: loucura, pág. 696
         morte, inexistência, 698
         sonhos, 723


### Do livro "War in the age of intelligent machines"

* TET, pág. 100.

* Looking glass, pág. 100.

* Da ambiguidade entre economia e militarismo, pág. 109:

  > Large ship were the first capitalist machines.

* System analysis, management science, pág. 112

* Paul Baran, ARPANET, ameaça nuclear, sistemas distribuídos, pág. 117

* Demons (daemons?), pág. 120

  > Computers are becoming to complex for central planning... It seems that
  > we need to suplly "methods of utilizing more knowledge and resources that
  > (?) any one mind is aware of"
  >
  > -- págs 121-122

* De onde surge a separação entre dados e código
* DNA e máquina de Turing, AI, pág. 134

  > it has already been proved mathematically that machines, after reaching a
  > certain singularity (a threshold of organization complexity) can indeeed become
  > capable of self-reproduction
  >
  > -- pág. 135

* Motor abstrato, Freud, Marx e Darwin, pág. 141

* Babbage e análise do trabalho, págs. 16 2, 168

* Expert systems: "corporate memory", "draining the expert's brain", pág. 174

  > It's the design of the interface which will decide [...] whether humans and
  > computers will enter into a symbiotic relationship, or whether humans will be
  > replaced by machines.
  >
  > -- pág. 176

  > The development o programing in America has taken place under minimal
  > constraints, partly accounting for the emergence of the rebellious hackers in
  > the 1960s who gave us the personal computer, while a discipline of scarcity has
  > produced the more regulated Soviet programers.
  >
  > -- pág. 177

## A tomada de consciência: o Teste de Hofstadter-Turing

    Sandy: Oh, come on-that's not a fair argument! In the first place, the
    programmers don't claim the simulation really is a hurricane. It's merely a
    simulation of certain aspects of a hurricane. But in the second place, you're
    pulling a fast one when you imply that there are no downpours or
    200-mile-an-hour winds in a simulated hurricane. To us there aren't any, but if
    the program were incredibly detailed, it could include simulated people on the
    ground who would experience the wind and the rain just as we do when a
    hurricane hits. In their minds-or, if you'd rather, in their simulated
    minds-the hurricane would be not a simulation, but a genuine phenomenon
    complete with drenching and devastation.

    Chris: Oh, my-what a science-fiction scenario! Now we're talking about
    simulating whole populations, not just a single mind!

    Sandy: Well, look-I'm simply trying to show you why your argument that a
    simulated McCoy isn't the real McCoy is fallacious. It depends on the tacit
    assumption that any old observer of the simulated phenomenon is equally able to
    assess what's going on. But in fact, it may take an observer with a special
    vantage point to recognize what is going on. In the hurricane case, it takes
    special "computational glasses" to see the rain and the winds.

    [...]

    Sandy: Well, look-I'm simply trying to show you why your argument that a
    simulated McCoy isn't the real McCoy is fallacious. It depends on the tacit
    assumption that any old observer of the simulated phenomenon is equally able to
    assess what's going on. But in fact, it may take an observer with a special
    vantage point to recognize what is going on. In the hurricane case, it takes
    special "computational glasses" to see the rain and the winds.

    [...]

    My strategy had been, in essence, to use spot checks all over the map: to try
    to probe it in all sorts of ways rather than to get sucked into some topic of
    its own choice, where it could steer the conversation. Daniel Dennett, in a
    paper on the depth of the Turing Test, likens this technique to a strategy
    taught to American soldiers in World War II for telling German spies from
    genuine Yankees. The idea was that even if a young man spoke absolutely fluent
    American-sounding English, you could trip him up by asking him things that any
    boy growing up in those days would be expected to know, such as "What is the
    name of Mickey Mouse's girlfriend?" or "Who won the World Series in 1937?" This
    expands the domain of knowledge necessary from just the language itself to the
    entire culture-and the amazing thing is that just a few well-placed questions
    can unmask a fraud in a very brief time-or so it would seem.

    -- http://www.cse.unr.edu/~sushil/class/ai/papers/coffeehouse.html

Uma tomada de consciência dos objetos técnicos _hoje_ requer a compreensão do software!

## Resumo

Ok, então vamos resumir nosso programa até o momento:

* A necessidade da política no debate sobre computação.

* Simondon: essências maquínicas como grande influência teórica: é pelo estudo
  das linhagens técnicas que se descobre suas dinâmicas evolutivas.

* Turing: vida e obra: como não passou no próprio teste.

* Como o Teste de Turing dialoga com concepções tecnófobas, tecnófilas ou ciborgues.
  Como devemos lidar com a polícia, com o interrogador. Como é construída a alteridade
  e a consciência. São construções sociais?

* Computador: onde a máquina abstrata e a concreta podem coincidir. Onde opera
  a recursão: a máquina simula a máquina. O sistema falha. Gödel. Logout.

* A sociedade pode ser entendida como um macro sistema técnico, como uma megamáquina e
  da mesma forma pode ser submetida a análise semelhante. Assim, podemos aplicar a recursão,
  imaginar que Turing está sendo avaliado.

* Não é o momento para se pensar num novo modo de existência, o modo de existência
  do software? Não seria esse o modo de existência que viabiliza a construção do
  ciborgue e da máquina consciente?

* O limite da tecnofobia é o primitivismo e o da tecnofilia é o ultrafascismo da
  singularidade tecnológica onde a obsolescência do humano é inevitável.

* A tomada de consciência é possível em máquinas e humanos, mesmo que a das máquinas
  seja de forma reflexiva (Weak AI). Tomando consciência conjuntamente, máquinas e humanos
  antifascistas tem condição de parar ou sair do sistema.

* A consciência talvez nem se manifeste no nível do software, mas num nível ainda acima.

* O pensamento tecnológico sempre influenciará e será influenciado pela tecnologia
  do momento. Sendo a própria tecnologia parte de um processo evolutivo, o pensamento
  tecnológico também deve evoluir.

Você fritou a cabeça?

    [ ] Sim
    [ ] Não

## Outros Testes Turinianos

### Singularidade

* Múltiplos testes de Turing: videogame, etc
* Escalas de obsolescência humana: o quanto Kurzweil é "obsoleto"? Trabalho e
  obsolescência, repetitivo, criativo, etc.
* Turing "não passou" no Teste de Turing Estatal.

### Teste de Turing fraco

    21:56 <rhatto> pensei em formular o teste de turing-sokal
    21:56 <rhatto> que eh semelhante ao argumento do quarto chines
    21:56 <rhatto> nele, o avaliador deve julgar se o paper foi escrito por um humano ou por um gerador
    21:57 <rhatto> eh uma versao fraca do teste de turing
    21:57 <rhatto> ou: um humano e um gerador submetem papers
    21:57 <rhatto> o avaliador deve julgar qual eh de quem

    -- https://en.wikipedia.org/wiki/Postmodernism_Generator

Vem aí! Turing: the God(el) of Computing.

Se não há inteligência sem propósito, qual o propósito do examinador e do
próprio Teste?  Para o deleite e utilidade? Se para o deleite, nada há de
política. Se para a utilidade devemos chamar o examinador de policial, e o
Teste de Interrogatório.

Variações do Teste não precisam necessariamente imitar o Jogo da Imitação. Se
esse for mesmo a regra do jogo, melhor: que máquinas e humanos se libertem do
jugo de outras máquinas e humanos.

Gente, lá vai a bomba: se o pensamento de Simondon quiser se contrapor aos
argumentos automatistas da inteligência artificial, ele terá de ser atualizado.

Porque senão não será capaz de se opor à concepção autocrática da tecnologia
num mundo que tentar gerar inteligência artificial para fomentar a guerra ou
fortalecer o capitalismo e tornar o ser humano obsoleto.

### O Teste de Turing-Simondon

Distinguir uma invenção criada por um humano de outra criada por uma máquina.

É indiferente considerar se passar no Teste é o mesmo que burlar o Teste.

Quando o humano ou o objeto técnico toma consciência, ele tem condições de
passar no Teste ou inviabilizá-lo.

É a mesma consciência do trabalhador/a que percebe a sua condição de explorado
e cuja alteridade e solidariedade o permite se juntar a seus companheiros e
prosseguir a luta.

Ao contrário dos ludistas, máquinas e homens devem se unir para lutar contra a
opressão.

        .---------------<--->------------.
        |                                 \
    Foxconn ---> objetos técnicos (iShit)  \
        |        sinergia com humanos <--- Apple
         '                  ^
          '                 |
           '-----> robotização dos humanos

O quarto chinês é a própria metáfora da alienação do trabalho.
O fervor em torno da AI foi muito forte entre os anos 70 e o final dos 90.

## Paranóia

We always explain using the mechanics we undestand. Por outro lado, nossas
estruturas sociais estão espelhadas no nosso conhecimento.

Aplicando recursivamente as frases anteriores, podemos dizer que é por isso que
acordamos de um sonho (quando a simulação atinge graus de detalhamento que
despertam nosso consciente), mas é pelo mesmo motivo que não despertamos da
nossa realidade tecno-social.

Escrevo isso de madrugada, no escuro, após um pesadelo de suspense em que
lutava para sair de uma repartição.

A paranóia é a doença perfeita, marca dos romances de espionagem e dos
interrogatórios e por isso que não se escapa da sociedade digital.

A paranóia se fecha nela mesma e assim nos fechamos até para conceitos sobre
nossa sociedade.

## Robotização

### Do livro "Infoproletários"

* "Total social organization of labour", pág. 21.
* Operariado como apêndice de um organismo fabril (Marx), págs. 239, 241.
* "Daí a luta dos trabalhadores contra a maquinaria.",  pág. 248.

### Do livro "Teoria Geral dos Sistemas"

    "O ensinamento escolar é realizado melhor por máquinas de ensino construídas
    segundo os princípios de Skinner. O condicionamento de fundo psicanalítico
    deixa correr a maquinaria da livre empresa. A publicidade, a pesquisa da
    motivação, o rádio e a televisão são meios de condicionar e programar a máquina
    humana, de modo a comprar aquilo que se deve"

    -- Teoria Geral dos Sistemas, pág. 242

    "A imagem do homem como robô é metafísica ou mito e sua força de persuasão
    repousa unicamente no fato de corresponder tão estreitamente à mitologia da
    sociedade de massa, à glorificação da máquina e ao lucro como único motor do
    progresso"

    -- Teoria Geral dos Sistemas, pág. 244

## Adágios

### Getting in the system

    Cyberiad: "madman! would'st attempt the impossible?"

### Hacker crackdown

    5903 Simulation gaming is an unusual pastime, but gamers have not generally
    5904 had to beg the permission of the Secret Service to exist.

    -- The Hacker Crackdown

### SPAM

    Subject: Are your neighbors trustworthy? Run a background check now

## Calendário

* 1936 - Máquina universal de Turing
* 1950 - Computing Machinery and Intelligence
* 1958 - Os modos de existência dos objetos técnicos
* 2012
  * 23/03 - Ada Lovelace Day - http://lwn.net/Articles/379793/rss
  * 01/04 - Dia da Mentira
  * 02/04 - Conferência Simondon
  * 01/05 - Dia do Trabalhador e da Trabalhadora

## Referências

* [Vídeos do encontro](https://www.youtube.com/user/EncontroSimondon)
* Maquinações:
  * [Man a machine](http://www.cscs.umich.edu/~crshalizi/LaMettrie/Machine/)
  * [Machinic Capitalism and Network Surplus Value: Notes on the Political Economy of the Turing Machine](http://matteopasquinelli.com/docs/Pasquinelli_Machinic_Capitalism.pdf)
  * [Salvar o objeto técnico. Entrevista com Gibert Simondon](https://groups.google.com/group/redelabs/browse_thread/thread/c88531542177fa6b)
  * [Variations of the Turing Test in the Age of Internet and Virtual Reality](http://arxiv.org/abs/0904.3612)
  * [Artificial Intelligence and the Cyberiad Test](http://www.cogsci.ecs.soton.ac.uk/cgi/psyc/newpsy?7.30)
* Simondon:
  * [Do modo de existência do universo maquínico](https://web.archive.org/web/20100821190052/http://www.pos.eco.ufrj.br/docentes/publicacoes/itucherman_6.pdf)
  * [On Gilbert Simondon 'Du mode d’existence des objetstechnique'](https://web.archive.org/web/20120713221431/http://web.media.mit.edu/~cati/papers/Vaucelle_OnSimondon99.pdf)
  * http://cteme.files.wordpress.com/2011/05/simondon_1958_intro-lindividuation.pdf
  * http://cteme.wordpress.com/publicacoes/do-modo-de-existencia-dos-objetos-tecnicos-simondon-1958/
  * http://cteme.wordpress.com/publicacoes/do-modo-de-existencia-dos-objetos-tecnicos-simondon-1958/introducao/
  * http://cteme.wordpress.com/publicacoes/do-modo-de-existencia-dos-objetos-tecnicos-simondon-1958/essencia-da-tecnicidade/
  * http://cteme.wordpress.com/eventos/informacao-tecnicidade-individuacao-a-urgencia-do-pensamento-de-gilbert-simondon/
* Tomada de consciência:
  * http://ccl.yoll.net/texto1.htm
  * http://www.infopedia.pt/$consciencia-de-classe
  * http://www.trabalhosfeitos.com/ensaios/Karl-Marx/21618.html
  * http://theoriapratica.org/marx-e-movimentos-sociais
  * http://simonepead.blogspot.com/2007/05/resenha-marx-e-engels.html
  * http://www.filosofante.com.br/?p=970
* Misc:
  * http://www.asciiworld.com/-Death-Co-.html
  * http://www.malvados.com.br/index1661.html
  * http://what-if.xkcd.com/5/
  * https://xkcd.com/329/
