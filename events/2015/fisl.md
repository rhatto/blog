[[!meta title="A Internet das Coisas Livres vencendo os gigantes da vigilância e do controle"]]

Está em curso uma batalha contra a capacidade das pessoas armazenarem e
processarem sua própria informação. Tanto a privacidade quanto as liberdades
fundamentais do software livre estão ameaçadas pela computação de nuvem e
dispositivos pessoais cada vez mais restritos.

Estamos atingindo o ponto sem retorno? Ou é possível reunir esforços e combater
essa tendência na prática com mini-servidores e serviços distribuídos?

Iniciativas concretas e possibilidades são apresentadas durante a palestra.

* FISL16 - 10/07/2015
* rhatto.fluxo.info/fisl / rhatto @ riseup.net
* `66CA 01CE 2BF2 C9B7 E8D6  4E34 0546 8239 64E3 9FCA`
* [Slides](slides.pdf).

# Distopia

\centerline{
  \includegraphics[height=1.5in]{images/apple_1984_ad_5.jpg}
}

<!--
![The Ware Tetralogy](images/apple_1984_ad_5.jpg)
-->

# Abismo

\centerline{
  \includegraphics[height=1in]{images/hacking_team_hacked-600x375.jpg}
}

<!--
![HackedTeam](images/hacking_team_hacked-600x375.jpg)
-->

<!--
    [...] we must see to it that this agency and all agencies that possess this
    technology operate within the law and under proper supervision so that we
    never cross over that abyss. That is the abyss from which there is no return.

    Senator Frank Church on NSA, 1975.
-->

    [...] temos que nos certificar que esta e todas
    as agências que possuem esta tecnologia operem dentro
    da lei e dentro de uma supervisão apropriada para que
    nunca cruzemos tal abismo. Aquele é o abismo do qual
    não há retorno.

    -- Senador Frank Church sobre a NSA, 1975.

# Utopia

\centerline{
  \includegraphics[height=2.5in]{images/Gnu_meditate_levitate.png}
}

\centerline {
  FSF e GNU: 30 anos (1984/85)
}

<!--
![GNU](images/Gnu_meditate_levitate.png)

FSF e GNU: 30 anos (1984/85).
-->

# A distopia de hoje

\centerline{
  \includegraphics[height=1in]{images/PRISM_logo_(PNG).png}
  \includegraphics[height=1in]{images/1423182746216.jpg}
}

<!--
![Teletela Samsung](images/1423182746216.jpg)
-->

* Internet das Coisas: terminais de acesso à Matrix com vigilância ubíqua.
* Vigilância de massa como modelo de governo e negócios.
* Terceirização: Anything As A Service.
* OpenCore e derivados: software proprietário 2.0.

# O Abismo de hoje

\centerline{
  \includegraphics[height=2in]{images/screenshot14.png}
  \includegraphics[height=2in]{images/il_570xN424040343_oha3.jpg}
}

<!--
![Psywar](images/screenshot14.png)
![Philip K. Dick](images/il_570xN424040343_oha3.jpg)
-->

\centerline {
  Somos a última geração livre?
}

# Calma!

\centerline{
  \includegraphics[height=3in]{images/keep-calm-and-punk-s-not-dead-1.png}
}

<!--
![Punks not dead](images/keep-calm-and-punk-s-not-dead-1.png)
-->

# A utopia de hoje

\centerline{
  \includegraphics[height=1.5in]{images/TheWares.jpg}
}

<!--
![The Ware Tetralogy](images/TheWares.jpg)
-->

* Free/Open Hardware
* Free/Open Software
* Free/Open Realware
* Free/Open Spectrum

# Soberania computacional

* São as 4 liberdades do software livre (rodar, estudar, redistribuir, melhorar).
* Acrescidas de segurança, privacidade e resiliência.
* É o poder de autocontrole em cada nível da pilha informacional.

# Mas como?

* Operando em todos os níveis!
* Tendendo do centralizado para o distribuído.
* Provendo acesso e serviços livres de tecnotoxinas.
* Fomentando pequenos negócios e/ou sem fins lucrativos.
* Atingindo massa crítica!

# Contra nós

\centerline{
  \includegraphics[height=2in]{images/tumblr_m4t0yiEn4A1qbctnjo1_1280.jpg}
}

<!--
![Fuck The System!](images/tumblr_m4t0yiEn4A1qbctnjo1_1280.jpg)
-->

* Prop. intelectual: segredos industriais, patentes, copyright.
* Recursos limitados: humanos, computacionais, materiais.
* IoT proprietária e barata em instalação.

# A nosso favor

\centerline{
  \includegraphics[height=1.5in]{images/powertothepeople-232x300.jpg}
}

<!--
![Power to the people!]()
-->

* Vontade!
* Prototipagem barata.
* Miríade de protocolos e padrões (IPv6, redes mesh, etc).
* Plataformas de desenvolvimento contemporâneas.
* Recursos limitados incentivam a criatividade e a simplicidade.

# Hora do Tour!

\centerline{
  \includegraphics[height=1.5in]{images/believe.jpg}
  \includegraphics[height=1.5in]{images/know.jpg}
}

<!--
![Believe](images/believe.jpg)
![Know](images/know.jpg)
-->

\centerline{
  Falar é fácil, mostre-nos o código!
}

# Free/Open Hardware

\centerline{
  \includegraphics[height=1.5in]{images/fernvale-frond-top_sm.jpg}
  \includegraphics[height=1.5in]{images/novena-laptop.jpg}
}

<!--
![Novena Laptop](images/novena-laptop.jpg)
-->

* Fernvale e Novena laptop.
* Raspberry Pi e Arduino são apenas a ponta do iceberg!
* Onde está a foundry aberta?

# Free/Open Software

\centerline{
  \includegraphics[height=1in]{images/bitmask.png}
  \includegraphics[height=1in]{images/noosfero.png}
}

<!--
![Bitmask](images/bitmask.png)
![Noosfero](images/noosfero.png)
-->

* Temos quase todo o stack usável e smartphones são a próxima fronteira.
* https://rhatto.fluxo.info/services

# Free/Open Realware

\centerline{
  \includegraphics[height=1.5in]{images/16511Neodymium.jpg}
  \includegraphics[height=1.5in]{images/impressora-3d-metamaquina-2-346301-MLB20305828066_052015-O.jpg}
}

<!--
![Biohack](images/16511Neodymium.jpg)
![Impressora 3D](images/impressora-3d-metamaquina-2-346301-MLB20305828066_052015-O.jpg)
-->

* Manufatura.
* Biohack!

# Free/Open Spectrum

\centerline{
  \includegraphics[height=2in]{images/gnuradio_board.jpg}
}

<!--
![GNU Radio USRP](images/gnuradio_board.jpg)
-->

* Redes de rádio definido por software!
* http://www.redeslivres.org.br
* http://gnuradio.org

TL;DR
=====

1. Distopia e utopia são dois aspectos da realidade que impulsionam mudanças sociais e evitam o abismo.
2. Soberania computacional é a aplicação das 4 liberdades do software livre junto com segurança e privacidade em todos os níveis.
3. É do esgotamento do modelo atual que as alternativas ganham força.
4. As comunidades de software livre são fundamentais neste processo e qualquer pessoa pode ajudar! :D

Referências
-----------

Iconografia dos slides:

* Teletela:
  * http://www.thedailybeast.com/articles/2015/02/05/your-samsung-smarttv-is-spying-on-you-basically.html
  * http://cdn.thedailybeast.com/content/dailybeast/articles/2015/02/05/your-samsung-smarttv-is-spying-on-you-basically/jcr:content/image.crop.800.500.jpg/1423182746216.cached.jpg
* http://images.mentalfloss.com/sites/default/files/apple_1984_ad_5.jpg
* https://twitter.com/xor/status/564356757007261696/photo/1
* http://sd.keepcalm-o-matic.co.uk/i/keep-calm-and-punk-s-not-dead-1.png
* https://3.bp.blogspot.com/_M5zGAdrZhzY/THr0FBR27wI/AAAAAAAAASc/POuRf7w9wZA/s1600/TheWares.jpg
* http://betanews.com/wp-content/uploads/2015/07/hacking_team_hacked-600x375.jpg
* https://gs1.wac.edgecastcdn.net/8019B6/data.tumblr.com/tumblr_m4t0yiEn4A1qbctnjo1_1280.jpg
* https://prod01-cdn03.cdn.firstlook.org/wp-uploads/sites/1/2014/02/screenshot14.png
* https://img1.etsystatic.com/012/0/6821980/il_570xN.424040343_oha3.jpg
* https://upload.wikimedia.org/wikipedia/commons/6/64/Gnu_meditate_levitate.png
* https://upload.wikimedia.org/wikipedia/commons/d/d2/PRISM_logo_(PNG).png
* http://ad7zj.net/kd7lmo/images/gnuradio_board.jpg
* http://liliputing.com/wp-content/uploads/2014/04/novena-laptop.jpg
* http://mlb-s1-p.mlstatic.com/impressora-3d-metamaquina-2-346301-MLB20305828066_052015-O.jpg
* http://www.healthguidance.org/hgimages/16511Neodymium.jpg
* https://s-media-cache-ak0.pinimg.com/736x/72/52/00/7252006bbb9fe9ea73f991b682f3e308.jpg
