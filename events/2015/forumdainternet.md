[[!meta title="V Fórum da Internet no Brasil"]]

* http://forumdainternet.cgi.br
* Trilha 3 - Cibersegurança e Confiança

Temas
-----

* Soberania computacional.
* Internacional:
  * https://twitter.com/ghappour/status/517061519738015744
  * https://twitter.com/ghappour/status/517060726448332800
  * Patriot Act, EO12333, FISA/FAA, Freedom Act.
* Brasil:
  * Lei do Grampo.
  * Lei Dieckman.
  * Marco Civil.
  * Proteção de Dados Pessoais.
* Casos emblemáticos: PRISM (cooperação) / MUSCULAR (enganação).
* Tipos de ataques:
  * Escalada de privilégios.
  * DDoS.
  * Bloqueio.
  * Desvio de rotas, MITM.
  * Psywar.
* Metas pé-no-chão:
  * Mais fibra é bom, mas não é necessariamente uma questão de segurança e sim de redundância e autonomia.
  * Autoridade certificadora brasileira nos navegadores.
