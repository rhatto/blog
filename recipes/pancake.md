[[!meta title="Panquecas da minha Avó"]]

## Ingedientes

* 2 xícaras (tamanho chá) de leite de vaca.
* 3 ovos não-galados e sem salmonela.
* 1 xícara de (tamanho chá) de farinha de trigo.
* 1 pitada de sal.

## Procedimento

* Bater tudo no liquidificador (exceto as xícaras, as cascas dos ovos, o próprio liquidificador e o universo circundante).
* Untar uma frigideira.
* Colocar uma dose (xícara tamanho café) da massa na frigideira.
* Abusar da sua habilidade para virar a massa quando estiver consistente. Recomendo a técnica de lançamento giratório da panqueca ao ar com show pirotécnico.
* Empilhar as panquecas ou enrolá-las com recheio doce ou salgado.

## Observações

* Se a massa ficar muito fina, adicionar um pouco mais de farinha.
