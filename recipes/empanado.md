[[!meta title="Massa para empanado"]]

* 1/2 xícara (chá) de maizena
* 1 xícara (chá) de farinha de trigo
* 1 colher chá de fermento em pó
* 1 colher chá de sal
* 1 ovo
* 1/4 xícara chá de óleo
* 1 xícara chá de leite

Bater tudo no liquidificador; depois, se quiser, acrescente
cheiro verde a gosto. Dá pra empanar beringela, couve-flor,
banana, frango, peixe etc
