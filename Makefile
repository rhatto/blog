#
# Global Makefile - https://templater.fluxo.info
#
# This Makefile contains basic, common targets and also includes
# any Makefile.* available in the current folder.
#

# Port to serve content
HTTP_PORT="8000"
HTTP_SERVER="http.server"

# Base to serve the content
HTTP_BASE="."

# Set CONTAINER based in what we have available in the system
# This variable can be user in other, included Makefiles to handle virtualization tasks
ifeq ($(shell which kvmx > /dev/null && test -s kvmxfile && echo yes), yes)
	CONTAINER = kvmx
else ifeq ($(shell which vagrant > /dev/null && test -s Vagrantfile && echo yes), yes)
	CONTAINER = vagrant
else ifeq ($(shell which docker > /dev/null && test -s Dockerfile && echo yes), yes)
	CONTAINER = docker
else
  CONTAINER = ''
endif

# See http://unix.stackexchange.com/questions/32182/simple-command-line-http-server#32200
#     http://php.net/manual/en/features.commandline.webserver.php
serve:
	@if [ "$(HTTP_SERVER)" = "SimpleHTTPServer" ]; then cd $(HTTP_BASE) && python -m SimpleHTTPServer $(HTTP_PORT);              fi
	@if [ "$(HTTP_SERVER)" = "ssi_server"       ]; then cd $(HTTP_BASE) && PYTHONDONTWRITEBYTECODE=0 ssi_server.py $(HTTP_PORT); fi
	@if [ "$(HTTP_SERVER)" = "http.server"      ]; then cd $(HTTP_BASE) && python3 -m http.server $(HTTP_PORT);                  fi
	@if [ "$(HTTP_SERVER)" = "php"              ]; then cd $(HTTP_BASE) && php -S localhost:$(HTTP_PORT);                        fi

# Configure a git post-receive hook
post_receive:
	git config receive.denyCurrentBranch ignore
	test -s bin/post-receive && cd .git/hooks && ln -sf ../../bin/post-receive

# Process any other Makefile whose filename matches Makefile.*
# See https://www.gnu.org/software/make/manual/html_node/Include.html
#
# Some of those files might even contain local customizations/overrides
# that can be .gitignore'd, like a Makefile.local for example.
-include Makefile.*

# Customization examples can be as simple as setting variables:
#CONTAINER  = vagrant
#CONTAINER  = docker
#DESTDIR   ?= vendor
